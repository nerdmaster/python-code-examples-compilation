# -*- coding: utf-8 -*-
#!/usr/bin/python
"""
Created on Sat Sep 13 02:45:08 2014

@author: benito
"""

#%%
#<- Code Begins:
#---------------------------------------------------------
def hello(name):
    print 'Hello, ', name

class MyClass:
    """A simple example class"""
    variable = "blah"

    def function(self):
        return "This is a message inside the class."

    def __init__(self):
        self.variable = ""

#    def __init__(self, value):
#        self.variable = value

def main():
    hello("World!")
    myobjectx = MyClass()
    # To access the variable inside of the newly created
    # object "MyObject" you would do the following:
    print myobjectx.variable         # returns: '' (nothing)
    myobjectx.variable = "yackity" # changes objectx's var
    print myobjectx.variable         # returns: 'yackity'
    # To access a function inside of an object you use
    # notation similar to accessing a variable:
    print myobjectx.function()       # returns:
                       # 'This is a message inside the class.'

if __name__ == '__main__':
    main()
#---------------------------------------------------------
#<- Code Ends.
#%%
