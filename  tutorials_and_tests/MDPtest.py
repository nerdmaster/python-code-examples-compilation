# MDP:: Modular toolkit for Data Processing
import mdp
import numpy as np
import pylab as plt

pcanode1 = mdp.nodes.PCANode()

x = np.random.random((100, 25))  # 25 variables, 100 observations

pcanode1.train(x)

pcanode1

for i in range(100):
    x = np.random.random((100, 25))
    pcanode1.train(x)


y = mdp.pca(x)

z = mdp.fastica(x, dtype='float32')

# ----------------

x = np.random.random((100, 25))  # 25 variables, 100 observations

pcanode1.train(x)
pcanode1.stop_training()
pcanode1.output_dim


avg = pcanode1.avg            # mean of the input data
v = pcanode1.get_projmatrix() # projection matrix

fdanode = mdp.nodes.FDANode()

for label in ['a', 'b', 'c']:
    x = np.random.random((100, 25))
    fdanode.train(x, label)

xTest = np.random.random((100, 25))
y_pca = pcanode1.execute(xTest)

x2 = np.random.random((100, 5))
y_exp = expnode(x2)

x3 = np.random.random((100, 25))
y_fda = fdanode(x3)

plt.plot(xTest,'.')
plt.grid()
plt.show()

# ----------------

# We start by generating some input signal at random
inp = np.random.random((1000, 20))

# Rescale input to have zero mean and unit variance
inp = (inp - np.mean(inp, 0))/np.std(inp, axis=0, ddof=0)

# We reduce the variance of the last 15 components,
# so that they are going to be eliminated by PCA

inp[:,5:] /= 10.0

# Mix the input signals linearly
x = mdp.utils.mult(inp,np.random.random((20, 20)))

# x is now the training data for our simulation.
# In the same way we also create a test set x_test.
inp_test = np.random.random((1000, 20))
inp_test = (inp_test - np.mean(inp_test, 0))/np.std(inp_test, 0)
inp_test[:,5:] /= 10.0
x_test = mdp.utils.mult(inp_test, np.random.random((20, 20)))

# We could now perform our analysis using only nodes, that’s the lengthy way...
# -> Perform PCA
pca = mdp.nodes.PCANode(output_dim=5)
pca.train(x)
out1 = pca(x)

# -> Perform ICA using CuBICA algorithm
ica = mdp.nodes.CuBICANode()
ica.train(out1)
out2 = ica(out1)

# Find the three largest local maxima in the output of the ICA node
# when applied to the test data, using a HitParadeNode
out1_test = pca(x_test)
out2_test = ica(out1_test)
hitnode = mdp.nodes.HitParadeNode(3)
hitnode.train(out2_test)
maxima, indices = hitnode.get_maxima()

# or we could use flows, which is the best way
## flow = mdp.Flow([mdp.nodes.PCANode(output_dim=5), mdp.nodes.CuBICANode()])

# Note that flows can be built simply by concatenating nodes
flow = mdp.nodes.PCANode(output_dim=5) + mdp.nodes.CuBICANode()

# Train the resulting flow
flow.train(x)

# Now the training phase of PCA and ICA are completed.
# Next we append a HitParadeNode which we want to train on the test data

## flow.append(mdp.nodes.HitParadeNode(3))

# As before, new nodes can be appended to an existing flow by adding them ot it
flow += mdp.nodes.HitParadeNode(3)

# Train the HitParadeNode on the test data
flow.train(x_test)
maxima, indices = flow[2].get_maxima()

# Just to check that everything works properly, we can calculate covariance
# between the generated sources and the output (should be approximately 1)
out = flow.execute(x)
cov = np.amax(abs(mdp.utils.cov2(inp[:,:5], out)), axis=1)
print cov

# Flow objects are defined as Python containers, and thus
# are endowed with most of the methods of Python lists.

# You can loop through a Flow
for node in flow:
    print repr(node)

#






