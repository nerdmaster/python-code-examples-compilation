# -*- coding: utf-8 -*-
#!/usr/bin/python

#----------------------------------------------------------

class Parent:        # define parent class
    parentAttr = 100
    __secretAttr = 0    # object's attribute not be visible
                        # outside the class definition
    def __init__(self):
        print "Calling parent constructor"
#
    def count(self):
        self.__secretAttr += 1
        print self.__secretAttr
#
    def parentMethod(self):
        print 'Calling parent method'
#
    def setAttr(self, attr):
        Parent.parentAttr = attr
#
    def getAttr(self):
        print "Parent attribute :", Parent.parentAttr
#
    def __str__(self):
        return 'Printing Parent Attibute [parentAttr]: %d.' % self.parentAttr

#----------------------------------------------------------

class Child(Parent): # define child class
    def __init__(self):
        print "Calling child constructor"
#
    def myMethod(self):
        print "Calling 'myMethod()' by child (method overwritten)"
#
    def childMethod(self):
        print 'Calling child method'

#----------------------------------------------------------

print "\nFamiglia:: creating instance of child"
c = Child()          # instance of child
print "\nFamiglia:: calling 'parentMethod()' on child"
c.parentMethod()     # calls parent's method
print "\nFamiglia:: calling 'setAttr(200)' on child"
c.setAttr(200)       # again call parent's method
print "\nFamiglia:: calling 'getAttr()' on child"
c.getAttr()          # again call parent's method

print "\nFamiglia:: calling 'childMethod()' on child"
c.childMethod()      # child calls its method
print "\nFamiglia:: calling 'myMethod()' on child"
c.myMethod()         # child calls overridden method

print "\nFamiglia:: calling 'print c' [__str__(self)]"
print c

print "\nFamiglia:: calling 'count()' on child"
c.count()
c.count()



"""
>>>
>>> print "\nFamiglia:: calling 'childMethod()' on child"

Famiglia:: calling 'childMethod()' on child
>>> c.childMethod()      # child calls its method
Calling child method
>>> print "\nFamiglia:: calling 'myMethod()' on child"

Famiglia:: calling 'myMethod()' on child
>>> c.myMethod()         # child calls overridden method
Calling 'myMethod()' by child (method overwritten)
>>>
>>> print "\nFamiglia:: calling 'print c' [__str__(self)]"

Famiglia:: calling 'print c' [__str__(self)]
>>> print c
Parent Attibute [parentAttr]: 200.
>>>
>>> print "\nFamiglia:: calling 'count()' on child"

Famiglia:: calling 'count()' on child
>>> c.count()
1
>>> c.count()
2
>>>
"""
