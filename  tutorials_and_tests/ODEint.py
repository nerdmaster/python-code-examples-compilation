#! /usr/bin/env python
#
#   File: "ODEint.py"
#------------------------------------------#
#   Python ODE integration Demo
#   (c) 2013 Benito R. Fernandez
#       The University of Texas at Austin
#

import numpy as np
import numpy.matlib as ml
#import pylab as plt
import matplotlib.pyplot as plt
# where in Mac OS instal: /Library/Python/2.7/site-packages/matplotlib-1.4.x-py2.7-macosx-10.8-intel.egg/matplotlib/pyplot.py

from scipy.integrate import odeint

mu, sigma = 0.0, 0.1
eta, lamda = 2.5, 1.0
Adist = 0.25

def f(X, t):
    x, y = X
    #-print t, x, y
    Free  = [ y,
             -2*x -0.3*y \
             +(0.5-2*x)+2*y*x-5*y-3*y*np.sin(y)   ]
    xdot, ydot = Free
    noise = sigma*ml.randn(1)+mu
    #-print noise
    Fdist = [ 0.0,
             -0*Adist*np.cos(3*t)*np.exp(-0.5*t)
             +noise[0,0] ]
    s = y + lamda*x
    #-print s
    u_eq = -ydot - lamda*xdot
    u_s  = -eta*np.tanh(s) # sign() doesn't converge
    u = u_eq + u_s
    #-print u_eq, u_s
    Fsmc  = [0.0,
             u ]
    Xdot = np.add(Free, np.add(Fdist, Fsmc))
    #-print t, Free, Fdist, Fsmc
    #-print Xdot
    return Xdot

init = [ 1.2, 0.5 ]
t = np.linspace(0, 2.0, 1000)
X = odeint(f, init, t)

x = X[:, 0]
y = X[:, 1]

plt.ion()

f1 = plt.figure(1)
plt.plot(x[1],y[1], 'bo', lw=1)
plt.plot(x,y, 'r-', lw=1)
plt.xlabel('x [m]')
plt.ylabel('y [m]')
plt.title('ODEint phase space ($\sigma$)')
plt.grid()
#plt.show()

f2 = plt.figure(2)
plt.plot(t,X, lw=1)
plt.xlabel('t [s]')
plt.ylabel('x, y [m]')
plt.title('ODEint response ($\mu$)')
plt.grid()
#plt.show()

s = y + lamda*x
f3 = plt.figure(3)
plt.plot(t,s, lw=1)
plt.xlabel('t [s]')
plt.ylabel('s')
plt.title('ODEint surface response ($\eta$)')
plt.grid()
plt.show()

#xdot = y
#ydot = -2*x -0.3*y +(0.5-2*x)+2*y*x-5*y-3*y*np.sin(y)
#u_eq = -ydot - lamda*xdot
#u_s  = -eta*np.sign(s)
#u = u_eq + u_s
#plt.plot(t,u_eq, lw=1, t,u_s, lw=1)
#plt.xlabel('t [s]')
#plt.ylabel('u_eq, u_s')
#plt.title('ODEint control response ($\alpha$)')
#plt.grid()
#plt.show()
