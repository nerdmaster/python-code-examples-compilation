#!/usr/bin/python
# plotSines.py

# Numpy is a library for handling arrays (like data points)
import numpy as np

# Pyplot is a module within the matplotlib library for plotting
import matplotlib.pyplot as plt

# Create an array of 256 linearly-spaced points from -pi to pi
x = np.linspace(-np.pi, np.pi, 256,endpoint=True)
s, c = np.sin(x), np.cos(x)

# Create a new figure of size 8x6 points, using 80 dots per inch
plt.figure(figsize=(10,8), dpi=80)

# Create a new subplot from a grid of 1x1
plt.subplot(1,1,1)

# Create the plot
# plot first graph (sines)
    ##-> plt.plot(x,s,label=r'sin(t)')
    ##-> # Changing colors and line widths
    ##-> plt.plot(x, c, color="blue", linewidth=2.5, linestyle="-")
plt.plot(x, c, color="blue", linewidth=2.5, linestyle="-", label="cos(t)")

# plot second graph (cosines)
    ##-> plt.plot(x,c,label=r'cos(t)')
    ##-> # Changing colors and line widths
    ##-> plt.plot(x, s, color="red",  linewidth=2.5, linestyle="-")
plt.plot(x, s, color="red",  linewidth=2.5, linestyle="-", label="sin(t)")

# add grid
plt.grid(which='major')

# add legend (labels defined already)
plt.legend(loc='upper left')

# give plot a title
theTitle = 'Plot of sin(t) & cos(t) '
plt.title(theTitle)

# make axis labels
plt.xlabel('t [s]')
plt.ylabel('sin(t) & cos(t) [m]')

# Setting limits
hRange = abs(x.max()-x.min())
sRange = abs(s.max()-s.min())
cRange = abs(c.max()-c.min())
vRange = max(sRange,cRange)
#
plt.xlim(x.min()-0.1*hRange, x.max()+0.1*hRange)
plt.ylim(c.min()-0.1*vRange, c.max()+0.1*vRange)

# Setting tick labels
##-> # Set x ticks
##-> plt.xticks(np.linspace(-4,4,9,endpoint=True))
plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],
           [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$+\pi/2$', r'$+\pi$'])

##-> # Set y ticks
##-> plt.yticks(np.linspace(-1,1,5,endpoint=True))
plt.yticks([-1, 0, +1],
           [r'$-1$', r'$0$', r'$+1$'])
       
plt.hold(True)

# Moving spines
ax = plt.gca()
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data',0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data',0))

# Annotate some points
t = 2*np.pi/3
plt.plot([t,t],[0,np.cos(t)], color ='blue', linewidth=2.5, linestyle="--")
plt.scatter([t,],[np.cos(t),], 50, color ='blue')

plt.annotate(r'$\sin(\frac{2\pi}{3})=\frac{\sqrt{3}}{2}$',
             xy=(t, np.sin(t)), xycoords='data',
             xytext=(+10, +30), textcoords='offset points', fontsize=16,
             arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"))

plt.plot([t,t],[0,np.sin(t)], color ='red', linewidth=2.5, linestyle="--")
plt.scatter([t,],[np.sin(t),], 50, color ='red')

plt.annotate(r'$\cos(\frac{2\pi}{3})=-\frac{1}{2}$',
             xy=(t, np.cos(t)), xycoords='data',
             xytext=(-90, -50), textcoords='offset points', fontsize=16,
             arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"))

# Devil is in the details
for label in ax.get_xticklabels() + ax.get_yticklabels():
    label.set_fontsize(16)
    label.set_bbox(dict(facecolor='white', edgecolor='None', alpha=0.65 ))


plt.hold(False)

# Save the figure in a separate file using 72 dots per inch
plt.savefig('sine_cosine_function.png',dpi=72)

# Draw the plot to the screen
plt.show()                      # <-------------------------------


