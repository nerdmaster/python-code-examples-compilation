#!/usr/bin/env python
#
# trying OOP with Python
#
class pet:
    number_of_legs = 0
    petType = ""
    petName = ""

    def count_legs(self):
        print "I have %s legs" % self.number_of_legs
        return self

    def sleep(self):
        print "zzz"
        return self

    def whatAmI(self):
        print "I am a ", self.petType

    def set_Legs(self, legs):
        self.number_of_legs = legs
        return self

#    def myName(self):

class dog(pet):
    def __init__(self):
        self.number_of_legs = 4
        self.petType = "dog"

#    def __init__(self,petType,petName):
#        self.number_of_legs = 4
#        self.petType = petType
#        self.petName = petName


    def bark(self):
        print "Woof"

#    def whatAmI(self):
#        print "I am a dog"

# - Running ...

doug = pet()
doug.number_of_legs = 4
print "Doug has %s legs." % doug.number_of_legs
doug.count_legs()
doug.sleep()

nemo = pet()
nemo.count_legs()

tintin = dog()
tintin.whatAmI()
tintin.bark()

doug.set_Legs(4).count_legs().sleep().sleep().sleep()
