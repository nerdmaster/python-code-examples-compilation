#!/usr/bin/env python

import networkx as nx
import matplotlib
matplotlib.use ('Agg')
import matplotlib.pyplot as plt
plt.ion() #toggles "interactive mode"
#import pylab as pl #import Matplotlib plotting interface
import time

def printMe(label,variable):
    print('--->>> %s\n    |') % label,
    print(variable)

def runCommand(command):
    print('--->>> %s') % str(command)
    exec command

def evalCommand(command):
    x = eval(command)
    return x

def draw_graph() :
    G = nx.Graph ()
    G.add_edges_from ([(1,2), (2,3), (1,3), (1,4)])
    nx.draw(G)
    plt.savefig ("simple_graph.png")
    plt.hsow()

start = time.clock()

#draw_graph()

print('+------------------------------------+')
print('| "networkx.py" graph creation demo  |')
print('|  (c) 2013 Benito R. Fernandez      |')
print('+------------------------------------+')
print('\n----------------------------------------------------------------')
print('-->: create graph: ')
print('--->>> G=nx.Graph()')
G=nx.Graph()
runCommand('G=nx.Graph()')
print('--->>> G.clear()')
G.clear()
runCommand('print 99')

#--------------------------------
print('--->>> G.add_edge(1,2)')
G.add_edge(1,2)
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [1, 2, 'spam']
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2)]

print('\n----------------------------------------------------------------')
print('--->>> G.add_nodes_from([2,3])')
G.add_nodes_from([2,3])
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [1, 2, 3, 'spam']
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2)]

print('\n----------------------------------------------------------------')
print('--->>> H=nx.path_graph(10)')
H=nx.path_graph(10)
#--------------------------------
print('--->>> G.add_nodes_from(H)')
G.add_nodes_from(H)
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [0, 1, 2, 3, 4, 5, 6, 'spam', 8, 9, 7]
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2)]

print('\n----------------------------------------------------------------')
print('--->>> e=(2,3)')
e=(2,3)
print('--->>> G.add_edge(*e) # unpack edge tuple*')
G.add_edge(*e) # unpack edge tuple*
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [0, 1, 2, 3, 4, 5, 6, 'spam', 8, 9, 7]
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2), (2, 3)]

print('\n----------------------------------------------------------------')
print('--->>> G.add_edges_from([(1,2),(1,3)])')
G.add_edges_from([(1,2),(1,3)])
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [0, 1, 2, 3, 4, 5, 6, 'spam', 8, 9, 7]
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2)]

print('\n----------------------------------------------------------------')
print('--->>> G.add_nodes_from("spam")')
G.add_nodes_from("spam") # adds 4 nodes: 's', 'p', 'a', 'm'
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [0, 1, 2, 3, 4, 5, 6, 'spam', 8, 9, 7, 'm', 'p', 's', 'a']
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2), (1, 3), (2, 3)]

print('\n----------------------------------------------------------------')
print('--->>> G.number_of_nodes()')
print('    |______________________')
print('    |'),
print(G.number_of_nodes()) # 15
print('--->>> G.number_of_edges()')
print('    |______________________')
print('    |'),
print(G.number_of_edges()) #  3
print('--->>> G.neighbors(1)')
print('    |______________________')
print('    |'),
print(G.neighbors(1)) # [2, 3]

print('\n----------------------------------------------------------------')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [0, 1, 2, 3, 4, 5, 6, 'spam', 8, 9, 7]
print('\n--->>> G.remove_nodes_from("spam")')
G.remove_nodes_from("spam")
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [0, 1, 2, 3, 4, 5, 6, 'spam', 8, 9, 7]
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2), (1, 3), (2, 3)]

print('\n----------------------------------------------------------------')
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2), (1, 3), (2, 3)]
print('\n--->>> G.remove_edge(1,3)')
G.remove_edge(1,3)
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [0, 1, 2, 3, 4, 5, 6, 'spam', 8, 9, 7]
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2)]
print('\n----------------------------------------------------------------')
print('--->>> edgelist=[(0,1),(5,2),(7,3)]')
edgelist=[(0,1),(5,2),(7,3)]
print('    |  edgelist')
print('    |'),
print(edgelist)
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2), (2, 3)]
print('--->>> for e in edgelist')
print('--->>>     G.add_edge(*e)')
for e in edgelist:
    G.add_edge(*e) # unpack edge tuple*
    print('    |'),
    print(G.edges()) # [(1, 2), (2, 3)],
                     # [(0, 1), (1, 2), (2, 3)]
                     # [(0, 1), (1, 2), (2, 3), (2, 5)]
                     # [(0, 1), (1, 2), (2, 3), (2, 5), (3, 7)]
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [0, 1, 2, 3, 4, 5, 6, 'spam', 8, 9, 7]
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2), (2, 3)]

print('\n----------------------------------------------------------------')
print('--->>> G.add_edge(1,3)')
G.add_edge(1,3)
print("--->>> G[1][3]['color']='blue'")
print('\n--->>> G[1][3]')                   #  {}
print('    |'),
print(G[1][3])
G[1][3]['color']='blue'
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [0, 1, 2, 3, 4, 5, 6, 'spam', 8, 9, 7]
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(0, 1), (1, 2), (1, 3), (2, 3), (2, 5), (3, 7)]
print('\n--->>> G[1][3]')
print('    |'),
print(G[1][3])                   #  {'color': 'blue'}
print('\n----------------------------------------------------------------')
print("--->>> G.add_node(10, time='5pm')")
G.add_node(10, time='5pm')
print('--->>> G.add_node(12, day="Friday")')
G.add_node(12, day="Friday")
print('\n--->>> G[1][3]')
print('\n--->>> G.node[10]')
print('    |'),
print(G.node[10])                   #  {'time': '5pm'}
print('--->>> G.node[10]')
print('    |'),
print(G.node[12])                   #  {'day': 'Friday'}
print('\n----------------------------------------------------------------')
print('--->>> G.add_edge(1,3)')
print('--->>> G.add_edge(10, 12, weight=4.7 )')
G.add_edge(10,12, weight=4.7)
print("--->>> G.add_nodes_from([3], time='2pm')")
G.add_nodes_from([3], time='2pm')
print("--->>> G.node[1]['room'] = 714")
G.node[3]['room'] = 714
print("--->>> G.nodes(data=True)")
print("--->>> print(G.nodes(data=True))")
print('    |'),
print(G.nodes(data=True))
print('\n----------------------------------------------------------------')
print("--->>> G.add_edges_from([(3,4),(4,5)], color='red')")
G.add_edges_from([(3,4),(4,5)], color='red')
print("--->>> G.add_edges_from([(1,2,{'color':'blue'}), (2,3,{'weight':8})])")
G.add_edges_from([(1,2,{'color':'blue'}), (2,3,{'weight':8})])
print("--->>> G[1][2]['weight'] = 4.7")
G[1][2]['weight'] = 4.7
print("--->>> G.edge[1][2]['weight'] = 4")
G.edge[1][2]['weight'] = 4
print(G.edges(data=True))
print('\n----------------------------------------------------------------')
print('--->>> Print All nodes'),
print('for i in G:')
print('--->>>     for nbr,eattr in nbrs.items():')
print("--->>>         print('G[i]: ',G[i]")
print("--->>>         for j in G[i]:")
print("--->>>             print('G[i][j]: ',str(G[i][j]))")
for i in G:
    pLabel = "    | G[" + str(i) + "]:  "
    print(pLabel),
    print(G[i]),
    print(' <--| '),
    print(G.node[i])
    for j in G[i]:
        pLabel = "    | G[" + str(i) + "," + str(j) + "]:"
        print(pLabel),
        print(str(G[i][j]))
print('\n----------------------------------------------------------------')
print('--->>> FG=nx.Graph()')
FG=nx.Graph()
print('--->>> weightedEdges = [(1,2,0.125),(1,3,0.75),(2,4,1.2),(3,4,0.375)]')
weightedEdges = [(1,2,0.125),(1,3,0.75),(2,4,1.2),(3,4,0.375)]
print('--->>> FG.add_weighted_edges_from(weightedEdges)')
FG.add_weighted_edges_from(weightedEdges)
print('--->>> for n,nbrs in FG.adjacency_iter():')
print('--->>>     for nbr,eattr in nbrs.items():')
print("--->>>         if data<0.5: print('(%d, %d, %.3f)' % (n,nbr,data))")
for n,nbrs in FG.adjacency_iter():
    for nbr,eattr in nbrs.items():
        data=eattr['weight']
        if data<0.5:
            print('    |'),
            print('(%d, %d, %.3f)' % (n,nbr,data))
print('\n----------------------------------------------------------------')
print('--->>> MG=nx.MultiGraph()')
MG=nx.MultiGraph()
print('--->>> MG.add_weighted_edges_from([(1,2,.5), (1,2,.75), (2,3,.5)])')
MG.add_weighted_edges_from([(1,2,.5), (1,2,.75), (2,3,.5)])
print("--->>> MG.degree(weight='weight')")      # {1: 1.25, 2: 1.75, 3: 0.5}
print(MG.degree(weight='weight'))
print("--->>> GG=nx.Graph()")
GG=nx.Graph()
print("--->>> for n,nbrs in MG.adjacency_iter():")
print("--->>>     for nbr,edict in nbrs.items():")
print("--->>>         minvalue=min([d['weight'] for d in edict.values()])")
print("--->>>         GG.add_edge(n,nbr, weight = minvalue)")
for n,nbrs in MG.adjacency_iter():
    for nbr,edict in nbrs.items():
        minvalue=min([d['weight'] for d in edict.values()])
        GG.add_edge(n,nbr, weight = minvalue)
print("--->>> nx.shortest_path(GG,1,3)")
print(nx.shortest_path(GG,1,3))                # [1, 2, 3]
print('\n----------------------------------------------------------------')
for i in range(0,len(G)-1):
    print 'G[', str(i), ']: ', G[i]

print(  '----------------------------------------------------------------')
print(  '----------------------------------------------------------------')
print(  '----------------------------------------------------------------')
print(  '----------------------------------------------------------------')
print(  '----------------------------------------------------------------')
print(  '----------------------------------------------------------------')
print('--->>> DG=nx.DiGraph()')
DG=nx.DiGraph()
print('--->>> DG.add_weighted_edges_from([(1,2,0.5), (3,1,0.75)])')
DG.add_weighted_edges_from([(1,2,0.5), (3,1,0.75)])

print("--->>> DG.out_degree(1,weight='weight')")        # 0.5
print(DG.out_degree(1,weight='weight'))

printMe("DG.out_degree(1)",DG.out_degree(1,weight='weight'))

print "--->>> DG.out_degree(1,weight='weight'): %f" % DG.out_degree(1,weight='weight')

print("--->>> DG.degree(1,weight='weight')")            # 1.25
DG.degree(1,weight='weight')
print(DG.degree(1,weight='weight'))
print('--->>> DG.successors(1)')                        # [2]
print(DG.successors(1))
print('--->>> DG.neighbors(1)')                         # [2]
print(DG.neighbors(1))
print('\n----------------------------------------------------------------')
print('\n----------------------------------------------------------------')
print('|-->: Analyzing graphs')
#--------------------------------
print('--->>> nx.connected_components(G)')
print('    |'),
print(nx.connected_components(G))

print('--->>> sorted(nx.degree(G).values())')
print('    |'),
print(sorted(nx.degree(G).values()))

print('--->>> print(nx.clustering(G))')
print('    |'),
print(nx.clustering(G))

print('--->>> print(nx.degree(G))')
print('    |'),
print(nx.degree(G))
# import matplotlib.pyplot as plt

# nx.draw(G)
# plt.show()
# plt.savefig("path.png")

#  If Graphviz and PyGraphviz, or pydot, are available on your system,
#  you can also use:
# nx.draw_graphviz(G)
# nx.write_dot(G,'file.dot')

#nx.draw_random(G)
#nx.draw_circular(G)
#nx.draw_spectral(G)

print('\n----------------------------------------------------------------')
print('\n--->>> G.add_nodes_from([2,3])')
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [0, 1, 2, 3, 4, 5, 6, 'spam', 8, 9, 7]
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2)]

print('\n----------------------------------------------------------------')
print('\n----------------------------------------------------------------')
print('\n--->>> G.add_nodes_from([2,3])')
print('    |______________________')
print('    |  graph: nodes')
print('    |'),
print(G.nodes()) # [0, 1, 2, 3, 4, 5, 6, 'spam', 8, 9, 7]
print('    |______________________')
print('    |  graph: edges')
print('    |'),
print(G.edges()) # [(1, 2)]

print('\n----------------------------------------------------------------')
print(  '------------------------- END OF DEMO. -------------------------')
print(  '----------------------------------------------------------------')

#H=nx.DiGraph(G)
#H.edges()                 # [(1, 2), (2, 1)]
edgelist=[(0,1),(1,2),(2,3),(3,1),(3,4),(4,0)]
H=nx.Graph(edgelist)
print 'Edges: ', H.edges()
print 'Nodes: ', H.nodes()

nx.draw(H)
plt.show()
plt.savefig("netWorkXgraph.png")
#pl.show()
#
#
#
#


stop = time.clock()
print(  '\n\n----->>> ELLAPSED TIME: '),
print stop-start,
print('[s] <<<-----\n\n\a')

#file = 'wiki.txt'
#wiki = nx.read_adjlist(file, delimiter='\ t ', create_using=nx.DiGraph())
#CG=nx.complete_graph(5)

