#define the Vehicle class
class Vehicle:
    name = ""
    kind = "car"
    color = ""
    value = 100.00 
    def description(self):
        desc_str = "%s is a %s %s worth $%.2f." % (self.name, self.color, self.kind, self.value)
        return desc_str
#your code goes here

MyCar1 = Vehicle()
MyCar1.name = "Ferrari"
MyCar1.kind = "convertible"
MyCar1.color = "red"
MyCar1.value = 60000

MyCar2 = Vehicle()
MyCar2.name = "Oddisey"
MyCar2.kind = "mini-van"
MyCar2.color = "white"
MyCar2.value = 10000


#checking code
print MyCar1.description()
print MyCar2.description()