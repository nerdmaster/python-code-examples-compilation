#! /usr/bin/env python
#
#   File: "pythonDemos.py"
#------------------------------------------#
#   Python Demos
#   (c) 2013 Benito R. Fernandez
#       The University of Texas at Austin
#
import time
#--------------------------------------#-#--------------------------------------#
#00000000011111111112222222222333333333344444444445555555555666666666677777777778
#12345678901234567890123456789012345678901234567890123456789012345678901234567890
#--------------------------------------#-#--------------------------------------#
# For Python, PEP 8 -- Style Guide for Python Code --
# has emerged as the style guide that most projects adhere to;
# it promotes a very readable and eye-pleasing coding style.
# Every Python developer should read it at some point;
# here are the most important points extracted for you:
#
# -> Use 4-space indentation, and no tabs.
#    spaces are a good compromise between small indentation (allows greater nesting
#    depth) and large indentation (easier to read). Tabs introduce confusion, and
#    are best left out.
# -> Wrap lines so that they don't exceed 79 characters.
#    This helps users with small displays and makes it possible to have several
#    code files side-by-side on larger displays.
# -> Use blank lines to separate functions and classes, and larger blocks of code
#    inside functions.
# -> When possible, put comments on a line of their own.
# -> Use docstrings. (See PEP 257 for Docstring Conventions)
# -> Use spaces around operators and after commas, but not directly inside
#    bracketing constructs: a = f(1, 2) + g(3, 4).
# -> Name your classes and functions consistently; the convention is to use
#    CamelCase for classes and lower_case_with_underscores for functions and methods.
#    Always use self as the name for the first method argument
# -> Don't use fancy encodings if your code is meant to be used in international
#    environments. Plain ASCII works best in any case.

# -- http://www.python.org/dev/peps/pep-0008/
#
#    # Aligned with opening delimiter
#    foo = long_function_name(var_one, var_two,
#                             var_three, var_four)
#
#    # More indentation included to distinguish this from the rest.
#    def long_function_name(
#            var_one, var_two, var_three,
#            var_four):
#        print(var_one)
#
#    Yes: import os
#         import sys
#
#    No:  import sys, os
#
#  - Immediately inside parentheses, brackets or braces.
#
#    Yes: spam(ham[1], {eggs: 2})
#    No:  spam( ham[ 1 ], { eggs: 2 } )
#
#  - Immediately before a comma, semicolon, or colon:
#
#    Yes: if x == 4: print x, y; x, y = y, x
#    No:  if x == 4 : print x , y ; x , y = y , x
#
#  - Immediately before the open parenthesis that starts the argument list of a function call:
#
#    Yes: spam(1)
#    No:  spam (1)
#
#  - Immediately before the open parenthesis that starts an indexing or slicing:
#
#    Yes: dict['key'] = list[index]
#    No:  dict ['key'] = list [index]
#
#------------------------------------------#-#------------------------------------------#
#%%
import os
filename = os.environ.get('PYTHONSTARTUP')
if filename and os.path.isfile(filename):
    execfile(filename)

import site

start = time.clock()

site.getusersitepackages()
#(console)->|'/Users/benito/.local/lib/python2.7/site-packages'

# this is the first comment
SPAM = 1        # and this is the second comment
                # ... and now a third!
STRING = "# This is not a comment."

stop = time.clock()
print(  '\n\n----->>> ELLAPSED TIME: '),
print stop-start,
print(' <<<-----\n\n\a')

#------------------------------------------#
#%%# a little bit of math
#------------------------------------------#
x = y = z = 0  # Zero x, y and z
3 * 3.75 / 1.5
#(console)->|7.5
7.0 / 2
#(console)->|3.5

1j * complex(0,1)
#(console)->|(-1+0j)
(1+2j)/(1+1j)
#(console)->|(1.5+0.5j)
a=1.5+0.5j
a.real
#(console)->|1.5
a.imag
#(console)->|0.5
abs(a)
#(console)->|1.5811388300841898

q, r = divmod(2**56, 10)
q
#(console)->|720575940379279
r
#(console)->|6

#------------------------------------------#
#%%# conditionals
#------------------------------------------#
the_world_is_flat = 1
if the_world_is_flat:
    print "Be careful not to fall off!"

#(console)->|Be careful not to fall off!

#------------------------------------------#
#%%# Strings
#------------------------------------------#
hello = "This is a rather long string containing\n\
several lines of text just as you would do in C.\n\
    Note that whitespace at the beginning of the line is\
 significant."

print hello
#(console)->|This is a rather long string containing
#(console)->|several lines of text just as you would do in C.
#(console)->|    Note that whitespace at the beginning of the line is significant.


word = 'Help' + 'A-'
word
#(console)->|'HelpA-'
len(word)
#(console)->|6
word[4]
#(console)->|'A'
word[0:2]
#(console)->|'He'
word[2:4]
#(console)->|'lp'
word[:2]
#(console)->|'He'
word[2:]
#(console)->|'lpA-'
word[1:100]
#(console)->|'elpA-'
word[2:1]
#(console)->|''
word[-2]
#(console)->|'A'
word[-2:]
#(console)->|'A-'
word[:-2]
#(console)->|'Help'

'<' + word*5 + '>'
#(console)->|'<HelpA-HelpA-HelpA-HelpA-HelpA->'

#------------------------------------------#
#%%# Documentation Strings
#------------------------------------------#
def my_function():
    """Do nothing, but document it.

    No, really, it doesn't do anything.
    """
    pass

print my_function.__doc__
#(console)->|Do nothing, but document it.
#(console)->|
#(console)->|    No, really, it doesn't do anything.



#------------------------------------------#
#%%# variables
#------------------------------------------#
counter = 100          # An integer assignment
miles   = 1000.0       # A floating point
name    = "John"       # A string

print counter
#(console)->|100
print miles
#(console)->|1000.0
print name
#(console)->|John

#------------------------------------------#
#%%# lists
#------------------------------------------#
q = [2, 3]
p = [1, q, 4]
len(p)
#(console)->|3
p[1]
#(console)->|[2, 3]
p[1][1]
#(console)->|3
p[1].append('xtra')
p
#(console)->|[1, [2, 3, 'xtra'], 4]
q
#(console)->|[2, 3, 'xtra']

#------------------------------------------#
#%%# loops
#------------------------------------------#
# Fibonacci series:
# the sum of two elements defines the next
a, b = 0, 1
while b < 10:
    print b
    a, b = b, a+b

#(console)->|1
#(console)->|1
#(console)->|2
#(console)->|3
#(console)->|5
#(console)->|8

a, b = 0, 1
while b < 1000:
    print b,
    a, b = b, a+b

#(console)->|1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987

#------------------------------------------#
# print
#------------------------------------------#
i = 256*256
print 'The value of i is', i
#(console)->|The value of i is 65536

#------------------------------------------#
# input
#------------------------------------------#
if __name__ == '__main__':
    x = 4
else:
    x = int(raw_input("Please enter an integer: "))

#(console)->|Please enter an integer: <4>

#------------------------------------------#
# if statement
#------------------------------------------#
if x < 0:
    x = 0
    print 'Negative changed to zero'
elif x == 0:
    print 'Zero'
elif x == 1:
    print 'Single'
else:
    print 'More'

#(console)->|More

#------------------------------------------#
#%%# for statement
#------------------------------------------#
words = ['cat', 'window', 'defenestrate']
for w in words:
    print w, len(w)

#(console)->|cat 3
#(console)->|window 6
#(console)->|defenestrate 12

#------------------------------------------#
#%%# loops
#------------------------------------------#
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
        # check if even, mod==0, no residue
            print n, 'equals', x, '*', n/x
            break
        else:
        # loop fell through without finding a factor
            print n, 'is a prime number'

#(console)->|3 is a prime number
#(console)->|4 equals 2 * 2
#(console)->|5 is a prime number
#(console)->|5 is a prime number
#(console)->|5 is a prime number
#(console)->|6 equals 2 * 3
#(console)->|7 is a prime number
#(console)->|7 is a prime number
#(console)->|7 is a prime number
#(console)->|7 is a prime number
#(console)->|7 is a prime number
#(console)->|8 equals 2 * 4
#(console)->|9 is a prime number
#(console)->|9 equals 3 * 3

for num in range(2, 10):
    if num % 2 == 0:
        print "Found an even number", num
        continue
    print     "Found an odd  number", num

#(console)->|Found an even number 2
#(console)->|Found an odd  number 3
#(console)->|Found an even number 4
#(console)->|Found an odd  number 5
#(console)->|Found an even number 6
#(console)->|Found an odd  number 7
#(console)->|Found an even number 8
#(console)->|Found an odd  number 9

#------------------------------------------#
#%%# if statement
#------------------------------------------#
range(10)
#(console)->|[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
range(5, 10)
#(console)->|[5, 6, 7, 8, 9]
range(0, 10, 3)
#(console)->|[0, 3, 6, 9]
range(-10, -100, -30)
#(console)->|[-10, -40, -70]

a = ['Mary', 'had', 'a', 'little', 'lamb']
for i in range(len(a)):
    print i, a[i]

#(console)->|0 Mary
#(console)->|1 had
#(console)->|2 a
#(console)->|3 little
#(console)->|4 lamb

#------------------------------------------#
# pass statement
#------------------------------------------#
#   while True:
#       pass  # Busy-wait for keyboard interrupt (Ctrl+C)

# This is commonly used for creating minimal classes:
class MyEmptyClass:
    pass

def initlog(*args):
    pass   # Remember to implement this!

#------------------------------------------#
#%%# functions
#------------------------------------------#
def fib(n):    # write Fibonacci series up to n
    """Print a Fibonacci series up to n."""
    a, b = 0, 1
    while a < n:
        print a,
        a, b = b, a+b

# Now call the function we just defined:
fib(2000)
#(console)->|0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597
fib
#(console)->|<function fib at 0x10941ad70>

f = fib
f(100)
#(console)->|0 1 1 2 3 5 8 13 21 34 55 89
print f(100)
#(console)->|0 1 1 2 3 5 8 13 21 34 55 89 None
f(0)
#(console)->|
fib(0)
#(console)->|
print fib(0)
#(console)->|None

def fib2(n): # return Fibonacci series up to n
    """Return a list containing the Fibonacci series up to n."""
    result = []
    a, b = 0, 1
    while a < n:
        result.append(a)    # see below
        a, b = b, a+b
    return result

f100 = fib2(100)    # call it
f100                # write the result
#(console)->|[0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]

#
def sum_two_numbers(a, b):
    return a + b

sum_two_numbers(3,2)
#(console)->|5
#

#------------------------------------------#
#%%# passing arguments
#------------------------------------------#
def ask_ok(prompt, retries=4, complaint='Yes or no, please!', \
           isMain=None):
    if __name__ == '__main__':
        ok = isMain
        if ok in ('y', 'ye', 'yes', 'Y', 'Ye', 'Yes', 'YE', 'YEs', 'YES'):
            return True
        if ok in ('n', 'no', 'nop', 'nope', 'N', 'No', 'nO', 'NO'):
            return False
    else:
        while True:
            ok = raw_input(prompt)
            if ok in ('y', 'ye', 'yes', 'Y', 'Ye', 'Yes', 'YE', 'YEs', 'YES'):
                return True
            if ok in ('n', 'no', 'nop', 'nope', 'N', 'No', 'nO', 'NO'):
                return False
            retries = retries - 1
            if retries < 0:
                raise IOError('refusenik user')
            print complaint

#
# This function can be called in several ways:
#

# giving only the mandatory argument:
if __name__ == '__main__':
    ask_ok('Do you really want to quit? ',isMain='y')
else:
    ask_ok('Do you really want to quit? ')
#(console)->|Do you really want to quit? <y>
#(console)->|True

# giving one of the optional arguments:
if __name__ == '__main__':
    ask_ok('Do you really want to quit? ',2,isMain='n')
else:
    ask_ok('Do you really want to quit? ',2)
#(console)->|OK to overwrite the file? <n>
#(console)->|False
if __name__ == '__main__':
    ask_ok('Do you really want to quit? ',isMain='')
else:
    ask_ok('Do you really want to quit? ')
ask_ok('OK to overwrite the file? ', 2)
#(console)->|OK to overwrite the file? <ok>
#(console)->|Yes or no, please!
#(console)->|OK to overwrite the file? <ok>
#(console)->|Yes or no, please!
#(console)->|OK to overwrite the file? <ok>
#(console)->|Traceback (most recent call last):
#(console)->|  File "<stdin>", line 1, in <module>
#(console)->|  File "<stdin>", line 10, in ask_ok
#(console)->|IOError: refusenik user


# or even giving all arguments:
if __name__ == '__main__':
    ask_ok('Do you really want to quit? ', 2, \
           'Come on, only yes or no! ', isMain='y')
else:
    ask_ok('Do you really want to quit? ')
#(console)->|OK to overwrite the file? <ok>
#(console)->|Come on, only yes or no! y
#(console)->|True

#%%
#The default values are evaluated at the point of function definition in the defining scope
i = 5
def f(arg=i):
    print arg

#
i = 6
f()
#(console)->|5

# ------------- #
def f(a, L=[]):
    L.append(a)
    return L

#
print f(1)
#(console)->|[1]
print f(5)
#(console)->|[1, 5]
print f(3)
#(console)->|[1, 5, 3]

# ------------- #
def f(a, L=None):
    if L is None:
        L = []
    L.append(a)
    return L

#
print f(1)
#(console)->|[1]
print f(5)
#(console)->|[5]

#print f(3,1)
#(console)->|Traceback (most recent call last):
#(console)->|  File "<stdin>", line 1, in <module>
#(console)->|  File "<stdin>", line 4, in f
#(console)->|AttributeError: 'int' object has no attribute 'append'

print f(3,[1])
#(console)->|[1, 3]
L1=f(3)
L1
#(console)->|[3]
L2=f(7,L1)
L2
#(console)->|[3, 7]

# default taken if parameter not present in call

def tryMe(n=3,m=2):
    print "n = ", n, " and m = ", m

tryMe(5,6)
#(console)->|n =  5  and m =  6
tryMe(5)
#(console)->|n =  5  and m =  2
tryMe()
#(console)->|n =  3  and m =  2

#
#------------------------------------------#
#%%# keyword arguments
#------------------------------------------#
def parrot(voltage, state='a stiff', action='voom', type='Norwegian Blue'):
    print "-- This parrot wouldn't", action,
    print "if you put", voltage, "volts through it."
    print "-- Lovely plumage, the", type
    print "-- It's", state, "!"

parrot(1000)                                          # 1 positional argument
#(console)->|-- This parrot wouldn't voom if you put 1000 volts through it.
#(console)->|-- Lovely plumage, the Norwegian Blue
#(console)->|-- It's a stiff !

parrot(voltage=1000)                                  # 1 keyword argument
#(console)->|-- This parrot wouldn't voom if you put 1000 volts through it.
#(console)->|-- Lovely plumage, the Norwegian Blue
#(console)->|-- It's a stiff !

parrot(voltage=1000000, action='VOOOOOM')             # 2 keyword arguments
#(console)->|-- This parrot wouldn't VOOOOOM if you put 1000000 volts through it.
#(console)->|-- Lovely plumage, the Norwegian Blue
#(console)->|-- It's a stiff !

parrot(action='VOOOOOM', voltage=1000000)             # 2 keyword arguments
#(console)->|-- This parrot wouldn't VOOOOOM if you put 1000000 volts through it.
#(console)->|-- Lovely plumage, the Norwegian Blue
#(console)->|-- It's a stiff !

parrot('a million', 'bereft of life', 'jump')         # 3 positional arguments
#(console)->|-- This parrot wouldn't jump if you put a million volts through it.
#(console)->|-- Lovely plumage, the Norwegian Blue
#(console)->|-- It's bereft of life !

parrot('a thousand', state='pushing up the daisies')  # 1 positional, 1 keyword
#(console)->|-- This parrot wouldn't voom if you put a thousand volts through it.
#(console)->|-- Lovely plumage, the Norwegian Blue
#(console)->|-- It's pushing up the daisies !

# Dictionaries can deliver keyword arguments with the **-operator:
d = {"voltage": "four million",
     "state": "bleedin' demised",
     "action": "VOOM"}
parrot(**d)
#(console)->|-- This parrot wouldn't VOOM if you put four million volts through it.
#(console)->|-- Lovely plumage, the Norwegian Blue
#(console)->|-- It's bleedin' demised !

#
#%%# ------------- #
#
def cheeseshop(kind, *arguments, **keywords):
    print "-- Do you have any", kind, "?"
    print "-- I'm sorry, we're all out of", kind
    for arg in arguments:
        print arg
    print "-" * 40
    keys = sorted(keywords.keys())
    for kw in keys:
        print kw, ":", keywords[kw]

cheeseshop("Limburger", "It's very runny, sir.",
           "It's really very, VERY runny, sir.",
           shopkeeper='Michael Palin',
           client="John Cleese",
           sketch="Cheese Shop Sketch")
#(console)->|-- Do you have any Limburger ?
#(console)->|-- I'm sorry, we're all out of Limburger
#(console)->|It's very runny, sir.
#(console)->|It's really very, VERY runny, sir.
#(console)->|----------------------------------------
#(console)->|client : John Cleese
#(console)->|shopkeeper : Michael Palin
#(console)->|sketch : Cheese Shop Sketch

#------------------------------------------#
#%%# Lambda Forms
#------------------------------------------#
# With the lambda keyword, small anonymous functions can be created.
def make_incrementor(n):
    return lambda x: x + n

f = make_incrementor(42)
f(0)
#(console)->|42
f(1)
#(console)->|43
f(5)
#(console)->|47
f(3)
#(console)->|45

#------------------------------------------#
#%%# Data Structures - Lists
#------------------------------------------#
a = [66.25, 333, 333, 1, 1234.5]
print a.count(333), a.count(66.25), a.count('x')
#(console)->|2 1 0

a.insert(2, -1)
a.append(333)
a
#(console)->|[66.25, 333, -1, 333, 1, 1234.5, 333]
a.index(333)
#(console)->|1
a.remove(333)
a
#(console)->|[66.25, -1, 333, 1, 1234.5, 333]
a.reverse()
a
#(console)->|[333, 1234.5, 1, 333, -1, 66.25]
a.sort()
a
#(console)->|[-1, 1, 66.25, 333, 333, 1234.5]

# ------
#%%# Using Lists as Queues
from collections import deque
queue = deque(["Eric", "John", "Michael"])
queue
#(console)->|deque(['Eric', 'John', 'Michael'])
queue.append("Terry")           # Terry arrives
queue.append("Graham")          # Graham arrives
queue.popleft()
#(console)->|'Eric'
queue
#(console)->|deque(['John', 'Michael', 'Terry', 'Graham'])
queue.pop()
#(console)->|'Graham'
queue
#(console)->|deque(['John', 'Michael', 'Terry'])

#------------------------------------------#
#%%# Functional Programming Tools
#------------------------------------------#
def f(x): return x % 2 != 0 and x % 3 != 0

dir()           # shows defined functions
#(console)->|['L1', 'L2', 'MyEmptyClass', 'SPAM', 'STRING', '__builtins__', '__doc__', '__name__', '__package__', 'a', 'ask_ok', 'b', 'cheeseshop', 'counter', 'd', 'deque', 'f', 'f100', 'fib', 'fib2', 'filename', 'hello', 'i', 'initlog', 'make_incrementor', 'miles', 'my_function', 'n', 'name', 'np', 'num', 'os', 'p', 'parrot', 'pl', 'q', 'queue', 'r', 'site', 'sum_two_numbers', 'the_world_is_flat', 'tryMe', 'w', 'word', 'words', 'x', 'y', 'z']


# filter(function, sequence) returns a sequence consisting of those items
# from the sequence for which function(item) is true.
filter(f, range(2, 25))
#(console)->|[5, 7, 11, 13, 17, 19, 23]

def cube(x): return x*x*x

# map(function, sequence) calls function(item) for each of the sequence's
# items and returns a list of the return values.
map(cube, range(1, 11))
#(console)->|[1, 8, 27, 64, 125, 216, 343, 512, 729, 1000]

def add(x,y): return x+y

# reduce(function, sequence) returns a single value constructed by calling
# the binary function function on the first two items of the sequence,
# then on the result and the next item, and so on.
seq = range(8)
map(add, seq, seq)
#(console)->|[0, 2, 4, 6, 8, 10, 12, 14]
reduce(add, range(1, 11))
#(console)->|55

# If there's only one item in the sequence, its value is returned;
# if the sequence is empty, an exception is raised.
# A third argument can be passed to indicate the starting value.
def sum(seq):
    def add(x,y): return x+y
    return reduce(add, seq, 0)

sum(range(1, 11))
#(console)->|55
sum([])
#(console)->|0

# List Comprehensions
squares = []
for x in range(10):
    squares.append(x**2)

squares
#(console)->|[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]

squares = [x**2 for x in range(10)]
squares
#(console)->|[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]

squares = map(lambda x: x**2, range(10))
squares
#(console)->|[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]

[(x, yy) for x in [1,2,3] for yy in [3,1,4] if x != yy]
#(console)->|[(1, 3), (1, 4), (2, 3), (2, 1), (2, 4), (3, 1), (3, 4)]

combs = []
for x in [1,2,3]:
    for yy in [3,1,4]:
        if x != yy:
            combs.append((x, yy))

combs
#(console)->|[(1, 3), (1, 4), (2, 3), (2, 1), (2, 4), (3, 1), (3, 4)]

#%%

vec = [-4, -2, 0, 2, 4]
[x*2 for x in vec]
#(console)->|[-8, -4, 0, 4, 8]

[x for x in vec if x >= 0]
#(console)->|[0, 2, 4]

[abs(x) for x in vec]
#(console)->|[4, 2, 0, 2, 4]

paired = [(x, x**2) for x in vec]
paired
#(console)->|[(-4, 16), (-2, 4), (0, 0), (2, 4), (4, 16)]
paired[1]
#(console)->|(-2, 4)

# matrices
vec2 = [[1,2,3], [4,5,6], [7,8,9]]
[num for elem in vec2 for num in elem]
#(console)->|[1, 2, 3, 4, 5, 6, 7, 8, 9]

matrix = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
]
matrix
#(console)->|[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]

[[row[j] for row in matrix] for j in range(4)]
#(console)->|[[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]]

transposed = []
for i in range(4):
    transposed.append([row[i] for row in matrix])

transposed
#(console)->|[[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]]

zip(*matrix)
#(console)->|[(1, 5, 9), (2, 6, 10), (3, 7, 11), (4, 8, 12)]

del matrix[0]
matrix
#(console)->|[[5, 6, 7, 8], [9, 10, 11, 12]]

#------------------------------------------#
#%%# little math
#------------------------------------------#
from math import pi
[str(round(pi, i)) for i in range(1, 6)]
#(console)->|['3.1', '3.14', '3.142', '3.1416', '3.14159']

# int(), which takes a string like '123' and returns its numeric value 123.
intergerString = '123'
int(intergerString)
#(console)->|123

entero = int('123')
entero
#(console)->|123
entero/2
#(console)->|61

#------------------------------------------#
#%%# Tuples and Sequences
#------------------------------------------#
# A tuple consists of a number of values separated by commas
t = 12345, 54321, 'hello!'
t[2]
#(console)->|'hello!'

x, y, z = t
x
#(console)->|12345
y
#(console)->|54321
z
#(console)->|'hello!'

# Tuples may be nested:
u = t, (1, 2, 3, 4, 5)
u
#(console)->|((12345, 54321, 'hello!'), (1, 2, 3, 4, 5))

# Tuples are immutable: ... t[0] = 88888 ... gives an error!
# but they can contain mutable objects:
v = ([1, 2, 3], [3, 2, 1])
v
#(console)->|([1, 2, 3], [3, 2, 1])
v[1][2]= 7
v
#(console)->|([1, 2, 3], [3, 2, 7])

#------------------------------------------#
#%%# Sets
#------------------------------------------#
basket = ['apple', 'orange', 'apple', 'pear', 'orange', 'banana']
fruit = set(basket)               # create a set without duplicates
fruit
#(console)->|set(['orange', 'pear', 'apple', 'banana'])
'orange' in fruit                 # fast membership testing
#(console)->|True

a = set('abracadabra')
b = set('alacazam')
a
#(console)->|set(['a', 'r', 'b', 'c', 'd'])
b
#(console)->|set(['a', 'c', 'z', 'm', 'l'])
a - b
#(console)->|set(['r', 'b', 'd'])
a & b
#(console)->|set(['a', 'c'])
a ^ b
#(console)->|set(['b', 'd', 'm', 'l', 'r', 'z'])

days = ['Monday', 'Tuesday', 'Wednesday',
        'Thursday', 'Friday']
days[1]
#(console)->|'Tuesday'

#------------------------------------------#
#%%# Dictionaries
#------------------------------------------#
# Dictionaries are sometimes found in other languages as
# "associative memories" or "associative arrays".
# A dictionary is a data type similar to arrays, but works with
# keys and values instead of indexes.
# Dictionaries are indexed by keys, which can be any immutable type.
# Each value stored in a dictionary can be accessed using a key,
# which is any type of object (a string, a number, a list, etc.)
# instead of using its index to address it.
#
tel = {'jack': 4098, 'sape': 4139}
tel['guido'] = 4127
tel
#(console)->|{'sape': 4139, 'jack': 4098, 'guido': 4127}

# The dict() constructor builds dictionaries directly
# from sequences of key-value pairs:
myDict = dict([('sape', 4139), ('guido', 4127), ('jack', 4098)])
myDict
#(console)->|{'sape': 4139, 'jack': 4098, 'guido': 4127}

# When the keys are simple strings, it is sometimes easier to specify
# pairs using keyword arguments:

dict(sape=4139, guido=4127, jack=4098)
#(console)->|{'sape': 4139, 'jack': 4098, 'guido': 4127}

phonebook = {
    "John" : 938477566,
    "Jack" : 938377264,
    "Jill" : 947662781
}
phonebook
#(console)->|{'Jill': 947662781, 'John': 938477566, 'Jack': 938377264}

for name, number in phonebook.iteritems():
    print "Phone number of %s is %d" % (name, number)

#(console)->|{'sape': 4139, 'jack': 4098, 'guido': 4127}

# To loop over two or more sequences at the same time,
# the entries can be paired with the zip() function.
questions = ['name', 'quest', 'favorite color']
answers = ['lancelot', 'the holy grail', 'blue']
for q, a in zip(questions, answers):
    print 'What is your {0}?  It is {1}.'.format(q, a)

#(console)->|Phone number of Jill is 947662781
#(console)->|Phone number of John is 938477566
#(console)->|Phone number of Jack is 938377264
#

tinydict = {'name': 'john','code':6734, 'dept': 'sales'}
print tinydict          # Prints complete dictionary
#(console)->|{'dept': 'sales', 'code': 6734, 'name': 'john'}

print tinydict.keys()   # Prints all the keys
#(console)->|['dept', 'code', 'name']

print tinydict.values() # Prints all the values
#(console)->|['sales', 6734, 'john']

print tinydict['name']  # Prints value for 'name' key
#(console)->|john

print tinydict['one']   # Prints value for 'one' key (doesn't exist)
#(console)->|Traceback (most recent call last):
#(console)->|  File "<stdin>", line 1, in <module>
#(console)->|KeyError: 'one'

print tinydict[2]       # Prints value for 2 key (doesn't exist)
#(console)->|Traceback (most recent call last):
#(console)->|  File "<stdin>", line 1, in <module>
#(console)->|KeyError: 2


#------------------------------------------#
#%%# Input and Output
#------------------------------------------#
# The str() function is meant to return representations of values which are
# fairly human-readable, while repr() is meant to generate representations
# which can be read by the interpreter
s = 'Hello, world.'
str(s)
#(console)->|'Hello, world.'
repr(s)
#(console)->|"'Hello, world.'"


1.0/7.0
#(console)->|0.14285714285714285
n = 1.0/7.0
str(n)
#(console)->|'0.142857142857'
repr(n)
#(console)->|'0.14285714285714285'

x = 10 * 3.25
y = 200 * 200
s = 'The value of x is ' + repr(x) + ', and y is ' + repr(y) + '...'
print s
#(console)->|The value of x is 32.5, and y is 40000...

#%%<------------------------------
for x in range(1,11):
    print x, x*x, x*x*x

#(console)->|1 1 1
#(console)->|2 4 8
#(console)->|3 9 27
#(console)->|4 16 64
#(console)->|5 25 125
#(console)->|6 36 216
#(console)->|7 49 343
#(console)->|8 64 512
#(console)->|9 81 729
#(console)->|10 100 1000

#%%<------------------------------
for x in range(1, 11):
    print repr(x).rjust(2), repr(x*x).rjust(3),
    # Note trailing comma on previous line
    print repr(x*x*x).rjust(4)

#(console)->| 1   1    1
#(console)->| 2   4    8
#(console)->| 3   9   27
#(console)->| 4  16   64
#(console)->| 5  25  125
#(console)->| 6  36  216
#(console)->| 7  49  343
#(console)->| 8  64  512
#(console)->| 9  81  729
#(console)->|10 100 1000

#%%<------------------------------
for x in range(1,11):
    print '{0:2d} {1:3d} {2:4d}'.format(x, x*x, x*x*x)

#(console)->| 1   1    1
#(console)->| 2   4    8
#(console)->| 3   9   27
#(console)->| 4  16   64
#(console)->| 5  25  125
#(console)->| 6  36  216
#(console)->| 7  49  343
#(console)->| 8  64  512
#(console)->| 9  81  729
#(console)->|10 100 1000

#%%# The brackets and characters within them (called format fields)
# are replaced with the objects passed into the str.format() method.
print 'We are the {} who say "{}!"'.format('knights', 'Ni')
#(console)->|We are the knights who say "Ni!"
print '{0} and {1}'.format('spam', 'eggs')
#(console)->|spam and eggs
print '{1} and {0}'.format('spam', 'eggs')
#(console)->|eggs and spam
print '{} and {}'.format('spam', 'eggs')
#(console)->|spam and eggs

print 'This {food} is {adjective}.'.format(
    food='spam', adjective='absolutely horrible')
#(console)->|This spam is absolutely horrible.

import math
print 'The value of PI is approximately {}.'.format(math.pi)
#(console)->|The value of PI is approximately 3.14159265359.
print 'The value of PI is approximately {!r}.'.format(math.pi)
#(console)->|The value of PI is approximately 3.141592653589793.
print 'The value of PI is approximately {0:.3f}.'.format(math.pi)
#(console)->|The value of PI is approximately 3.142.

# old string formating
import math
print 'The value of PI is approximately %5.3f.' % math.pi
#(console)->|The value of PI is approximately   3.142.

# Output Formatting
# The pprint module offers more sophisticated control over printing
# both built-in and user defined objects in a way that is
# readable by the interpreter.
import pprint
t = [[[['black', 'cyan'], 'white', ['green', 'red']], [['magenta',
    'yellow'], 'blue']]]
pprint.pprint(t, width=30)
#(console)->|[[[['black', 'cyan'],
#(console)->|   'white',
#(console)->|   ['green', 'red']],
#(console)->|  [['magenta', 'yellow'],
#(console)->|   'blue']]]


# The textwrap module formats paragraphs of text to fit a given screen width:
import textwrap
doc = """The wrap() method is just like fill() except that it returns
a list of strings instead of one big string with newlines to separate
the wrapped lines."""

print textwrap.fill(doc, width=40)
#-----------|00000000001111111111222222222233333333334444444444
#-----------|01234567890123456789012345678901234567890123456789
#--------------------------------------------------------------
#(console)->|The wrap() method is just like fill()
#(console)->|except that it returns a list of strings
#(console)->|instead of one big string with newlines
#(console)->|to separate the wrapped lines.


#%%# The locale module accesses a database of culture specific data formats.
import locale
loc = locale.getlocale()                # get current locale
#locale.setlocale(locale.LC_ALL, '')
locale.setlocale(locale.LC_ALL, 'en_US.ISO8859-1')
conv = locale.localeconv()              # get a mapping of conventions
#(console)->|'en_US.ISO8859-1'
x = 1234567.8
locale.format("%d", x, grouping=True)
#(console)->|'1,234,567'

#------------------------------------------#
#%%# Reading and Writing Files
#------------------------------------------#
# open() returns a file object, and is most commonly used
# with two arguments: open(filename, mode).
f = open('workfile', 'w')
print f
#(console)->|<open file 'workfile', mode 'w' at 0x109411540>

# The first argument is a string containing the filename.
# The second argument is another string containing a few characters describing
# the way in which the file will be used.
#   'r'  when the file will only be read,
#   'w'  for only writing (an existing file with the same name will be erased), and
#   'a'  opens the file for appending;
#        any data written to the file is automatically added to the end.
#   'r+' opens the file for both reading and writing.
# The mode argument is optional; 'r' will be assumed if it's omitted.

#------------------------------------------#
# Methods of File Objects
#------------------------------------------#
# To read a file's contents, call f.read(size), which reads some quantity of data
# and returns it as a string.
f = open('workfile', 'r')
print f
f.read()
#(console)->|''

f = open('workfile2', 'w')
# f.write(string) writes the contents of string to the file, returning None.
f.write('This is a test\n')
f.write('This is also a test\n')
#

# To write something other than a string,
# it needs to be converted to a string first:
value = ('the answer', 42)
s = str(value)
f.write(s)

# When you're done with a file, call f.close() to close it and free up any
# system resources taken up by the open file.
f.close()

# f.readline() reads a single line from the file; a newline character (\n)
# is left at the end of the string, and is only omitted on the last line
# of the file if the file doesn't end in a newline.
f = open('workfile2', 'r')
f.readline()
#(console)->|'This is a test\n'

for line in f:
    print line,

#(console)->|This is also a test
#(console)->|('the answer', 42)

#%%# f.tell() returns an integer giving the file object's current position
# in the file, measured in bytes from the beginning of the file.
# To change the file object's position, use f.seek(offset, from_what).
f = open('workfile3.txt', 'r+')
f.write('0123456789abcdef')
f.seek(5)     # Go to the 6th byte in the file
f.read(1)
#(console)->|'5'
f.seek(-3, 2) # Go to the 3rd byte before the end
f.read(1)
#(console)->|'2'

#%%# It is good practice to use the with keyword when dealing with file objects.
# This has the advantage that the file is properly closed after its suite
# finishes, even if an exception is raised on the way.
with open('workfile', 'r') as f:
    read_data = f.read()

f.closed

# When you want to save more complex data types like lists, dictionaries, or
# class instances, things get a lot more complicated.

f = open('formattedData.txt','w')
for x in range(1,11):
    f.write(str('{0:2d} {1:3d} {2:4d}'.format(x, x*x, x*x*x))+'\n')

f.close()

#%%# Python provides a standard module called 'pickle'.
# This is an amazing module that can take almost any Python object
# (even some forms of Python code!), and convert it to a string representation;
# this process is called pickling.
# If you have an object x, and a file object f that's been opened for writing,
# the simplest way to pickle the object takes only one line of code:
#   pickle.dump(x, f)
# To unpickle the object again, if f is a file object which has been opened
# for reading:

try:
    f = open('myfile.txt')
    s = f.readline()
    i = int(s.strip())
except IOError as e:
    print "I/O error({0}): {1}".format(e.errno, e.strerror)
except ValueError:
    print "Could not convert data to an integer."
except:
    print "Unexpected error:", sys.exc_info()[0]
    raise

#(console)->|Could not convert data to an integer.

# The try ... except statement has an optional else clause, which,
# when present, must follow all except clauses.
# It is useful for code that must be executed if the try clause does not
# raise an exception. For example:

for arg in sys.argv[1:]:
    try:
        f = open(arg, 'r')
    except IOError:
        print 'cannot open', arg
    else:
        print arg, 'has', len(f.readlines()), 'lines'
        f.close()

#(console)->|Traceback (most recent call last):
#(console)->|  File "<stdin>", line 1, in <module>
#(console)->|NameError: name 'sys' is not defined


#%%# The raise statement allows the programmer to force a specified exception to occur.
try:
    raise Exception('spam', 'eggs')
except Exception as inst:
    print type(inst)     # the exception instance
    print inst.args      # arguments stored in .args
    print inst           # __str__ allows args to printed directly
    x, y = inst.args
    print 'x =', x
    print 'y =', y

#(console)->|<type 'exceptions.Exception'>
#(console)->|('spam', 'eggs')
#(console)->|('spam', 'eggs')
#(console)->|x = spam
#(console)->|y = eggs

#

#------------------------------------------#
#%%# Handling Exceptions
#------------------------------------------#
# There are (at least) two distinguishable kinds of errors:
#   syntax errors and exceptions.
# Syntax errors, also known as parsing errors.
# Even if a statement or expression is syntactically correct,
# it may cause an error when an attempt is made to execute it.
# Errors detected during execution are called exceptions and
# are not unconditionally fatal.
# For example:
#   10 * (1/0)  # ZeroDivisionError: integer division or modulo by zero
#   4 + spam*3  # NameError: name 'spam' is not defined
#   '2' + 2     # TypeError: cannot concatenate 'str' and 'int' objects
# Exceptions come in different types, and
# the type is printed as part of the message:
# the types in the example are ZeroDivisionError, NameError and TypeError.

# It is possible to write programs that handle selected exceptions.
while True:
    try:
        x = int(raw_input("Please enter a number: "))
        print "You entered: ", x
        print "\n Thanks.\n"
        break
    except ValueError:
        print "Oops!  That was no valid number.  Try again..."

#(console)->|Please enter a number: <a>
#(console)->|Oops!  That was no valid number.  Try again...
#(console)->|Please enter a number: <2.3>
#(console)->|Oops!  That was no valid number.  Try again...
#(console)->|Please enter a number: <4>
#(console)->|You entered:  4

# The try statement works as follows.
#  - First, the try clause (the statement(s) between the try and
#    except keywords) is executed.
#  - If no exception occurs, the except clause is skipped and
#    execution of the try statement is finished.
#  - If an exception occurs during execution of the try clause,
#    the rest of the clause is skipped.
#    Then if its type matches the exception named after the
#    except keyword, the except clause is executed, and then
#    execution continues after the try statement.
#  - If an exception occurs which does not match the exception named
#    in the except clause, it is passed on to outer try statements;
#    if no handler is found, it is an unhandled exception and
#    execution stops with a message as shown above.

# A try statement may have more than one except clause,
# to specify handlers for different exceptions.

#------------------------------------------#
#%%# Classes
#------------------------------------------#
# Compared with other programming languages, Python's class mechanism adds
# classes with a minimum of new syntax and semantics.
#
# Objects are an encapsulation of variables and functions into a single entity.
# Objects get their variables and functions from classes.
# Classes are essentially a template to create your objects.

# class ClassName:
#     <statement-1>
#     .
#     .
#     <statement-N>

# A very basic class would look something like this:

class MyClass:
    """A simple example class"""
    variable = "blah"

    def function(self):
        return "This is a message inside the class."

    def __init__():
        self.variable = ""

    def __init__(self, value):
        self.variable = value


myobjectx = MyClass()

# Now the variable "myobjectx" holds an object of the class "MyClass"
# that contains the variable and the function defined within
# the class called "MyClass".
#
# To access the variable inside of the newly created object "MyObject"
# you would do the following:
myobjectx.variable
#(console)->|''
myobjectx.variable = "yackity" # changes variable of objectx
myobjectx.variable
#(console)->|'yackity'

# To access a function inside of an object you use notation similar
# to accessing a variable:
myobjectx.function()
#(console)->|'This is a message inside the class.'

# Of course, a language feature would not be worthy of the name "class"
# without supporting inheritance. The syntax for a derived class
# definition looks like this:

# class DerivedClassName(BaseClassName):
#     <statement-1>
#     .
#     .
#     <statement-N>

# for multiple inheritances:
# class DerivedClassName(Base1, Base2, Base3)

# ----------------- # -------------------- #
#
#%%# trying OOP with Python
#
class pet:
    number_of_legs = 0
    petType = ""
    petName = ""
    def count_legs(self):
        print "I have %s legs" % self.number_of_legs
        return self
    def sleep(self):
        print "zzz"
        return self
    def whatAmI(self):
        print "I am a ", self.petType
    def set_Legs(self, legs):
        self.number_of_legs = legs
        return self

#    def myName(self):

class dog(pet):
    def __init__(self):
        self.number_of_legs = 4
        self.petType = "dog"
#    def __init__(self,petType,petName):
#        self.number_of_legs = 4
#        self.petType = petType
#        self.petName = petName
    def bark(self):
        print "Woof"
#
    def whatAmI(self):
        print "I am a dog"

# -----------------
# - Running ...

doug = pet()
doug.number_of_legs = 4
print "Doug has %s legs." % doug.number_of_legs
#(console)->|Doug has 4 legs.
doug.count_legs()
#(console)->|I have 4 legs
doug.sleep()
#(console)->|zzz

nemo = pet()
nemo.count_legs()
#(console)->|I have 0 legs

tintin = dog()
tintin.whatAmI()
#(console)->|I am a dog
tintin.bark()
#(console)->|Woof

doug.set_Legs(4).count_legs().sleep().sleep().sleep()
#(console)->|I have 4 legs
#(console)->|zzz
#(console)->|zzz
#(console)->|zzz
#(console)->|<__main__.pet instance at 0x104531e60>ount_legs().sleep().sleep().sleep()


#------------------------------------------#
#%%# Iterators
#------------------------------------------#
# The 'for' statement calls iter() on the container object.
# The function returns an iterator object that defines the method next()
# which accesses elements in the container one at a time.
# When there are no more elements, next() raises a StopIteration exception
# which tells the for loop to terminate.
s = 'abc'
it = iter(s)
it
#(console)->|<iterator object at 0x10941ed10>
it.next()
#(console)->|'a'
it.next()
#(console)->|'b'
it.next()
#(console)->|'c'
it.next()
#(console)->|Traceback (most recent call last):
#(console)->|  File "<stdin>", line 1, in <module>
#(console)->|StopIteration

#%%# Having seen the mechanics behind the iterator protocol,
# it is easy to add iterator behavior to your classes.
# Define an __iter__() method which returns an object with a next() method.
# If the class defines next(), then __iter__() can just return self:
class Reverse:
    """Iterator for looping over a sequence backwards."""
    def __init__(self, data):
        self.data = data
        self.index = len(data)
    def __iter__(self):
        return self
    def next(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]

#
rev = Reverse('spam')
iter(rev)
#(console)->|<__main__.Reverse instance at 0x1045243b0>
for char in rev:
    print char

#(console)->|m
#(console)->|a
#(console)->|p
#(console)->|s

# With generators
def reverse(data):
    for index in range(len(data)-1, -1, -1):
        yield data[index]

for char in reverse('golf'):
    print char

#(console)->|f
#(console)->|l
#(console)->|o
#(console)->|g

#

#------------------------------------------#
#%%# Brief Tour of the Standard Library
#------------------------------------------#
# Operating System Interface
import os
os.getcwd()                     # Return the current working directory
#(console)->|'/Users/benito/Development/unix/Python_Development/tutorials_and_tests'
os.chdir('..')
os.getcwd()                     # Return the current working directory
#(console)->|
os.chdir('/')  # Change current working directory
os.getcwd()                     # Return the current working directory
#(console)->|
os.system('mkdir today')        # Run the command mkdir in the system shell
dir(os)                         # Returns a list of all module functions

########################################################################
########################################################################
########################################################################
########################################################################
########################################################################
########################################################################
########################################################################

#%%# For daily file and directory management tasks, the shutil module provides
# a higher level interface that is easier to use:
import shutil
shutil.copyfile('data.db', 'archive.db')
shutil.move('/build/executables', 'installdir')

# Command Line Arguments
# Common utility scripts often need to process command line arguments.
# These arguments are stored in the sys module's argv attribute as a list.
# For instance the following output results from running python demo.py
# one two three at the command line:
#   >>>
#   >>> import sys
#   >>> print sys.argv
#   ['demo.py', 'one', 'two', 'three']

# The re module provides regular expression tools for advanced
# string processing. For complex matching and manipulation,
# regular expressions offer succinct, optimized solutions:
import re
re.findall(r'\bf[a-z]*', 'which foot or hand fell fastest')
#(console)->|['foot', 'fell', 'fastest']

#%%# Mathematics
# The math module gives access to the underlying C library functions
# for floating point math:
import math
math.cos(math.pi / 4.0)
#(console)->|0.7071067811865476
math.log(1024, 2)
#(console)->|10.0

# The random module provides tools for making random selections:
import random
random.choice(['apple', 'pear', 'banana'])
#(console)->|'apple'
random.sample(xrange(100), 10)   # sampling without replacement
#(console)->|[31, 42, 93, 56, 12, 69, 20, 88, 1, 90]
random.random()    # random float
#(console)->|0.17517878738860715
random.randrange(6)
#(console)->|1

# Internet Access
# There are a number of modules for accessing the internet and
# processing internet protocols.
# Two of the simplest are urllib2 for retrieving data from URLs
# and smtplib for sending mail:
import urllib2
for line in urllib2.urlopen('http://tycho.usno.navy.mil/cgi-bin/timer.pl'):
    if 'CST' in line or 'CDT' in line:  # look for Central Standard Time
        print line

#(console)->|<BR>Aug. 12, 10:41:03 PM CDT    Central Time

#%%#
import smtplib
server = smtplib.SMTP('localhost')
server.sendmail('benito@austin.utexas.edu', 'benito.fernandez@gmail.com',
"""To: benito.fernandez@gmail.com
From: benito@austin.utexas.edu

This is a test of mail from Python.
""")
server.quit()

# The datetime module supplies classes for manipulating dates and times
# in both simple and complex ways.

#%%# dates are easily constructed and formatted
from datetime import date
now = date.today()
now
#(console)->|datetime.date(2014, 8, 12)
now.strftime("%Y.%m.%d: %b %d, %Y is a %A on the %dth day of %B.")
#(console)->|'2014.08.12: Aug 12, 2014 is a Tuesday on the 12th day of August.'

# dates support calendar arithmetic
birthday = date(1959, 1, 24)
age = now - birthday
age.days
#(console)->|20289

# Data Compression
# Common data archiving and compression formats are directly
# supported by modules including: zlib, gzip, bz2, zipfile
# and tarfile.
import zlib
s = 'witch which has which witches wrist watch'
len(s)
#(console)->|41
t = zlib.compress(s)
len(t)
#(console)->|37
x_recovered = zlib.decompress(t)
zlib.crc32(s)
#(console)->|226805979

# Performance Measurement
# In contrast to timeit's fine level of granularity,
# the profile and pstats modules provide tools for
# identifying time critical sections in larger blocks of code.

#%%# Quality Control

# One approach for developing high quality software is
# to write tests for each function as it is developed and
# to run those tests frequently during the development process.
def average(values):
    """Computes the arithmetic mean of a list of numbers.

    >>> print average([20, 30, 70])
    40.0
    """
    return sum(values, 0.0) / len(values)

average([1,2,3])
#(console)->|2


import doctest
doctest.testmod()   # automatically validate the embedded tests


# The simplest way to start using doctest
# (but not necessarily the way you'll continue to do it)
# is to end each module M with:

if __name__ == "__main__":
    import doctest
    doctest.testmod()

# doctest then examines docstrings in module M.

# Running the module as a script causes the examples in the docstrings
# to get executed and verified:

#>>> python M.py

# This won't display anything unless an example fails,
# in which case the failing example(s) and the cause(s) of the failure(s)
# are printed to stdout, and
# the final line of output is ***Test Failed*** N failures.

# Run it with the -v switch instead:

#>>> python M.py -v

# and a detailed report of all examples tried is printed
# to standard output, along with assorted summaries at the end.

# Since Python 2.6, there is also a command line shortcut for running testmod().
# You can instruct the Python interpreter to run the doctest module directly
# from the standard library and pass the module name(s) on the command line:

#>>> python -m doctest -v example.py

# Another simple application of doctest is testing interactive examples in
# a text file. This can be done with the testfile() function:

#>>> import doctest
#>>> doctest.testfile("example.txt")

# where, "example.txt" The file content is treated as if it were
# a single giant docstring; the file doesn't need to contain
# a Python program!

#------------------------------------------#
#%%# Templating
#------------------------------------------#
# The string module includes a versatile Template class with a
# simplified syntax suitable for editing by end-users.
# This allows users to customize their applications without
# having to alter the application.
#
# The format uses placeholder names formed by $ with valid Python identifiers
# (alphanumeric characters and underscores).
# Surrounding the placeholder with braces allows it to be followed by more
# alphanumeric letters with no intervening spaces.
# Writing $$ creates a single escaped $:

from string import Template
t = Template('${village}folk send $$10 to $cause.')
t.substitute(village='Nottingham', cause='the ditch fund')
#(console)->|

#------------------------------------------#
#%%# Multi-threading
#------------------------------------------#
# Threading is a technique for decoupling tasks which are not
# sequentially dependent. Threads can be used to improve the responsiveness
# of applications that accept user input while other tasks
# run in the background.
import threading, zipfile

class AsyncZip(threading.Thread):
    def __init__(self, infile, outfile):
        threading.Thread.__init__(self)
        self.infile = infile
        self.outfile = outfile
    def run(self):
        f = zipfile.ZipFile(self.outfile, 'w', zipfile.ZIP_DEFLATED)
        f.write(self.infile)
        f.close()
        print 'Finished background zip of: ', self.infile

background = AsyncZip('mydata.txt', 'myarchive.zip')
background.start()
print 'The main program continues to run in foreground.'
#(console)->|

background.join()    # Wait for the background task to finish
print 'Main program waited until background was done.'
#(console)->|

# The principal challenge of multi-threaded applications is
# coordinating threads that share data or other resources.
# To that end, the threading module provides a number of
# synchronization primitives including
# locks, events, condition variables, and semaphores.

#%%# The array module provides an array() object that is like a list
# that stores only homogeneous data and stores it more compactly.
from array import array
a = array('H', [4000, 10, 700, 22222])
sum(a)
#(console)->|26932
a[1:3]
#(console)->|array('H', [10, 700])
"""
array('H', [10, 700])
"""

#%%# The collections module provides a deque() object that is like a list
# with faster appends and pops from the left side but slower lookups
# in the middle.
from collections import deque
d = deque(["task1", "task2", "task3"])
d.append("task4")
print "Handling", d.popleft()
#(console)->|Handling task1

# The heapq module provides functions for implementing heaps
# based on regular lists. The lowest valued entry is always
# kept at position zero.
from heapq import heapify, heappop, heappush
data = [1, 3, 5, 7, 9, 2, 4, 6, 8, 0]
heapify(data)                      # rearrange the list into heap order
heappush(data, -5)                 # add a new entry
[heappop(data) for j in range(3)]  # fetch the three smallest entries
#(console)->|[-5, 0, 1]

#------------------------------------------#
#%%# Decimal Floating Point Arithmetic
#------------------------------------------#
# The decimal module offers a Decimal datatype for decimal
# floating point arithmetic.
# Compared to the built-in float implementation of binary floating point,
# the class is especially helpful for

#  - financial applications and other uses which require exact
#    decimal representation,
#  - control over precision,
#  - control over rounding to meet legal or regulatory requirements,
#  - tracking of significant decimal places, or
#  - applications where the user expects the results to match calculations
#    done by hand.
from decimal import *
x = Decimal('0.70') * Decimal('1.05')
x
#(console)->|Decimal('0.7350')

x.quantize(Decimal('0.01'))  # round to nearest cent
#(console)->|Decimal('0.74')

getcontext().prec = 36
Decimal(1) / Decimal(7)
#(console)->|Decimal('0.142857142857142857142857142857142857')

#------------------------------------------#
#%%# Pyzo - coherent Python environment for scientific programming.
#------------------------------------------#
# Pyzo we will build a coherent scientific Python environment that
# is easy to use for normal people.
# The Pyzo distro will get you a basic setup suitable for
# scientific programming. But the installation is transparent,
# so additional packages can be installed in the conventional way.
#
import numpy as np
np.__version__
#(console)->|'1.8.1'

import pylab as pl
pl.__version__
#(console)->|

import matplotlib as mp
mp.__version__
#(console)->|'1.3.1'

#------------------------------------------#
# Key Bindings
#------------------------------------------#
# The key bindings and some other parameters of the Readline library
# can be customized by placing commands in an initialization file
# called ~/.inputrc. Key bindings have the form
#   key-name: function-name
# or
#   "string": function-name


#------------------------------------------#
#%%# PGF/TikZ backend
#------------------------------------------#
import matplotlib.ticker as mticker
locator = mticker.MaxNLocator(nbins=5)
print(locator.tick_values(10, 19.5))
#(console)->|[ 10.  12.  14.  16.  18.  20.]

matplotlib.use('pgf')

# Figures can also be directly compiled and saved to PDF with:
plt.savefig('figure.pgf')

# or registering it for handling pdf output
from matplotlib.backends.backend_pgf import FigureCanvasPgf
matplotlib.backend_bases.register_backend('pdf', FigureCanvasPgf)
locator = mticker.MaxNLocator(nbins=10)
print(locator.tick_values(7.1, 19.5))
#(console)->|[  6.    7.5   9.   10.5  12.   13.5  15.   16.5  18.   19.5]

#------------------------------------------#
#%%# iPython
#------------------------------------------#
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import jn

import time
start = time.clock()
print(  '\n\n----->>> Starting Process ... \n'),

x = linspace(0,4*np.pi)
for i in range(6):
    plt.plot(x,jn(i,x))

plt.grid()
plt.show()

stop = time.clock()
print(  '\n\n----->>> ELLAPSED TIME: '),
print stop-start,
print(' <<<-----\n\n\a')

#------------------------------------------#
#%%# ODEint
#------------------------------------------#
import numpy as np
#import pylab as plt
import matplotlib.pyplot as plt
# where in Mac OS instal: /Library/Python/2.7/site-packages/matplotlib-1.4.x-py2.7-macosx-10.8-intel.egg/matplotlib/pyplot.py

from scipy.integrate import odeint

def equation(X, t):
    x, y = X
    Fdist = [-np.sin(2*t)*np.exp(-1.0*t),
             -np.cos(3*t)*np.exp(-0.5*t) ]
    Xdot = Fdist + \
         [ (0.5-2*x)-2*y,
            x-5*y-3*y
         ]
    return Xdot

import time
start = time.clock()
print(  '\n\n----->>> Starting Process ... \n'),
# ETC 2.13 (T-room alcove) Jack's Office Hours @ 10:00-11:00+

init = [ -0.2, 0.5 ]
t = np.linspace(0, 5, 100)
X = odeint(equation, init, t)

stop = time.clock()
print(  '\n\n----->>> ELLAPSED TIME: '),
print stop-start,
print(' <<<-----\n\n\a')

# x = X[:, 0]
# y = X[:, 1]
# plt.plot(t,x, 'r-', lw=1)
# plt.plot(t,y, 'b-', lw=1)

plt.plot(t,X, lw=1)
plt.xlabel('t [s]')
plt.ylabel('x, y [m]')
plt.title('ODEint response ($\mu$)')
plt.grid()
plt.show()

stop = time.clock()
print(  '\n\n----->>> ELLAPSED TIME: '),
print stop-start,
print(' <<<-----\n\n\a')

#%%#--------------<<<<<<<<<<<<<<<<<<
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
X, Y, Z = axes3d.get_test_data(0.05)
cset = ax.contour(X, Y, Z)
ax.clabel(cset, fontsize=9, inline=1)

plt.show()

#%%#--------------<<<<<<<<<<<<<<<<<<
import matplotlib.pyplot as plt
import pylab as pl
# plt.plot(range(5))  # plots in the matplotlib window
# display(gcf())      # embeds the current figure in the qtconsole
# display(*getfigs()) # embeds all active figures in the qtconsole

f = plt.figure()

plt.plot(1+0.1*(pl.rand(100)-0.5),'b.')
plt.title('Random signal')
#pl.display(f)
plt.show()

#------------------------------------------#
#%%# use turtle to draw
#------------------------------------------#
import turtle

turtlename = turtle.Turtle()
def Tsquare(turtlename):
    turtlename.forward(90)
    turtlename.right(90)
    turtlename.forward(90)
    turtlename.right(90)
    turtlename.forward(90)
    turtlename.right(90)
    turtlename.forward(90)

def createSquare(turtlename,direction):
    turtlename.forward(90)
    turtlename.right(90)
    turtlename.forward(90)
    turtlename.right(90)
    turtlename.forward(90)
    turtlename.right(90)
    turtlename.forward(90)

wn = turtle.Screen()
turtlename.right(45)
Tsquare(turtlename)

turtlename.forward(90)
turtlename.left(90)
turtlename.forward(90)
turtlename.left(90)
turtlename.forward(90)
turtlename.left(90)
turtlename.forward(90)

#------------------------------------------#
#%%# undecided
#------------------------------------------#

#------------------------------------------#
#%%# undecided
#------------------------------------------#

#------------------------------------------#
#%%# undecided
#------------------------------------------#

#------------------------------------------#
#%%# undecided
#------------------------------------------#

#
#%%#   End of "pythonDemos.py"
#
