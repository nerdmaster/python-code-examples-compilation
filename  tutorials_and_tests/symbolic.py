from __future__ import division
import sympy as sym
from sympy import *
x, y, z = symbols("x y z")
k, m, n = symbols("k m n", integer=True)
f, g, h = map(Function, 'fgh')

Rational(3,2)*pi + exp(I*x) / (x**2 + y)

exp(I*x).subs(x,pi).evalf()

e = x + 2*y
srepr(e)

exp(pi * sqrt(163)).evalf(50)
exp(pi * sqrt(163)).evalf()

eq = ((x+y)**2 * (x+1))
eq
expand(eq)

a = 1/x + (x*sin(x) - 1)/x
a
simplify(a)


eq = Eq(x**3 + 2*x**2 + 4*x + 8, 0)
eq

solve(eq, x)

a, b = symbols('a b')
Sum(6*n**2 + 2**n, (n, a, b))

