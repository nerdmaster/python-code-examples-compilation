#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# iPython demos
#
#load http://matplotlib.sourceforge.net/plot_directive/mpl_examples/mplot3d/contour3d_demo.py

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
X, Y, Z = axes3d.get_test_data(0.05)
cset = ax.contour(X, Y, Z)
ax.clabel(cset, fontsize=9, inline=1)

plt.show()

#----------------------------------------------------------

StringCommand = "f = [2*x[0] + 4*x[1], 18*x[1] - 18]"

def func(x,StringCommand):
    exec StringCommand
    return f

print StringCommnand

fsolve(func, [1, 1], StringCommand)

#----------------------------------------------------------
