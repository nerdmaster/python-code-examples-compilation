#!/bin/bash

hostname=$1

var="echo $HOSTNAME | sed 's/someregex to fit all$//'"

#first="echo $1 | cut -d '-' -f1"
#two="echo $1 | cut -d '-' -f2"
#three= "echo $1 | cut -d '-' -f3"

alias   one="echo $hostname | awk -F'-' '{print $1}'"
alias   two="echo $hostname | awk -F'-' '{print $2}'"
alias three="echo $hostname | awk -F'-' '{print $3}' | sed 's/\..*//'"

echo "One: [$one]"
echo "Two: [$two]"
echo "Three: [$three]"

alias    maquina="echo $hostname | awk -F'-' '{print $1}'"
alias      group="echo $hostname | awk -F'-' '{print $2}'"
alias   building="echo $hostname | awk -F'-' '{print $3}' | sed 's/\..*//'"

#----------
id
w
who
who -m
whoami

last reboot
uname
uname -a
uname -m

hostname
hostid

du
df

ps

env

resize

cal

date

time ls -l


# Uses of 'cat'
# cat filename                          # Lists the file.
# cat file.1 file.2 file.3 > file.123   # Combines three files into one.

uptime

declare -r PI=3.14159265358979     # Read-only variable, i.e., a constant.

echo

printf "Pi to 2 decimal places = %1.2f" $PI
echo
printf "Pi to 9 decimal places = %1.9f" $PI  # It even rounds off correctly.

printf "\n"                                  # Prints a line feed,
                                             # Equivalent to 'echo' . . .

echo $PATH



