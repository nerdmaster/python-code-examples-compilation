# -*- coding: utf-8 -*-
"""
Created on Thu Oct  9 02:31:58 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                        |
+------------------------------------------------------+
"""
def f(u, t):
    return a*u*(1 - u/R)

a = 2
R = 1E+5
A = 1
