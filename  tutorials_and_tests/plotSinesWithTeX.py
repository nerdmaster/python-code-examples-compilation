#!/usr/bin/python
# plotSinesWithTeX.py
from numpy import *
from pylab import *
from matplotlib import rc, rcParams
# Pyplot is a module within the matplotlib library for plotting
import matplotlib.pyplot as plt

# Make use of TeX﻿
rc('text',usetex=True)

# Change all fonts to 'Computer Modern'
rc('font',**{'family':'serif','serif':['Computer Modern']})

#Notice how I'm calling the functions in the local namespace?
x = linspace(0,2*pi,100)
y, z = sin(x), cos(x)

#Create the plot, noticing how I call the functions directly?
# Create the plot
plt.plot(x,y,label=r'$f(x) = \sin(x)$')
plt.hold(True)
plt.plot(x,z,label=r'$g(x) = \cos(x)$')
plt.title(r'$\int_0^{\infty} t^{x-1} e^{-t} dt$', fontsize=18)
plt.xlabel(r'$\alpha \sim \Gamma \leftarrow (M_{\odot})$',fontsize=17)
plt.ylabel(r'Text in Computer Modern font', fontsize=17)
plt.legend(loc='lower left')
plt.axis([0,4,-1,1])
plt.hold(False)

# Save the figure in a separate file
plt.savefig('sine_function_legend.png')

# Draw the plot to the screen
plt.show()