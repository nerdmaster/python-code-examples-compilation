# scatterplot.py
import numpy as np
import pylab as pl

# Make an array of x values
x = [1, 2, 3, 4, 5]

# Make an array of y values for each x value
y = [1, 4, 9, 16, 25]

# use pylab to plot x and y as red circles
pl.plot(x, y, 'ro')
pl.xlabel('x [m]')
pl.ylabel('y [m/s]')
pl.title('Phase plot')

# pl.xlim(x_low, x_high)
# pl.ylim(y_low, y_high)

# show the plot on the screen
pl.show()