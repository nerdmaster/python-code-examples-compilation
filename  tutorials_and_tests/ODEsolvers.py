#
# User-friendly Python interface to ODE solvers
#
# The ODE function can be written in Python, C, or Fortran.
# The solver directly calls the Fortran/C function,
# eliminating the Python overhead during integration.
# This yields the high performance required for large
# 2D and 3D problems.
#
# Current solvers

# -> LSODE — Livermore Solver for ODEs
# ->    Adams and BDF methods with stiffness detection.
# -> RKC — Runge-Kutta-Chebyshev method
# ->    Stabilized explicit method for use with mildly stiff problems.
# -> Forward Euler
# -> Backward Euler
# -> Trapezoid
# -> Fourth-order Runge-Kutta
# ->    Suite of common integrators, implemented in Python.

from ode.lsode import lsode
import myode
t0 = 0.
tf = 0.7
y0 = myode.initial_condition( t0 )

try:
    solver = lsode( myode.f, t0, y0 )
    solver.reltol = 1e-3
    solver.set_jacobian_bandwidth( 19*19, 19*19 )
    y = solver.integrate( tf )

    print "Steps",   solver.steps
    print "F evals", solver.function_evals

except Exception, e:
    print e
