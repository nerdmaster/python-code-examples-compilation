# -*- coding: utf-8 -*-
"""
Created on Thu Oct  9 02:00:26 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                        |
+------------------------------------------------------+
"""

from numpy import *
import matplotlib.pyplot as plt
from matplotlib.pyplot import *
from scipy import *

plt.ion()

t = linspace(0, 0.1,1000)
w = 60*2*pi

figure(1)
plot(t,cos(w*t-4*pi/3))

figure(2)
plot(t,cos(w*t))
plot(t,cos(w*t-2*pi/3))

print ' ... figures plotted. Now showing'

which = raw_input('which figure to close? [1,2]')
close(int(which))
    #close any previously open plots - this doesn't work when running via Eclipse

plt.show()

which = raw_input('which figure to close? [1,2]')

close(which) 
    #close any previously open plots - this doesn't work when running via Eclipse

