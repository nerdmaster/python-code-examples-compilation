#! /usr/bin/env python
#
#   File: "vode.py"
#------------------------------------------#
#   A Tutorial for the Odespy Interface to ODE Solvers
#
#   (c) 2013 Benito R. Fernandez
#       The University of Texas at Austin
#
# The Odespy package makes it easy to specify an ODE problem in Python
# and get it solved by a wide variety of different numerical methods
# and software.
#

# define state derivatives function
# ---------------------------------
def f(t, x):
    return [x[1], 3.*(1. - x[0]*x[0])*x[1] - x[0]]

# import plotting
# ---------------
import pylab as plt

# initial conditions
# ------------------
x0 = [2.0, 0.0]

# simulation conditions
# ---------------------
tStart  =   0.0
tFinal  =  30.0
nPoints = 150

# run using scipy's vode
#---------------------------------------------------------------
from scipy.integrate import ode
r = ode(f).set_integrator('vode', method='adams',
                          order=10, rol=0, atol=1e-6,
                          with_jacobian=False)
r.set_initial_value(x0, tStart)
dt = tFinal/nPoints
x=[]; t=[]
while r.successful() and r.t <= tFinal:
      r.integrate(r.t + dt)
      x.append(r.y);  t.append(r.t)     # note: prints results

plt.plot(t,x)
plt.grid()
plt.show()

# run using odespy's vode
#---------------------------------------------------------------
import odespy, numpy
for method in odespy.Lsode, odespy.DormandPrince, odespy.Vode:
    solver = method(f, rtol=0.0, atol=1e-6,
                    adams_or_bdf='adams', order=10)
    solver.set_initial_condition(x0)
    t_points = numpy.linspace(tStart, tFinal, nPoints)
    x, t = solver.solve(t_points)

# extract solution components for plotting
x_0 = x[:,0]
x_1 = x[:,1]

from matplotlib.pyplot import plot, show
plot(t, x_0, 'r-', t, x_1, 'b-')
show()

plt.plot(t,x)
plt.grid()
plt.show()

# The solve method in solver classes also allows a second argument,
# "terminate", which is a user-implemented Python function specifying
# when the solution process is to be terminated.
#
# For example, terminating when the solution reaches
# an asymptotic (known) value a::
#
##->...:def terminate(u, t, step_no):
##->...:    # u and t are arrays. Most recent solution is u[step_no].
##->...:    tolerance = 1E-6
##->...:    return abs(u[step_no] - a) < tolerance
##->...:
##->...:u, t = solver.solve(time_points, terminate)
