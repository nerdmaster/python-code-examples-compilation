# Python-Constraint
#
# python-constraint is a Python module offering solvers
# for Constraint Solving Problems (CSPs) over finite domains
# in simple and pure Python.

from constraint import *

problem = Problem()
problem.addVariable("a", [1,2,3])
problem.addVariable("b", [4,5,6])
problem.getSolutions()

problem.addConstraint(lambda a, b: a*2 == b,
                          ("a", "b"))
problem.getSolutions()
##>[{'a': 3, 'b': 6}, {'a': 2, 'b': 4}]

problem2 = Problem()
problem2.addVariables(["a", "b"], [1, 2, 3])
problem2.addConstraint(AllDifferentConstraint())
problem2.getSolutions()
##>[{'a': 3, 'b': 2}, {'a': 3, 'b': 1}, {'a': 2, 'b': 3},
##> {'a': 2, 'b': 1}, {'a': 1, 'b': 2}, {'a': 1, 'b': 3}]

# Rooks
#--------------------------------------------------------
# Here is an example solving the classical rooks problem:

problem = Problem()
numpieces = 8
cols = range(numpieces)
rows = range(numpieces)
problem.addVariables(cols, rows)
for col1 in cols:
    for col2 in cols:
        if col1 < col2:
            problem.addConstraint(lambda row1, row2: row1 != row2,
                                  (col1, col2))
solutions = problem.getSolutions()
