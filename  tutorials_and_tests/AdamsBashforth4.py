def AdamsBashforth4( f, x0, t ):
    """
    Fourth-order Adams-Bashforth method::

    u[n+1] = u[n] + dt/24.*(55.*f(u[n], t[n]) - 59*f(u[n-1], t[n-1]) +
                            37*f(u[n-2], t[n-2]) - 9*f(u[n-3], t[n-3]))

    for constant time step dt.

    RK2 is used as default solver for first steps.
    """
    n = len( t )
    x = numpy.array( [ x0 ] * n )


    for i in xrange( n - 1 ):
        h = t[i+1] - t[i]
        f0 = f( x[i], t[i] )
        k1 = h * f0
        k2 = h * f( x[i] + 0.5 * k1, t[i] + 0.5 * h )
        k3 = h * f( x[i] + 0.5 * k2, t[i] + 0.5 * h )
        k4 = h * f( x[i] + k3, t[i+1] )
        x[i+1] = x[i] + h * ( 55.0 * f0 - 59.0 * k1 + 37.0 * k2 - 9.0 * k3 ) / 24.0

    return x
