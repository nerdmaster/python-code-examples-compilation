QPlainTextEdit, QTextEdit { background-color: black;
                                       color: green ;
                  selection-background-color: #ccc}
.error { background-color: yellow;
                    color: red; }
.in-prompt { background-color: gray;
                        color: yellow; }
.in-prompt-number { background-color: gray;
                    font-weight: bold; }
.out-prompt { background-color: white;
                         color: darkred; }
.out-prompt-number { background-color: white;
                     font-weight: bold; }
