#!/usr/bin/python
""" Simple example of classes"""
class Employee:
   'Common base class for all employees'
   __empCount = 0

   def __init__(self, name, salary):
      self.name = name
      self.salary = salary
      Employee.__empCount += 1

   def displayCount(self):
     print "Total Employees %d" % Employee.__empCount

   def displayEmployee(self):
      print "Name : ", self.name,  ", Salary: $", self.salary



# Instead of using the normal statements to access attributes,
# you can use following functions:
#
# -> The getattr(obj, name[, default]) : to access the attribute of object.
#
# -> The hasattr(obj,name) : to check if an attribute exists or not.
#
# -> The setattr(obj,name,value) : to set an attribute.
#    If attribute does not exist then it would be created.
#
# -> The delattr(obj, name) : to delete an attribute.

#-------------------------------------------------------

# "This would create first object of Employee class"
emp1 = Employee("Zara", 2000)
# "This would create second object of Employee class"
emp2 = Employee("Manni", 5000)
emp1.displayEmployee()
emp2.displayCount()

# print "Total number of Employees %d" % Employee.__empCount
#<-  gives error __empCount is private


# emp1.setattr(emp1, 'age', 8) # Set attribute 'age' at 8
# emp1.hasattr(emp1, 'age')    # Returns true if 'age' attribute exists
# emp1.getattr(emp1, 'age')    # Returns value of 'age' attribute
# emp1.delattr(empl, 'age')    # Delete attribute 'age'


print "Employee.__doc__:", Employee.__doc__
print "Employee.__name__:", Employee.__name__
print "Employee.__module__:", Employee.__module__
print "Employee.__bases__:", Employee.__bases__
print "Employee.__dict__:", Employee.__dict__
