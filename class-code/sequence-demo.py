# -*- coding: utf-8 -*-
"""
Created on Thu Oct  2 01:30:07 2014

@author: benito
"""

class FunctionalList(object):
	'''A class wrapping a list with some extra functional magic, like head,
	tail, init, last, drop, and take.'''
	def __init__(self, values=None):
		if values is None:
			self.values = []
		else:
			self.values = values
	def __len__(self):
		return len(self.values)
	def __getitem__(self, key):
		# if key is of invalid type or value, the list values will raise the error
		return self.values[key]
	def __setitem__(self, key, value):
		self.values[key] = value
	def __delitem__(self, key):
		del self.values[key]
	def __iter__(self):
		return iter(self.values)
	def __reversed__(self):
		return FunctionalList(reversed(self.values))
	def append(self, value):
		self.values.append(value)
		# get last element
		return self.values[-1]


ls = FunctionalList()
ls.append(1)
ls.append(2)
ls.append(3)
print ls[0]
print len(ls)
print 5 in ls
ls[0] = 4
reversed(ls)
