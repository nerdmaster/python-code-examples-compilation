# -*- coding: utf-8 -*-
"""
Created on Thu Sep 25 22:38:10 2014

@author: benito
"""

# https://docs.google.com/document/d/1RxMXsvenznKQb5E-IeFpZxQc0nFgJbIXfelrhEA-y7k/pub

# ---------------------------------------------------- #
# File: triangle_class.py:
# ---------------------------------------------------- #
# Author(s): your_name(s), with BitBucket handle (name)
# [you may add confluence or github link]
# ---------------------------------------------------- #
# Plaftorm:    {Windows | Unix (includes Mac OS)}
# Environment: Python 2.7.x
# Libaries:    numpy 1.9.0
#              matplotlib 1.4.0
#       	 scipy 0.14.0-Py2.7
#              . . .
#       	 sympy 0.7.5
# ---------------------------------------------------- #
# Description:
"""class Trinagle. Has width, height and area."""
# <include name of other modules used/imported>
# <include name of other modules used/imported>
# ---------------------------------------------------- #

# ------------------------
class Triangle:

	def __init__(self, height=0, width=0): # required
		self.height = height
		self.width = width
		self.area = self.getArea()

	def getArea(self): # now class-dependent
		return self.height * self.width / 2

# ------------------------
# END-OF-Class:->Circle<-

# ---------------------------------------------------- #
# END-OF-File:->your_file_name.py<-
# ---------------------------------------------------- #

