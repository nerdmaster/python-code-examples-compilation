# -*- coding: utf-8 -*-
"""
Created on Thu Oct  2 01:34:36 2014

@author: benito
"""

import collections

class ListBasedSet(collections.Set):
    def __init__(self, iterable):
        self.elements = lst = []
        for value in iterable:
            if value not in lst:
                lst.append(value)
    def __iter__(self):
        return iter(self.elements)
    def __contains__(self, value):
        return value in self.elements
    def __len__(self):
        return len(self.elements)


s1 = ListBasedSet('abcdef')
s2 = ListBasedSet('defghi')
overlap = s1 & s2

print s1.elements
#-> ['a', 'b', 'c', 'd', 'e', 'f']

print s2.elements
#-> ['d', 'e', 'f', 'g', 'h', 'i']

print overlap.elements
#-> ['d', 'e', 'f’]
