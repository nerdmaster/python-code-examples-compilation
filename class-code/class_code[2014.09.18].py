# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 15:34:43 2014
Working with numpy arrays:

@author: benito
"""

import numpy as np

def lineBreak():
    print '\n' + 30*'-' + '\n'

lineBreak()#-----------------------------------------------------
#Create a 9x1 vector of numbers 0 to 3, make it into a 3x3 array
a = np.array(range(0,9))
#a = np.arange(9)
print 'Initial array:\n' + \
      '>>> a = np.array(range(0,9)) \n%s' % a

lineBreak()#-----------------------------------------------------
A = a.reshape((3,3))
print 'Reshaped array:\n' + \
      '>>> A = a.reshape((3,3)) \n%s' % A
print A.shape

lineBreak()#-----------------------------------------------------
#rotate top row to bottom
B = np.roll(A,-1,0)
print 'rotate top row to bottom:\n' + \
      '>>> B = np.roll(A,-1,0) \n%s'  \
      % B


lineBreak()#-----------------------------------------------------
#find find entries bigger than three
mask = (A>3)&(A<7)
print 'find find entries bigger than three:\n' + \
      '>>> mask = (A>3)&(A<7) \n%s'  \
      % mask.astype(np.int)

lineBreak()#-----------------------------------------------------
#create a matrix of the first and 3rd rows
C = A[[0,2],:]
print 'create a matrix of the first and 3rd rows:\n' + \
      '>>> C = A[[0,2],:] \n%s'  \
      % C

lineBreak()#-----------------------------------------------------
# matrix multiplication
D = np.dot(C,A)
print 'matrix multiplication:\n' + \
      '>>> D = np.dot(C,A) \n%s'  \
      % D
print '>>> C.dot(A) \n%s' % C.dot(A)

lineBreak()#-----------------------------------------------------
# componentwise multiplication
print 'componentwise multiplication:\n' + \
      'np.multiply(A,mask) \n%s'  \
      % np.multiply(A,mask)
print 'slice:\n' + \
      '>>> D[0:1,1:2] \n%s' % D[0:1,1:3]

lineBreak()#-----------------------------------------------------
# Beginning work with scipy.ndimage:
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob

lineBreak()#-----------------------------------------------------
# Gauss filter on delta fuction
im = np.zeros((10,10))
im[4,6] = 1
blurry_im = ndimage.gaussian_filter(im,2)
print 'Gauss filter on delta fuction:\n' + \
      '>>> im = np.zeros((10,10))\n' + \
      'im[4,6] = 1 \n%s'  \
      % im
print '>>> blurry_im = ndimage.gaussian_filter(im,2) \n%s'  \
      % blurry_im

lineBreak()#-----------------------------------------------------
plt.figure(figsize=(10,5))
plt.subplot(121)
plt.imshow(im, cmap=plt.cm.gray)
plt.axis('off')
plt.subplot(122)
plt.imshow(blurry_im)
plt.axis('off')
plt.show()

lineBreak()#-----------------------------------------------------
# draw a square in the middle of a 256x256 pixel image
sq_im = np.zeros((256,256))
# sq_im[1:4,1:4] = np.ones((3,3))
# sq_im[1:4,1:4] = 1
sq_im[64:-64,64:-64] = 1

plt.figure(figsize=(10,5))
plt.subplot(121)
plt.imshow(sq_im, cmap=plt.cm.gray, interpolation='none')

plt.axis('off')
# plt.subplot(122)
# plt.imshow(blurry_im)
# plt.axis('off')
plt.show()

lineBreak()#-----------------------------------------------------
#rotate--with and without reshape - false.  (check im.shape in both cases)

lineBreak()#-----------------------------------------------------
#blur it a little

lineBreak()#-----------------------------------------------------
#apply sobel filter to images from ndimage to find edges

print '... done!'

