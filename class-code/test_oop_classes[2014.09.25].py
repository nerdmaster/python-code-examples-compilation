# -*- coding: utf-8 -*-
"""
Created on Thu Sep 25 23:01:07 2014

@author: benito
"""
import numpy as math
class MyFuntions:
	def exp(self, value=0.0):
		return 0

print 'starting ...'

myFun = MyFuntions()
print math.exp(1)
#2.71828
print myFun.exp(1)
# 0


class Gaussian:
	def __init__(self,exponent):
		self.exponent = exponent
	def __call__(self,arg):
		return math.exp(-self.exponent*arg*arg)

print 'running Gaussian'

func = Gaussian(0.1)
eval_func = func(3.)
print eval_func, func(3.)
#-> 0.406569659741 0.406569659741

print ' ... done!'

