# -*- coding: utf-8 -*-
#!/usr/bin/python
"""
Created on Fri Sep 12 23:29:17 2014

@author: benito
"""
# import Libraries
import matplotlib.pyplot as plt
import turtle
import time

# global variables
gravity  = 9.81 # [m/s^2],  acceleration of gravity
friction = 0.05 # [1/s]     coefficient of friction
deltaT   = 0.01 # [s],      time step

# model
# --------------------------------------------------------
# initial values
vz = [10] # [m/s],          initial velocity (upward)
pz = [1]  # [m],            initial position (vertical)
tt = [0]  # [s],            time stamp
# --------------------- Euler solver ---------------------
while pz[-1] > -9:
 az = -gravity - friction*vz[-1]
 vz.append( vz[-1] + az * deltaT )
 pz.append( pz[-1] + vz[-1] * deltaT + 0.5 * az * deltaT**2 )
 tt.append( tt[-1] + deltaT )
# --------------------------------------------------------

# data visualization
for i in range(len(pz)):
    print tt[i], pz[i]

#plt.clf()
plt.plot(tt,pz)
plt.xlabel('iteration')
plt.ylabel('[m]')
plt.title('Falling Ball')

plt.show()
"""
time.sleep(10)

# file data management
plt.subplots_adjust(wspace = 0.4)
plt.subplots_adjust(hspace = 0.6)
plt.savefig('figure.png')

# animation
win = turtle.Screen()
win.bgcolor("white")
tess = turtle.Turtle()
tess.shape("turtle")
tess.color("blue")
tess.shapesize(5,5,2)
tess.pencolor("white")
# move turtle to initial position
tess.sety( pz[0] * 50)
tess.penup()
tess.left(90)
ss = tess.stamp()
time.sleep(1)
tess.clearstamp(ss)

# iteration
# exercise: draw each position of the turtle
# according to pz and place a turtle stamp
for entry in pz:
 tess.sety( entry * 50 )
time.sleep(5)
"""