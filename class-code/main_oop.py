#https://docs.google.com/document/d/1RxMXsvenznKQb5E-IeFpZxQc0nFgJbIXfelrhEA-y7k/pub

# ---------------------------------------------------- #
# File: main_oop.py
# ---------------------------------------------------- #
# Author(s): your_name(s), with BitBucket handle (name)
# [you may add confluence or github link]
# ---------------------------------------------------- #
# Plaftorm:    {Windows | Unix (includes Mac OS)}
# Environment: Python 2.7.x
# Libaries:    numpy 1.9.0
#              matplotlib 1.4.0
#       	 scipy 0.14.0-Py2.7
#              . . .
#       	 sympy 0.7.5
# ---------------------------------------------------- #
# Description:
"""<docstring: put here the description of your code>"""
# <include name of other modules used/imported>
# <include name of other modules used/imported>
# ---------------------------------------------------- #

import circle_class as cc
import triangle_class as tc

circ = cc.Circle()
tri1 = tc.Triangle()
tri2 = tc.Triangle()

print circ
print circ.radius
print circ.area
print circ.getArea()

print tri1.height
print tri1.width
print tri1.area
print tri1.getArea()

print tri2.height
print tri2.width
print tri2.area
print tri2.getArea()

tri1.height = 1.0
tri1.width  = 0.5
circ.radius = 2.0

print circ
print circ.radius
print circ.area
print circ.getArea()

print tri1.height
print tri1.width
print tri1.area
print tri1.getArea()

# ---------------------------------------------------- #
# END-OF-File:->your_file_name.py<-
# ---------------------------------------------------- #
