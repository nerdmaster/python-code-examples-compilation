# https://docs.google.com/document/d/1RxMXsvenznKQb5E-IeFpZxQc0nFgJbIXfelrhEA-y7k/pub

# ---------------------------------------------------- #
# File: circle_class.py:
# ---------------------------------------------------- #
# Author(s): your_name(s), with BitBucket handle (name)
# [you may add confluence or github link]
# ---------------------------------------------------- #
# Plaftorm:    {Windows | Unix (includes Mac OS)}
# Environment: Python 2.7.x
# Libaries:    numpy 1.9.0
#              matplotlib 1.4.0
#       	 scipy 0.14.0-Py2.7
#              . . .
#       	 sympy 0.7.5
# ---------------------------------------------------- #
# Description:
"""class Circle. Has radius and area."""
# <include name of other modules used/imported>
# <include name of other modules used/imported>
# ---------------------------------------------------- #

# ------------------------
class Circle:
    pi = 3.1415
    def __init__(self, radius=0.0):
        self.radius = radius
        self.area = self.getArea()

    def getArea(self):
        return self.pi*self.radius**2


# ------------------------
# END-OF-Class:->Circle<-

# ---------------------------------------------------- #
# END-OF-File:->your_file_name.py<-
# ---------------------------------------------------- #

