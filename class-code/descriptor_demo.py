# -*- coding: utf-8 -*-
"""
Created on Wed Oct  1 23:07:37 2014

@author: benito
"""
import numpy

class RevealAccess(object):
    """A data descriptor that sets and returns values
       normally and prints a message logging their access.
    """

    def __init__(self, initval=None, name='var'):
        self.val = initval
        self.name = name

    def __get__(self, obj, objtype):
        print 'Retrieving', self.name
        return self.val

    def __set__(self, obj, val):
        print 'Updating', self.name
        self.val = val

class MyClass(object):
    x = RevealAccess(10, 'var "x"')
    y = 5
    z = RevealAccess(1, 'var "z"')

m = MyClass()

print m

print m.x
#-> Retrieving var "x"
#-> 10

m.x = 20
#-> Updating var "x"

print m.x
#-> Retrieving var "x"
#-> 20

print m.y
#-> 5

print m.z
#-> Retrieving var "z"
#-> 1

m.y = 3

#-> print m.y
#-> 3

m.z = 11
#-> Updating var "z"

print m.z
#-> Retrieving var "z"
#-> 11



class C(object):
    def getx(self):
        print " calling getter..."
        return self.__x
    def setx(self, value):
        print " calling setter... with value=%s" % value
        self.__x = value
    def delx(self):
        print " calling delete..."
        del self.__x
    x = property(getx, setx, delx, \
    "I'm the 'x' property.")

c = C()

print c
#-> <__main__.C object at 0x1006bf7d0>

c.x=3
#-> calling setter...

print c.x
#-> calling getter...
#-> 3

del c
print " ...done!"

