# -*- coding: utf-8 -*-
"""
Created on Thu Oct  2 10:55:05 2014

@author: benito
"""

class Word(str):
	'''Class for words, defining comparison based on word length.'''
	def __new__(cls, word):
		# Note that we have to use __new__.
		# This is because str is an immutable
		# type, so we have to initialize it early (at creation)
		if ' ' in word:
			print "Value contains spaces. Truncating to first space."
			word = word[:word.index(' ')]
			# Word is now all chars before first space
			return str.__new__(cls, word)
	def __gt__(self, other):
		return len(self) > len(other)
	def __lt__(self, other):
		return len(self) < len(other)
	def __ge__(self, other):
		return len(self) >= len(other)
	def __le__(self, other):
		return len(self) <= len(other)
	def __eq__(self, other):
		return super(str, self).__eq__(other)

w1 = Word('This is a long sentence   ')
#-> Value contains spaces. Truncating to first space.
print "w1 is: '%s'" % w1
#-> 'This'
w2 = Word('Averylong word')
#-> Value contains spaces. Truncating to first space.
print "w2 is: '%s'" % w2
#-> 'Averylong’
print 'w1 > w2 is:', w1 > w2
#-> False
print 'w1<w2 is:', w1<w2
#-> True
w3 = Word('This is not the same sentence')
#-> Value contains spaces. Truncating to first space.
print "w3 is: '%s'" % w3
#-> 'This'

