# -*- coding: utf-8 -*-
"""
Created on Tue Sep 16 11:19:15 2014
- Circular Buffer
@author: jose & benito
(c) 2014 The University of Texas at Austin
"""

class RingBuffer:
    def __init__(self, size):
        self.data = [None for i in xrange(size)]

    def append(self, x):
        self.data.pop(0)
        self.data.append(x)

    def get(self):
        return self.data

import time
start = time.clock()
print(  '\n\n----->>> Starting Process ... \n'),

# Test:
buf = RingBuffer(4)
print buf.get()
for i in xrange(10):
    buf.append(i)
    print buf.get()

stop = time.clock()
print(  '\n\n----->>> ELLAPSED TIME: '),
print stop-start,
print(' <<<-----\n\n\a')

