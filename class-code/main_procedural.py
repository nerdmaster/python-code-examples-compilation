# -*- coding: utf-8 -*-
"""
Created on Thu Sep 25 22:50:42 2014

@author: benito
"""

# Example:
# main_procedural.py
height = 0
width = 0
def area():
	return height * width / 2

# main
height = 1
width = 0.5
print 'the area is:', area()
