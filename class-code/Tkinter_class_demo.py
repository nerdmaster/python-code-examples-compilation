# -*- coding: utf-8 -*-
"""
Created on Thu Oct  2 16:19:54 2014

@author: benito
"""

from Tkinter import *

class Application(Frame):
    def say_hi(self):
        print "hi everyone, ME-369P rules!"

    def bye(self):
        print "bye everyone, Thanks!"
        self.quit

    def createWidgets(self):
        self.QUIT = Button(self, text="QUIT",command=  self.bye)
        self.hi_there = Button(self, text="Hello",command=  self.say_hi)
        self.QUIT["fg"]   = "red"

        self.QUIT.pack({"side": "left"})
        self.hi_there.pack({"side": "left"})

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.createWidgets()

root = Tk()
app = Application(master=root)
app.mainloop()
root.destroy()