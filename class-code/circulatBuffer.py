# -*- coding: utf-8 -*-
"""
Created on Tue Sep 16 11:19:15 2014
- Circular Buffer
@author: benito
"""


import collections as coll
import time

start = time.clock()
print(  '\n\n----->>> Starting Process ... '),

d = coll.deque(maxlen=10)
d

deque([], maxlen=10)
for i in xrange(20):
    d.append(i)

d

deque([10, 11, 12, 13, 14, 15, 16, 17, 18, 19], maxlen=10)

d

def add_to_buffer( self, num ):
    self.mylist.pop( 0 )
    self.mylist.append( num )

stop = time.clock()
print(  '\n\n----->>> ELLAPSED TIME: '),
print stop-start,
print(' <<<-----\n\n\a')


