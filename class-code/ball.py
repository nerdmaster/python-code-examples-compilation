# -*- coding: utf-8 -*-
"""
Created on Fri Sep 12 19:44:53 2014

@author: benito
"""

import matplotlib.pyplot as plt
# import turtle
# import time
# define global variables
gg=9.81 # [m/s^2] gravity
tt=0.01 # [s]     time interval seconds
#model
ddz = -gg #z double dot
#solver 1 time
dz = [10] # [m/s] initial velocity
zz = [1]  # [m]   initial position
while zz[-1] > -9:
    dz.append( dz[-1] + ddz * tt)
    zz.append( zz[-1] + dz[-1] * tt + 0.5 * ddz * tt**2)
#Data Visualization
plt.plot(zz)
plt.xlabel('iteration')
plt.ylabel('[m]')
plt.title('Falling Ball')

# file data management
plt.subplots_adjust(wspace = 0.4)
plt.subplots_adjust(hspace = 0.4)
plt.savefig('ball_figure.png')