#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 29 22:48:14 2014

@author: benito

from: ZetCode Tkinter tutorial

This script shows a simple window
on the screen.

This program creates a quit
button. When we press the button,
the application terminates.

author: Jan Bodnar
last modified: December 2010
website: www.zetcode.com

author: Jan Bodnar
last modified: January 2011
website: www.zetcode.com
"""

from Tkinter import Tk, Frame, BOTH

class Example(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent, background="white")

        self.parent = parent
        self.parent.title("Centered window")
        self.pack(fill=BOTH, expand=1)
        self.centerWindow()

        self.initUI()

    def initUI(self):
        self.parent.title("Simple with Quit button")
        self.style = Style()
        self.style.theme_use("default")

        self.pack(fill=BOTH, expand=1)

        quitButton = Button(self, text="Quit",
            command=self.quit)
        quitButton.place(x=50, y=50)

    def centerWindow(self):

        w = 290
        h = 150

        sw = self.parent.winfo_screenwidth()
        sh = self.parent.winfo_screenheight()

        x = (sw - w)/2
        y = (sh - h)/2
        self.parent.geometry('%dx%d+%d+%d' % (w, h, x, y))


def main():

    root = Tk()
    root.geometry("250x150+300+300")
    app = Example(root)
    root.mainloop()

    raiz = Tk()
    ex = Example(raiz)
    root.mainloop()

if __name__ == '__main__':
    main()



