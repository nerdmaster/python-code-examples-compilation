% Example diagrams from the tutorial: Putting a Diagram in Chains
% Source: The TikZ & PGF manual
\documentclass{article}

\usepackage{tikz}

\usetikzlibrary{%
  arrows,%
  shapes.misc,% wg. rounded rectangle
  shapes.arrows,%
  chains,%
  matrix,%
  positioning,% wg. " of "
  scopes,%
  decorations.pathmorphing,% /pgf/decoration/random steps | erste Graphik
  shadows%
}
\usepackage{verbatim}

\begin{comment}
:Title: Putting a diagrams in chains
:Slug: diagram-chains
:Tags: Diagrams, Chains, PGF 2.0, Manual
:Grid: 1x2

Diagrams from the *Putting a diagrams in chains* tutorial.

:Source: The TikZ & PGF manual

Thanks to `Dr. Ludger Humbert`_ for submitting a standalone version of the diagrams.

.. _Dr. Ludger Humbert: https://haspe.homeip.net/cgi-bin/pyblosxom.cgi

\end{comment}

\usepackage{xxcolor}

\usetikzlibrary{chains,matrix,scopes,decorations.shapes,arrows,shapes}

% for Railroad-Diagram
\tikzset{
  nonterminal/.style={
    % The shape:
    rectangle,
    % The size:
    minimum size=6mm,
    % The border:
    very thick,
    draw=red!50!black!50,         % 50% red and 50% black,
                                  % and that mixed with 50% white
    % The filling:
    top color=white,              % a shading that is white at the top...
    bottom color=red!50!black!20, % and something else at the bottom
    % Font
    font=\itshape
  },
  terminal/.style={
    % The shape:
    rounded rectangle,
    minimum size=6mm,
    % The rest
    very thick,draw=black!50,
    top color=white,bottom color=black!20,
    font=\ttfamily},
  skip loop/.style={to path={-- ++(0,#1) -| (\tikztotarget)}}
}
{
  \tikzset{terminal/.append style={text height=1.5ex,text depth=.25ex}}
  \tikzset{nonterminal/.append style={text height=1.5ex,text depth=.25ex}}
}

\renewcommand\familydefault{\sfdefault}

\listfiles

\title{Lexical Diagrams}
\author{Benito R. Fernandez}

\begin{document}

\maketitle

\tikzset{
  nonterminal/.style={
    % The shape:
    rectangle,
    % The size:
    minimum size=6mm,
    % The border:
    very thick,
    draw=red!50!black!50,         % 50% red and 50% black,
                                  % and that mixed with 50% white
    % The filling:
    top color=white,              % a shading that is white at the top...
    bottom color=red!50!black!20, % and something else at the bottom
    % Font
    font=\itshape
  },
  terminal/.style={
    % The shape:
    rounded rectangle,
    minimum size=6mm,
    % The rest
    very thick,draw=black!50,
    top color=white,bottom color=black!20,
    font=\ttfamily},
  skip loop/.style={to path={-- ++(0,#1) -| (\tikztotarget)}}
}

{
  \tikzset{terminal/.append style={text height=1.5ex,text depth=.25ex}}
  \tikzset{nonterminal/.append style={text height=1.5ex,text depth=.25ex}}
}

 \section{Lexical syntax}
 The syntax of the language exists at two levels. The bottom level is lexical syntax. This syntax is defined at character level. In particular, white-space is not allowed between the boxes. The upper level is the context-free syntax level. At this level, white space constructs may be added (or in some cases must be added) between boxes in the diagram, such as space or new-line characters, or comments.
 
 The input to the tools is the ASCII character set. That is also the format in which the input has to be delivered to the tools. To prevent misunderstanding about the characters allowed in this chapter, references are made to Unicode characters. Such a reference starts with an uppercase `U' followed by a four digit hexadecimal Unicode1 character identification, for example U005C.
 
 \subsection{Lexical tokens}
 The syntax and the grammar presented here are explained using syntax diagrams, also known as rail(-road) diagrams. The rule in the diagram is read by starting at the left, and following the line without making sharp turns until it ends at the right hand side. 
 Diagrams can be nested. When the contents of another diagram should be used, the name of the needed diagram is shown in a rectangular box. Choice between two or more alternatives is also possible in a diagram. In a diagram with choices, the first alternative (at Track 1) is sometimes empty, which indicates an optional rule. Repetition looks similar to choice. The difference is that the circle segments at the top are in the opposite direction compared to the previous diagram. 
 
 \subsubsection{Unsigned decimal numbers}
 Unsigned decimal numbers are a sequence of one or more decimal digits (U0030 upto and including U0039) as shown in the lexical Number diagram in Figure 2.1. The node 0 | 1 | . . . | 9 means selection of one of the characters `0' or `1' or . . . or `9' (one of the U0030 upto and including U0039). The node 1 | 2 | . . . | 9 has a similar meaning, except that the `0' (U0030) may not be chosen. As you can see in the diagram, value 0 is entered as the single character `0'. All other
(unsigned) values start with a
 
\begin{center}
 \begin{tikzpicture}[
    >=latex,thick,
    /pgf/every decoration/.style={/tikz/sharp corners},
    fuzzy/.style={decorate,
        decoration={random steps,segment length=0.5mm,amplitude=0.15pt}},
    minimum size=6mm,line join=round,line cap=round,
    terminal/.style={rectangle,draw,fill=white,fuzzy,rounded corners=3mm},
    nonterminal/.style={rectangle,draw,fill=white,fuzzy},
    node distance=4mm
  ]

    \ttfamily
    \begin{scope}[start chain,
            every node/.style={on chain},
            terminal/.append style={join=by {->,shorten >=-1pt,
                fuzzy,decoration={post length=4pt}}},
            nonterminal/.append style={join=by {->,shorten >=-1pt,
                fuzzy,decoration={post length=4pt}}},
            support/.style={coordinate,join=by fuzzy}
        ]
        \node [support]             (start)        {};
        \node [nonterminal]                        {unsigned integer};
        \node [support]             (after ui)     {};
        \node [terminal]                           {.};
        \node [support]             (after dot)    {};
        \node [terminal]                           {digit};
        \node [support]             (after digit)  {};
        \node [support]             (skip)         {};
        \node [support]             (before E)     {};
        \node [terminal]                           {E};
        \node [support]             (after E)      {};
        \node [support,xshift=5mm]  (between)      {};
        \node [support,xshift=5mm]  (before last)  {};
        \node [nonterminal]                        {unsigned integer};
        \node [support]             (after last)   {};
        \node [coordinate,join=by ->] (end)        {};
    \end{scope}
    \node (plus)  [terminal,above=of between] {+};
    \node (minus) [terminal,below=of between] {-};

    \begin{scope}[->,decoration={post length=4pt},rounded corners=2mm,
            every path/.style=fuzzy]
        \draw (after ui)    -- +(0,.7)  -| (skip);
        \draw (after digit) -- +(0,-.7) -| (after dot);
        \draw (before E)    -- +(0,-1.2) -| (after last);
        \draw (after E)     |- (plus);
        \draw (plus)        -| (before last);
        \draw (after E)     |- (minus);
        \draw (minus)       -| (before last);
    \end{scope}
 \end{tikzpicture}
\end{center}


\begin{center}
 \begin{tikzpicture}[
        point/.style={coordinate},>=stealth',thick,draw=black!50,
        tip/.style={->,shorten >=0.007pt},every join/.style={rounded corners},
        hv path/.style={to path={-| (\tikztotarget)}},
        vh path/.style={to path={|- (\tikztotarget)}},
        text height=1.0ex,text depth=.25ex % align text horizontally
    ]
    \matrix[column sep=3mm] {
        % First row:
        & & & & & & &  & & & & \node (plus) [terminal] {+};\\
        % Second row:
        \node (p1) [point] {}; & \node (ui1) [nonterminal] {unsigned integer};&
        \node (p2) [point] {}; & \node (dot) [terminal]    {.};               &
        \node (p3) [point] {}; & \node (digit) [terminal]    {digit};         &
        \node (p4) [point] {}; & \node (p5)  [point]  {};                     &
        \node (p6) [point] {}; & \node (e)   [terminal]    {E};               &
        \node (p7) [point] {}; &                                              &
        \node (p8) [point] {}; & \node (ui2) [nonterminal] {unsigned integer};&
        \node (p9) [point] {}; & \node (p10) [point]       {};\\
        % Third row:
        & & & & & & &  & & & & \node (minus)[terminal] {-};\\
    };

    { [start chain]
        \chainin (p1);
        \chainin (ui1)   [join=by tip];
        \chainin (p2)    [join];
        \chainin (dot)   [join=by tip];
        \chainin (p3)    [join];
        \chainin (digit) [join=by tip];
        \chainin (p4)    [join];
        { [start branch=digit loop]
        \chainin (p3) [join=by {skip loop=-6mm,tip}];
        }
        \chainin (p5)    [join,join=with p2 by {skip loop=6mm,tip}];
        \chainin (p6)    [join];
        \chainin (e)     [join=by tip];
        \chainin (p7)    [join];
        { [start branch=plus]
        \chainin (plus)  [join=by {vh path,tip}];
        \chainin (p8)    [join=by {hv path,tip}];
        }
        { [start branch=minus]
        \chainin (minus) [join=by {vh path,tip}];
        \chainin (p8)    [join=by {hv path,tip}];
        }
        \chainin (p8)    [join];
        \chainin (ui2)   [join=by tip];
        \chainin (p9)    [join,join=with p6 by {skip loop=-11mm,tip}];
        \chainin (p10)   [join=by tip];
  }
 \end{tikzpicture}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% for Railroad-Diagram
\tikzset{
  nonterminal/.style={
    % The shape:
    rectangle,
    % The size:
    minimum size=6mm,
    % The border:
    very thick,
    draw=red!50!black!50,         % 50% red and 50% black,
                                  % and that mixed with 50% white
    % The filling:
    top color=white,              % a shading that is white at the top...
    bottom color=red!50!black!20, % and something else at the bottom
    % Font
    font=\itshape
  },
  terminal/.style={
    % The shape:
    rounded rectangle,
    minimum size=6mm,
    % The rest
    very thick,draw=black!50,
    top color=white,bottom color=black!20,
    font=\ttfamily},
  skip loop/.style={to path={-- ++(0,#1) -| (\tikztotarget)}}
}
{
  \tikzset{terminal/.append style={text height=1.5ex,text depth=.25ex}}
  \tikzset{nonterminal/.append style={text height=1.5ex,text depth=.25ex}}
}

%% if_stmt ::= "if" expression ":" suite
%%                ( "elif" expression ":" suite )*
%%                ["else" ":" suite]
%%
\hspace{-35ex}
\begin{tikzpicture}[point/.style={coordinate},>=stealth',thick,draw=black!50,
                    tip/.style={->,shorten >=0.007pt},every join/.style={rounded corners},
                    hv path/.style={to path={-| (\tikztotarget)}},
                    vh path/.style={to path={|- (\tikztotarget)}},
                    text height=1.5ex,text depth=.25ex]     % um die Hoehe des Punktes festzuzurren
  \matrix[ampersand replacement=\&,column sep=4mm] {
    \node (p1)  [point]  {}; \&    \node (ifs)    [terminal]     {if};             \&
    \node (p2)  [point]  {}; \&    \node (expr1)  [nonterminal]  {expr};           \&
    \node (p3)  [point]  {}; \&    \node (colon1) [terminal]     {:};              \&
    \node (p4)  [point]  {}; \&    \node (suite1) [nonterminal]  {suite};          \&
    \node (p5)  [point]  {}; \&    \node (p6)     [point]  {};                     \&
    \node (p6a) [point]  {}; \&    \node (p6b)    [point]  {};                     \&
    \node (p7)  [point]  {}; \&    \node (elif)   [terminal]     {elif};           \&
    \node (p8)  [point]  {}; \&    \node (expr2)  [nonterminal]  {expr};           \&
    \node (p9)  [point]  {}; \&    \node (colon2) [terminal]     {:};              \&
    \node (p10) [point]  {}; \&    \node (suite2) [nonterminal]  {suite};          \&
    \node (p11) [point]  {}; \&    \node (p12)    [point]  {};                     \&
    \node (p12a)[point]  {}; \&    \node (p12b)   [point]  {};                     \&
    \node (p13) [point]  {}; \&    \node (else)   [terminal]     {else};           \&
    \node (p14) [point]  {}; \&    \node (colon3) [terminal]     {:};              \&
    \node (p15) [point]  {}; \&    \node (suite3) [nonterminal]  {suite};          \&
    \node (p16) [point]  {}; \&    \node (p17)    [point]       {};\\
  };

  { [start chain]
    \chainin (p1);
    \chainin (ifs)    [join=by tip];
    \chainin (p2)     [join];
    \chainin (expr1)  [join=by tip];
    \chainin (p3)     [join];
    \chainin (colon1) [join=by tip];
    \chainin (p4)     [join];
    \chainin (suite1) [join=by tip];
    \chainin (p7)     [join];
    \chainin (elif)   [join=by tip];
    \chainin (p8)     [join];
    \chainin (expr2)  [join=by tip];
    \chainin (p9)     [join];
    \chainin (colon2) [join=by tip];
    \chainin (p10)    [join];
    \chainin (suite2) [join=by tip];
    \chainin (p12a)   [join,join=with p6 by {skip loop=-11mm,tip}];
    \chainin (p13)    [join];
    \chainin (else)   [join=by tip];
    \chainin (p14)    [join];
    \chainin (colon3) [join=by tip];
    \chainin (p15)    [join];
    \chainin (suite3) [join=by tip];
    \chainin (p16)    [join];
    \chainin (p17)    [join=by tip];
    \chainin (p16)    [join,join=with p12b by {skip loop=-11mm,tip}];
    \chainin (p6b)    [join=with p11 by {skip loop=11mm,tip}];
  }
\end{tikzpicture}

\end{document}

