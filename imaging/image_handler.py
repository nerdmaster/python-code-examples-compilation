# -*- coding: utf-8 -*-
"""
Created on Fri Sep 12 21:49:54 2014

@author: joser
"""

from scipy import misc
from scipy import ndimage
import matplotlib.pyplot as plt
import numpy as np


image = misc.imread('texas.png', True)
lx, ly = image.shape

#Geometrical transformations:
# Cropping
crop_image = image[lx / 4: - lx / 4, ly / 4: - ly / 4]

# Upside down flip
flip_ud_image = np.flipud(image)

# Rotation
rotate_image = ndimage.rotate(image, 45)

# Rotation no reshape
rotate_image_noreshape = ndimage.rotate(image, 45, reshape=False)

print "figure 1: Basic Geometric trasnformation"

plt.figure(figsize=(12.5, 2.5))
plt.subplot(151)
plt.imshow(image, cmap=plt.cm.gray)
plt.axis('off')
plt.subplot(152)
plt.imshow(crop_image, cmap=plt.cm.gray)
plt.axis('off')
plt.subplot(153)
plt.imshow(flip_ud_image, cmap=plt.cm.gray)
plt.axis('off')
plt.subplot(154)
plt.imshow(rotate_image, cmap=plt.cm.gray)
plt.axis('off')
plt.subplot(155)
plt.imshow(rotate_image_noreshape, cmap=plt.cm.gray)
plt.axis('off')

plt.subplots_adjust(wspace=0.02, hspace=0.3, top=1, bottom=0.1, left=0, right=1)
plt.show()


# Edge detection
print "figure 2: Pattern matching"
plt.figure(figsize=(10, 10))

im = np.zeros((256, 256))
im[64:-64, 64] = 1
im[64:-64, -64] = 1
im[120, 30:-30] = 1
im[30, 30:-30] = 1
im[20:-100, -104] = 1

im_match = 0.075*im
#im = ndimage.rotate(im, 15, mode='constant')
#im = ndimage.gaussian_filter(im, 8)

plt.subplot(1,3,1)
plt.imshow(im, cmap=plt.cm.gray)
plt.axis('off')

# Image size
x, y = im.shape

pattern = np.zeros((10,10))
pattern[0:10, 5] = 1
pattern[5, 0:10] = 1
px, py = pattern.shape

plt.subplot(1,3,2)
plt.imshow(pattern, cmap=plt.cm.gray)
plt.axis('off')

num_matches = 0
for i in range(0,(y-py)):
    for j in range(0,(x-px)):
        im_test = im[i:(i+px), j:(j+py)]
        if(np.array_equal(pattern, im_test)):
            print "There's a match at", i,j
            #im_match[i:(i+50),j:(j+50)]=pattern
            im_match[i:(i+px), j:(j+py)] = pattern 
            num_matches += 1

if(num_matches==0):
    print "There's no matches"
else:
    print num_matches, "match(es)"
        
plt.subplot(1,3,3)
plt.imshow(im_match, cmap=plt.cm.gray)
plt.axis('off')
plt.show()
