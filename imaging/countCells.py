# -*- coding: utf-8 -*-
"""
Created on Mon Sep 15 01:00:19 2014
Our first task will be to take this image and count the number of nuclei
@author: benito
"""

import numpy as np
import scipy
import pylab
import pymorph
import mahotas
from scipy import ndimage

# Before we start, let us import the needed files
dna = mahotas.imread('dna.jpeg')

# Instead of the default jet colourmap, we can set it to the gray one

# Playing Around

# In interactive mode, you can see the image:
pylab.imshow(dna)
pylab.gray()
pylab.show()

# We can explore our array a bit more:
print dna.shape
print dna.dtype
print dna.max()
print dna.min()

pylab.imshow(dna // 2)
pylab.show()

# Some Actual Work

# Here's the first idea for counting the nuclei. We are going to threshold the image and count the number of objects.

T = mahotas.thresholding.otsu(dna)
pylab.imshow(dna > T)
pylab.show()

# This isn't too good. The image contains many small objects. There are a couple of ways to solve this. A simple one is to smooth the image a bit using a Gaussian filter.

dnaf = ndimage.gaussian_filter(dna, 8)
T = mahotas.thresholding.otsu(dnaf)
pylab.imshow(dnaf > T)
pylab.show()

# We now have some merged nuclei (those that are touching), but overall the result looks much better. The final count is only one extra function call away:

labeled,nr_objects = ndimage.label(dnaf > T)
print nr_objects
pylab.imshow(labeled)
pylab.jet()
pylab.show()

## Second Task: Segmenting the Image
# The previous result was acceptable for a first pass, but there were still nuclei glued together. Let's try to do better.
#
# Here is a simple, traditional, idea:
#
# 1) Smooth the image
# 2) Find regional maxima
# 3) Use the regional maxima as seeds for watershed


# Finding the seeds

# Here's our first try:

dnaf = ndimage.gaussian_filter(dna, 8)
rmax = pymorph.regmax(dnaf)
pylab.imshow(pymorph.overlay(dna, rmax))
pylab.show()

# After a little fiddling around, we decide to try the same idea with a bigger sigma value:

dnaf = ndimage.gaussian_filter(dna, 16)
rmax = pymorph.regmax(dnaf)
pylab.imshow(pymorph.overlay(dna, rmax))


# We can easily count the number of nuclei now:

seeds,nr_nuclei = ndimage.label(rmax)
print nr_nuclei

# Watershed

# We are going to apply watershed to the distance transform of the thresholded image:

T = mahotas.thresholding.otsu(dnaf)
dist = ndimage.distance_transform_edt(dnaf > T)
dist = dist.max() - dist
dist -= dist.min()
dist = dist/float(dist.ptp()) * 255
dist = dist.astype(np.uint8)
pylab.imshow(dist)
pylab.show()


# After we contrast stretched the dist image, we can call pymorph.cwatershed to get the final result [5] (the colours in the image come from it being displayed using the jet colourmap):

nuclei = pymorph.cwatershed(dist, seeds)
pylab.imshow(nuclei)
pylab.show()

# It's easy to extend this segmentation to the whole plane by using generalised Voronoi (i.e., each pixel gets assigned to its nearest nucleus):

whole = mahotas.segmentation.gvoronoi(nuclei)
pylab.imshow(whole)
pylab.show()


# Often, we want to provide a little quality control and remove those cells whose nucleus touches the border. So, let's do that:

borders = np.zeros(nuclei.shape, np.bool)
borders[ 0,:] = 1
borders[-1,:] = 1
borders[:, 0] = 1
borders[:,-1] = 1
at_border = np.unique(nuclei[borders])
for obj in at_border:
    whole[whole == obj] = 0
pylab.imshow(whole)
pylab.show()





