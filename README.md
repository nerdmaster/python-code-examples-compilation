# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository contains code to support the course:
ME-369P: Application Programming for Engineers
The University of Texas at Austin
Mechanical Engineering Department

* Quick summary
The folder contains code for different aspects of Python Programming
as applied to Engineering systems.

Code has been copied from pther sources and tutorials as well as
code developed for the course by the Faculty, Teaching Assistants,
other Graduate Students, and the Students taking the course.

* Version
0.1.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
[Jack Hall](jackhall@utexas.edu),
[Jose Capriles](jrcapriles@gmail.com),
[Gray Thomas](gray.c.thomas@gmail.com), or
[Carlos Garcia](garciacarlos@gmail.com)

* Repo owner or admin
The course is taught by Professors:
[Luis Sentis](lsentis@gmail.com) 
and 
[Benito Fernandez](benito.fernandez@gmail.com)

Teaching Associates/Assistants and Contributors include:
[Gwendolyn Johnson](gwendolynbrook@gmail.com),
[Chien-Liang Fok](liangfok@utexas.edu),
[Jack Hall](jackhall@utexas.edu),
[Jose Capriles](jrcapriles@gmail.com),
[Gray Thomas](gray.c.thomas@gmail.com),
[Carlos Garcia](garciacarlos@gmail.com),
[Kwan Suk Kim](linupa@gmail.com),
[Ye Zhao](yezhao@utexas.edu), and
[Donghyun Kim](dk6587@utexas.edu) (김동현) .

* Other community or team contact
If you would like to contribute, let me know at 
[Benito Fernandez](benito.fernandez@gmail.com), 
[Luis Sentis](lsentis@gmail.com), 
[Jack Hall](jackhall@utexas.edu), 
[Jose Capriles](jrcapriles@gmail.c om), 
[Gray Thomas](gray.c.thomas@gmail.com), 
[Carlos Garcia](garciacarlos@gmail.com), 
or contact anyone in the admin group.

Thanks