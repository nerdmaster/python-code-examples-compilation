# -*- coding: utf-8 -*-
"""
Created on Thu Sep 25 22:56:49 2014

@author: benito
"""

# File: test_circular_buffer_class.py
import circular_buffer_class as cbc

buffer = cbc.CircularBuffer()

for index in range(44):
    buffer.add(index)
    buffer.printCircularBuffer()

buffer.printCircularBuffer()
