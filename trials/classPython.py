# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""

from collections import namedtuple
import numpy as np

class Point(object):
    def __init__(self, X =0, Y =0):
        self.x = X
        self.y = Y
    def __repr__(self):
        return 'Point(%s,%s)' % (self.x,self.y)
    def __str__(self):
        return 'Point(%s,%s)' % (self.x,self.y)
    def __sub__(self,other):
        return Point(self.x - other.x, self.y - other.y)
    def __add__(self,other):
        return Point(self.x + other.x, self.y + other.y)
               
        
tPoint = namedtuple('Point', 'x, y')    
    
    
class Line(object):
     def __init__(self, Head, Tail  = Point() ):
        self.tail = Tail
        self.head = Head
        
     def length(self):
         vec = self.tail - self.head
         return np.sqrt(vec.x*vec.x + vec.y*vec.y)
    def findNearestPoint(self, to=Point(0.0, 0.0)):
        # return point of self (line) that is closest possible to 'to'
        P = self.tail
        vec = self.head - self.tail
        u = float(vec.dot(to - P))/float(vec.dot(vec))
        if u < 0: 
            u = 0 
        elif u > 1: 
            u = 1
        closetPoint = P + (u*vec)
        return closetPoint, closetPoint.lenth()
   
    
    
class Segment(Line):
    

class Circle(object):
    
    
    
    