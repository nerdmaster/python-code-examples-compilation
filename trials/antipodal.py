import math
from collections import namedtuple

# Antipodal distance (or polygon fetch or polygon diameter) for concave polygons

# If I were writing an algorithm I would simply check if a line between two vertices on the polygon intersects any line that forms one of the edges. Here's my pseudo code:

# 1) Identify all the vertices, store in a list
# 2) Identify all the edges, store in a list (as pairs of vertices from 1, perhaps)
# 3) for each vertex, get the distance to all other vertices, except:
#    a) exclude neighbouring vertices (those that share a pairing with this vertex in 2, perhaps)
#    b) exclude any line that intersects any line in 2. using something from here.
# 4) store all the valide distances with reference to the vertices in 1.
# 5) do what ever you want with the results, write new lines out, store the longest one for each polygon ...

# Now, I'm not sure if this is what you're after, but you certainly can do the above in ArcPy.

"""
I would be tempted to do this using angels, almost like line of sight. If while iterating the vertices in the shape the angles between the origin vertex and destination vertex continue in a consistent direction, all the points are candidates for the antipodal. If an angle switches direction, then that point is hidden by or hides the previous point. If it is hidden by the previous point, the point needs to be skipped. If it hides the previous point, the previous point(s) need to be removed from the candidate list.

Create a PolygonCandidates list
For each vertex (point k)
Create new list for candidates (Point, Angle)
Add the current vertex to candidates list (point k)
Iterate clockwise around polygon, for each remaining vertex (point i)
If the angle to the current point (from point k to point i) continues in a clockwise direction 1.add the point
If the angle to the current point continues in a counter-clockwise direction
If the previous two candidate points, plus the current point form a right hand turn.
Remove the last point in the list until the current angle and last candidate list angle is in a counterclockwise direction.
Add the current point to the candidates list
Add all but the first two, and last candidate points to a PolygonCandidates list
Find the furthest point in the PolygonCandidates list.
I'm not sure what to do with cases where the origin, and two other vertices all fall along the same line. In that case, the angle would be the same. If you had a polygon with holes, you could find the min/max angle of each hole, and remove any candidate point that lies within that range.

The main advantage to this approach would be that you don't have to test for line intersection between the current line segment and all the polygon edges.

This works...I think. I have updated the pseudo code above and the python in order to make it easier to read.
"""

import math
from collections import namedtuple


Point = namedtuple("Point", "position x y")
Vector = namedtuple("Vector", "source dest angle")

def isClockwise(angle1, angle2):
    diff = angle2 - angle1
    #print("         angle1:%s angle2:%s diff: %s" % (angle1, angle2, diff))
    if(diff > math.pi/2):
        diff = diff - math.pi/2
    elif (diff < -math.pi/2):
        diff = diff + math.pi/2
    #print("         diff:%s" % (diff))
    if(diff > 0):
        return False
    return True

def getAngle(origin, point):
    return math.atan2(point.y - origin.y, point.x-origin.x)

#returns a list of candidate vertcies.  This will include the first, second, and second to last points
#the first and last points in the polygon must be the same
#k is the starting position, only vertices after this position will be evaluated
def getCandidates (k, polygon):

    origin = polygon[k]
    candidates = [Vector(k,k,0)]
    prevAngle = 0;
    currentAngle = 0;
    for i in range(k + 1, len(polygon) - 1):

        current = polygon[i]
        #print("vertex i:%s x:%s y:%s  " % (i, current.x, current.y))

        if(i == k+1):
            prevAngle = getAngle(origin, current)
            candidates.append(Vector(k,i,prevAngle))
        else:
            currentAngle = getAngle(origin, current)
            #print("     prevAngle:%s currentAngle:%s  " % (prevAngle, currentAngle))
            if isClockwise(prevAngle, currentAngle):
                #print("     append")
                candidates.append(Vector(k,i,currentAngle))
                prevAngle = currentAngle
            else:
                #look at the angle between current, candidate-1 and candidate-2
                if(i >= 2):
                    lastCandinate = polygon[candidates[len(candidates) - 1].dest]
                    secondLastCandidate = polygon[candidates[len(candidates) - 2].dest]
                    isleft = ((lastCandinate.x - secondLastCandidate.x)*(current.y - secondLastCandidate.y) - (lastCandinate.y - secondLastCandidate.y)*(current.x - secondLastCandidate.x)) > 0
                    #print("     test for what side of polygon %s" % (isleft))
                    if(i-k >= 2 and not isleft):
                        while isClockwise(currentAngle, candidates[len(candidates) - 1].angle):
                            #print("     remove %s" % (len(candidates) - 1))
                            candidates.pop()
                        #print("     append (after remove)")
                        candidates.append(Vector(k,i,currentAngle))
                        prevAngle = currentAngle

        #for i in range(len(candidates)):
        #   print("candidate i:%s x:%s y:%s a:%s " % (candidates[i][0], candidates[i][1], candidates[i][2], candidates[i][3]))

    return candidates

def calcDistance(point1, point2):
    return math.sqrt(math.pow(point2.x - point1.x, 2) + math.pow(point2.y - point1.y, 2))

def findMaxDistance(polygon, candidates):
    #ignore the first 2 and last result
    maxDistance = 0
    maxVector = Vector(0,0,0);
    for i in range(len(candidates)):
        currentDistance = calcDistance(polygon[candidates[i].source], polygon[candidates[i].dest])
        if(currentDistance > maxDistance):
            maxDistance = currentDistance
            maxVector = candidates[i];
    if(maxDistance > 0):
        print ("The Antipodal distance is %s from %s to %s" % (maxDistance, polygon[candidates[i].source], polygon[candidates[i].dest]))
    else:
        print ("There is no Antipodal distance")

def getAntipodalDist(polygon):
    polygonCandidates = []
    for j in range(0, len(polygon) - 1):
        candidates = getCandidates(j, polygon)
        for i in range(2, len(candidates) - 1):
            #print("candidate i:%s->%s x:%s y:%s  " % (candidates[i].source, candidates[i].dest, candidates[i].x, candidates[i].y))
            polygonCandidates.append(candidates[i])

    for i in range(len(polygonCandidates)):
        print("candidate i:%s->%s" % (polygonCandidates[i].source, polygonCandidates[i].dest))
    findMaxDistance(polygon, polygonCandidates)


getAntipodalDist([Point(0,0,0),Point(1,-2,0),Point(2,-2,3),Point(3,2,2),Point(4,-1,1),Point(5,4,0),Point(6,0,0)])
getAntipodalDist([Point(0,0,0),Point(1,2,1),Point(2,1,4),Point(3,3,5),Point(4,5,4),Point(5,4,1),Point(6,0,0)])
getAntipodalDist([Point(0,0,0),Point(1,1,1),Point(2,2,1),Point(3,1,4),Point(4,3,5),Point(5,5,4),Point(6,4,1),Point(7,0,0)])
getAntipodalDist([Point(0,0,0),Point(1,-1,3),Point(2,1,4),Point(3,3,3),Point(4,2,0),Point(5,-2,-1),Point(6,0,0)])
