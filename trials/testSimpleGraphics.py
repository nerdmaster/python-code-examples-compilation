# -*- coding: utf-8 -*-
"""
File: testSimpleGraphics.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""

#try:  # import as appropriate for 2.x vs. 3.x
#   import tkinter as tk
#except:
#   import Tkinter as tk
#
#import time, os, sys
#from numpy import power, sqrt, sin, cos, pi
#import numpy as np
#import numbers
#import time


from simpleGraphics import *

#<------------------------------------------------------->#
def test():
#<------------------------------------------------------->#
    global win
    ORIGIN = Point(0.0, 0.0)
    win = GraphWin()
    win.setCoords(-10,-10,10,10)
    print 'Starting ...'
    
    t = Text(Point(0,0), "Centered Text")
    t.setSize(24)
    t.draw(win)
    print 'Drawn centered text'
    e = Entry(Point(5,6), 10)
    e.draw(win)
    print 'Drawn text entry point'
    
    print 'Click mouse'
    where = win.getMouse()#<--------------------(pause)
    p = Polygon(where + Point( 1, 1), 
                where + Point(-2, 3), 
                where + Point( 2,-5))
    p.draw(win)
    print 'Drawn polygon'
    p.setFill("red")
    p.setOutline("blue")
    p.setWidth(2)
    s = "Polygon Points: "
    for pt in p.getPoints():
        s = s + "(%0.1f,%0.1f) " % (pt.getX(), pt.getY())
    print 's:', s
    t.setText(e.getText())
    e.setFill("green")
    e.setText("Spam!")
    for step in range(10):
        e.move(-0.2,-0.5)
        p.move(0.2,0.3)
        time.sleep(0.2)
    print 'Click to continue ...'
    where = win.getMouse()#<--------------------(pause)
    s = "Clicked at: "
    s = s + "(%0.1f,%0.1f) " % (where.x, where.y)
    t.setText(s)
    
    print 'Click mouse to clear'
    where = win.getMouse()#<--------------------(pause)
    p.undraw()
    e.undraw()
    
#-----------------------------------------------------------------

    Ccenter = Point(-2, -5)
    Ccenter.draw(win)
    
    cir = Circle(Ccenter, 2.5)
    cir.setOutline('red')
    cir.setFill('blue')
    cir.draw(win)

    Rcenter = Point(3, 2)
    Rcenter.draw(win)
    size = Point(2, 3)
    
    rect = Rectangle(Rcenter+size, Rcenter-size)
    rect.setOutline('blue')
    rect.setFill('yellow')
    rect.draw(win)
    
    Pcenter = Point(-2, 0)
    Pcenter.draw(win)
    p0 = Point(-2.0, 0.0)
    p1 = Point(-1.0, 3.0)
    p2 = Point(-2.0, 4.0)
    p3 = Point( 1.0, 2.0)
    p4 = Point( 2.0,-1.0)
    p5 = Point( 2.0,-2.0)
    p6 = Point( 3.0,-3.0)
    p7 = Point(-2.0,-4.0)
    p8 = Point(-1.0,-1.0)
    polyPoints = [p0, p1, p2, p3, p4, p5, p6, p7, p8]
    poly = Polygon(polyPoints)
    poly.setOutline('black')
    poly.setFill('cyan')
    poly.draw(win)
    edges = poly.getEdges()
    for edge in edges:
        print edge
    e0 = edges[0]
    print 'e[0], e[1]', e0[0], e0[1]
    print 'e[0].x, e[0].y', e0[0].x, e0[0].y
 
    print 'CLick to condinue ....'
    where = win.getMouse()#<--------------------(pause)
   
#-----------------------------------------------------------------

    print 'CLick to see if inside or outside circle'
    L = None
    while True:
        location = win.getMouse()
        inside = cir.inRegion(location)
        howFar, where = cir.distance(location)
        print 'mouse clicked @ ', location
        if inside:
            print 'clicked inside!\n .... bye bye.'
            if not (L == None):
                L.undraw()
            cir.undraw()
            break
        else:
            print 'clicked outside'
            if not (L == None):
                L.undraw()
            L = Line (location, where)
            L.draw(win)
    Ccenter.undraw()

#-----------------------------------------------------------------

    print 'CLick to see if inside or outside rectangle'
    L = None
    while True:
        location = win.getMouse()
        print '--> mouse clicked @ ', location
        inside = rect.inRegion(location)
        howFar, where = rect.distance(location)
        print ' closest point: %s, distance = %4.2f' % (where, howFar)
        if inside:
            print 'clicked inside!\n .... bye bye.'
            if not (L == None):
                L.undraw()
            rect.undraw()
            break
        else:
            print 'clicked outside'
            if not (L == None):
                L.undraw()
            L = Line (location, where)
            L.draw(win)
    Rcenter.undraw()

#-----------------------------------------------------------------

    print 'CLick to see if inside or outside polygon'
    L = None
    while True:
        location = win.getMouse()
        print '--> mouse clicked @ ', location
#        inside = poly.inRegion(location)
        howFar, where = poly.distance(location)
        print ' closest point: %s, distance = %4.2f' % (where, howFar)
        if False:
            print 'clicked inside!\n .... bye bye.'
            if not (L == None):
                L.undraw()
#            poly.undraw()
#            break
        else:
            print 'clicked outside'
            if not (L == None):
                L.undraw()
            L = Line (location, where)
            L.draw(win)
    Pcenter.undraw()

#-----------------------------------------------------------------

    print 'Click to read the mouse position ...'
    where = win.getMouse()#<--------------------(pause)
    e.setText(where)
    t.setStyle("bold")
    t.setFace("arial")
    t.setSize(32)
    e.move(0,-2)

#-----------------------------------------------------------------

    print 'Click to continue ...'
    where = win.getMouse()#<--------------------(pause)
    print 'Drawing lines'
    p0 = Point(2.0, 1.0)
    print '>>> p0 = Point(2,1)'
    print '>>> p0\n', p0
    p1 = Point(7.0, 5.0)
    print '>>> p1 = Point(2,3)'
    print '>>> p1\n', p1
    p2 = Point(2.0, 4.0)
    print '>>> p2 = Point(5,7)'
    print '>>> p2\n', p2
    print '>>> p2[1]\n', p2[1]
    p3 = Point(1.0, 2.0)
    L1 = Line (p1, p2)
    L1.draw(win)
    time.sleep(0.5)
    L2 = Line (p1, p3)
    L2.draw(win)
    time.sleep(0.5)
    L3 = Line(p3, p2)
    L3.draw(win)
    time.sleep(0.5)
    print 'L3:', L3
    print 'L3.dir:', L3.dir
    H = win.getHeight()/2
    V = win.getWidth()/2
    print 'H: %s, V: %s' % (H, V)
    print 'click at the origin:'    

#-----------------------------------------------------------------

    where = win.getMouse(True)#<--------------------(pause)
    print 'clicked at:', where
    origin = where
    print 'origin', origin
    origin.draw(win)
    print 'click at the outter point:'    

#-----------------------------------------------------------------

    where = win.getMouse(True)#<--------------------(pause)
    outter = where
    print 'outter point:', outter
    outter.draw(win)
    L4 = Line(origin, outter)
    print 'radius = %5.3f,' % L4.length()
    L4.draw(win)
    for a in range(73):
        r = 2*pi*a/36
        print a ,r
        origin.move(sin(r),cos(r))
        L4.undraw()
        L4 = Line(origin, outter)
        L4.draw(win)
        time.sleep(0.1)

#-----------------------------------------------------------------

    win.promptMouse(0, 5, 'Click to Finish')
    win.promptClose(0, -3) 
    print ' ... done!'
    win.close()

test()

