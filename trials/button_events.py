class Paddle(object):
    def __init__(self, up, down):
        self.position = 5
        self.up_key = up
        self.down_key = down

    def move(self, pressed):
        if self.up_key in pressed and self.down_key not in pressed:
            self.position += 1
        elif self.down_key in pressed and self.up_key not in pressed:
            self.position -= 1


class Pong(Frame):
    def __init__(self):
        self.paddle1 = Paddle('w', 's')
        self.paddle2 = Paddle('Up', 'Down')
        self.buttons = set()
        self.bind_all('<KeyPress>', lambda event: self.buttons.add(event.keysym))
        self.bind_all('<KeyRelease>', lambda event: self.buttons.discard(event.keysym))

    def update(self):
        paddle1.move(self.buttons)
        paddle2.move(self.buttons)
