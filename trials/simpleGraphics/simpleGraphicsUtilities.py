# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""

formato = {'real': '%5.3f', # format for reals   display
           'int':  '%4d'  , # format for integers display
          }

#<------------------------------------------------------->#
#<---------------------- Utilities ---------------------->#
#<------------------------------------------------------->#

#<------------------------------------------------------->#
def is_numeric(obj):
    attrs = ['__add__', '__sub__', '__mul__', '__div__', '__pow__']
    return all(hasattr(obj, attr) for attr in attrs)

#<------------------------------------------------------->#
def pause(canvas):
    while True:
        where = canvas.getMouse()
        print where

#<------------------------------------------------------->#
def to_right(v1, v2):
    return (v1[0]*v2[1] - v1[1]*v2[0]) < 0
    
#<------------------------------------------------------->#
def color_rgb(r,g,b):
    """r,g,b are intensities of red, green, and blue in range(256)
    Returns color specifier string for the resulting color"""
    return "#%02x%02x%02x" % (r,g,b)

#<------------------------------------------------------->#
def value_oredered_list(start, end, step):
    if type(step) == int:
        step = float(end-start)/float(step)
    scale = float(end-start)/step
    result = range(int(scale*start), int(scale*end)+1, int(scale*step))
    delta = start - result[0]/scale
    for i in range(len(result)):
        result[i] = result[i]/scale + delta
    return result
        
#===================================================================
#===================================================================
#
# routine for performing the "point in polygon" inclusion test

# Copyright 2001, softSurfer (www.softsurfer.com)
# This code may be freely used and modified for any purpose
# providing that this copyright notice is included with it.
# SoftSurfer makes no warranty for this code, and cannot be held
# liable for any real or imagined damage resulting from its use.
# Users of this code must verify correctness for their application.

# translated to Python by Maciej Kalisiak <mac@dgp.toronto.edu>

#   a Point is represented as a tuple: (x,y)

#===================================================================

# is_left(): tests if a point is Left|On|Right of an infinite line.

#   Input: three points P0, P1, and P2
#   Return: >0 for P2 left of the line through P0 and P1
#           =0 for P2 on the line
#           <0 for P2 right of the line
#   See: the January 2001 Algorithm "Area of 2D and 3D Triangles and Polygons"

#<------------------------------------------------------->#
def is_left(P0, P1, P2):
    return (P1[0] - P0[0]) * (P2[1] - P0[1]) - (P2[0] - P0[0]) * (P1[1] - P0[1])

#===================================================================

# cn_PnPoly(): crossing number test for a point in a polygon
#     Input:  P = a point,
#             V[] = vertex points of a polygon
#     Return: 0 = outside, 1 = inside
# This code is patterned after [Franklin, 2000]

#<------------------------------------------------------->#
def cn_PnPoly(P, V):
    cn = 0    # the crossing number counter

    # repeat the first vertex at end
    V = tuple(V[:])+(V[0],)

    # loop through all edges of the polygon
    for i in range(len(V)-1):   # edge from V[i] to V[i+1]
        if ((V[i][1] <= P[1] and V[i+1][1] > P[1])   # an upward crossing
            or (V[i][1] > P[1] and V[i+1][1] <= P[1])):  # a downward crossing
            # compute the actual edge-ray intersect x-coordinate
            vt = (P[1] - V[i][1]) / float(V[i+1][1] - V[i][1])
            if P[0] < V[i][0] + vt * (V[i+1][0] - V[i][0]): # P[0] < intersect
                cn += 1  # a valid crossing of y=P[1] right of P[0]

    return cn % 2   # 0 if even (out), and 1 if odd (in)

#===================================================================

# wn_PnPoly(): winding number test for a point in a polygon
#     Input:  P = a point,
#             V[] = vertex points of a polygon
#     Return: wn = the winding number (=0 only if P is outside V[])

#<------------------------------------------------------->#
def wn_PnPoly(P, V):
    wn = 0   # the winding number counter

    # repeat the first vertex at end
    V = tuple(V[:]) + (V[0],)

    # loop through all edges of the polygon
    for i in range(len(V)-1):     # edge from V[i] to V[i+1]
        if V[i][1] <= P[1]:        # start y <= P[1]
            if V[i+1][1] > P[1]:     # an upward crossing
                if is_left(V[i], V[i+1], P) > 0: # P left of edge
                    wn += 1           # have a valid up intersect
        else:                      # start y > P[1] (no test needed)
            if V[i+1][1] <= P[1]:    # a downward crossing
                if is_left(V[i], V[i+1], P) < 0: # P right of edge
                    wn -= 1           # have a valid down intersect
    return wn

#===================================================================
#===================================================================

# orientation of the point (x,y) wrt segment (a0,b0)-(a1,b1)
# orientation of the point P = Point(x,y) with respect to the given 
# segment P_tail = Point(x0,y0) --> P_head = Point(x1,y1)
# results: 0: collinear   
#          1: within right half-plane   
#         -1: within left half-plane
# calculated with a simple cross product

#<------------------------------------------------------->#
def orientation(P, P_tail, P_head):
    x , y  = P.asTuple()
    x0, y0 = P_tail.asTuple()
    x1, y1 = P_head.asTuple()
    return cmp((x1-x0)*y + (y0-y1)*x + x0*y1-x1*y0, 0)
 
# The double winding number of the given path ps about the given point p0
#<------------------------------------------------------->#
def windingNumber(P, pathPoints):
    # Use the point P = Point(x,y) to divide the plane into two half-planes 
    # "above" (-1) and "below"(1) depending on which half-plane each point is in.
    # return "aligned" (0) if it's equivalent.
    h = [cmp(point, P) for point in pathPoints]
    w = 0
    for j in range(len(pathPoints)):
        i, k = (j-1)%len(pathPoints), (j+1)%len(pathPoints)  # Previous and next points in the path
        if h[j] * h[k] == 1:  # Both points are in the same half-plane, so do nothing
            pass
        elif h[j] * h[k] == -1:  # Crossing from one half-plane to the other
            w += orientation(P, pathPoints[j], pathPoints[k])
        elif h[j] == 0:  # p[j] == P
            if h[i] != h[k]:  # Cross from one half-plane to the other, don't change w
                pass
            else:  # Leave P through the same half-plane you entered through
                w += orientation(pathPoints[k], pathPoints[i], pathPoints[j])
        else:  # this pathPoints is leading either toward or away from P
            pass
    return w

#<------------------------------------------------------->#
