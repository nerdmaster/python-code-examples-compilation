"""
WalkerGraphics
"""

import scipy, math, time
import Tkinter 
import Image, ImageTk, ImageDraw

DefaultImageSize = (400,400)

root = Tkinter.Tk()
root.withdraw()

class WalkerDisplay (Tkinter.Label):

    def __init__(self, walker, size=DefaultImageSize, mode='P'):
        top = Tkinter.Toplevel()
        top.title('WalkerDisplay')
        Tkinter.Label.__init__(self, top)
	self.size = size
	self.mode = mode
	self.walker = walker
        self.xMargin = 0.3
        self.yMargin = 0.3
        self.canvas = Tkinter.Canvas(top, 
				     width=self.size[0], height=self.size[1])
	self.im = Image.new('RGB', self.size)
        self.blank = self.im.copy()
        self.draw = ImageDraw.Draw(self.im)
	if self.mode == '1':
	    self.tkimage = \
			 ImageTk.BitmapImage(self.im,
					     foreground="white")
	else:
	    self.tkimage = \
			 ImageTk.PhotoImage(self.im)
	self.canvas.create_image(0, 0, anchor=Tkinter.NW, image=self.tkimage)
        self.canvas.pack()

    def reset(self):
        self.im.paste(self.blank)

    def setTitle(self, title):
        self.master.title(title)

    def scale(self, x, y, xcenter=None):
        if xcenter:
            sx = (x-xcenter)/(self.walker.L)*(1.-2.*self.xMargin)*self.size[0]\
                 + self.size[0]/2
        else:
            sx = x*(1.-2.*self.xMargin)*self.size[0]
        sy = self.size[1] - \
             y*(1-2.*self.yMargin)*self.size[1] - self.yMargin*self.size[1]
        return sx, sy

    def update(self, clear=True, sleep=0.0):
        if clear:
            self.reset()
        time.sleep(sleep)
        windowMargin = 0.1
        bodysize=40
        footsize=10
        windowSize = self.size[0]
        color = (255,255,255)
        center = windowSize/2.
        x1,y1,z1 = self.walker.GetBodyPos()
        x2,y2,z2 = self.walker.GetStanceFootPos()
        x3,y3,z3 = self.walker.GetSwingFootPos()
        sx1, sy1 = self.scale(x1, y1, xcenter=x1)
        #sx1, sy1 = self.scale(x1, y1, xcenter=x2-0.25*self.walker.L)
        sx2, sy2 = self.scale(x2, y2, xcenter=x1)
        #sx2, sy2 = self.scale(x2, y2, xcenter=x2-0.25*self.walker.L)
        sx3, sy3 = self.scale(x3, y3, xcenter=x1)
        #sx3, sy3 = self.scale(x3, y3, xcenter=x2-0.25*self.walker.L)
        self.draw.line( ((sx1, sy1),
                            (sx2, sy2)), fill='green', width=10)
        self.draw.rectangle( ((sx2, sy2),
                            (sx2+3*footsize, sy2+footsize)),
                           fill='green')
        self.draw.line( ((sx1, sy1),
                         (sx3, sy3)), fill='yellow', width=10)
        self.draw.rectangle( ((sx3, sy3),
                            (sx3+3*footsize, sy3+footsize)),
                           fill='yellow')
        self.draw.ellipse( ((sx1-bodysize/2, sy1-bodysize/2),
                            (sx1+bodysize/2, sy1+bodysize/2)), fill='blue')
        self.tkimage.paste(self.im)
        self.canvas.update()

	    

# Copyright (C) Cornell University
# All rights reserved.
# Apache License, Version 2.0


