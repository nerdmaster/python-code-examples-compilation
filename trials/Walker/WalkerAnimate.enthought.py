"""Animates Walker"""
#
# Uses Enthought TVTK
# If you are using things interactively, you must start
# ipython with the --pylab flag
#
# See the exercise "Walker.pdf" from Walker.html
# in  http://www.physics.cornell.edu/~myers/teaching/ComputationalMethods/ComputerExercises/
#
import enthought.tvtk.tools.visual as V
import time

scene=V.get_viewer()

scene.title = 'Walker'
scene.height = scene.width = 600
# Follow walker as it moves forward
scene.autocenter = 1
# Scale set automatically by system, but don't keep rescaling as walker moves
scene.autoscale = 1
# Slow down walker to reasonable speed
framesPerSecond = 120

# Define "rate" command which keeps animation frames from exceeding a certain rate
# This is not a particularly profound approach.  The "official" way to do this
# in tvtk is to use the tvtk.tools.visual.iterate command.

class timer() :
    def __init__(self) :
        self.time=time.time()
    def __call__(self,rate):
        newtime=time.time()
        timedif=newtime-self.time
        if rate*timedif<1 :
            time.sleep(timedif)
        self.time=time.time()

arate = timer();

# Set Camera up -- we will call this as needed 
def resetcamera(targ) :
    camera=scene.scene.camera
    camera.parallel_projection=1
    camera.position=(0,0,3)
    camera.focal_point=targ
    camera.view_up=(0,1,0)
    camera.parallel_scale=1
    camera.clipping_range=(1,5)

import Walker as W
reload(W)	# for ipython to reload properly after changes in Walker
import scipy

# Walker Display

class WalkerDisplay:
    """Defines VPython cylinders for stanceLeg, swingLeg."""
    def __init__(self, w):
        """Store walker being displayed.
	Set up stanceLeg and swingLeg to be cylinders.
	stanceLeg extends from stance foot position to body position.
	swingLeg extends from body position to swing foot position."""
    	self.w = w # the walker being displayed
	d = 0.06   # thickness of legs
	self.stanceLeg = V.cone(pos=w.GetBodyPos(), radius = d, 
                                color = V.color.red,
                                axis = -w.GetBodyPos()+w.GetStanceFootPos())
	self.swingLeg = V.cone(pos=w.GetBodyPos(), radius = d, 
                               color = V.color.orange,
                               axis = w.GetSwingFootPos()-w.GetBodyPos())

    def update(self):
        """Updates pos and axis for stanceLeg and swingLeg."""
	self.stanceLeg.pos = self.w.GetBodyPos()
	self.stanceLeg.axis = (-self.w.GetBodyPos()+self.w.GetStanceFootPos())
	self.swingLeg.pos = self.w.GetBodyPos()
	self.swingLeg.axis = self.w.GetSwingFootPos()-self.w.GetBodyPos()

class CenteredWalkerDisplay:
    """Defines display centered on midpoint between stance and swing feet."""
    def __init__(self, w):
        """Store walker being displayed.
	Set up stanceLeg and swingLeg to be cylinders.
	stanceLeg extends from stance foot position to body position.
	swingLeg extends from body position to swing foot position."""
    	self.w = w # the walker being displayed
	d = 0.06   # thickness of legs
        center = (w.GetStanceFootPos() + w.GetSwingFootPos())/2.0
	self.stanceLeg = V.cone(pos=w.GetBodyPos()-center, radius = d, 
                                color = V.color.red,
                                axis = -w.GetBodyPos()+w.GetStanceFootPos())
	self.swingLeg = V.cone(pos=w.GetBodyPos()-center, radius = d, 
                               color = V.color.orange,
                               axis = w.GetSwingFootPos()-w.GetBodyPos())
    def update(self):
        """Updates pos and axis for stanceLeg and swingLeg."""
        center = (self.w.GetStanceFootPos() + self.w.GetSwingFootPos())/2.0
	self.stanceLeg.pos = self.w.GetBodyPos()-center
	self.stanceLeg.axis = (-self.w.GetBodyPos()+self.w.GetStanceFootPos())
	self.swingLeg.pos = self.w.GetBodyPos()-center
	self.swingLeg.axis = self.w.GetSwingFootPos()-self.w.GetBodyPos() 


def SillyWalk(w, dt=0.1):
    """Integrates walker differential equations, but ignores heel strikes.
    (1) Set scene.autocenter=0 to avoid following walker center of mass.
    (2) Pick times for some reasonable range (say to 100), 
        find trajectory from odeint.
    (3) Run through trajectory, setting state of system and 
        then updating display. Run rate(framesPerSecond) inside loop
	to slow things down.
    (4) Set scene.autocenter back to 1."""
    scene.autoscale = 1
    scene.autocenter = 1
    dw = WalkerDisplay(w)
    scene.autoscale = 0
    scene.autocenter = 0
    time = scipy.arange(0.0,100.0,dt)
    y0 = w.GetStateVector()[:]
    resetcamera(w.GetBodyPos())
    y_trajectory = scipy.integrate.odeint(w.dydt, y0, time, atol=1.0e-10)
    for n in range(len(time)):
        #arate(framesPerSecond)
        w.SetStateVector(y_trajectory[n])
        #resetcamera(w.GetBodyPos())
        dw.update()
    scene.autocenter = 1
    dw.stanceLeg.visible = 0
    dw.swingLeg.visible = 0


def SimpleWalk(w, dwc=None, gamma=0.009, t_initial=0., t_final=40.2, dt=0.02):
    """Set gamma for walker w. Walk forward in steps of dt, 
    and update display."""
    w.gamma = gamma
    if dwc is None:
        dwc = CenteredWalkerDisplay(w)
    for t in scipy.arange(t_initial, t_final, dt):
        arate(framesPerSecond)	# Pause to show all frames
        w.Walk(t, t+dt, dt*29./30.)
        dwc.update()
    dwc.stanceLeg.visible = 0
    dwc.swingLeg.visible = 0


def MultiWalkFromFile(w, filename="AttractorPointsPruned.dat"):
    """Reads gammas, initial conditions from file to start on attractor"""
    scene.autoscale = 1
    scene.autocenter = 1
    dwc = CenteredWalkerDisplay(w)
    scene.autoscale = 0
    scene.autocenter = 0
    for line in file(filename):
        w.gamma, w.theta, w.thetaDot, w.phi, w.phiDot = \
                 map(float, line.split())
        # File states saved just after heel strike
	w.RoundCollisionConditionPositive()
        SimpleWalk(dwc, w.gamma, dt=0.02, t_final=20.)
    dwc.stanceLeg.visible = 0
    dwc.swingLeg.visible = 0

def yesno():
    response = raw_input('    Continue? (y/n) ')
    if len(response)==0:        # [CR] returns true
        return True
    elif response[0] == 'n' or response[0] == 'N':
        return False
    else:                       # Default
        return True
    
def demo():
    print "Walker Animation Demo"
    if not yesno(): return
    print "Silly Walk"
    w = W.Walker()
    SillyWalk(w)
    if not yesno(): return
    w = W.Walker()
    print "Simple Walk, gamma=0.009"
    SimpleWalk(w, gamma=0.009)
    if not yesno(): return
    print "Simple Walk, gamma=0.012"
    SimpleWalk(w, gamma=0.012)
    if not yesno(): return
    print "Simple Walk, gamma=0.015"
    SimpleWalk(w, gamma=0.015)
    if not yesno(): return
    print "Simple Walk, gamma=0.017"
    SimpleWalk(w, gamma=0.017)
    if not yesno(): return
    print "Simple Walk, gamma=0.020"
    SimpleWalk(w, gamma=0.020)
    
if __name__ == '__main__':
    demo()

# Copyright (C) Cornell University
# All rights reserved.
# Apache License, Version 2.0


