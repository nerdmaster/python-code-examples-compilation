# -*- coding: utf-8 -*-
"""
File: simpleGeomertry.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
URL: http://www.maprantala.com/2010/05/16/measuring-distance-from-a-point-to-a-line-segment/
Description: Define Point, Segment and some methods
--------------------------------------------------------
Inputs: None
--------------------------------------------------------
Outputs: None
--------------------------------------------------------
"""
from numpy import power, sqrt, sin, cos, pi
import numpy as np
import numbers
import scipy.linalg as la
import simpleGraphics as g
import matplotlib.mlab as ml

def is_numeric(obj):
    attrs = ['__add__', '__sub__', '__mul__', '__div__', '__pow__']
    return all(hasattr(obj, attr) for attr in attrs)
    
def value_oredered_list(start, end, step):
    if type(step) == int:
        step = float(end-start)/float(step)
    scale = float(end-start)/step
    result = range(int(scale*start), int(scale*end)+1, int(scale*step))
    delta = start - result[0]/scale
    for i in range(len(result)):
        result[i] = result[i]/scale + delta
    return result
        
# orientation of the point (x,y) wrt segment (a0,b0)-(a1,b1)
# orientation of the point P = Point(x,y) with respect to the given 
# segment P_tail = Point(x0,y0) --> P_head = Point(x1,y1)
# results: 0: collinear   
#          1: within right half-plane   
#         -1: within left half-plane
# calculated with a simple cross product
def orientation(P, P_tail, P_head):
    x , y  = P.asTuple()
    x0, y0 = P_tail.asTuple()
    x1, y1 = P_head.asTuple()
    return cmp((x1-x0)*y + (y0-y1)*x + x0*y1-x1*y0, 0)
 
# The double winding number of the given path ps about the given point p0
def windingNumber(P, pathPoints):
    # Use the point P = Point(x,y) to divide the plane into two half-planes 
    # "above" (-1) and "below"(1) depending on which half-plane each point is in.
    # return "aligned" (0) if it's equivalent.
    h = [cmp(point, P) for point in pathPoints]
    w = 0
    for j in range(len(pathPoints)):
        i, k = (j-1)%len(pathPoints), (j+1)%len(pathPoints)  # Previous and next points in the path
        if h[j] * h[k] == 1:  # Both points are in the same half-plane, so do nothing
            pass
        elif h[j] * h[k] == -1:  # Crossing from one half-plane to the other
            w += orientation(P, pathPoints[j], pathPoints[k])
        elif h[j] == 0:  # p[j] == P
            if h[i] != h[k]:  # Cross from one half-plane to the other, don't change w
                pass
            else:  # Leave P through the same half-plane you entered through
                w += orientation(pathPoints[k], pathPoints[i], pathPoints[j])
        else:  # this pathPoints is leading either toward or away from P
            pass
    return w
 
def pause(canvas):
    while True:
        where = canvas.getMouse()
        print where
    
class Point(object):
    # A simple Point class
    global canvas
    def __init__(self, X, Y = None):
        if isinstance(X, Point):
            self.x = X.x
            self.y = X.y
        elif type(X) in (list, tuple):
            self.x = X[0]
            self.y = X[1]
        elif type(X) == np.ndarray:
            self.x = X[0]
            self.y = X[1]
        elif isinstance(X, dict):
            self.x = X['x']
            self.y = X['y']
        else:
            self.x = X
            self.y = Y
    def __str__(self):
        return "Point( %5.3f, %5.3f )" % (self.x, self.y)
    def __repr__(self):
        return str(self)
    def abs(self):
        # absolute length of vector ||v||
        return sqrt(self.x*self.x + self.y*self.y)     
    def distance (self, other):
        return sqrt(power((self.x - other.x), 2) \
               +    power((self.y - other.y), 2))
    #---------------------------------------------
    # magics:
    #---------------------------------------------
    def __eq__(self,other):
        if self.x==other.x and self.y==other.y:
            return True
        else:
            return False
    def __ne__(self,other):
        if self==other:
            return False
        else:
            return True
    def __sub__(self,other):
        return Point(self.x - other.x, self.y - other.y)
    def __add__(self,other):
        return Point(self.x + other.x, self.y + other.y)
    def __neg__(self):
        return Point(-self.x, -self.y)
    def __mul__(self, other):
        if type(other) == float or type(other) == int:
            return Point(self.x * other, self.y * other)
        else:
            raise TypeError, 'The arguments passed must be Numerical'
    def __rmul__(self, other):
        if type(other) == float or type(other) == int:
            return Point(self.x * other, self.y * other)
        else:
            raise TypeError, 'The arguments passed must be Numerical'
    def __div__(self, other):
        if is_numeric(other):
            return Point(self.x / other, self.y / other)
        else:
            raise TypeError, 'The arguments passed must be Numerical'
    def __gt__(self, other):
        # ||self|| > ||other|| ?
        if isinstance(other, Point):
            return (self.abs() > other.abs())
        elif type(other) == numbers.Real:
            return (self.abs() > other)
    def __lt__(self, other):
        # ||self|| < ||other|| ?
        if isinstance(other, Point):
            return (self.abs() < other.abs())
        elif type(other) == numbers.Real:
            return (self.abs() < other)
    def __cmp__(self, other):
        if self < other:
            return -1
        elif self > other:
            return 1
        else:
            return 0
    def __getitem__(self, other):
        values = {0:self.x,1:self.y}
        return values.get(other)
    def __setitem__(self, idx, val):
        if idx == 0:
            self.x = val
        if idx == 1:
            self.y = val
        elif idx == 'x':
            self.x = val
        elif idx == 'y':
            self.y = val
    def __hash__(self):
        return hash((self.x, self.y))
    def normalized(self):
        Normalized = Point(0,0)
        if self.abs() != 0.0:
             Normalized = self / self.abs();
        return Normalized
    def dot(self, other):
        return (self.x*other.x + self.y*other.y)
    def asList(self):
        # return elements in a list
        return [self.x, self.y]
    def asArray(self):
        # return elements as an array
        return np.array([self.x,self])
        
#    def draw(self, canvas = None):
#        import graphics as g
#        if not canvas:
#            canvas = g.GraphWin()
#        pt = self.asList()
#        print 'pt:', pt
#        pt = g.Point(pt[0], pt[1])
#        pt.draw(canvas)
    def move(self, dx, dy):
        self.x = self.x + dx
        self.y = self.y + dy

#    def draw(self, canvas):
#        x,y = canvas.toScreen(self.x,self.y)
#        canvas.create_rectangle(x,y,x+1,y+1,options)
#        pause(canvas)
#        return canvas.create_rectangle(x,y,x+1,y+1,options)
        
            
class Segment(object):
    # A simple line segment class
    EPS = 0.0000001
    def __init__(self, P1, P2):
        self.tail = P1
        self.head = P2
    def __repr__(self):
        text = 'Segment([%s-%s])' % (self.tail,self.head)
        return text
    def length (self):
        return sqrt(power((p1.x - p2.x), 2) \
               +       power((p1.y - p2.y), 2))
    def __setitem__(self, idx, val):
        if idx == 0:
            self.tail = val
        elif idx == 0:
            self.head = val
        elif idx == 'tail':
            self.tail = val
        elif idx == 'head':
            self.head = val
        return self.points[idx]
    def __getitem__(self, idx):
        if idx == 0:
            return self.tail
        elif idx == 0:
            return self.head
        elif idx == 'tail':
            return self.tail
        elif idx == 'head':
            return self.head
    def head(self):
        return self.head
    def tail(self):
        return self.tail
    def distance(self, P):
        # distance from segment to point
        LineMag = self.length()
        if LineMag < Segment.EPS:
            return np.inf, np.inf
        SegmentMagnitude = self.length()
        u = (((P.x - self.tail.x) * (self.head.x - self.tail.x)) \
           + ((P.y - self.tail.y) * (self.head.y - self.tail.y)))\
           / SegmentMagnitude / SegmentMagnitude
        if (u < Segment.EPS) or (u > 1):
            #// closest point does not fall within the line segment, 
            #   take the shorter distance to an endpoint
            d1 = P.distance(self.tail)
            d2 = P.distance(self.head)
            if d1 > d2:
                DistancePointSegment = d2
                PointInSegment = self.head
            else:
                DistancePointSegment = d1
                PointInSegment = self.tail
        else:
            # Intersecting point is on the line, use the formula
            d1 = self.tail.x + u * (self.head.x - self.tail.x)
            d2 = self.tail.y + u * (self.head.y - self.tail.y)
            PointInSegment = Point(d1, d2)
            DistancePointSegment = PointInSegment.distance(P)
        return DistancePointSegment, PointInSegment
    def findNearestPoint(self, to=Point(0.0, 0.0)):
        # return point of self (line) that is closest possible to 'to'
        P = self.tail
        vec = self.head - self.tail
        u = float(vec.dot(to - P))/float(vec.dot(vec))
        if u < 0: 
            u = 0 
        elif u > 1: 
            u = 1
        return (P + (u*vec))
#    def draw(self, canvas = None):
#        import graphics as g
#        if not canvas:
#            canvas = g.GraphWin()
#        tail = self.tail
#        head = self.head
#        ln = g.Line(g.Point(tail[0], tail[1]), g.Point(head[0],head[1]))
#        print 'pt:', ln
#        ln.draw(canvas)
#    def _draw(self, canvas, options):
#        p1 = self.p1
#        p2 = self.p2
#        x1,y1 = canvas.toScreen(p1.x,p1.y)
#        x2,y2 = canvas.toScreen(p2.x,p2.y)
#        return canvas.create_line(x1,y1,x2,y2,options)
        
class Line(Segment):
    def __repr__(self):
        text = 'Line([%s->%s])' % (self.tail,self.head)
        return text
    def findNearestPoint(self, to=Point(0.0, 0.0)):
        # return point of self (line) that is closest possible to 'to'
        P = self.tail
        vec = self.head - self.tail
        u = float(vec.dot(to - P))/float(vec.dot(vec))
        return (P + (u*vec))
    
class Vector(Segment):
    # A simple Vector class
    def __init__(self, Head, Tail = Point(0,0)):
        self.tail = Tail
        self.head = Head
    def __repr__(self):
        text = 'Vector([%s->%s])' % (self.tail,self.head)
        return text
    def dot(self, P):
        # distance from segment to point
        pass


if __name__ == "__main__":
#    global canvas
#    canvas = g.GraphWin()

    p0 = Point(0.0, 0.0)
    print '>>> p0 = Point(0,0)'
    print '>>> p0\n', p0
    p1 = Point(2.0, 3.0)
    print '>>> p1 = Point(2,3)'
    print '>>> p1\n', p1
    p2 = Point(5.0, 7.0)
    print '>>> p2 = Point(5,7)'
    print '>>> p2\n', p2
    print '>>> p2[1]\n', p2[1]
    pDict = {'x': 1.0, 'y': 2.0}
    print ">>> pDict = {'x': 1.0, 'y': 2.0}"
    print '>>> pDict\n', pDict
    p3 = Point(pDict)
    print '>>> p3 = Point(pDict)'
    print '>>> p3\n', p3
    pTuple = (3.0, 4.0)
    print ">>> pTuple = (3.0, 4.0)"
    print '>>> pTuple\n', pTuple
    p4 = Point(pTuple)
    print '>>> p4 = Point(pTuple)'
    print '>>> p4\n', p4
    pList = [5.0, 6.0]
    print ">>> pList = [5.0, 6.0]"
    print '>>> pList\n', pList
    p5 = Point(pList)
    print '>>> p5 = Point(pList)'
    print '>>> p5\n', p5
    pArray = np.array([1,2])
    print ">>> pArray = np.array([1,2])"
    print '>>> pArray\n', pArray
    p6 = Point(pArray)
    print '>>> p6 = Point(pArray)'
    print '>>> p6\n', p6
    print '>>> print p6[1]=3'
    p6[1]=3
    print '>>> p6\n', p6
    print '>>> cmp(p5,p6)\n', cmp(p5,p6)
    print '>>> cmp(p6,p5)\n', cmp(p6,p5)
    print '>>> cmp(p5,p5)\n', cmp(p5,p5)
    
    s1 = Segment(p1,p2)
    print '>>> s1 = Segment(p1,p2)'
    print '>>> s1\n', s1
    print '>>> s1.length()\n', s1.length()
    
    p7  = Point( 0.0,  0.0)
    p8  = Point( 7.0,  7.0)
    p9  = Point( 0.0,  0.0)
    p10 = Point( 3.0,  3.0)
    p11 = Point( 2.0,  7.0)
    p12 = Point(-1.0, -1.0)
    
    points = [p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12]
    
    for point in points:
        distance, where = s1.distance(point)
        print 'd(%s) is %5.3f \n--> Intercept @ %s' % (point, distance, where)

    p13 = p4.normalized()
    print ('p4:{}\np13 = p4.normalized():{}').format(p4, p13)
    p14 = s1.findNearestPoint(p0)
    print '>>> p4\n', p0
    print '>>> p14 = s1.findNearestPoint(p0)'
    print '>>> p14\n', p14
    p15 = s1.findNearestPoint(p4)
    print '>>> p4\n', p4
    print '>>> p15 = s1.findNearestPoint(p4)'
    print '>>> p15\n', p15
    
    v1 = Vector(3,2)
    v2 = Vector(p4,p2)
    
    print 'v1:', v1
    print 'v2:', v2

    l1 = Line(p1,p2)
    print '>>> l1 = Line(p1,p2)'
    print '>>> l1\n', l1
    print '>>> l1.length()\n', l1.length()
    p14 = l1.findNearestPoint(p0)
    print '>>> p4\n', p0
    print '>>> p14 = l1.findNearestPoint(p0)'
    print '>>> p14\n', p14
    p15 = l1.findNearestPoint(p4)
    print '>>> p4\n', p4
    print '>>> p15 = l1.findNearestPoint(p4)'
    print '>>> p15\n', p15
    
#    pause(canvas)
  
  