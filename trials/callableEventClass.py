# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""


def myfunc(sender):
     print("event triggered by %s" % sender)

class MyClass(object):
    def __init__(self, items = None):
#        self.anevent = EventHandler(self)
        self.anevent = myfunc
        self.items = items
    def __call__(*args):
        print 'callable class method'
    def whoami(self):
        return self.__class__.__name__
    def __iter__(self):
        return (i for i in self.items)
    def __str__(self):
        return self.items

myobj = MyClass([1, 2, 3, 4])
myobj.anevent += myfunc
for item in myobj:
    print item
myobj.anevent()
print myobj
print (item for item in myobj)



