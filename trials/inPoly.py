# -*- coding: utf-8 -*-
"""
File: inPoly.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""

import numpy as np
import matplotlib as mp
import scipy as sp

# determine if a point is inside a given polygon or not
# Polygon is a list of (x,y) pairs.

def point_inside_polygon(x,y,poly):

    n = len(poly)
    inside =False

    p1x,p1y = poly[0]
    for i in range(n+1):
        p2x,p2y = poly[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xinters:
                        inside = not inside
        p1x,p1y = p2x,p2y

    return inside
    




#XY is the 2D array.
#A is one of the 1D arrays.
#poly is a matplotlib.patches.Polygon

polyList = [(1, 1), (4, 2), (5, 5), (3, 8), (0, 4)]

pointSet = set(['(3, 0), (10, -2), (3,3), (5, 5), (3, 8), (0, 4)'])

#mask = np.array([bool(poly.get_path().contains_point(i)) for i in XY])
#
#matplotlib.pylab.hist(A[mask], 100)
#matplotlib.pylab.show()

class Vector():
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y
    def __str__(self):
        return 'V_[%5.2f,%5.2f]' % (self.x,self.y)
    def __repr__(self):
        return 'V->[%5.2f,%5.2f]' % (self.x,self.y)
    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)
    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)
    def __getitem__(self, other):
        values = {0:self.x,1:self.y}
        return values.get(other)
    def __setitem__(self, idx, val):
        if idx == 0:
            self.x = val
        if idx == 1:
            self.y = val

v1 = Vector(2,3)
v2 = Vector(1,5)

v3 = v1-v2
v4 = v1+v2

print 'v1, v2, v3, v4\n', v1, v2, v3, v4

#-----------------------------------------------

def to_right(v1, v2):
    return (v1[0]*v2[1] - v1[1]*v2[0]) < 0

poly = [Vector(1, 1), 
        Vector(4, 2), 
        Vector(5, 5), 
        Vector(3, 8), 
        Vector(0, 4)]

print '\n Polygon vertices:'
for i in range(len(poly)):
    print 'poly[%d]: %s' %(i, poly[i])

points = [Vector( 3, 0), 
          Vector(10,-2),  
          Vector( 1, 3),  
          Vector( 3, 3),  
          Vector( 2, 4),  
          Vector( 4, 2),  
          Vector( 7, 5),  
          Vector( 6, 7),  
          Vector( 5, 5),  
          Vector( 3, 8),  
          Vector( 9, 3),  
          Vector(10, 7),  
          Vector( 8, 7),  
          Vector(-2,-1),  
          Vector( 1, 8),  
          Vector( 0, 4)]
          
print '\n Selected Points:'
for i in range(len(points)):
    print 'point[%d]: %s' %(i, points[i])

for i in range(len(poly)):
    v1 = poly[i-1]
    v2 = poly[i]
    for p in points:
        if(to_right(v2-v1, p-v1)):
            points.remove(p)

print '\n Selected Points after:'
for i in range(len(points)):
    print 'point[%d]: %s' %(i, points[i])

#-----------------------------------------------

poly2 = [Vector( 1, 1), 
         Vector( 4, 2), 
         Vector(10, 2), 
         Vector( 9, 9), 
         Vector( 7, 8), 
         Vector( 7, 4), 
         Vector( 5, 5), 
         Vector( 3, 8), 
         Vector( 0, 4)]

print '\n Polygon vertices:'
for i in range(len(poly2)):
    print 'poly2[%d]: %s' %(i, poly2[i])

points = [Vector( 3, 0), 
          Vector(10,-2),  
          Vector( 1, 3),  
          Vector( 3, 3),  
          Vector( 2, 4),  
          Vector( 4, 2),  
          Vector( 7, 5),  
          Vector( 6, 7),  
          Vector( 5, 5),  
          Vector( 3, 8),  
          Vector( 9, 3),  
          Vector(10, 7),  
          Vector( 8, 7),  
          Vector(-2,-1),  
          Vector( 1, 8),  
          Vector( 0, 4)]
          
print '\n Selected Points:'
for i in range(len(points)):
    print 'point[%d]: %s' %(i, points[i])

for i in range(len(poly)):
    v1 = poly2[i-1]
    v2 = poly2[i]
    for p in points:
        if(to_right(v2-v1, p-v1)):
            points.remove(p)

print '\n Selected Points after:'
for i in range(len(points)):
    print 'point[%d]: %s' %(i, points[i])



# See http://local.wasp.uwa.edu.au/~pbourke/geometry/insidepoly/
import math
 
class Point:
    def __init__(self, h, v):
        self.h = h
        self.v = v
 
def InsidePolygon(polygon, p):
   angle = 0.0
   n = len(polygon)
 
   for i, (h, v) in enumerate(polygon):
      p1 = Point(h - p.h, v - p.v)
      h, v = polygon[(i + 1) % n]
      p2 = Point(h - p.h, v - p.v)
      angle += Angle2D(p1.h, p1.v, p2.h, p2.v);
 
   if abs(angle) < math.pi:
      return False
 
   return True
 
def Angle2D(x1, y1, x2, y2):
   theta1 = math.atan2(y1, x1)
   theta2 = math.atan2(y2, x2)
   dtheta = theta2 - theta1
   while dtheta > math.pi:
      dtheta -= 2.0 * math.pi
   while dtheta < -math.pi:
      dtheta += 2.0 * math.pi
 
   return dtheta
   

import matplotlib.mlab as ml
help(ml.inside_poly)

points = [[ 1.00, 3.00],
          [ 3.00, 3.00],
          [ 2.00, 4.00],
          [ 4.00, 2.00],
          [ 7.00, 5.00],
          [ 6.00, 7.00],
          [ 5.00, 5.00],
          [ 8.00, 7.00],
          [ 0.00, 4.00]]
          
poly2 =[[1.00,1.00], [4.00,2.00], [10.00,2.00], [9.00,9.00], 
        [7.00,8.00], [7.00,4.00], [5.00,5.00], [3.00,8.00], [0.00,4.00]]

results = ml.inside_poly(points, poly2)

for result in results:
    print result, points[result]


#typedef struct {int x, y;} Point;
#
#wn_PnPoly( Point P, Point V[], int n )
#{
#    int    wn = 0;    // the  winding number counter
#
#    // loop through all edges of the polygon
#    for (each edge E[i]:V[i]V[i+1] of the polygon) {
#        if (E[i] crosses upward ala Rule #1)  {
#            if (P is  strictly left of E[i])    // Rule #4
#                 ++wn;   // a valid up intersect right of P.x
#        }
#        else
#        if (E[i] crosses downward ala Rule  #2) {
#            if (P is  strictly right of E[i])   // Rule #4
#                 --wn;   // a valid down intersect right of P.x
#        }
#    }
#    return wn;    // =0 <=> P is outside the polygon
#
#}
