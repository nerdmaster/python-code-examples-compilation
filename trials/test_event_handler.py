# -*- coding: utf-8 -*-
"""
File: test_event_handler.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: Test Event Handling in MatPlotLib
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""
'''
 Event name            Class and description
'button_press_event'   MouseEvent - mouse button is pressed
'button_release_event' MouseEvent - mouse button is released
'draw_event'           DrawEvent - canvas draw
'key_press_event'	     KeyEvent - key is pressed
'key_release_event'    KeyEvent - key is released
'motion_notify_event'	  MouseEvent - mouse motion
'pick_event'	           PickEvent - an object in the canvas is selected
'resize_event'	        ResizeEvent - figure canvas is resized
'scroll_event'	        MouseEvent - mouse scroll wheel is rolled
'figure_enter_event'	  LocationEvent - mouse enters a new figure
'figure_leave_event'	  LocationEvent - mouse leaves a figure
'axes_enter_event'	     LocationEvent - mouse enters a new axes
'axes_leave_event'	     LocationEvent - mouse leaves an axes

onHilite(ev)
Mouse event processor which highlights the artists under the cursor. Connect this to the ‘motion_notify_event’ using:

canvas.mpl_connect('motion_notify_event',canvas.onHilite)
onRemove(ev)
Mouse event processor which removes the top artist under the cursor. Connect this to the ‘mouse_press_event’ using:

canvas.mpl_connect('mouse_press_event',canvas.onRemove)

pick_event(mouseevent, artist, **kwargs)
This method will be called by artists who are picked and will fire off PickEvent callbacks registered listeners


'''

#from __future__ import print_function

import sys
import numpy as np
import matplotlib
#matplotlib.use("WxAgg")
#matplotlib.use("TkAgg")
#matplotlib.use("GTKAgg")
#matplotlib.use("Qt4Agg")
#matplotlib.use("CocoaAgg")
#matplotlib.use("MacOSX")
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
from matplotlib.text import Text
from matplotlib.image import AxesImage
import matplotlib.patches as patches
import matplotlib.path as mpath
from datetime import datetime
from numpy.random import rand



print("***** TESTING WITH BACKEND: %s"%matplotlib.get_backend() + " *****")

# - data points
#----------------------------------
t = np.arange(0.0, 2.0, 0.01)
y1 = 2*np.sin(2*np.pi*t)
y2 = 3*np.cos(2*np.pi*2*t)

def plot_data(ax):
    print ('starting ... creating data')
    t = np.arange(0.0, 2.0, 0.01)
    y1 = 2*np.sin(2*np.pi*t)
    y2 = 3*np.cos(2*np.pi*2*t)
    line1, = ax.plot(t, y1, lw=2, color='red', label='1 HZ')
    line2, = ax.plot(t, y2, lw=2, color='blue', label='2 HZ')

# - editable path
#----------------------------------
Path = mpath.Path

pathdata = [
    (Path.MOVETO, (1.58, -2.57)),
    (Path.CURVE4, (0.35, -1.1)),
    (Path.CURVE4, (-1.75, 2.0)),
    (Path.CURVE4, (0.375, 2.0)),
    (Path.LINETO, (0.85, 1.15)),
    (Path.CURVE4, (2.2, 3.2)),
    (Path.CURVE4, (3, 0.05)),
    (Path.CURVE4, (2.0, -0.5)),
    (Path.CLOSEPOLY, (1.58, -2.57)),
    ]

codes, verts = zip(*pathdata)
path = mpath.Path(verts, codes)
patch = patches.PathPatch(path, facecolor='green', edgecolor='yellow', alpha=0.5)


EventTypes = [u'resize_event', 
              u'draw_event', 
              u'key_press_event', 
              u'key_release_event', 
              u'button_press_event', 
              u'button_release_event', 
              u'scroll_event', 
              u'motion_notify_event', 
              u'pick_event', 
              u'idle_event', 
              u'figure_enter_event', 
              u'figure_leave_event', 
              u'axes_enter_event', 
              u'axes_leave_event', 
              u'close_event']

FileTypes  = {u'pgf': u'PGF code for LaTeX', 
              u'svgz': u'Scalable Vector Graphics', 
              u'tiff': u'Tagged Image File Format', 
              u'jpg': u'Joint Photographic Experts Group', 
              u'raw': u'Raw RGBA bitmap', 
              u'jpeg': u'Joint Photographic Experts Group', 
              u'png': u'Portable Network Graphics', 
              u'ps': u'Postscript', 
              u'svg': u'Scalable Vector Graphics', 
              u'eps': u'Encapsulated Postscript', 
              u'rgba': u'Raw RGBA bitmap', 
              u'pdf': u'Portable Document Format', 
              u'tif': u'Tagged Image File Format'}

class event_handler(object):
    _Events = dict()
    def __init__(self):
        self._Events = {'nothing'        : None,
                   'button_press_event'  : None,
                   'button_release_event': None,
                   'motion_notify_event' : None,
                   'draw_event'          : None,
                   'close_event'         : None,
                   'figure_enter_event'  : None,
                   'figure_leave_event'  : None,
                   'axes_enter_event'    : None,
                   'axes_leave_event'    : None,
                   'idle_event'          : None,
                   'key_press_event'     : None,
                   'key_release_event'   : None,
                   'pick_event'          : None,
                   'pick_event'          : None,
                   'pick_event'          : None,
                   'pick_event'          : None,
                   'pick_event'          : None,
                   'pick_event'          : None,
                   'pick_event'          : None,
                   'button_press_event'  : None}
    def add_event(self, new_event):
        if not new_event in self._Events:
            self._Events.append(new_event)
        pass
    def del_event(self, old_event):
        if  old_event in self._Events:
            self._Events.remove(old_event)
        pass

#<<<<<<<<<<<<<<<<<---------------------------------->>>>>>>>>>>>>>>>>#

class EventHandler:
#--|
    eventsList = dict()
    Canvas = None
    Nsteps = 100
    IdleCount = 0
    def __init__(self, canvas, eventTypes = None, eventFunctions = None):
        self.Canvas = canvas
        self.eventQueue = list()
        self.eventsID = list()
        if not eventTypes:
            print 'Created default event handler methods'
            self.addEvent('resize_event', self.On_Resize)
            self.addEvent('draw_event', self.On_Draw)
            self.addEvent('key_press_event', self.On_KeyDown)
            self.addEvent('key_release_event', self.On_KeyUp)
            self.addEvent('button_press_event', self.On_MouseDown)
            self.addEvent('button_release_event', self.On_MouseUp)
            self.addEvent('scroll_event', self.On_Scroll)
            self.addEvent('motion_notify_event', self.On_MouseMove)
            self.addEvent('pick_event', self.On_Pick)
            self.addEvent('idle_event', self.On_Idle)
            self.addEvent('figure_enter_event', self.On_EnterFigure)
            self.addEvent('figure_leave_event', self.On_LeaveFigure)
            self.addEvent('axes_enter_event', self.On_EnterAxes)
            self.addEvent('axes_leave_event', self.On_LeaveAxes)
            self.addEvent('close_event', self.On_Close)
        else:
            for eType, eFunc in eventTypes:
                eID = self.Canvas.mpl_connect(eType, eFunc)
                self.eventQueue.append([eType, eID]) 
        self.pressevent = None
        print 'adding event handlers <- callback functions...'
#--|
    def addEvent(self, eventType, eventHandlerFunction = None):
        if eventType not in EventTypes:
            print('Warning:: Unknown event requested', eventType)
        elif eventType in self.eventsList:
            print('Warning:: Event already in eventsList. Remove before adding')
        else:
            eID = self.Canvas.mpl_connect(eventType, eventHandlerFunction)
            self.eventQueue.append([eventType, eID]) 
#--|
    def getEventIndex(self, eventType):
        if eventType in self.eventQueue:
            return self.eventQueue.remove(self.eventQueue[eventType]) 
        else:
            print('Warning:: Trying to remove an event not in the list')
#--|
    def removeEvent(self, event):
        if event in self.eventQueue:
            self.eventQueue.remove(self.eventQueue[event]) 
            self.Canvas.mpl_disconnect(event)
        else:
            print('Warning:: Trying to remove an event not in the list')
#--|
    def unknownEvent(self, event):
        pass
#--|
    def onpress(self, event):
        if event.inaxes!=ax_topRight:
            return
        if not circ.contains(event)[0]:
            return
        self.pressevent = event
#--|
    def onrelease(self, event):
        self.pressevent = None
        self.x0, self.y0 = circ.center
#--|
    def onmove(self, event):
        if self.pressevent is None or event.inaxes!=self.pressevent.inaxes:
            return

        dx = event.xdata - self.pressevent.xdata
        dy = event.ydata - self.pressevent.ydata
        circ.center = self.x0 + dx, self.y0 + dy
        line.set_clip_path(circ)
        self.canvas.draw()
#--|
#    def whichCanvas(mouse):
#        where = pointInRegion(ax_array)
#        if not where:
#            return None
#        else:
#            return where
#--|
#    def pick_handler(self, event):
#        mouseevent = event.mouseevent
#        artist = event.artist
#        # now do something with this...
#--|    
    def On_MouseDown(self, event):
        if event.dblclick:
            print("DBLCLICK", event)
        else:
            print("DOWN    ", event)
        canvas = event.figure.canvas
        axes = event.axes
        print('you pressed', event.button, event.xdata, event.ydata)
        print('It is in (canvas, axes): ', (canvas, axes))
#--|    
    def On_MouseUp(self, event):
        print("UP      ", event)
#--|    
    def On_MouseMove(self, event):
        plt.title(datetime.now())
        event.canvas.draw()
#--|    
    def update_title(fig):
        plt.title(datetime.now())
        plt.draw()
#--|    
    def On_Draw(self, event):
        timer.start()
        fig.canvas.mpl_disconnect(did_draw)
#        self.removeEvent(event)
#--|    
    def On_Close(self, event):
#        self.Canvas.close()
        print('Closed Figure!')
#--|    
    def On_EnterAxes(self, event):
        print('enter_axes', event.inaxes)
        event.inaxes.patch.set_facecolor('yellow')
        print 'event.inaxes :: ', event.inaxes
    #    plot_data(ax_topLeft)
        event.canvas.draw()
#--|    
    def On_LeaveAxes(self, event):
        print('leave_axes', event.inaxes)
        event.inaxes.patch.set_facecolor('white')
        event.canvas.draw()
#--|    
    def On_EnterFigure(self, event):
        print('enter_figure', event.canvas.figure)
        event.canvas.figure.patch.set_facecolor('red')
        event.canvas.draw()
#--|    
    def On_LeaveFigure(self, event):
        print('leave_figure', event.canvas.figure)
        event.canvas.figure.patch.set_facecolor('grey')
        event.canvas.draw()
    
    def On_Idle(self, event):
        self.IdleCount +=1
        print('idle', self.IdleCount)
        line1.set_ydata(2*np.sin(2*np.pi*t*(self.Nsteps - \
                      self.IdleCount)/float(self.Nsteps)))
        event.canvas.draw()
        # test boolean return removal
        if self.IdleCount == self.Nsteps:
            self.IdleCount = 0
            return False
        return True
#--|
    def On_KeyDown(self, event):
        print('pressed: ', event.key)
        sys.stdout.flush()
        if event.key=='x':
            visible = ax_bottomLeft.get_visible()
            ax_bottomLeft.set_visible(not visible)
            fig.canvas.draw()
#--|
    def On_KeyUp(self, event):
        print('released: ', event.key)
        sys.stdout.flush()
        if event.key=='x':
            visible = ax_bottomLeft.get_visible()
            ax_bottomLeft.set_visible(visible)
            fig.canvas.draw()
        if event.key=='q':
            exit()
#--|
    def On_Pick(self, event):
        # on the pick event, find the orig line corresponding to the
        # legend proxy line, and toggle the visibility
        canvas = event.figure.canvas
        axes = event.axes
        artist = event.artist #<- in case the artist picked is not a line!?
        legline = event.artist
        origline = lined[legline]
        vis = not origline.get_visible()
        origline.set_visible(vis)
        # Change the alpha on the line in the legend so we can see what lines
        # have been toggled
        if vis:
            legline.set_alpha(1.0)
        else:
            legline.set_alpha(0.2)
        fig.canvas.draw()
        
        
    def onpick1(event):
        if isinstance(event.artist, Line2D):
            thisline = event.artist
            xdata = thisline.get_xdata()
            ydata = thisline.get_ydata()
            ind = event.ind
            print('onpick1 line:', zip(np.take(xdata, ind), np.take(ydata, ind)))
        elif isinstance(event.artist, Rectangle):
            patch = event.artist
            print('onpick1 patch:', patch.get_path())
        elif isinstance(event.artist, Text):
            text = event.artist
            print('onpick1 text:', text.get_text())
#--|
    def onpick2(event):
        print('onpick2 line:', event.pickx, event.picky)
#--|
    def onpick3(event):
        ind = event.ind
        print('onpick3 scatter:', ind, np.take(x, ind), np.take(y, ind))
#--|
    def On_Resize(self, event):
        print('event not implemented: ', event.key)
        sys.stdout.flush()
#--|
    def On_Scroll(self, event):
        print('event not implemented: ', event.key)
        sys.stdout.flush()

#<<<<<<<<<<<<<<<<<---------------------------------->>>>>>>>>>>>>>>>>#
print 'starting up...'

# Setup plot and callbacks.
#plt.subplot(111, aspect='equal')
#fig = plt.gcf()

fig, ax_array = plt.subplots(2, 2)
[[ax_topLeft,    ax_topRight],
 [ax_bottomLeft, ax_bottomRight]] = np.reshape(ax_array, (2,2))

handler = EventHandler(fig.canvas)

fig.suptitle('mouse hover over figure or axes to trigger events')

# Create a new timer object. Set the interval to 100 milliseconds
# (1000 is default) and tell the timer what function should be called.
print ('starting timer...')
timer = fig.canvas.new_timer(interval=500)
timer.add_callback(handler.update_title(), fig)
timer.start()

print 'adding event handlers <- callback functions...'
#-----------------
cid_up   = fig.canvas.mpl_connect('button_press_event', OnClick)
cid_down = fig.canvas.mpl_connect('button_release_event', OnRelease)
cid_move = fig.canvas.mpl_connect('motion_notify_event', OnMotion)
#-----------------
id_close = fig.canvas.mpl_connect('close_event', handle_close)
#-----------------
did_draw = fig.canvas.mpl_connect('draw_event', start_timer)
#-----------------
gid_f_in  = fig.canvas.mpl_connect('figure_enter_event', enter_figure)
gid_f_out = fig.canvas.mpl_connect('figure_leave_event', leave_figure)
gid_a_in  = fig.canvas.mpl_connect('axes_enter_event', enter_axes)
gid_a_out = fig.canvas.mpl_connect('axes_leave_event', leave_axes)
#-----------------
cid_idle  = fig.canvas.mpl_connect('idle_event', on_idle)
#-----------------
kid_press = fig.canvas.mpl_connect('key_press_event', press)
#-----------------
cid_pick  = fig.canvas.mpl_connect('pick_event', onpick)
#-----------------
#-----------------
#-----------------

plt.gca().text(0.5, 0.5, "Click on the canvas to test mouse events.",
               ha="center", va="center")

print ('plotting ...')

# <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> #

''' top-left subplot'''
ax_topLeft.set_title('Click on legend line to toggle line on/off')
line1, = ax_topLeft.plot(t, y1, lw=2, color='red', label='1 HZ')
line2, = ax_topLeft.plot(t, y2, lw=2, color='blue', label='2 HZ')
leg = ax_topLeft.legend(loc='upper left', fancybox=True, shadow=True)
leg.get_frame().set_alpha(0.4)
lines = [line1, line2]
lined = dict()

for legline, origline in zip(leg.get_lines(), lines):
    legline.set_picker(5)  # 5 pts tolerance
    lined[legline] = origline

# <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> #

'''   top-right subplot   '''
"""      path editor      """
ax_topRight.add_patch(patch)

# <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> #

''' bottom-left subplot'''
circ = patches.Circle( (0.5, 0.5), 0.25, alpha=0.8, fc='yellow')
ax_bottomLeft.add_patch(circ)

x, y = np.random.rand(2, 200)
ax_bottomLeft.plot(x, y, alpha=0.2)
line, = ax_bottomLeft.plot(x, y, alpha=1.0, clip_path=circ)


# <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> #

'''     bottom-right subplot     '''
""" lasso demo + add/remove pts. """

x, y, c, s = rand(4, 100)
col = ax_bottomRight.scatter(x, y, 100*s, c, picker=True)
fig.canvas.mpl_connect('pick_event', onpick3)

line, = ax_bottomRight.plot(np.random.rand(12), np.random.rand(12), 'go', picker=5) # 5 points tolerance
ax_bottomRight.set_title('click on points, rectangles or text', picker=True)
ax_bottomRight.set_ylabel('ylabel', picker=True, bbox=dict(facecolor='red'))

divider = make_axes_locatable(ax_bottomRight)

# pick the rectangle
bars = ax_bottomRight.bar(range(10), rand(10), picker=True)
for label in ax_bottomRight.get_xticklabels():  # make the xtick labels pickable
    label.set_picker(True)








# <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> #
# <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> #
# <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> #
# <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> #


plt.show()

print('... done!')


"""
from matplotlib.patches import Rectangle

# We just subclass Rectangle so that it can be called with an Axes
# instance, causing the rectangle to update its shape to match the
# bounds of the Axes
class UpdatingRect(Rectangle):
    def __call__(self, ax):
        self.set_bounds(*ax.viewLim.bounds)
        ax.figure.canvas.draw_idle()

# A class that will regenerate a fractal set as we zoom in, so that you
# can actually see the increasing detail.  A box in the left panel will show
# the area to which we are zoomed.
class MandlebrotDisplay(object):
    def __init__(self, h=500, w=500, niter=50, radius=2., power=2):
        self.height = h
        self.width = w
        self.niter = niter
        self.radius = radius
        self.power = power

    def __call__(self, xstart, xend, ystart, yend):
        self.x = np.linspace(xstart, xend, self.width)
        self.y = np.linspace(ystart, yend, self.height).reshape(-1,1)
        c = self.x + 1.0j * self.y
        threshold_time = np.zeros((self.height, self.width))
        z = np.zeros(threshold_time.shape, dtype=np.complex)
        mask = np.ones(threshold_time.shape, dtype=np.bool)
        for i in range(self.niter):
            z[mask] = z[mask]**self.power + c[mask]
            mask = (np.abs(z) < self.radius)
            threshold_time += mask
        return threshold_time

    def ax_update(self, ax):
        ax.set_autoscale_on(False) # Otherwise, infinite loop

        #Get the number of points from the number of pixels in the window
        dims = ax.axesPatch.get_window_extent().bounds
        self.width = int(dims[2] + 0.5)
        self.height = int(dims[2] + 0.5)

        #Get the range for the new area
        xstart,ystart,xdelta,ydelta = ax.viewLim.bounds
        xend = xstart + xdelta
        yend = ystart + ydelta

        # Update the image object with our new data and extent
        im = ax.images[-1]
        im.set_data(self.__call__(xstart, xend, ystart, yend))
        im.set_extent((xstart, xend, ystart, yend))
        ax.figure.canvas.draw_idle()

md = MandlebrotDisplay()
Z = md(-2., 0.5, -1.25, 1.25)

fig1, (ax1, ax2) = plt.subplots(1, 2)
ax1.imshow(Z, origin='lower', extent=(md.x.min(), md.x.max(), md.y.min(), md.y.max()))
ax2.imshow(Z, origin='lower', extent=(md.x.min(), md.x.max(), md.y.min(), md.y.max()))

rect = UpdatingRect([0, 0], 0, 0, facecolor='None', edgecolor='black')
rect.set_bounds(*ax2.viewLim.bounds)
ax1.add_patch(rect)

# Connect for changing the view limits
ax2.callbacks.connect('xlim_changed', rect)
ax2.callbacks.connect('ylim_changed', rect)

ax2.callbacks.connect('xlim_changed', md.ax_update)
ax2.callbacks.connect('ylim_changed', md.ax_update)

plt.show()


"""


