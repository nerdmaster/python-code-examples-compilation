from scipy.integrate import odeint

class PendulumFunctor:
    """
    A functor (an object called as a function) to represent the pendulum.
    """
    def __init__(self, parameters):
        # bind parameters to a local name for later access
        pass

    def __call__(self, time, state):
        # compute state derivatives from the equations of motion, using
        # the parameters bound to this instance
        pass


def use_functor(duration, initial_condition, parameters):
    """
    Instantiate the class and pass the instance to odeint as a function,
    which works because __call__ is defined.
    """
    system = PendulumFunctor(parameters)
    results = odeint(system, duration, initial_condition)
    # display results


#########################################

def pendulum(time, state, parameters):
    """
    A function to represent the pendulum.
    """
    # compute state derivatives from the equation of motion, using
    # the parameters passed in as an argument
    pass


def use_function(duration, initial_condition, parameters):
    """
    Pass the function to odeint.
    """
    results = odeint(pendulum, duration, initial_condition, parameters)
    # display results


########################################

class PendulumMethod:
    """
    A class to represent the pendulum, with a method for the state equations.
    """
    def __init__(self, parameters):
        # same as above
        pass

    def ode(self, time, state):
        # same as __call__ in the functor
        pass


def use_method(duration, initial_condition, parameters):
    """
    Instantiate the class with the given parameters, and pass a method bound
    to the instance to odeint.
    """
    system = PendulumMethod(parameters)
    results = odeint(system.ode, duration, initial_conditions)
    # display results
