from threading import Thread
from time import sleep
from PythonCard import model
import urllib
import re


def get_quote(symbol):
        global quote
        base_url = 'http://finance.google.com/finance?q='
        content = urllib.urlopen(base_url + symbol).read()
        m = re.search('class="pr".*?>(.*?)<', content)
        if m:
            quote = m.group(1)
        else:
            quote = 'N/A'
        return quote

def get_change(symbol):
    global change
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('class="chg".*?>(.*?)<', content)
    if m:
        change = m.group(1)
    else:
        change = 'N/A'
    return change

def get_open(symbol):
    global opens
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?op".*?>(.*?)<', content)
    if m:
        opens = m.group(1)
    else:
        opens = 'N/A'
    return opens

def get_high(symbol):
    global high
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?hi".*?>(.*?)<', content)
    if m:
        high = m.group(1)
    else:
        high = 'N/A'
    return high

def get_high52(symbol):
    global high52
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?hi52".*?>(.*?)<', content)
    if m:
        high52 = m.group(1)
    else:
        high52 = 'N/A'
    return high52

def get_low(symbol):
    global low
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?lo".*?>(.*?)<', content)
    if m:
        low = m.group(1)
    else:
        low = 'N/A'
    return low

def get_vol(symbol):
    global vol
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?vo".*?>(.*?)<', content)
    if m:
        vol = m.group(1)
    else:
        vol = 'N/A'
    return vol

def get_mc(symbol):
    global mc
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?mc".*?>(.*?)<', content)
    if m:
        mc = m.group(1)
    else:
        mc = 'N/A'
    return mc

def get_lo52(symbol):
    global lo52
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?lo52".*?>(.*?)<', content)
    if m:
        lo52 = m.group(1)
    else:
        lo52 = 'N/A'
    return lo52

def get_pe(symbol):
    global pe
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?lo52".*?>(.*?)<', content)
    if m:
        pe = m.group(1)
    else:
        pe = 'N/A'
    return pe

def get_beta(symbol):
    global beta
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?beta".*?>(.*?)<', content)
    if m:
        beta = m.group(1)
    else:
        beta = 'N/A'
    return beta

def get_div(symbol):
    global div
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?div".*?>(.*?)<', content)
    if m:
        div = m.group(1)
    else:
        div = 'N/A'
    return div

def get_yield(symbol):
    global yield1
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?yield".*?>(.*?)<', content)
    if m:
        yield1 = m.group(1)
    else:
        yield1 = 'N/A'
    return yield1

def get_shares(symbol):
    global shares
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?shares".*?>(.*?)<', content)
    if m:
        shares = m.group(1)
    else:
        shares = 'N/A'
    return shares

def get_own(symbol):
    global own
    base_url = 'http://finance.google.com/finance?q='
    content = urllib.urlopen(base_url + symbol).read()
    m = re.search('".*?own".*?>(.*?)<', content)
    if m:
        own = m.group(1)
    else:
        own = 'N/A'
    return own

class stock(model.Background):

    def on_getQuote_mouseClick(self, event):
        global symbol
        symbol = self.components.quotese.text
        class Repeater(Thread):
                def __init__(self,interval,fun,*args,**kw):
                        Thread.__init__(self)
                        self.interval=interval
                        self.fun=fun
                        self.args=args
                        self.kw=kw
                        self.keep_going=True

                def run(self):
                       while(self.keep_going):
                            sleep(self.interval)
                            self.fun(*self.args,**self.kw)



        def Refresh(*a):
                get_quote(symbol)
                get_change(symbol)
                self.components.current.text = quote
                self.components.change.text = change

        r=Repeater(1.0, Refresh)
        r.start()



    def on_stockinfo_mouseClick(self, event):
        global symbol
        symbol = self.components.quotese.text
        get_open(symbol)
        get_high(symbol)
        get_high52(symbol)
        get_low(symbol)
        get_vol(symbol)
        get_mc(symbol)
        get_lo52(symbol)
        get_pe(symbol)
        get_beta(symbol)
        get_div(symbol)
        get_yield(symbol)
        get_shares(symbol)
        get_own(symbol)
        self.components.inst.text = own
        self.components.shares.text = shares
        self.components.yield1.text = yield1
        self.components.div.text = div
        self.components.beta.text = beta
        self.components.pe.text = pe
        self.components.lo52.text = lo52
        self.components.mkt.text = mc
        self.components.vol.text = vol
        self.components.opens.text = opens
        self.components.high.text = high
        self.components.hi52.text = high52
        self.components.low.text = low

    def on_save_mouseClick(self, event):
        stock1 = open('stock1.txt', 'w')
        stock1.write(self.components.stock1.text)
        stock1.close()
        stock2 = open('stock2.txt', 'w')
        stock2.write(self.components.stock2.text)
        stock2.close()
        stock3 = open('stock3.txt', 'w')
        stock3.write(self.components.stock3.text)
        stock3.close()
        stock4 = open('stock4.txt', 'w')
        stock4.write(self.components.stock4.text)
        stock4.close()

    def on_load_mouseClick(self, event):
        load1 = open('stock1.txt' , 'r').read()
        self.components.stock1.text = load1

        load2 = open('stock2.txt' , 'r').read()
        self.components.stock2.text = load2

        load3 = open('stock3.txt' , 'r').read()
        self.components.stock3.text = load3

        load4 = open('stock4.txt' , 'r').read()
        self.components.stock4.text = load4

    def on_update_mouseClick(self, event):
        symbol = self.components.stock1.text
        get_quote(symbol)
        self.components.change1.text = quote

        symbol = self.components.stock2.text
        get_quote(symbol)
        self.components.change2.text = quote

        symbol = self.components.stock3.text
        get_quote(symbol)
        self.components.change3.text = quote

        symbol = self.components.stock4.text
        get_quote(symbol)
        self.components.change4.text = quote

    def on_clear_mouseClick(self, event):
        self.components.stock1.text = ""
        self.components.stock2.text = ""
        self.components.stock3.text = ""
        self.components.stock4.text = ""
        self.components.change1.text = ""
        self.components.change2.text = ""
        self.components.change3.text = ""
        self.components.change4.text = ""

if __name__ == '__main__':
    app = model.Application(stock)
    app.MainLoop()

