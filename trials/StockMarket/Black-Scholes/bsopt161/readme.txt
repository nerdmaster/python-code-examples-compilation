!Options  - The Black-Scholes Option Pricing Model Add-In for Excel 
Unregistered Version  1.61
by Ray Steele

********************************************************
The file readme16.xls has the same (and more) information in an Excel document.
I advise viewing it instead of this.
If you do not have Excel 5.0 or higher, you cannot use the add-in anyway
********************************************************


Notice: This program is Shareware!  You may evaluate if for a period of no more than 30 days. After this time you must either register (read REGISTERING below) or remove it from your system.
Failure to comply with this condition is a violation of United States and international copyright law.  This program is fully functional but you will sleep better if you register.  Please register if you use !Options.

TABLE OF CONTENTS
1)  What is !Options?
2)  What do I need to use !Options?
3)  Tell me more about what !Options does!
4)  How do I install !Options?
5)  What if I like !Options?
6)  How do I register !Options?
7)  What if I have question about !Options?

1)  What is !Options?

!Options is an Excel shareware add-in that adds functionality for Black-Scholes calls, puts (using put-call parity), call delta (hedge ratio), and determines the implied standard deviation of a call.
They are available through function wizard (under All or Financial) with simple on-line help. 

2)  What do I need?

You will need Microsoft Excel 5.0 (or higher) for Windows and a little finance knowledge.

3)  Tell me more about what !Options does!

The unregistered shareware version adds four (4) functions and a help file to Excel.  All of these functions are in full, uncrippled form.  They are...

BS_CALL - Syntax: BS_CALL(STOCK, EXERCISE, DAYS, RISKFREE, STDDEV)
          Calculates the theoretical price of a European call option"
BS_CSTD - Syntax: BS_CSTD(STOCK, EXERCISE, DAYS, RISKFREE, CALL_PRICE)
          Calculates the implied standard deviation of a European call option"
BS_CDELTA - Syntax: BS_CDELTA(STOCK, EXERCISE, DAYS, RISKFREE, STDDEV)
          Calculates the delta (hedge ratio) of European call option"
BS_PUT - Syntax: BS_PUT(STOCK, EXERCISE, DAYS, RISKFREE, STDDEV)
          Calculates the theoretical price of a European put option"

             Function Wizard:
Note: If you are unfamiliar with the function wizard, it is worth learning.  It is NOT the fastest way to write formulas, but it is  painless.  Click on the button that looks like the one above, for a list of the functions.           
The Black-Scholes functions are available under "all" and "financial".  Select the one you want and click on the help button for on line help.  (Alternatively, you may select Insert - Function to do the same thing)

When you register, you will receive !Options 2.0.  It adds several functions to the aforementioned list.  
Additional functions in the registered version of !Options follow.

BS_CGAMMA     Calculates the gamma of a European call"
BS_CTHETA     Calculates the theta of a European call"
BS_CVEGA      Calculates the vega of a European call"
BS_CRHO       Calculates the rho of a European call"
BS_PSTD       Calculates the implied standard deviation of a European put"
BS_PDELTA     Calculates the delta (hedge ratio) of a European put"
BS_PGAMMA     Calculates the gamma of a European put"
BS_PTHETA     Calculates the theta of a European put"
BS_PVEGA      Calculates the vega of a European put"
BS_PRHO       Calculates the rho of a European put"

It also has context sensitive online help.


4)  How do I install !Options?

In the interest of simplicity I have not included an install program. You get three files in the shareware version.

You are currently reading the following file which should be kept for registration.

You should copy the !options.xla file to the ..\EXCEL\LIBRARY

You should copy the !options.hlp file to the ..\EXCEL

"Installation Instructions 
1. Copy the files above to the appropriate subdirectories.
2. Run Microsoft Excel
3. On the Tools menu, select Add-Ins.
4. Check the !Options by Ray Steele checkbox.
5. Click OK.
6) You may now use the functions like any others."

5)  What if I like !Options?

If you use the program for more than 30 days, see #6 REGISTERING.  You will receive additional functions and context sensitive help.

6. How do I register !Options?

Fill out the form on the "Registration Form ot Excel spreadsheet "readme16.xls" sheet and return it with your payment.  You may choose to receive the registered version electronically or through snail mail.  There is no additional charge for electronic transfer.  You may also print the form from http://pilot.msu.edu/user/steelera/register.html

Pricing:

$25 US = single registration (for delivery via e-mail or FTP)
$30 US = single registration (snail mail delivery within US)
$35 US = single registration (snail mail delivery outside of US)
Multi-user discount available upon request.

Make checks or money orders payable to:
          Ray Steele
          100 Park West Drive #2G-10
          Lansing, MI  48917"

7)  What if I have question about !Options?

For problems, questions, or suggestions please try one of the following

1)  Go to the !Options homepage for FAQs, 
          http://pilot.msu.edu/user/steelera/faqs.html

2)  send e-mail to the following address:
          steelera@pilot.msu.edu

3)  send snail mail to the above address