# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""


import csv
import sys

print 'starting ...'

filename = 'cvs_dict.csv'
f = open(filename, 'wt')
try:
    fieldnames = ('fecha', 'numero', 'letra')
    writer = csv.DictWriter(f, fieldnames=fieldnames)
    headers = dict( (n,n) for n in fieldnames )
    writer.writerow(headers)
    for i in range(10):
        writer.writerow({ 'fecha':'08/%02d/07' % (i+1),
                          'numero':'%02d' % (i+1),
                          'letra':chr(ord('a') + i),
                          })
finally:
    f.close()

print ' file written. Now reading (blind)'

print open(filename, 'rt').read()

print 'now reading as DictReader ...'

f = open(filename, 'rt')
try:
    reader = csv.DictReader(f)
    for row in reader:
        print row
    print row['fecha']
    print row['numero']
    print row['letra']
finally:
    f.close()
    
print '... done!'
