function symbols=get_symbols(symbolfile,symbolpath)
%
% This function return all symbols from a symbol file
% A symbol file is a simple text file contain a stock symbol at each line
% EXAMPLE:
% symbols=get_symbols('all_symbols.txt');
%
% If you have MATLAB, you can just use "importdata(symbolfile)" to get job done.
% If you run this script in MATLAB, please change fgetline to fgetl
%
% 12-Feb-2010, added deblank to make sure no space
%
%   Copyright 2010 EdgeMe
%   EdgeMe, 10-Feb-2010
%
%
    symbols=[];
    if nargin<2, symbolpath='c:\relativestrength\list'; end

    datafile=[symbolpath,'\',symbolfile];

     if exist(datafile)>0

         fid = fopen(datafile);
         while (~feof(fid))
             temp=fgetline(fid); % if run in the MATLAB, use fgetl(fid)
             newlinestart= regexp(temp,'\n') ;%get the line, but the return may contains a empty new line

             if isempty(newlinestart) %means no empty new line
                    symbols{end+1} =deblank(temp);
             else

                    symbols{end+1} =deblank(temp(1:newlinestart-1)); % only save the content without the empty new line
             end
         end
     else
        warning(['Could not find the file ',symbolfile]);
         return;
     end

    end