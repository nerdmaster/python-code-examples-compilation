function [dates opens highs lows closes volumes]=get_symbol_data(symbol,days)
%
% This function returns stock data array by input stock file .csv file and number of days
%
%
% If you have MATLAB, you can use "importdata" function to get the job done easily.
% This script can be run both in MATLAB and in FreeMAT
% If you run this script in MATLAB, please change fgetline to fgetl
%
% 14-feb-2010, notice the zero volume day is in dataset, we have to get rid of these days
%
%   Copyright 2010 EdgeMe
%   EdgeMe, 10-Feb-2010
%
%
dates=[];
    opens=[];
    highs=[];
    closes=[];
    lows=[];
    volumes=[];
    %this is our data file path, change to your if different
    datapath='c:\relativestrength\temp';
    symbolfile=[symbol,'.csv'];
    datafile=[datapath,'\',symbolfile];

    extradays=10; %some days are holidays, but data file still have entry;

    if exist(datafile)>0
     rawdata=[];
     fid = fopen(datafile);
     d=0;
     while (d<days+extradays)
         rawdata{end+1} = fgetline(fid);% if run in the MATLAB, use fgetl(fid)
         d=d+1;
     end
     fclose(fid);
     totaldays=numel(rawdata);

     for i=0:totaldays-1
         % since the first data is the latest data, so we treat the oldest data first
         if ~isempty(rawdata{totaldays-i})
         temp=split_str_by_comma(rawdata{totaldays-i});

         if isempty(temp)
             return;
         end
         if str2num(temp{6})>0 %make sure we have real data
         dates{end+1}=temp{1};
         opens(end+1)=str2num(temp{2});
         highs(end+1)=str2num(temp{3});
         lows(end+1)=str2num(temp{4});
         closes(end+1)=str2num(temp{5});
         volumes(end+1)=str2num(temp{6});
         end
         end
     end
     if numel(dates)>days
         dates=dates(end-days+1:end);
         opens=opens(end-days+1:end);
         highs=highs(end-days+1:end);
         lows=lows(end-days+1:end);
         closes=closes(end-days+1:end);
         volumes=volumes(end-days+1:end);
     end

    else
      warning(['No data file found for ',symbol]);
      return;
    end


end