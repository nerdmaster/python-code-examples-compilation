%
% This script is to generate a watch list according to your magic formula criteria
%

% to get list of symbols we want to choose from
symbols=get_symbols('all_symbols.txt');

% initialize the result list
myList=[];

% now loop thru all symbols
for i=1:numel(symbols)

    % get data for each symbol
    [dates opens highs lows closes volumes]=get_symbol_data(symbols{i},2);

    % for this example, we will generate a list of stock that move up 2% for the day
    % so we need only 2 days of data; change the criteria of your own if you wish.

    % make sure we have data for the symbol
    if (~isempty(closes))
       % now let's find out if the stock meet our 2% up day
        if (100*(closes(2)-closes(1)))/closes(1) >= 2
        %ok, this one is up 2%
        % save into the list
          myList{end+1}=symbols{i};
        end
    end

end

% now we are here, myList should contain all the stock that met our criteria.
% we can save into a HTML file with a hyperlink to a charting website
if ~isempty(myList) %make sure we have at least one stock that met our criteria to save to a file
 save2html('myList.html',myList,'My list per my magic formula'); % generate a HTML file
 disp('Your magic stock list is generated in myList.html');
else
 disp('Sorry, No stock met your magic formula');
end