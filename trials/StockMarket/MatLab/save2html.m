function save2html(htmlfile,cells,title)
%
%   This script is to save cells into a table format html file
%
%   Copyright 2010 EdgeMe
%   EdgeMe, 10-Feb-2010
%
%

fid = fopen(htmlfile,'w');
fprintf(fid,['<table style="color:black; font-size:20; font-family:Times;" cellpadding="0" cellspacing="0" border="1">']);

fprintf(fid,['<title>',title,'</title>']);

[r c] = size(cells);

if r==1 && c>1 % transpose the cells
    cells=cells';
end

[r c] = size(cells);

for j=1:r; % for each row

 fprintf(fid,'<tr>');

 for i=1:c; %for each column

  temp = cells{j,i};
        if ~isempty(temp)
       if isnumeric(temp) && ~isnan(temp);
       td = num2str(temp);
        elseif ischar(temp)
       td = temp;
             end

        else
            td = '&nbsp;';
        end

        fprintf(fid,['<td>','<a href="http://www.finviz.com/quote.ashx?t=',td,'" target="_blank">',td,'</td>']);

 end

 fprintf(fid,'</tr>');

end


fprintf(fid,'</table>');
fclose(fid);

end