function dataout = ema(datain,period)
%
% This function is to get EMA of a given datain and a given period
%
%   Copyright 2010 EdgeMe
%   EdgeMe, 10-Feb-2010
%
%
    f = 2/(period+1);
    N = numel(datain);
    dataout = zeros(N,1);
    dataout(1) = datain(1);
    for i=2:N
    dataout(i) = f*(datain(i)-dataout(i-1)) + dataout(i-1);
    end

end