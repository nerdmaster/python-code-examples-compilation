# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""

import sys
import csv
from dateutil import parser
import matplotlib.pyplot as plt
import numpy as np

print 'starting ...'
csv.register_dialect('pipes', delimiter='|')
csv.register_dialect('escaped', escapechar='\\', doublequote=False, quoting=csv.QUOTE_NONE)
csv.register_dialect('singlequote', quotechar="'", quoting=csv.QUOTE_ALL)
print csv.list_dialects()

CSV_file = 'AAPL[2000-2014].csv'
'''Date,Open,High,Low,Close,Volume,Adj_Close'''

"""<<<<<----------------------------------------------->>>>>"""

with open(CSV_file, 'rt') as csvfile:
    dialect = csv.Sniffer().sniff(csvfile.read(10))
    csvfile.seek(0)
    reader = csv.reader(csvfile, dialect)
    headers = reader.next()
    print 'headers are:\n', headers
    try:
        index, data, Data = 0, {}, []
        for row in reader:
            _date,_open,_high,_low,_close,_volume,_adj_close = row
            Date,Open,High,Low,Close,Volume,Adj_Close = parser.parse(_date), \
            float(_open), float(_high), float(_low), float(_close), int(_volume),\
            float(_adj_close)
            Data.append([Open,High,Low,Close,Volume,Adj_Close])
            data[index] = Date,Open,High,Low,Close,Volume,Adj_Close
            index += 1
        Npoints = len(data)
        print Npoints, index
        print 'first entry-->',data[data.keys()[-1]]
        print 'last  entry-->',data[data.keys()[0]]
    except csv.Error as e:
        sys.exit('file %s, line %d: %s' % (csvfile, reader.line_num, e))

    finally:
        csvfile.close()

print 'file read, ploting ...'
datum = np.fliplr(np.transpose(Data))
plt.plot(datum[0])
plt.show()
print ' ... plot done ...'

#sys.exit() # quit

"""<<<<<----------------------------------------------->>>>>"""

#f = open(CSV_file, 'rt')
#try:
#    reader = csv.reader(f)
#    for row in reader:
#        print row
#finally:
#    f.close()

"""<<<<<----------------------------------------------->>>>>"""

f = open('CSV_test.csv', 'wt')
try:
    writer = csv.writer(f)
    writer.writerow( ('numero', 'letra', 'fecha') )
    for i in range(10):
        writer.writerow( ('%2d' % (i+1), chr(ord('a') + i), '08/%02d/2014' % (i+1)) )
finally:
    f.close()

"""<<<<<----------------------------------------------->>>>>"""

print open('CSV_test.csv', 'rt').read()

"""<<<<<----------------------------------------------->>>>>"""

#with open('testdata.pipes', 'r') as f:
#    reader = csv.reader(f, dialect='pipes')
#    for row in reader:
#        print row
        
"""<<<<<----------------------------------------------->>>>>"""

"""<<<<<----------------------------------------------->>>>>"""

"""<<<<<----------------------------------------------->>>>>"""

"""<<<<<----------------------------------------------->>>>>"""

print '... done!'

