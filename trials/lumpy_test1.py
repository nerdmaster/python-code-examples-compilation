# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
#from simpleGraphics import *
import matplotlib.pyplot as plt
import copy
from scitools.Lumpy import Lumpy
from swampy import *
from math import *
import numpy as np

lumpy = Lumpy()
lumpy.make_reference()

def countdown(n):
    if n <= 0:
        print('BastFoff!')
        lumpy.object_diagram()
    else:
        print(n)
        countdown(n-1)   

# run the test code
x = [1, 2, 3]
y = x
z = list(x)

message = 'And now something different'
n = 15
pi = 3.141593

objeto = (message, n, pi)
print('object: ', objeto)

countdown(3)

lumpy.object_diagram()

