# -*- coding: utf-8 -*-
"""
Created on Thu Sep 11 15:41:10 2014

@author: benito
"""
#------------------------------------------#
#%%   <--  cell separator for Spyder       #
#------------------------------------------#
# use turtle to draw
#------------------------------------------#
import turtle

turtlename = turtle.Turtle()
def Tsquare(turtlename):
    turtlename.forward(90)
    turtlename.right(90)
    turtlename.forward(90)
    turtlename.right(90)
    turtlename.forward(90)
    turtlename.right(90)
    turtlename.forward(90)

def createSquare(turtlename,direction):
    for n in range(1, 5):
        turtlename.forward(90)
        turtlename.right(90)

wn = turtle.Screen()
wn.reset
turtlename.right(45)
createSquare(turtlename,'right')
#Tsquare(turtlename)
#
#turtlename.forward(90)
#turtlename.left(90)
#turtlename.forward(90)
#turtlename.left(90)
#turtlename.forward(90)
#turtlename.left(90)
#turtlename.forward(90)

#------------------------------------------#
#%%   <--  cell separator for Spyder       #
#------------------------------------------#

