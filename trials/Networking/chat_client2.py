# chat_client.py

import sys, socket, select

HOST = 'localhost'
SOCKET_LIST = []
RECV_BUFFER = 4096
PORT = 5555

def chat_client():
    if(len(sys.argv) < 3) :
        print 'Usage : python chat_client.py hostname port'
        host = HOST
        port = PORT
        print ' ... using (%s,%s) as default\n' % (host, port)
    else:
        host = sys.argv[1]
        port = int(sys.argv[2])

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # TCP
    client_socket.settimeout(2)

    # connect to remote host
    try :
        client_socket.connect((host, port))
    except :
        print 'Unable to connect to (%s,%s)' % (host, port)
        sys.exit()

    print 'Connected to remote (host,port) = (%s,%s). ' % (host, port)
    print 'You can start sending messages'

    sys.stdout.write('[Me] '); sys.stdout.flush()

    while True:
        socket_list = [sys.stdin, client_socket]

        # Get the list sockets which are readable
        read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])

        for sock in read_sockets:
            if sock == client_socket:
                # incoming message from remote server, client_socket
                data = sock.recv(RECV_BUFFER)
                if not data :
                    print '\n...Disconnected from chat server. Exiting...'
                    sys.exit()
                else :
                    #print data
                    sys.stdout.write(data)
                    sys.stdout.write('[Me] '); sys.stdout.flush()

            else :
                # user entered a message
                msg = sys.stdin.readline()
                client_socket.send(msg)
                sys.stdout.write('[Me] '); sys.stdout.flush()

if __name__ == "__main__":

    sys.exit(chat_client())

