# -*- coding: utf-8 -*-
"""
File: TCPclient.py
Created on Mon Nov 11 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: An echo server that uses select to handle multiple clients at a time.  Entering any line of input at the terminal will exit the server.
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------

"""
'''
Select

The select() method uses the following syntax:

select(input,output,exception[,timeout])

The first three arguments are lists of socket or file objects that are waiting for input, output, or exceptions. If you do not specify a timeout, then the select call will block until one of the listed sockets has an input, output, or exception event occur. If you specify a timeout period, given as a floating point number of seconds, then the socket call will return after this time if no sockets are ready. A timeout value of zero indicates that the sockets are checked but the call will not block if none are ready.

The select() method returns a tuple of three lists: the subset of the input sockets and files that have input, output, or exception events. If a timeout has occurred without any input, output or exception events, then all three lists will be empty.
'''
import select
import socket
import sys

host = ''
port = 50000
backlog = 5
size = 1024
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((host,port))
server.listen(5)
print ' "echoserver-select" started ...\n'
input = [server,sys.stdin]
running = 1
while running:
    inputready,outputready,exceptready = select.select(input,[],[])

    for s in inputready:

        if s == server:
            # handle the server socket
            client, address = server.accept()
            print 'connection established with ', client, '@ ', address
            input.append(client)

        elif s == sys.stdin:
            # handle standard input
            junk = sys.stdin.readline()
            print 'data entered at stdin: ', junk
            running = 0 

        else:
            # handle all other sockets
            data = s.recv(size)
            if data:
                s.send(data)
            else:
                s.close()
                input.remove(s)
server.close()
