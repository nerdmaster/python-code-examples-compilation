#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""


''' ZMQserver.py'''
import zmq

ctx = zmq.Context()

s = ctx.socket(zmq.PUSH)

s.bind("ipc://*:5678")

while True:
    s.send("bla bla bla") # blocks if no receivers
    print "Sent one"

