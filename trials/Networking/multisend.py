#!/usr/bin/python
"""
	multisend.py

	This program sends a message to the specified multicast 
	address once every second.  

	programmer: ALLAN CRUSE
	written on: 05 JAN 2009
"""

import socket, time

group = "224.3.3.6"
saddr = socket.gethostbyname( group )
port = 54321
sock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
message = " -- Hello -- "

while 1:
	sock.sendto( message, ( saddr, port ) )
	time.sleep( 1 )

