# -*- coding: utf-8 -*-
"""
File: test.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: test file
--------------------------------------------------------
"""

import os, time

cwd = os.getcwd()
home = os.path.expanduser('~')

currentTime = time.ctime(time.time())
currentWorkingDirectory = cwd
homeDirectory = home

print('homeDirectory = ', home)
print('currentTime is: ', currentTime)
print('currentWorkingDirectory = ', cwd)

print '...Done!'