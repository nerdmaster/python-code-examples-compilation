# -*- coding: utf-8 -*-
"""
File: findIP.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: takes web-page "name" and finds IP address
--------------------------------------------------------
"""
import sys
try:
    hostname = sys.argv[1]
except:
    hostname = "localhost"

import socket
try:	
    hostip = socket.gethostbyname( hostname )
except:
    hostip = "unknown"

print "The IP-address for '" + hostname \
    + "' is " + hostip

