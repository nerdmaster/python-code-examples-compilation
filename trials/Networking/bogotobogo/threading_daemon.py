#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""


'''
DAEMON THREADS
Daemons are only useful when the main program is running, and it's okay to kill them off once the other non-daemon threads have exited. Without daemon threads, we have to keep track of them, and tell them to exit, before our program can completely quit. By setting them as daemon threads, we can let them run and forget about them, and when our program quits, any daemon threads are killed automatically.

Usually our main program implicitly waits until all other threads have completed their work. However, sometimes programs spawn a thread as a daemon that runs without blocking the main program from exiting. Using daemon threads is useful for services where there may not be an easy way to interrupt the thread or where letting the thread die in the middle of its work without losing or corrupting data. To designate a thread as a daemon, we call its setDaemon() method with a boolean argument. The default setting for a thread is non-daemon. So, passing True turns the daemon mode on.
'''

import threading
import time
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)

def n():
    logging.debug('Starting')
    logging.debug('Exiting')

def d():
    logging.debug('Starting')
    time.sleep(5)
    logging.debug('Exiting')

if __name__ == '__main__':

	t = threading.Thread(name='non-daemon', target=n)
	d = threading.Thread(name='daemon', target=d)
	d.setDaemon(True)

	d.start()
	t.start()

#	d.cancel()
 
	print 'joining them\n'

	t1 = threading.Thread(name='non-daemon', target=n)
	d1 = threading.Thread(name='daemon', target=d)
	d1.setDaemon(True)
	d1.start()
	t1.start()
	print 'd1.isAlive()', d1.isAlive()

	d1.join()
	print 'd1.isAlive()', d1.isAlive()
	t1.join()
	print 'd1.isAlive()', d1.isAlive()
 
#	d1.cancel()
#	print 'd1.isAlive()', d1.isAlive()
 
	print 'joining them with delay start\n'

	t2 = threading.Thread(name='non-daemon', target=n)
	d2 = threading.Thread(name='daemon', target=d)
	d2.setDaemon(True)
	d2.start()
	t2.start()

	d2.join(3.0)
	print 'd2.isAlive()', d2.isAlive()
	t2.join()
	print 'd2.isAlive()', d2.isAlive()
    
#	d2.cancel()
#	print 'd.isAlive()', d2.isAlive()
 
	print 'joining them with longer delay start\n'

	t3 = threading.Thread(name='non-daemon', target=n)
	d3 = threading.Thread(name='daemon', target=d)
	d3.setDaemon(True)
	d3.start()
	t3.start()

	d3.join(7.0)
	print 'd3.isAlive()', d3.isAlive()
	t3.join()
	print 'd3.isAlive()', d3.isAlive()
    
#	d3.cancel()
#	print 'd3.isAlive()', d3.isAlive()
 
   