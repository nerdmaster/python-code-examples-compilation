# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
"""

#<<<<<<<<<<<<<<<<<<<_____________________>>>>>>>>>>>>>>>>>>>#

import os

print 'Starting ...\n'

cwd = os.getcwd()
home = os.path.expanduser('~')

currentWorkingDirectory = os.getcwd()
print(currentWorkingDirectory)
print(os.path.expanduser('~'))

os.chdir('//anaconda')
print('Current Working Directory:', cwd)
print('home directory:', home)

print(os.path.join(os.path.expanduser('~'),'dir', 'subdir', 'k.py'))

pathname = "/Users/K/dir/subdir/k.py"

(dirname, filename) = os.path.split(pathname)
(shortname, extension) = os.path.splitext(filename)

#<<<<<<<<<<<<<<<<<<<_____________________>>>>>>>>>>>>>>>>>>>#

import glob, time

def getDate(entry):
# time.struct_time(tm_year=2013, tm_mon=2, tm_mday=2, tm_hour=21, tm_min=12, tm_sec=35, tm_wday=5, tm_yday=33, tm_isdst=0)

    metadata = os.stat(entry)
    year    = time.localtime(metadata.st_mtime).tm_year
    month   = time.localtime(metadata.st_mtime).tm_mon
    day     = time.localtime(metadata.st_mtime).tm_mday
    weekday = time.localtime(metadata.st_mtime).tm_wday
    hour    = time.localtime(metadata.st_mtime).tm_hour
    minute  = time.localtime(metadata.st_mtime).tm_min
    second  = time.localtime(metadata.st_mtime).tm_sec
    
    response = "%3s, %4s.%02s.%02s @ %02s.%02s.%02s" % \
               (weekday,year,month,day,hour,minute,second)
    return response
    
os.chdir('//anaconda')
directoryList = glob.glob('*')
for entry in directoryList: 
    metadata = os.stat(entry)
    print('{:<15} :: {}'.format(entry, getDate(entry)))

os.chdir(directoryList[0])

#<<<<<<<<<<<<<<<<<<<_____________________>>>>>>>>>>>>>>>>>>>#

os.environ['TRIALS'] = '/Users/benito/Development/classes/ApplicationProgr4Engrs/ME-369-Code-Repository/trials/Networking'

os.chdir(os.path.expandvars('$TRIALS'))

print(cwd)
PythonFilesDirectoryList = glob.glob('*.py')
fileName = 'test.py'
_mode = 'r'
_encoding='utf-8'
if fileName in PythonFilesDirectoryList:
    testFile = open(fileName)
    print(os.path.realpath(fileName))
    print('file.mode:', testFile.mode)
    print('file.name:', testFile.name)
    print('file.encoding:', testFile.encoding)
    testFile.close()
    
    testFile = open(fileName)
    print '<----'
#    testFile.encoding = 'utf-8'
#    print('file.mode:', testFile.mode)
#    print('file.name:', testFile.name)
#    print('file.encoding:', testFile.encoding)
#    testFile.close()
#    
#    testFile = open(fileName, encoding='utf-8')
    contents = testFile.read()
    print('contents of ', testFile, ' are:\n', contents)
    testFile.seek(10)
    char = testFile.read(10)
    print('testFile.read(10): "', char,'"')
    testFile.tell()
    testFile.close()
    print(' is testFile.closed? ', testFile.closed)
    with open(fileName, _mode) as _file:
        _file.seek(16)
        char = _file.read(1)
        print(char)
        _file.close()
    if not testFile.close():
        testFile.close()
    print(' is testFile.closed? ', testFile.closed)
    print('... printing whole file (%s):\n' % fileName)
    lineCount = 0
    with open(fileName, mode=_mode) as _file:
        for line in _file:
            lineCount += 1
            print('{:<5} {}'.format(lineCount, line.rstrip()))
  
#<<<<<<<<<<<<<<<<<<<_____________________>>>>>>>>>>>>>>>>>>>#
print ' ... now opening a file to write\n'

#with open('myfile.txt', encoding='utf-8') as file:
with open('myfile.txt', mode='w') as file:
    file.write('Copy and paste is a design error.')
with open('myfile.txt') as file:
    print(file.read())

with open('myfile.txt', mode='a') as file:
    file.write('\nTesting shows the presence, not the absence of bugs.')
with open('myfile.txt') as file:
    print(file.read())
    
    
#<<<<<<<<<<<<<<<<<<<_____________________>>>>>>>>>>>>>>>>>>>#

import io

my_string = 'C is quirky, flawed, and an enormous success. - Dennis Ritchie (1941-2011)'

my_file = io.StringIO(my_string)
my_file.read()

my_file.read()

my_file.seek(0)

my_file.read(10)

my_file.tell()

my_file.seek(10)

my_file.read()

#<<<<<<<<<<<<<<<<<<<_____________________>>>>>>>>>>>>>>>>>>>#

import gzip

print '... compressing file'
with gzip.open('myfile.gz', mode='wb') as compressed:
    compressed.write('640K ought to be enough for anybody (1981). - Bill Gates(1981)'.encode('utf-8'))

entry = glob.glob('myfile.gz')
print('{:<15} :: {}'.format(entry, getDate(entry)))

#print '... uncompressing file'
#with gzip.open('myfile.gz', mode='rb') as uncompressed:
#    recoveredText = uncompressed.read()
#    print recoveredText


#<<<<<<<<<<<<<<<<<<<_____________________>>>>>>>>>>>>>>>>>>>#

print '...Done!'

