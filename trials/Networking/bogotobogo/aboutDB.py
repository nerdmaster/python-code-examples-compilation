#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""
print 50*'-' #<----------------------------------------------

'''
SQLITE
------
"SQLite is a software library that implements a self-contained, serverless, zero-configuration, transactional SQL database engine. SQLite is the most widely deployed SQL database engine in the world." 
- http://www.sqlite.org/
'''
import sqlite3
print 'sqlite3.version: ', sqlite3.version

print '''
CONNECTING TO THE DATABASE
--------------------------
To connect to the database, we can use sqlite3.connect function by passing the name of a file to open or create it:

db = sqlite3.connect('test.db')
'''
db = sqlite3.connect('test.db')

print '''
We can use the argument ":memory:" to create a temporary DB in the RAM:

dbm = db = sqlite3.connect(':memory:')
'''
dbm = db = sqlite3.connect(':memory:')

print db

dbm.close()

print '''
When we are done working with the DB we need to close the connection:

dbm.close()
db.close()
'''

"""
CREATE TABLE
To use the database, we need to get a cursor object and pass the SQL statements to the cursor object to execute them. Then, we should commit the changes.

We are going to create a books table with title, author, price and year columns.

db = sqlite3.connect('test2.db')
cursor = db.cursor()
cursor.execute('''CREATE TABLE books(id INTEGER PRIMARY KEY, 
                   title TEXT, author TEXT, price TEXT, year TEXT)
               ''')
db.commit() 

Note that the commit function is invoked on the db object, not the cursor object.
"""
#db2 = sqlite3.connect('test2.db')
cursor = db.cursor()
#cursor.execute('''CREATE TABLE books(id INTEGER PRIMARY KEY, 
#                   title TEXT, author TEXT, price TEXT, year TEXT)
#               ''')
result = cursor.execute('''.tables''')
print 'result: ', result
cursor.execute('''.mode column''')
cursor.execute('''.headers on''')
results = cursor.execute('''SELECT * FROM books;''')
print results

print '''
Note that the commit function is invoked on the db object, not the cursor object.
'''

print """
DROP TABLE
To drop a table:

cursor.execute('''DROP TABLE books''')
db.commit()
"""

#cursor.execute('''DROP TABLE books''')
db2.commit()
db2.close()

print '''
INSERT - INSERTING DATA INTO THE DATABASE
To insert data we use the cursor to execute the query. In this example we are going to insert two books in the database, their information will stored in python variables.
'''

print 'creating a db with tables, by element'
db3 = sqlite3.connect('test2.db')
cursor3 = db3.cursor()

title1 = 'Learning Python'
author1 = 'Mark Lutz'
price1 = '$36.19'
year1 ='Jul 6, 2013'

title2 = 'Two Scoops of Django: Best Practices For Django 1.6'
author2 = 'Daniel Greenfeld'
price2 = '$34.68'
year2 = 'Feb 1, 2014'

cursor3.execute('''INSERT INTO books(title, author, price, year)
                  VALUES(?,?,?,?)''', (title1, author1, price1, year1))

cursor3.execute('''INSERT INTO books(title, author, price, year)
                  VALUES(?,?,?,?)''', (title2, author2, price2, year2))

db3.commit()
print 'db committed..'
print db3

print '''
Note: If we need values from Python variables it is recommended to use the "?" placeholder. Never use string operations or concatenation to make your queries because is very insecure.

The values of the Python variables are passed inside a tuple.

If we have more books to insert, we can continue. But this time, we'll do it another way: passing a dictionary using the ":keyname" placeholder:
'''

title3 = 'Python Cookbook'
author3 = 'David Beazley'
price3 = '$30.29'
year3 = 'May 29, 2013'

cursor3.execute('''INSERT INTO books(title, author, price, year)
                  VALUES(:title, :author, :price, :year)''',
                  {'title':title3, 'author':author3, 'price':price3, 'year':year3})

#->| <sqlite3.Cursor object at 0x7f1d2717d650>

db3.commit()

print """
If we need to insert several users, we can use executemany and a list with the tuples:


books = [(title4,author4, price4, year4),
         (title5,author5, price5, year5)]
cursor.executemany('''INSERT INTO books(title, author, price, year) VALUES(?,?,?,?)''', books)
db.commit()
"""
title4 = 'The Quick Python Book'
author4 = 'Naomi R. Ceder'
price4 = '$16.39'
year4 = 'Jan 15, 2010'

title5 ='Python Testing'
author5 ='David Sale'
price5 = '$38.20'
year5 = 'Sep 2, 2014'

books = [(title4,author4, price4, year4),
         (title5,author5, price5, year5)]
cursor3.executemany('''INSERT INTO books(title, author, price, year) VALUES(?,?,?,?)''', books)
db3.commit()

print db3


'''
To retrieve data, we need to execute the query against the cursor object and then use fetchone() to retrieve a single row or fetchall() to retrieve all the rows.
'''
cursor3.execute('''SELECT title, author, price FROM books''')
book1 = cursor3.fetchone()
print book1

all_cols = cursor.fetchall()
print all_cols

for col in all_cols:
    print('{0} : {1}, {2}'.format(col[0], col[1], col[2]))

'''
The cursor object works as an iterator, invoking fetchall() automatically:
'''
cursor3.execute('''SELECT title, author, price, year FROM books''')
for col in cursor3:
    # col[0] returns the first column in the query (title), col[1] returns author column.
    print('{0} : {1}, {2}, {3}'.format(col[0], col[1], col[2], col[3]))

'''
SELECT - 2. RETRIEVING DATA WITH "?"
We can use the ? placeholder to retrieve data with conditions:
'''
book_id = 3
cursor3.execute('''SELECT title, author, price FROM books WHERE id=?''',(book_id,))
book = cursor3.fetchone()
print(book)

'''
UPDATE DATA
Now, we want to update data in the table. Let's switch the price of the 4th book (id = 4).
'''
newPrice = '$19.99'
book_id = 4
cursor3.execute('''UPDATE books SET price = ? WHERE id = ?''', (newPrice, book_id))

'''
We can check if the price of the 4th (book_id = 4) book has been updated from $16.39 to $19.99:
'''
cursor3.execute('''SELECT title, author, price FROM books''')
all_books = cursor3.fetchall()
print(all_books)

'''
DELETE DATA
'''
delete_book_id = 5
cursor3.execute('''DELETE FROM books WHERE id = ?''', (delete_book_id,))
db.commit()

'''Let's check the last boot (id = 5) is gone:'''
cursor3.execute('''SELECT title, author, price FROM books''')
print(cursor3.fetchall())



db3.close()

print 50*'-' #<----------------------------------------------

'''
PYMONGO
-------

'''


# m.py

def get_db():
    from pymongo import MongoClient
    client = MongoClient('localhost:27017')
    db = client.myFirstMB
    return db

def add_country(db):
    db.countries.insert({"name" : "Canada"})
    
def get_country(db):
    return db.countries.find_one()

if __name__ == "__main__":

    db = get_db() 
    add_country(db)
    print get_country(db)
    
