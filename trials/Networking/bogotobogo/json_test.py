#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""
import psutil
import os
import subprocess
import string
import bitstring
import json
import codecs

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

procs_id = 0
procs = {}
procs_data = []

ismvInfo = {
   'baseName':' ',
   'video': {
      'src':[],
      'TrackIDvalue':[],
      'Duration': 0,
      'QualityLevels': 1,
      'Chunks': 0,
      'Url': '',
      'index':[],
      'bitrate':[],
      'fourCC':[],
      'width': [],
      'height':[],
      'codecPrivateData': [],
      'fragDurations':[]
   },
   'audio': {
      'src':[],
      'TrackIDvalue':[],
      'QualityLevels': 1,
      'index':[],
      'bitrate':[],
      'fourCC':[],
      'samplingRate':[],
      'channels':[],
      'bitsPerSample':[],
      'packetSize':[],
      'audioTag': [],
      'codecPrivateData': [],
      'fragDurations': [],
   }
}

def runCommand(cmd, use_shell = False, return_stdout = False, 
                    busy_wait = True,  poll_duration = 0.5):
    # Sanitize cmd to string
    cmd = map(lambda x: '%s' % x, cmd)
    if use_shell:
        command = ' '.join(cmd)
    else:
        command = cmd

    if return_stdout:
        proc = psutil.Popen(cmd, shell = use_shell, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    else:
        proc = psutil.Popen(cmd, shell = use_shell,
                                stdout = open('/dev/null', 'w'),
                                stderr = open('/dev/null', 'w'))

    global procs_id
    global procs
    global procs_data
    proc_id = procs_id
    procs[proc_id] = proc
    procs_id += 1
    data = { }

    while busy_wait:
        returncode = proc.poll()
        if returncode == None:
            try:
                data = proc.as_dict(attrs = ['get_io_counters', 'get_cpu_times'])
            except Exception, e:
                pass
            time.sleep(poll_duration)
        else:
            break

    (stdout, stderr) = proc.communicate()
    returncode = proc.returncode
    del procs[proc_id]

    if returncode != 0:
        raise Exception(stderr)
    else:
        if data:
            procs_data.append(data)
        return stdout

# server manifest
def ismParse(data):
    # need to remove the string below to make xml parse work
    data = data.replace(' xmlns="http://www.w3.org/2001/SMIL20/Language"','')
    root = ET.fromstring(data)

    # head 
    for m in root.iter('head'):
        for p in m.iter('meta'):
            ismvInfo['baseName'] = (p.attrib['content']).split('.')[0]

    # videoAttributes
    for v in root.iter('video'):
        ismvInfo['video']['src'].append(v.attrib['src'])
        for p in v.iter('param'):
            ismvInfo['video']['TrackIDvalue'].append(p.attrib['value'])

    # audioAttributes
    for a in root.iter('audio'):
        ismvInfo['audio']['src'].append(a.attrib['src'])
        for p in a.iter('param'):
            ismvInfo['audio']['TrackIDvalue'].append(p.attrib['value'])

# client manifest
def ismcParse(data):
    root = ET.fromstring(data)

    # duration
    # streamDuration = root.attrib['Duration']
    ismvInfo['video']['Duration'] = root.attrib['Duration']

    for s in root.iter('StreamIndex'):
        if(s.attrib['Type'] == 'video'):
            ismvInfo['video']['QualityLevels'] = s.attrib['QualityLevels']
            ismvInfo['video']['Chunks'] = s.attrib['Chunks']
            ismvInfo['video']['Url'] = s.attrib['Url']
            for q in s.iter('QualityLevel'):
                ismvInfo['video']['index'].append(q.attrib['Index'])
                ismvInfo['video']['bitrate'].append(q.attrib['Bitrate'])
                ismvInfo['video']['fourCC'].append(q.attrib['FourCC'])
                ismvInfo['video']['width'].append(q.attrib['MaxWidth'])
                ismvInfo['video']['height'].append(q.attrib['MaxHeight'])
                ismvInfo['video']['codecPrivateData'].append(q.attrib['CodecPrivateData'])

            # video frag duration
            for c in s.iter('c'):
                ismvInfo['video']['fragDurations'].append(c.attrib['d'])

        elif(s.attrib['Type'] == 'audio'):
            ismvInfo['audio']['QualityLevels'] = s.attrib['QualityLevels']
            ismvInfo['audio']['Url'] = s.attrib['Url']
            for q in s.iter('QualityLevel'):
                #ismvInfo['audio']['index'] = q.attrib['Index'] 
                ismvInfo['audio']['index'].append(q.attrib['Index'])
                ismvInfo['audio']['bitrate'].append(q.attrib['Bitrate'])
                ismvInfo['audio']['fourCC'].append(q.attrib['FourCC'])
                ismvInfo['audio']['samplingRate'].append(q.attrib['SamplingRate'])
                ismvInfo['audio']['channels'].append(q.attrib['Channels'])
                ismvInfo['audio']['bitsPerSample'].append(q.attrib['BitsPerSample'])
                ismvInfo['audio']['packetSize'].append(q.attrib['PacketSize'])
                ismvInfo['audio']['audioTag'].append(q.attrib['AudioTag'])
                ismvInfo['audio']['codecPrivateData'].append(q.attrib['CodecPrivateData'])
            # audio frag duration
            for c in s.iter('c'):
                #audioFragDuration.append(c.attrib['d'])
                ismvInfo['audio']['fragDurations'].append(c.attrib['d'])

def populateManifestMetadata(base):
    try:
        # parse server manifest and populate ismv info data
        with open(base+'.ism', 'rb') as manifest:
            ismData = manifest.read()
            ismParse(ismData)

        # parse client manifest and populate ismv info data
        with open(base+'.ismc', 'rb') as manifest:
            ismcData = manifest.read()
            ismcParse(ismcData)

    except Exception, e:
        raise RuntimeError("issue opening ismv manifest file")

# input 
# ismvFIles - list of ismv files
# base      - basename of ismv files
def setManifestMetadata(ismvFiles, base):
    #cmd = ['ismindex','-n', ismTmpName,'bunny_400.ismv','bunny_894.ismv','bunny_2000.ismv' ] 
    cmd = ['ismindex','-n', base]
    for ism in ismvFiles:
        cmd.append(ism)
    stdout = runCommand(cmd, return_stdout = True, busy_wait = False)
    populateManifestMetadata(base)

if __name__ == '__main__':

   ismvFiles = ['bunny_400.ismv','bunny_894.ismv','bunny_2000.ismv']
   base = 'bunny'

   setManifestMetadata(ismvFiles, base)

   # save to json file
   with codecs.open('ismvInfo.json', 'w', encoding='utf-8') as f:
        json.dump(ismvInfo, f)

