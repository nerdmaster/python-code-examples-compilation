#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""
import yaml, json

print 50*'-' #<----------------------------------------------

'''
WHAT IS YAML?

 - strings do not require quotations.
 
 - The specific number of spaces in the indentation is unimportant as long as 
   parallel elements have the same left justification and the hierarchically 
   nested elements are indented further.

 - The sample above defines:
   - An associative array with 7 top level keys
   - The "items" key contains a 2-element array (or "list")
   - Each element of which is itself an associative array with 
     differing keys.
   - Relational data and redundancy removal are displayed:
     - The "ship-to" associative array content is copied from the "bill-to" 
       associative array's content as indicated by the anchor (&) and 
       reference (*) labels.

 - Optional blank lines can be added for readability.

 - Multiple documents can exist in a single file/stream and are 
   separated by "---".

 - An optional "..." can be used at the end of a file (useful for signaling 
   an end in streamed communications without closing the pipe).



YAML VS JSON?
------------
This answer is an abstracts from What is the difference between YAML and JSON? [http://stackoverflow.com/questions/1726802/what-is-the-difference-between-yaml-and-json-when-to-prefer-one-over-the-other]

Technically YAML is a superset of JSON. This means that, in theory at least, a YAML parser can understand JSON, but not necessarily the other way around.

See the official specs, in the section entitled "YAML: Relation to JSON".
[http://yaml.org/spec/1.2/spec.html#id2759572]

In general, there are certain things available from YAML that are not available from JSON.

YAML is visually easier to look at. In fact the YAML homepage is itself valid YAML, yet it is easy for a human to read. YAML has the ability to reference other items within a YAML file using "anchors." Thus it can handle relational information as one might find in a MySQL database. YAML is more robust about embedding other serialization formats such as JSON or XML within a YAML file. In practice neither of these last two points will likely matter for things that we do, but in the long term, YAML may be a more robust and viable data serialization format.

'''

print '''
JASON TO YAML CONVERSION
Let's convert the following json to yaml:

{
  "foo": "bar",
  "baz": [
    "qux",
    "quxx"
  ],
  "corge": null,
  "grault": 1,
  "garply": true,
  "waldo": "false",
  "fred": "undefined",
  "emptyArray": [],
  "emptyObject": {},
  "emptyString": ""
}
'''

sample = {
  "foo": "bar",
  "baz": [
    "qux",
    "quxx"
  ],
  "corge": None,
  "grault": 1,
  "garply": True,
  "waldo": "false",
  "fred": "undefined",
  "emptyArray": [],
  "emptyObject": {},
  "emptyString": ""
}

json_obj = json.dumps(sample)
print 'json_obj =', json_obj

ff = open('data.yml', 'wb')
yaml.dump(sample, ff, default_flow_style=False)

ydump = yaml.dump(sample, default_flow_style=False)
print 'ydump=',ydump

print 50*'-' #<----------------------------------------------

'''
We can reads in the yaml and write it to json:
'''

stream = file('data.yml', 'r')
yml_loaded = yaml.load(stream)

with open('data.json','wb') as f:
    json.dump(yml_loaded, f)
    
print 'yml_loaded: \n', yml_loaded


print 50*'-' #<----------------------------------------------

print yaml.load("""
      name: Vorlin Laruknuzum
      sex: Male
      class: Priest
      title: Acolyte
      hp: [32, 71]
      sp: [1, 13]
      gold: 423
      inventory:
      - a Holy Book of Prayers (Words of Wisdom)
      - an Azure Potion of Cure Light Wounds
      - a Silver Wand of Wonder
      """)

{'name': 'Vorlin Laruknuzum', 'gold': 423, 'title': 'Acolyte', 'hp': [32, 71],
'sp': [1, 13], 'sex': 'Male', 'inventory': ['a Holy Book of Prayers (Words of Wisdom)',
'an Azure Potion of Cure Light Wounds', 'a Siver Wand of Wonder'], 'class': 'Priest'}

print yaml.dump({'name': "The Cloak 'Colluin'", 'depth': 5, 'rarity': 45,
'weight': 10, 'cost': 50000, 'flags': ['INT', 'WIS', 'SPEED', 'STEALTH']})

print 50*'-' #<----------------------------------------------

with open('tree.yaml', 'r') as f:
    doc = yaml.load(f)

# To access "branch1 text" you would use:

txt = doc["treeroot"]["branch1"]
print txt

print 50*'-' #<----------------------------------------------

document = """
  a: 1
  b:
    c: 3
    d: 4
"""
print 'document = ', document

print 'yaml.dump(yaml.load(document))'
print yaml.dump(yaml.load(document))

print 'yaml.dump(yaml.load(document), default_flow_style=False)'
print yaml.dump(yaml.load(document), default_flow_style=False)

print 50*'-' #<----------------------------------------------
'''
Loading YAML

Warning: It is not safe to call yaml.load with any data received from an untrusted source! yaml.load is as powerful as pickle.load and so may call any Python function. Check the yaml.safe_load function though.

yaml.load accepts a byte string, a Unicode string, an open binary file object, or an open text file object. A byte string or a file must be encoded with utf-8, utf-16-be or utf-16-le encoding.
'''

yaml.load("""
    - Hesperiidae
    - Papilionidae
    - Apatelodidae
    - Epiplemidae
    """)

yaml.load(u"""
    hello: Привет!
    """)

print 50*'-' #<----------------------------------------------
'''
PyYAML allows you to construct a Python object of any type.
'''
yaml.load("""
    none: [~, null]
    bool: [true, false, on, off]
    int: 42
    float: 3.14159
    list: [LITE, RES_ACID, SUS_DEXT]
    dict: {hp: 13, sp: 5}
    """)

print 50*'-' #<----------------------------------------------
'''
Even instances of Python classes can be constructed using the !!python/object tag.
'''
class Hero:
    def __init__(self, name, hp, sp):
        self.name = name
        self.hp = hp
        self.sp = sp
    def __repr__(self):
        return "%s(name=%r, hp=%r, sp=%r)" % (
            self.__class__.__name__, self.name, self.hp, self.sp)

yaml.load("""
    !!python/object:__main__.Hero
    name: Welthyr Syxgon
    hp: 1200
    sp: 0
    """)

print 50*'-' #<----------------------------------------------
'''
Dumping YAML

The yaml.dump function accepts a Python object and produces a YAML document.
'''
print yaml.dump({'name': 'Silenthand Olleander', 'race': 'Human',
    'traits': ['ONE_HAND', 'ONE_EYE']})

'''
yaml.dump accepts the second optional argument, which must be an open text or binary file. In this case, yaml.dump will write the produced YAML document into the file. Otherwise, yaml.dump returns the produced document.
'''
data = """
    none: [~, null]
    bool: [true, false, on, off]
    int: 42
    float: 3.14159
    list: [LITE, RES_ACID, SUS_DEXT]
    dict: {hp: 13, sp: 5}
    """
stream = file('document.yaml', 'w')
yaml.dump(data, stream)    # Write a YAML representation of data to 'document.yaml'.
stream.close()

print yaml.dump(data)      # Output the document to the screen.

'''
If you need to dump several YAML documents to a single stream, use the function yaml.dump_all. yaml.dump_all accepts a list or a generator producing

Python objects to be serialized into a YAML document. The second optional argument is an open file.
'''
print yaml.dump([1,2,3], explicit_start=True)
#->| --- [1, 2, 3]

print yaml.dump_all([1,2,3], explicit_start=True)
#->| --- 1
#->| --- 2
#->| --- 3
#->| ...


print 50*'-' #<----------------------------------------------
print '''
You may even dump instances of Python classes.

class Hero:
    def __init__(self, name, hp, sp):
        self.name = name
        self.hp = hp
        self.sp = sp
    def __repr__(self):
        return "%s(name=%r, hp=%r, sp=%r)" % (
            self.__class__.__name__, self.name, self.hp, self.sp)

print yaml.dump(Hero("Galain Ysseleg", hp=-3, sp=2))
'''
print yaml.dump(Hero("Galain Ysseleg", hp=-3, sp=2))
#->| !!python/object:__main__.Hero {hp: -3, name: Galain Ysseleg, sp: 2}

print 'print Hero("Galain Ysseleg", hp=-3, sp=2)'
print Hero("Galain Ysseleg", hp=-3, sp=2)

#->| Hero(name='Galain Ysseleg', hp=-3, sp=2)

print 50*'-' #<----------------------------------------------
print """
Constructors, representers, resolvers

You may define your own application-specific tags. The easiest way to do it is to define a subclass of yaml.YAMLObject:
"""
class Monster(yaml.YAMLObject):
    yaml_tag = u'!Monster'
    def __init__(self, name, hp, ac, attacks):
        self.name = name
        self.hp = hp
        self.ac = ac
        self.attacks = attacks
    def __repr__(self):
        return "%s(name=%r, hp=%r, ac=%r, attacks=%r)" % (
            self.__class__.__name__, self.name, self.hp, self.ac, self.attacks)
'''
The above definition is enough to automatically load and dump Monster objects:
'''

yaml.load("""
--- !Monster
name: Cave spider
hp: [2,6]    # 2d6
ac: 16
attacks: [BITE, HURT]
""")

Monster(name='Cave spider', hp=[2, 6], ac=16, attacks=['BITE', 'HURT'])

print yaml.dump(Monster(
    name='Cave lizard', hp=[3,6], ac=16, attacks=['BITE','HURT']))

print 50*'-' #<----------------------------------------------
'''
yaml.YAMLObject uses metaclass magic to register a constructor, which transforms a YAML node to a class instance, and a representer, which serializes a class instance to a YAML node.

If you don't want to use metaclasses, you may register your constructors and representers using the functions yaml.add_constructor and yaml.add_representer. For instance, you may want to add a constructor and a representer for the following Dice class:
'''
class Dice(tuple):
    def __new__(cls, a, b):
        return tuple.__new__(cls, [a, b])
    def __repr__(self):
        return "Dice(%s,%s)" % self

print Dice(3,6)

# The default representation for Dice objects is not pretty:

print yaml.dump(Dice(3,6))
#->|  !!python/object/new:__main__.Dice
#->| - !!python/tuple [3, 6]

'''
Suppose you want a Dice object to represented as AdB in YAML:

>>> print yaml.dump(Dice(3,6))

3d6

First we define a representer that converts a dice object to a scalar node with the tag !dice, then we register it.
'''
def dice_representer(dumper, data):
    return dumper.represent_scalar(u'!dice', u'%sd%s' % data)

yaml.add_representer(Dice, dice_representer)

# Now you may dump an instance of the Dice object:

print yaml.dump({'gold': Dice(10,6)})

'''
Let us add the code to construct a Dice object:
'''
def dice_constructor(loader, node):
    value = loader.construct_scalar(node)
    a, b = map(int, value.split('d'))
    return Dice(a, b)

yaml.add_constructor(u'!dice', dice_constructor)

# Then you may load a Dice object as well:

print yaml.load("""
initial hit points: !dice 8d4
""")

'''
You might not want to specify the tag !dice everywhere. There is a way to teach PyYAML that any untagged plain scalar which looks like XdY has the implicit tag !dice. Use add_implicit_resolver:
'''
import re
pattern = re.compile(r'^\d+d\d+$') # ^<---pattern--->$
yaml.add_implicit_resolver(u'!dice', pattern)

# Now you don't have to specify the tag to define a Dice object:

print yaml.dump({'treasure': Dice(10,20)})

#->| {treasure: 10d20}

print yaml.load("""
damage: 5d10
""")


print 50*'-' #<----------------------------------------------


print 50*'-' #<----------------------------------------------
'''
If a string or a file contains several documents, you may load them all with the yaml.load_all function.
'''

documents = """
    ---
    name: The Set of Gauntlets 'Pauraegen'
    description: >
        A set of handgear with sparks that crackle
        across its knuckleguards.
    ---
    name: The Set of Gauntlets 'Paurnen'
    description: >
      A set of gauntlets that gives off a foul,
      acrid odour yet remains untarnished.
    ---
    name: The Set of Gauntlets 'Paurnimmen'
    description: >
      A set of handgear, freezing with unnatural cold.
    """

for data in yaml.load_all(documents):
    print data




