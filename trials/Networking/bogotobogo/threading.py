#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: threading.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""

import threading

def f():
    print 'thread function'
    return

def g(id):
    print 'thread function %s' %(id)
    return

def Main():
    print '------'
    for i in range(3):
        t = threading.Thread(target=f)
        t.start()
    print '------'
    for i in range(3):
        t = threading.Thread(target=g, args=(i,))
        t.start()
    print '------'

if __name__ == '__main__':
    Main()