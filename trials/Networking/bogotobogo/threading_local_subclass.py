#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""
'''
SUBCLASSING THREADING.LOCAL
To initialize the settings so all threads start with the same value, we need to use a subclass and set the attributes in __init__().
'''
import threading
import logging
import random

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-10s) %(message)s',)

def show(d):
    try:
        val = d.val
    except AttributeError:
        logging.debug('No value yet')
    else:
        logging.debug('value=%s', val)

def f(d):
    show(d)
    d.value = random.randint(1, 100)
    show(d)

class MyLocal(threading.local):
    def __init__(self, v):
        logging.debug('Initializing %r', self)
        self.val = v

if __name__ == '__main__':
    d = MyLocal(999)
    show(d)

    for i in range(2):
        t = threading.Thread(target=f, args=(d,))
        t.start()