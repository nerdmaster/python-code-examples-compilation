#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""
'''
FUNCTIONS DEF
The def create a function object and assigns it to a name. Its general format is:




  

def <name>(arg1, arg2,...,argN)
	<statement>
The statement block becomes the function's body. The def header line specifies a function name and the function bodies often contain a return statement:

def <name>(arg1, arg2,...,argN)
	return <value>
The notable thing here is that the function doesn't define a return datatype. Python functions do not specify the datatype of their return value. They don't even specify whether or not they return a value. Actually, however, every Python function returns a value if the function ever executes a return statement, and it will return that value. Otherwise, it will return None.

Everything in Python is a function, all functions return a value even if it is None, and all functions start with def.

Here are brief descriptions:


def is an executable code. 
Python functions are written with a new statement, the def. Unlike functions in compiled language def is an executable statement. Our function does not exist until Python reaches and runs the def. Actually, it's legal to nest def statements inside if statement, while loops, and even other defs. In general, def statements are coded in module files and are naturally run to generate functions when a module file is first imported.

def creates an object and assigns it to a name. 
When Python reaches and runs a def statement, it generates a new function object and assigns it to the function's name. As with all assignments, the function name becomes a reference to the function object. There's nothing magic about the name of a function. The function object can be assigned to other names, stored in a list, and so son. Functions objects may also have arbitrary user-defined attributes attached to them to record data.

return sends a result object back to the caller. 
When a function is called, the caller stops until the function finishes its work and returns control to the caller. Functions that compute a value send it back to the caller with a return statement. The returned value becomes the result of the function call.
'''

# function.py
def func():
    print('@func()')
    def func1():
        print('@func1()')
        def func2():
            print('@func2()')
        func2()
    func1()

func()

'''
Here, the function was assigned to a different name and called through the new name. Functions are just object. They are recorded explicitly in memory at program execution time. In fact, besides calls, functions allow arbitrary attributes to be attached to record information for later use:

def func():          # Create function object
func()			            # Call object
func.attr = value    # Attach attributes
'''
# Create and assign function
def times(x, y):     # Create and assign function
    return x * y     # Body executed when called

times(2, 5)          # Arguments in parentheses

x = times(365,5)
print x

times('blah!', 4)

'''
KEYWORD
Here is the keyword example:
'''
def f(a, b, c):
    print(a, b, c)
	
f(1, 2, 3)

f(c=3, b=2, a=1)

'''
DEFAULT
'''
def fd(a, b=2, c=3):
    print(a, b, c)

fd(1)

fd(10, 50)

fd(10, 70, 90)

"""
ARGUMENT MATCHING TABLE

Syntax                Location	  Descruption
---------------------------------------------------------------------------
func(value)           Caller	   Normal argument: matched by position
func(name=value)      Caller	   Keyword argument: matched by name
func(*sequence)       Caller	   Pass all object in sequence as individual positional arguments
func(**dict)          Caller    Pass all key/value pairs in dict as individual keyword arguments
def func(name)        Function  Normal argument: matched any passed value by position or name
def func(name=value)  Function  Default argument value, if not passed in the call
def func(*name)       Function  Matches and collects remaining positional arguments in a tuple
def func(**name)      Function  Matches and collects remaining keyword arguments in a dictionary
def func(*args, name) Function  Arguments that must be passed by keyword only in calls
"""

'''
*ARGS AND **KWARGS - COLLECTING AND UNPACKING ARGUMENTS
Putting *args and/or **kwargs as the last items in our function definition's argument list allows that function to accept an arbitrary number of anonymous and/or keyword arguments.
Those arguments are called Keyword Arguments. Actually, they are place holders for multiple arguments, and they are useful especially when we need to pass a different number of arguments each time we call the function.

We may want to use *args when we're not sure how many arguments might be passed to our function, i.e. it allows us to pass an arbitrary number of arguments to our function.


COLLECTING ARGUMENTS
'''
def f(*args):
	print(args)
	
f()

f(10)

f(10, 20, 30)


def print_all(*args):
	for x in enumerate(args):
		print x

print_all('A','b','b','a')

'''
The ** is similar but it only works for keyword arguments. In other words, it collects them into a new dictionary. Actually, ** allows us to convert from keywords to dictionaries:
'''
def f(**kargs):
	print(kargs)
 
def kargs_function(**kargs):
	for k,v in kargs.items():
		print (k,v)
  
kargs_function(**{'uno':'one','dos':'two','tres':'three'})

kargs_function(dos='two', tres='three', uno='one')

'''
UNPACKING ARGUMENTS
We can use the * or ** when we call a function. In other words, it unpacks a collection of arguments, rather than constructing a collection of arguments. In the following example, we pass five arguments to a function in a tuple and let Python unpack them into individual arguments:
'''
def ff(a, b, c, d, e):
	print(a, b, c, d, e)

args = (10, 20)
args += (30, 40, 50)

ff(*args)
ff(*(10, 20), **{'d':40, 'e':50, 'c':30})
ff(10, *(20, 30), **{'d':40, 'e':50})
ff(10, c = 30, *(20,), **{'d':40, 'e':50})
ff(10, *(20,30), d=40, e=50)
ff(10, *(20,), c=30, **{'d':40, 'e':50})

'''
ARBITRARY FUNCTION
In the code below, we support any function with any arguments by passing along whatever arguments that were sent in:

>>> def A_function(f, *args, **kargs):
	return f(*args, **kargs)
'''

def f2(a, b, c, d, e):
	return a*b*c*d*e

print(f2(10, 20, c=30, d=40, e=50))

'''
APPLY() - DEPRECATED
f(*args, **kargs)         # newer call syntax: f(*sequence, **dict)
apply(f, args, kargs)     # deprecated built-in: apply(f, sequence, dict)
'''

def echo(*args, **kargs):
	print(args, kargs)
 
echo(10, 20, a=30, b=40)

'''
If we use the apply():
'''
args = (10, 20)
kargs = {'a':30, 'b':40}

apply(echo, args, kargs)

echo(*args, **kargs)







