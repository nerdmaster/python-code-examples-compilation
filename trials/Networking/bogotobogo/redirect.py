#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: redirect.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""

#redirect.py
import sys
class StdoutRedirect:
    def __init__(self, newOut):
        self.newOut = newOut

    def __enter__(self):
        self.oldOut = sys.stdout
        sys.stdout = self.newOut

    def __exit__(self, *args):
        sys.stdout = self.oldOut

print('X')
with open('output', mode='w') as myFile:
    with StdoutRedirect(myFile):
	print('Y')
print('Z')


import os

os.system('echo $HOME')
os.system('echo %s' %'$HOME')

# os.system('command_1 < input_file | command_2 > output_file')

os.system('echo $HOME > outfile')

f = open('outfile','r')
print(f.read())
f.close()

stream = os.popen('echo $HOME')
stream.read()

# subprocess.call(args, *, stdin=None, stdout=None, stderr=None, shell=False)

os.chdir('/')

import subprocess

subprocess.call(['ls','-l'])

subprocess.call('echo $HOME', shell=True)

# subprocess.check_call(args, *, stdin=None, stdout=None, stderr=None, shell=False)

# subprocess.check_output(args, *, stdin=None, stderr=None, 
#                         shell=False, universal_newlines=False)

output = subprocess.check_output(['ls','-l'])
print output

output = subprocess.check_output(['echo','$HOME'], shell=True)
print output


