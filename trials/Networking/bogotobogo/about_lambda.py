#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""
'''
FUNCTIONS LAMBDA
Python supports the creation of anonymous functions (i.e. functions that are not bound to a name) at runtime, using a construct called lambda. This is not exactly the same as lambda in functional programming languages such as Lisp, but it is a very powerful concept that's well integrated into Python and is often used in conjunction with typical functional concepts like filter(), map() and reduce().

Like def, the lambda creates a function to be called later. But it returns the function instead of assigning it to a name. This is why lambdas are sometimes known as anonymous functions. In practice, they are used as a way to inline a function definition, or to defer execution of a code.

The following code shows the difference between a normal function definition, func and a lambda function, lamb:
'''

def func(x): return x ** 3

print('func(5)',func(5))

lamb = lambda x: x ** 3

print('lamb(5)',lamb(5))

'''
As we can see, func() and lamb() do exactly the same and can be used in the same ways. Note that the lambda definition does not include a return statement -- it always contains an expression which is returned. Also note that we can put a lambda definition anywhere a function is expected, and we don't have to assign it to a variable at all.

The lambda's general form is :

lambda arg1, arg2, ...argN : expression using arguments

Function objects returned by running lambda expressions work exactly the same as those created and assigned by defs. However, there are a few differences that make lambda useful in specialized roles:

lambda is an expression, not a statement.
Because of this, a lambda can appear in places a def is not allowed. For example, places like inside a list literal, or a function call's arguments. As an expression, lambda returns a value that can optionally be assigned a name. In contrast, the def statement always assigns the new function to the name in the header, instead of returning is as a result.
lambda's body is a single expression, not a block of statements.
The lambda's body is similar to what we'd put in a def body's return statement. We simply type the result as an expression instead of explicitly returning it. Because it is limited to an expression, a lambda is less general that a def. We can only squeeze design, to limit program nesting. lambda is designed for coding simple functions, and def handles larger tasks.
'''

def f(x, y, z): return x + y + z

f(2, 30, 400)

'''
We can achieve the same effect with lambda expression by explicitly assigning its result to a name through which we can call the function later:
'''

f = lambda x, y, z: x + y + z

f(2, 30, 400)

# Default work on lambda arguments:
mz = (lambda a = 'Wolfgangus', b = ' Theophilus', c = ' Mozart': a + b + c)

mz('Wolfgang', ' Amadeus')

# In the following example, the value for the name title would have been passes in as a default argument value:

def writer():
	title = 'Sir'
	name = (lambda x:title + ' ' + x)
	return name

who = writer()
who('Arthur Ignatius Conan Doyle')

"""
WHY LAMBDA?
The lambdas can be used as a function shorthand that allows us to embed a function within the code. For instance, callback handlers are frequently coded as inline lambda expressions embedded directly in a registration call's arguments list. Instead of being define with a def elsewhere in a file and referenced by name, lambdas are also commonly used to code jump tables which are lists or dictionaries of actions to be performed on demand.
"""

L = [lambda x: x ** 2,
     lambda x: x ** 3,
     lambda x: x ** 4]

for f in L:
	print(f(3))

print(L[0](11))

'''
n the example above, a list of three functions was built up by embedding lambda expressions inside a list. A def won't work inside a list literal like this because it is a statement, not an expression. If we really want to use def for the same result, we need temporary function names and definitions outside:
'''

def f1(x): return x ** 2

def f2(x): return x ** 3

def f3(x): return x ** 4

# Reference by name
L = [f1, f2, f3]
for f in L:
	print(f(3))
 
print(L[0](3))

# If we know what we're doing, we can code most statements as expressions:


min = (lambda x, y: x if x < y else y)
min(101*99, 102*98)

'''
If we need to perform loops within a lambda, we can also embed things like map calls and list comprehension expressions.
'''
import sys

fullname = lambda x: list(map(sys.stdout.write,x))

f = fullname(['Wassily ', 'Wassilyevich ', 'Kandinsky'])

fullname = lambda x: [sys.stdout.write(a) for a in x]
t = fullname(['Wassily ', 'Wassilyevich ', 'Kandinsky'])
print t

'''
Here is the description of map built-in function.

map(function, iterable, ...)
Return an iterator that applies function to every item of iterable, yielding the results. If additional iterable arguments are passed, function must take that many arguments and is applied to the items from all iterables in parallel. With multiple iterables, the iterator stops when the shortest iterable is exhausted.

So, in the above example, sys.stdout.write is an argument for function, and the x is an iterable item, list, in the example.
'''

"""
NESTED LAMBDA
In the following example, the lambda appears inside a def and so can access the value that the name x has in the function's scope at the time that the enclosing function was called:
"""
def action(x):
	# Make and return function, remember x
	return (lambda newx: x + newx)

ans = action(99)
ans

ans(100)

'''
Though not clear in this example, note that lambda also has access to the names in any enclosing lambda. Let's look at the following example:
'''

action = (lambda x: (lambda newx: x + newx))
ans = action(99)
print(ans)

print(ans(100))


print( (  (lambda x: (lambda newx: x + newx)) (99)) (100) )











