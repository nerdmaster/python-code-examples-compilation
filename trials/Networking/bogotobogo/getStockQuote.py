# -*- coding: utf-8 -*-
"""
File: getStockQuote
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
"""
import sys, socket

try:    
    StockSymbol = sys.argv[1]
except: 
    StockSymbol = 'AAPL'

try:
	host = "download.finance.yahoo.com"
	port = 80
	sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
	sock.settimeout( 2 )
	sock.connect( ( host, port) )
	command = 'GET /d/quotes.csv?s=' + StockSymbol + \
           '&f=sl1d1t1c1ohgv&e=.csv \r\n'
	sock.send( command )
	sock.send( '\r\n' );
	response_string, server = sock.recvfrom( 4096 )
	quote = str.split( response_string, ',' )

except  socket.error, msg:
	print 'An error occurred:', msg

finally:
	print
	print  StockSymbol, ' Stock: ', \
        '$'+quote[1], 'at', quote[3], 'on', quote[2]
