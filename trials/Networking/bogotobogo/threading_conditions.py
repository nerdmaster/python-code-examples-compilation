#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""
'''
CONDITION OBJECTS
In this chapter, we'll learn another way of synchronizing threads: using a Condition object. Because a condition variable is always associated with some kind of lock, it can be tied to a shared resource. A lock can be passed in or one will be created by default. Passing one in is useful when several condition variables must share the same lock. The lock is part of the condition object: we don't have to track it separately. So, the condition object allows threads to wait for the resource to be updated.

In the following example, the consumer threads wait for the Condition to be set before continuing. The producer thread is responsible for setting the condition and notifying the other threads that they can continue.
'''
import threading
import time
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)

def consumer(cv):
    logging.debug('Consumer thread started ...')
    with cv:
    	logging.debug('Consumer waiting ...')
        cv.wait()
        logging.debug('Consumer consumed the resource')

def producer(cv):
    logging.debug('Producer thread started ...')
    with cv:
        logging.debug('Making resource available')
        logging.debug('Notifying to all consumers')
        cv.notifyAll()

if __name__ == '__main__':
    condition = threading.Condition()
    cs1 = threading.Thread(name='consumer1', target=consumer, args=(condition,))
    cs2 = threading.Thread(name='consumer2', target=consumer, args=(condition,))
    pd = threading.Thread(name='producer', target=producer, args=(condition,))

    cs1.start()
    time.sleep(2)
    cs2.start()
    time.sleep(2)
    pd.start()
