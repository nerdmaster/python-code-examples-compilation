#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""


import sys

arguments = sys.argv[1:]
print 'arguments: ', arguments

if '=' in arguments:
    variable = arguments[0]
    n = 1 if '=' in variable else 2
    formula = ''
    for a in arguments[n:]:
        formula += a    
    result = eval(formula)
    formula = variable + '=' + formula
    print 'formula: ', formula
    exec(formula)
    display = "print 'result: ', '%s = %s'" % (formula, result)
    exec("print 'result: ', result")
    exec(display)


