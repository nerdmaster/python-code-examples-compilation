#!/usr/bin/python
"""
	multirecv.py

	This program joins the multicast group corresponding to
	the specified multicast address and displays all of the
	messages sent to that group.  Our code has been adapted
	from an online multicast example by Jason R. Briggs. 

	programmer: ALLAN CRUSE
	written on: 05 JAN 2009
"""

import socket, struct

group = "224.3.3.6"
saddr = socket.gethostbyname( group )
port = 54321
sock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
buflen = 256
sock.bind( ( saddr, port ) )
ifaddr = socket.INADDR_ANY
skaddr = socket.inet_pton( socket.AF_INET, saddr )
mreq = struct.pack( '4sl', socket.inet_aton( saddr ), ifaddr )

try:
	sock.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )

except socket.error, ( value, message ):
	print message

else:
	try:
	  sock.setsockopt( socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq )

	except socket.error, ( value, message ):
	  print message

	else:
	  while True:
		message, address = sock.recvfrom( 256 ) 
		print message

	  sock.close()

