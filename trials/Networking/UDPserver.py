# -*- coding: utf-8 -*-
"""
File: TCPserver.py
Created on Mon Nov 11 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: Test Event Handling in MatPlotLib
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------

A simple TCP server
-------------------
waits for client connection. Takes the message and returns
it capitalized.

"""
import socket, sys

def main():
    print ' creating TCP socket ...'
    host = '127.0.0.10' #'localhost'
    port = 11111
    buffer_size = 1024
    backlog = 5
    socket_family = socket.AF_INET
    socket_type = socket.SOCK_STREAM
    socket_options = (socket.SOL_SOCKET, \
                      socket.SO_REUSEADDR, buffer_size)
    
    try:
        sckt = socket.socket(socket_family, socket_type)
        print sckt.family
        print sckt.type
        print sckt._sock
#        sckt.setsockop(socket_options)
        sckt.bind((host, port))
        print 'TCP server started ...'
        print 'host: ', host, 'port: ', port
        # wait for one (1) client:
        sckt.listen(backlog)
    except socket.error, (code,message):
        if sckt: sckt.close()
        print 'socket error: ', code
        print "Could not open socket: ", message
        sys.exit(1)
    
    terminate = False
    while not terminate:
        client_connection, client_address = sckt.accept()
        print 'connection established with ...'
        print 'IP address: '   + str(client_address) + \
              ', connection: ' + str(client_connection)
        while True:
            data = client_connection.recv(buffer_size)
            if not data:
                break
            else:
                print 'data received ', str(data)
                message = str(data).upper
                sckt.send(message)
        sckt.close()
        if message == 'Q': terminate = True
    if sckt: sckt.close()
    print 'TCP server closing ... Done!'

if __name__ == '__main__':
    print 'Running main() ...'
    main()
    