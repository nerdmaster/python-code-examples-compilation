#!//anaconda/bin/python
# -*- coding: utf-8 -*-
"""
File: 
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------

ØMQ (also known as ZeroMQ, 0MQ, or zmq) looks like an embeddable networking library but acts like a
concurrency framework. It gives you sockets that carry atomic messages across various transports like inprocess,
inter-process, TCP, and multicast. You can connect sockets N-to-N with patterns like fan-out, pub-sub,
task distribution, and request-reply. It's fast enough to be the fabric for clustered products. Its asynchronous I/O
model gives you scalable multicore applications, built as asynchronous message-processing tasks. It has a score
of language APIs and runs on most operating systems. ØMQ is from iMatix and is LGPLv3 open source.

"""
#--------------------------------------------------------------
''' ZMQserver.py'''
import zmq

ctx = zmq.Context()

s = ctx.socket(zmq.PUSH)

s.bind("ipc://*:5678")

while True:
    s.send("bla bla bla") # blocks if no receivers
    print "Sent one"

#--------------------------------------------------------------
''' ZMQserver.py'''
import zmq

ctx = zmq.Context()

s = ctx.socket(zmq.PULL)
s.connect("ipc://*:5678")

while True:
    msg = s.recv()
    print 'Got msg:', msg

#--------------------------------------------------------------


