# -*- coding: utf-8 -*-
"""
File: TCPclient.py
Created on Mon Nov 11 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: Test Event Handling in MatPlotLib
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------

A simple TCP client
-------------------
Ask for message and sends it until message is 'q'

"""
import socket

def main():
    socket_family = socket.AF_INET
    socket_type = socket.SOCK_STREAM
    host = socket.gethostname() #'localhost'
    port = 50000
    buffer_size = 1024
    
    print 'creating TCP socket'
    sckt = socket.socket(socket_family, socket_type)
    print 'trying to connect to TCP server: ', host
    print ' at port: ', port
    sckt.connect((host,port))
    print 'TCP client created ...\n'
    print 'connecting to server @ ' + host + ':' + port
    
    message = raw_input('-> ')
    print 'echo:-> ', message, '<-'
    terminate = False
    while not terminate:
        sckt.send(message)
        data = sckt.recv(buffer_size)
        print 'received ', str(data)
        message = raw_input('-> ')
        if message == 'q': terminate = True
        
    sckt.close()
    print '... Done!'

if __name__ == '__main__':
    print 'TCPclient::> Running main() ...'
    main()