# -*- coding: utf-8 -*-
"""
File: point_in_polygon.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""
'''
http://codereview.stackexchange.com/questions/54461/calculating-if-a-point-is-within-a-polygon-or-outside-of-it
'''

import numpy as np
import random

def findAnglesBetweenTwoVectors(v1s, v2s):
    dot_v1_v2 = np.einsum('ij,ij->i', v1s, v2s)
    dot_v1_v1 = np.einsum('ij,ij->i', v1s, v1s)
    dot_v2_v2 = np.einsum('ij,ij->i', v2s, v2s)

    return np.arccos(dot_v1_v2/(np.sqrt(dot_v1_v1*dot_v2_v2)))

def calculateDirectionOfRayNotParallelToAnyEdgeOfPolygon(vertices, difference = 1e-2, referenceDirectionVector = np.array([1, 0]), maxWhileTime = 0.5):

    relevantVectors = vertices - np.roll(vertices, 1, axis = 0)
    angles = findAnglesBetweenTwoVectors(relevantVectors, np.repeat([referenceDirectionVector], len(relevantVectors), axis=0))
    angles = np.append(angles, np.pi+angles)

    for x in xrange(50):
        direction = random.random()*2*np.pi
        if np.all(np.abs(angles-direction) >= difference):
            return direction

    raise StandardError("No direction found!")

def calculateNumberOfBoundaryIntersections(point, direction, vertices):

    neighbourVertices = np.roll(vertices, 1, axis = 0)
    ss = neighbourVertices - vertices

    r = produceDirectionVectorGivenDirection(direction)

    qs_minus_p = vertices - point
    r_cross_ss = np.cross(r, ss)

    ts = np.cross(qs_minus_p, ss)/r_cross_ss
    if 0 in ts:
        return 1

    us = np.cross((qs_minus_p), r)/r_cross_ss

    return np.sum((ts >= 0) & (us >= 0) & (us < 1))

def isPointWithinPolygon(point, polygonVertexCoords):
    fixedDirection = calculateDirectionOfRayNotParallelToAnyEdgeOfPolygon(polygonVertexCoords)

    numberOfBoundaryIntersections = calculateNumberOfBoundaryIntersections(point, fixedDirection, polygonVertexCoords)

    if numberOfBoundaryIntersections%2 == 0:
        return False
    else:
        return True
        
    
#!/usr/bin/env python



ef determineUnitVectorNotParallelToAnyEdgeOfPolygon(vertices):
    edges = np.roll(vertices, 1, axis = 0) - vertices

    for x in range(100):
        direction = 2*np.pi*random.random()
        directionVector = np.array([np.cos(direction), np.sin(direction)])

        if 0 not in np.cross(edges, directionVector):
            return directionVector

def calculateNumberOfBoundaryIntersections(point, directionVector, vertices):

    neighbourVertices = np.roll(vertices, 1, axis = 0)
    ss = neighbourVertices - vertices

    r = directionVector

    qs_minus_p = vertices - point
    r_cross_ss = np.cross(r, ss)

    ts = np.cross(qs_minus_p, ss)/r_cross_ss
    if 0 in ts:
        return 1

    us = np.cross((qs_minus_p), r)/r_cross_ss

    return np.sum((ts >= 0) & (us >= 0) & (us < 1))

def isPointWithinPolygon(point, polygonVertexCoords):
    numberOfBoundaryIntersections = calculateNumberOfBoundaryIntersections(point, determineUnitVectorNotParallelToAnyEdgeOfPolygon(polygonVertexCoords), polygonVertexCoords)

    if numberOfBoundaryIntersections%2 == 0:
        return False
    else:
        return True
        
#
# routine for performing the "point in polygon" inclusion test

# Copyright 2001, softSurfer (www.softsurfer.com)
# This code may be freely used and modified for any purpose
# providing that this copyright notice is included with it.
# SoftSurfer makes no warranty for this code, and cannot be held
# liable for any real or imagined damage resulting from its use.
# Users of this code must verify correctness for their application.

# translated to Python by Maciej Kalisiak <mac@dgp.toronto.edu>

#   a Point is represented as a tuple: (x,y)

#===================================================================

# is_left(): tests if a point is Left|On|Right of an infinite line.

#   Input: three points P0, P1, and P2
#   Return: >0 for P2 left of the line through P0 and P1
#           =0 for P2 on the line
#           <0 for P2 right of the line
#   See: the January 2001 Algorithm "Area of 2D and 3D Triangles and Polygons"

def is_left(P0, P1, P2):
    return (P1[0] - P0[0]) * (P2[1] - P0[1]) - (P2[0] - P0[0]) * (P1[1] - P0[1])

#===================================================================

# cn_PnPoly(): crossing number test for a point in a polygon
#     Input:  P = a point,
#             V[] = vertex points of a polygon
#     Return: 0 = outside, 1 = inside
# This code is patterned after [Franklin, 2000]

def cn_PnPoly(P, V):
    cn = 0    # the crossing number counter

    # repeat the first vertex at end
    V = tuple(V[:])+(V[0],)

    # loop through all edges of the polygon
    for i in range(len(V)-1):   # edge from V[i] to V[i+1]
        if ((V[i][1] <= P[1] and V[i+1][1] > P[1])   # an upward crossing
            or (V[i][1] > P[1] and V[i+1][1] <= P[1])):  # a downward crossing
            # compute the actual edge-ray intersect x-coordinate
            vt = (P[1] - V[i][1]) / float(V[i+1][1] - V[i][1])
            if P[0] < V[i][0] + vt * (V[i+1][0] - V[i][0]): # P[0] < intersect
                cn += 1  # a valid crossing of y=P[1] right of P[0]

    return cn % 2   # 0 if even (out), and 1 if odd (in)

#===================================================================

# wn_PnPoly(): winding number test for a point in a polygon
#     Input:  P = a point,
#             V[] = vertex points of a polygon
#     Return: wn = the winding number (=0 only if P is outside V[])

def wn_PnPoly(P, V):
    wn = 0   # the winding number counter

    # repeat the first vertex at end
    V = tuple(V[:]) + (V[0],)

    # loop through all edges of the polygon
    for i in range(len(V)-1):     # edge from V[i] to V[i+1]
        if V[i][1] <= P[1]:        # start y <= P[1]
            if V[i+1][1] > P[1]:     # an upward crossing
                if is_left(V[i], V[i+1], P) > 0: # P left of edge
                    wn += 1           # have a valid up intersect
        else:                      # start y > P[1] (no test needed)
            if V[i+1][1] <= P[1]:    # a downward crossing
                if is_left(V[i], V[i+1], P) < 0: # P right of edge
                    wn -= 1           # have a valid down intersect
    return wn

#===================================================================
#===================================================================

# Compute 2x the winding number of a path around a point
# Exactly correct results with integer inputs
 
# orientation of the point (x,y) with respect to the given segment (a0,b0)-(a1,b1)
# 0: collinear   1: within right half-plane   -1: within left half-plane
# calculated with a simple cross product
def orient((x,y), (a0,b0), (a1,b1)):
    return cmp((a1-a0)*y + (b0-b1)*x + a0*b1-a1*b0, 0)
 
# The double winding number of the given path ps about the given point p0
def windingnumber(p0, ps):
    # Use the point (x,y) to divide the plane into two half-planes "above" and "below"
    # -1 or 1 depending on which half-plane each point is in. (0 if it's equivalent)
    h = [cmp(p, p0) for p in ps]
    w = 0
    for j in range(len(ps)):
        i, k = (j-1)%len(ps), (j+1)%len(ps)  # Previous and next points in the path
        if h[j] * h[k] == 1:  # Both points are in the same half-plane, so do nothing
            pass
        elif h[j] * h[k] == -1:  # Crossing from one half-plane to the other
            w += orient(p0, ps[j], ps[k])
        elif h[j] == 0:  # p[j] == p0
            if h[i] != h[k]:  # Cross from one half-plane to the other, don't change w
                pass
            else:  # Leave p0 through the same half-plane you entered through
                w += orient(ps[k], ps[i], ps[j])
        else:  # this segment is leading either toward or away from p0
            pass
    return w
 
# Test #1: normal W-shaped concave path
path = (1,0), (3,4), (4,1), (6,5), (0,5), (0,1)
 
test = "\n".join(" ".join(str(windingnumber((x,y), path)) for x in range(-1,8)) for y in range(6,-2,-1))
print test
 
assert test == """
0 0 0 0 0 0 0 0 0
0 1 1 1 1 1 1 1 0
0 1 2 2 1 2 2 0 0
0 1 2 2 0 2 1 0 0
0 1 2 1 0 2 0 0 0
0 1 2 0 0 1 0 0 0
0 0 1 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0
"""
 
 
print
 
# Test #2: complex path with a double wind and a figure 8
path = (3,0), (5,3), (3,6), (1,2), (6,2), (6,8), (9,5), (1,4)
 
test = "\n".join(" ".join(str(windingnumber((x,y), path)) for x in range(0,11)) for y in range(9,-2,-1))
print test
 
assert test == """0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 -1 0 0 0 0
0 0 0 0 0 0 -1 -1 0 0 0
0 0 0 1 0 0 -1 -2 -1 0 0
0 0 0 2 0 0 -1 -2 -2 -1 0
0 1 3 4 4 2 1 0 0 0 0
0 0 4 4 4 3 1 0 0 0 0
0 1 2 3 3 1 1 0 0 0 0
0 0 0 2 0 0 0 0 0 0 0
0 0 0 1 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0"""
 
 