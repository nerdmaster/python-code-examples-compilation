# -*- coding: utf-8 -*-
"""
File: simpleGraphics.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
URL: http://www.maprantala.com/2010/05/16/measuring-distance-from-a-point-to-a-line-segment/
Description: Define Point, Segment and some methods
--------------------------------------------------------
Inputs: None
--------------------------------------------------------
Outputs: None
--------------------------------------------------------
"""
# Modified from: graphics.py
"""Simple object oriented graphics library  

The library is designed to make it very easy for novice programmers to
experiment with computer graphics in an object oriented fashion. It is
written by John Zelle for use with the book "Python Programming: An
Introduction to Computer Science" (Franklin, Beedle & Associates).

LICENSE: This is open-source software released under the terms of the
GPL (http://www.gnu.org/licenses/gpl.html).

PLATFORMS: The package is a wrapper around Tkinter and should run on
any platform where Tkinter is available.

INSTALLATION: Put this file somewhere where Python can see it.

OVERVIEW: There are two kinds of objects in the library. The GraphWin
class implements a window where drawing can be done and various
GraphicsObjects are provided that can be drawn into a GraphWin. As a
simple example, here is a complete program to draw a circle of radius
10 centered in a 100x100 window:

--------------------------------------------------------------------
from graphics import *

def main():
    win = GraphWin("My Circle", 100, 100)
    c = Circle(Point(50,50), 10)
    c.draw(win)
    win.getMouse()#<----------------------------(pause) # Pause to view result
    win.close()    # Close window when done

main()
--------------------------------------------------------------------
GraphWin objects support coordinate transformation through the
setCoords method and pointer-based input through getMouse.

The library provides the following graphical objects:
    Point
    Line
    Circle
    Oval
    Rectangle
    Polygon
    Text
    Entry (for text-based input)
    Image

Various attributes of graphical objects can be set such as
outline-color, fill-color and line-width. Graphical objects also
support moving and hiding for animation effects.

The library also provides a very simple class for pixel-based image
manipulation, Pixmap. A pixmap can be loaded from a file and displayed
using an Image object. Both getPixel and setPixel methods are provided
for manipulating the image.

DOCUMENTATION: For complete documentation, see Chapter 4 of "Python
Programming: An Introduction to Computer Science" by John Zelle,
published by Franklin, Beedle & Associates.  Also see
http://mcsp.wartburg.edu/zelle/python for a quick reference"""
'''
Added Button(Rectangle, Text)
      Segment(Line)
      Slider
      PushButton(Button)
      
'''

try:  # import as appropriate for 2.x vs. 3.x
   import tkinter as tk
except:
   import Tkinter as tk
import matplotlib.mlab as ml
inPoly = ml.inside_poly
import time, os, sys
from numpy import power, sqrt, sin, cos, pi
import numpy as np
import numbers
import time

# Default values for various item configuration options. Only a subset of
#   keys may be present in the configuration dictionary for a given item
DEFAULT_CONFIG = {\
      "fill":"",
      "outline":"black",
      "width":"1",
      "arrow":"none",
      "text":"",
      "justify":"center",
      "font": ("helvetica", 12, "normal")}

EPS = 0.0000001
global ORIGIN
ORIGIN = None
LOG = True
formato = {'real': '%5.3f', # format for reals   display
           'int':  '%4d'  , # format for integers display
          }
##########################################################################
# Module Exceptions

class GraphicsError(Exception):
    """Generic error class for graphics module exceptions."""
    pass

OBJ_ALREADY_DRAWN  = "Object currently drawn"
UNSUPPORTED_METHOD = "Object doesn't support operation"
BAD_OPTION         = "Illegal option value"
DEAD_THREAD        = "Graphics thread quit unexpectedly"

_root = tk.Tk()
_root.withdraw()

#<------------------------------------------------------->#
#<---------------------- Utilities ---------------------->#
#<------------------------------------------------------->#
def update():
    _root.update()

#<------------------------------------------------------->#
def is_numeric(obj):
    attrs = ['__add__', '__sub__', '__mul__', '__div__', '__pow__']
    return all(hasattr(obj, attr) for attr in attrs)

#<------------------------------------------------------->#
def pause(canvas):
    while True:
        where = canvas.getMouse()
        print where

def to_right(v1, v2):
    return (v1[0]*v2[1] - v1[1]*v2[0]) < 0
    
#<------------------------------------------------------->#
def color_rgb(r,g,b):
    """r,g,b are intensities of red, green, and blue in range(256)
    Returns color specifier string for the resulting color"""
    return "#%02x%02x%02x" % (r,g,b)

def value_oredered_list(start, end, step):
    if type(step) == int:
        step = float(end-start)/float(step)
    scale = float(end-start)/step
    result = range(int(scale*start), int(scale*end)+1, int(scale*step))
    delta = start - result[0]/scale
    for i in range(len(result)):
        result[i] = result[i]/scale + delta
    return result
        
# orientation of the point (x,y) wrt segment (a0,b0)-(a1,b1)
# orientation of the point P = Point(x,y) with respect to the given 
# segment P_tail = Point(x0,y0) --> P_head = Point(x1,y1)
# results: 0: collinear   
#          1: within right half-plane   
#         -1: within left half-plane
# calculated with a simple cross product
def orientation(P, P_tail, P_head):
    x , y  = P.asTuple()
    x0, y0 = P_tail.asTuple()
    x1, y1 = P_head.asTuple()
    return cmp((x1-x0)*y + (y0-y1)*x + x0*y1-x1*y0, 0)
 
# The double winding number of the given path ps about the given point p0
def windingNumber(P, pathPoints):
    # Use the point P = Point(x,y) to divide the plane into two half-planes 
    # "above" (-1) and "below"(1) depending on which half-plane each point is in.
    # return "aligned" (0) if it's equivalent.
    h = [cmp(point, P) for point in pathPoints]
    w = 0
    for j in range(len(pathPoints)):
        i, k = (j-1)%len(pathPoints), (j+1)%len(pathPoints)  # Previous and next points in the path
        if h[j] * h[k] == 1:  # Both points are in the same half-plane, so do nothing
            pass
        elif h[j] * h[k] == -1:  # Crossing from one half-plane to the other
            w += orientation(P, pathPoints[j], pathPoints[k])
        elif h[j] == 0:  # p[j] == P
            if h[i] != h[k]:  # Cross from one half-plane to the other, don't change w
                pass
            else:  # Leave P through the same half-plane you entered through
                w += orientation(pathPoints[k], pathPoints[i], pathPoints[j])
        else:  # this pathPoints is leading either toward or away from P
            pass
    return w
 




############################################################################
# Graphics classes start here
############################################################################
        
#<------------------------------------------------------->#
class GraphWin(tk.Canvas):
#<------------------------------------------------------->#
    """A GraphWin is a toplevel window for displaying graphics."""

    def __init__(self, title="Graphics Window",
                 width=500, height=500, autoflush=True):
        master = tk.Toplevel(_root)
        master.protocol("WM_DELETE_WINDOW", self.close)
        tk.Canvas.__init__(self, master, width=width, height=height)
        self.master.title(title)
        self.pack()
        master.resizable(0,0)
        self.foreground = "black"
        self.items = []
        self.mouseX = None
        self.mouseY = None
        self.bind("<Button-1>", self._onClick)
        self.height = height
        self.width = width
        self.autoflush = autoflush
        self._mouseCallback = None
        self.trans = None
        self.closed = False
        master.lift()
        if autoflush: _root.update()
     
    def __checkOpen(self):
        if self.closed:
            raise GraphicsError("window is closed")

    def setBackground(self, color):
        """Set background color of the window"""
        self.__checkOpen()
        self.config(bg=color)
        self.__autoflush()
        
    def setCoords(self, x1, y1, x2, y2):
        """Set coordinates of window to run from (x1,y1) in the
        lower-left corner to (x2,y2) in the upper-right corner."""
        self.trans = Transform(self.width, self.height, x1, y1, x2, y2)

    def yUp(self):
        """Set coordinates of window to run from (0,0) in the
        lower-left corner and the window widthand height
        in the upper left corner, so y increases upward."""
        self.setCoords(0, 0, self.width, self.height)

    def close(self):
        """Close the window"""
        if self.closed: return
        self.closed = True
        self.master.destroy()
        self.__autoflush()

    def isClosed(self):
        return self.closed

    def isOpen(self):
        return not self.closed

    def __autoflush(self):
        if self.autoflush:
            _root.update()
    
    def plot(self, x, y, color="black"):
        """Set pixel (x,y) to the given color"""
        self.__checkOpen()
        xs,ys = self.toScreen(x,y)
        self.create_line(xs,ys,xs+1,ys, fill=color)
        self.__autoflush()
        
    def plotPixel(self, x, y, color="black"):
        """Set pixel raw (independent of window coordinates) pixel
        (x,y) to color"""
        self.__checkOpen()
        self.create_line(x,y,x+1,y, fill=color)
        self.__autoflush()
      
    def flush(self):
        """Update drawing to the window"""
        self.__checkOpen()
        self.update_idletasks()
        
    def getMouse(self, worldCoordinates = True):
        """Wait for mouse click and return Point object representing
        the click"""
        self.update()      # flush any prior clicks
        self.mouseX = None
        self.mouseY = None
        while self.mouseX == None or self.mouseY == None:
            self.update()
            if self.isClosed(): raise GraphicsError("getMouse in closed window")
            time.sleep(.1) # give up thread
        if worldCoordinates:
            x,y = self.toWorld(self.mouseX, self.mouseY)
        else:
            x,y = (self.mouseX, self.mouseY)
        self.mouseX = None
        self.mouseY = None
        return Point(x,y)

    def checkMouse(self):
        """Return last mouse click or None if mouse has
        not been clicked since last call"""
        if self.isClosed():
            raise GraphicsError("checkMouse in closed window")
        self.update()
        if self.mouseX != None and self.mouseY != None:
            x,y = self.toWorld(self.mouseX, self.mouseY)
            self.mouseX = None
            self.mouseY = None
            return Point(x,y)
        else:
            return None
            
    def promptMouse(self, x, y, prompt = 'Click Mopuse'):
        '''Temporarily place the prompt text at (x,y),
           and wait for and then return a mouse click.'''
        message = Text(Point(x, y), prompt)
        message.draw(self)
        pt = self.getMouse()
        message.undraw()
        return pt
    
    def promptClose(self, x, y=None):
        '''
        Place a prompt to close the window at (x,y)
        or if y is None, in existing Text object x,
        and close after a mouse click.
        '''
        prompt = 'Click anywhere to quit.'
        if isinstance(x, Text):
            x.setText(prompt)
            self.getMouse()
        else:    
            self.promptMouse(x, y, prompt)
        self.close()

    def getHeight(self):
        """Return the height of the window"""
        return self.height
        
    def getWidth(self):
        """Return the width of the window"""
        return self.width
    
    def toScreen(self, x, y):
        trans = self.trans
        if trans:
            return self.trans.screen(x,y)
        else:
            return x,y
                      
    def toWorld(self, x, y):
        trans = self.trans
        if trans:
            return self.trans.world(x,y)
        else:
            return x,y
        
    def setMouseHandler(self, func):
        self._mouseCallback = func
        
    def _onClick(self, e):
        self.mouseX = e.x
        self.mouseY = e.y
        if self._mouseCallback:
            self._mouseCallback(Point(e.x, e.y)) 
                      

#<------------------------------------------------------->#
class Transform:
#<------------------------------------------------------->#
    """Internal class for 2-D coordinate transformations"""
    
    def __init__(self, w, h, xlow, ylow, xhigh, yhigh):
        # w, h are width and height of window
        # (xlow,ylow) coordinates of lower-left [raw (0,h-1)]
        # (xhigh,yhigh) coordinates of upper-right [raw (w-1,0)]
        xspan = (xhigh-xlow)
        yspan = (yhigh-ylow)
        self.xbase = xlow
        self.ybase = yhigh
        self.xscale = xspan/float(w-1)
        self.yscale = yspan/float(h-1)
        
    def screen(self,x,y):
        # Returns x,y in screen (actually window) coordinates
        xs = (x-self.xbase) / self.xscale
        ys = (self.ybase-y) / self.yscale
        return int(xs+0.5),int(ys+0.5)
        
    def world(self,xs,ys):
        # Returns xs,ys in world coordinates
        x = xs*self.xscale + self.xbase
        y = self.ybase - ys*self.yscale
        return x,y


#<------------------------------------------------------->#
class GraphicsObject:
#<------------------------------------------------------->#
    """Generic base class for all of the drawable objects"""
    # A subclass of GraphicsObject should override _draw and
    #   and _move methods.
    
    def __init__(self, options):
        # options is a list of strings indicating which options are
        # legal for this object.
        
        # When an object is drawn, canvas is set to the GraphWin(canvas)
        #    object where it is drawn and id is the TK identifier of the
        #    drawn shape.
        self.canvas = None
        self.id = None

        # config is the dictionary of configuration options for the widget.
        config = {}
        for option in options:
            config[option] = DEFAULT_CONFIG[option]
        self.config = config
        
    def setFill(self, color):
        """Set interior color to color"""
        self._reconfig("fill", color)
        
    def setOutline(self, color):
        """Set outline color to color"""
        self._reconfig("outline", color)
        
    def setWidth(self, width):
        """Set line weight to width"""
        self._reconfig("width", width)

    def draw(self, graphwin):

        """Draw the object in graphwin, which should be a GraphWin
        object.  A GraphicsObject may only be drawn into one
        window. Raises an error if attempt made to draw an object that
        is already visible."""

        if self.canvas and not self.canvas.isClosed(): raise GraphicsError(OBJ_ALREADY_DRAWN)
        if graphwin.isClosed(): raise GraphicsError("Can't draw to closed window")
        self.canvas = graphwin
        self.id = self._draw(graphwin, self.config)
        if graphwin.autoflush:
            _root.update()
      
    def undraw(self):

        """Undraw the object (i.e. hide it). Returns silently if the
        object is not currently drawn."""
        
        if not self.canvas: return
        if not self.canvas.isClosed():
            self.canvas.delete(self.id)
            if self.canvas.autoflush:
                _root.update()
        self.canvas = None
        self.id = None

    def move(self, dx, dy):

        """move object dx units in x direction and dy units in y
        direction"""
        
        self._move(dx,dy)
        canvas = self.canvas
        if canvas and not canvas.isClosed():
            trans = canvas.trans
            if trans:
                x =  dx / trans.xscale 
                y = -dy / trans.yscale
            else:
                x = dx
                y = dy
            self.canvas.move(self.id, x, y)
            if canvas.autoflush:
                _root.update()
           
    def _reconfig(self, option, setting):
        # Internal method for changing configuration of the object
        # Raises an error if the option does not exist in the config
        #    dictionary for this object
        if option not in self.config:
            raise GraphicsError(UNSUPPORTED_METHOD)
        options = self.config
        options[option] = setting
        if self.canvas and not self.canvas.isClosed():
            self.canvas.itemconfig(self.id, options)
            if self.canvas.autoflush:
                _root.update()

    def _draw(self, canvas, options):
        """draws appropriate figure on canvas with options provided
        Returns Tk id of item drawn"""
        pass # must override in subclass

    def _move(self, dx, dy):
        """updates internal state of object to move it dx,dy units"""
        pass # must override in subclass

    def _selected(self, mousePosition):
        """checks if mouse is in the object's 'active' region"""
        pass # must override in subclass

         
#<------------------------------------------------------->#
class Point(GraphicsObject):
#<------------------------------------------------------->#
    global ORIGIN
    def __init__(self, X, Y):
        GraphicsObject.__init__(self, ["outline", "fill"])
        self.setFill = self.setOutline
        self.x = X
        self.y = Y
        
    def _draw(self, canvas, options):
        x,y = canvas.toScreen(self.x,self.y)
        return canvas.create_rectangle(x,y,x+1,y+1,options)
        
    def _move(self, dx, dy):
        self.x = self.x + dx
        self.y = self.y + dy
        
    def clone(self):
        other = Point(self.x,self.y)
        other.config = self.config.copy()
        return other
                
    def __str__(self):
        return "Point(%5.3f, %5.3f)" % (self.x, self.y)

    def __repr__(self):
        return str(self)

    def distance (self, other = ORIGIN):
        return sqrt(power((self.x - other.x), 2) \
               +    power((self.y - other.y), 2))
                
    def abs(self):
        # absolute length of vector ||v||
        # return sqrt(self.x*self.x + self.y*self.y)
        return self.distance(Point(0,0))     

#---------------------------------------------
# magics:
#---------------------------------------------
#--- Logical Operators
    def __eq__(self,other):
        if self.x==other.x and self.y==other.y:
            return True
        else:
            return False
    def __ne__(self,other):
        if self==other:
            return False
        else:
            return True
    def __gt__(self, other):
        # ||self|| > ||other|| ?
        if isinstance(other, Point):
            return (self.abs() > other.abs())
        elif type(other) == numbers.Real:
            return (self.abs() > other)
    def __lt__(self, other):
        # ||self|| < ||other|| ?
        if isinstance(other, Point):
            return (self.abs() < other.abs())
        elif type(other) == numbers.Real:
            return (self.abs() < other)
    def __cmp__(self, other):
        if self < other:
            return -1
        elif self > other:
            return 1
        else:
            return 0
#--- Arithmetic Operators
    def __neg__(self):
        return Point(-self.x, -self.y)
    def __sub__(self,other):
        return Point(self.x - other.x, self.y - other.y)
    def __add__(self,other):
        return Point(self.x + other.x, self.y + other.y)
    def __mul__(self, other):
        if type(other) == float or type(other) == int:
            return Point(self.x * other, self.y * other)
        else:
            raise TypeError, 'The arguments passed must be Numerical'
    def __rmul__(self, other):
        if type(other) == float or type(other) == int:
            return Point(self.x * other, self.y * other)
        else:
            raise TypeError, 'The arguments passed must be Numerical'
    def __div__(self, other):
        if is_numeric(other):
            return Point(self.x / other, self.y / other)
        else:
            raise TypeError, 'The arguments passed must be Numerical'
#--- Ingerface Operators
    def getX(self): return self.x
    def getY(self): return self.y
    def __hash__(self):
        return hash((self.x, self.y))
    def __getitem__(self, other):
        values = {0:self.x,1:self.y}
        return values.get(other)
    def __setitem__(self, idx, val):
        if idx == 0:
            self.x = val
        if idx == 1:
            self.y = val

    def normalized(self):
        Normalized = Point(0,0)
        if self.abs() != 0.0:
             Normalized = self / self.abs();
        return Normalized
    def dot(self, other):
        return (self.x*other.x + self.y*other.y)
    def asList(self):
        # return elements in a list
        return [self.x, self.y]
    def asTuple(self):
        # return elements in a list
        return self.x, self.y
    def asArray(self):
        # return elements as an array
        return np.array([self.x,self])

#<------------------------------------------------------->#
class _BBox(GraphicsObject):
#<------------------------------------------------------->#
    # Internal base class for objects represented by bounding box
    # (opposite corners) Line segment is a degenerate case.
    
    def __init__(self, p1, p2, options=["outline","width","fill"]):
        GraphicsObject.__init__(self, options)
        self.p1 = p1.clone()
        self.p2 = p2.clone()

    def _move(self, dx, dy):
        delta = Point(dx, dy)
        self.p1 = self.p1 + delta
        self.p2 = self.p2 + delta
                
    def getP1(self): return self.p1.clone()

    def getP2(self): return self.p2.clone()
    
    def getCenter(self):
        return (self.p2 + self.p1) / 2
    
    def getWidth(self):
        return abs(self.p2.x - self.p1.x)
    
    def getHeight(self):
        return abs(self.p2.y - self.p1.y)
    
    def area(self):
        return self.getHeight() * self.getWidth()
    
    def inRegion(self, mouseLocation):
        distance = mouseLocation  - self.getCenter()
        inside = (abs(distance.x) < self.getWidth()  /2 + EPS) \
             and (abs(distance.y) < self.getHeight() /2 + EPS)
        if LOG:
            print '->@_BBox.inRegion()\n mouse: %s,\n size: %s,\n diff: %s' \
                % (mouseLocation, Point(self.getWidth(),self.getHeight())/2, distance)
        if inside:
            if LOG:
                print '--> clicked inside region'
                for _ in range(10):
                    self.setFill('yellow')
                    time.sleep(0.2)
                    self.setFill('red')
                    time.sleep(0.2)                
            return True
        else:
            if LOG:print '--> clicked outside region'
            return False

    def findNearestPoint(self, to=Point(0.0, 0.0)):
        # should be overwritten by derived classes <- HERE!
        # return point of self (line) that is closest possible to 'to'
        vertices = (self.p1, Point(self.p2.x, self.p1.y), 
                    self.p2, Point(self.p1.x, self.p2.y))
        edges = (Segment(vertices[0], vertices[1]), 
                 Segment(vertices[1], vertices[2]), 
                 Segment(vertices[2], vertices[3]), 
                 Segment(vertices[3], vertices[0]))
        Pmin = self.getCenter()
        d_min = 9999
        if LOG: k = 0
        for edge in edges:
            d, P = edge.distance(to)
            if d < d_min:
                d_min = d
                Pmin = P
                if LOG:
                    print '%2d)new min distance found %4.2f @ %s'\
                        % (k, d_min,Pmin)
            if LOG: k += 1
        return Pmin

    def distance(self, P):
        # distance from segment to point
        closest = self.findNearestPoint(P)
        return P.distance(closest), closest

#<------------------------------------------------------->#
class Line(_BBox):
#<------------------------------------------------------->#
    def __init__(self, p1, p2):
        _BBox.__init__(self, p1, p2, ["arrow","fill","width"])
        self.setFill(DEFAULT_CONFIG['outline'])
        self.setOutline = self.setFill
        self.dir = (self.p2 - self.p1).normalized()
   
    def __str__(self):
        return "Line({0} : {1})".format(self.p1, self.p2)

    def __repr__(self):
        return str(self)

    def clone(self):
        other = Line(self.p1, self.p2)
        other.config = self.config.copy()
        return other
  
    def _draw(self, canvas, options):
        p1 = self.p1
        p2 = self.p2
        x1,y1 = canvas.toScreen(p1.x,p1.y)
        x2,y2 = canvas.toScreen(p2.x,p2.y)
        return canvas.create_line(x1,y1,x2,y2,options)
        
    def setArrow(self, option):
        if not option in ["first","last","both","none"]:
            raise GraphicsError(BAD_OPTION)
        self._reconfig("arrow", option)

    def head(self):
        return self.p1

    def tail(self):
        return self.p2

    def length (self):
        return self.p1.distance(self.p2)

    def getFraction(self, alpha = 0.5):
        return self.p1 + (self.p2 - self.p1) * alpha

    def findNearestPoint(self, to=Point(0.0, 0.0)):
        # return point of self (line) that is closest possible to 'to'
        P = self.tail
        vec = self.head - self.tail
        u = float(vec.dot(to - P))/float(vec.dot(vec))
        return (P + (u*vec))

    def __setitem__(self, idx, val):
        if idx == 0:
            self.p1 = val
        else:
            self.p2 = val
    def __getitem__(self, idx):
        if idx == 0:
            return self.p1
        else:
            return self.p2
               
#<------------------------------------------------------->#
class Segment(Line):
#<------------------------------------------------------->#
    def __init__(self, p1, p2):
        Line.__init__(self, p1, p2)
        self.setFill(DEFAULT_CONFIG['outline'])
        self.setOutline = self.setFill

    def findNearestPoint(self, to=Point(0.0, 0.0)):
        # return point of self (line) that is closest possible to 'to'
        P = self.tail()
        vec = self.head() - self.tail()
        print 'creating segment @', P, to
        u = float(vec.dot(to - P))/float(vec.dot(vec))
        if u < 0: 
            u = 0 
        elif u > 1: 
            u = 1
        return (P + (u*vec))

    def __str__(self):
        return "Segment({0} -> {1})".format(self.p1, self.p2)

    def __repr__(self):
        return str(self)

    def __setitem__(self, idx, val):
        if idx == 0:
            self.p1 = val
        else:
            self.p2 = val
    def __getitem__(self, idx):
        if idx == 0:
            return self.p1
        else:
            return self.p2
               
#<------------------------------------------------------->#
class Rectangle(_BBox):
#<------------------------------------------------------->#
    def __init__(self, p1, p2):
        _BBox.__init__(self, p1, p2)
    
    def _draw(self, canvas, options):
        p1 = self.p1
        p2 = self.p2
        x1,y1 = canvas.toScreen(p1.x,p1.y)
        x2,y2 = canvas.toScreen(p2.x,p2.y)
        return canvas.create_rectangle(x1,y1,x2,y2,options)
        
    def __str__(self):
        return "Rectangle({0}, {1})".format(self.p1, self.p2) 
                
    def clone(self):
        other = Rectangle(self.p1, self.p2)
        other.config = self.config.copy()
        return other

    def findNearestPoint(self, to=Point(0.0, 0.0)):
        # should be overwritten by derived classes <- HERE!
        # return point of self (line) that is closest possible to 'to'
        p1 = self.getP1()
        p2 = self.getP2()
        vertices = (p1, Point(p2.x, p1.y), 
                    p2, Point(p1.x, p2.y))
        edges = (Segment(vertices[0], vertices[1]), 
                 Segment(vertices[1], vertices[2]), 
                 Segment(vertices[2], vertices[3]), 
                 Segment(vertices[3], vertices[0]))
        Pmin = self.getCenter()
        d_min = 9999
        if LOG: k = 0
        for edge in edges:
            d, P = edge.distance(to)
            if d < d_min:
                d_min = d
                Pmin = P
                if LOG:
                    print '%2d)new min distance found %4.2f @ %s' % (k, d_min,Pmin)
            if LOG: k += 1
        return Pmin

    def inRegion(self, mouseLocation):
        distance = mouseLocation  - self.getCenter()
        inside = (abs(distance.x) < self.getWidth()  /2 + EPS) \
             and (abs(distance.y) < self.getHeight() /2 + EPS)
        if LOG:
            print '->@Rectangle.inRegion()\n mouse: %s,\n size: %s,\n diff: %s' \
                % (mouseLocation, Point(self.getWidth(),self.getHeight())/2, distance)
        if inside:
            if LOG:
                print '--> clicked inside'
                for _ in range(5):
                    self.setFill('yellow')
                    time.sleep(0.05)
                    self.setFill('red')
                    time.sleep(0.05)                
            return True
        else:
            if LOG:print '--> clicked outside'
            return False
        
#<------------------------------------------------------->#
class Oval(_BBox):
#<------------------------------------------------------->#
    def __init__(self, p1, p2):
        _BBox.__init__(self, p1, p2)
        
    def __str__(self):
        return "Oval({0}, {1})".format(self.p1, self.p2) 
                
    def clone(self):
        other = Oval(self.p1, self.p2)
        other.config = self.config.copy()
        return other
   
    def _draw(self, canvas, options):
        p1 = self.p1
        p2 = self.p2
        x1,y1 = canvas.toScreen(p1.x,p1.y)
        x2,y2 = canvas.toScreen(p2.x,p2.y)
        return canvas.create_oval(x1,y1,x2,y2,options)

    def findNearestPoint(self, to=Point(0.0, 0.0)):
        # should be overwritten by derived classes <- HERE!
        # return point of self (line) that is closest possible to 'to'
        vector = to - self.getCenter()
        if LOG:
            print '@Circle.findNearest()::\n mouse: ', to,
            print 'center@ ', self.getCenter(), ' radius; ', self.getRadius()
            print 'vector: ', vector, vector.abs()
            print '-> normalized: ', vector / vector.abs()
            print '-> intercept@ ', self.getCenter() + self.getRadius() * vector / vector.abs()
        return self.getCenter() + self.getRadius() * vector / vector.abs()

    def inRegion(self, mouseLocation):
        centro = self.getCenter()
        radio  = self.getRadius()
        distance = (mouseLocation - self.getCenter()).abs()
        if LOG:
            print '->@Oval.inRegion()\n mouse: %s,\n center: %s,\n radius: %s, diff: %4.2f' \
                % (mouseLocation, centro, radio, distance)
        if distance < radio:
            if LOG:
                print '--> clicked inside'
                for _ in range(5):
                    self.setFill('yellow')
                    time.sleep(0.05)
                    self.setFill('red')
                    time.sleep(0.05)                
            return True
        else:
            if LOG:
                print '--> clicked outside'
            return False
    
#<------------------------------------------------------->#
class Circle(Oval):
#<------------------------------------------------------->#
    def __init__(self, center, radius):
        p1 = Point(center.x-radius, center.y-radius)
        p2 = Point(center.x+radius, center.y+radius)
        Oval.__init__(self, p1, p2)
        self.radius = radius
        
    def clone(self):
        other = Circle(self.getCenter(), self.radius)
        other.config = self.config.copy()
        return other
        
    def getRadius(self):
        return self.radius
              
    def __str__(self):
        return "Circle({0}, {1})".format(self.getCenter(), self.radius) 
          
    def findNearestPoint(self, to=Point(0.0, 0.0)):
        # should be overwritten by derived classes <- HERE!
        # return point of self (line) that is closest possible to 'to'
        vector = to - self.getCenter()
        if LOG:
            print '@Circle.findNearest()::\n mouse: ', to,
            print 'center@ ', self.getCenter(), ' radius; ', self.getRadius()
            print 'vector: ', vector, vector.abs()
            print '-> normalized: ', vector / vector.abs()
            print '-> intercept@ ', self.getCenter() + self.getRadius() * vector / vector.abs()
        return self.getCenter() + self.getRadius() * vector / vector.abs()

    def inRegion(self, mouseLocation):
        distance = (mouseLocation - self.getCenter()).abs()
        if LOG:
            print '->@Circle.inRegion()\n mouse: %s,\n center: %s,\n radius: %s, diff: %4.2f' \
                % (mouseLocation, self.getCenter(), self.getRadius(), distance)
        if distance < self.getRadius():
            if LOG:
                print '--> clicked inside'
                for _ in range(5):
                    self.setFill('yellow')
                    time.sleep(0.05)
                    self.setFill('red')
                    time.sleep(0.05)               
            return True
        else:
            if LOG:
                print '--> clicked outside'
            return False

#<------------------------------------------------------->#
class Polygon(GraphicsObject):
#<------------------------------------------------------->#
    def __init__(self, *points):
        # if points passed as a list, extract it
        if len(points) == 1 and type(points[0]) == type([]):
            points = points[0]
        self.points = list(map(Point.clone, points))
        GraphicsObject.__init__(self, ["outline", "width", "fill"])
        
    def clone(self):
        other = Polygon(*self.points)
        other.config = self.config.copy()
        return other

    def getPoints(self):
        return list(map(Point.clone, self.points))

    def __str__(self):
        return "Polygon({0})".format(
               ", ".join([str(pt) for pt in self.points])) 

    def _move(self, dx, dy):
        for p in self.points:
            p.move(dx,dy)
   
    def getVertices(self):
        if LOG:
            print 'Vertices: '
            for i in range(len(self.points)):
                print i, self.points[i]
        return self.points
        
    def getHeight(self):
        """Return the height of the frame enclosing the polygon"""
        x_min =  np.inf
        x_max = -np.inf
        for p in self.points:
            if p.x < x_min: x_min = p.x
            if p.x > x_max: x_max = p.x
        return abs(x_max - x_min)
            
        return self.height
        
    def getWidth(self):
        """Return the width of the frame enclosing the polygon"""
        y_min =  np.inf
        y_max = -np.inf
        for p in self.points:
            if p.y < y_min: y_min = p.y
            if p.y > y_max: y_max = p.y
        return abs(y_max - y_min)
    
    def getEdges(self):
        edges = []
        if LOG:
            print 'Edges: '
        for i in range(len(self.points)-1):
            if LOG:
                print i, self.points[i], self.points[i+1]
            edges.append(Segment(self.points[i], self.points[i+1]))
        edges.append(Segment(self.points[i+1], self.points[0]))
        return edges
        
    def _draw(self, canvas, options):
        args = [canvas]
        for p in self.points:
            x,y = canvas.toScreen(p.x,p.y)
            args.append(x)
            args.append(y)
        args.append(options)
        return GraphWin.create_polygon(*args) 

#    def inRegion(self, mouseLocation):
#        distance = mouseLocation  - self.getCenter()
#        inside = (abs(distance.x) < self.getWidth()  /2 + EPS) \
#             and (abs(distance.y) < self.getHeight() /2 + EPS)
#        if LOG:
#            print '->@Polygon.inRegion()\n mouse: %s,\n size: %s,\n diff: %s' \
#                % (mouseLocation, Point(self.getWidth(),self.getHeight())/2, distance)
#        if inside:
#            if LOG:
#                print '->@Polygon.inRegion()--> clicked inside region'
#                for _ in range(10):
#                    self.setFill('yellow')
#                    time.sleep(0.2)
#                    self.setFill('red')
#                    time.sleep(0.2)                
#            return True
#        else:
#            if LOG:print '->@Polygon.inRegion()--> clicked outside region'
#            return False

    def inRegion(self, mouseLocation):
        if LOG:
            print '->@Polygon.inRegion()\n mouse: %s,\n size: %s' \
                % (mouseLocation, Point(self.getWidth(),self.getHeight())/2)
        windings = windingNumber(mouseLocation, self.getPoints())
        inside = bool(windings % 2) or (windings < 0)
        if LOG:
            print 'windings', windings
            if inside: 
                print 'is inside polygon'
                print '--> clicked inside'
                for _ in range(5):
                    self.setFill('yellow')
                    time.sleep(0.05)
                    self.setFill('red')
                    time.sleep(0.05)               
            else: 
                print 'is outside polygon'
        return inside

    def findNearestPoint(self, to=Point(0.0, 0.0)):
        # should be overwritten by derived classes <- HERE!
        # return point of self (line) that is closest possible to 'to'
        d_min = 9999
        if LOG: k = 0
        edges = self.getEdges()
        for edge in edges:
            d, P = edge.distance(to)
            if d < d_min:
                d_min = d
                Pmin = P
                if LOG:
                    print '%2d)new min distance found %4.2f @ %s' % (k, d_min,Pmin)
            if LOG: k += 1
        return Pmin

    def dist( self, P,  segment ):
           P0 = segment.getP1()
           P1 = segment.getP2()
           v = P1 - P0
           w = P - P0
           c1 = w * v
           c2 = v * v
           if ( c1 <= 0 ):  # before P0
               return P.distance(P0)
           if ( c2 <= c1 ): # after P1
               return P.distance(P1)
           b = c1 / c2
           Pb = P0 + b * v
           return P.distance(Pb)
           
    def distance(self, P):
        # distance from segment to point
        closest = self.findNearestPoint(P)
        return P.distance(closest), closest

    def getVertices(self):
        if LOG:
            print 'Vertices: '
        for i in range(len(self.points)):
            print i, self.points[i]
        return self.points

    def asList(self):
        # return elements in a list
        result = []
        for point in self.points:
            result.append([point.x, point.y])
        return result

    def asArray(self):
        # return elements as an array
        result = np.array([])
        for point in self.points:
            result.append([point.x, point.y])
        return result


#<------------------------------------------------------->#
class Text(GraphicsObject):
#<------------------------------------------------------->#
    def __init__(self, p, text):
        GraphicsObject.__init__(self, ["justify","fill","text","font"])
        self.setText(text)
        self.anchor = p.clone()
        self.setFill(DEFAULT_CONFIG['outline'])
        self.setOutline = self.setFill
        
    def _draw(self, canvas, options):
        p = self.anchor
        x,y = canvas.toScreen(p.x,p.y)
        return canvas.create_text(x,y,options)
        
    def _move(self, dx, dy):
        self.anchor.move(dx,dy)
        
    def clone(self):
        other = Text(self.anchor, self.config['text'])
        other.config = self.config.copy()
        return other

    def setText(self,text):
        self._reconfig("text", text)
        
    def getText(self):
        return self.config["text"]
            
    def getAnchor(self):
        return self.anchor.clone()

    def setFace(self, face):
        if face in ['helvetica','arial','courier','times roman']:
            f,s,b = self.config['font']
            self._reconfig("font",(face,s,b))
        else:
            raise GraphicsError(BAD_OPTION)

    def setSize(self, size):
        if 5 <= size <= 36:
            f,s,b = self.config['font']
            self._reconfig("font", (f,size,b))
        else:
            raise GraphicsError(BAD_OPTION)

    def setStyle(self, style):
        if style in ['bold','normal','italic', 'bold italic']:
            f,s,b = self.config['font']
            self._reconfig("font", (f,s,style))
        else:
            raise GraphicsError(BAD_OPTION)

    def setTextColor(self, color):
        self.setFill(color)

    def __str__(self):
        return "Text({0}, {1!r})".format(self.anchor, self.getText()) 


#<------------------------------------------------------->#
class Entry(GraphicsObject):
#<------------------------------------------------------->#
    def __init__(self, p, width):
        GraphicsObject.__init__(self, [])
        self.anchor = p.clone()
        #print self.anchor
        self.width = width
        self.text = tk.StringVar(_root)
        self.text.set("")
        self.fill = "gray"
        self.color = "black"
        self.font = DEFAULT_CONFIG['font']
        self.entry = None

    def _draw(self, canvas, options):
        p = self.anchor
        x,y = canvas.toScreen(p.x,p.y)
        frm = tk.Frame(canvas.master)
        self.entry = tk.Entry(frm,
                              width=self.width,
                              textvariable=self.text,
                              bg = self.fill,
                              fg = self.color,
                              font=self.font)
        self.entry.pack()
        #self.setFill(self.fill)
        return canvas.create_window(x,y,window=frm)

    def getText(self):
        return self.text.get()

    def _move(self, dx, dy):
        self.anchor.move(dx,dy)

    def getAnchor(self):
        return self.anchor.clone()

    def clone(self):
        other = Entry(self.anchor, self.width)
        other.config = self.config.copy()
        other.text = tk.StringVar()
        other.text.set(self.text.get())
        other.fill = self.fill
        return other

    def setText(self, t):
        self.text.set(t)

            
    def setFill(self, color):
        self.fill = color
        if self.entry:
            self.entry.config(bg=color)

            
    def _setFontComponent(self, which, value):
        font = list(self.font)
        font[which] = value
        self.font = tuple(font)
        if self.entry:
            self.entry.config(font=self.font)


    def setFace(self, face):
        if face in ['helvetica','arial','courier','times roman']:
            self._setFontComponent(0, face)
        else:
            raise GraphicsError(BAD_OPTION)

    def setSize(self, size):
        if 5 <= size <= 36:
            self._setFontComponent(1,size)
        else:
            raise GraphicsError(BAD_OPTION)

    def setStyle(self, style):
        if style in ['bold','normal','italic', 'bold italic']:
            self._setFontComponent(2,style)
        else:
            raise GraphicsError(BAD_OPTION)

    def setTextColor(self, color):
        self.color=color
        if self.entry:
            self.entry.config(fg=color)

    def __str__(self):
        return 'Entry({0}, {1})'.format(self.anchor, self.width) 


#<------------------------------------------------------->#
class Image(GraphicsObject):
#<------------------------------------------------------->#
    idCount = 0
    imageCache = {} # tk photoimages go here to avoid GC while drawn 
    
    def __init__(self, p, *pixmap):
        GraphicsObject.__init__(self, [])
        self.anchor = p.clone()
        self.imageId = Image.idCount
        Image.idCount = Image.idCount + 1
        if len(pixmap) == 1: # file name provided
            self.img = tk.PhotoImage(file=pixmap[0], master=_root)
        else: # width and height provided
            width, height = pixmap
            self.img = tk.PhotoImage(master=_root, width=width, height=height)
                
    def _draw(self, canvas, options):
        p = self.anchor
        x,y = canvas.toScreen(p.x,p.y)
        self.imageCache[self.imageId] = self.img # save a reference  
        return canvas.create_image(x,y,image=self.img)
    
    def _move(self, dx, dy):
        self.anchor.move(dx,dy)
        
    def undraw(self):
        try:
            del self.imageCache[self.imageId]  # allow gc of tk photoimage
        except KeyError:
            pass
        GraphicsObject.undraw(self)

    def getAnchor(self):
        return self.anchor.clone()
        
    def clone(self):
        other = Image(Point(0,0), 0, 0)
        other.img = self.img.copy()
        other.anchor = self.anchor.clone()
        other.config = self.config.copy()
        return other

    def getWidth(self):
        """Returns the width of the image in pixels"""
        return self.img.width() 

    def getHeight(self):
        """Returns the height of the image in pixels"""
        return self.img.height()

    def getPixel(self, x, y):
        """Returns a list [r,g,b] with the RGB color 
        values for pixel (x,y)
        r,g,b are in range(256)

        """
        
        value = self.img.get(x,y) 
        if type(value) ==  type(0):
            return [value, value, value]
        else:
            return list(map(int, value.split())) 

    def setPixel(self, x, y, color):
        """Sets pixel (x,y) to the given color
        
        """
        self.img.put("{" + color +"}", (x, y))
        

    def save(self, filename):
        """Saves the pixmap image to filename.
        The format for the save image is 
        determined from the filname extension.
        """
        
        path, name = os.path.split(filename)
        ext = name.split(".")[-1]
        self.img.write( filename, format=ext)

        
def test_point_in_Region(grapho, base = Point(0,0), name = 'unamed object'):
    global win
    print 'CLick to see if inside or outside ', name
    print 'Using the winding-number method'
    L = None
    while True:
        location = win.getMouse()
        print '--> mouse clicked @ ', location
        howFar, where = grapho.distance(location)
        print ' closest point: %s, distance = %4.2f' % (where, howFar)
        if grapho.inRegion(location):
            print '<-- clicked inside!\n .... bye bye.'
            if not (L == None):
                L.undraw()
            grapho.undraw()
            break
        else:
            print '--> clicked outside'
            if not (L == None):
                L.undraw()
            L = Line (location, where)
            L.draw(win)
    base.undraw()
    
#<------------------------------------------------------->#
global win
def test():
#<------------------------------------------------------->#
    global win
    ORIGIN = Point(0.0, 0.0)
    win = GraphWin()
    win.setCoords(-10,-10,10,10)
    print 'Starting ...'
    
    t = Text(Point(0,0), "Centered Text")
    t.setSize(24)
    t.draw(win)
    print 'Drawn centered text'
    e = Entry(Point(5,6), 10)
    e.draw(win)
    print 'Drawn text entry point'
    
    print 'Click mouse'
    where = win.getMouse()#<--------------------(pause)
    p = Polygon(where + Point( 1, 1), 
                where + Point(-2, 3), 
                where + Point( 2,-5))
    p.draw(win)
    print 'Drawn polygon'
    p.setFill("red")
    p.setOutline("blue")
    p.setWidth(2)
    s = "Polygon Points: "
    for pt in p.getPoints():
        s = s + "(%0.1f,%0.1f) " % (pt.getX(), pt.getY())
    print 's:', s
    t.setText(e.getText())
    e.setFill("green")
    e.setText("Spam!")
    for step in range(10):
        e.move(-0.2,-0.5)
        p.move(0.2,0.3)
        time.sleep(0.2)
    print 'Click to continue ...'
    where = win.getMouse()#<--------------------(pause)
    s = "Clicked at: "
    s = s + "(%0.1f,%0.1f) " % (where.x, where.y)
    t.setText(s)
    
    print 'Click mouse to clear'
    where = win.getMouse()#<--------------------(pause)
    p.undraw()
    e.undraw()
    
#-----------------------------------------------------------------

    Ccenter = Point(-2, -5)
    Ccenter.draw(win)
    
    cir = Circle(Ccenter, 2.5)
    cir.setOutline('red')
    cir.setFill('blue')
    cir.draw(win)
    
    print 'simpleGraphics:: Circle Drawn.'

    Rcenter = Point(-5, 2)
    Rcenter.draw(win)
    size = Point(2, 3)
    
    rect = Rectangle(Rcenter+size, Rcenter-size)
    rect.setOutline('blue')
    rect.setFill('yellow')
    rect.draw(win)
    
    print 'simpleGraphics:: Rectangle Drawn.'

    Plocation = Point(-6, 0)
    Plocation.draw(win)
    p0 = Point(-2.0, 0.0) + Plocation
    p1 = Point(-1.0, 3.0) + Plocation
    p2 = Point(-2.0, 4.0) + Plocation
    p3 = Point( 1.0, 2.0) + Plocation
    p4 = Point( 2.0,-1.0) + Plocation
    p5 = Point( 2.0,-2.0) + Plocation
    p6 = Point( 3.0,-3.0) + Plocation
    p7 = Point(-2.0,-4.0) + Plocation
    p8 = Point(-1.0,-1.0) + Plocation
    polyPoints = [p0, p1, p2, p3, p4, p5, p6, p7, p8]
    poly = Polygon(polyPoints)
    poly.setOutline('black')
    poly.setFill('cyan')
    poly.draw(win)

    print 'simpleGraphics:: Polygon_0 Drawn.'

    print '... printing vertices of Polygon_0'
    edges = poly.getVertices()
    
    print '... printing edges of Polygon_0'
    edges = poly.getEdges()
    
    e0 = edges[0]
    print 'e[0], e[1]', e0[0], e0[1]
    print 'e[0].x, e[0].y', e0[0].x, e0[0].y
    
    points = [Point( 3, 0), Point(10,-2), Point( 1, 3),  
              Point( 3, 3), Point( 2, 4), Point( 4, 2),  
              Point( 7, 5), Point( 6, 7), Point( 5, 5),  
              Point( 3, 8), Point( 9, 3), Point(10, 7),  
              Point( 8, 7), Point(-2,-1), Point( 1, 8),  
              Point( 0, 4)]

    p_points = []
    for point in points:
        p_points.append([point.x, point.y])

#-----------------------------------------------

    #-----------------------------------------------
    # Polygon 1: simple convex
    #-----------------------------------------------

    poly1 = [Point( 1, 1), Point( 4, 2), Point(10, 2),
             Point( 9, 9), Point( 7, 8), Point( 7, 4), 
             Point( 5, 5), Point( 3, 8), Point( 0, 4)]
    
    p_poly1 = []
    c1 = Point(-3,2)
    c1.draw(win)
    for point in poly1:
        point = point + c1
        p_poly1.append([point.x, point.y])

    print '\n Polygon_1 vertices:'
    for i in range(len(poly1)):
        print 'poly1[%d]: %s' %(i, poly1[i])

    poly_1 = Polygon(poly1)
    poly_1.setOutline('green')
    poly_1.setFill('gray')
    poly_1.draw(win)
    
    print 'simpleGraphics:: Polygon_1 Drawn.'

    #-----------------------------------------------
    # Polygon 2: complex (non convex, no overlap)
    #-----------------------------------------------
    
    poly2 = [Point(1, 1), Point(4, 2), Point(5, 5), 
             Point(3, 8), Point(0, 4)]
    
    p_poly2 = []
    c2 = Point ( 0, 0)
    c2.draw(win)
    for point in poly2:
        point = point + c2
        p_poly2.append([point.x, point.y])

    print '\n Polygon_2 vertices:'
    for i in range(len(poly2)):
        print 'poly2[%d]: %s' %(i, poly2[i])
    poly_2 = Polygon(poly2)
    poly_2.setOutline('red')
    poly_2.setFill('purple')
    poly_2.draw(win)
    
    print 'simpleGraphics:: Polygon_2 Drawn.'

    #-----------------------------------------------
    # Polygon 3: more complex (non convex, overlap)
    #-----------------------------------------------
    
    poly3 = [Point( 0, 0), Point( 8, 0), Point( 8, 4),
             Point( 2, 4), Point( 2, 3), Point( 6, 3), 
             Point( 6, 2), Point( 4, 2), Point( 4, 9),
             Point( 0, 9)]

    p_poly3 = []
    c3 = Point(-5,-6)
    c3.draw(win)
    for point in poly3:
        point = point + c3
        p_poly3.append([point.x, point.y])

    print '\n Polygon_3 vertices:'
    for i in range(len(poly3)):
        print 'poly2[%d]: %s' %(i, poly3[i])

    poly_3 = Polygon(poly3)
    poly_3.setOutline('purple')
    poly_3.setFill('pink')
    poly_3.draw(win)
    
    print 'simpleGraphics:: Polygon_3 Drawn.'

#-----------------------------------------------------------------

    print 50*'_' + '\n' + 50*'|' + '\n' + 50*'V'
    print '--------------------------------------------------------'
    print 'CLick to condinue .... NEXT: testing click inside object'
    print '--------------------------------------------------------'
    where = win.getMouse()#<--------------------(pause)
   
#-----------------------------------------------------------------
    print '--------------------------------------------------------'
    print 'CLick to see if inside or outside circle<---------------'
    print '--------------------------------------------------------'
    L = None
    while True:
        location = win.getMouse()
        howFar, where = cir.distance(location)
        print 'mouse clicked @ ', location        
        if cir.inRegion(location):
            print '<-- clicked inside!\n .... bye bye.'
            if not (L == None):
                L.undraw()
            cir.undraw()
            break
        else:
            print '--> clicked outside'
            if not (L == None):
                L.undraw()
            L = Line (location, where)
            L.draw(win)
    Ccenter.undraw()

#-----------------------------------------------------------------

 #-----------------------------------------------------------------
    print '--------------------------------------------------------'
    print 'CLick to see if inside or outside rectangle<------------'
    print '--------------------------------------------------------'
    L = None
    while True:
        location = win.getMouse()
        print '--> mouse clicked @ ', location
        howFar, where = rect.distance(location)
        print ' closest point: %s, distance = %4.2f' % (where, howFar)
        if rect.inRegion(location):
            print '<-- clicked inside!\n .... bye bye.'
            if not (L == None):
                L.undraw()
            rect.undraw()
            break
        else:
            print '--> clicked outside'
            if not (L == None):
                L.undraw()
            L = Line (location, where)
            L.draw(win)
    Rcenter.undraw()

#-----------------------------------------------------------------

##-----------------------------------------------------------------
##-----------------------------------------------------------------
##-----------------------------------------------------------------
#
#    p_points = [[ 3, 0], [10,-2], [ 1, 3],  
#               [ 3, 3], [ 2, 4], [ 4, 2],  
#               [ 7, 5], [ 6, 7], [ 5, 5],  
#               [ 3, 8], [ 9, 3], [10, 7],  
#               [ 8, 7], [-2,-1], [ 1, 8],  
#               [ 0, 4]]
#
#    #-----------------------------------------------
#    
#    p_poly = [[ 1, 1], [ 4, 2], [10, 2], 
#              [ 9, 9], [ 7, 8], [ 7, 4], 
#              [ 5, 5], [ 3, 8], [ 0, 4]]
#    
#
#    #-----------------------------------------------
#    
#    poly3  = [[ 0, 0], [ 8, 0], [ 8, 4], 
#              [ 2. 4], [ 2, 3], [ 6, 3], 
#              [ 6, 2], [ 4, 2], [ 4,10],
#              [ 0,10]]
#    
#    p_poly = poly_2.asList()
#
#    # return elements in a list
#    p_points = []
#    for point in points:
#        p_points.append([point.x, point.y])
#
#    print 50*'*'
#    print 'Polygon_2'
#    print 50*'*'
#
#    print '\n Selected Points:'
#    for i in range(len(p_points)):
#        print 'point[%d]: %s' %(i, p_points[i])
#
#    print '\n Polygon Points:'
#    for i in range(len(p_poly)):
#        print 'point[%d]: %s' %(i, p_poly[i])
#
#    results = inPoly(p_points,p_poly)
#    print 50*'*'
#    print '\n Selected Points inside Polygon_1:'
#    for i in results:
#        print 'point[%d]: %s' %(i, p_points[i])
#
#    print 50*'*'
#    print 50*'*'
#
##-----------------------------------------------------------------
##-----------------------------------------------------------------
##-----------------------------------------------------------------
#
#    print 50*'*'
#    print 'Polygon_2 again!'
#    print 50*'*'
#    
#    print '\n Selected Points:'
#    for i in range(len(p_points)):
#        print 'point[%d]: %s' %(i, p_points[i])
#
#    print '\n Polygon Points:'
#    for i in range(len(p_poly)):
#        print 'point[%d]: %s' %(i, p_poly[i])
#
#    results = inPoly(p_points,p_poly)
#    print 50*'*'
#    print '\n Selected Points inside Polygon_1:'
#    for i in results:
#        print 'point[%d]: %s' %(i, p_points[i])
#
#    print 50*'*'
#    print 50*'*'
#
#    print 'Click to continue ...'
#    where = win.getMouse()#<--------------------(pause)
#
#
##-----------------------------------------------------------------
##-----------------------------------------------------------------
##-----------------------------------------------------------------

    print '--------------------------------------------------------'
    print 'CLick to see if inside or outside polygon<--------------'
    print '--------------------------------------------------------'
    L = None
    while True:
        location = win.getMouse()
        print '--> mouse clicked @ ', location
        howFar, where = poly.distance(location)
        print ' closest point: %s, distance = %4.2f' % (where, howFar)
        if poly.inRegion(location):
            print '<-- clicked inside!\n .... bye bye.'
            if not (L == None):
                L.undraw()
            poly.undraw()
            break
        else:
            print '--> clicked outside'
            if not (L == None):
                L.undraw()
            L = Line (location, where)
            L.draw(win)
    Plocation.undraw()

#-----------------------------------------------------------------

    print 'Polygon_1'
    
    points1 = points
              
    print '\n Selected Points:'
    for i in range(len(points1)):
        print 'point[%d]: %s' %(i, points1[i])
    
    print '--------------------------------------------------------'
    print 'CLick to see if inside or outside polygon 1<------------'
    print '--------------------------------------------------------'
#    results = inPoly(points1,poly1)
#    print '\n Selected Points inside Polygon_1:'
#    for i in range(len(results)):
#        print 'point[%d]: %s' %(i, points2[i])
#    
#    for i in range(len(poly1)):
#        v1 = poly1[i-1]
#        v2 = poly1[i]
#        for p in points1:
#            if(to_right(v2-v1, p-v1)):
#                points1.remove(p)
#    
#    print '\n Selected Points after running Polygon_1:'
#    for i in range(len(points1)):
#        print 'point[%d]: %s' %(i, points1[i])
    
    test_point_in_Region(poly_1, base = c1, name = 'polygon_1')    
#    print 'CLick to see if inside or outside polygon_1'
#    print 'Using to-the-right method'
#    L = None
#    while True:
#        location = win.getMouse()
#        print '--> mouse clicked @ ', location
#        howFar, where = poly_1.distance(location)
#        print ' closest point: %s, distance = %4.2f' % (where, howFar)
#        if poly_1.inRegion(location):
#            print '<-- clicked inside!\n .... bye bye.'
#            if not (L == None):
#                L.undraw()
#            poly_1.undraw()
#            break
#        else:
#            print '--> clicked outside'
#            if not (L == None):
#                L.undraw()
#            L = Line (location, where)
#            L.draw(win)
#    c1.undraw()

#-----------------------------------------------------------------

    print 'Polygon_2'
    
    points2 = points

    print '\n Selected Points:'
    for i in range(len(points2)):
        print 'point[%d]: %s' %(i, points2[i])
    
    print '--------------------------------------------------------'
    print 'CLick to see if inside or outside polygon 2<------------'
    print '--------------------------------------------------------'
#    results = inPoly(points2,poly2)
#    print '\n Selected Points inside Polygon_2:'
#    for i in range(len(results)):
#        print 'point[%d]: %s' %(i, points2[i])
#    
#    for i in range(len(poly2)):
#        v1 = poly2[i-1]
#        v2 = poly2[i]
#        for p in points2:
#            if(to_right(v2-v1, p-v1)):
#                points2.remove(p)
#    
#    print '\n Selected Points after running Polygon_2:'
#    for i in range(len(points2)):
#        print 'point[%d]: %s' %(i, points2[i])
 
    test_point_in_Region(poly_2, base = c2, name = 'polygon_2')    
#    print 'CLick to see if inside or outside polygon_2'
#    print 'Using to-the-right method'
#    L = None
#    while True:
#        location = win.getMouse()
#        print '--> mouse clicked @ ', location
#        howFar, where = poly_2.distance(location)
#        print ' closest point: %s, distance = %4.2f' % (where, howFar)
#        if poly_2.inRegion(location):
#            print '<-- clicked inside!\n .... bye bye.'
#            if not (L == None):
#                L.undraw()
#            poly_2.undraw()
#            break
#        else:
#            print '--> clicked outside'
#            if not (L == None):
#                L.undraw()
#            L = Line (location, where)
#            L.draw(win)
#    c2.undraw()

#-----------------------------------------------------------------

    print 'Polygon_3'
    
    points3 = points

    print '\n Selected Points:'
    for i in range(len(points3)):
        print 'point[%d]: %s' %(i, points3[i])
    
    print '--------------------------------------------------------'
    print 'CLick to see if inside or outside polygon 2<------------'
    print '--------------------------------------------------------'
#    results = inPoly(points3,poly3)
#    print '\n Selected Points inside Polygon_2:'
#    for i in range(len(results)):
#        print 'point[%d]: %s' %(i, points3[i])
#    
#    for i in range(len(poly3)):
#        v1 = poly3[i-1]
#        v2 = poly3[i]
#        for p in points3:
#            if(to_right(v2-v1, p-v1)):
#                points3.remove(p)
#    
#    print '\n Selected Points after running Polygon_2:'
#    for i in range(len(points2)):
#        print 'point[%d]: %s' %(i, points3[i])
 
    test_point_in_Region(poly_3, base = c3, name = 'polygon_3')    
#    print 'CLick to see if inside or outside polygon_3'
#    print 'Using to-the-right method'
#    L = None
#    while True:
#        location = win.getMouse()
#        print '--> mouse clicked @ ', location
#        howFar, where = poly_3.distance(location)
#        print ' closest point: %s, distance = %4.2f' % (where, howFar)
#        if poly_3.inRegion(location):
#            print '<-- clicked inside!\n .... bye bye.'
#            if not (L == None):
#                L.undraw()
#            poly_3.undraw()
#            break
#        else:
#            print '--> clicked outside'
#            if not (L == None):
#                L.undraw()
#            L = Line (location, where)
#            L.draw(win)
#    c3.undraw()

#-----------------------------------------------------------------

    print 'Click to read the mouse position ...'
    where = win.getMouse()#<--------------------(pause)
    e.setText(where)
    t.setStyle("bold")
    t.setFace("arial")
    t.setSize(32)
    e.move(0,-2)

#-----------------------------------------------------------------

    print 'Click to continue ...'
    where = win.getMouse()#<--------------------(pause)
    print 'Drawing lines'
    p0 = Point(2.0, 1.0)
    print '>>> p0 = Point(2,1)'
    print '>>> p0\n', p0
    p1 = Point(7.0, 5.0)
    print '>>> p1 = Point(2,3)'
    print '>>> p1\n', p1
    p2 = Point(2.0, 4.0)
    print '>>> p2 = Point(5,7)'
    print '>>> p2\n', p2
    print '>>> p2[1]\n', p2[1]
    p3 = Point(1.0, 2.0)
    L1 = Line (p1, p2)
    L1.draw(win)
    time.sleep(0.5)
    L2 = Line (p1, p3)
    L2.draw(win)
    time.sleep(0.5)
    L3 = Line(p3, p2)
    L3.draw(win)
    time.sleep(0.5)
    print 'L3:', L3
    print 'L3.dir:', L3.dir
    H = win.getHeight()/2
    V = win.getWidth()/2
    print 'H: %s, V: %s' % (H, V)
    print 'click at the origin:'    

#-----------------------------------------------------------------

    where = win.getMouse(True)#<--------------------(pause)
    print 'clicked at:', where
    origin = where
    print 'origin', origin
    origin.draw(win)
    print 'click at the outter point:'    

#-----------------------------------------------------------------

    where = win.getMouse(True)#<--------------------(pause)
    outter = where
    print 'outter point:', outter
    outter.draw(win)
    L4 = Line(origin, outter)
    print 'radius = %5.3f,' % L4.length()
    L4.draw(win)
    for a in range(73):
        r = 2*pi*a/36
        print a ,r
        origin.move(sin(r),cos(r))
        L4.undraw()
        L4 = Line(origin, outter)
        L4.draw(win)
        time.sleep(0.1)

#-----------------------------------------------------------------

    win.promptMouse(0, 5, 'Click to Finish')
    win.promptClose(0, -3) 
    print ' ... done!'
    win.close()

if __name__ == "__main__":
    global win
    test()
