# -*- coding: utf-8 -*-
"""
File: aboutGraphics.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
Description: 
--------------------------------------------------------
Inputs: 
--------------------------------------------------------
Outputs: 
--------------------------------------------------------
"""
from simpleGraphics import *

def face():
    canvas = GraphWin('Face', 200, 150) # give title and dimensions
    canvas.yUp() # make right side up coordinates!

    head = Circle(Point(40,100), 25) # set center and radius
    head.setFill("yellow")
    head.draw(canvas)

    eye1 = Circle(Point(30, 105), 5)
    eye1.setFill('blue')
    eye1.draw(canvas)

    eye2 = Line(Point(45, 105), Point(55, 105)) # set endpoints
    eye2.setWidth(3)
    eye2.draw(canvas)

    mouth = Oval(Point(30, 90), Point(50, 85)) # set corners of bounding box
    mouth.setFill("red")
    mouth.draw(canvas)

    label = Text(Point(100, 120), 'A face')
    label.draw(canvas)

    message = Text(Point(canvas.getWidth()/2, 20), 'Click anywhere to quit.')
    message.draw(canvas)
    where = canvas.getMouse()
    return where

def inRegion(graphObject, mouseLocation):
    print 'mouseLocation: ',  mouseLocation
    centro = graphObject.getCenter()
    print 'self.getCenter: ', centro
    centro = Point(x, y)
    print 'centro = self.getCenter: ', centro
    #locale = Point(mouseLocation)
    distance = (locale - centro).length
    if distance < self.getRadius:
        print 'clicked inside'
        for _ in range(10):
            self.setFill('yellow')
            time.sleep(0.2)
            self.setFill('red')
            time.sleep(0.2)                
        return True
    else:
        return False

canvas = GraphWin()

pt = Point(100, 50)
pt.draw(canvas)

cir = Circle(pt, 25)
cir.draw(canvas)

cir.setOutline('red')
cir.setFill('blue')

line = Line(pt, Point(150, 100))
line.draw(canvas)

rect = Rectangle(Point(20, 10), pt)
rect.draw(canvas)

line.move(10, 40)

print('cir:', cir)
print('line:', line)
print('rect:', rect)

while True:
    where = canvas.getMouse()
    print where
    donde = face()
    print 'face @', donde

canvas.close()
print '... done!'




