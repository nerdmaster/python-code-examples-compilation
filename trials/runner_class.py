# -*- coding: utf-8 -*-
# File: runner_class.py
"""
Created on Fri Sep 22 12:00:04 2014
@author: benito
Runner Example for magic methods
"""

class RaceStatistics:
    def __init__(self, distance, calories):
        self.distance = distance
        self.calories = calories

    def __radd__(self, other):
        if other.__class__ == self.__class__:
            return RaceStatistics(self.distance + other.distance, self.calories + other.calories)
        else:
            return self

class Runner:
    def __init__(self, name):
        self.name = name
        self.races = []

    def run(self, meters):
        pass

    def __str__(self):
    	return "Runner %s" % self.name

    def add_race(self, distance, calories):
    	new_run = RaceStatistics(distance, calories)
    	self.races.append(new_run)

    def get_all_races_statistics(self):
    	return sum(self.races)

    def get_total_distance(self):
    	return self.get_all_races_statistics().distance

    def get_total_calories(self):
    	return self.get_all_races_statistics().calories

    def __cmp__(self, other):
    	return self.get_total_distance() == other.get_total_distance()

    def __lt__(self, other):
    	return self.get_total_distance() < other.get_total_distance()

    def __gt__(self, other):
    	return self.get_total_distance() > other.get_total_distance()

def showRun(runner):
    print '%s ran %s [Km]' % (runner.name, runner.get_total_distance())

def printDashes():
    print '\n' + 30*'-' + '\n'

# test::

pete = Runner("Peter")
pete.add_race(distance=15, calories=150)
pete.add_race(distance=20, calories=450)
pete.add_race(distance=14, calories=250)

carlos = Runner("Carlos")
carlos.add_race(distance=15, calories=150)
carlos.add_race(distance=20, calories=450)
carlos.add_race(distance=14, calories=250)

print pete
#> Runner Peter

print '%s ran %s [Km]' % (pete.name, pete.get_total_distance())
#> 49

print pete.get_total_calories()
#> 850
printDashes()

print pete == carlos
#> True

showRun(carlos)
carlos.add_race(distance=14, calories=250)
showRun(carlos)

printDashes()

if pete > carlos:
    print "%s is the best runner" % pete
elif pete < carlos:
    print "%s is the best runner" % carlos
else:
    print "%s and %s are both good" % (pete, carlos)

#> Runner Carlos is the best runner





