# -*- coding: utf-8 -*-
"""
Created on Tue Oct 14 16:53:06 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""
try:
    import networkx as nx
except ImportError:
    raise ImportError("bondGraph requires networkx")

try:
    import matplotlib as plt
except ImportError:
    raise ImportError("bondGraph requires matplotlib")

import random as rnd

canvas = plt.interactive(1)
#plt.draw()

BG = nx.Graph()

BG.add_edge(1,2)
print(sorted(BG.edges()))
#    [(1, 2)]

BG.add_node(42)
print(sorted(BG.nodes()))
#    [1, 2, 42]

BG.add_path([0,1,2,3,4,7,42])
BG.add_edge(1,7)

print BG.edges()

BG.edges(data=True)
print BG.edges()

print BG.nodes()
print BG.name
    
class BondGraph():
    _bondGraph = nx.Graph()
    def __init__(self,bg):
        _bondGraph = bg
        print 'calling BG __init__'
    def __repr__(self):
        print 'calling BG __repr__'
        return _bondGraph.nodes()

bg = BondGraph(BG)

print bg._bondGraph

#<---------------------------------------------->#
G = nx.Graph()

# construct weighted graph representing preferences
size_set_A = 4
size_set_B = 6
for i in range(0,size_set_A):
    for j in range(0,size_set_B):
        G.add_edge(i,size_set_A+1+j,weight=rnd.uniform(0,1))

# find the optimal matching
G2 = nx.Graph()
M = nx.max_weight_matching(G)
for i in range(0,size_set_A):
    G2.add_edge(i,M[i])

# specify which position to plot each vertex
pos = {}
for i in range(0,size_set_A):
    pos[i] = (i,1)

for j in range(0,size_set_B):
    pos[size_set_A+j+1] = (j,2)

print '    drawing the graph ...'
# draw the graph
nx.draw(G,pos)
print '... drawing the graph ...'
nx.draw_networkx_edges(G2,pos,alpha=0.4,edge_color='r',width=10)


print '... done!'

#<---------------------------------------------->#

print '... really done!'

