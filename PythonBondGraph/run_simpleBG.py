# -*- coding: utf-8 -*-
"""
File: 'run_simpleBG.py'
Created on Tue Oct 14 22:59:06 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""
print ' starting pyBondGraphs ...'

try:
    import networkx as Bondgraph
except ImportError:
    raise ImportError("bondGraph requires networkx")

try:
    import matplotlib.pylab as plt
except ImportError:
    raise ImportError("bondGraph requires matplotlib")

import random as rnd
import matplotlib.image as pig

from Bondgraphs import gtnn
from gtnn import *


g = Bondgraph(' single mass/spring/damper ') 
c = g.addNode(Capacitor(1.96, ’spring’))
r = g.addNode(Resistor(0.02, ’damper’))
i = g.addNode(Inertia(0.002, ’mass’))
j = g.addNode(CommonFlowJunction()) 
s = g.addNode(EffortSource( ’E’))

g.bondNodes(j , c) 
g.bondNodes(j , r) 
g.bondNodes(j , i) 
g.bondNodes(s, j)
g.assignCausality() 
g.numberElements()

print StateSpace(g.equations ())

print '... done!'
