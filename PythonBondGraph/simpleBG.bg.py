
#title single mass/spring/damper chain .
#

from BondGraph import *
#from BondGraph.BGconstants import *


bg = BondGraph()

### nodes ##
#
## Spring , damper and mass . 
C1 = BGelement(Name = 'spring-1',
               Type = StorageType.Capacitor,
               Position = [2,2])
C1.setModulus(1.96)

print 'so far ... ', C1

#begin node
#       ￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼id spring
#    type C 
#    parameter 1.96
#end node 
#
#begin node
#    id damper 
#    type R 
#    parameter 0.02
#end node
#
#begin node 
#    id mass
#    type I
#    parameter 0.002
#end node
#
## Common flow point joining s/d/m
#begin node
#    id point1
#    type cfj 
#end node
#
## ￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼# Input force , acting on spring and damper 
#begin node
#    id force 
#    type Se 
#    parameter Y
#end node
#
### bonds ##
#
## Spring and damper joined to mass 
#begin bond
#    t￼￼￼￼￼￼￼￼￼￼￼￼tail point1 
#    head spring
#end bond
#
#begin bond
#    tail point1
#    head damper 
#end bond
#
#begin bond
#    tail point1
#    head mass 
#end bond
#
#begin bond 
#    tail force
#    head point1 
#end bond
#
## EOF

print '...done!'