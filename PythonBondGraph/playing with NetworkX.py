# -*- coding: utf-8 -*-
"""
Created on Wed Oct 15 09:33:25 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""

from BondGraphs import *
import networkx as nx

G=nx.Graph()
G.add_node(1)
G.add_nodes_from([2,3])
H=nx.path_graph(10)

print '---------------------------------------'
print ' A graph'
print '---------------------------------------'
print G.nodes()

G.add_nodes_from(H)
print G.nodes()

print '---------------------------------------'
print ' G.add_edge(1,2)\n e=(2,3)\n G.add_edge(*e)'
print '---------------------------------------'
G.add_edge(1,2)
e=(2,3)
G.add_edge(*e) # unpack edge tuple*

print G.nodes()
print G.edges()

print '---------------------------------------'
print ' G.neighbors(1)'
print '---------------------------------------'
print G.neighbors(1)

print '---------------------------------------'
print ' G.add_edges_from([(1,2),(1,3)])'
print '---------------------------------------'
G.add_edges_from([(1,2),(1,3)])
print G.nodes()
print G.edges()

print '---------------------------------------'
print ' G.add_edges_from(H.edges())'
print '---------------------------------------'
G.add_edges_from(H.edges())
print G.nodes()
print G.edges()

print '---------------------------------------'
print ' G.remove_node(3)'
print '---------------------------------------'
G.remove_node(3)
print G.nodes()
print G.edges()


# For example, after removing all nodes and edges,
G.clear()
# we add new nodes/edges and NetworkX quietly ignores any that are already present.

print '---------------------------------------'
print ''' 
G.clear()
G.add_edges_from([(1,2),(1,3)])
G.add_node(1)
G.add_edge(1,2)
G.add_node("spam") # adds node "spam"
G.add_nodes_from("spam")'''
print '---------------------------------------'
G.add_edges_from([(1,2),(1,3)])
G.add_node(1)
G.add_edge(1,2)
G.add_node("spam") # adds node "spam"
G.add_nodes_from("spam") # adds 4 nodes: 's', 'p', 'a', 'm'

print '---------------------------------------'
print ' G.number_of_nodes()\n G.nodes()'
print '---------------------------------------'
print G.number_of_nodes()
print G.nodes()

print '---------------------------------------'
print ' G.number_of_edges()\n G.edges()'
print '---------------------------------------'
print G.number_of_edges()
print G.edges()

print '---------------------------------------'
print ' G.remove_nodes_from("spam")'
print '---------------------------------------'
G.remove_nodes_from("spam")
print G.nodes()
print G.edges()

print '---------------------------------------'
print ' G.remove_edge(1,3)'
print '---------------------------------------'
G.remove_edge(1,3)
print G.nodes()
print G.edges()

print '---------------------------------------'
print ' Now a digraph\n H=nx.DiGraph(G)'
print '---------------------------------------'
H=nx.DiGraph(G)
print H.nodes()
print H.edges()

print '---------------------------------------'
print ' edgelist=[(0,1),(1,2),(2,3)]\n H=nx.Graph(edgelist)'
print '---------------------------------------'
edgelist=[(0,1),(1,2),(2,3)]
H=nx.Graph(edgelist)
print H.nodes()
print H.edges()



