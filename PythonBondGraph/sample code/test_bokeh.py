# -*- coding: utf-8 -*-
"""
Created on Sun Oct 19 14:46:19 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""

from __future__ import division

import numpy as np
from bokeh.plotting import *

# Skip the first point because it can be troublesome
theta = np.linspace(0, 8*np.pi, 10000)[1:]

# Compute the radial coordinates for some different spirals
lituus = theta**(-1/2)          # lituus
golden = np.exp(0.306349*theta)/50 # golden
arch   = theta                  # Archimedean
fermat = theta**(1/2)           # Fermat's

# Now compute the X and Y coordinates (polar mappers planned for Bokeh later)
golden_x = golden*np.cos(theta)
golden_y = golden*np.sin(theta)
lituus_x = lituus*np.cos(theta)
lituus_y = lituus*np.sin(theta)
arch_x   = arch*np.cos(theta)
arch_y   = arch*np.sin(theta)
fermat_x = fermat*np.cos(theta)
fermat_y = fermat*np.sin(theta)

print '... done with math'

# output to static HTML file
output_file("lines.html")

print '... plotting ....'

# Plot the Archimedean spiral using the `line` renderer. Note how we set the
# color, line thickness, title, and legend value.
line(arch_x, arch_y, color="red", line_width=2,
     title="Archimean", legend="Archimedean")

# EXERCISE: reproduce the above plot for one of the other spirals

# Let's try to put all lines on one plot for comparison. First we need to
# turn on `hold` so that each renderer does not create a brand new plot
hold()

line(golden_x, golden_y, color="blue", line_width=2,legend="Golden")
line(lituus_x, lituus_y, color="green", line_width=2,legend="Lituus")
line(fermat_x, fermat_y, color="black", line_width=2,legend="Fermat")

# Next we need to actually create a new figure, so that the following
# renderers work on a new plot, and not the last one.
figure()

# EXERCISE: add all four spirals to one plot, each with different line colors
# and legend values. NOTE: title only set on the first renderer.

# OK, so that doesn't look so good because Bokeh tried to autoscale to
# accomodate all the data. We can use set the plot range explicitly

# EXERCISE: create a new figure

# EXERCISE: add x_range and y_range parameters to the first `line`, to set the
# range to [-10, 10].

show()      # show the plot

print '... a web page should have opened with interactive tools....'
print '... done!'

