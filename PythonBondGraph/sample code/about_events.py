# -*- coding: utf-8 -*-
"""
Created on Mon Oct 27 01:34:18 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""
'''
Here we will look at an example of creating and installing a handler for 
message boxes.
'''

def handleMessageBox(messageBox):
    test.log("MessageBox opened: '%s' - '%s'" % (
        messageBox.windowText, messageBox.text))
    messageBox.close()

#def main():
#    startApplication("myapp")
#    installEventHandler("MessageBoxOpened", "handleMessageBox")


'''
Another special event is Crash. This is useful when we want to install an 
event handler to be called when the AUT crashes—for example, to do cleanups 
or to restart the AUT.
'''

def crashHandler():
    test.log("Deleting lock files after AUT crash")
    deleteLockFiles()

def main():
    startApplication("myapp")
    installEventHandler("Crash", "crashHandler")
    installEventHandler("MessageBoxOpened", "handleMessageBox")
   
   
'''
It is possible to set up an event handler that will respond to particular 
types of events for all objects of a specified type. 
'''
def handleCheckBox(obj):
    test.log("QCheckBox '%s' clicked" % objectName(obj))

def main():
    startApplication("myapp")
    installEventHandler("QCheckBox", "QMouseEvent", "handleCheckBox")
    
'''
The third kind of event handling is for events that occur to particular objects. 
'''  

def handleDescriptionLineEdit(obj):
    lineEdit = cast(obj, QLineEdit)
    test.log("QLineEdit '%s' text changed: %s" % (
        objectName(obj), lineEdit.text))

def main():
    startApplication("myapp")
    lineEdit = waitForObject(":Description:_QLineEdit")
    installEventHandler(lineEdit, "QKeyEvent",
        "handleDescriptionLineEdit")



