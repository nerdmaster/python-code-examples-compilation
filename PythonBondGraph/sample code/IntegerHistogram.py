import scipy

def IntegerHistogram(integers, bins):
    #
    # Histogram of numbers of integers in ranges bins[x] <= i < bins[x+1]
    # Returns two arrays:
    # centers of integer range ic
    # and normalized probability distribution D[ic]
    # 
    # Find out how many entries are in each bin 
    # (from scipy.stats.histogram2)
    n = scipy.searchsorted(scipy.sort(integers),bins)
    n = scipy.concatenate([n, [len(integers)]])
    Number = n[1:] - n[:-1]
    NumberOverflow = Number[-1]
    Number = Number[:-1]

    # Then, find out min and max integer in each range
    #
    # Inefficient way if range is enormous
    # intPossible = scipy.arange(bins[len(bins)-1])
    # n = scipy.searchsorted(scipy.sort(intPossible),bins)
    # n = scipy.concatenate([n, [len(intPossible)]])
    # iNumber = n[1:] - n[:-1]

    # Efficient method: gives array one shorter (missing zero at end)
    #iNumber = scipy.zeros(len(bins)-1, scipy.Float)
    #centers = scipy.zeros(len(bins)-1, scipy.Float)
    iNumber = scipy.zeros(len(bins)-1, float)
    centers = scipy.zeros(len(bins)-1, float)
    for i in range(len(bins)-1):
        if (scipy.floor(bins[i+1])==bins[i+1]):
	    iNumber[i] = scipy.floor(bins[i+1])-scipy.ceil(bins[i])
	else:
	    iNumber[i] = scipy.floor(bins[i+1])-scipy.ceil(bins[i])+1
	centers[i] = scipy.ceil(bins[i]) + 0.5*(iNumber[i]-1)

    # Eliminate 0/0 if bins have no integers
    # Is there a fancy method for this?
    outCenter = []
    outD = []
    for i in range(len(centers)):
        if iNumber[i] != 0:
	    outCenter.append(centers[i])
	    outD.append(Number[i]/iNumber[i])
	else:
	    assert(outD == [], 
	    	"integer_histogram has data in bin where no ints are!")

    return scipy.array(outCenter), scipy.array(outD)

# Copyright (C) Cornell University
# All rights reserved.
# Apache License, Version 2.0


