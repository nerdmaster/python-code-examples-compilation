# -*- coding: utf-8 -*-
"""
Created on Sat Oct 18 14:28:47 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""

#from overloading import overload
from collections import Iterable


def flatten1(ob):
    if isinstance(ob, basestring) or not isinstance(ob, Iterable):
        yield ob
    else:
        for o in ob:
            for ob in flatten1(o):
                yield ob
                
#def flatten2(ob):
#    """Flatten an object to its component iterables"""
#    yield ob
#
#@overload
#def flatten2(ob: basestring):
#    yield ob
#
#@overload
#def flatten2(ob: Iterable):
#    for o in ob:
#        for ob in flatten(o):
#            yield ob
                #
#@overload
#def flatten(ob: MyString):
#    yield ob

ob1 = 'a string'
ob2 = ['1', '2', '3']

fob1 = flatten1(ob1)
print fob1

fob2 = flatten1(ob2) 
print fob2

