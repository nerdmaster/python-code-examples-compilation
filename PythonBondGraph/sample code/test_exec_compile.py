# -*- coding: utf-8 -*-
"""
Created on Sat Oct 18 11:31:59 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""
from time import time as t

print 'starting ...\n'
# Ntimes = 100 # 500000

code_global = compile('''
sum = 0
for x in xrange(100):
    sum += x

g_sum = sum
''', '<string>', 'exec')

code_local = compile('''
def f():
    sum = 0
    for x in xrange(100):
        sum += x
    return sum
''', '<string>', 'exec')

def test_global():
    exec code_global in {}

def test_local():
    ns = {}
    exec code_local in ns
    ns['f']()

t0 = t()

test_global()

t1 = t()

test_local()

t2 = t()

print 'local = %s' % (t1 - t0)
#->| local = 2.42842793465

print 'global = %s' % (t2 - t1)
#->| global = 0.0224208831787

class Foo(object):
    def __del__(self):
        print 'Deleted'

foo = Foo()

from math import *

print '\n -> straight-forward  function call'
user_func = raw_input("type a function: y = ")

for x in range(1,10):
	print "x = ", x , ", y = ", eval(user_func)


# A LITTLE MORE SECURE

# make a list of safe functions
safe_list = ['math','acos', 'asin', 'atan', 'atan2', 'ceil', 'cos', 'cosh', 
             'degrees', 'e', 'exp', 'fabs', 'floor', 'fmod', 'frexp', 
             'hypot', 'ldexp', 'log', 'log10', 'modf', 'pi', 'pow', 
             'radians', 'sin', 'sinh', 'sqrt', 'tan', 'tanh']
#use the list to filter the local namespace
safe_dict = dict([ (k, locals().get(k, None)) for k in safe_list ])
#add any needed builtins back in.
safe_dict['abs'] = abs

hidden_value2 = "this is secret"

def dangerous_function(filename):
	print open(filename).read()

user_func2 = raw_input("type a function: y = ")

for x in range(1,10):
	print "x = ", x , ", y = ", eval(user_func2,{},{})
 # print "x = ", x , ", y = ", eval(user_func,{"__builtins__":None},{})
 # print "x = ", x , ", y = ", \
 #  eval(user_func,{"__builtins__":None},{"x":x,"sin":sin})

print ' now with a restricted list of functions'
for fun in safe_list:
    print fun + ', ',
    
print '\n'

for x in range(1,10):
	# add x in
	safe_dict['x']=x
	print "x = ", x , ", y = ", \
  eval(user_func,{"__builtins__":None},safe_dict)


print '... done!'
