# -*- coding: utf-8 -*-
"""
Created on Sat Oct 18 17:12:03 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""
from sympy import *
from sympy.diffgeom import (LieDerivative)

x = Symbol('x'); y = Symbol('y'); z = Symbol('z')
f = Symbol('f'); g = Symbol('g'); h = Symbol('h')
F = Symbol('F')

x, y, r, theta = symbols('x, y, r, theta')
x0, y0, r0, theta0 = symbols('x0, y0, r0, theta0')
f = Function('f')
g = Function('g')
h = Function('h')

f = 3*sin(x)
g = x**2 -2*abs(x)
h = x**2 -2*x*y

def Lie(f,h,x):
    result = []
    for element in f:
        result.append (diff(element, x)*f)
    return result

def Lies(h,f,x):
    return diff(h, x)*f

Lie(f,h,x)
#->| 3*(2*x - 2)*sin(x)

Lie(f,h,y)

Lie(g,f,x)

F = [f, g]
z = [x, y]

Lies(F,h,z)
