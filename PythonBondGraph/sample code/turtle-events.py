# -*- coding: utf-8 -*-
"""
Created on Mon Oct 27 01:18:44 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""
'''
10. Event-Driven Programming
Most programs and devices like a cellphone respond to events — things
that happen. For example, you might move your mouse, and the computer responds.
Or you click a button, and the program does something interesting.
In this chapter we’ll touch very briefly on how event-driven programming works.

10.1. Keypress events
Here’s a program with some new features. Copy it into your workspace, run it.
When the turtle window opens, press the arrow keys and make tess move about!
'''
import turtle

# The next four functions are our "event handlers".
def h1():
   tess.forward(30)

def h2():
   tess.left(45)

def h3():
   tess.right(45)

def h4():
   tess.down(45)

def h5():
    wn.bye()                        # Close down the turtle window

# Create Window with turtle
turtle.setup(400,500)                # Determine the window size
wn = turtle.Screen()                 # Get a reference to the window
wn.title("Handling keypresses!")     # Change the window title
wn.bgcolor("lightgreen")             # Set the background color
tess = turtle.Turtle()               # Create our favorite turtle

# These lines "wire up" keypresses to the handlers we've defined.
wn.onkey(h1, "Up")
wn.onkey(h2, "Left")
wn.onkey(h3, "Right")
wn.onkey(h4, "Down")
wn.onkey(h5, "q")

# Now we need to tell the window to start listening for events,
# If any of the keys that we're monitoring is pressed, its
# handler will be called.
wn.listen()
wn.mainloop()


'''
Here are some points to note:

 - We need the call to the window’s listen method at line 31, otherwise 
 it won’t notice our keypresses.
 - We named our handler functions h1, h2 and so on, but we can choose 
 better names. The handlers can be arbitrarily complex functions that call 
 other functions, etc.
 - Pressing the q key on the keyboard calls function h4 (because we bound 
 the q key to h4 on line 26). While executing h4, the window’s bye method 
 (line 24) closes the turtle window, which causes the window’s mainloop call 
 (line 31) to end its execution. Since we did not write any more statements 
 after line 32, this means that our program has completed everything, 
 so it too will terminate.
 - We can refer to keys on the keyboard by their character code 
 (as we did in line 26), or by their symbolic names. Some of the 
 symbolic names to try are Cancel (the Break key), BackSpace, Tab, 
 Return(the Enter key), Shift_L (any Shift key), Control_L (any Control key), 
 Alt_L (any Alt key), Pause, Caps_Lock, Escape, Prior (Page Up), 
 Next (Page Down), End, Home, Left, Up, Right, Down, Print, Insert, 
 Delete, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, Num_Lock, 
 and Scroll_Lock.

10.2. Mouse events

A mouse event is a bit different from a keypress event because its handler 
needs two parameters to receive x,y coordinate information telling us where 
the mouse was when the event occurred.
'''
"""
import turtle

turtle.setup(400,500)
wn = turtle.Screen()
wn.title("How to handle mouse clicks on the window!")
wn.bgcolor("lightgreen")

tess = turtle.Turtle()
tess.color("purple")
tess.pensize(3)
tess.shape("circle")

def h1(x, y):
   tess.goto(x, y)

wn.onclick(h1)  # Wire up a click on the window.
wn.mainloop()




"""