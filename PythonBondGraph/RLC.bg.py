# -*- coding: utf-8 -*-
"""
Created on Fri Oct 17 13:02:35 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""
import numpy as np
import matplotlib.pyplot as plt

fnameData = 'RLC_Data.dat'
fnameBGS  = 'RLC_BGS.bgs'

## Setting parameters

C = 0.5 #<- Capacitance 0.5 F
L = 1.5 #<- Inductance 1.5 H
R = 100 #<- Resistance 100 Ohm
Va = 10 #<- Voltage source amplitude 10 V
f = 10  #<- Voltage source frequency 10 Hz

omega = 2*np.pi/f

t = np.linspace(0,10,200)
#-------------------------------------
V = Va * np.sin(omega*t) #<- Voltage source function
plt.plot(t,V)

Vc = (1/C)*q   #<- Capacitor-enforced voltage function
#-------------------------------------

#-------------------------------------
# Bond Connectivity Map
#-------------------------------------
bonds = [(2,1),
         (3,2),
         (4,2),
         (5,2)]

#-------------------------------------
# Elements Characteristics
#-------------------------------------
elements[1].etype = 'MSE'
elements[1].exprtype = 'NMC'
elements[1].mvars = sym('t')
elements[1].expr = V

elements[2].etype = '1'

elements[3].etype = 'C'
elements[3].exprtype = 'NE'
elements[3].x0 = 0
elements[3].expr = Vc
elements[3].phx = 1
elements[3].nn = true

elements[4].etype = 'I'
elements[4].exprtype = 'CC'
elements[4].x0 = 0
elements[4].expr = 1/L
elements[4].phx = 2
elements[4].nn = true

elements[5].etype = 'R'
elements[5].exprtype = 'CC'
elements[5].expr = R

## Storing runopts and BGsystem

save(fnameBGSD,'runopts','bgs')
