# -*- coding: utf-8 -*-
"""
Created on Mon Oct 20 15:35:29 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""

import graphlab as gl

url = 'http://s3.amazonaws.com/GraphLab-Datasets/movie_ratings/training_data.csv'

data = gl.SFrame.read_csv(url, column_type_hints={"rating":int})

data.show()

model = gl.recommender.create(data, user_id="user", item_id="movie", target="rating")

results = model.recommend(users=None, k=5)

