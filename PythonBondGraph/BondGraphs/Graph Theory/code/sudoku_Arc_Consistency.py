'''
An example method of solving Sudoku puzzles on a 9x9 grid. Use arc consistency to speed up the search for solutions.

John Quinn <jquinn@cit.mak.ac.ug>
'''
import copy
import time

def check_constraint(c,Assg):
    '''
    Is the given constraint satisfied by the given variable assignment.
    Here we expect the constraint just to be a list of variables, and to
    satisfy the constraint each non-null variable is different.
    '''
    assignments = []
    for v in c:
        this_assignment = Assg[v]
        if not this_assignment == None:
            assignments.append(Assg[v][0])

    # the constraint is satisfied if there are no duplicates
    valid = (len(set(assignments))==len(assignments))
    
    return valid

def check_consistency(d,x,Dom,c):
    '''
    Given domains Dom of a set of variables c, check that there is at least one 
    assignment in which which x=d and all the other variables are different.
    '''
    Assg = []
    D = []
    for v in c:
        if v != x:
            D.append(Dom[v])
            Assg.append(None)
        else:
            D.append([d])
            Assg.append(d)

    valid = check_consistency_inner(D,Assg)
    return valid

def check_consistency_inner(Dom,Assg):
    '''
    The inner function call for checking consistency. Given only a domain over
    some variables, and an assignment, try to assign the remaining variables.
    '''
    # find the first unassigned variable
    #if None in Assg:
    try:
               
        v = Assg.index(None)

        # try to make another assignment
        for d in Dom[v]:
            if d not in Assg:
                Assg_prime = Assg[:]
                Assg_prime[v] = d
                consistent = check_consistency_inner(Dom,Assg_prime)
                if consistent:
                    return True

        # if we haven't found any value, it is inconsistent
        return False

    except ValueError:
        return True


def arc_consistency(V,Dom,C):
    D = copy.deepcopy(Dom)
    
    # construct a list of all constraint arcs
    TDA = []
    for v in V:
        for c in C:
            if v in c:
                TDA.append([v,c])

    # keep processing arcs until they've all been made consistent
    while len(TDA)>0:
        arc = TDA[0]
        x = arc[0]
        c = arc[1]
        ND = []
        TDA.remove(arc)

        # find the values of x for which c can be satisfied
        for d in D[x]:
            if check_consistency(d,x,D,c):
                ND.append(d)

        # if there has been a change, add the arcs to revisit
        if ND != D[x]:
            #print len(D[x])-len(ND)
            for c_prime in C:
                if (c_prime == c) or (not (x in c_prime)):
                    pass
                for z in c_prime:
                    if (z != x) and ([z,c_prime] not in TDA):
                            TDA.append([z,c_prime])
            D[x] = ND

    return D

def generate_and_test(V,Dom,Assg,C):
    '''
    Enumerate all the possible assignments.
    '''
    Assg_copy = Assg[:]
    try:
        # find the first unassigned variable
        v = Assg.index(None)

        # loop through the values in its domain
        for d in Dom[v]:
            Assg_copy[v] = [d]

            valid = True
            for c in C:
                valid = valid and check_constraint(c,Assg_copy)

            if valid:
                AssgPrime = generate_and_test(V,Dom,Assg_copy,C)
                if AssgPrime != None:
                    return AssgPrime

        return None

    except ValueError:
        return Assg_copy

def solver(puzzle):
    '''
    Given a number grid (list of lists) with missing values, find a valid
    solution.
    '''
    n = len(puzzle)

    # the list of variables
    V = range(n**2)

    # Domains for all the unallocated variables
    Dom = []
    # Current assignment to variables
    Assg = []
    for y in range(n):
        for x in range(n):
            if puzzle[y][x] == None:
                Dom.append(range(1,n+1))
                Assg.append(None)
            else:
                Dom.append([puzzle[y][x]])
                Assg.append([puzzle[y][x]])


    # there are n column/row/block constraints.
    C = []

    for i in range(n):
        # all the column constraints
        C.append(range(i*n,(i+1)*n))

        # all the row constraints
        C.append(range(i,i+(n-1)*n+1,n))

    # all the block constraints (hard coded for 9x9 grid for now)
    C.append([0,1,2,9,10,11,18,19,20])
    C.append([3,4,5,12,13,14,21,22,23])
    C.append([6,7,8,15,16,17,24,25,26])
    C.append([27,28,29,36,37,38,45,46,47])
    C.append([30,31,32,39,40,41,48,49,50])
    C.append([33,34,35,42,43,44,51,52,53])
    C.append([54,55,56,63,64,65,72,73,74])
    C.append([57,58,59,66,67,68,75,76,77])
    C.append([60,61,62,69,70,71,78,79,80])

    starttime = time.clock()

    Dom = arc_consistency(V,Dom,C)
    print Dom
    solution = generate_and_test(V,Dom,Assg,C)

    elapsed = time.clock() - starttime

    return solution, elapsed


def run():

    puzzle1 = [[5,3,4,6,7,8,9,1,2],
              [6,7,2,1,9,5,3,4,8],
              [1,9,8,None,4,2,5,6,7],
              [8,5,9,7,6,1,4,2,3],
              [4,2,6,8,5,3,7,9,1],
              [7,1,3,9,2,4,8,5,6],
              [9,6,1,5,3,7,2,8,4],
              [2,8,7,4,1,9,6,3,5],
              [3,4,5,2,8,6,1,7,9]]

    puzzle2 = [[5,3,4,6,7,8,9,1,2],
              [6,7,2,1,9,5,None,4,8],
              [1,9,8,3,4,2,5,6,7],
              [8,5,9,7,6,1,4,2,3],
              [4,2,None,8,5,3,7,9,1],
              [7,1,3,9,2,4,8,5,6],
              [9,6,1,5,3,7,2,8,4],
              [2,8,7,4,1,9,6,3,5],
              [3,4,5,2,8,6,1,7,9]]

    puzzle3 = [[5,3,4,6,7,8,9,1,2],
              [6,7,2,1,None,5,3,4,8],
              [1,9,8,3,4,2,5,6,7],
              [8,5,9,7,6,1,4,2,3],
              [4,2,6,8,5,3,7,None,1],
              [7,1,3,9,2,4,8,5,6],
              [9,6,1,5,3,7,2,8,4],
              [2,8,None,4,1,9,6,3,5],
              [3,4,5,2,8,6,1,7,9]]

    puzzle4 = [[5,3,4,6,7,8,9,1,2],
              [6,7,None,1,9,5,3,4,8],
              [1,9,8,3,4,2,5,6,7],
              [8,5,9,7,6,1,None,2,3],
              [4,2,6,8,5,3,7,9,1],
              [7,1,None,9,2,4,8,5,6],
              [9,6,1,5,3,7,2,8,4],
              [2,8,7,4,1,9,None,3,5],
              [3,4,5,2,8,6,1,7,9]]

    puzzle5 = [[5,3,None,6,7,8,9,1,2],
              [6,7,2,1,9,5,3,4,8],
              [1,None,8,3,None,2,5,6,7],
              [8,5,9,7,6,1,4,2,3],
              [4,2,6,8,5,3,7,9,1],
              [7,1,3,9,2,None,8,5,6],
              [9,6,1,5,3,7,2,8,4],
              [2,8,7,4,None,9,6,3,5],
              [3,4,5,2,8,6,1,7,9]]

    puzzle6 = [[5,3,None,6,7,8,9,1,2],
              [6,7,2,1,9,5,3,4,8],
              [1,None,8,3,None,2,5,6,7],
              [8,5,9,7,6,1,4,2,3],
              [4,2,6,8,5,3,7,9,1],
              [7,1,3,9,2,None,8,5,6],
              [9,None,1,5,3,7,2,8,4],
              [2,8,7,4,None,9,6,3,5],
              [3,4,5,2,8,6,1,7,9]]

    puzzle7 = [[5,3,None,6,7,8,9,1,2],
              [6,7,2,1,9,5,3,4,8],
              [1,None,8,3,None,2,5,6,7],
              [8,5,9,7,6,1,4,2,3],
              [4,2,6,8,5,3,7,9,1],
              [7,1,3,9,2,None,8,5,6],
              [9,None,1,5,3,7,2,8,4],
              [2,8,7,4,None,9,6,None,5],
              [3,4,5,2,8,6,1,7,9]]

    # Easy
    puzzle_full1 = [[5,3,None,None,7,None,None,None,None],
              [6,None,None,1,9,5,None,None,None],
              [None,9,8,None,None,None,None,6,None],
              [8,None,None,None,6,None,None,None,3],
              [4,None,None,8,None,3,None,None,1],
              [7,None,None,None,2,None,None,None,6],
              [None,6,None,None,None,None,2,8,None],
              [None,None,None,4,1,9,None,None,5],
              [None,None,None,None,8,None,None,7,9]]
    # Normal
    puzzle_full2 = [[None,None,3,None,2,None,6,None,None],
              [9,None,None,3,None,5,None,None,1],
              [None,None,1,8,None,6,4,None,None],
              [None,None,8,1,None,2,9,None,None],
              [7,None,None,None,None,None,None,None,8],
              [None,None,6,7,None,8,2,None,None],
              [None,None,2,6,None,9,5,None,None],
              [8,None,None,2,None,3,None,None,9],
              [None,None,5,None,1,None,3,None,None]]
    # Hard
    puzzle_full3 = [[3,None,None,None,None,8,None,None,None],
              [7,None,8,3,2,None,None,None,5],
              [None,None,None,9,None,None,None,1,None],
              [9,None,None,None,None,4,None,2,None],
              [None,None,None,None,1,None,None,None,None],
              [None,7,None,8,None,None,None,None,9],
              [None,5,None,None,None,3,None,None,None],
              [8,None,None,None,4,7,5,None,3],
              [None,None,None,5,None,None,None,None,6]]

    # "Extra challenging"
    puzzle_full4 = [[None,2,None,None,None,None,None,None,7],
              [None,7,None,None,None,4,None,1,None],
              [9,None,5,None,None,None,None,None,None],
              [None,8,None,6,3,None,None,None,2],
              [7,None,None,None,None,None,None,None,1],
              [2,None,None,None,1,8,None,6,None],
              [None,None,None,None,None,None,4,None,9],
              [None,3,None,1,None,None,None,2,None],
              [4,None,None,None,None,None,None,8,None]]
              
    # "Hardest puzzle ever"
    puzzle_full5 = [[None,None,5,3,None,None,None,None,None],
              [8,None,None,None,None,None,None,2,None],
              [None,7,None,None,1,None,5,None,None],
              [4,None,None,None,None,5,3,None,None],
              [None,1,None,None,7,None,None,None,6],
              [None,None,3,2,None,None,None,8,None],
              [None,6,None,5,None,None,None,None,9],
              [None,None,4,None,None,None,None,3,None],  
              [None,None,None,None,None,9,7,None,None]]    
              

    sol,elapsed = solver(puzzle_full5)
    if not sol == None:
        print 'Solution found:'
        for i in range(9):
            print(sol[i*9:(i+1)*9])
    else:
        print 'No solution found.'
    print 'Elapsed time: %.4f seconds.' % elapsed


if __name__=='__main__':
    run()
