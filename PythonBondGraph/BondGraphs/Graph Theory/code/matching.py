import random as rnd
import networkx as nx
import matplotlib as plt
#import csv

fig = plt.interactive(2)

plt.interactive(1)
G = nx.Graph()

# construct weighted graph representing preferences
size_set_A = 4
size_set_B = 6
for i in range(0,size_set_A):
    for j in range(0,size_set_B):
        G.add_edge(i,size_set_A+1+j,weight=rnd.uniform(0,1))

# find the optimal matching
G2 = nx.Graph()
M = nx.max_weight_matching(G)
for i in range(0,size_set_A):
    G2.add_edge(i,M[i])

# specify which position to plot each vertex
pos = {}
for i in range(0,size_set_A):
    pos[i] = (i,1)

for j in range(0,size_set_B):
    pos[size_set_A+j+1] = (j,2)

print '    drawing the graph ...'
# draw the graph
nx.draw(G,pos)
print '... drawing the graph ...'
nx.draw_networkx_edges(G2,pos,alpha=0.4,edge_color='r',width=10)

#plt.draw()

print '... done!'