'''
Generate a random graph and then graphically display the shortest path
between pairs of vertices.

John Quinn <jquinn@cit.mak.ac.ug>
'''

import networkx as nx
import matplotlib.pyplot as plt

# randomly generate a graph
nnodes = 150
G=nx.random_geometric_graph(nnodes,0.125)
pos=G.pos 
origin = 0

plt.figure()

# for a few different destination nodes, show the shortest path
# from the origin node.
for destination in range(1,nnodes):

    # draw the edges and nodes of the full graph
    plt.ioff()
    plt.cla()
    nx.draw_networkx_edges(G,pos,nodelist=[origin],alpha=0.4)
    nx.draw_networkx_nodes(G,pos,
                           node_size=80,
                           node_color='#0000aa')

    # and highlight the origin and destination
    nx.draw_networkx_nodes(G,pos,nodelist=[origin,destination],
                           node_size=300,
                           node_color='#00dd00')

    plt.xlim(-0.05,1.05)
    plt.ylim(-0.05,1.05)
    plt.ion()
    plt.axis('off')
    plt.draw()
    raw_input("Press enter to find shortest path")
    plt.ioff()

    try:
        # find the shortest path using Dijkstra's algorithm
        shortestpath = nx.dijkstra_path(G,origin,destination)
    
        # now highlight the nodes and edges in the shortest path
        Gsp = nx.subgraph(G,shortestpath)
        nx.draw_networkx_nodes(Gsp,pos,
                               node_size=300,
                               node_color='#ff0000')
    
        nx.draw_networkx_edges(Gsp,pos,alpha=0.4,edge_color='r',width=10)
    
        nx.draw_networkx_nodes(G,pos,nodelist=[origin,destination],
                               node_size=300,
                               node_color='#00dd00')
    
        plt.xlim(-0.05,1.05)
        plt.ylim(-0.05,1.05)
        plt.ion()
        plt.axis('off')
        plt.draw()

    except nx.exception.NetworkXError:
        print("No path from origin to destination.")

    raw_input("Press enter to select new destination")

