# Install script for directory: /Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/PACC" TYPE FILE FILES
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Math.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Socket.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Threading.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Util.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/XML.hpp"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/PACC/Math" TYPE FILE FILES
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Math/Matrix.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Math/QRandSequencer.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Math/Vector.hpp"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/PACC/Util" TYPE FILE FILES
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Util/Assert.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Util/Date.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Util/MTRand.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Util/Randomizer.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Util/RandomPermutation.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Util/SignalHandler.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Util/StringFunc.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Util/Timer.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Util/Tokenizer.hpp"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/PACC/XML" TYPE FILE FILES
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/XML/Attribute.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/XML/Document.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/XML/Finder.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/XML/Iterator.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/XML/Node.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/XML/Streamer.hpp"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/PACC/Socket" TYPE FILE FILES
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Socket/Address.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Socket/Cafe.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Socket/ConnectedUDP.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Socket/Exception.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Socket/Port.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Socket/TCP.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Socket/TCPServer.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Socket/UDP.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Socket/UDPServer.hpp"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/PACC/Threading" TYPE FILE FILES
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Threading/Condition.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Threading/Exception.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Threading/Mutex.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Threading/Semaphore.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Threading/Task.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Threading/Thread.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Threading/ThreadPool.hpp"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/PACC/Threading/TLS.hpp"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/libpacc.1.2.1.dylib"
    "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/libpacc.dylib"
    )
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libpacc.1.2.1.dylib"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libpacc.dylib"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      EXECUTE_PROCESS(COMMAND "/usr/bin/install_name_tool"
        -id "libpacc.1.2.1.dylib"
        "${file}")
      IF(CMAKE_INSTALL_DO_STRIP)
        EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "${file}")
      ENDIF(CMAKE_INSTALL_DO_STRIP)
    ENDIF()
  ENDFOREACH()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/Users/benito/Development/Mac OS X Development/libBondGraph-1.0.0/PACC/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
