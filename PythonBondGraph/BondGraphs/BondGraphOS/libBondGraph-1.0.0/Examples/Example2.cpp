#include <iostream>
#include <BondGraph.h>
#include <stdexcept>
#include <fstream>

using namespace BG;

void createBondGraph(BondGraph &ioBondGraph) {
	
	double lValueR1 = 1000;
	double lValueR2 = 2000;
	double lValueR3 = 5000;
	
	double lValueC1 = 2e-4;
	double lValueC2 = 0.1;
	
	double lValueSe = 20;
	
	Source *lSe = new Source(Source::eEffort);
	lSe->setValue(lValueSe);
	ioBondGraph.addComponent(lSe);
	
	Junction *lJ0_1 = new Junction(Junction::eZero);
	ioBondGraph.addComponent(lJ0_1);
	
	Passive *lC1 = new Passive(Passive::eCapacitor);
	lC1->setValue(lValueC1);
	ioBondGraph.addComponent(lC1);
	
	Junction *lJ1_1 = new Junction(Junction::eOne);
	ioBondGraph.addComponent(lJ1_1);
	
	Passive *lR1 = new Passive(Passive::eResistor);
	lR1->setValue(lValueR1);
	ioBondGraph.addComponent(lR1);
	
	Junction *lJ0_2 = new Junction(Junction::eZero);
	ioBondGraph.addComponent(lJ0_2);
	
	Passive *lR2 = new Passive(Passive::eResistor);
	lR2->setValue(lValueR2);
	ioBondGraph.addComponent(lR2);
	
	Junction *lJ1_2 = new Junction(Junction::eOne);
	ioBondGraph.addComponent(lJ1_2);
	
	Passive *lC2 = new Passive(Passive::eCapacitor);
	lC2->setValue(lValueC2);
	ioBondGraph.addComponent(lC2);
	
	Junction *lJ0_3 = new Junction(Junction::eZero);
	ioBondGraph.addComponent(lJ0_3);
	
	Passive *lR3 = new Passive(Passive::eResistor);
	lR3->setValue(lValueR3);
	ioBondGraph.addComponent(lR3);
	
	Junction *lJ1_3 = new Junction(Junction::eOne);
	ioBondGraph.addComponent(lJ1_3);
	
	//Create the bonds
	ioBondGraph.connect(lSe,lJ0_1);
	ioBondGraph.connect(lJ0_1,lJ1_1); 
	ioBondGraph.connect(lJ1_1,lC1);
	ioBondGraph.connect(lJ1_1,lJ0_2);
	ioBondGraph.connect(lJ0_2,lR1);
	ioBondGraph.connect(lJ1_2,lJ0_2);
	ioBondGraph.connect(lJ1_2,lR2);
	ioBondGraph.connect(lJ0_3,lJ1_2);
	ioBondGraph.connect(lJ0_3,lC2);
	ioBondGraph.connect(lJ1_3,lJ0_3);
	ioBondGraph.connect(lJ1_3,lR3);
	ioBondGraph.connect(lJ0_1,lJ1_3);
	
}


int main (int argc, char * const argv[]) {

	PACC::XML::Streamer lStream(std::cout);
	PACC::Matrix lA,lB,lB2,lC,lD,lD2;
	vector<double> lInitial;
	
	BondGraph lBG;
	createBondGraph(lBG);
	try{
		lBG.assignCausality();
		lBG.plotGraph("bg_ex2.svg");
		ofstream lFileOut("bg_ex2.xml");
		PACC::XML::Streamer lFileStream(lFileOut);
		lBG.write(lFileStream);
		lFileOut.close();
		
		lBG.computeStateEquation();
		lBG.getStateMatrix(lA,lB);
		lA.write(lStream); std::cout << std::endl;
		lB.write(lStream); std::cout << std::endl;	
		
		lInitial.resize(2,0);
		lBG.setInitialState(lInitial);
		lBG.simulate(1, 0.001);
		lBG.writeSimulationLog("bg_ex2.csv");
	} catch(std::runtime_error inError) {
		std::cerr << "Error catched : " << inError.what() << std::endl;
	}

    return 0;
}
