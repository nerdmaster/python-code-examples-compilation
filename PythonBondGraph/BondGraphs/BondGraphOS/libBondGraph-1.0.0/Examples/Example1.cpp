#include <iostream>
#include <PACC/Math.hpp>
#include <BondGraph.h>
#include <HybridBondGraph.h>
#include <stdexcept>
#include <fstream>


using namespace BG;


int main (int argc, char * const argv[]) {
	PACC::Matrix lA,lB,lB2,lC,lD,lD2;
	PACC::XML::Streamer lStream(std::cout);
	
	BondGraph lBondGraph;
	PACC::XML::Document lDocument;
	PACC::XML::Iterator lRoot;

	
	ostringstream lFilename;
	lFilename << "bondgraph_1";
	lDocument.parse(lFilename.str()+string(".xml"));
	lRoot = lDocument.getFirstDataTag();
	lBondGraph.read(lRoot);
	lBondGraph.plotGraph(lFilename.str()+string(".svg"));
	lBondGraph.assignCausality();
	lBondGraph.plotGraph(lFilename.str()+string("c.svg"));
	
	lBondGraph.simplify();
	lBondGraph.assignCausality();
	lBondGraph.plotGraph(lFilename.str()+string("_simplified.svg"));
	lBondGraph.computeStateEquation();
	
	lBondGraph.getStateMatrix(lA,lB,lB2);
	lBondGraph.getOutputMatrix(lC,lD,lD2);
	lA.write(lStream); std::cout << std::endl;
	lB.write(lStream); std::cout << std::endl;
	lC.write(lStream); std::cout << std::endl;
	lD.write(lStream); std::cout << std::endl;
	
    return 0;
}
