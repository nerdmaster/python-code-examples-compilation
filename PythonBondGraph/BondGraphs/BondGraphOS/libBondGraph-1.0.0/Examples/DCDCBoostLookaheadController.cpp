/*
 *  DCDCBoostLookaheadController.cpp
 *  Copyright 2010 Jean-Francois Dupuis.
 *
 *  This file is part of HBGGP.
 *  
 *  HBGGP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  HBGGP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with HBGGP.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 25/03/10.
 */

#include "DCDCBoostLookaheadController.h"
#include "stringutil.h"
#include "BGException.h"
#include "SwitchME.h"
#include "HybridBondGraph.h"

using namespace BG;

void DCDCBoostLookaheadController::writeLog() {
	
	const vector<double>& lOutputsVariables = mBondGraph->getOutputVariables();
	
	(*mLogger)["State"].push_back(mCurrentState);
	for(unsigned int i = 0; i < mTargets.size(); ++i) {
		(*mLogger)[string("Target_")+int2str(i)].push_back(mTargets[i]);
		(*mLogger)[string("Output_")+int2str(i)].push_back(lOutputsVariables[i]);
	}
}


void DCDCBoostLookaheadController::createBondGraphDouble(BG::HybridBondGraph &ioBondGraph, double inVi, vector<double> inR) {
	
	//Create the sources
	vector<Source*> lSe(1);
	for(unsigned int i = 0; i < lSe.size(); ++i) {
		lSe[i] = new Source(Source::eEffort);
		lSe[i]->setValue(inVi);
		ioBondGraph.addComponent(lSe[i]);
	}
	
	//Create a switch
	vector<Switch*> lSw(3);
	for(unsigned int i = 0; i < lSw.size(); ++i) {
		lSw[i] = new Switch;
		push_back(lSw[i]);
	}
	
	//Create the jonctions
	vector<Junction*> lJ0(3);
	for(unsigned int i = 0; i < lJ0.size(); ++i) {
		lJ0[i] = new Junction(Junction::eZero);
		ioBondGraph.addComponent(lJ0[i]);
	}
	vector<Junction*> lJ1(3);
	for(unsigned int i = 0; i < lJ1.size(); ++i) {
		lJ1[i] = new Junction(Junction::eOne);
		ioBondGraph.addComponent(lJ1[i]);
	}
	
	//Create component
	vector<Passive*> lC(2);
	for(unsigned int i = 0; i < lC.size(); ++i) {
		lC[i] = new Passive(Passive::eCapacitor);
		ioBondGraph.addComponent(lC[i]);
	}
	lC[0]->setValue(800e-6);
	lC[1]->setValue(146.6e-6);
	
	vector<Passive*> lR(2);
	for(unsigned int i = 0; i < lR.size(); ++i) {
		lR[i] = new Passive(Passive::eResistor);
		ioBondGraph.addComponent(lR[i]);
	}
	lR[0]->setValue(inR[0]);
	lR[1]->setValue(inR[1]);
	
	vector<Passive*> lI(1);
	for(unsigned int i = 0; i < lI.size(); ++i) {
		lI[i] = new Passive(Passive::eInductor);
		lI[i]->setValue(150e-6);
		ioBondGraph.addComponent(lI[i]);
	}
	
	//Create the bonds
	vector<Bond*> lOutputBondsEffort(2);
	vector<Bond*> lOutputBondsFlow(1);
	
	lOutputBondsFlow[0] = ioBondGraph.connect(lSe[0],lJ1[0]);
	ioBondGraph.connect(lJ1[0],lI[0]);
	ioBondGraph.connect(lJ1[0],lJ0[0]);
	ioBondGraph.connect(lJ0[0],lSw[0]);
	ioBondGraph.connect(lJ0[0],lJ1[1]);
	ioBondGraph.connect(lJ1[1],lSw[1]);
	ioBondGraph.connect(lJ1[1],lJ0[1]);
	ioBondGraph.connect(lJ0[1],lC[0]);
	lOutputBondsEffort[0] = ioBondGraph.connect(lJ0[1],lR[0]);
	
	ioBondGraph.connect(lJ0[0],lJ1[2]);
	ioBondGraph.connect(lJ1[2],lSw[2]);
	ioBondGraph.connect(lJ1[2],lJ0[2]);
	ioBondGraph.connect(lJ0[2],lC[1]);
	lOutputBondsEffort[1] = ioBondGraph.connect(lJ0[2],lR[1]);
	
	
	ioBondGraph.setOutputBonds(lOutputBondsEffort,lOutputBondsFlow);

	ioBondGraph.postConnectionInitialization();
	
}
