/*
 *  DCDCBoostMain.cpp
 *  Copyright 2010 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 30/03/10.
 */

#include <iostream>
#include <PACC/Math.hpp>
#include <HybridBondGraph.h>
#include <stdexcept>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <cfloat>
#include "BGException.h"
#include "SimulationCase.h"
#include "DCDCBoostLookaheadController.h"

#define NBOUTPUTS 3

using namespace BG;
using namespace std;

inline std::string int2str(long inInteger)
{
	std::ostringstream lStringOS;
	lStringOS << inInteger;
	return lStringOS.str().c_str();
}

int simulateConverter(string& inLogFilename, HybridBondGraph* inBondGraph,LookaheadController *inController,
				vector<SimulationCase>& inSimulationCases, double inSimulationDuration, unsigned int inAllowDifferentialCausality);

int main (int argc, char * const argv[]) {
	
	vector<SimulationCase> lSimulationCases;
	string lTargetString("0[1.5,6.25,34.1](1.875,3.75),5e-3[1.5,10,50](1.875,3.75),10e-3[1.5,14,40](1.875,3.75),15e-3[1.5,20,100](1.875,3.75)");
	readSimulationCase(lTargetString, lSimulationCases);
	
	if(lSimulationCases[0].getTime(0) != 0) {
		throw runtime_error("DCDCBoostEvalOp : Not applying control target at time 0");
	}
	
	PACC::XML::Streamer lStream(std::cout);
	
	double lTimeStep = 1e-5;
	
	DCDCBoostLookaheadController *lController = new DCDCBoostLookaheadController(lTimeStep);
	HybridBondGraph lHBG;
	lHBG.addSwitchController(lController);
	
	string lFilename("DCDCBoost");
	
	//Create the hybrid bond graph
	lController->createBondGraphDouble(lHBG,1.5, vector<double>(2,6.25));
	
//	//or read it from file
//	PACC::XML::Document lDocument;
//	PACC::XML::Iterator lRoot;
//	lDocument.parse(lFilename+string(".xml"));
//	lRoot = lDocument.getFirstDataTag();
//	lHBG.read(lRoot);
	
	//Draw the bond graph
	lHBG.plotGraph(lFilename+string(".svg"));
	lHBG.simplify();
	lHBG.plotGraph(lFilename+string("_simplified.svg"));

	lHBG.setDifferentialCausalitySupport(false);
	
	return simulateConverter(lFilename,&lHBG,lController,lSimulationCases,20e-3,0);
}

#define NBOUTPUTS 3
#define NBPARAMETERS 3 
int simulateConverter(string& inLogFilename, HybridBondGraph* inBondGraph,LookaheadController *inController,
				vector<SimulationCase>& inSimulationCases, double inSimulationDuration, unsigned int inAllowDifferentialCausality) {
	try {
		std::vector<double> lInitialOutput(1,0);
		std::map<std::string, std::vector<double> > &lLogger = inBondGraph->getSimulationLog();
		
		unsigned int lTry = 0;
		double lCurrentTarget = 0;
		
		for(int g = inSimulationCases.size()-1; g >= 0; --g) {
			bool lRun = true;
			if(lRun) {
				
				bool lSimulationRan = false;
				
				
				//Define if the system is aware of the new parameter.
				bool lSimulateWithInitialParameterOnly = false;
				
				for(unsigned int i = 0; i < inSimulationCases[g].getSize(); ++i) {
					
					if( inSimulationCases[g].getTime(i) >= inSimulationDuration )
						throw runtime_error("Applying control target later than simulation end");
					if(inSimulationCases[g].getTargets(i).size() != NBOUTPUTS-1)
						throw runtime_error("There should be 1 target value for each control time");
					if(inSimulationCases[g].getParameters(i).size() != NBPARAMETERS)
						throw runtime_error("There should be 1 parameter value for each control time");
	
					//Assign parameters
					const vector<double>& lParameters = inSimulationCases[g].getParameters(i);
					vector<BG::Component*> lComponents = inBondGraph->getComponents();
					if(!lParameters.empty() && i == 1) {
						inBondGraph->saveAsParameterMatrix();
					}					
					unsigned int lFound = 0;
					for(unsigned int k = 0; k < lComponents.size() && lFound < 2;++k) {
						if(lComponents[k] != 0) {
							if(lComponents[k]->getName() == "R3") {
								lComponents[k]->setValue(lParameters[1]);
								++lFound;
							} else if(lComponents[k]->getName() == "R4") {
								lComponents[k]->setValue(lParameters[2]);
								++lFound;
							}
						}
					}
					
					if(!lParameters.empty()) {
						inBondGraph->clearStateMatrix();
					}

					//Compute the current target
					vector<double> lTargets = inSimulationCases[g].getTargets(i);
					if(!lSimulateWithInitialParameterOnly || i == 0) {
						lCurrentTarget = 0;
						for(unsigned int k = 0; k < lTargets.size(); ++k) {
							lCurrentTarget += lTargets[k]*lTargets[k]/(lParameters[0]*lParameters[k+1]);
						}
					}
					lTargets.push_back(lCurrentTarget);
					inController->setTarget(lTargets);
					
					if(i == 0) {
						//Find a valid initial state
						unsigned int lInitialSwitchState = 0;
						double lMaxConfiguration = pow(2.0,int(inBondGraph->getSwitches().size()));
						for(;lInitialSwitchState < lMaxConfiguration; ++lInitialSwitchState) { 
							try{ 
								inController->initialize(&(*inBondGraph),lInitialSwitchState);
							} catch(BG::CausalityException inError) {
								continue;
							}
							break;
						}
						
						//Check if a valid initial state have been found
						if(lInitialSwitchState == lMaxConfiguration) {
							if(inAllowDifferentialCausality == 2) {
								inBondGraph->setDifferentialCausalitySupport(true);
								lInitialSwitchState = 0;
							} else {
								lSimulationRan = false;
								throw BG::CausalityException("No initial state found!");
								break;
							}
						}
						
						//Reset the bond graph
						lLogger.clear();
						inBondGraph->reset();

					}
					
					//Run the simulation
					if(i < inSimulationCases[g].getSize()-1) {
						if( i != 0 )
							inBondGraph->simulate(inSimulationCases[g].getTime(i+1),inController->getSimulationTimeStep(),false,lSimulateWithInitialParameterOnly);
						else
							inBondGraph->simulate(inSimulationCases[g].getTime(i+1),inController->getSimulationTimeStep(),false,false);
					}
					else
						inBondGraph->simulate(inSimulationDuration,inController->getSimulationTimeStep(),false,lSimulateWithInitialParameterOnly);
					
				}
				
				//Evaluate the results
				if(lSimulationRan) {
					//Clean logger
					for(map<std::string, vector<double> >::iterator lIter = lLogger.begin(); lIter != lLogger.end();) {
						if(lIter->first != "Output_0" &&
						   lIter->first != "Output_1" &&
						   lIter->first != "Output_2" &&
						   lIter->first != "Target_0" &&
						   lIter->first != "Target_1" &&
						   lIter->first != "Target_2" &&
						   lIter->first != "State" &&
						   lIter->first != "time" &&
						   lIter->first != "S1") {
							lLogger.erase(lIter++);
						} else {
							++lIter;		
						}
					}
					inBondGraph->writeSimulationLog(std::string("DCDCBoost_Lookahead_")+int2str(g)+std::string(".csv"));
				} 
				
				//Log simulation data
				inBondGraph->writeSimulationLog(inLogFilename+string("_")+int2str(lTry)+string(".csv"));
				++lTry;
			}
		}
	
		return 0;
	} 
	catch(BG::CausalityException inError) {
		return 1;
	}
}


