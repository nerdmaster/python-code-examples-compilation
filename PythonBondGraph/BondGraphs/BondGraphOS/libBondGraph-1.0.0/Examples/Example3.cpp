#include <iostream>
#include <BondGraph.h>
#include <HybridBondGraph.h>
#include <stdexcept>
#include <fstream>

using namespace BG;

int main (int argc, char * const argv[]) {

	PACC::XML::Streamer lStream(std::cout);
	PACC::Matrix lA,lB,lB2,lC,lD,lD2;
	vector<double> lInitial;
	
	//Read hybrid bond graph from file
	HybridBondGraph lBondGraph,lBondGraph2;
	PACC::XML::Document lDocument;
	PACC::XML::Iterator lRoot;
	
	//Read the first bond graph
	ostringstream lFilename;
	lFilename << "Bondgraph_comp1";
	lDocument.parse(lFilename.str()+string(".xml"));
	lRoot = lDocument.getFirstDataTag();
	lBondGraph.read(lRoot);
	lBondGraph.plotGraph(lFilename.str()+string(".svg"));
	
	lBondGraph.simplify();
	lBondGraph.plotGraph(lFilename.str()+string("_simplified.svg"));
	
	ofstream lFilestream( (lFilename.str()+string("_simplified.xml")).c_str() );
	PACC::XML::Streamer lStreamer(lFilestream);
	lBondGraph.write(lStreamer);
	
	//Compare bond graph
	lFilename.str("");
	lFilename << "Bondgraph_comp3";
	lDocument.parse(lFilename.str()+string(".xml"));
	lRoot = lDocument.getFirstDataTag();
	lBondGraph2.read(lRoot);
	lBondGraph2.plotGraph(lFilename.str()+string(".svg"));
	
	lBondGraph2.simplify();
	lBondGraph2.plotGraph(lFilename.str()+string("_simplified.svg"));

	//Compare bond graph using a known common starting comparison point for faster inequality detection
	cout << "Compare results: " << lBondGraph.compare(lBondGraph2,lBondGraph.getEffortOutputBonds()[0],lBondGraph2.getEffortOutputBonds()[0]) << endl;
	//Compare bond graph without known common point
	cout << "Compare results without starting point: " << lBondGraph.compare(lBondGraph2) << endl;
	
    return 0;
}
