/*
 *  Simulator.h
 *  Copyright 2008 Jean-Francois Dupuis.
 *
 *  This file is part of EvoStateController.
 *  
 *  EvoStateController is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  EvoStateController is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with EvoStateController.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 03/10/08.
 */

#ifndef Simulator_H
#define Simulator_H

#include "SystemDynamic.h"
#include "Controller.h"

class Simulator {
public:
	Simulator(SystemDynamic* inDynamic);
	Simulator(SystemDynamic* inDynamic, Controller* inController);
	~Simulator() {}
	
	void setTimeStep(double inContinuous, double inDiscrete = 0);
	
	void simulate(double inStopTime);
private:
	SystemDynamic* mDynamic;
	Controller* mController;
	
	double mTime;
	
	double mDiscreteTimeStep;
	double mContinuousTimeStep;
};

#endif
