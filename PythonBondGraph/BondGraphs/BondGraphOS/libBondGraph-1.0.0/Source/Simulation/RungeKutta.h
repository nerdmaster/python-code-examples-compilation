/***************************************************************************
 *   Copyright (C) 2004 by Jean-Francois Dupuis                            *
 *   jfdupuis@gel.ulaval.ca                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef RUNGEKUTTA_H
#define RUNGEKUTTA_H

#include <vector>
#include <stdexcept>

using namespace std;


/**
@author Jean-Francois Dupuis
*/
class RungeKutta{
	
private:
	
	void rk4_coupled(double t, double h,
					 const vector<double> & y, const vector<double> & dydt, vector<double> & yout,
					 const vector<double> & z, const vector<double> & dzdt, vector<double> & zout);
	
	void rk4(double t, double h, const vector<double> & y, const vector<double> & dydt, vector<double> & yout);
	
	virtual void ode(double t, const vector<double> & y, vector<double> & dydt) {throw runtime_error("Not implemented");}
	virtual void ode(double t, const vector<double> & y, vector<double> & dydt, const vector<double> & z, vector<double> & dzdt) {throw runtime_error("Not implemented");} 
	
public:
	RungeKutta() {}
	virtual ~RungeKutta() {}
	
	void rkdumb( double inT0, double inTf, double inTimeStep, const vector<double> & inY0,
				vector<double> & outTime, vector< vector<double> > & outValue);
	
	void rkdumb_coupled(double inT0, double inTf, double inTimeStep,
						const vector<double> & inY0, const vector<double> & inZ0,
						vector<double> & outTime, vector< vector<double> > & outYValue, vector< vector<double> > & outZValue);
};

#endif
