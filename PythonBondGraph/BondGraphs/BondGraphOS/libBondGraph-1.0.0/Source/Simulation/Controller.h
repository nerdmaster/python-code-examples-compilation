/*
 *  Controller.h
 *  Copyright 2008 Jean-Francois Dupuis.
 *
 *  This file is part of EvoStateController.
 *  
 *  EvoStateController is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  EvoStateController is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with EvoStateController.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 15/10/08.
 */

#ifndef Controller_H
#define Controller_H

#include <vector>
#include <map>
#include <string>

class Controller {
public:
	Controller(std::map<std::string, std::vector<double> > *inLogger) { mLogger = inLogger; }
	~Controller() {}
	
	virtual void setTarget(const std::vector<double>& inTargets) { mTargets = inTargets;	}
	
	virtual std::vector<double> getOutputs() = 0;
	virtual void setInput(const std::vector<double>& inInputs) = 0;
	
	virtual void writeLog() = 0;
	
protected:
	std::map<std::string, std::vector<double> > *mLogger;
	
	std::vector<double> mTargets;
	std::vector<double> mInputs;
};

#endif
