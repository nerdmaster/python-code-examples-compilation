/*
 *  SystemDynamic.h
 *  Copyright 2008 Jean-Francois Dupuis.
 *
 *  This file is part of EvoStateController.
 *  
 *  EvoStateController is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  EvoStateController is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with EvoStateController.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 02/10/08.
 */

#ifndef SystemDynamic_H
#define SystemDynamic_H

#if defined USE_RKF
	#include "RungeKuttaFehlberg.h"
#elif defined USE_RKFVS
	#include "RungeKuttaFehlbergVariableStep.h"
#else
	#include "RungeKutta.h"
#endif

#include <map>
#include <string>

class SystemDynamic : 
#if defined USE_RKF
	public RungeKuttaFehlberg {
#elif defined USE_RKFVS
	public RungeKuttaFehlbergVariableStep {
#else
	public RungeKutta {	
#endif
public:
	SystemDynamic(std::map<std::string, std::vector<double> > *inLogger) { mTime = 0; mLogger=inLogger; }
	virtual ~SystemDynamic() {}

	void setTimeStep(double inTimeStep) { mTimeStep = inTimeStep; }
	void simulate(double inEndTime);
	
	//! Return Observed variable. By default, return state variables.
	virtual const std::vector<double>& getObservableVariables() { return mStateVariables; }
	
	//! Return the state variables of the system
	virtual const std::vector<double>& getStateVariables() const { return mStateVariables; }
	void setStateVariables(const std::vector<double>& inStates)  { mStateVariables = inStates; }
	virtual void setInput(std::vector<double> inInputs) { mInputs = inInputs; }
	virtual const std::vector<double>& getInput() const { return mInputs; }
	
	virtual void reset();
	
	double getTime() const { return mTime; }
	
	virtual void initialize(const std::vector<double> &inValues) = 0;
	
	virtual void writeLog() = 0;
	
protected:
#ifdef USE_RKF
	virtual void ode(float t, float *&y, float *&dydt) = 0;
#else
	virtual void ode(double t, const std::vector<double> & y, std::vector<double> & dydt) = 0;
#endif
	
	unsigned int mSystemDimension;
	double mTimeStep;
	double mTime;
	
	std::map<std::string, std::vector<double> > *mLogger;
	
	std::vector<double> mInputs;
	std::vector<double> mStateVariables;
	std::vector<double> mOutPutVariables;
	std::vector<double> mInitialValues;
};

#endif
