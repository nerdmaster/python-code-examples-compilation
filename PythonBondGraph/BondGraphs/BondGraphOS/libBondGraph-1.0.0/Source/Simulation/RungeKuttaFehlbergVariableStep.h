/*
 *  RungeKuttaFehlbergVariableStep.h
 *  Copyright 2010 Jean-Francois Dupuis.
 *
 *  This file is part of HBGGP.
 *  
 *  HBGGP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  HBGGP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with HBGGP.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 19/05/10.
 */

///Source for the computing functions : numerical receipe in C, second edition

#ifndef RungeKuttaFehlbergVariableStep_H
#define RungeKuttaFehlbergVariableStep_H

#include "nrtypes.h"
#include <vector>
#include <stdexcept>
#include <iostream>

class RungeKuttaFehlbergVariableStep {
public:
	RungeKuttaFehlbergVariableStep() { xp_p = 0; }
	virtual ~RungeKuttaFehlbergVariableStep() { }
	
	void rkf(double inT0, double inTf, const std::vector<double> & inInitial, std::vector<double> & outValue);
	
//	virtual void ode(float t, float *&y, float *&dydt) { throw runtime_error("Not implemented"); }
//	virtual void ode(double t, double *&y, double *&dydt) { throw runtime_error("Not implemented"); };
	virtual void ode(double t, const std::vector<double> &y, std::vector<double>& dydt) { throw std::runtime_error("Not implemented"); };

private:
	double dxsav; //Intermediate value saving intervales
	int kmax,kount; //Number of intermediate value saving limits
	std::vector<double> *xp_p; //Intermediate value x
	Mat_DP *yp_p; //Intermediate value y
	
	void rkck(const std::vector<double> &y, const std::vector<double> &dydx, const double x,
			  const double h, std::vector<double> &yout, std::vector<double> &yerr);
	
	void rkqs(std::vector<double> &y, std::vector<double> &dydx, double &x, const double htry,
			  const double eps, const std::vector<double> &yscal, double &hdid, double &hnext);
	
	void odeint(std::vector<double> &ystart,  double x1,  double x2,  double eps,
				 double h1,  double hmin, int &nok, int &nbad);
	
};

#endif
