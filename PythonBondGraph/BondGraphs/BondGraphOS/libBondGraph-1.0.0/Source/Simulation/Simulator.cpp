/*
 *  Simulator.cpp
 *  Copyright 2008 Jean-Francois Dupuis.
 *
 *  This file is part of EvoStateController.
 *  
 *  EvoStateController is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  EvoStateController is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with EvoStateController.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 03/10/08.
 */

#include "Simulator.h"
#include <iostream>

Simulator::Simulator(SystemDynamic* inDynamic) :
mDynamic(inDynamic)
{
	mController = 0;
	mDiscreteTimeStep = 1e-3;
	mContinuousTimeStep = mDiscreteTimeStep/100;
}

Simulator::Simulator(SystemDynamic* inDynamic, Controller* inController) :
mDynamic(inDynamic), mController(inController) 
{
	mDiscreteTimeStep = 1e-3;
	mContinuousTimeStep = mDiscreteTimeStep/100;
}

void Simulator::simulate(double inStopTime) {
	if(mController != 0) {
		while(mDynamic->getTime() < inStopTime) {
			mDynamic->setInput(mController->getOutputs());
			mDynamic->simulate(mDynamic->getTime()+mDiscreteTimeStep);
			mController->setInput(mDynamic->getObservableVariables());
			mDynamic->writeLog();
			mController->writeLog();
		}
	} else {
		while(mDynamic->getTime() < inStopTime) {
			mDynamic->simulate(mDynamic->getTime()+mDiscreteTimeStep);
			mDynamic->writeLog();
		}
	}
}

void Simulator::setTimeStep(double inContinuous, double inDiscrete) { 
	mDiscreteTimeStep = inDiscrete; 
	mContinuousTimeStep = inContinuous; 
	
	mDynamic->setTimeStep(mContinuousTimeStep);
}

