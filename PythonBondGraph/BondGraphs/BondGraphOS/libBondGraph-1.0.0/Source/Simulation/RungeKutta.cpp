/***************************************************************************
 *   Copyright (C) 2004 by Jean-Francois Dupuis                            *
 *   jfdupuis@gel.ulaval.ca                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "RungeKutta.h"

#include <stdexcept>
#include <iostream>
#include <cmath>

void RungeKutta::rk4_coupled(double t, double h,
								 const vector<double> & y, const vector<double> & dydt, vector<double> & yout,
								 const vector<double> & z, const vector<double> & dzdt, vector<double> & zout)
{
	unsigned int N = y.size();

	vector<double> dym(N, 0);
	vector<double> dyt(N, 0);
	vector<double> yt(N, 0);

	vector<double> dzm(N, 0);
	vector<double> dzt(N, 0);
	vector<double> zt(N, 0);

	double hh = h*0.5;
	double h6 = h/6.0;
	double th = t+hh;

	// First step
	for(int i = 0; i < N; ++i) {
		yt[i]=y[i]+hh*dydt[i];
		zt[i]=z[i]+hh*dzdt[i];
	}

	// Second step
	ode(th, yt, dyt, zt, dzt);
	for(int i = 0; i < N; ++i) {
		yt[i]=y[i]+hh*dyt[i];
		zt[i]=z[i]+hh*dzt[i];
	}

	// Third step
	ode(th, yt, dym, zt, dzm);
	for(int i = 0; i < N; ++i) {
		yt[i] = y[i] + h*dym[i];
		dym[i] += dyt[i];

		zt[i] = z[i] + h*dzm[i];
		dzm[i] += dzt[i];
	}
	// Fourth step
	ode(t+h, yt, dyt, zt, dzt);

	// Compute final value
	for(int i = 0; i < N; ++i) {
		yout[i]=y[i]+h6*(dydt[i]+dyt[i]+2.0*dym[i]);
		zout[i]=z[i]+h6*(dzdt[i]+dzt[i]+2.0*dzm[i]);
	}
}

void RungeKutta::rkdumb_coupled(double inT0, double inTf, double inTimeStep,
						const vector<double> & inY0, const vector<double> & inZ0,
						vector<double> & outTime, vector< vector<double> > & outYValue, vector< vector<double> > & outZValue)
{
	if((inTf - inT0) < inTimeStep)
		throw runtime_error("RungeKutta::rkdumb_coupled() : TimeStep is greater than Tf - T0");

	outTime.resize(0);
	outYValue.resize(0);
	outZValue.resize(0);

	unsigned int N = inY0.size();
	unsigned int M = inZ0.size();

	vector<double> v(N,0);
	vector<double> vout(N,0);
	vector<double> dv(N,0);

	vector<double> z(N,0);
	vector<double> zout(N,0);
	vector<double> dz(N,0);

	//Load starting values.
	for (int i = 0; i < N; ++i) {
		v[i] = inY0[i];
	}
	for (int i = 0; i < M; ++i) {
		z[i] = inZ0[i];
	}
	outYValue.push_back(v);
	outZValue.push_back(z);
	outTime.push_back(inT0);

	double t = inT0;

	double nstep = ceil( (inTf - t)/inTimeStep );
	double h = (inTf - t)/nstep;

	//Iterate
	while( t < inTf ) {
		ode(t, v, dv, z, dz);
		rk4_coupled(t, h, v, dv, vout, z, dz, zout);

		if( (t+h) == t)
			throw(runtime_error("RungeKutta::rkdumb : Step size too small"));

		t += h;

		//Store intermediate steps.
		outTime.push_back(t);
		for(int i = 0; i < N; ++i) {
			v[i]=vout[i];
		}
		for (int i = 0; i < M; ++i) {
			z[i] = zout[i];
		}
		outYValue.push_back(v);
		outZValue.push_back(z);
	}
}


void RungeKutta::rk4(double t, double h, const vector<double> & y, const vector<double> & dydt, vector<double> & yout)
{
	unsigned int N = y.size();

	vector<double> dym(N, 0);
	vector<double> dyt(N, 0);
	vector<double> yt(N, 0);

	double hh = h*0.5;
	double h6 = h/6.0;
	double th = t+hh;

	// First step
	for(int i = 0; i < N; ++i)
		yt[i]=y[i]+hh*dydt[i];

	// Second step
	ode(th,yt,dyt);
	for(int i = 0; i < N; ++i)
		yt[i]=y[i]+hh*dyt[i];

	// Third step
	ode(th,yt,dym);
	for(int i = 0; i < N; ++i) {
		yt[i]=y[i]+h*dym[i];
		dym[i] += dyt[i];
	}
	// Fourth step
	ode(t+h,yt,dyt);

	// Compute final value
	for(int i=0; i < N; ++i)
		yout[i]=y[i]+h6*(dydt[i]+dyt[i]+2.0*dym[i]);
}

void RungeKutta::rkdumb(double inT0, double inTf, double inTimeStep, const vector<double> & inY0,
                        vector<double> & outTime, vector< vector<double> > & outValue)
{
	if(inTimeStep > abs(inTf - inT0))
		throw runtime_error("RungeKutta::rkdumb() : TimeStep is greater than Tf - T0");

	outTime.resize(0);
	outValue.resize(0);

	unsigned int N = inY0.size();

	vector<double> v(N);
	vector<double> vout(N);
	vector<double> dv(N);

	//Load starting values.
	for (int i = 0; i < N; ++i) {
		v[i] = inY0[i];
	}
	outValue.push_back(v);
	outTime.push_back(inT0);

	if( inTf > inT0 ) {
		//Forward integration
		double t = inT0;
		double nstep = ceil( (inTf - t)/inTimeStep );
		double h = (inTf - t)/nstep;

		//Iterate
		while( t < inTf ) {
			ode(t, v, dv);
			rk4(t, h, v, dv, vout);

			if( (t+h) == t)
				throw(runtime_error("RungeKutta::rkdumb : Step size too small"));

			t += h;

			//Store intermediate steps.
			outTime.push_back(t);
			for(int i = 0; i < N; ++i) {
				v[i]=vout[i];
			}
			outValue.push_back(v);
		}
	} else {
		//Backward integration
		double t = inT0;
		double nstep = ceil( (t - inTf)/inTimeStep );
		double h = -(t - inTf)/nstep;
		
		//Iterate
		while( t > inTf ) {
			ode(t, v, dv);
			rk4(t, h, v, dv, vout);
			
			if( (t+h) == t)
				throw(runtime_error("RungeKutta::rkdumb : Step size too small"));
			
			t += h;
			
			//Store intermediate steps.
			outTime.push_back(t);
			for(int i = 0; i < N; ++i) {
				v[i]=vout[i];
			}
			outValue.push_back(v);
		}
	}
}
