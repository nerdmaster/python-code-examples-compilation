/*
 *  RungeKuttaFehlberg.h
 *  Copyright 2008 Jean-Francois Dupuis.
 *
 *  This file is part of ThreeTanks.
 *  
 *  ThreeTanks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ThreeTanks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ThreeTanks.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 04/11/08.
 */

#ifndef RungeKuttaFehlberg_H
#define RungeKuttaFehlberg_H

#include <vector>
#include <stdexcept>
using namespace std;

class RungeKuttaFehlberg {

public:
	RungeKuttaFehlberg() {}
	~RungeKuttaFehlberg() {}
	
	void rkf(double inT0, double inTf, const vector<double> & inY0, vector<double> & outValue);
	void rkf_step(double inT0, double inTf, unsigned int inStep, const vector<double> & inInitial, 
				  vector<double> & outTime, vector< vector<double> > & outValue);
	
private:
	float r4_abs ( float x );
	float r4_epsilon ( void );
	void r4_fehl ( int neqn, 
				  float y[], float t, float h, float yp[], float f1[], float f2[], float f3[], 
				  float f4[], float f5[], float s[] );
	float r4_max ( float x, float y );
	float r4_min ( float x, float y );
	int r4_rkf45 ( int neqn, 
				  float y[], float yp[], float *t, float tout, float *relerr, float abserr, 
				  int flag );
	float r4_sign ( float x );
	
	double r8_abs ( double x );
	double r8_epsilon ( void );
	void r8_fehl ( int neqn, 
				  double y[], double t, double h, double yp[], double f1[], double f2[], double f3[], 
				  double f4[], double f5[], double s[] );
	double r8_max ( double x, double y );
	double r8_min ( double x, double y );
	int r8_rkf45 ( int neqn, 
				  double y[], double yp[], double *t, double tout, double *relerr, double abserr, 
				  int flag );
	double r8_sign ( double x );
	
	void timestamp ( void );
	
	virtual void ode(float t, float *&y, float *&dydt) { throw runtime_error("Not implemented"); }
	virtual void ode(double t, double *&y, double *&dydt) { throw runtime_error("Not implemented"); };
};

#endif
