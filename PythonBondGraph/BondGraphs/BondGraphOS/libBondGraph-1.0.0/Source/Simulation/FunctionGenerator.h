/*
 *  FunctionGenerator.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 24/04/09.
 */

#ifndef FunctionGenerator_H
#define FunctionGenerator_H

class FunctionGenerator {
public:
	FunctionGenerator() {}
	virtual ~FunctionGenerator() {}
	
	virtual double getOutput(double inTime) const = 0;
};


class PeriodicFunction : public FunctionGenerator {
public:
	PeriodicFunction(double inAmplitude, double inFrequency, double inPhase = 0, double inZero = 0) : mAmplitude(inAmplitude), mFrequency(inFrequency), mPhase(inPhase), mZero(inZero) {}
	virtual ~PeriodicFunction() {}
	virtual double getOutput(double inTime) const = 0;
protected:
	double mAmplitude;
	double mFrequency;	//! Frequency in hertz
	double mPhase;		//! Phase in radian
	double mZero;		//! Average of the signal.
};

class SinFunction : public PeriodicFunction {
public:
	SinFunction(double inAmplitude, double inFrequency, double inPhase = 0, double inZero = 0) : PeriodicFunction(inAmplitude,inFrequency,inPhase,inZero) {}
	virtual ~SinFunction() {}
	
	virtual double getOutput(double inTime) const;
};

class CosFunction : public PeriodicFunction {
public:
	CosFunction(double inAmplitude, double inFrequency, double inPhase = 0, double inZero = 0) : PeriodicFunction(inAmplitude,inFrequency,inPhase,inZero)  {}
	virtual ~CosFunction() {}
	
	virtual double getOutput(double inTime) const;
};

class SquareFunction : public PeriodicFunction {
public:
	SquareFunction(double inAmplitude, double inFrequency, double inPhase = 0, double inZero = 0) : PeriodicFunction(inAmplitude,inFrequency,inPhase,inZero)  {}
	virtual ~SquareFunction() {}
	
	virtual double getOutput(double inTime) const;
};

class PWMFunction : public PeriodicFunction {
public:
	PWMFunction(double inAmplitude, double inFrequency, double inPhase = 0, double inZero = 0) : PeriodicFunction(inAmplitude,inFrequency,inPhase,inZero), mDutyCycle(0.5) {}
	virtual ~PWMFunction() {}
	
	void setDutyCycle(double inDutyCycle) { mDutyCycle = inDutyCycle; }
	double getDutyCycle() const { return mDutyCycle; }
	
	virtual double getOutput(double inTime) const;
	
protected:
	double mDutyCycle; //!< Duty cycle of the signal, defined between 0 and 1.
};

#endif
