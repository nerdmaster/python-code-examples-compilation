/*
 *  FunctionGenerator.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 24/04/09.
 */

#include "FunctionGenerator.h"
#include <cmath>

double SinFunction::getOutput(double inTime) const {
	return mAmplitude*sin(2*M_PI*mFrequency*inTime - mPhase) + mZero; 
}

double CosFunction::getOutput(double inTime) const {
	return mAmplitude*cos(2*M_PI*mFrequency*inTime - mPhase) + mZero; 
}

double SquareFunction::getOutput(double inTime) const {
	if(sin(2*M_PI*mFrequency*inTime - mPhase) > 0)
		return mAmplitude + mZero; 
	else
		return mZero;
}

double PWMFunction::getOutput(double inTime) const {

	double lM = fmod((inTime-mPhase), (1/mFrequency));
	if( lM*mFrequency < mDutyCycle ) {
		return (mAmplitude + mZero); 
	} else {
		return mZero;
	}
}
