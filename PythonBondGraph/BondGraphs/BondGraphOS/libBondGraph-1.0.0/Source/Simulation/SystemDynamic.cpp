/*
 *  SystemDynamic.cpp
 *  Copyright 2008 Jean-Francois Dupuis.
 *
 *  This file is part of EvoStateController.
 *  
 *  EvoStateController is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  EvoStateController is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with EvoStateController.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 02/10/08.
 */

#include "SystemDynamic.h"

void SystemDynamic::simulate(double inEndTime) {
	vector<double> lTime;

#if defined(USE_RKF) || defined(USE_RKFVS)
	vector<double> lOutValue;
	rkf(mTime, inEndTime, mStateVariables, lOutValue);
	mStateVariables = lOutValue;
#else
	vector< vector<double> > lOutValue;
	rkdumb(mTime, inEndTime, mTimeStep, mStateVariables, lTime, lOutValue);

	mStateVariables.resize(lOutValue.back().size());
	for(unsigned int i = 0; i < lOutValue.back().size(); ++i) {
		mStateVariables[i] = lOutValue.back()[i]; 
	}
#endif
		
	mTime = inEndTime;
}

void SystemDynamic::reset() {
	mInputs.resize(0);
	mStateVariables = mInitialValues;
	mTime = 0;
}
