/*
 *  BondGraphDynamic.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 02/04/09.
 */

#ifndef BondGraphDynamic_H
#define BondGraphDynamic_H

#include "SystemDynamic.h"
#include "PACC/Math.hpp"

namespace BG {

class BondGraph;
class VariableName;
	
class BondGraphDynamic : public SystemDynamic {
public:
	BondGraphDynamic(BondGraph* inBondGraph, std::map<std::string, std::vector<double> > *inLogger);
	virtual ~BondGraphDynamic() { }
	
	virtual const std::vector<double>& getObservableVariables();
	
	virtual void initialize(const std::vector<double> &inValues);
	virtual void writeLog();
	
	const PACC::Matrix& getDuDt() const { return mDuDt; }
	
	PACC::Matrix mDuDt;
	PACC::Matrix mPreviousInput;
	double mPreviousTime;
	
	void saveSimulationState();
	void restoreSimulationState();
	
	void reorganiseStateVariable(const VariableName& inOldNames, const PACC::Matrix& inC, const PACC::Matrix& inD, 
								 const PACC::Matrix& inD2, const PACC::Matrix& inSd);
	
protected:
	BondGraph* mBondGraph;
	
#ifdef USE_RKF
	virtual void ode(float t, float *&y, float *&dydt);
#else
	virtual void ode(double t, const std::vector<double> & y, std::vector<double> & dydt);
#endif
	std::vector<double> mSavedStateVariables;
	double mSavedPreviousTime;
	std::vector<double> mSavedInputs;
	PACC::Matrix mSavedPreviousInput; 
	PACC::Matrix mSavedDuDt;
	double mSavedTime;
	
};
}
#endif
