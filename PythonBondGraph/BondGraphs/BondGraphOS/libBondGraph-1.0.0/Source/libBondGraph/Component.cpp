/*
 *  Component.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#include "Component.h"
#include <stdexcept>
#include "BGException.h"

using namespace BG;

bool ComponentCount::operator==(const ComponentCount &inRight) const {
	if(U != inRight.U) return false;
	if(R != inRight.R) return false;
	if(C != inRight.C) return false;
	if(I != inRight.I) return false;
	if(J != inRight.J) return false;
	return true;
}

Component::Component(ElementType inType, double inValue, long int inId) : BondGraphElement(inType, inId) {
	mValue = inValue;
}

void Component::connect(Port* inPort) { 
	if(mComponentGroup != eJ && mPorts.size() > 0)
		throw BGException("Single port component connected to more than one port!");
		
	
	mPorts.push_back(inPort); 
	inPort->connect(this); 
}

void Component::disconnect(Port* inPort) { 
	std::vector<Port*>::iterator lPortIter = find(mPorts.begin(), mPorts.end(), inPort);
	if(lPortIter != mPorts.end())
		mPorts.erase(lPortIter);
}

void Component::clearCausality() {
	for(unsigned int i = 0; i < mPorts.size(); ++i) {
		mPorts[i]->setCausality(eAcausal);
	}
}

const ComponentCount& Component::countConnectionByGroup() {
	mComponentCount.U = 0;
	mComponentCount.C = 0;
	mComponentCount.I = 0;
	mComponentCount.R = 0;
	mComponentCount.J = 0;
	
	for(std::vector<Port*>::const_iterator lIter = mPorts.begin(); lIter != mPorts.end(); ++lIter) {
		switch( (*lIter)->getBond()->getGroup() ) {
			case Bond::eU:
				++mComponentCount.U;
				break;
			case Bond::eS:
			case Bond::eSd:
				if((*lIter)->getBond()->getStorageType() == Bond::eC)
					++mComponentCount.C;
				else 
					++mComponentCount.I;
				break;	
			case Bond::eR:
				++mComponentCount.R;
				break;
			case Bond::eJ:
				++mComponentCount.J;
				break;
			default:
				throw BGException("Component::countConnectionByGroup() : Unknown group type");	
		}
	}
	
	return mComponentCount;
}
