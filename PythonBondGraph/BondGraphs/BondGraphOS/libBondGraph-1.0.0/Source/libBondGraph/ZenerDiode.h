/*
 *  ZenerDiode.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 20/10/09.
 */

#ifndef ZenerDiode_H
#define ZenerDiode_H

#include "Switch.h"
#include "SwitchController.h"

namespace BG {

class ZenerDiode : public Switch, public SwitchController {
	
public:
	ZenerDiode(double inVoltage);
	~ZenerDiode() {}
	
	void setId(long int inId);
	
	virtual void reset() {  }
	virtual void updateSwitchState(double inTime, const std::vector<double>& inInputs);
	virtual void writeLog();
	virtual void initialize();
	
private:
	double mBreakdownVoltage;
	double mPreviousI;
};
	
}

#endif
