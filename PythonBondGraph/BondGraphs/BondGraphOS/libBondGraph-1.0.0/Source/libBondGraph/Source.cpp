/*
 *  Source.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#include "Source.h"
#include <sstream>
#include <stdexcept>
#include "BGException.h"

using namespace std;
using namespace BG;

Source::Source(SourceType inType, double inValue, long int inId)
: mType(inType), Component(eSource,inValue) {
	setId(inId);
	setComponentGroup();
	mGenerator = 0;
}

Source::Source(SourceType inType, const FunctionGenerator* inGenerator, long int inId)
: mType(inType), Component(eSource,0) {
	setId(inId);
	setComponentGroup();
	mGenerator = inGenerator;
}

Source::~Source() {
	delete mGenerator;
}


void Source::setId(long int inId) {
	mId = inId;
	std::ostringstream lNameStream;
	lNameStream << SOURCEPREFIX << inId;
	mName = lNameStream.str();
}

/*! \brief Set the causality of the source according to his type.
 */
void Source::setCausality() {
	switch(mType) {
		case eEffort:
		case eEffortS:
			mPorts[0]->setCausality(eFlowCausal); //Causality at the origine of the bond
			break;
		case eFlow:
		case eFlowS:
			mPorts[0]->setCausality(eEffortCausal); //Causality at the origine of the bond
			break;			
	}
}

/*! \brief Apply the causality constrains of the component.
 *  This methods will apply the causality constrains of the component. That is,
 *	set the port to effort causal for a flow source and flow causal to an
 *	effort source. The inversion come from the fact that the port is
 *	at the beginning of the bond.
 *  \param  inPort Newly assign causality port.
 */
void Source::applyCausalityConstrain(Port* inPort) {
	switch(mType) {
		case eEffort:
		case eEffortS:
			if(mPorts[0]->getCausality() != eFlowCausal) //Causality at the origine of the bond
				throw BGException("Conflicting causality");
			break;
		case eFlow:
		case eFlowS:
			if(mPorts[0]->getCausality() != eEffortCausal) //Causality at the origine of the bond
				throw BGException("Conflicting causality");
			break;			
	}
}

double Source::getValue(double inTime) const {
	//Is the source constant
	if( (mType == eEffort) || (mType == eFlow) ) {
		return Component::getValue();
	}
	
	//The source make use of signal generator
	return mGenerator->getOutput(inTime);
}

void Source::write(PACC::XML::Streamer& inStream, bool inIndent) const {
	inStream.openTag("Component", inIndent);
	inStream.insertAttribute("type", "Source");
	inStream.insertAttribute("id", mId);
	inStream.insertAttribute("name", mName);
	inStream.insertAttribute("subtype", mType);
	inStream.insertAttribute("value", mValue);
	inStream.closeTag();
}
