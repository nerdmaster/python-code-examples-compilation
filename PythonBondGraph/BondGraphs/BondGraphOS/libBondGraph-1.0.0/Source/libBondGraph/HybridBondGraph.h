/*
 *  HybridBondGraph.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 02/04/09.
 */

#ifndef HybridBondGraph_H
#define HybridBondGraph_H

#include "BondGraph.h"
#include "Switch.h"
#include "SwitchController.h"

namespace BG {
	
//!	Structure to save variable names.
struct VariableName {
	std::vector<std::string> x;	//!< Name of the state variables.
	std::vector<std::string> xd;	//!< Name of the storage variables in derivative causality.
	std::vector<std::string> u;	//!< Name of the source variables.
	std::vector<std::string> out;//!< Name of the output variables.
};

//! Structure to save component type counts
struct VariableCount {
	int u;	//!< Number of source bond
	int s;	//!< Number of storage bond with integral causality
	int sd;	//!< Number of storage bond with derivative causality
	int r;	//!< Number of dissipative bond
	int j;	//!< Number of junction bond
};
	
	
/*! \brief Hybrid %Bond Graph
 This class describes a hybrid bond graph, which extend the classical bond graph 
 with switches. A hybrid bond graph is define in the same maner as the normal bond
 graph describes in BondGraph. Switch are then added to the bond graph with the 
 addSwitch methods.
 
 Switches in the bond graph act like sources. When the state of a switch is true,
 that means that it will act as a flow source set to zero. On the other hand, when
 it's set to false, that means that the switch now act as an effort source set to zero.
 
 In order to control the state of the switches throughout the simulation, a SwitchController
 need to be provided to the class constructor. This controller will be user defined 
 so that it will process the input and generate the appropriate output switch state
 vector.
 
 As changing the state of the switches might change dramatically the state equation, 
 each switch states are then associated with respective state equation. Therefore,
 when a new state is encountered, the causality and state equation for this switch
 configuration is computed. When the same switch state that was used before is encountered,
 then the state matrices are reloaded from memory avoiding to recomputing them.
 */
class HybridBondGraph : public BondGraph {
protected:
	std::vector<bool> mInitialState;			//!< Initial switch states.
	unsigned int mState;						//!< Bond graph state from the binary representation of all switches.
	
	unsigned int mSwitchIdCounter;				//!< Id counter for switches.
	std::vector<Switch*> mSwitches;				//!< List of all the switches
	std::vector<Source*> mPureSources;			//!< List of all the sources, excluding the one that are also switches
	std::vector<SwitchController*> mControllers;		//!< User defined switch controllers.
	
	std::map<unsigned int, std::vector<PACC::Matrix> > mStateMatrix;	//!< State equation matrix A, B and B2 for each state.
	std::map<unsigned int, std::vector<PACC::Matrix> > mOutputMatrix; //!< Output equation matrix C, D and D2 for each state.
	std::map<unsigned int, VariableName > mNames;                //!< Name of equation variables for each state.
	std::map<unsigned int, VariableCount > mCounts;              //!< Count of component type for each state.

	std::vector<PACC::Matrix> mOutputMatrixCurrent; //!< Hold the output matrix for transition after matrix clear
	VariableName mNamesCurrent;  //!< Hold the output matrix for transition after matrix clear
	
	void updateStateMatrix(bool inIsInitialization=false);
	
	bool simplifyRemoveRedundantSwitch();
	virtual void simplifyRemoveEmptyJunction();
	
	//Initial parameter matrix
	std::map<unsigned int, std::vector<PACC::Matrix> > mInitialStateMatrix;	
	std::map<unsigned int, std::vector<PACC::Matrix> > mInitialOutputMatrix;
	std::map<unsigned int, VariableName > mInitialNames;         
	std::map<unsigned int, VariableCount > mInitialCounts;       
	
public:
	HybridBondGraph(/*SwitchController *inController*/);
	~HybridBondGraph();

	HybridBondGraph(const HybridBondGraph& inBondGraph);
	HybridBondGraph& operator=(const HybridBondGraph& inBondGraph);
	
	
	virtual void read(PACC::XML::ConstIterator inNode);
	
	virtual void simplify();
	
	//! Reassign initial state of the hybrid bond graph.
	void reset();
	void clearStateMatrix();
	
	const std::vector<SwitchController*>& getControllers() const { return mControllers; }
		
	void setInitialSwitchState(const std::vector<bool> &inSwitchesValue);
	void setInitialStateVariable(const std::vector<double> &inStateValues);
	void setInitialState(const std::vector<bool> &inSwitchesValue, const std::vector<double> &inStateValues);
	void getSwitchState(std::vector<bool>& outState) const;
	
	const std::vector<Switch*> getSwitches() const { return mSwitches; }
	
	//! Add a switch to the hybrid bond graph.
	void addSwitch(Switch *inSwitch);
	
	//! Add a switch controller and also add related switch to the hybrid bond graph.
	void addSwitchController(SwitchController *inController);
	
	void saveAsParameterMatrix();
	void simulate(double inStopTime, double inSimulationStep, bool inReset=true, bool inWithInitialParameters=false);

	void simulateVirtualFixParameters(const std::vector<bool>& inSwitchState, double inStep, std::vector<double>& outResults, std::vector<double>& outStates);
	void simulateVirtual(const std::vector<bool>& inSwitchState, double inStep, std::vector<double>& outResults, std::vector<double>& outStates);
	void simulateVirtual(const std::vector<double>& inState, const std::vector<bool>& inSwitchState, double inStep, std::vector<double>& outResults, std::vector<double>& outStates);
	
	void postConnectionInitialization();
	
	//const std::vector<Source*>& getSources() const;
	std::vector<Source*>& getSources();
	
	virtual void replaceComponent(Component*& inOldComponent, Component*& inNewComponent);
	virtual void removeComponent(Component* inComponent);
};
}
#endif
