/*
 *  Junction.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#include "Junction.h"
#include <sstream>
#include <stdexcept>
#include <vector>

using namespace BG;

Junction::Junction(JunctionType inType, long int inId) 
: mType(inType), Component(eJunction) {
	setId(inId);
	setComponentGroup();
}

void Junction::setId(long int inId) {
	mId = inId;
	std::ostringstream lNameStream;
	lNameStream << JUNCTIONPREFIX;
	switch(mType) {
		case eZero:
			lNameStream << "0_";
			break;
		case eOne:
			lNameStream << "1_";
			break;
	}
	lNameStream << inId;
	mName = lNameStream.str();
}

/*! \brief Apply the causality contrains of the junction.
 *  This methods will apply the causality constrains of the junction to 
 *	the other ports. 
 *	For 0-junction, only one port can have an effort causality stroke.
 *
 *	For 1-junction, only one port can have a flow causality. The other 
 *	should have an effort causality stroke.
 *  \param  inPort Newly assign causality port.
 */
void Junction::applyCausalityConstrain(Port* inPort) {
	if(mType == eZero) {
		if(inPort->getCausality() == eFlowCausal) {
			//Check if all except one has flow causality, then assign effort causality to this last one
			std::vector<Port*> lUnassignedPorts;
			for(unsigned int i = 0; i < mPorts.size(); ++i) {
				if(mPorts[i] != inPort) {
					if(	mPorts[i]->getCausality() == eAcausal ) {
						lUnassignedPorts.push_back(mPorts[i]);
					}
				}
			}
			if(lUnassignedPorts.size() == 1) {
				//There is only one port left and all the other are flow causal. Then, it should be an effort causal port.
				lUnassignedPorts[0]->setCausality(eEffortCausal);
			}
		} else {
			//Assign flow causality to all other ports as the base effort one is defined
			for(unsigned int i = 0; i < mPorts.size(); ++i) {
				if(mPorts[i] != inPort) {
					mPorts[i]->setCausality(eFlowCausal);
				}
			}
		}
	} else {
		if(inPort->getCausality() == eEffortCausal) {
			//Check if all except one has effort causality, then assign flow causality to this last one
			std::vector<Port*> lUnassignedPorts;
			for(unsigned int i = 0; i < mPorts.size(); ++i) {
				if(mPorts[i] != inPort) {
					if(	mPorts[i]->getCausality() == eAcausal ) {
						lUnassignedPorts.push_back(mPorts[i]);
					}
				}
			}
			if(lUnassignedPorts.size() == 1) {
				//There is only one port left and all the other are effort causal. Then, it should be an flow causal port.
				lUnassignedPorts[0]->setCausality(eFlowCausal);
			}
		} else {
			//Assign effort causality to all other ports as the base flow one is defined
			for(unsigned int i = 0; i < mPorts.size(); ++i) {
				if(mPorts[i] != inPort) {
					mPorts[i]->setCausality(eEffortCausal);
				}
			}
		}
	}
}

void Junction::write(PACC::XML::Streamer& inStream, bool inIndent) const {
	inStream.openTag("Component",inIndent);
	inStream.insertAttribute("type", "Junction");
	inStream.insertAttribute("id", mId);
	inStream.insertAttribute("name", mName);
	inStream.insertAttribute("subtype", mType);
	inStream.closeTag();
}
