/*
 *  Passive.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#ifndef Passive_H
#define Passive_H

#include <string>
#include "BondGraphElement.h"
#include "Component.h"

namespace BG {

/*! \brief %Passive element of a bond graph
 *  This class describes a passive component of a bond graph.
 */
class Passive : public Component {
public:
	// Transformater and gyrator could probably move to the junction class.
	//!Passive type
	enum PassiveType { 
		eResistor,		//!< A resistor component
		eResistorM,		//Not supported
		eConductor,		//Not supported
		eConductorM,	//Not supported
		eCapacitor,		//!< A capacitance component
		eCapacitorM,	//Not supported
		eInductor,		//!< A inertial component
		eInductorM,		//Not supported
		eTransformer,	//!< A tranformator
		eTransformerM,	//Not supported
		eGyrator,		//!< A gyrator
		eGyratorM		//Not supported
	};
	
private:
	PassiveType mType;	//!< Passive type

public:
	Passive(PassiveType inType, double inValue = 0, long int inId = -1);
	virtual ~Passive() {}
	
	//! Set the type of the passive component.
	void setType(PassiveType inType);
	//! Return the type of the passive component.
	PassiveType getType() const { return mType; }

	//! Set the ID of the passive component.
	void setId(long int inId);
	
	virtual void applyCausalityConstrain(Port* inPort);
	
	//! Set the component group according to his type.
	virtual void setComponentGroup();
	//! Check if the component has an integral causality.
	bool isIntegralCausality() const;
	
	virtual void write(PACC::XML::Streamer& inStream, bool inIndent=true) const;
};
}


#endif
