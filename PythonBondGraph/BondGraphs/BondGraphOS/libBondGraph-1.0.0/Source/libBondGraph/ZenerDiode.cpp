/*
 *  ZenerDiode.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 20/10/09.
 */

#include "ZenerDiode.h"
#include "BondGraphDefines.h"
#include <cmath>
#include <assert.h>

using namespace BG;

ZenerDiode::ZenerDiode(double inVoltage) : mBreakdownVoltage(inVoltage),mPreviousI(0) {
	this->push_back((Switch*)(this));
}

void ZenerDiode::setId(long int inId) {
	mId = inId;
	std::ostringstream lNameStream;
	lNameStream << "DZ" << inId;
	mName = lNameStream.str();
}

void ZenerDiode::initialize() {
	addTrigger(mPorts[0]->getBond(), BG::eEffort);
	addTrigger(mPorts[0]->getBond(), BG::eFlow);
}

void ZenerDiode::writeLog() {
	(*mLogger)[mName+std::string(".state")].push_back(getState());
}

void ZenerDiode::updateSwitchState(double inTime, const std::vector<double>& inInputs) {
	assert(size() == 1);
	assert(mSwitchTriggers.size() == 2);
	
	double lV = inInputs[mSwitchTriggers[0]->getIndex()];
	double lI = inInputs[mSwitchTriggers[1]->getIndex()];
	
	if(!getState()) { 
		//ZenerDiode is in conducting mode
		if( (lI > 0) && (fabs(lV) < mBreakdownVoltage) ) {
			setState(true); //Turn off the diode if reverting current and if the voltage is smaller that the breakdown voltage.
		}
		if( (lI < 0) && (lV > 0) ) {
			setState(true); //The diode start producing energy as a source.
		}
	} else {
		//ZenerDiode is not in conducting mode
		if( (lV < 0) || (fabs(lV) > mBreakdownVoltage) ) {
			setState(false); //Turn on the diode if voltage is positive or if the voltage is greater that the breakdown voltage
		}
	}
	
	if(!getState()) {
//		if(lI < 0) {
//			Component::setValue(0);		//When in forward conduction mode, the zener as no forward voltage
//		} else {
			Component::setValue(mBreakdownVoltage+1e-6); //In conducting mode, the zener has constant voltage
//		}
	} else {
		Component::setValue(0);
	}
	

			
}
