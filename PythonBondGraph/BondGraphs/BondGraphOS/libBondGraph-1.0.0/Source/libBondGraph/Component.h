/*
 *  Component.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#ifndef Component_H
#define Component_H

#include <vector>
#include "BondGraphElement.h"
#include "Port.h"

namespace BG {
	
class Port;
	
//! Connected component counts.
class ComponentCount {
public:
	int U;		//!< Number of source element (Se,Sf)
	int C;		//!< Number of storage element C
	int I;		//!< Number of storage element I
	int R;		//!< Numer of decipative element (R)
	int J;		//!< Number of junction with power conservation (J,GY,TR)
	
	bool operator==(const ComponentCount &inRight) const;
};
	
	
/*! \brief %Bond Graph component
 *  This class describes a bond graph component. A component is linked to a Bond
 *	throught a Port.
 */
class Component : public BondGraphElement {
public:
	//! Component groups
	enum ComponentGroup {
		eU,		//!< Source element (Se,Sf)
		eS,		//!< Storage element (I,C)
		eR,		//!< Decipative element (R)
		eJ		//!< Junction with power conservation (J,GY,TR)
	};
	

	
protected:
	std::vector< Port* > mPorts;	//!< Ports connected to the component. Size depends on the type of component.
	double mValue;					//!< Basic value of the component.
	ComponentGroup mComponentGroup;	//!< Component group association.
	ComponentCount mComponentCount; //!< Connected component counted by group type.
	
public:
	Component(ElementType inType, double inValue = 0, long int inId = -1);
	virtual ~Component() {}
	
	//! Set the value of the component.
	void setValue(double inValue) { mValue = inValue; }
	//! Return the value of the component.
	virtual double getValue() const { return mValue; }
	
	//! Return the ports associated to the component.
	std::vector<Port*>& getPorts() { return mPorts; }
	Port* getPorts(unsigned int i) { return mPorts[i]; }
	
	//! Connect the component to a port.
	void connect(Port* inPort);
	
	//! Disconnect the component of a port
	void disconnect(Port* inPort);
	
	//! Set all associated port causality to an acausal.
	void clearCausality();
	
	const ComponentCount& countConnectionByGroup();
	
	//! Apply the causality constrains to the other ports of the component.
	virtual void applyCausalityConstrain(Port* inPort) = 0;
	
	//! Return the component associated group.
	ComponentGroup getComponentGroup() const { return mComponentGroup; }
	//! Find the component association group.
	virtual void setComponentGroup() = 0;
	
	virtual void write(PACC::XML::Streamer& inStream, bool inIndent=true) const = 0;
};
	
}

#endif
