/*
 *  SwitchME.cpp
 *  Copyright 2010 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 29/03/10.
 */

#include "SwitchME.h"

using namespace BG;

//SwitchME::SwitchME(long int inDependentSwitchId, bool inInitialState, long int inId)
//: Switch(inInitialState, inId) { 
//	mDependentSwitch = 0;
//	mDependentSwitchId = inDependentSwitchId;
//}

SwitchME::SwitchME(Switch* inSwitch, bool inMutualyExclusive, bool inInitialState, long int inId)
: Switch(inInitialState, inId), mIsMutuallyExclusive(inMutualyExclusive) { 
	mDependentSwitch = inSwitch;
	mDependentSwitch->setSwitchId(-1);
}

void SwitchME::setState(bool inState) {
	this->BG::Switch::setState(inState);
	if(mIsMutuallyExclusive)
		mDependentSwitch->BG::Switch::setState(!inState);
	else
		mDependentSwitch->BG::Switch::setState(inState);
}

void SwitchME::setType(SourceType inType) {
	this->BG::Switch::setType(inType);
	if(mIsMutuallyExclusive) {
		if(mType == eFlow) {
			mDependentSwitch->BG::Switch::setType(eEffort);
		}
		else {
			mDependentSwitch->BG::Switch::setType(eFlow);
		}
	} else {
		mDependentSwitch->BG::Switch::setType(inType);
	}
		
}

void SwitchME::setComponentGroup() { 
	this->BG::Switch::setComponentGroup();
	mDependentSwitch->BG::Switch::setComponentGroup();
}

//void connectDependentSwitch(HybridBondGraph* inBondGraph) {
//	mDependentSwitch = 
//}

void SwitchME::write(PACC::XML::Streamer& inStream, bool inIndent) const {
	inStream.openTag("Component", inIndent);
	inStream.insertAttribute("type", "SwitchME");
	inStream.insertAttribute("id", mId);
	inStream.insertAttribute("swid", mSwitchId);
	inStream.insertAttribute("swid2", mDependentSwitch->getId());
	inStream.insertAttribute("name", mName);
	inStream.closeTag();
}
