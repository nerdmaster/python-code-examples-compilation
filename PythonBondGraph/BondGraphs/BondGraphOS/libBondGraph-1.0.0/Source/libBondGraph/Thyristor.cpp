/*
 *  Thyristor.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 16/10/09.
 */

#include "Thyristor.h"
#include "BondGraphDefines.h"
#include <assert.h>

using namespace BG;

Thyristor::Thyristor() {
	push_back((Switch*)(this));
}

void Thyristor::initialize() { 
	addTrigger(mPorts[0]->getBond(), BG::eEffort);
	addTrigger(mPorts[0]->getBond(), BG::eFlow);
	addTrigger(mGateBondTrigger, BG::eEffort);
}

void Thyristor::updateSwitchState(double inTime, const std::vector<double>& inInputs) {
	assert(size() == 1);
	assert(mSwitchTriggers.size() == 3);
		
	double lV = inInputs[mSwitchTriggers[0]->getIndex()];
	double lI = inInputs[mSwitchTriggers[1]->getIndex()];
	double lG = inInputs[mSwitchTriggers[2]->getIndex()];
	
	if(!getState()) { 
		//Thyristor is in conducting mode
		if(lI <= 0)
			setState(true);
	} else {
		//Thyristor is not in conducting mode
		if(lV > 0 && lG > 0)
			setState(false);
	}
	
}

