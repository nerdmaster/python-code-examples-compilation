/*
 *  Junction.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#ifndef Junction_H
#define Junction_H

#include <string>
#include "BondGraphElement.h"
#include "Component.h"

namespace BG {

/*! \brief %Junction component of a bond graph
 *  This class describes a junction component of a bond graph.
 */
class Junction : public Component {
public:
	//! Type of junction.
	enum JunctionType { 
		eZero,	//!< O-junction
		eOne	//!< 1-junction
	};
	
private:
	JunctionType mType; //! Junction Type.
	
public:
	Junction(JunctionType inType, long int inId = -1);
	virtual ~Junction() {}
	
	//! Set the junction type.
	void setType(JunctionType inType) { mType = inType; }
	//! Return the junction type.
	JunctionType getType() const { return mType; }
	

	void setId(long int inId);
	virtual void applyCausalityConstrain(Port* inPort);
	
	//! Set the component association group.
	virtual void setComponentGroup() { mComponentGroup = eJ; }
	
	virtual void write(PACC::XML::Streamer& inStream, bool inIndent=true) const;
};
}


#endif
