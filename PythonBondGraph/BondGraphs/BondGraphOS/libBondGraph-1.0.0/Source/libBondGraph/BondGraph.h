/*
 *  BondGraph.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#ifndef BondGraph_H
#define BondGraph_H

#include <vector>
#include "PACC/Math.hpp"
#ifndef WITHOUT_GRAPHVIZ
#include <graphviz/gvc.h>
#endif
#include "BondGraphElement.h"
#include "Bond.h"
#include "Junction.h"
#include "Passive.h"
#include "Source.h"
#include <string>
#include "Junction.h"
#include "BondGraphDynamic.h"

namespace BG {
	
/*! \brief %Bond Graph
	This class describes a bond graph composed of components and bonds. 
	To fully define a bond graph, one needs to define each components 
	using the addComponent method. Then, components are connected together
	with bonds using the connect method.

	Here is an example how to create a bond graph:
	 \code
	 //Create the source
	 Source *lSf = new Source(Source::eFlow,2);
	 ioBondGraph.addComponent(lSf);
	 
	 //Create the jonctions
	 Junction *lJ0_1 = new Junction(Junction::eZero);
	 ioBondGraph.addComponent(lJ0_1);
	 
	 Junction *lJ1_1 = new Junction(Junction::eOne);
	 ioBondGraph.addComponent(lJ1_1);
	 
	 Junction *lJ0_2 = new Junction(Junction::eZero);
	 ioBondGraph.addComponent(lJ0_2);
	 
	 //Create the resistor
	 Passive *lR = new Passive(Passive::eResistor, 2000);
	 ioBondGraph.addComponent(lR);
	 
	 //Create the storage
	 Passive *lC1 = new Passive(Passive::eCapacitor, 2e-4);
	 ioBondGraph.addComponent(lC1);
	 
	 Passive *lI = new Passive(Passive::eInductor,0.1);
	 ioBondGraph.addComponent(lI); 
	 
	 Passive *lC2 = new Passive(Passive::eCapacitor,3e-3);
	 ioBondGraph.addComponent(lC2);
	 
	 //Create the bonds
	 ioBondGraph.connect(lSf,lJ0_1);
	 ioBondGraph.connect(lJ0_1,lC1); 
	 ioBondGraph.connect(lJ0_1,lJ1_1);
	 ioBondGraph.connect(lJ1_1,lI);
	 ioBondGraph.connect(lJ1_1,lJ0_2);
	 ioBondGraph.connect(lJ0_2,lC2);
	 ioBondGraph.connect(lJ0_2,lR);
	 \endcode
 
	Both element and bond ID start at 1. These ID are attributed when the
	element or the bond is added to the bond graph.

	The bond graph is represented as a linked list. All elements of the bond
	graph are interconnected with Port. Each port connect a Bond and a Component.
	Therefor, two component are linked like this : 
		%Component -> %Port -> %Bond -> %Port -> %Component
 */

class BondGraph {
protected:
	unsigned int mElementIdCounter;			//!< Id counter for elements
	unsigned int mBondIdCounter;			//!< Id counter for bonds
	
	std::vector<Component*> mComponents;	//!< List of all elements
	std::vector<Bond*> mBonds;				//!< List of all bonds
	

	bool mAllowDifferentialCausality;		//!< Flag use to know if differential causality is allowed during simulation.
	
#ifndef WITHOUT_GRAPHVIZ
	//GVC_t* mContext;
	void plotGraphConnection(Component* inComponent, Agraph_t *inGraph, std::vector<Agnode_t*> &inNodes, bool inShowBondId);
#endif
	
	bool parsingCompare(Component* inComponent1, Component* inComponent2);
	
	//Simplification related members
//	Source* combineRedundantSource(vector<Source*>& inComponents, Junction::JunctionType inJunctionType);
	Passive* combineRedundantPassive(std::vector<Passive*>& inComponents, Junction::JunctionType inJunctionType);
	bool simplifyRemoveDoubleBond();
	virtual void simplifyRemoveEmptyJunction();
	void simplifyRemoveRedundantJunction();
	bool simplifyRemoveRedundantComponents();
	void simplifyRemoveTerminalJunction();
	void simplifyRemoveUnconnectedComponent();
	
	//Causality assignment related members and methods
	void resetPortFlag();
	void propagateCausality(Component *inComponent, Port* inPort);
	void countComponentType();
	
	//State equation related members and methods
	int mUcount;	//!< Number of source bond
	int mScount;	//!< Number of storage bond with integral causality
	int mSDcount;	//!< Number of storage bond with derivative causality
	int mRcount;	//!< Number of dissipative bond
	int mJcount;	//!< Number of junction bond
	
	std::vector<Source*> mSources;
	std::vector<std::string> mNameX;		//!< Name of the state variables.
	std::vector<std::string> mNameXd;	//!< Name of the storage variables in derivative causality.
	std::vector<std::string> mNameU;		//!< Name of the source variables.
	std::vector<std::string> mOutputName;//!< Name of the output variables.
	
	PACC::Matrix mS;		//!< Matrix defined such that \f$z = S x\f$
	PACC::Matrix mSd;		//!< Matrix defined such that \f$z_d = S_d x_d\f$
	PACC::Matrix mL;		//!< Matrix defined such that \f$d_o = L d_i\f$
	PACC::Matrix mJ;		//!< Matrix defined such that \f$[\dot{x},z_d,d_i,v]^T = J [z,\dot{x_d},d_o,u]^T\f$
	PACC::Matrix mJj;		//!< Matrix defined such that \f$[J_{e},J_{f}]^T = J_j [z,\dot{x_d},d_o,u]^T\f$
	PACC::Matrix mJFE;		//!< Matrix defined such that \f$[\dot{x},z_d,d_i,v]^T = J_{FE} [z,\dot{x_d},d_o,u]^T\f$
	PACC::Matrix mJIE;		//!< Matrix defined such that \f$[J_{e},J_{f}]^T = J_{IE} [z,\dot{x_d},d_o,u]^T\f$
	PACC::Matrix mJFI;		//!< Matrix defined such that \f$[\dot{x},z_d,d_i,v]^T = J_{FI} [J_{e},J_{f}]^T\f$
	PACC::Matrix mJII;		//!< Matrix defined such that \f$[J_{e},J_{f}]^T = J_{II}*[J_{e},J_{f}]^T\f$, where \f$J_e\f$ and \f$J_f\f$ are the effort and flow vector of the junctions bonds. For example \f$[J_{e},J_{f}]^T = [e_1,e_2,e_3,f_1,f_2,f_3]^T\f$
	PACC::Matrix mA;		//!< Matrix defined such that \f$\dot{x} = Ax + Bu\f$
	PACC::Matrix mB;		//!< Matrix defined such that \f$\dot{x} = Ax + Bu\f$
	PACC::Matrix mB2;		//!< Matrix defined such that \f$\dot{x} = Ax + Bu + B_2 \dot{u}\f$
	PACC::Matrix mC;		//!< Matrix defined such that \f$[z,d_i,d_o,v,J_{e},J_{f}]^T = Cx + Du\f$
	PACC::Matrix mD;		//!< Matrix defined such that \f$[z,d_i,d_o,v,J_{e},J_{f}]^T = Cx + Du\f$
	PACC::Matrix mD2;		//!< Matrix defined such that \f$[z,z_d,\dot{x_d},d_i,d_o,v,J_{e},J_{f}]^T = Cx + Du + D_2 \dot{u}\f$
	PACC::Matrix mCr;		//!< Matrix defined such that \f$y = C_r x + D_r u\f$, where y is made of the effort and flow of the output bonds 
	PACC::Matrix mDr;		//!< Matrix defined such that \f$y = C_r x + D_r u\f$, where y is made of the effort and flow of the output bonds
	PACC::Matrix mD2r;		//!< Matrix defined such that \f$y = C_r x + D_r u + D_{2r} \dot{u}\f$, where y is mode of the effort and flow of the output bonds
	
	void setJMatrixElement(const std::vector< Port* > &inPorts, Passive::PassiveType inType);
	void setJMatrixElement(const Port* inBasePort, const Port* inPort, Junction::JunctionType inType);
	
	void initializeMatrix();
	void computeSMatrix();
	void computeLMatrix();
	void computeJMatrix();
	void computeABMatrix();
	void computeCDMatrix();
	
	std::vector<Bond*> mEffortOutputBonds;	//!< Bonds that are designated to be an effort outport.
	std::vector<Bond*> mFlowOutputBonds;		//!< Bonds that are designated to be a flow output.
	
	void computeReducedOutputMatrix();
	
#ifdef DEBUG_EQN
	void displayEquation(const Port* inBasePort, const Port* inPort, Junction::JunctionType inType);
#endif
	
	//Simulation related members
	std::vector<double> getSourceValues(double inTime) const;
	BG::BondGraphDynamic mDynamics;
	std::map<std::string, std::vector<double> > mSimulationLog;

public:
	BondGraph();
	virtual ~BondGraph();
	
	BondGraph(const BondGraph& inBondGraph);
	BondGraph& operator=(const BondGraph& inBondGraph);
	
	Component* findUnvisitedComponent();
#ifndef WITHOUT_GRAPHVIZ
	void plotGraph(std::string inFilename, bool inShowBondId = true, char* inFormat="svg");
#endif
	//!Write the bond graph in XML form.
	std::string serialize(bool inIndent=false) const;
	void write(PACC::XML::Streamer& inStream, bool inIndent=true) const;
	virtual void read(PACC::XML::ConstIterator inNode);
	
	virtual void simplify();
	
	bool compare(BondGraph& inBondgraph, const Bond* inStart1 = 0, const Bond* inStart2 = 0);
	
	//Creation related methods
	void addComponent(Component*  inComponent);
	void addBond(Bond* inBond);
	
	Bond* connect(Component *inOrigin, Component *inDest, Causality inCausality=eAcausal, int inBondId=-1);
	
	//Manipulation related methods
	void removeBond(Bond* inBond);
	virtual void removeComponent(Component* inComponent);
	void removeComponent(std::vector<Component*>& inComponents);
	void reconnectComponent(Port* inComponentPort, Component* inNewOrigin);
	virtual void replaceComponent(BG::Component*& inOldComponent, BG::Component*& inNewComponent);
	
	//Causality computation related methods
	void clearCausality();
	void assignCausality();
	
	//State equation related methods
	void computeStateEquation();
	
	void addOutputBonds(Bond* inBond, ValueType inType);
	void setOutputBonds(Bond* inEffortBond, Bond* inFlowBonds);
	void setOutputBonds(const std::vector<Bond*> &inEffortBonds, const std::vector<Bond*> &inFlowBonds);
	const std::vector<Bond*>& getEffortOutputBonds() const { return mEffortOutputBonds; }
	const std::vector<Bond*>& getFlowOutputBonds() const { return mFlowOutputBonds; }
	
	bool hasDeferentialCausality() const { return(mSDcount > 0 ? true : false); }
	void getStateMatrix(PACC::Matrix &outA, PACC::Matrix &outB, PACC::Matrix &outB2) const { outA = mA; outB = mB; outB2 = mB2; }
	void getStateMatrix(PACC::Matrix &outA, PACC::Matrix &outB) const { outA = mA; outB = mB;}
	
	//! Get the complete output matrix for all the effort and flow value of all the bonds in the bond graph.
	void getFullOutputMatrix(PACC::Matrix &outC, PACC::Matrix &outD, PACC::Matrix &outD2) const { outC = mC; outD = mD; outD2 = mD2; }
	//! Get the complete output matrix for all the effort and flow value of all the bonds in the bond graph.
	void getFullOutputMatrix(PACC::Matrix &outC, PACC::Matrix &outD) const { outC = mC; outD = mD; }

	/*! Get the output matrix for the desired effort and flow values defined by setOutputBonds. 
	 *	The returned matrix is a reduced and sorted version of the matrices obtained by getFullOutputMatrix.
	 *	The effort values are placed at the beginning of the output vector, while the flow values are then placed
	 *	at the end.
	 */
	void getOutputMatrix(PACC::Matrix &outC, PACC::Matrix &outD, PACC::Matrix &outD2) const { outC = mCr; outD = mDr; outD2 = mD2r; }
	void getOutputMatrix(PACC::Matrix &outC, PACC::Matrix &outD) const { outC = mCr; outD = mDr; }

	
	//Simulation related methods
	void setDifferentialCausalitySupport(bool inPolicy) { mAllowDifferentialCausality = inPolicy; }
	void setInitialState(const std::vector<double> &inValues);
	const std::vector<double>& getStateVariables() const;
	//void getStateVariables(std::vector<double>& outStates) const; //!< Return x
	//void getOutputVariables(std::vector<double>& outOutputs);
	const std::vector<double>& getOutputVariables();
	double getSimulationTime() const { return mDynamics.getTime(); }
	const std::vector<double>& getInputs() const;	//!< Return u
	const std::vector<double>& getInputsDt() const; //!< Return du/dt
	void simulate(double inStopTime, double inSimulationStep/*, double inLogStep*/);
	void writeSimulationLog(std::string inFilename);
	std::map<std::string, std::vector<double> >& getSimulationLog() { return mSimulationLog; }
	
	
	//const int getNumberStateVariables() { return mScount; }
	const std::vector<std::string>& getStateVariableNames() const { return mNameX; }
	const std::vector<std::string>& getDerivativeVariableNames() const { return mNameXd; }
	const std::vector<std::string>& getSourceVariableNames() const { return mNameU; }
	const std::vector<std::string>& getOutputVariableNames() const { return mOutputName; }

	const std::vector<Component*>& getComponents() const { return mComponents; }
	std::vector<Component*>& getComponents() { return mComponents; }
	//virtual const std::vector<Source*>& getSources() const { return mSources; }
	virtual std::vector<Source*>& getSources() { return mSources; }
	virtual const std::vector<Bond*>& getBonds() const { return mBonds; }
};
}


#endif
