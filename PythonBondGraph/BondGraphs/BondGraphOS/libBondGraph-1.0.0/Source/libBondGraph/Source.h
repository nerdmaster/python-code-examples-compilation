/*
 *  Source.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#ifndef Source_H
#define Source_H

#include <string>
#include "BondGraphElement.h"
#include "Component.h"
#include "FunctionGenerator.h"

namespace BG {


/*! \brief %Source element of a bond graph
 *  This class describes a source component of a bond graph.
 */
class Source : public Component {
public:
	//! Source type
	enum SourceType { 
		eEffort,		//!< Effort source with constant amplitude.
		eEffortS,		//!< Effort source using signal generator.
		eFlow,			//!< Flow source with constant amplitude.
		eFlowS,			//!< Flow source using signal generator.
	};
	
protected:
	SourceType mType;	//! Source type.
	
	const FunctionGenerator* mGenerator; //! Source signal generator
	
public:
	Source(SourceType inType, double inValue = 0, long int inId = -1);
	Source(SourceType inType, const FunctionGenerator* inGenerator, long int inId = -1);
	virtual ~Source();
	
	double getValue(double inTime) const;
	
	//! Set the type of source.
	virtual void setType(SourceType inType) { mType = inType; }
	//! Return the type of source.
	SourceType getType() const { return mType; }
	
	//! Set the source ID.
	void setId(long int inId);
	
	void setCausality();
	virtual void applyCausalityConstrain(Port* inPort);
	
	//! Set the component associated group.
	virtual void setComponentGroup() { mComponentGroup = eU; }
	
	virtual void write(PACC::XML::Streamer& inStream, bool inIndent=true) const;
}; 
}


#endif
