/*
 *  BondGraphDefines.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#ifndef BondGraphDefines_H
#define BondGraphDefines_H

namespace BG {

#define	SWITCHPREFIX "Sw"				//!< Switch variable prefix
#define SOURCEPREFIX "S"				//!< Source variable prefix
#define PASSIVERESISTORPREFIX "R"		//!< Resistor variable prefix
#define PASSIVECONDUCTORPREFIX "G"		//!< Conductor variable prefix
#define PASSIVECAPACITORPREFIX "C"		//!< Capacitor variable prefix
#define PASSIVEINDUCTORPREFIX "I"		//!< Inductor variable prefix
#define PASSIVETRANSFORMERPREFIX "TF"	//!< Transformer 
#define PASSIVEGYRATORPREFIX "GY"		//!< Gryrator variable prefix
#define JUNCTIONPREFIX "J"				//!< Junction variable prefix
#define BONDPREFIX "B"					//!< Bond variable prefix


//! Causality of a Bond
enum Causality { 
	eAcausal,		//!< A-causal bond
	eEffortCausal,	//!< Causality stroke is at the origin of the bond
	eFlowCausal		//!< Causality stroke is at the end of the bond
};
	
	
enum ValueType { 
	eEffort,
	eFlow
};
	
}

#endif
