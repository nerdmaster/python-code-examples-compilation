/*
 *  SwitchME.h
 *  Copyright 2010 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 29/03/10.
 */

#ifndef SwitchME_H
#define SwitchME_H

#include "Switch.h"

namespace BG {

class SwitchME : public BG::Switch {
public:
//	SwitchME(long int inDependentSwitchID, bool inInitialState = false, long int inId = -1);
	SwitchME(Switch* inDependentSwitch, bool inMutualyExclusive=true, bool inInitialState = false, long int inId = -1);
	~SwitchME() {}
	
	//! Set the state of the switch. True means that flow is zero, false means that effort is zero.
	virtual void setState(bool inState);
	
	//! Set the type of source. Will change the state accordingly.
	virtual void setType(SourceType inType);
	
	//! Set the switch ID.
	virtual void setSwitchId(long int inId) { mSwitchId = inId; }
	//! Return the switch ID.
	virtual long int getSwitchId() const { return mSwitchId; }
	
	//! Set the component associated group.
	virtual void setComponentGroup();
	
//	//! Connect the dependent switch after reading
//	void connectDependentSwitch(HybridBondGraph* inBondGraph);
	
	virtual void write(PACC::XML::Streamer& inStream, bool inIndent=true) const;
protected:
	long int mDependentSwitchId; //!< Dependent switch Id. Use temporary when reading the bond graph.
	Switch* mDependentSwitch;
	bool mIsMutuallyExclusive; //!< Determine if the switch are commuted together or in a mutually exclusive way.
};
	
}

#endif
