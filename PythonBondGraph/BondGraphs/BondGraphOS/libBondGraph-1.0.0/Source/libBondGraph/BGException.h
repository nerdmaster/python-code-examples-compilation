/*
 *  BGException.h
 *  Copyright 2010 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 25/02/10.
 */

#ifndef BGException_H
#define BGException_H

#include <stdexcept>
#include <string>

namespace BG {

class BGException : public std::runtime_error {
public:
	BGException(const std::string & inMessage) : std::runtime_error(inMessage) {}
};

class CausalityException : public BGException {
public:
	CausalityException(const std::string & inMessage) : BGException(inMessage) {}
};
	
class InvalidCausalityException : public CausalityException {
public:
	InvalidCausalityException(const std::string & inMessage) : CausalityException(inMessage) {}
};	
	
class DifferentialCausalityException : public CausalityException {
public:
	DifferentialCausalityException(const std::string & inMessage) : CausalityException(inMessage) {}
};	
	
}

#endif
