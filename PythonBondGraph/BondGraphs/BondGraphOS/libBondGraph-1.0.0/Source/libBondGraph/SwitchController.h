/*
 *  SwitchController.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 30/04/09.
 */

#ifndef SwitchController_H
#define SwitchController_H

#include <vector>
#include <map>
#include "Switch.h"


namespace BG {

class HybridBondGraph;
	
/*! \brief %Switch controller for a hybrid bond graph
 *  This pure abstract class describes a switch controller for a
 *	hybrid bond graph. A user defined derived class need to be defined
 *	in order to design a working controller.
 *
 *	This controller receive as input, the time and the output variable 
 *	of the bond graph. When those input are received via the method 
 *	updateSwitchState, the switch managed by this controller are updated.
 *
 *	This controller provide a vector of the states of each switch in the
 *	hybrid bond graph.
 */
class SwitchController : public std::vector<Switch*> {
public:
	SwitchController() { mLogger = 0; }
	~SwitchController() { }
	
	void setLogger(std::map<std::string, std::vector<double> > *inLogger) { mLogger = inLogger; }
	
	//! Update the state of the switch managed by this controller.
	virtual void updateSwitchState(double inTime, const std::vector<double>& inInputs, bool inWithInitialParameters=false) = 0;
	
	//! Log writing function.
	virtual void writeLog() = 0;
	
	//! Reset function call when the simulation is reinitialized.
	virtual void reset() = 0;
	
	//! Post connection initialization.
	virtual void initialize() = 0;
		
protected:
	std::map<std::string, std::vector<double> > *mLogger;
};
}
#endif
