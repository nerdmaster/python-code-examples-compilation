/*
 *  Transistor.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 16/10/09.
 */

#ifndef Transistor_H
#define Transistor_H

#include "Switch.h"
#include "SwitchController.h"

namespace BG {
	
/*! \brief Transistor Component
 *  Model the transistor as an ideal switch. 
 */
class Transistor : public Switch, public SwitchController {
	
public:
	Transistor();
	~Transistor() {}
	
	void setId(long int inId);
	
	virtual void reset() {  }
	virtual void updateSwitchState(double inTime, const std::vector<double>& inInputs);
	
	virtual void initialize();
	virtual void writeLog();
	
	void setGateTrigger(Bond* inBond) { mGateBondTrigger = inBond; }
private:
	Bond* mGateBondTrigger;

};
	
}

#endif
