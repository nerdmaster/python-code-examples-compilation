/*
 *  BondGraphDynamic.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 02/04/09.
 */

#include "BondGraphDynamic.h"
#include "BondGraph.h"
#include <cfloat>
#include "stringutil.h"
#include "VectorUtil.h"
#include "HybridBondGraph.h"
#include "BGException.h"

using namespace BG;

BondGraphDynamic::BondGraphDynamic(BondGraph* inBondGraph, std::map<std::string, std::vector<double> > *inLogger) : SystemDynamic(inLogger) { 
	mBondGraph = inBondGraph; 
}

void BondGraphDynamic::saveSimulationState() {
	mSavedTime = mTime;
	mSavedInputs = mInputs;
	mSavedStateVariables = mStateVariables;
	mSavedPreviousTime = mPreviousTime;
	mSavedPreviousInput = mPreviousInput;
	mSavedDuDt = mDuDt;
}
void BondGraphDynamic::restoreSimulationState() {
	mTime = mSavedTime;
	mStateVariables = mSavedStateVariables;
	mPreviousTime = mSavedPreviousTime;
	mPreviousInput = mSavedPreviousInput;
	mDuDt = mSavedDuDt;
	mInputs = mSavedInputs;
}

void BondGraphDynamic::initialize(const std::vector<double> &inValues) { 
	
//	assert(inValues.size() == mBondGraph->getStateVariables().size());
	
	mStateVariables = inValues;
	mInitialValues = mStateVariables;
	mPreviousTime = 0;
	mPreviousInput.resize(0,0);
	mDuDt.resize(0,0);
}

void BondGraphDynamic::writeLog() {
	(*mLogger)["time"].push_back(mTime);
	
	vector<string> lStateNames = mBondGraph->getStateVariableNames();
	vector<string> lSourceNames = mBondGraph->getSourceVariableNames();
	vector<string> lOutputNames = mBondGraph->getOutputVariableNames();
	
	assert(mStateVariables.size() == mBondGraph->getStateVariables().size());
	assert(lStateNames.size() == mStateVariables.size());
	
//	for(unsigned int i = 0; i < mStateVariables.size(); ++i) {
//		(*mLogger)[lStateNames[i]].push_back(mStateVariables[i]);
//	}
	
	for(unsigned int i = 0; i < mInputs.size(); ++i) {
		(*mLogger)[lSourceNames[i]].push_back(mInputs[i]);
	}
	
	vector<double> lOutputVariable = getObservableVariables();
	for(unsigned int i = 0; i < lOutputVariable.size(); ++i) {
		(*mLogger)[string("Output_")+int2str(i)+string("-")+lOutputNames[i]].push_back(lOutputVariable[i]);
	}
}

/*! \brief Reorganize the State Variable Vector
 *  In the event of a change in the number of the state variable, this method should
 *	be called in order to insure continuity of the state variable value. The change
 *	in number of state variable can occur in hybrid bond graph when a storage
 *	component change causality after a new switch configuration.
 */
void BondGraphDynamic::reorganiseStateVariable(const VariableName& inOldNames, const PACC::Matrix& inC, const PACC::Matrix& inD, 
											   const PACC::Matrix& inD2, const PACC::Matrix& inSd) {
	
	const vector<string>& lOldStateVariableName = inOldNames.x;
	const vector<string>& lNewStateVariableName = mBondGraph->getStateVariableNames();
	
	vector<double> lOldStateVariableValues = mStateVariables;
	mStateVariables.resize(lNewStateVariableName.size());

	PACC::Matrix lXd;
	//Compute the corresponding Xd. Computation could be avoided by variable look up.
	if(inSd.getRows() > 0 && inSd.getCols() > 0) {
		PACC::Matrix lOutput = inC*lOldStateVariableValues + inD*mInputs + inD2*mDuDt;
		PACC::Matrix lZd;
		lOutput.extractRows(lZd,inOldNames.x.size(),inOldNames.x.size()+inOldNames.xd.size()-1);
		lXd = inSd.invert()*lZd;
	}
	
	
	for(unsigned int i = 0; i < lNewStateVariableName.size(); ++i) {
		bool lFound = false;
		for(unsigned int j = 0; j < lOldStateVariableName.size(); ++j) {
			if(lNewStateVariableName[i] == lOldStateVariableName[j]) {
				mStateVariables[i] = lOldStateVariableValues[j];
				lFound = true;
				break;
			}
		}
		if(!lFound) {
			assert(lXd.size() == inOldNames.xd.size());
			//New state variable. Need to get the value of the component in derivative causality form.
			for(unsigned int k = 0; k < inOldNames.xd.size(); ++k) {
				if(inOldNames.xd[k] == lNewStateVariableName[i]) {
					mStateVariables[i] = lXd[k];
					lFound = true;
					break;
				}
			}
		}
		//assert(lFound);
		if(!lFound) {
			mBondGraph->plotGraph("bug_statevarible.svg");
			cout << "Old name : " << inOldNames.xd << endl;
			cout << "Old state variable: " << lOldStateVariableName << endl;
			cout << "New state variable: " << lNewStateVariableName << endl;
			
			throw std::runtime_error(string("New state variable not found, looking for ")+lNewStateVariableName[i]);
		}
	}												  
}

#ifdef USE_RKF
void BondGraphDynamic::ode(float t, float *&y, float *&dydt) {
	
	PACC::Matrix lInput(mInputs);
	
	PACC::Matrix lA,lB,lB2;
	
	mBondGraph->getStateMatrix(lA,lB,lB2);
	
	PACC::Matrix lState;
	lState.resize(lA.getRows(),1);
	for(unsigned int i = 0; i < lState.getRows(); ++i) {
		lState(i,0) = y[i];
	}
	
	PACC::Matrix ldydt;
	if(lB2.size() > 0) {
		//Compute du/dt
		if(t != mPreviousTime) {
			double dt = t - mPreviousTime;
			if(dt != 0) 
				mDuDt = (lInput - mPreviousInput)*(1/dt);
			else
				mDuDt.resize(0,0);
			
			mPreviousTime = t;
			mPreviousInput = lInput;
		}	
		
		ldydt = lA*lState + lB*lInput + lB2*mDuDt;
		
	} else {
		ldydt = lA*lState + lB*lInput;		
		
	}
	
	//Format for output
	for(unsigned int i = 0; i < lState.getRows(); ++i) {
		dydt[i] = ldydt(i,0);
	}
}

#else
void BondGraphDynamic::ode(double t, const std::vector<double> & y, std::vector<double> & dydt) {
	PACC::Matrix lInput(mInputs);
	
	//PACC::Matrix lState(y);
	PACC::Matrix lA,lB,lB2;
	
	mBondGraph->getStateMatrix(lA,lB,lB2);

	
	if(lB2.size() > 0) {
		//Compute du/dt
		if(t != mPreviousTime) {
			double dt = t - mPreviousTime;
			if(dt != 0) 
				mDuDt = (lInput - mPreviousInput)*(1/dt);
			else
				mDuDt.resize(0,0);
			
			mPreviousTime = t;
			mPreviousInput = lInput;
		}	
		
		dydt = lA*y + lB*lInput + lB2*mDuDt;

	} else {
		dydt = lA*y + lB*lInput;		
		
	}
}
#endif

const vector<double>& BondGraphDynamic::getObservableVariables() {
	
	PACC::Matrix lState(mStateVariables);

	PACC::Matrix lInput(mInputs);
	PACC::Matrix lC,lD,lD2;
	
	mBondGraph->getOutputMatrix(lC,lD,lD2);
	
	if(lD2.size() > 0) {
		//Compute du/dt
		if(mTime != mPreviousTime) {
			double dt = mTime - mPreviousTime;
			if(dt != 0) 
				mDuDt = (lInput - mPreviousInput)*(1/dt);
			else
				mDuDt.resize(0,0);
			
			mPreviousTime = mTime;
			mPreviousInput = lInput;
		}	
		
		mOutPutVariables = lC*lState + lD*lInput + lD2*mDuDt;
		return(mOutPutVariables);
		
	} else {
		mOutPutVariables = lC*lState + lD*lInput;
		return(mOutPutVariables);		
		
	}
}

