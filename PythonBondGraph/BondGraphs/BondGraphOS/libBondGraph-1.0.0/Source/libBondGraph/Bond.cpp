/*
 *  Bond.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#include "Bond.h"
#include <sstream>
#include <stdexcept>
#include "Passive.h"

using namespace BG;

Bond::Bond(long int inId) : BondGraphElement(eBond) {
	setId(inId);
	mFromPort = 0;
	mToPort = 0;
}

Bond::~Bond() {
	delete mFromPort;
	delete mToPort;
	
	mFromPort = 0;
	mToPort = 0;
}

void Bond::setId(long int inId) {
	mId = inId;
	std::ostringstream lNameStream;
	lNameStream << BONDPREFIX << inId;
	mName = lNameStream.str();
}


void Bond::connect(Port* inFromPort, Port* inToPort) { 
	mFromPort = inFromPort; 
	mFromPort->connect(this);
	
	mToPort = inToPort; 
	mToPort->connect(this);
}

Port* Bond::getOtherEnd(Port* inPort) const {
	if(mFromPort == inPort)
		return mToPort;
	else if(mToPort == inPort)
		return mFromPort;
	else
		return NULL;
}

void Bond::flipPowerStroke() {
	if(mFromPort->isPowerSide()) {
		mFromPort->setPowerStroke(false);
		mToPort->setPowerStroke(true);
	} else {
		mFromPort->setPowerStroke(true);
		mToPort->setPowerStroke(false);
	}
}

void Bond::findGroup() {
	if(mToPort->getComponent()->getComponentGroup() == Component::eU || mFromPort->getComponent()->getComponentGroup() == Component::eU) {
		mGroup = eU;
		
		Port* lComponentPort = 0;
		if(mToPort->getComponent()->getComponentGroup() == Component::eU) {
			lComponentPort = mToPort;
		} else {
			lComponentPort = mFromPort;
		}
		
		if(lComponentPort->getCausality() == eEffortCausal) {
			mEffortAsInput = false;
		} else {
			mEffortAsInput = true;
		}
		
	}
	else if(mToPort->getComponent()->getComponentGroup() == Component::eR || mFromPort->getComponent()->getComponentGroup() == Component::eR) {
		mGroup = eR;
		
		Port* lComponentPort = 0;
		if(mToPort->getComponent()->getComponentGroup() == Component::eR) {
			lComponentPort = mToPort;
		} else {
			lComponentPort = mFromPort;
		}
		
		if(lComponentPort->getCausality() == eEffortCausal) {
			mEffortAsInput = false;
		} else {
			mEffortAsInput = true;
		}
	}
	else if(mToPort->getComponent()->getComponentGroup() == Component::eJ && mFromPort->getComponent()->getComponentGroup() == Component::eJ) {
		mGroup = eJ;
	}
	else if(mToPort->getComponent()->getComponentGroup() == Component::eS || mFromPort->getComponent()->getComponentGroup() == Component::eS) {
		//Find the if the causality is an integral or derivative one		
		Passive* lPassive = 0;
		Port* lComponentPort = 0;
		if(mToPort->getComponent()->getComponentGroup() == Component::eS) {
			lPassive = dynamic_cast<Passive*> (mToPort->getComponent());
			lComponentPort = mToPort;
		} else {
			lPassive = dynamic_cast<Passive*> (mFromPort->getComponent());
			lComponentPort = mFromPort;
		}
		
		if(lPassive->isIntegralCausality()) {
			mGroup = eS;
		} else {
			mGroup = eSd;	
		}
		
		if(lComponentPort->getCausality() == eEffortCausal) {
			mEffortAsInput = false;
		} else {
			mEffortAsInput = true;
		}
		
		if(lPassive->getType() == Passive::eConductor || lPassive->getType() == Passive::eConductorM)
			mStorageType = eC;
		else
			mStorageType = eI;
		
	}	
}


void Bond::setMatrixIndex(int inIndex, int inOffset, int inEffort, int inFlow, int inJOffset) { 
	mMatrixIndex.index = inIndex; 
	mMatrixIndex.offset = inOffset; 
	mMatrixIndex.joffset = inJOffset;
	mMatrixIndex.effort = inEffort;
	mMatrixIndex.flow = inFlow;
	mMatrixIndex.reffort = -1;
	mMatrixIndex.rflow = -1;	
}

void Bond::write(PACC::XML::Streamer& inStream, bool inIndent) const {
	inStream.openTag("Bond", inIndent);
	inStream.insertAttribute("id", mId);
	inStream.insertAttribute("name", mName);
	inStream.insertAttribute("from", mFromPort->getComponent()->getId());
	inStream.insertAttribute("to", mToPort->getComponent()->getId());
	inStream.insertAttribute("causality",mToPort->getCausality());
	inStream.closeTag();
}
