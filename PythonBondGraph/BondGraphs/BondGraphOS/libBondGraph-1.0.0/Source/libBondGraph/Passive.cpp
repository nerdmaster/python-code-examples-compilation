/*
 *  Passive.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#include "Passive.h"
#include <stdexcept>
#include "BGException.h"

using namespace BG;

Passive::Passive(PassiveType inType, double inValue, long int inId) 
: mType(inType), Component(ePassive,inValue) {
	setId(inId);
	setComponentGroup();
}

void Passive::setType(PassiveType inType) {
	mType = inType; 
	setId(mId);
	setComponentGroup();
}

void Passive::setComponentGroup() {
	switch(mType) {
		case eResistor:
		case eResistorM:
			mComponentGroup = eR;
			break;
		case eConductor:
		case eConductorM:
			mComponentGroup = eR;
			break;
		case eCapacitor:
		case eCapacitorM:
			mComponentGroup = eS;
			break;
		case eInductor:
		case eInductorM:
			mComponentGroup = eS;
			break;
		case eTransformer:
		case eTransformerM:
			mComponentGroup = eJ;
			break;
		case eGyrator:
		case eGyratorM:
			mComponentGroup = eJ;
			break;
		default:
			throw BGException("Passive::Passive() : Unknown type of passive element");
	}
}

bool Passive::isIntegralCausality() const {
	switch(mType) {
		case eCapacitor:
		case eCapacitorM:
			if(mPorts[0]->getCausality() == eEffortCausal)
				return false;
			else
				return true;
			break;
		case eInductor:
		case eInductorM:
			if(mPorts[0]->getCausality() == eEffortCausal)
				return true;
			else
				return false;
			break;
		default:
			return false;
			break;
	}
}

void Passive::setId(long int inId) {
	mId = inId;
	std::ostringstream lNameStream;
	switch(mType) {
		case eResistor:
		case eResistorM:
			lNameStream << PASSIVERESISTORPREFIX << inId;
			break;
		case eConductor:
		case eConductorM:
			lNameStream << PASSIVECONDUCTORPREFIX << inId;
			break;
		case eCapacitor:
		case eCapacitorM:
			lNameStream << PASSIVECAPACITORPREFIX << inId;
			break;
		case eInductor:
		case eInductorM:
			lNameStream << PASSIVEINDUCTORPREFIX << inId;
			break;
		case eTransformer:
		case eTransformerM:
			lNameStream << PASSIVETRANSFORMERPREFIX << inId;
			break;
		case eGyrator:
		case eGyratorM:
			lNameStream << PASSIVEGYRATORPREFIX << inId;
			break;
		default:
			throw BGException("Passive::Passive() : Unknown type of passive element");
	}
	mName = lNameStream.str();
}

/*! \brief Apply the causality constrains of the component.
 *  This methods will apply the causality constrains of the component to the other
 *	ports. This is for tranformer and gyrator only.
 *
 *	For transformer, the two ports should have different causality.
 *
 *	For gyrator, the two ports should have the same causality.
 *  \param  inPort Newly assign causality port.
 */
void Passive::applyCausalityConstrain(Port* inPort) {
	if(mType == eTransformer || mType == eTransformerM) {
		//Need to have different causality
		for(unsigned int i = 0; i < mPorts.size(); ++i) {
			if(mPorts[i] != inPort) {
				mPorts[i]->setCausality( (inPort->getCausality()==eEffortCausal)?eFlowCausal:eEffortCausal );
			}
		}
	} else if(mType == eGyrator || mType == eGyratorM) {
		//Need to have different causality
		for(unsigned int i = 0; i < mPorts.size(); ++i) {
			if(mPorts[i] != inPort) {
				mPorts[i]->setCausality( (inPort->getCausality()==eEffortCausal)?eEffortCausal:eFlowCausal );
			}
		}
	}
}

void Passive::write(PACC::XML::Streamer& inStream, bool inIndent) const {
	inStream.openTag("Component",inIndent);
	inStream.insertAttribute("type", "Passive");
	inStream.insertAttribute("id", mId);
	inStream.insertAttribute("name", mName);
	inStream.insertAttribute("subtype", mType);
	inStream.insertAttribute("value", mValue);
	inStream.closeTag();
}

