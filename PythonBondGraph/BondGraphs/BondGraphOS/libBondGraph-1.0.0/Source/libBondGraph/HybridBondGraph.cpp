/*
 *  HybridBondGraph.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 02/04/09.
 */

#include "HybridBondGraph.h"
#include <cmath>
#include <assert.h>
#include "BGException.h"
#include "stringutil.h"
#include "VectorUtil.h"
#include "SwitchME.h"

#include <sstream>
#include <PACC/XML.hpp>

using namespace BG;

HybridBondGraph::HybridBondGraph(/*SwitchController *inController*/) : mSwitchIdCounter(1) { 
//	mControllers = inController; 
//	mControllers->setLogger(&mSimulationLog);
	mState = 0;
}
	
HybridBondGraph::~HybridBondGraph() {
	for(unsigned int i = 0; i < mControllers.size(); ++i) {
		if(mControllers[i] != 0) {
			delete mControllers[i];
			mControllers[i] = 0;
		}
	}
}

HybridBondGraph::HybridBondGraph(const HybridBondGraph& inBondGraph) {
	*this = inBondGraph;
}

HybridBondGraph& HybridBondGraph::operator=(const HybridBondGraph& inBondGraph) {
	//throw runtime_error("HybridBondGraph::operator= not defined!");
	
	//dirty way by lazyness
	std::ostringstream lStream;
	PACC::XML::Streamer lStreamer(lStream);
	inBondGraph.write(lStreamer);
	
	std::istringstream lStreami(lStream.str());
	PACC::XML::Document lXMLParser;
	lXMLParser.parse(lStreami);
	PACC::XML::ConstIterator lRootNode = lXMLParser.getFirstRoot(); 
	this->read(lRootNode);
	
	return *this;
}

void HybridBondGraph::clearStateMatrix() {
	
	mOutputMatrixCurrent = mOutputMatrix[mState];
	mNamesCurrent = mNames[mState];                
	
	mStateMatrix.clear();
	mOutputMatrix.clear();
	mNames.clear();
	mCounts.clear();
}


void HybridBondGraph::reset() {
	//Set the switch to the initial state
	assert(mInitialState.size() == mSwitches.size());
	for(unsigned int i = 0; i < mInitialState.size(); ++i) {
		mSwitches[i]->setState(mInitialState[i]);
	}
	updateStateMatrix();
	mDynamics.reset();
	for(unsigned int i = 0; i < mControllers.size(); ++i)
		mControllers[i]->reset();
}


void HybridBondGraph::setInitialSwitchState(const std::vector<bool> &inSwitchesValue) {
	mInitialState = inSwitchesValue;
	assert(mInitialState.size() == mSwitches.size());
	for(unsigned int i = 0; i < mInitialState.size(); ++i) {
		mSwitches[i]->setState(mInitialState[i]);
	}
	updateStateMatrix(true);
	mDynamics.setInput(getSourceValues(0));
	mDynamics.setStateVariables(std::vector<double>(mScount,0));
}

void HybridBondGraph::setInitialStateVariable(const std::vector<double> &inStateValues) {
	if(mScount != inStateValues.size()) {
		throw BGException("The number of initial values must be equal to the number of states in the system.");
	}
	mDynamics.initialize(inStateValues);
}

/*! \brief Set the initial states of the switches and the state variables.
 *  \param  inSwitchesValue Initial state of the switches.
 *  \param  inStateValues Initial state variables values.
 */
void HybridBondGraph::setInitialState(const std::vector<bool> &inSwitchesValue, const std::vector<double> &inStateValues) {
	setInitialSwitchState(inSwitchesValue);
	setInitialStateVariable(inStateValues);
}

void HybridBondGraph::getSwitchState(std::vector<bool>& outStates) const {
	outStates.resize(mSwitches.size());
	for(unsigned int i = 0; i < mSwitches.size(); ++i) {
		outStates[i] = mSwitches[i]->getState();
	}
}

void HybridBondGraph::addSwitch(Switch *inSwitch)  {
	//Add the switch to the regular component vector in order
	//to use the switches as normal source component for the
	//computation of the state equation.
	if(inSwitch->getId() < 0) {
		inSwitch->setId(mElementIdCounter++);
	}
	if( mComponents.size() < inSwitch->getId() ) {
		mComponents.resize(inSwitch->getId());
		if(mElementIdCounter <= mComponents.size()) 
			mElementIdCounter = mComponents.size()+1;
	}
	
	mComponents[inSwitch->getId()-1] = inSwitch;
	
	//Add the switch to the vector of switches in the hybrid
	//bond graph for quick reference to them.
	if(inSwitch->getSwitchId() < 0) {
		inSwitch->setSwitchId(mSwitchIdCounter++);
	}
	if( mSwitches.size() < inSwitch->getSwitchId() )
		mSwitches.resize(inSwitch->getSwitchId(),0);
	mSwitches[inSwitch->getSwitchId()-1] = inSwitch;
}

void HybridBondGraph::addSwitchController(SwitchController *inController) { 
	inController->setLogger(&mSimulationLog);
	mControllers.push_back(inController);	
}

/*! Set the state of the switches
 *  A state that is true means that flow is zero, false means that effort is zero.
 */
void HybridBondGraph::updateStateMatrix(bool inIsInitialization) {
	//Identify the new state
	unsigned int lNewState = 0;
	for(int i = 0; i < mSwitches.size(); ++i) {
		lNewState += static_cast<unsigned int>(int(mSwitches[i]->getState()) * pow(2.0,i));
	}
	
	int lPreviousNbStateVariable = getStateVariables().size();
	
	if( (mState != lNewState) || inIsInitialization || mStateMatrix.empty()) {
		unsigned int lOldState = mState;
		mState = lNewState;
		
		//Set the new state matrix according to the state of the switch
		map<unsigned int, vector<PACC::Matrix> >::iterator lIter = mStateMatrix.find(lNewState);
		if( lIter != mStateMatrix.end() ) {
			//Retrieve the already computed matrix
			mA = mStateMatrix[lNewState][0];
			mB = mStateMatrix[lNewState][1];
			mB2 = mStateMatrix[lNewState][2];
			
			mCr = mOutputMatrix[lNewState][0];
			mDr = mOutputMatrix[lNewState][1];
			mD2r = mOutputMatrix[lNewState][2];
			
			mC = mOutputMatrix[lNewState][3];
			mD = mOutputMatrix[lNewState][4];
			mD2 = mOutputMatrix[lNewState][5];
			//mSd = mOutputMatrix[lNewState][6]; //Don't need to recopy it.
			
			//Update relevent informations
			mUcount = mCounts[lNewState].u;
			mScount = mCounts[lNewState].s;
			mSDcount = mCounts[lNewState].sd;
			mRcount = mCounts[lNewState].r;
			mJcount = mCounts[lNewState].j;			
			
			mNameX = mNames[lNewState].x;
			mNameXd = mNames[lNewState].xd;
			mNameU = mNames[lNewState].u;
			mOutputName = mNames[lNewState].out;
			
			//If the number of state variable changed, a transition should be made
			if(mScount != lPreviousNbStateVariable) {
				if (mNames.find(lOldState) == mNames.end()) {
					//State matrix as been cleared
					mDynamics.reorganiseStateVariable(mNamesCurrent, mOutputMatrixCurrent[3], 
													  mOutputMatrixCurrent[4], mOutputMatrixCurrent[5], mOutputMatrixCurrent[6]);
				} else {
					mDynamics.reorganiseStateVariable(mNames[lOldState], mOutputMatrix[lOldState][3], 
													  mOutputMatrix[lOldState][4], mOutputMatrix[lOldState][5], mOutputMatrix[lOldState][6]);
				}
			}
			
		} else {
			//Compute the new state matrix
			clearCausality();
			assignCausality();
			
			computeStateEquation();
			
	
#ifdef DEBUG_STATEEQN
#ifndef WITHOUT_GRAPHVIZ
			ostringstream lFileNameStream;
			if(hasDeferentialCausality() && !mAllowDifferentialCausality)
				lFileNameStream << "hybridbondgraph_d_" << mState << ".svg";
			else
				lFileNameStream << "hybridbondgraph_" << mState << ".svg";
			plotGraph(lFileNameStream.str());
#endif
			
			PACC::XML::Streamer lStream(cout);
			cout << "State matrix for state " << lNewState << endl;
			cout << "State variables : " << getStateVariableNames() << endl;
			mA.write(lStream); cout << endl;
			mB.write(lStream); cout << endl;
			if(mSDcount != 0)
				mB2.write(lStream); cout << endl;
			
			mC.write(lStream); cout << endl;
			mD.write(lStream); cout << endl;
			if(mSDcount != 0)
				mD2.write(lStream); cout << endl;
			
			mCr.write(lStream); cout << endl;
			mDr.write(lStream); cout << endl;
			if(mSDcount != 0)
				mD2r.write(lStream); cout << endl;
#endif
			
			if(hasDeferentialCausality() && !mAllowDifferentialCausality)
				throw DifferentialCausalityException(string("The bond graph as differential causality for state ")+int2str(lNewState));
			
			//Save the current matrix if not done already
			mStateMatrix[lNewState].resize(3);
			mStateMatrix[lNewState][0] = mA;
			mStateMatrix[lNewState][1] = mB;
			mStateMatrix[lNewState][2] = mB2;
			
			mOutputMatrix[lNewState].resize(7);
			mOutputMatrix[lNewState][0] = mCr;
			mOutputMatrix[lNewState][1] = mDr;
			mOutputMatrix[lNewState][2] = mD2r;
			
			mOutputMatrix[lNewState][3] = mC;
			mOutputMatrix[lNewState][4] = mD;
			mOutputMatrix[lNewState][5] = mD2;
			mOutputMatrix[lNewState][6] = mSd;
			
			mCounts[lNewState].u = mUcount;
			mCounts[lNewState].s = mScount;
			mCounts[lNewState].sd = mSDcount;
			mCounts[lNewState].r = mRcount;
			mCounts[lNewState].j = mJcount;
			
			mNames[lNewState].x = mNameX;
			mNames[lNewState].xd = mNameXd;
			mNames[lNewState].u = mNameU;
			mNames[lNewState].out = mOutputName;
			
			if(!inIsInitialization) {
				//If the number of state variable changed, a transition should be made
				if(mScount != lPreviousNbStateVariable) {
					if (mNames.find(lOldState) == mNames.end()) {
						//State matrix as been cleared
						mDynamics.reorganiseStateVariable(mNamesCurrent, mOutputMatrixCurrent[3], 
														  mOutputMatrixCurrent[4], mOutputMatrixCurrent[5], mOutputMatrixCurrent[6]);
					} else {
						mDynamics.reorganiseStateVariable(mNames[lOldState], mOutputMatrix[lOldState][3], 
														  mOutputMatrix[lOldState][4], mOutputMatrix[lOldState][5], mOutputMatrix[lOldState][6]);
					}
				}
			}
		}
	}
}


/*! \brief Simulate the hybrid bond graph.
 *  This method runs a simulation of the present hybrid bond graph. 
 *	Before starting the simulation, the system is reinitialized with
 *	the previously provided initial values for states and switches.
 *	
 *	The bond graph state equation are evaluated using Runge-Kutta with
 *	a time step ten times smaller than the one provided. The time step
 *	provided specify in fact the interval of exchange of information between
 *	the state equation of the bond graph and the controller input and output
 *	generation and the log writing.
 *  \param  inStopTime Simulation duration in seconds.
 *  \param  inSimulationStep Simulation time step increment. 
 *	\param	inReset Reset the simulation upon starting.
 *	\param	inWithInitialParameters The matrix safed as known parameter will used instead of the real one for updating the switches.
 */
void HybridBondGraph::simulate(double inStopTime, double inSimulationStep, bool inReset, bool inWithInitialParameters) {
	//Initialize
	if(inReset) reset();
	
	mDynamics.setTimeStep(inSimulationStep/10);
		
	while(mDynamics.getTime() < inStopTime) {
		//Update state matrices according to the switch states.
		updateStateMatrix();
		
		//Get source value.
		mDynamics.setInput( getSourceValues(mDynamics.getTime()) );
		
		//Simulate the system with the source value
		mDynamics.simulate(mDynamics.getTime() + inSimulationStep);
		
		//Set the input to the controller.
		for(unsigned int i = 0; i < mControllers.size(); ++i) {
			mControllers[i]->updateSwitchState(mDynamics.getTime(), mDynamics.getObservableVariables(),inWithInitialParameters);
			mControllers[i]->writeLog();
		}
		mDynamics.writeLog();
		
	}
}

void HybridBondGraph::saveAsParameterMatrix() {
	mInitialStateMatrix = mStateMatrix;
	mInitialOutputMatrix = mOutputMatrix;
	mInitialNames = mNames;
	mInitialCounts = mCounts;
}

void HybridBondGraph::simulateVirtualFixParameters(const std::vector<bool>& inSwitchState, double inStep, std::vector<double>& outResults, std::vector<double>& outStates) {
	//Restore the state matrix to the saved initial parameters
	std::map<unsigned int, std::vector<PACC::Matrix> > lCurrentStateMatrix = mStateMatrix;
	std::map<unsigned int, std::vector<PACC::Matrix> > lCurrentOutputMatrix = mOutputMatrix;
	std::map<unsigned int, VariableName > lCurrentNames = mNames;         
	std::map<unsigned int, VariableCount > lCurrentCounts = mCounts;   
	
	mStateMatrix = mInitialStateMatrix;
	mOutputMatrix = mInitialOutputMatrix;
	mNames = mInitialNames;
	mCounts = mInitialCounts;
	
	simulateVirtual(getStateVariables(),inSwitchState,inStep,outResults,outStates);
	
	mStateMatrix = lCurrentStateMatrix;
	mOutputMatrix = lCurrentOutputMatrix;
	mNames = lCurrentNames;
	mCounts = lCurrentCounts;
	
}

void HybridBondGraph::simulateVirtual(const std::vector<bool>& inSwitchState, double inStep, std::vector<double>& outResults, std::vector<double>& outStates) {
	simulateVirtual(getStateVariables(),inSwitchState,inStep,outResults,outStates);
}

void HybridBondGraph::simulateVirtual(const std::vector<double>& inState, const std::vector<bool>& inSwitchState, double inStep, std::vector<double>& outResults, std::vector<double>& outStates) {
		
	//Get current state of the simulation
	vector<bool> lCurrentSwitchState;
	getSwitchState(lCurrentSwitchState);
	
	try{
		mDynamics.saveSimulationState();
		
		//Assign new state
		mDynamics.initialize(inState);
		
		//Get source value.
		mDynamics.setInput( getSourceValues(mDynamics.getTime()) );
		
		//Simulate
		assert(inSwitchState.size() == mSwitches.size());
		for(unsigned int i = 0; i < mSwitches.size(); ++i) {
			mSwitches[i]->setState(inSwitchState[i]);
		}
		
		updateStateMatrix();
		
	#ifdef DEBUG_CONTROLLER
		cout << "Current state variable: " << mDynamics.getStateVariables() << endl;
		cout << "Current output: " << mDynamics.getObservableVariables() << endl;
	#endif
		
	//#ifdef DEBUG_CONTROLLER
	//	PACC::XML::Streamer lStream(cout);
	//	cout << "Virtual simulation of switch states " << inSwitchState << " and using state matrix: " << endl;
	//	mA.write(lStream); cout << endl;
	//	mB.write(lStream); cout << endl;
	//	if(mSDcount != 0)
	//		mB2.write(lStream); cout << endl;
	//	
	//	mC.write(lStream); cout << endl;
	//	mD.write(lStream); cout << endl;
	//	if(mSDcount != 0)
	//		mD2.write(lStream); cout << endl;
	//	
	//	mCr.write(lStream); cout << endl;
	//	mDr.write(lStream); cout << endl;
	//	if(mSDcount != 0)
	//		mD2r.write(lStream); cout << endl;
	//#endif
		
		mDynamics.simulate(mDynamics.getTime() + inStep);
		
	#ifdef DEBUG_CONTROLLER
		cout << "Switch state: " << inSwitchState << endl;
		cout << "Expected state variable: " << mDynamics.getStateVariables() << endl;
		cout << "Expected output: " << mDynamics.getObservableVariables() << endl;
	#endif
		
		outResults = mDynamics.getObservableVariables();
		outStates = getStateVariables();
		
		//Restore old state of the simulation
		for(unsigned int i = 0; i < lCurrentSwitchState.size(); ++i) {
			mSwitches[i]->setState(lCurrentSwitchState[i]);
		}

		try{
			updateStateMatrix();
		} catch(BG::CausalityException inError) {
			//Ignore the causality error as the bond graph is set back to the previous state no matter what
		}
		
		mDynamics.restoreSimulationState();
		
	} catch(BG::CausalityException &inError) {
		//In case of exception, restore the simulation state
		for(unsigned int i = 0; i < lCurrentSwitchState.size(); ++i) {
			mSwitches[i]->setState(lCurrentSwitchState[i]);
		}
		try{
			updateStateMatrix();
		} catch(BG::CausalityException inError) {
			//Ignore the causality error as the bond graph is set back to the previous state no matter what
		}
		mDynamics.restoreSimulationState();
		
		throw inError;
	}
}



void HybridBondGraph::postConnectionInitialization() {
	for(unsigned int k = 0; k < mControllers.size(); ++k) {
		mControllers[k]->initialize();
		//Add the related switches.
		for(unsigned int i = 0; i < mControllers[k]->size(); ++i) {
			//Check if the switch was previously added
			vector<Switch*>::iterator lIter = find(mSwitches.begin(), mSwitches.end(), (*mControllers[k])[i]);
			if(lIter == mSwitches.end())
				addSwitch((*mControllers[k])[i]);		
		}
	}
}


/*! \brief Get the Bond Graph Source
 *  This method return the sources in the hybrid bond graph without including the switches,
 *	as are considered as sources in a bond graph.
 *  \return Bond graph sources.
 */
vector<Source*>& HybridBondGraph::getSources() {
	mPureSources.clear();
	for(unsigned int i = 0; i < mComponents.size(); ++i) {
		if(mComponents[i] != 0) {
			if( mComponents[i]->getComponentGroup()  == Component::eU) {
				if(find(mSwitches.begin(), mSwitches.end(), mComponents[i]) == mSwitches.end() ) {
					mPureSources.push_back(dynamic_cast<Source*>(mComponents[i]));
				}
			}
		}
	}
	return mPureSources;
}

void HybridBondGraph::read(PACC::XML::ConstIterator inNode)
{
	//Remove all previous components and bonds
	for(unsigned int i = 0; i < mBonds.size(); ++i) {
		if(mBonds[i] != 0) {
			delete mBonds[i];
			mBonds[i] = 0;
		}
	}
	for(unsigned int i = 0; i < mComponents.size(); ++i) {
		if(mComponents[i] != 0) {
			delete mComponents[i];
			mComponents[i] = 0;
		}
	}
	mComponents.resize(0);
	mBonds.resize(0);
	mEffortOutputBonds.resize(0);
	mFlowOutputBonds.resize(0);
	
	if((inNode->getType() != PACC::XML::eData) || (inNode->getValue() != "BondGraph"))
		throw BGException("tag <BondGraph> expected!");
	
	//Get output Id
	string lEffortOut;
	string lFlowOut;
	if(inNode->isDefined("eout"))
		lEffortOut = inNode->getAttribute("eout");
	if(inNode->isDefined("fout"))
		lFlowOut = inNode->getAttribute("fout");
	
	
	//Go through all component
	for(PACC::XML::ConstIterator lChild=inNode->getFirstChild(); lChild; lChild=lChild->getNextSibling()) {
		if(lChild->getValue() == "Component") {
			int lSubType = -1; 
			if(lChild->isDefined("subtype"))
				lSubType = atoi(lChild->getAttribute("subtype").c_str());
			
			int lId = atoi(lChild->getAttribute("id").c_str());
			
			if(lChild->getAttribute("type") == "Junction") {
				addComponent(new Junction(Junction::JunctionType(lSubType),lId));
			}
			else if(lChild->getAttribute("type") == "Source"){
				addComponent(new Source(Source::SourceType(lSubType),atof(lChild->getAttribute("value").c_str()),lId));  
			}
			else if(lChild->getAttribute("type") == "Passive"){
				addComponent(new Passive(Passive::PassiveType(lSubType),atof(lChild->getAttribute("value").c_str()),lId));  
			}
			else if(lChild->getAttribute("type") == "Switch"){
				Switch* lSwitch = new Switch(false,lId);
				int lSwId = atoi(lChild->getAttribute("swid").c_str());
				lSwitch->setSwitchId(lSwId);
				if(lSwId >= 0) {
					if(!mControllers.empty())
						if(mControllers[0] != 0)
							mControllers[0]->push_back(lSwitch);
					addSwitch(lSwitch);  
				} else {
					addComponent(lSwitch);
				}
			}
			else if(lChild->getAttribute("type") == "SwitchME"){
				int lSwId =  atoi(lChild->getAttribute("swid").c_str());
				int lSwId2 = atoi(lChild->getAttribute("swid2").c_str());
				Switch* lDependentSwitch = dynamic_cast<Switch*>(mComponents[lSwId2-1]);
				SwitchME* lSwitch = new SwitchME(lDependentSwitch,false,lId);
				lSwitch->setSwitchId(lSwId);
				addSwitch(lSwitch);  
			}			
			else {
				throw BGException("Unknown component type!");
			}
		}
	}
	mElementIdCounter = mComponents.size()+1;
	//Add the bond
	for(PACC::XML::ConstIterator lChild=inNode->getFirstChild(); lChild; lChild=lChild->getNextSibling()) {
		if(lChild->getValue() == "Bond") {
			int lFrom = atoi(lChild->getAttribute("from").c_str());
			int lTo = atoi(lChild->getAttribute("to").c_str());
			Causality lCausality = Causality(atoi(lChild->getAttribute("causality").c_str()));
			int lId = atoi(lChild->getAttribute("id").c_str());
			connect(mComponents[lFrom-1],mComponents[lTo-1], lCausality, lId);
		}
	}
	//Adjust bond counter
	mBondIdCounter = mBonds.size()+1;
	
	//Assign output bonds
	std::istringstream lISS(lEffortOut);
	if(!lEffortOut.empty()) {
		vector<unsigned int> lEffortOutputId;
		lISS >> lEffortOutputId;
		for(unsigned int i = 0; i < lEffortOutputId.size(); ++i) {
			assert(mBonds[lEffortOutputId[i]-1]->getId() == lEffortOutputId[i]);
			Bond* lBond = mBonds[lEffortOutputId[i]-1];
			mEffortOutputBonds.push_back(lBond);
		}
	}
	
	if(!lFlowOut.empty()) {
		lISS.clear();
		lISS.str(lFlowOut);
		vector<unsigned int> lFlowOutputId;
		lISS >> lFlowOutputId;
		for(unsigned int i = 0; i < lFlowOutputId.size(); ++i) {
			assert(mBonds[lFlowOutputId[i]-1]->getId() == lFlowOutputId[i]);
			mFlowOutputBonds.push_back(mBonds[lFlowOutputId[i]-1]);
		}
	}
}

void HybridBondGraph::simplify() {
	do {
		BondGraph::simplify();
	} while(simplifyRemoveRedundantSwitch());	
}

bool HybridBondGraph::simplifyRemoveRedundantSwitch() {
	//Simplify redundant component at the same junction
	bool lMultipleSwitchFound = false;
	for(unsigned int i = 0; i < mComponents.size(); ++i) {
		if(mComponents[i] != 0) {
			
			Component* lJctComponent = mComponents[i];
			if( lJctComponent->getElementType() == BondGraphElement::eJunction ) {
				//Junction* lJunction = dynamic_cast<Junction*>(lJctComponent);
				
				//Check if a source is connected to that junction
				bool lHasSource = false;
				if(mPureSources.empty()) getSources();
				for(vector<Source*>::iterator lSourceIter = mPureSources.begin(); lSourceIter != mPureSources.end();++lSourceIter) {
					if((*lSourceIter) != 0) {
						Bond* lBond = (*lSourceIter)->getPorts(0)->getBond();
						Component* lComponent = lBond->getOtherEnd((*lSourceIter)->getPorts(0))->getComponent();
						if( lComponent == lJctComponent ) {
							lHasSource = true;
						}
					}
				}
		
				//Keep only one switch per junction, remove all switch if a source is present
				bool lMultipleSwitch = false;
				for(vector<Switch*>::iterator lSwitchIter = mSwitches.begin(); lSwitchIter != mSwitches.end();) {
					if((*lSwitchIter) != 0) {
						Bond* lBond = (*lSwitchIter)->getPorts(0)->getBond();
						Component* lComponent = lBond->getOtherEnd((*lSwitchIter)->getPorts(0))->getComponent();
						if( lComponent == lJctComponent ) {
							if(lMultipleSwitch || lHasSource) {
		
								BondGraph::removeComponent(*lSwitchIter); //Don't delete the switch twice, use bondgraph removecomponent
								
								for(unsigned int j = 0; j < mControllers.size(); ++j) {
									vector<Switch*>::iterator lIter = find(mControllers[j]->begin(),mControllers[j]->end(),*lSwitchIter);
									if(lIter != mControllers[j]->end())
										mControllers[j]->erase(lIter);
								}
								lSwitchIter = mSwitches.erase(lSwitchIter++);
								lMultipleSwitchFound = true;
							} else {
								lMultipleSwitch = true;
								++lSwitchIter;
							}
						} else {
							++lSwitchIter;
						}
					} else {
						++lSwitchIter;
					}
				}
			}
		}
	}
	return lMultipleSwitchFound;
}

void HybridBondGraph::removeComponent(Component* inComponent) {
	vector<Component*>::iterator lComponentIter = find(mComponents.begin(), mComponents.end(), inComponent);
	assert(lComponentIter != mComponents.end());
	
	for(unsigned int i = 0; i < inComponent->getPorts().size(); ++i) {
		Port* lPort = inComponent->getPorts()[i];
		Bond* lBond = lPort->getBond();
		//Disconnect the attached component
		Port* lOldPort = lBond->getOtherEnd(lPort);
		lOldPort->getComponent()->disconnect(lOldPort);
		
		
		//Check if the bond is part of the output
		vector<Bond*>::iterator lBondIter = find(mEffortOutputBonds.begin(),mEffortOutputBonds.end(),lBond);
		if( lBondIter != mEffortOutputBonds.end() ) {
			*lBondIter = 0;
			mEffortOutputBonds.erase(lBondIter);
		}
		
		lBondIter = find(mFlowOutputBonds.begin(),mFlowOutputBonds.end(),lBond);
		if( lBondIter != mFlowOutputBonds.end() ) {
			*lBondIter = 0;
			mFlowOutputBonds.erase(lBondIter);
		}
		
		//Delete bond
		lBondIter = find(mBonds.begin(),mBonds.end(),lBond);
		assert(lBondIter != mBonds.end());
		*lBondIter = 0;
		delete lBond;
		lBond = 0;
	}
	//Once all the bond are deleted, delete the component

	if( typeid(*inComponent).name() == typeid(Switch).name() ) {
		Switch* lSwitch = dynamic_cast<Switch*>(inComponent);
		//Remove the switch from the switch vector
		vector<Switch*>::iterator lIter = find(mSwitches.begin(),mSwitches.end(),lSwitch);
		if(lIter != mSwitches.end()) {
			mSwitches.erase(lIter);
		}
		//Remove the switch from the controllers
		for(unsigned int j = 0; j < mControllers.size(); ++j) {
			lIter = find(mControllers[j]->begin(),mControllers[j]->end(),lSwitch);
			if(lIter != mControllers[j]->end())
				mControllers[j]->erase(lIter);
		}
		
		//Reorganize switch ID
		for(vector<Switch*>::iterator lIter = mSwitches.begin(); lIter != mSwitches.end();) {
			if(*lIter == 0) {
				lIter = mSwitches.erase(lIter++);
			} else {
				lIter++;
			}
		}
		for(unsigned int i = 0; i < mSwitches.size(); ++i) {
			mSwitches[i]->setSwitchId(i+1);
		}
		mSwitchIdCounter = mSwitches.size()+1;

	}
	
	delete inComponent;
	*lComponentIter = 0;
	inComponent = 0;
}

void HybridBondGraph::replaceComponent(Component*& inOldComponent, Component*& inNewComponent) {
	vector<Component*>::iterator lComponentIter = find(mComponents.begin(), mComponents.end(), inOldComponent);
	assert(lComponentIter != mComponents.end());
	assert(*lComponentIter != 0);
	
	//Avoid replacing a switch for a new switch
	if( typeid(*inOldComponent).name() == typeid(Switch).name() && typeid(*inNewComponent).name() == typeid(Switch).name() ) {
		Component* lTempComponent = inNewComponent;
		inNewComponent = inOldComponent;
		delete lTempComponent;
		lTempComponent = 0;
		return;
	}
	
	//Connect the new component to the same ports
	std::vector<Port*> lPorts = inOldComponent->getPorts();
	for(unsigned int i = 0; i < lPorts.size(); ++i) {
		inNewComponent->connect(lPorts[i]);
	}
	
	//Delete the OldComponent
	bool lReplacingSwitch = false;
	if( typeid(*inOldComponent).name() == typeid(Switch).name() ) {
		lReplacingSwitch = true;
		Switch* lSwitch = dynamic_cast<Switch*>(inOldComponent);
		
		//Remove the switch from the switch vector
		vector<Switch*>::iterator lIter = find(mSwitches.begin(),mSwitches.end(),lSwitch);
		if(lIter != mSwitches.end()) {
			mSwitches.erase(lIter);
		}
		//Remove the switch from the controllers
		for(unsigned int j = 0; j < mControllers.size(); ++j) {
			lIter = find(mControllers[j]->begin(),mControllers[j]->end(),lSwitch);
			if(lIter != mControllers[j]->end())
				mControllers[j]->erase(lIter);
		}
	}

	int lOldId = inOldComponent->getId();

	delete inOldComponent;
	*lComponentIter = 0;
	inOldComponent = 0;
	
	//Add the component
	inNewComponent->setId(lOldId);
	//If the new component is a switch, the switch need to be added
	if( typeid(*inNewComponent).name() == typeid(Switch).name() ) {
		addSwitch(dynamic_cast<Switch*>(inNewComponent));
		
		SwitchController* lController = this->getControllers()[0];
		lController->push_back(dynamic_cast<Switch*>(inNewComponent));
	} else {
		addComponent(inNewComponent);
	}
	
	if( lReplacingSwitch ) {
		//Reorganize switch ID
		for(vector<Switch*>::iterator lIter = mSwitches.begin(); lIter != mSwitches.end();) {
			if(*lIter == 0) {
				lIter = mSwitches.erase(lIter++);
			} else {
				lIter++;
			}
		}
		for(unsigned int i = 0; i < mSwitches.size(); ++i) {
			mSwitches[i]->setSwitchId(i+1);
		}
		mSwitchIdCounter = mSwitches.size()+1;
	}
	
	
}

void HybridBondGraph::simplifyRemoveEmptyJunction() {
	//Find all junctions
	vector<Junction*> lJunctions;
	for(unsigned int i = 0; i < mComponents.size(); ++i) {
		Component* lComponent = mComponents[i];
		if(lComponent != 0) {
			if( lComponent->getElementType() == BondGraphElement::eJunction )
				lJunctions.push_back(dynamic_cast<Junction*>(lComponent));
		}
	}
	
	int lRemainingJunction = lJunctions.size();
	if(lJunctions.size() > 1) {
		for(unsigned int i = 0; i < lJunctions.size() && lRemainingJunction > 1; ++i) {
			Junction* lJunction = lJunctions[i];
			
			//Remove junction if the number of connected component is 2 
			if(lJunction->getPorts().size() == 2) {
				//Check that the power is flowing in the same direction
				Port* lPort1 = lJunction->getPorts()[0];
				Port* lPort2 = lJunction->getPorts()[1];
				
				Component* lComponent1 = lPort1->getBond()->getOtherEnd(lPort1)->getComponent();
				Component* lComponent2 = lPort2->getBond()->getOtherEnd(lPort2)->getComponent();
				
				//Check if at least one of the two components is a jonction
				if(lComponent1->getElementType() == BondGraphElement::eJunction || lComponent2->getElementType() == BondGraphElement::eJunction)  {
				
					if(lPort1->isPowerSide() == lPort2->isPowerSide()) {
						//Check if we are dealing with a switch. In that case with can simplify even with opposite power direction.
						//If the component is a switch, change the power direction of the other bond.
						if( typeid(*lComponent1).name() == typeid(Switch).name() ) {
							lPort2->getBond()->flipPowerStroke();
						} else if( typeid(*lComponent2).name() == typeid(Switch).name() ) {
							lPort1->getBond()->flipPowerStroke();
						}
					}
					
					if(lPort1->isPowerSide() != lPort2->isPowerSide()) {
						//Get the bond connected to the first port
						Bond* lBond1 = lPort1->getBond();
						//Get the other end port
						Port* lToPort1 = lBond1->getOtherEnd(lPort1);
						
						//Get the bond connected to the first port
						Bond* lBond2 = lPort2->getBond();
						//Get the other end port
						Port* lToPort2 = lBond2->getOtherEnd(lPort2);
						//Get the component
						//Component* lComponent2 = lToPort2->getComponent();
						
						//By pass the second bond
						lComponent2->disconnect(lToPort2);
						
						Port* lNewPort = new Port(lToPort2->isPowerSide(), lToPort2->getCausality());
						//lNewPort->connect(lBond1);
						//lNewPort->connect(lComponent2);
						
						lComponent2->connect(lNewPort);
						if(lToPort2->isPowerSide())
							lBond1->connect(lToPort1, lNewPort);
						else
							lBond1->connect(lNewPort, lToPort1);
						
						//Check if the bond is part of the output
						vector<Bond*>::iterator lBondIter = find(mEffortOutputBonds.begin(),mEffortOutputBonds.end(),lBond2);
						if( lBondIter != mEffortOutputBonds.end() ) {
							*lBondIter = lBond1;
						}
						
						lBondIter = find(mFlowOutputBonds.begin(),mFlowOutputBonds.end(),lBond2);
						if( lBondIter != mFlowOutputBonds.end() ) {
							*lBondIter = lBond1;
						}
						
						
						//Delete the junction and bond2
						lBondIter = find(mBonds.begin(),mBonds.end(),lBond2);
						assert(lBondIter != mBonds.end());
						*lBondIter = 0;
						delete lBond2;
						
						vector<Component*>::iterator lComponentIter = find(mComponents.begin(), mComponents.end(), lJunction);
						assert(lComponentIter != mComponents.end());
						*lComponentIter = 0;
						delete lJunction;
						
						--lRemainingJunction;
					}
				}
			}
		}
	}
}
;
