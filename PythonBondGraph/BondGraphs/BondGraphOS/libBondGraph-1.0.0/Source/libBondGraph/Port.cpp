/*
 *  Port.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#include "Port.h"

using namespace BG;

Port::Port(bool inPower, Causality inCausality) : mCausality(inCausality), mIsPowerSide(inPower) {
	mComponent = 0;
	mBond = 0;
}
Port::~Port() {
	mComponent = 0;
	mBond = 0;
}

void Port::setCausality(Causality inCausality) { 
	mCausality = inCausality; 
}

void Port::setVisited(bool inStatus) { 
	mVisited = inStatus; 
}
