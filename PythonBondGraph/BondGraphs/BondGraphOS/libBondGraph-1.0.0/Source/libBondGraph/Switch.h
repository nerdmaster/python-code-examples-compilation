/*
 *  Switch.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 02/04/09.
 */

#ifndef Switch_H
#define Switch_H

#include "Source.h"


namespace BG {
	
class SwitchTrigger {
private:
	Bond* mBond;
	ValueType mType;
	
public:
	SwitchTrigger() { mBond = 0; }
	SwitchTrigger(Bond* inBond, ValueType inType) { mBond = inBond; mType = inType; }
	~SwitchTrigger() {};
	
	Bond* getBond() { return mBond; }
	ValueType getOutputType() const { return mType; }
	unsigned int getIndex() const;
};
	
	
/*! \brief %Switch element of a hybrid bond graph
 *  This class describes a switch component of a hybrid bond graph.
 */
class Switch : public Source {
	
public:
	Switch(bool inInitialState = false, long int inId = -1);
	virtual ~Switch() {}
	
	//! Set the state of the switch. True means that flow is zero, false means that effort is zero.
	virtual void setState(bool inState);
	//! Return the state of the switch.
	virtual bool getState() const { return mState; }
	
	//! Set the type of source. Will change the state accordingly.
	virtual void setType(SourceType inType);
	
	//! Set the switch component ID.
	virtual void setId(long int inId);
	
	//! Set the switch ID.
	virtual void setSwitchId(long int inId) { mSwitchId = inId; }
	//! Return the switch ID.
	virtual long int getSwitchId() const { return mSwitchId; }
	
	//! Set the component associated group.
	virtual void setComponentGroup() { mComponentGroup = eU; }
	
	virtual void write(PACC::XML::Streamer& inStream, bool inIndent=true) const;
	
protected:
	long int mSwitchId;		//!< Switch's ID. This is used in addition of the component ID.
private:
	bool mState;			//!< State of the switch. True means that flow is zero, false means that effort is zero.
	
};

}
#endif
