/*
 *  Switch.cpp
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 02/04/09.
 */

#include "Switch.h"

using namespace BG;

unsigned int SwitchTrigger::getIndex() const { 
	if(mType == eEffort)
		return mBond->getMatrixIndex().reffort;
	else
		return mBond->getMatrixIndex().rflow;
}


Switch::Switch(bool inInitialState, long int inId)
: Source(eEffort, double(0), inId), mSwitchId(-1){
	setState(inInitialState);	
	setId(inId);
	setComponentGroup();
}

void Switch::setId(long int inId) {
	mId = inId;
	std::ostringstream lNameStream;
	lNameStream << SWITCHPREFIX << inId;
	mName = lNameStream.str();
}

void Switch::setState(bool inState) {
	mState = inState;
	if(mState)
		mType = eFlow;
	else
		mType = eEffort;
}

void Switch::setType(SourceType inType) {
	mType = inType;
	if(mType == eFlow) 
		mState = true;
	else
		mState = false;
}

void Switch::write(PACC::XML::Streamer& inStream, bool inIndent) const {
	inStream.openTag("Component", inIndent);
	inStream.insertAttribute("type", "Switch");
	inStream.insertAttribute("id", mId);
	inStream.insertAttribute("swid", mSwitchId);
	inStream.insertAttribute("name", mName);
	inStream.closeTag();
}
