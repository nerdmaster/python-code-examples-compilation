/*
 *  Port.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#ifndef BG_Port_H
#define BG_Port_H

#include <iostream>
#include "Bond.h"
#include "Component.h"

namespace BG {
	
class Bond;
class Component;
	
/*! \brief %Bond Graph Port
 *  This class is used as an interface between Bond and Component. It carry
 *	power direction and causality information.
 */
class Port {
public:
	Port(bool inPower=false, Causality inCausality=eAcausal);
	~Port();
	
	//! \brief Connect to a component.
	void connect(Component *inComponent) { mComponent = inComponent; }
	//! \brief Connect to a bond.
	void connect(Bond *inBond) { mBond = inBond; }
	
	//! \brief Get the connected component.
	Component* getComponent() { return mComponent; }
	//! \brief Get the connected component.
	const Component* getComponent() const { return mComponent; }
	//! \brief Get the connected bond.
	Bond* getBond() { return mBond; }
	//! \brief Get the connected bond.
	const Bond* getBond() const { return mBond; }
	
	//! \brief Get the causality of the port.
	Causality getCausality() const { return mCausality; }
	//! \brief Set the causality of the port.
	void setCausality(Causality inCausality) ;
	
	//! \brief Check if the port is the outgoing power flow of the bond.
	bool isPowerSide() const { return mIsPowerSide; }
	
	//! Assign the power direction to the port. True means that the power is going from the bond to the component.	
	void setPowerStroke(bool inValue) { mIsPowerSide = inValue; }
	
	//! Check if the port was previously visited by a recursive methods.
	bool wasVisited() const { return mVisited; }
	//! Set the visited flag status.
	void setVisited(bool inStatus=true);
	
private:
	Component *mComponent;	//!< Attached component
	Bond *mBond;			//!< Attached bond
	
	Causality mCausality;	//!< Causality of the port
	bool mIsPowerSide;		//!< Tell if the port is on the arrowhead size of the power stroke of the bond. True means that the power is going from the bond to the component.
	
	bool mVisited;			//!< Flag used to by recursive method to walk throught the bond graph.
};
}

#endif
