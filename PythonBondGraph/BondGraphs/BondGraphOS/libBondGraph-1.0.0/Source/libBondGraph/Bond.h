/*
 *  Bond.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#ifndef Bond_H
#define Bond_H

#include <string>
#include "BondGraphElement.h"
#include "Port.h"



namespace BG {
	
class Port;

/*! \brief %Bond element of a bond graph.
 *  A bond link two Component via Port. 
 */
class Bond : public BondGraphElement {
public:
	//! Define the linking type of a bond.
	enum BondGroup {
		eU,		//!< Bond linking a junction with a source component (Se,Sf)
		eS,		//!< Bond linking a junction with a storage component (I,C) with integral causality
		eSd,	//!< Bond linking a junction with a storage component (I,C) with derivative causality
		eR,		//!< Bond linking a junction with a decipative component (R)
		eJ		//!< Bond linking a junction with another junction component (J,TF,GY)
	};
	
	//! Define the type of storage bond to complement the BondGroup
	enum BondStorageType {
		eC,	//!< Capacitor storage type
		eI  //!< Inductor storage type
	};
	
	/*! \brief State equation matrix index.
	 *	The index of the bond give the position of the bond into is respective 
	 *	vector and the offset give the additional index into the concatenated 
	 *	vector. See BondGraph::initializeMatrix for details.
	 */
	typedef struct MatrixIndex {
		int index;		//!< Index in base vector \f$x\f$, \f$x_d\f$, \f$z\f$, \f$z_d\f$, \f$d_o\f$ or \f$d_i\f$.
		int offset;		//!< Index offset in concatenated vector \f$[\dot{x},z_d,d_i,v]^T\f$ or \f$[z,\dot{x_d},d_o,u]^T\f$.
		int joffset;	//!< Index offset for flow in the junction vector \f$[J_{e},J_{f}]^T\f$.
		int effort;		//!< Index of the effort value in the output vector \f$[z,z_d,\dot{x_d},d_i,d_o,v,J_{e},J_{f}]\f$. A negative value refer to the \f$\dot{x}\f$ vector.
		int flow;		//!< Index of the flow value in the output vector \f$[z,z_d,\dot{x_d},d_i,d_0,v,J_{e},J_{f}]\f$. A negative value refer to the \f$\dot{x}\f$ vector.
		int reffort;	//!< Index of the bond's effort value in the reduced output matrix, set -1 if undefined
		int rflow;		//!< Index of the bond's flow value in the reduced output matrix, set -1 if undefined	
	};
	
private:
	Port* mFromPort;			//!< Origine port of the bond.
	Port* mToPort;				//!< Destination port of the bond.
	
	BondStorageType mStorageType; //!< Storage type to complete the group type.
	BondGroup mGroup;			//!< Bond group association.	
	bool mEffortAsInput;		//!< Describs if the effort is an input. If true, the effort of the bond is part of the vector \f$[z,\dot{x_d},d_o,u]^T\f$. If false, the effort of the bond is part fo the vector \f$[\dot{x},z_d,d_i,v]^T\f$.

	MatrixIndex mMatrixIndex;	//!< State equation matrix index
	int mStorageIndex;			//!< Index identifying the storage component
		
public:
	Bond(long int inId = -1);
	virtual ~Bond();

	//! Get the incoming bond port.
	Port* getFromPort() const { return mFromPort; }
	//! Get the outcoming bond port.
	Port* getToPort() const { return mToPort; }
	
	//! Get the other end of the bond.
	Port* getOtherEnd(Port* inPort) const;
	
	//! Set the identification number of the bond.
	void setId(long int inId);

	//! Connect two ports together.
	void connect(Port* inFromPort, Port* inToPort);
	
	//! Find the right label according to the connected elements.
	void findGroup();
	
	//! Return the group type of the bond.
	BondGroup getGroup() const { return mGroup; }
	BondStorageType getStorageType() const { return mStorageType; }
	
	//! Know if the effort value is part of the inputs.
	bool isEffortInput() const { return mEffortAsInput; }
	
	//! Change the power direction of the bond.
	void flipPowerStroke();
	
	//! Set the matrix index of the bond
	void setMatrixIndex(int inIndex, int inOffset, int inEffort, int inFlow, int inJOffset = 0);
	//! Return the matrix index.
	const MatrixIndex& getMatrixIndex() const { return mMatrixIndex; }
	MatrixIndex& getMatrixIndex()  { return mMatrixIndex; }
	
	//! Set the index of the associated storage component
	void setStorageIndex(int inIndex) { mStorageIndex = inIndex; }
	//! Get the index of the associated storage component
	int getStorageIndex() const { return mStorageIndex; }
	
	void write(PACC::XML::Streamer& inStream, bool inIndent=true) const;
};

}

#endif
