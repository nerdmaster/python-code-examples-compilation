/*
 *  BondGraphElement.h
 *  Copyright 2009 Jean-Francois Dupuis.
 *
 *  This file is part of libBondGraph.
 *  
 *  libBondGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  libBondGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libBondGraph.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 05/03/09.
 */

#ifndef BondGraphElement_H
#define BondGraphElement_H

#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <PACC/XML/Streamer.hpp>
#include "BondGraphDefines.h"

namespace BG {

/*! \brief Element of a bond graph
 *  This class describes an element of a bond graph. The an element is generic
 *	class for Bond and Component.
 */
class BondGraphElement {
public:
	//! %Bond graph element type
	enum ElementType { 
		eSource,	//!< Source component
		ePassive,	//!< Passive component
		eJunction,	//!< Junction component
		eBond		//!< Bond element
	};
	
protected:
	ElementType mElementType;       //!< Type of element
	std::string mName;				//!< Name of the element
	std::string mDisplayedName;		//!< Name of the element
	long int mId;					//!< Element's ID
		
public:
	BondGraphElement(ElementType inType, long int inId = -1) : mElementType(inType), mId(inId) {}
	virtual ~BondGraphElement() {}
	
	//! Return the element type.
	ElementType getElementType() { return mElementType; } //Could use rtti instead, but not sure of performance
	
	//! Return the element name.
	const std::string& getName() const { return mName; }
	//! Set the element name to overide the default naming.
	void setName(const std::string& inName) { mName = inName; }
	
	//! Return the element name.
	const std::string& getDisplayedName() const { if(mDisplayedName.empty()) return mName; else return mDisplayedName; }
	//! Set the element name to overide the default naming.
	void setDisplayedName(const std::string& inName) { mDisplayedName = inName; }
	
	//! Return the element ID.
	long int getId() const { return mId; }
	//! Set the element ID. Need to be specific to each kind of element in order to set the right name prefix.
	virtual void setId(long int inId) = 0;

	virtual void write(PACC::XML::Streamer& inStream, bool inIndent=true) const = 0;
}; 
}


#endif
