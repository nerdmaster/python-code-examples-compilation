"""
   NERDLabNet: Activation.py
   Module with some basic neuron activation functions.
   (c) 1990-2014 The University of Texas at Austin
                 Benito R. Fernandez
        NERDLab: Neuro-Engineering, Research & Development Laboratory
"""

import numpy as np
import matplotlib.pyplot as plt

class ActivationFunction:
    """ Default activation function"""
    def __init__(self):
        self.outMin =  np.inf
        self.outMax = -np.inf
        self.outRange = np.inf

    def value(self, x):
        """:returns: the value of activation for value x"""
        pass

    def derivative(self, x):
        """:returns: activation function's derivative value at input x"""
        pass

# Identity Activation Function
class Activation_Identity(ActivationFunction):
    """Identity activation function"""
    def value(self, x):
        """:returns: x"""
        return x

    def derivative(self, x):
        """:returns: always 1.0"""
        return 1.0

# Sigmoidal Activation Function
class Activation_Sigmoid(ActivationFunction):
    """ Sigmoid activation function"""
    def __init__(self):
        self.outMin = 0
        self.outMax = 1
        self.outRange = self.outMax - self.outMin

    def value(self, x):
        """:returns: sigmoid of input"""
        return self.outMin + self.outRange * 1.0 / (1.0 + np.exp(-1 * x))

    def derivative(self, x):
        """:returns: sigmoid's derivative value at input x"""
        logSigmoid = 1.0 / (1.0 + np.exp(-1 * x))
        return logSigmoid * (1.0 - logSigmoid)

# Hyperbolic Tangent Activation Function
class Activation_Tanh(ActivationFunction):
    """ Scaled sigmoid activation function"""
    def __init__(self):
        self.outMin = -1
        self.outMax =  1
        self.outRange = self.outMax - self.outMin

    def value(self, x):
        """:returns: value of activation function: tanh at input x"""
        return np.tanh(x)

    def derivative(self, x):
        """:returns: scaled sigmoid's derivative"""
        tanh = np.tanh(x)
        return (1.0 + tanh) * (1.0 - tanh)


def main():
    l1 = Activation_Sigmoid()
    l2 = Activation_Tanh()
    xx = np.linspace(-10, 10, 100)
    yy1 = [l1.value(x) for x in xx]
    yy2 = [l2.value(x) for x in xx]
    dd1 = [l1.derivative(x) for x in xx]
    dd2 = [l2.derivative(x) for x in xx]
    plt.plot(xx, yy1)
    plt.plot(xx, dd1)
    plt.plot(xx, yy2)
    plt.plot(xx, dd2)
    plt.show()


if __name__ == "__main__":
    main()
