package WorldModel 
  annotation (Coordsys(
      extent=[0, 0; 402, 261], 
      grid=[2, 2], 
      component=[20, 20]));
  model World_1 
    // Initial conditions for state variables
    parameter Real Population_0=1.65e9 "World population in 1900";
    parameter Real Pollution_0=2.0e8 "Pollution in 1900";
    parameter Real Nat_Resources_0=9.0e11 
      "Unrecoverable natural resources in 1900";
    parameter Real Cap_Invest_0=4.0e8 "Capital investment in 1900";
    parameter Real CIAF_0=0.2 
      "Proportion of capital investment in agriculture in 1900";
    
    // Parameters
    parameter Real BRN=0.04 "Normal birth rate";
    parameter Real CIAFN=0.3 "CIAF normalization";
    parameter Real CIAFT=15.0 "CIAF time constant";
    parameter Real CIDN=0.025 "Normal capital discard";
    parameter Real CIGN=0.05 "Normal capital generation";
    parameter Real DRN=0.028 "Normal death rate";
    parameter Real ECIRN=1.0 "Capital normalization";
    parameter Real FC=1.0 "Food coefficient";
    parameter Real FN=1.0 "Food normalization";
    parameter Real Land_Area=1.35e8 "Area of arable land";
    parameter Real NRI=9.0e11 "Initial natural resources";
    parameter Real NRUN=1.0 "Normal resource utilization";
    parameter Real POLN=1.0 "Normal pollution";
    parameter Real POLS=3.5999e9 "Standard pollution";
    parameter Real Pop_dens_norm=26.5 "Normal population density";
    parameter Real QLS=1.0 "Standard quality of life";
    
    // Output variables
    Real years "Time in calendar years";
    annotation (
      Coordsys(
        extent=[-220, -260; 440, 220], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Ellipse(extent=[-62, 10; -58, 6], style(fillColor=73)), 
        Ellipse(extent=[98, 196; 102, 192], style(fillColor=73)), 
        Ellipse(extent=[138, -12; 142, -16], style(fillColor=73)), 
        Ellipse(extent=[-2, -18; 2, -22], style(fillColor=73)), 
        Ellipse(extent=[-178, 2; -174, -2], style(fillColor=73)), 
        Ellipse(extent=[78, 8; 82, 4], style(fillColor=73)), 
        Ellipse(extent=[-2, -92; 2, -96], style(fillColor=73)), 
        Ellipse(extent=[-178, -62; -174, -66], style(fillColor=73)), 
        Ellipse(extent=[-2, -64; 2, -68], style(fillColor=73)), 
        Ellipse(extent=[158, 32; 162, 28], style(fillColor=73)), 
        Ellipse(extent=[38, -258; 42, -262], style(fillColor=73)), 
        Ellipse(extent=[298, -98; 302, -102], style(fillColor=73)), 
        Ellipse(extent=[298, 32; 302, 28], style(fillColor=73)), 
        Ellipse(extent=[298, 82; 302, 78], style(fillColor=73)), 
        Ellipse(extent=[298, 172; 302, 168], style(fillColor=73)), 
        Ellipse(extent=[342, -166; 346, -170], style(fillColor=73)), 
        Ellipse(extent=[354, -230; 358, -234], style(fillColor=74)), 
        Ellipse(extent=[-98, 10; -94, 6], style(fillColor=73)), 
        Ellipse(extent=[-138, -18; -134, -22], style(fillColor=73))), 
      Icon(
        Rectangle(extent=[-220, 220; 442, -262], style(fillColor=49)), 
        Text(extent=[-70, 170; 296, 102], string="Forrester's"), 
        Text(extent=[-70, 46; 296, -22], string="Original"), 
        Text(extent=[-70, -32; 296, -100], string="World Model")), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    SystemDynamics.Source Source1 annotation (extent=[-160, 130; -140, 150]);
    SystemDynamics.Rate_1 Birth_Rate annotation (extent=[-140, 120; -100, 160]
      );
    SystemDynamics.Level Population(x0=Population_0) annotation (extent=[-90, 
          120; -50, 160]);
    SystemDynamics.Rate_1 Death_Rate annotation (extent=[-40, 120; 0, 160]);
    SystemDynamics.Sink Sink1 annotation (extent=[0, 130; 20, 150]);
    SystemDynamics.Source Source2 annotation (extent=[40, 130; 60, 150]);
    SystemDynamics.Rate_1 Generation annotation (extent=[60, 120; 100, 160]);
    SystemDynamics.Level Natural_Resources(x0=Nat_Resources_0) annotation (
        extent=[110, 120; 150, 160]);
    SystemDynamics.Rate_1 Depletion annotation (extent=[160, 120; 200, 160]);
    SystemDynamics.Sink Sink2 annotation (extent=[200, 130; 220, 150]);
    SystemDynamics.Const Gen_Const(k=0) annotation (extent=[64, 86; 96, 116]);
    SystemDynamics.Funct BRMM(x_vals=0:5, y_vals={1.2,1.0,0.85,0.75,0.7,0.7}) 
      annotation (extent=[-90, 170; -120, 200]);
    SystemDynamics.Funct DRMM(x_vals=0:0.5:5, y_vals={3.0,1.8,1.0,0.8,0.7,0.6,
          0.53,0.5,0.5,0.5,0.5}) annotation (extent=[66, 156; 36, 186]);
    SystemDynamics.Gain BR_norm(k=BRN) annotation (extent=[-150, 88; -120, 118
          ]);
    SystemDynamics.Prod_5 Prod_5_1 annotation (extent=[-176, 64; -146, 94]);
    SystemDynamics.Gain DR_norm(k=DRN) annotation (extent=[12, 88; -18, 118]);
    SystemDynamics.Prod_5 Prod_5_2 annotation (extent=[4, 64; 34, 94]);
    SystemDynamics.Funct BRFM(x_vals=0:4, y_vals={0.0,1.0,1.6,1.9,2.0}) 
      annotation (extent=[-190, 20; -160, 50], rotation=90);
    SystemDynamics.Funct BRPM(x_vals=0:10:60, y_vals={1.02,0.9,0.7,0.4,0.25,
          0.15,0.1}) annotation (extent=[-150, 20; -120, 50], rotation=90);
    SystemDynamics.Funct BRCM(x_vals=0:5, y_vals={1.05,1.0,0.9,0.7,0.6,0.55}) 
      annotation (extent=[-110, 20; -80, 50], rotation=90);
    SystemDynamics.Funct DRCM(x_vals=0:5, y_vals={0.9,1.0,1.2,1.5,1.9,3.0}) 
      annotation (extent=[-40, 20; -10, 50], rotation=90);
    SystemDynamics.Funct DRPM(x_vals=0:10:60, y_vals={0.92,1.3,2.0,3.2,4.8,6.8
          ,9.2}) annotation (extent=[0, 20; 30, 50], rotation=90);
    SystemDynamics.Funct DRFM(x_vals=0:0.25:2, y_vals={30.0,3.0,2.0,1.4,1.0,
          0.7,0.6,0.5,0.5}) annotation (extent=[40, 20; 70, 50], rotation=90);
    SystemDynamics.Gain Crowd_Rat(k=1.0/(Land_Area*Pop_dens_norm)) annotation 
      (extent=[-76, 20; -44, 50], rotation=-90);
    SystemDynamics.Gain NR_norm(k=NRUN) annotation (extent=[148, 88; 178, 118]
      );
    SystemDynamics.Prod_2 Prod_2_1 annotation (extent=[126, 64; 156, 94]);
    SystemDynamics.Funct NRMM(x_vals=0:10, y_vals={0.0,1.0,1.8,2.4,2.9,3.3,3.6
          ,3.8,3.9,3.95,4.0}) annotation (extent=[186, 94; 216, 64], rotation=
          180);
    SystemDynamics.Source Source3 annotation (extent=[40, -50; 60, -30]);
    SystemDynamics.Rate_1 CI_Generation annotation (extent=[60, -60; 100, -20]
      );
    SystemDynamics.Level Capital_Investment(x0=Cap_Invest_0) annotation (
        extent=[110, -60; 150, -20]);
    SystemDynamics.Rate_1 CI_Discard annotation (extent=[160, -60; 200, -20]);
    SystemDynamics.Sink Sink3 annotation (extent=[200, -50; 220, -30]);
    SystemDynamics.Gain CIG_norm(k=CIGN) annotation (extent=[50, -92; 80, -62]
      );
    SystemDynamics.Prod_2 Prod_2_2 annotation (extent=[26, -114; 56, -84]);
    SystemDynamics.Gain CID_norm(k=CIDN) annotation (extent=[150, -92; 180, -
          62]);
    SystemDynamics.Funct CIM(x_vals=0:5, y_vals={0.1,1.0,1.8,2.4,2.8,3.0}) 
      annotation (extent=[128, -114; 98, -84]);
    SystemDynamics.Gain MSL(k=1.0/ECIRN) annotation (extent=[262, 16; 292, 46]
      );
    SystemDynamics.Gain NRFR(k=1.0/NRI) annotation (extent=[122, 36; 152, 66])
      ;
    SystemDynamics.Gain ECIR(k=1.0/(1.0 - CIAFN)) annotation (extent=[228, 16
          ; 258, 46]);
    SystemDynamics.Prod_3 Prod_3_1 annotation (extent=[200, 16; 230, 44], 
        rotation=-90);
    SystemDynamics.Funct NREM(x_vals=0:0.25:1, y_vals={0.0,0.15,0.5,0.85,1.0})
       annotation (extent=[190, 66; 160, 36], rotation=180);
    Modelica.Blocks.Math.Division CIR annotation (extent=[122, -10; 92, 20]);
    SystemDynamics.Source Source4 annotation (extent=[-160, -150; -140, -130])
      ;
    SystemDynamics.Rate_1 P_Generation annotation (extent=[-140, -160; -100, -
          120]);
    SystemDynamics.Level Pollution(x0=Pollution_0) annotation (extent=[-90, -
          160; -50, -120]);
    SystemDynamics.Rate_1 P_Absorption annotation (extent=[-40, -160; 0, -120]
      );
    SystemDynamics.Sink Sink4 annotation (extent=[0, -150; 20, -130]);
    SystemDynamics.Linear NotCIAF(m=-1.0, b=1.0) annotation (extent=[174, -4; 
          204, 26]);
    SystemDynamics.Gain Pol_Ratio(k=1.0/POLS) annotation (extent=[-44, -110; -
          14, -80]);
    SystemDynamics.Funct FCM(x_vals=0:5, y_vals={2.4,1.0,0.6,0.4,0.3,0.2}) 
      annotation (extent=[-100, -32; -70, -62], rotation=180);
    SystemDynamics.Prod_3 Prod_3_2 annotation (extent=[-102, -80; -132, -52], 
        rotation=-90);
    SystemDynamics.Gain Food_Ratio(k=FC/FN) annotation (extent=[-134, -80; -
          164, -50]);
    SystemDynamics.Source Source5 annotation (extent=[40, -150; 60, -130]);
    SystemDynamics.Rate_1 CIAFG annotation (extent=[60, -160; 100, -120]);
    SystemDynamics.Level CIAF(x0=CIAF_0) annotation (extent=[110, -160; 150, -
          120]);
    SystemDynamics.Rate_1 CIAFD annotation (extent=[160, -160; 200, -120]);
    SystemDynamics.Sink Sink5 annotation (extent=[200, -150; 220, -130]);
    SystemDynamics.Gain CIAF_D(k=1.0/CIAFT) annotation (extent=[148, -194; 178
          , -164]);
    SystemDynamics.Gain CIAF_G(k=1.0/CIAFT) annotation (extent=[110, -194; 80
          , -164]);
    SystemDynamics.Prod_2 Prod_2_3 annotation (extent=[104, -246; 134, -216]);
    Modelica.Blocks.Math.Division P_Abs annotation (extent=[-58, -194; -28, -
          164]);
    SystemDynamics.Gain Pol_norm(k=POLN) annotation (extent=[-150, -194; -120
          , -164]);
    SystemDynamics.Prod_2 Prod_2_4 annotation (extent=[-176, -234; -146, -204]
      );
    SystemDynamics.Funct POLCM(x_vals=0:5, y_vals={0.05,1.0,3.0,5.4,7.4,8.0}) 
      annotation (extent=[-132, -204; -102, -234], rotation=180);
    SystemDynamics.Funct POLAT(x_vals=0:10:60, y_vals={0.6,2.5,5.0,8.0,11.5,
          15.5,20.0}) annotation (extent=[-52, -204; -22, -234], rotation=180);
    SystemDynamics.Funct CFIFR(x_vals=0:0.5:2, y_vals={1.0,0.6,0.3,0.15,0.1}) 
      annotation (extent=[84, -216; 54, -246], rotation=180);
    SystemDynamics.Funct FPM(x_vals=0:10:60, y_vals={1.02,0.9,0.65,0.35,0.2,
          0.1,0.05}) annotation (extent=[-48, -52; -18, -82], rotation=180);
    SystemDynamics.Prod_2 Prod_2_5 annotation (extent=[214, -184; 244, -214]);
    SystemDynamics.Gain CIRA(k=1.0/CIAFN) annotation (extent=[240, -240; 270, 
          -210]);
    SystemDynamics.Funct FPCI(x_vals=0:6, y_vals={0.5,1.0,1.4,1.7,1.9,2.05,2.2
          }) annotation (extent=[-138, -92; -168, -122], rotation=180);
    SystemDynamics.Funct CIQR(x_vals=0:0.5:2, y_vals={0.7,0.8,1.0,1.5,2.0}) 
      annotation (extent=[152, -214; 182, -244], rotation=180);
    Modelica.Blocks.Math.Division QLMF annotation (extent=[330, -238; 300, -
          208]);
    SystemDynamics.Funct QLM(x_vals=0:5, y_vals={0.2,1.0,1.7,2.3,2.7,2.9}) 
      annotation (extent=[334, -154; 304, -184], rotation=180);
    SystemDynamics.Funct QLF(x_vals=0:4, y_vals={0.0,1.0,1.8,2.4,2.7}) 
      annotation (extent=[366, -218; 396, -248], rotation=180);
    SystemDynamics.Prod_4 Prod_4_1 annotation (extent=[360, -178; 390, -148], 
        rotation=-90);
    SystemDynamics.Funct QLC(x_vals=0:0.5:5, y_vals={2.0,1.3,1.0,0.75,0.55,
          0.45,0.38,0.3,0.25,0.22,0.2}) annotation (extent=[350, -98; 320, -128
          ], rotation=90);
    SystemDynamics.Funct QLP(x_vals=0:10:60, y_vals={1.04,0.85,0.6,0.3,0.15,
          0.05,0.02}) annotation (extent=[390, -98; 360, -128], rotation=90);
    SystemDynamics.Gain Quality_of_Life(k=QLS) annotation (extent=[392, -184; 
          432, -144]);
  equation 
    connect(Source1.MassInPort1, Birth_Rate.MassOutPort1) annotation (points
        =[-139, 140; -132, 140], style(color=77));
    connect(Birth_Rate.MassOutPort2, Population.MassInPort1) annotation (
        points=[-108, 140; -94, 140], style(color=77));
    connect(Population.MassInPort2, Death_Rate.MassOutPort1) annotation (
        points=[-46, 140; -32, 140], style(color=77));
    connect(Death_Rate.MassOutPort2, Sink1.MassInPort1) annotation (points=[-
          10, 140; -1, 140], style(color=77));
    connect(Source2.MassInPort1, Generation.MassOutPort1) annotation (points=[
          62, 140; 70, 140], style(color=77));
    connect(Generation.MassOutPort2, Natural_Resources.MassInPort1) 
      annotation (points=[92, 140; 108, 140], style(color=77));
    connect(Natural_Resources.MassInPort2, Depletion.MassOutPort1) annotation 
      (points=[152, 140; 168, 140], style(color=77));
    connect(Depletion.MassOutPort2, Sink2.MassInPort1) annotation (points=[190
          , 140; 198, 140], style(color=77));
    connect(Gen_Const.OutPort1, Generation.InPort1) annotation (points=[80, 
          111.5; 80, 120], style(color=3));
    connect(Birth_Rate.InPort1, BR_norm.OutPort1) annotation (points=[-120, 
          118; -120, 103; -124.5, 103], style(color=3));
    connect(Death_Rate.InPort1, DR_norm.OutPort1) annotation (points=[-20, 118
          ; -20, 104; -12, 104], style(color=3));
    connect(Prod_5_2.OutPort1, DR_norm.InPort1) annotation (points=[20, 90; 20
          , 104; 7.5, 104], style(color=3));
    connect(DRMM.OutPort1, Prod_5_2.InPort2) annotation (points=[34.5, 172; 28
          , 172; 28, 104; 40, 104; 40, 80; 29.5, 80], style(color=3));
    connect(Population.OutPort3, Prod_5_2.InPort1) annotation (points=[-56, 
          128; -56, 80; 8.5, 80], style(color=3));
    connect(Prod_5_1.OutPort1, BR_norm.InPort1) annotation (points=[-160, 90; 
          -160, 104; -145.5, 104], style(color=3));
    connect(BRMM.OutPort1, Prod_5_1.InPort1) annotation (points=[-121.5, 186; 
          -184, 186; -184, 80; -172, 80], style(color=3));
    connect(Population.OutPort2, Prod_5_1.InPort2) annotation (points=[-84, 
          126; -84, 80; -150, 80], style(color=3));
    connect(BRPM.OutPort1, Prod_5_1.InPort5) annotation (points=[-136, 51.5; -
          136, 60; -162, 60; -162, 68.5], style(color=3));
    connect(Prod_5_1.InPort4, BRCM.OutPort1) annotation (points=[-154, 70; -96
          , 70; -96, 52], style(color=3));
    connect(DRPM.OutPort1, Prod_5_2.InPort5) annotation (points=[14, 51.5; 14
          , 60; 20, 60; 20, 68], style(color=3));
    connect(Prod_5_1.InPort3, BRFM.OutPort1) annotation (points=[-168, 72; -
          176, 72; -176, 51.5], style(color=3));
    connect(Prod_5_2.InPort3, DRCM.OutPort1) annotation (points=[12, 72; -24, 
          72; -24, 52], style(color=3));
    connect(Prod_5_2.InPort4, DRFM.OutPort1) annotation (points=[26, 72; 54, 
          72; 54, 50], style(color=3));
    connect(Population.OutPort1, Crowd_Rat.InPort1) annotation (points=[-70, 
          126; -70, 68; -60, 68; -60, 45.5], style(color=3));
    connect(Crowd_Rat.OutPort1, BRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -96, 8; -96, 23], style(color=3));
    connect(Crowd_Rat.OutPort1, DRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -24, 8; -24, 23], style(color=3));
    connect(Depletion.InPort1, NR_norm.OutPort1) annotation (points=[180, 118
          ; 180, 104; 172, 104], style(color=3));
    connect(Prod_2_1.OutPort1, NR_norm.InPort1) annotation (points=[140, 90; 
          140, 104; 152, 104], style(color=3));
    connect(Prod_2_1.InPort1, Population.OutPort4) annotation (points=[130, 80
          ; 100, 80; 100, 194; -51, 194; -51, 152; -52, 152], style(color=3));
    connect(Source3.MassInPort1, CI_Generation.MassOutPort1) annotation (
        points=[62, -40; 68, -40], style(color=77));
    connect(CI_Generation.MassOutPort2, Capital_Investment.MassInPort1) 
      annotation (points=[92, -40; 106, -40], style(color=77));
    connect(Capital_Investment.MassInPort2, CI_Discard.MassOutPort1) 
      annotation (points=[152, -40; 168, -40], style(color=77));
    connect(CI_Discard.MassOutPort2, Sink3.MassInPort1) annotation (points=[
          190, -40; 198, -40], style(color=77));
    connect(CI_Generation.InPort1, CIG_norm.OutPort1) annotation (points=[80, 
          -62; 80, -77; 75.5, -77], style(color=3));
    connect(CIG_norm.InPort1, Prod_2_2.OutPort1) annotation (points=[54, -76; 
          42, -76; 42, -88], style(color=3));
    connect(Prod_2_2.InPort1, Population.OutPort4) annotation (points=[30, -98
          ; 20, -98; 20, -14; 310, -14; 310, 194; -51, 194; -51, 152; -52, 152]
        , style(color=3));
    connect(CI_Discard.InPort1, CID_norm.OutPort1) annotation (points=[180, -
          60; 180, -78; 175.5, -78], style(color=3));
    connect(Capital_Investment.OutPort3, CID_norm.InPort1) annotation (points=
          [144, -52; 144, -76; 156, -76], style(color=3));
    connect(CIM.OutPort1, Prod_2_2.InPort2) annotation (points=[96, -100; 51.5
          , -100], style(color=3));
    connect(NRMM.OutPort1, Prod_2_1.InPort2) annotation (points=[184.5, 80; 
          152, 80], style(color=3));
    connect(Natural_Resources.OutPort2, NRFR.InPort1) annotation (points=[116
          , 126; 116, 51; 126.5, 51], style(color=3));
    connect(NREM.OutPort1, Prod_3_1.InPort1) annotation (points=[192, 52; 214
          , 52; 214, 39.8], style(color=3));
    connect(NRFR.OutPort1, NREM.InPort1) annotation (points=[147.5, 50; 163, 
          50], style(color=3));
    connect(Capital_Investment.OutPort4, CIR.inPort1) annotation (points=[148
          , -30; 148, 14; 128, 14], style(color=3));
    connect(CIR.outPort, Prod_3_1.InPort3) annotation (points=[90, 6; 80, 6; 
          80, 30; 204, 30], style(color=3));
    connect(Prod_3_1.OutPort1, ECIR.InPort1) annotation (points=[225.5, 30; 
          232.5, 30], style(color=3));
    connect(CIR.inPort2, Prod_2_2.InPort1) annotation (points=[126, -4; 140, -
          4; 140, -14; 20, -14; 20, -98; 30, -98], style(color=3));
    connect(Source4.MassInPort1, P_Generation.MassOutPort1) annotation (points
        =[-139, -140; -130, -140], style(color=77));
    connect(P_Generation.MassOutPort2, Pollution.MassInPort1) annotation (
        points=[-110, -140; -92, -140], style(color=77));
    connect(Pollution.MassInPort2, P_Absorption.MassOutPort1) annotation (
        points=[-48, -140; -30, -140], style(color=77));
    connect(P_Absorption.MassOutPort2, Sink4.MassInPort1) annotation (points=[
          -10, -140; -1, -140], style(color=77));
    connect(NotCIAF.OutPort1, Prod_3_1.InPort2) annotation (points=[204, 12; 
          216, 12; 216, 20.2], style(color=3));
    connect(DRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[16, 22; 16, 
          12; 0, 12; 0, -95; -18.5, -95], style(color=3));
    connect(BRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-136, 22; -
          136, -20; 0, -20; 0, -95; -18.5, -95], style(color=3));
    connect(Crowd_Rat.OutPort1, FCM.InPort1) annotation (points=[-60, 24; -60
          , -48; -72, -48], style(color=3));
    connect(FCM.OutPort1, Prod_3_2.InPort1) annotation (points=[-102, -46; -
          117, -46; -117, -56], style(color=3));
    connect(Food_Ratio.InPort1, Prod_3_2.OutPort1) annotation (points=[-138.5
          , -66; -127.5, -66], style(color=3));
    connect(Food_Ratio.OutPort1, BRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 22], style(color=3));
    connect(Food_Ratio.OutPort1, DRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 0; 54, 0; 54, 22], style(color=3));
    connect(Pollution.OutPort4, Pol_Ratio.InPort1) annotation (points=[-54, -
          129; -54, -95; -39.5, -95], style(color=3));
    connect(Source5.MassInPort1, CIAFG.MassOutPort1) annotation (points=[60, -
          140; 68, -140], style(color=77));
    connect(CIAFG.MassOutPort2, CIAF.MassInPort1) annotation (points=[90, -140
          ; 106, -140], style(color=77));
    connect(CIAF.MassInPort2, CIAFD.MassOutPort1) annotation (points=[152, -
          140; 168, -140], style(color=77));
    connect(CIAFD.MassOutPort2, Sink5.MassInPort1) annotation (points=[190, -
          140; 198, -140], style(color=77));
    connect(CIAF.OutPort4, NotCIAF.InPort1) annotation (points=[146, -128; 146
          , -106; 246, -106; 246, -8; 168, -8; 168, 10; 176, 10], style(color=3
        ));
    connect(CIAF_D.OutPort1, CIAFD.InPort1) annotation (points=[173.5, -180; 
          180, -180; 180, -161], style(color=3));
    connect(CIAF.OutPort3, CIAF_D.InPort1) annotation (points=[144, -154; 144
          , -180; 152.5, -180], style(color=3));
    connect(P_Abs.outPort, P_Absorption.InPort1) annotation (points=[-26.5, -
          180; -20, -180; -20, -161], style(color=3));
    connect(Pollution.OutPort1, P_Abs.inPort1) annotation (points=[-70, -154; 
          -70, -170; -64, -170], style(color=3));
    connect(P_Generation.InPort1, Pol_norm.OutPort1) annotation (points=[-120
          , -161; -120, -180; -124.5, -180], style(color=3));
    connect(Prod_2_4.OutPort1, Pol_norm.InPort1) annotation (points=[-162, -
          208.5; -162, -180; -146, -180], style(color=3));
    connect(Population.OutPort5, Prod_2_4.InPort1) annotation (points=[-86, 
          152; -96, 152; -96, 166; -198, 166; -198, -219; -171.5, -219], style(
          color=3));
    connect(POLCM.OutPort1, Prod_2_4.InPort2) annotation (points=[-133.5, -220
          ; -150.5, -220], style(color=3));
    connect(CIR.outPort, POLCM.InPort1) annotation (points=[90, 6; 80, 6; 80, 
          -6; 10, -6; 10, -116; -102.5, -116; -102.5, -218; -105, -218], style(
          color=3));
    connect(POLAT.OutPort1, P_Abs.inPort2) annotation (points=[-53.5, -220; -
          74, -220; -74, -188; -64, -188], style(color=3));
    connect(CIAFG.InPort1, CIAF_G.OutPort1) annotation (points=[80, -162; 80, 
          -180; 84.5, -180], style(color=3));
    connect(Prod_2_3.OutPort1, CIAF_G.InPort1) annotation (points=[120, -220.5
          ; 120, -180; 105.5, -180], style(color=3));
    connect(CFIFR.OutPort1, Prod_2_3.InPort1) annotation (points=[86, -231; 
          108.5, -231], style(color=3));
    connect(FPM.OutPort1, Prod_3_2.InPort3) annotation (points=[-50, -66; -
          106.5, -66], style(color=3));
    connect(CIAF.OutPort1, Prod_2_5.InPort1) annotation (points=[130, -154; 
          130, -200; 218.5, -200], style(color=3));
    connect(Prod_2_5.InPort2, Prod_3_1.InPort3) annotation (points=[239.5, -
          198; 256, -198; 256, -12; 160, -12; 160, 30; 204.5, 30], style(color=
            3));
    connect(Prod_2_5.OutPort1, CIRA.InPort1) annotation (points=[228, -208; 
          228, -226; 244, -226], style(color=3));
    connect(FPCI.OutPort1, Prod_3_2.InPort2) annotation (points=[-136, -106; -
          118, -106; -118, -75.8], style(color=3));
    connect(CIRA.OutPort1, FPCI.InPort1) annotation (points=[266, -224; 280, -
          224; 280, -250; -210, -250; -210, -106; -165, -106], style(color=3));
    connect(Prod_2_3.InPort2, CIQR.OutPort1) annotation (points=[129.5, -230; 
          150.5, -230], style(color=3));
    connect(QLMF.outPort, CIQR.InPort1) annotation (points=[300, -223; 290, -
          223; 290, -256; 200, -256; 200, -230; 180, -230], style(color=3));
    connect(FPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-20, -66; 0, 
          -66; 0, -96; -18, -96], style(color=3));
    connect(POLAT.InPort1, FPM.InPort1) annotation (points=[-24, -218; 0, -218
          ; 0, -66; -21, -66], style(color=3));
    connect(QLF.OutPort1, QLMF.inPort2) annotation (points=[364.5, -232; 336, 
          -232], style(color=3));
    connect(Food_Ratio.OutPort1, QLF.InPort1) annotation (points=[-160, -64; -
          220, -64; -220, -260; 400, -260; 400, -232; 393, -232], style(color=3
        ));
    connect(CFIFR.InPort1, QLF.InPort1) annotation (points=[56, -230; 40, -230
          ; 40, -260; 400, -260; 400, -232; 393, -232], style(color=3));
    connect(QLM.OutPort1, QLMF.inPort1) annotation (points=[335.5, -168; 344, 
          -168; 344, -214; 334, -214], style(color=3));
    connect(MSL.OutPort1, QLM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, -170; 306, -170], style(color=3));
    connect(MSL.OutPort1, NRMM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, 80; 214, 80], style(color=3));
    connect(ECIR.OutPort1, MSL.InPort1) annotation (points=[254, 32; 266.5, 32
          ], style(color=3));
    connect(CIM.InPort1, QLM.InPort1) annotation (points=[126, -100; 300, -100
          ; 300, -170; 308, -170], style(color=3));
    connect(DRMM.InPort1, NRMM.InPort1) annotation (points=[64, 170; 300, 170
          ; 300, 80; 213, 80], style(color=3));
    connect(BRMM.InPort1, NRMM.InPort1) annotation (points=[-92, 186; -58, 186
          ; -58, 202; 300, 202; 300, 80; 212, 80], style(color=3));
    connect(Prod_4_1.InPort4, QLM.OutPort1) annotation (points=[366, -168; 
          335.5, -168], style(color=3));
    connect(Prod_4_1.InPort2, QLF.OutPort1) annotation (points=[376, -174; 376
          , -200; 356, -200; 356, -232; 364.5, -232], style(color=3));
    connect(QLP.OutPort1, Prod_4_1.InPort1) annotation (points=[374, -130; 374
          , -152], style(color=3));
    connect(Prod_4_1.InPort3, QLC.OutPort1) annotation (points=[366, -158; 334
          , -158; 334, -129.5], style(color=3));
    connect(QLC.InPort1, Crowd_Rat.OutPort1) annotation (points=[334, -100; 
          334, 208; -208, 208; -208, 8; -60, 8; -60, 24], style(color=3));
    connect(QLP.InPort1, BRPM.InPort1) annotation (points=[376, -100; 376, 214
          ; -216, 214; -216, -20; -136, -20; -136, 23], style(color=3));
    connect(Prod_4_1.OutPort1, Quality_of_Life.InPort1) annotation (points=[
          386, -164; 400, -164], style(color=3));
    years = time + 1900;
  end World_1;
  block Parameter_Change 
    extends SystemDynamics.Interfaces.Nonlin_2;
    annotation (Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]));
  equation 
    y = u1*u2;
  end Parameter_Change;
  model World_2 
    // Initial conditions for state variables
    parameter Real Population_0=1.65e9 "World population in 1900";
    parameter Real Pollution_0=2.0e8 "Pollution in 1900";
    parameter Real Nat_Resources_0=9.0e11 
      "Unrecoverable natural resources in 1900";
    parameter Real Cap_Invest_0=4.0e8 "Capital investment in 1900";
    parameter Real CIAF_0=0.2 
      "Proportion of capital investment in agriculture in 1900";
    
    // Parameters
    parameter Real BRN=0.04 "Normal birth rate";
    parameter Real CIAFN=0.3 "CIAF normalization";
    parameter Real CIAFT=15.0 "CIAF time constant";
    parameter Real CIDN=0.025 "Normal capital discard";
    parameter Real CIGN=0.05 "Normal capital generation";
    parameter Real DRN=0.028 "Normal death rate";
    parameter Real ECIRN=1.0 "Capital normalization";
    parameter Real FC=1.0 "Food coefficient";
    parameter Real FN=1.0 "Food normalization";
    parameter Real Land_Area=1.35e8 "Area of arable land";
    parameter Real NRI=9.0e11 "Initial natural resources";
    parameter Real POLN=1.0 "Normal pollution";
    parameter Real POLS=3.5999e9 "Standard pollution";
    parameter Real Pop_dens_norm=26.5 "Normal population density";
    parameter Real QLS=1.0 "Standard quality of life";
    
    // Output variables
    Real years "Time in calendar years";
    
    // Manipulated parameters
    Real NRUN "Normal resource utilization";
    
    annotation (
      Coordsys(
        extent=[-220, -260; 440, 220], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Ellipse(extent=[-62, 10; -58, 6], style(fillColor=73)), 
        Ellipse(extent=[98, 196; 102, 192], style(fillColor=73)), 
        Ellipse(extent=[138, -12; 142, -16], style(fillColor=73)), 
        Ellipse(extent=[-2, -18; 2, -22], style(fillColor=73)), 
        Ellipse(extent=[-178, 2; -174, -2], style(fillColor=73)), 
        Ellipse(extent=[78, 8; 82, 4], style(fillColor=73)), 
        Ellipse(extent=[-2, -92; 2, -96], style(fillColor=73)), 
        Ellipse(extent=[-178, -62; -174, -66], style(fillColor=73)), 
        Ellipse(extent=[-2, -64; 2, -68], style(fillColor=73)), 
        Ellipse(extent=[158, 32; 162, 28], style(fillColor=73)), 
        Ellipse(extent=[38, -258; 42, -262], style(fillColor=73)), 
        Ellipse(extent=[298, -98; 302, -102], style(fillColor=73)), 
        Ellipse(extent=[298, 32; 302, 28], style(fillColor=73)), 
        Ellipse(extent=[298, 82; 302, 78], style(fillColor=73)), 
        Ellipse(extent=[298, 172; 302, 168], style(fillColor=73)), 
        Ellipse(extent=[342, -166; 346, -170], style(fillColor=73)), 
        Ellipse(extent=[354, -230; 358, -234], style(fillColor=74)), 
        Ellipse(extent=[-98, 10; -94, 6], style(fillColor=73)), 
        Ellipse(extent=[-138, -18; -134, -22], style(fillColor=73))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65), 
      Icon(
        Rectangle(extent=[-220, 222; 442, -262], style(fillColor=49)), 
        Text(extent=[-152, 142; 368, 72], string="1st Modification"), 
        Text(
          extent=[-148, 46; 372, -24], 
          string="NRUN", 
          style(color=77)), 
        Text(
          extent=[-150, -34; 370, -104], 
          string="reduced to 0.25", 
          style(color=73)), 
        Text(
          extent=[-150, -120; 370, -190], 
          string="in 1970", 
          style(color=73))));
    SystemDynamics.Source Source1 annotation (extent=[-160, 130; -140, 150]);
    SystemDynamics.Rate_1 Birth_Rate annotation (extent=[-140, 120; -100, 160]
      );
    SystemDynamics.Level Population(x0=Population_0) annotation (extent=[-90, 
          120; -50, 160]);
    SystemDynamics.Rate_1 Death_Rate annotation (extent=[-40, 120; 0, 160]);
    SystemDynamics.Sink Sink1 annotation (extent=[0, 130; 20, 150]);
    SystemDynamics.Source Source2 annotation (extent=[40, 130; 60, 150]);
    SystemDynamics.Rate_1 Generation annotation (extent=[60, 120; 100, 160]);
    SystemDynamics.Level Natural_Resources(x0=Nat_Resources_0) annotation (
        extent=[110, 120; 150, 160]);
    SystemDynamics.Rate_1 Depletion annotation (extent=[160, 120; 200, 160]);
    SystemDynamics.Sink Sink2 annotation (extent=[200, 130; 220, 150]);
    SystemDynamics.Const Gen_Const(k=0) annotation (extent=[64, 86; 96, 116]);
    SystemDynamics.Funct BRMM(x_vals={0,1,2,3,4,5,20}, y_vals={1.2,1.0,0.85,
          0.75,0.7,0.7,0.7}) annotation (extent=[-90, 170; -120, 200]);
    SystemDynamics.Funct DRMM(x_vals={0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,20}, 
        y_vals={3.0,1.8,1.0,0.8,0.7,0.6,0.53,0.5,0.5,0.5,0.5,0.5}) annotation (
        extent=[66, 156; 36, 186]);
    SystemDynamics.Gain BR_norm(k=BRN) annotation (extent=[-150, 88; -120, 118
          ]);
    SystemDynamics.Prod_5 Prod_5_1 annotation (extent=[-176, 64; -146, 94]);
    SystemDynamics.Gain DR_norm(k=DRN) annotation (extent=[12, 88; -18, 118]);
    SystemDynamics.Prod_5 Prod_5_2 annotation (extent=[4, 64; 34, 94]);
    SystemDynamics.Funct BRFM(x_vals={0,1,2,3,4,20}, y_vals={0.0,1.0,1.6,1.9,
          2.0,2.0}) annotation (extent=[-190, 20; -160, 50], rotation=90);
    SystemDynamics.Funct BRPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.02,
          0.9,0.7,0.4,0.25,0.15,0.1,0.1}) annotation (extent=[-150, 20; -120, 
          50], rotation=90);
    SystemDynamics.Funct BRCM(x_vals=0:5, y_vals={1.05,1.0,0.9,0.7,0.6,0.55}) 
      annotation (extent=[-110, 20; -80, 50], rotation=90);
    SystemDynamics.Funct DRCM(x_vals=0:5, y_vals={0.9,1.0,1.2,1.5,1.9,3.0}) 
      annotation (extent=[-40, 20; -10, 50], rotation=90);
    SystemDynamics.Funct DRPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={0.92,
          1.3,2.0,3.2,4.8,6.8,9.2,9.2}) annotation (extent=[0, 20; 30, 50], 
        rotation=90);
    SystemDynamics.Funct DRFM(x_vals={0,0.25,0.5,0.75,1,1.25,1.5,1.75,2,20}, 
        y_vals={30.0,3.0,2.0,1.4,1.0,0.7,0.6,0.5,0.5,0.5}) annotation (extent=[
          40, 20; 70, 50], rotation=90);
    SystemDynamics.Gain Crowd_Rat(k=1.0/(Land_Area*Pop_dens_norm)) annotation 
      (extent=[-76, 20; -44, 50], rotation=-90);
    SystemDynamics.Prod_2 Prod_2_1 annotation (extent=[126, 64; 156, 94]);
    SystemDynamics.Funct NRMM(x_vals=0:10, y_vals={0.0,1.0,1.8,2.4,2.9,3.3,3.6
          ,3.8,3.9,3.95,4.0}) annotation (extent=[186, 94; 216, 64], rotation=
          180);
    SystemDynamics.Source Source3 annotation (extent=[40, -50; 60, -30]);
    SystemDynamics.Rate_1 CI_Generation annotation (extent=[60, -60; 100, -20]
      );
    SystemDynamics.Level Capital_Investment(x0=Cap_Invest_0) annotation (
        extent=[110, -60; 150, -20]);
    SystemDynamics.Rate_1 CI_Discard annotation (extent=[160, -60; 200, -20]);
    SystemDynamics.Sink Sink3 annotation (extent=[200, -50; 220, -30]);
    SystemDynamics.Gain CIG_norm(k=CIGN) annotation (extent=[50, -92; 80, -62]
      );
    SystemDynamics.Prod_2 Prod_2_2 annotation (extent=[26, -114; 56, -84]);
    SystemDynamics.Gain CID_norm(k=CIDN) annotation (extent=[150, -92; 180, -
          62]);
    SystemDynamics.Funct CIM(x_vals={0,1,2,3,4,5,20}, y_vals={0.1,1.0,1.8,2.4,
          2.8,3.0,3.0}) annotation (extent=[128, -114; 98, -84]);
    SystemDynamics.Gain MSL(k=1.0/ECIRN) annotation (extent=[262, 16; 292, 46]
      );
    SystemDynamics.Gain NRFR(k=1.0/NRI) annotation (extent=[122, 36; 152, 66])
      ;
    SystemDynamics.Gain ECIR(k=1.0/(1.0 - CIAFN)) annotation (extent=[228, 16
          ; 258, 46]);
    SystemDynamics.Prod_3 Prod_3_1 annotation (extent=[200, 16; 230, 44], 
        rotation=-90);
    SystemDynamics.Funct NREM(x_vals=0:0.25:1, y_vals={0.0,0.15,0.5,0.85,1.0})
       annotation (extent=[190, 66; 160, 36], rotation=180);
    Modelica.Blocks.Math.Division CIR annotation (extent=[122, -10; 92, 20]);
    SystemDynamics.Source Source4 annotation (extent=[-160, -150; -140, -130])
      ;
    SystemDynamics.Rate_1 P_Generation annotation (extent=[-140, -160; -100, -
          120]);
    SystemDynamics.Level Pollution(x0=Pollution_0) annotation (extent=[-90, -
          160; -50, -120]);
    SystemDynamics.Rate_1 P_Absorption annotation (extent=[-40, -160; 0, -120]
      );
    SystemDynamics.Sink Sink4 annotation (extent=[0, -150; 20, -130]);
    SystemDynamics.Linear NotCIAF(m=-1.0, b=1.0) annotation (extent=[174, -4; 
          204, 26]);
    SystemDynamics.Gain Pol_Ratio(k=1.0/POLS) annotation (extent=[-44, -110; -
          14, -80]);
    SystemDynamics.Funct FCM(x_vals=0:5, y_vals={2.4,1.0,0.6,0.4,0.3,0.2}) 
      annotation (extent=[-100, -32; -70, -62], rotation=180);
    SystemDynamics.Prod_3 Prod_3_2 annotation (extent=[-102, -80; -132, -52], 
        rotation=-90);
    SystemDynamics.Gain Food_Ratio(k=FC/FN) annotation (extent=[-134, -80; -
          164, -50]);
    SystemDynamics.Source Source5 annotation (extent=[40, -150; 60, -130]);
    SystemDynamics.Rate_1 CIAFG annotation (extent=[60, -160; 100, -120]);
    SystemDynamics.Level CIAF(x0=CIAF_0) annotation (extent=[110, -160; 150, -
          120]);
    SystemDynamics.Rate_1 CIAFD annotation (extent=[160, -160; 200, -120]);
    SystemDynamics.Sink Sink5 annotation (extent=[200, -150; 220, -130]);
    SystemDynamics.Gain CIAF_D(k=1.0/CIAFT) annotation (extent=[148, -194; 178
          , -164]);
    SystemDynamics.Gain CIAF_G(k=1.0/CIAFT) annotation (extent=[110, -194; 80
          , -164]);
    SystemDynamics.Prod_2 Prod_2_3 annotation (extent=[104, -246; 134, -216]);
    Modelica.Blocks.Math.Division P_Abs annotation (extent=[-58, -194; -28, -
          164]);
    SystemDynamics.Gain Pol_norm(k=POLN) annotation (extent=[-150, -194; -120
          , -164]);
    SystemDynamics.Prod_2 Prod_2_4 annotation (extent=[-176, -234; -146, -204]
      );
    SystemDynamics.Funct POLCM(x_vals={0,1,2,3,4,5,100}, y_vals={0.05,1.0,3.0,
          5.4,7.4,8.0,8.0}) annotation (extent=[-132, -204; -102, -234], 
        rotation=180);
    SystemDynamics.Funct POLAT(x_vals={0,10,20,30,40,50,60,120}, y_vals={0.6,
          2.5,5.0,8.0,11.5,15.5,20.0,20.0}) annotation (extent=[-52, -204; -22
          , -234], rotation=180);
    SystemDynamics.Funct CFIFR(x_vals={0,0.5,1,1.5,2,20}, y_vals={1.0,0.6,0.3,
          0.15,0.1,0.1}) annotation (extent=[84, -216; 54, -246], rotation=180)
      ;
    SystemDynamics.Funct FPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.02,
          0.9,0.65,0.35,0.2,0.1,0.05,0.05}) annotation (extent=[-48, -52; -18, 
          -82], rotation=180);
    SystemDynamics.Prod_2 Prod_2_5 annotation (extent=[214, -184; 244, -214]);
    SystemDynamics.Gain CIRA(k=1.0/CIAFN) annotation (extent=[240, -240; 270, 
          -210]);
    SystemDynamics.Funct FPCI(x_vals={0,1,2,3,4,5,6,100}, y_vals={0.5,1.0,1.4,
          1.7,1.9,2.05,2.2,2.2}) annotation (extent=[-138, -92; -168, -122], 
        rotation=180);
    SystemDynamics.Funct CIQR(x_vals={0,0.5,1,1.5,2,10}, y_vals={0.7,0.8,1.0,
          1.5,2.0,2.0}) annotation (extent=[152, -214; 182, -244], rotation=180
      );
    Modelica.Blocks.Math.Division QLMF annotation (extent=[330, -238; 300, -
          208]);
    SystemDynamics.Funct QLM(x_vals={0,1,2,3,4,5,20}, y_vals={0.2,1.0,1.7,2.3,
          2.7,2.9,2.9}) annotation (extent=[334, -154; 304, -184], rotation=180
      );
    SystemDynamics.Funct QLF(x_vals={0,1,2,3,4,20}, y_vals={0.0,1.0,1.8,2.4,
          2.7,2.7}) annotation (extent=[366, -218; 396, -248], rotation=180);
    SystemDynamics.Prod_4 Prod_4_1 annotation (extent=[360, -178; 390, -148], 
        rotation=-90);
    SystemDynamics.Funct QLC(x_vals=0:0.5:5, y_vals={2.0,1.3,1.0,0.75,0.55,
          0.45,0.38,0.3,0.25,0.22,0.2}) annotation (extent=[350, -98; 320, -128
          ], rotation=90);
    SystemDynamics.Funct QLP(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.04,
          0.85,0.6,0.3,0.15,0.05,0.02,0.02}) annotation (extent=[390, -98; 360
          , -128], rotation=90);
    SystemDynamics.Gain Quality_of_Life(k=QLS) annotation (extent=[392, -184; 
          432, -144]);
    Parameter_Change NR_norm annotation (extent=[146, 88; 176, 118]);
  equation 
    connect(Source1.MassInPort1, Birth_Rate.MassOutPort1) annotation (points
        =[-139, 140; -132, 140], style(color=77));
    connect(Birth_Rate.MassOutPort2, Population.MassInPort1) annotation (
        points=[-108, 140; -94, 140], style(color=77));
    connect(Population.MassInPort2, Death_Rate.MassOutPort1) annotation (
        points=[-46, 140; -32, 140], style(color=77));
    connect(Death_Rate.MassOutPort2, Sink1.MassInPort1) annotation (points=[-
          10, 140; -1, 140], style(color=77));
    connect(Source2.MassInPort1, Generation.MassOutPort1) annotation (points=[
          62, 140; 70, 140], style(color=77));
    connect(Generation.MassOutPort2, Natural_Resources.MassInPort1) 
      annotation (points=[92, 140; 108, 140], style(color=77));
    connect(Natural_Resources.MassInPort2, Depletion.MassOutPort1) annotation 
      (points=[152, 140; 168, 140], style(color=77));
    connect(Depletion.MassOutPort2, Sink2.MassInPort1) annotation (points=[190
          , 140; 198, 140], style(color=77));
    connect(Gen_Const.OutPort1, Generation.InPort1) annotation (points=[80, 
          111.5; 80, 120], style(color=3));
    connect(Birth_Rate.InPort1, BR_norm.OutPort1) annotation (points=[-120, 
          118; -120, 103; -124.5, 103], style(color=3));
    connect(Death_Rate.InPort1, DR_norm.OutPort1) annotation (points=[-20, 118
          ; -20, 104; -12, 104], style(color=3));
    connect(Prod_5_2.OutPort1, DR_norm.InPort1) annotation (points=[20, 90; 20
          , 104; 7.5, 104], style(color=3));
    connect(DRMM.OutPort1, Prod_5_2.InPort2) annotation (points=[34.5, 172; 28
          , 172; 28, 104; 40, 104; 40, 80; 29.5, 80], style(color=3));
    connect(Population.OutPort3, Prod_5_2.InPort1) annotation (points=[-56, 
          128; -56, 80; 8.5, 80], style(color=3));
    connect(Prod_5_1.OutPort1, BR_norm.InPort1) annotation (points=[-160, 90; 
          -160, 104; -145.5, 104], style(color=3));
    connect(BRMM.OutPort1, Prod_5_1.InPort1) annotation (points=[-121.5, 186; 
          -184, 186; -184, 80; -172, 80], style(color=3));
    connect(Population.OutPort2, Prod_5_1.InPort2) annotation (points=[-84, 
          126; -84, 80; -150, 80], style(color=3));
    connect(BRPM.OutPort1, Prod_5_1.InPort5) annotation (points=[-136, 51.5; -
          136, 60; -162, 60; -162, 68.5], style(color=3));
    connect(Prod_5_1.InPort4, BRCM.OutPort1) annotation (points=[-154, 70; -96
          , 70; -96, 52], style(color=3));
    connect(DRPM.OutPort1, Prod_5_2.InPort5) annotation (points=[14, 51.5; 14
          , 60; 20, 60; 20, 68], style(color=3));
    connect(Prod_5_1.InPort3, BRFM.OutPort1) annotation (points=[-168, 72; -
          176, 72; -176, 51.5], style(color=3));
    connect(Prod_5_2.InPort3, DRCM.OutPort1) annotation (points=[12, 72; -24, 
          72; -24, 52], style(color=3));
    connect(Prod_5_2.InPort4, DRFM.OutPort1) annotation (points=[26, 72; 54, 
          72; 54, 50], style(color=3));
    connect(Population.OutPort1, Crowd_Rat.InPort1) annotation (points=[-70, 
          126; -70, 68; -60, 68; -60, 45.5], style(color=3));
    connect(Crowd_Rat.OutPort1, BRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -96, 8; -96, 23], style(color=3));
    connect(Crowd_Rat.OutPort1, DRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -24, 8; -24, 23], style(color=3));
    connect(Prod_2_1.InPort1, Population.OutPort4) annotation (points=[130, 80
          ; 100, 80; 100, 194; -51, 194; -51, 152; -52, 152], style(color=3));
    connect(Source3.MassInPort1, CI_Generation.MassOutPort1) annotation (
        points=[62, -40; 68, -40], style(color=77));
    connect(CI_Generation.MassOutPort2, Capital_Investment.MassInPort1) 
      annotation (points=[92, -40; 106, -40], style(color=77));
    connect(Capital_Investment.MassInPort2, CI_Discard.MassOutPort1) 
      annotation (points=[152, -40; 168, -40], style(color=77));
    connect(CI_Discard.MassOutPort2, Sink3.MassInPort1) annotation (points=[
          190, -40; 198, -40], style(color=77));
    connect(CI_Generation.InPort1, CIG_norm.OutPort1) annotation (points=[80, 
          -62; 80, -77; 75.5, -77], style(color=3));
    connect(CIG_norm.InPort1, Prod_2_2.OutPort1) annotation (points=[54, -76; 
          42, -76; 42, -88], style(color=3));
    connect(Prod_2_2.InPort1, Population.OutPort4) annotation (points=[30, -98
          ; 20, -98; 20, -14; 310, -14; 310, 194; -51, 194; -51, 152; -52, 152]
        , style(color=3));
    connect(CI_Discard.InPort1, CID_norm.OutPort1) annotation (points=[180, -
          60; 180, -78; 175.5, -78], style(color=3));
    connect(Capital_Investment.OutPort3, CID_norm.InPort1) annotation (points=
          [144, -52; 144, -76; 156, -76], style(color=3));
    connect(CIM.OutPort1, Prod_2_2.InPort2) annotation (points=[96, -100; 51.5
          , -100], style(color=3));
    connect(NRMM.OutPort1, Prod_2_1.InPort2) annotation (points=[184.5, 80; 
          152, 80], style(color=3));
    connect(Natural_Resources.OutPort2, NRFR.InPort1) annotation (points=[116
          , 126; 116, 51; 126.5, 51], style(color=3));
    connect(NREM.OutPort1, Prod_3_1.InPort1) annotation (points=[192, 52; 214
          , 52; 214, 39.8], style(color=3));
    connect(NRFR.OutPort1, NREM.InPort1) annotation (points=[147.5, 50; 163, 
          50], style(color=3));
    connect(Capital_Investment.OutPort4, CIR.inPort1) annotation (points=[148
          , -30; 148, 14; 128, 14], style(color=3));
    connect(CIR.outPort, Prod_3_1.InPort3) annotation (points=[90, 6; 80, 6; 
          80, 30; 204, 30], style(color=3));
    connect(Prod_3_1.OutPort1, ECIR.InPort1) annotation (points=[225.5, 30; 
          232.5, 30], style(color=3));
    connect(CIR.inPort2, Prod_2_2.InPort1) annotation (points=[126, -4; 140, -
          4; 140, -14; 20, -14; 20, -98; 30, -98], style(color=3));
    connect(Source4.MassInPort1, P_Generation.MassOutPort1) annotation (points
        =[-139, -140; -130, -140], style(color=77));
    connect(P_Generation.MassOutPort2, Pollution.MassInPort1) annotation (
        points=[-110, -140; -92, -140], style(color=77));
    connect(Pollution.MassInPort2, P_Absorption.MassOutPort1) annotation (
        points=[-48, -140; -30, -140], style(color=77));
    connect(P_Absorption.MassOutPort2, Sink4.MassInPort1) annotation (points=[
          -10, -140; -1, -140], style(color=77));
    connect(NotCIAF.OutPort1, Prod_3_1.InPort2) annotation (points=[204, 12; 
          216, 12; 216, 20.2], style(color=3));
    connect(DRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[16, 22; 16, 
          12; 0, 12; 0, -95; -18.5, -95], style(color=3));
    connect(BRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-136, 22; -
          136, -20; 0, -20; 0, -95; -18.5, -95], style(color=3));
    connect(Crowd_Rat.OutPort1, FCM.InPort1) annotation (points=[-60, 24; -60
          , -48; -72, -48], style(color=3));
    connect(FCM.OutPort1, Prod_3_2.InPort1) annotation (points=[-102, -46; -
          117, -46; -117, -56], style(color=3));
    connect(Food_Ratio.InPort1, Prod_3_2.OutPort1) annotation (points=[-138.5
          , -66; -127.5, -66], style(color=3));
    connect(Food_Ratio.OutPort1, BRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 22], style(color=3));
    connect(Food_Ratio.OutPort1, DRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 0; 54, 0; 54, 22], style(color=3));
    connect(Pollution.OutPort4, Pol_Ratio.InPort1) annotation (points=[-54, -
          129; -54, -95; -39.5, -95], style(color=3));
    connect(Source5.MassInPort1, CIAFG.MassOutPort1) annotation (points=[60, -
          140; 68, -140], style(color=77));
    connect(CIAFG.MassOutPort2, CIAF.MassInPort1) annotation (points=[90, -140
          ; 106, -140], style(color=77));
    connect(CIAF.MassInPort2, CIAFD.MassOutPort1) annotation (points=[152, -
          140; 168, -140], style(color=77));
    connect(CIAFD.MassOutPort2, Sink5.MassInPort1) annotation (points=[190, -
          140; 198, -140], style(color=77));
    connect(CIAF.OutPort4, NotCIAF.InPort1) annotation (points=[146, -128; 146
          , -106; 246, -106; 246, -8; 168, -8; 168, 10; 176, 10], style(color=3
        ));
    connect(CIAF_D.OutPort1, CIAFD.InPort1) annotation (points=[173.5, -180; 
          180, -180; 180, -161], style(color=3));
    connect(CIAF.OutPort3, CIAF_D.InPort1) annotation (points=[144, -154; 144
          , -180; 152.5, -180], style(color=3));
    connect(P_Abs.outPort, P_Absorption.InPort1) annotation (points=[-26.5, -
          180; -20, -180; -20, -161], style(color=3));
    connect(Pollution.OutPort1, P_Abs.inPort1) annotation (points=[-70, -154; 
          -70, -170; -64, -170], style(color=3));
    connect(P_Generation.InPort1, Pol_norm.OutPort1) annotation (points=[-120
          , -161; -120, -180; -124.5, -180], style(color=3));
    connect(Prod_2_4.OutPort1, Pol_norm.InPort1) annotation (points=[-162, -
          208.5; -162, -180; -146, -180], style(color=3));
    connect(Population.OutPort5, Prod_2_4.InPort1) annotation (points=[-86, 
          152; -96, 152; -96, 166; -198, 166; -198, -219; -171.5, -219], style(
          color=3));
    connect(POLCM.OutPort1, Prod_2_4.InPort2) annotation (points=[-133.5, -220
          ; -150.5, -220], style(color=3));
    connect(CIR.outPort, POLCM.InPort1) annotation (points=[90, 6; 80, 6; 80, 
          -6; 10, -6; 10, -116; -102.5, -116; -102.5, -218; -105, -218], style(
          color=3));
    connect(POLAT.OutPort1, P_Abs.inPort2) annotation (points=[-53.5, -220; -
          74, -220; -74, -188; -64, -188], style(color=3));
    connect(CIAFG.InPort1, CIAF_G.OutPort1) annotation (points=[80, -162; 80, 
          -180; 84.5, -180], style(color=3));
    connect(Prod_2_3.OutPort1, CIAF_G.InPort1) annotation (points=[120, -220.5
          ; 120, -180; 105.5, -180], style(color=3));
    connect(CFIFR.OutPort1, Prod_2_3.InPort1) annotation (points=[86, -231; 
          108.5, -231], style(color=3));
    connect(FPM.OutPort1, Prod_3_2.InPort3) annotation (points=[-50, -66; -
          106.5, -66], style(color=3));
    connect(CIAF.OutPort1, Prod_2_5.InPort1) annotation (points=[130, -154; 
          130, -200; 218.5, -200], style(color=3));
    connect(Prod_2_5.InPort2, Prod_3_1.InPort3) annotation (points=[239.5, -
          198; 256, -198; 256, -12; 160, -12; 160, 30; 204.5, 30], style(color=
            3));
    connect(Prod_2_5.OutPort1, CIRA.InPort1) annotation (points=[228, -208; 
          228, -226; 244, -226], style(color=3));
    connect(FPCI.OutPort1, Prod_3_2.InPort2) annotation (points=[-136, -106; -
          118, -106; -118, -75.8], style(color=3));
    connect(CIRA.OutPort1, FPCI.InPort1) annotation (points=[266, -224; 280, -
          224; 280, -250; -210, -250; -210, -106; -165, -106], style(color=3));
    connect(Prod_2_3.InPort2, CIQR.OutPort1) annotation (points=[129.5, -230; 
          150.5, -230], style(color=3));
    connect(QLMF.outPort, CIQR.InPort1) annotation (points=[300, -223; 290, -
          223; 290, -256; 200, -256; 200, -230; 180, -230], style(color=3));
    connect(FPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-20, -66; 0, 
          -66; 0, -96; -18, -96], style(color=3));
    connect(POLAT.InPort1, FPM.InPort1) annotation (points=[-24, -218; 0, -218
          ; 0, -66; -21, -66], style(color=3));
    connect(QLF.OutPort1, QLMF.inPort2) annotation (points=[364.5, -232; 336, 
          -232], style(color=3));
    connect(Food_Ratio.OutPort1, QLF.InPort1) annotation (points=[-160, -64; -
          220, -64; -220, -260; 400, -260; 400, -232; 393, -232], style(color=3
        ));
    connect(CFIFR.InPort1, QLF.InPort1) annotation (points=[56, -230; 40, -230
          ; 40, -260; 400, -260; 400, -232; 393, -232], style(color=3));
    connect(QLM.OutPort1, QLMF.inPort1) annotation (points=[335.5, -168; 344, 
          -168; 344, -214; 334, -214], style(color=3));
    connect(MSL.OutPort1, QLM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, -170; 306, -170], style(color=3));
    connect(MSL.OutPort1, NRMM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, 80; 214, 80], style(color=3));
    connect(ECIR.OutPort1, MSL.InPort1) annotation (points=[254, 32; 266.5, 32
          ], style(color=3));
    connect(CIM.InPort1, QLM.InPort1) annotation (points=[126, -100; 300, -100
          ; 300, -170; 308, -170], style(color=3));
    connect(DRMM.InPort1, NRMM.InPort1) annotation (points=[64, 170; 300, 170
          ; 300, 80; 213, 80], style(color=3));
    connect(BRMM.InPort1, NRMM.InPort1) annotation (points=[-92, 186; -58, 186
          ; -58, 202; 300, 202; 300, 80; 212, 80], style(color=3));
    connect(Prod_4_1.InPort4, QLM.OutPort1) annotation (points=[366, -168; 
          335.5, -168], style(color=3));
    connect(Prod_4_1.InPort2, QLF.OutPort1) annotation (points=[376, -174; 376
          , -200; 356, -200; 356, -232; 364.5, -232], style(color=3));
    connect(QLP.OutPort1, Prod_4_1.InPort1) annotation (points=[374, -130; 374
          , -152], style(color=3));
    connect(Prod_4_1.InPort3, QLC.OutPort1) annotation (points=[366, -158; 334
          , -158; 334, -129.5], style(color=3));
    connect(QLC.InPort1, Crowd_Rat.OutPort1) annotation (points=[334, -100; 
          334, 208; -208, 208; -208, 8; -60, 8; -60, 24], style(color=3));
    connect(QLP.InPort1, BRPM.InPort1) annotation (points=[376, -100; 376, 214
          ; -216, 214; -216, -20; -136, -20; -136, 23], style(color=3));
    connect(Prod_4_1.OutPort1, Quality_of_Life.InPort1) annotation (points=[
          386, -164; 400, -164], style(color=3));
    connect(Depletion.InPort1, NR_norm.OutPort1) annotation (points=[180, 118
          ; 180, 104; 174, 104], style(color=3));
    connect(Prod_2_1.OutPort1, NR_norm.InPort1) annotation (points=[140, 90; 
          140, 108; 146, 108], style(color=3));
    years = time + 1900;
    
    // Parameter equations
    NRUN = if years > 1970 then 0.25 else 1.0;
    NR_norm.InPort2.signal[1] = NRUN;
  end World_2;
  model World_3 
    // Initial conditions for state variables
    parameter Real Population_0=1.65e9 "World population in 1900";
    parameter Real Pollution_0=2.0e8 "Pollution in 1900";
    parameter Real Nat_Resources_0=9.0e11 
      "Unrecoverable natural resources in 1900";
    parameter Real Cap_Invest_0=4.0e8 "Capital investment in 1900";
    parameter Real CIAF_0=0.2 
      "Proportion of capital investment in agriculture in 1900";
    
    // Parameters
    parameter Real BRN=0.04 "Normal birth rate";
    parameter Real CIAFN=0.3 "CIAF normalization";
    parameter Real CIAFT=15.0 "CIAF time constant";
    parameter Real CIDN=0.025 "Normal capital discard";
    parameter Real CIGN=0.05 "Normal capital generation";
    parameter Real DRN=0.028 "Normal death rate";
    parameter Real ECIRN=1.0 "Capital normalization";
    parameter Real FC=1.0 "Food coefficient";
    parameter Real FN=1.0 "Food normalization";
    parameter Real Land_Area=1.35e8 "Area of arable land";
    parameter Real NRI=9.0e11 "Initial natural resources";
    parameter Real POLS=3.5999e9 "Standard pollution";
    parameter Real Pop_dens_norm=26.5 "Normal population density";
    parameter Real QLS=1.0 "Standard quality of life";
    
    // Output variables
    Real years "Time in calendar years";
    
    // Manipulated parameters
    Real NRUN "Normal resource utilization";
    Real POLN "Normal pollution";
    
    annotation (
      Coordsys(
        extent=[-220, -260; 440, 220], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Ellipse(extent=[-62, 10; -58, 6], style(fillColor=73)), 
        Ellipse(extent=[98, 196; 102, 192], style(fillColor=73)), 
        Ellipse(extent=[138, -12; 142, -16], style(fillColor=73)), 
        Ellipse(extent=[-2, -18; 2, -22], style(fillColor=73)), 
        Ellipse(extent=[-178, 2; -174, -2], style(fillColor=73)), 
        Ellipse(extent=[78, 8; 82, 4], style(fillColor=73)), 
        Ellipse(extent=[-2, -92; 2, -96], style(fillColor=73)), 
        Ellipse(extent=[-178, -62; -174, -66], style(fillColor=73)), 
        Ellipse(extent=[-2, -64; 2, -68], style(fillColor=73)), 
        Ellipse(extent=[158, 32; 162, 28], style(fillColor=73)), 
        Ellipse(extent=[38, -258; 42, -262], style(fillColor=73)), 
        Ellipse(extent=[298, -98; 302, -102], style(fillColor=73)), 
        Ellipse(extent=[298, 32; 302, 28], style(fillColor=73)), 
        Ellipse(extent=[298, 82; 302, 78], style(fillColor=73)), 
        Ellipse(extent=[298, 172; 302, 168], style(fillColor=73)), 
        Ellipse(extent=[342, -166; 346, -170], style(fillColor=73)), 
        Ellipse(extent=[354, -230; 358, -234], style(fillColor=74)), 
        Ellipse(extent=[-98, 10; -94, 6], style(fillColor=73)), 
        Ellipse(extent=[-138, -18; -134, -22], style(fillColor=73))), 
      Window(
        x=0.46, 
        y=0.04, 
        width=0.44, 
        height=0.65), 
      Icon(
        Rectangle(extent=[-220, 222; 442, -262], style(fillColor=49)), 
        Text(extent=[-152, 142; 368, 72], string="2nd Modification"), 
        Text(
          extent=[-148, 46; 372, -24], 
          string="POLN", 
          style(color=77)), 
        Text(
          extent=[-150, -34; 370, -104], 
          string="reduced to 0.25", 
          style(color=73)), 
        Text(
          extent=[-150, -120; 370, -190], 
          string="in 1970", 
          style(color=73))));
    SystemDynamics.Source Source1 annotation (extent=[-160, 130; -140, 150]);
    SystemDynamics.Rate_1 Birth_Rate annotation (extent=[-140, 120; -100, 160]
      );
    SystemDynamics.Level Population(x0=Population_0) annotation (extent=[-90, 
          120; -50, 160]);
    SystemDynamics.Rate_1 Death_Rate annotation (extent=[-40, 120; 0, 160]);
    SystemDynamics.Sink Sink1 annotation (extent=[0, 130; 20, 150]);
    SystemDynamics.Source Source2 annotation (extent=[40, 130; 60, 150]);
    SystemDynamics.Rate_1 Generation annotation (extent=[60, 120; 100, 160]);
    SystemDynamics.Level Natural_Resources(x0=Nat_Resources_0) annotation (
        extent=[110, 120; 150, 160]);
    SystemDynamics.Rate_1 Depletion annotation (extent=[160, 120; 200, 160]);
    SystemDynamics.Sink Sink2 annotation (extent=[200, 130; 220, 150]);
    SystemDynamics.Const Gen_Const(k=0) annotation (extent=[64, 86; 96, 116]);
    SystemDynamics.Funct BRMM(x_vals={0,1,2,3,4,5,20}, y_vals={1.2,1.0,0.85,
          0.75,0.7,0.7,0.7}) annotation (extent=[-90, 170; -120, 200]);
    SystemDynamics.Funct DRMM(x_vals={0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,20}, 
        y_vals={3.0,1.8,1.0,0.8,0.7,0.6,0.53,0.5,0.5,0.5,0.5,0.5}) annotation (
        extent=[66, 156; 36, 186]);
    SystemDynamics.Gain BR_norm(k=BRN) annotation (extent=[-150, 88; -120, 118
          ]);
    SystemDynamics.Prod_5 Prod_5_1 annotation (extent=[-176, 64; -146, 94]);
    SystemDynamics.Gain DR_norm(k=DRN) annotation (extent=[12, 88; -18, 118]);
    SystemDynamics.Prod_5 Prod_5_2 annotation (extent=[4, 64; 34, 94]);
    SystemDynamics.Funct BRFM(x_vals={0,1,2,3,4,20}, y_vals={0.0,1.0,1.6,1.9,
          2.0,2.0}) annotation (extent=[-190, 20; -160, 50], rotation=90);
    SystemDynamics.Funct BRPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.02,
          0.9,0.7,0.4,0.25,0.15,0.1,0.1}) annotation (extent=[-150, 20; -120, 
          50], rotation=90);
    SystemDynamics.Funct BRCM(x_vals=0:5, y_vals={1.05,1.0,0.9,0.7,0.6,0.55}) 
      annotation (extent=[-110, 20; -80, 50], rotation=90);
    SystemDynamics.Funct DRCM(x_vals=0:5, y_vals={0.9,1.0,1.2,1.5,1.9,3.0}) 
      annotation (extent=[-40, 20; -10, 50], rotation=90);
    SystemDynamics.Funct DRPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={0.92,
          1.3,2.0,3.2,4.8,6.8,9.2,9.2}) annotation (extent=[0, 20; 30, 50], 
        rotation=90);
    SystemDynamics.Funct DRFM(x_vals={0,0.25,0.5,0.75,1,1.25,1.5,1.75,2,20}, 
        y_vals={30.0,3.0,2.0,1.4,1.0,0.7,0.6,0.5,0.5,0.5}) annotation (extent=[
          40, 20; 70, 50], rotation=90);
    SystemDynamics.Gain Crowd_Rat(k=1.0/(Land_Area*Pop_dens_norm)) annotation 
      (extent=[-76, 20; -44, 50], rotation=-90);
    SystemDynamics.Prod_2 Prod_2_1 annotation (extent=[126, 64; 156, 94]);
    SystemDynamics.Funct NRMM(x_vals=0:10, y_vals={0.0,1.0,1.8,2.4,2.9,3.3,3.6
          ,3.8,3.9,3.95,4.0}) annotation (extent=[186, 94; 216, 64], rotation=
          180);
    SystemDynamics.Source Source3 annotation (extent=[40, -50; 60, -30]);
    SystemDynamics.Rate_1 CI_Generation annotation (extent=[60, -60; 100, -20]
      );
    SystemDynamics.Level Capital_Investment(x0=Cap_Invest_0) annotation (
        extent=[110, -60; 150, -20]);
    SystemDynamics.Rate_1 CI_Discard annotation (extent=[160, -60; 200, -20]);
    SystemDynamics.Sink Sink3 annotation (extent=[200, -50; 220, -30]);
    SystemDynamics.Gain CIG_norm(k=CIGN) annotation (extent=[50, -92; 80, -62]
      );
    SystemDynamics.Prod_2 Prod_2_2 annotation (extent=[26, -114; 56, -84]);
    SystemDynamics.Gain CID_norm(k=CIDN) annotation (extent=[150, -92; 180, -
          62]);
    SystemDynamics.Funct CIM(x_vals={0,1,2,3,4,5,20}, y_vals={0.1,1.0,1.8,2.4,
          2.8,3.0,3.0}) annotation (extent=[128, -114; 98, -84]);
    SystemDynamics.Gain MSL(k=1.0/ECIRN) annotation (extent=[262, 16; 292, 46]
      );
    SystemDynamics.Gain NRFR(k=1.0/NRI) annotation (extent=[122, 36; 152, 66])
      ;
    SystemDynamics.Gain ECIR(k=1.0/(1.0 - CIAFN)) annotation (extent=[228, 16
          ; 258, 46]);
    SystemDynamics.Prod_3 Prod_3_1 annotation (extent=[200, 16; 230, 44], 
        rotation=-90);
    SystemDynamics.Funct NREM(x_vals=0:0.25:1, y_vals={0.0,0.15,0.5,0.85,1.0})
       annotation (extent=[190, 66; 160, 36], rotation=180);
    Modelica.Blocks.Math.Division CIR annotation (extent=[122, -10; 92, 20]);
    SystemDynamics.Source Source4 annotation (extent=[-160, -150; -140, -130])
      ;
    SystemDynamics.Rate_1 P_Generation annotation (extent=[-140, -160; -100, -
          120]);
    SystemDynamics.Level Pollution(x0=Pollution_0) annotation (extent=[-90, -
          160; -50, -120]);
    SystemDynamics.Rate_1 P_Absorption annotation (extent=[-40, -160; 0, -120]
      );
    SystemDynamics.Sink Sink4 annotation (extent=[0, -150; 20, -130]);
    SystemDynamics.Linear NotCIAF(m=-1.0, b=1.0) annotation (extent=[174, -4; 
          204, 26]);
    SystemDynamics.Gain Pol_Ratio(k=1.0/POLS) annotation (extent=[-44, -110; -
          14, -80]);
    SystemDynamics.Funct FCM(x_vals=0:5, y_vals={2.4,1.0,0.6,0.4,0.3,0.2}) 
      annotation (extent=[-100, -32; -70, -62], rotation=180);
    SystemDynamics.Prod_3 Prod_3_2 annotation (extent=[-102, -80; -132, -52], 
        rotation=-90);
    SystemDynamics.Gain Food_Ratio(k=FC/FN) annotation (extent=[-134, -80; -
          164, -50]);
    SystemDynamics.Source Source5 annotation (extent=[40, -150; 60, -130]);
    SystemDynamics.Rate_1 CIAFG annotation (extent=[60, -160; 100, -120]);
    SystemDynamics.Level CIAF(x0=CIAF_0) annotation (extent=[110, -160; 150, -
          120]);
    SystemDynamics.Rate_1 CIAFD annotation (extent=[160, -160; 200, -120]);
    SystemDynamics.Sink Sink5 annotation (extent=[200, -150; 220, -130]);
    SystemDynamics.Gain CIAF_D(k=1.0/CIAFT) annotation (extent=[148, -194; 178
          , -164]);
    SystemDynamics.Gain CIAF_G(k=1.0/CIAFT) annotation (extent=[110, -194; 80
          , -164]);
    SystemDynamics.Prod_2 Prod_2_3 annotation (extent=[104, -246; 134, -216]);
    Modelica.Blocks.Math.Division P_Abs annotation (extent=[-58, -194; -28, -
          164]);
    SystemDynamics.Prod_2 Prod_2_4 annotation (extent=[-176, -234; -146, -204]
      );
    SystemDynamics.Funct POLCM(x_vals={0,1,2,3,4,5,100}, y_vals={0.05,1.0,3.0,
          5.4,7.4,8.0,8.0}) annotation (extent=[-132, -204; -102, -234], 
        rotation=180);
    SystemDynamics.Funct POLAT(x_vals={0,10,20,30,40,50,60,120}, y_vals={0.6,
          2.5,5.0,8.0,11.5,15.5,20.0,20.0}) annotation (extent=[-52, -204; -22
          , -234], rotation=180);
    SystemDynamics.Funct CFIFR(x_vals={0,0.5,1,1.5,2,20}, y_vals={1.0,0.6,0.3,
          0.15,0.1,0.1}) annotation (extent=[84, -216; 54, -246], rotation=180)
      ;
    SystemDynamics.Funct FPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.02,
          0.9,0.65,0.35,0.2,0.1,0.05,0.05}) annotation (extent=[-48, -52; -18, 
          -82], rotation=180);
    SystemDynamics.Prod_2 Prod_2_5 annotation (extent=[214, -184; 244, -214]);
    SystemDynamics.Gain CIRA(k=1.0/CIAFN) annotation (extent=[240, -240; 270, 
          -210]);
    SystemDynamics.Funct FPCI(x_vals={0,1,2,3,4,5,6,100}, y_vals={0.5,1.0,1.4,
          1.7,1.9,2.05,2.2,2.2}) annotation (extent=[-138, -92; -168, -122], 
        rotation=180);
    SystemDynamics.Funct CIQR(x_vals={0,0.5,1,1.5,2,10}, y_vals={0.7,0.8,1.0,
          1.5,2.0,2.0}) annotation (extent=[152, -214; 182, -244], rotation=180
      );
    Modelica.Blocks.Math.Division QLMF annotation (extent=[330, -238; 300, -
          208]);
    SystemDynamics.Funct QLM(x_vals={0,1,2,3,4,5,20}, y_vals={0.2,1.0,1.7,2.3,
          2.7,2.9,2.9}) annotation (extent=[334, -154; 304, -184], rotation=180
      );
    SystemDynamics.Funct QLF(x_vals={0,1,2,3,4,20}, y_vals={0.0,1.0,1.8,2.4,
          2.7,2.7}) annotation (extent=[366, -218; 396, -248], rotation=180);
    SystemDynamics.Prod_4 Prod_4_1 annotation (extent=[360, -178; 390, -148], 
        rotation=-90);
    SystemDynamics.Funct QLC(x_vals=0:0.5:5, y_vals={2.0,1.3,1.0,0.75,0.55,
          0.45,0.38,0.3,0.25,0.22,0.2}) annotation (extent=[350, -98; 320, -128
          ], rotation=90);
    SystemDynamics.Funct QLP(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.04,
          0.85,0.6,0.3,0.15,0.05,0.02,0.02}) annotation (extent=[390, -98; 360
          , -128], rotation=90);
    SystemDynamics.Gain Quality_of_Life(k=QLS) annotation (extent=[392, -184; 
          432, -144]);
    Parameter_Change NR_norm annotation (extent=[146, 88; 176, 118]);
    Parameter_Change Pol_norm annotation (extent=[-154, -194; -124, -164]);
  equation 
    connect(Source1.MassInPort1, Birth_Rate.MassOutPort1) annotation (points
        =[-139, 140; -132, 140], style(color=77));
    connect(Birth_Rate.MassOutPort2, Population.MassInPort1) annotation (
        points=[-108, 140; -94, 140], style(color=77));
    connect(Population.MassInPort2, Death_Rate.MassOutPort1) annotation (
        points=[-46, 140; -32, 140], style(color=77));
    connect(Death_Rate.MassOutPort2, Sink1.MassInPort1) annotation (points=[-
          10, 140; -1, 140], style(color=77));
    connect(Source2.MassInPort1, Generation.MassOutPort1) annotation (points=[
          62, 140; 70, 140], style(color=77));
    connect(Generation.MassOutPort2, Natural_Resources.MassInPort1) 
      annotation (points=[92, 140; 108, 140], style(color=77));
    connect(Natural_Resources.MassInPort2, Depletion.MassOutPort1) annotation 
      (points=[152, 140; 168, 140], style(color=77));
    connect(Depletion.MassOutPort2, Sink2.MassInPort1) annotation (points=[190
          , 140; 198, 140], style(color=77));
    connect(Gen_Const.OutPort1, Generation.InPort1) annotation (points=[80, 
          111.5; 80, 120], style(color=3));
    connect(Birth_Rate.InPort1, BR_norm.OutPort1) annotation (points=[-120, 
          118; -120, 103; -124.5, 103], style(color=3));
    connect(Death_Rate.InPort1, DR_norm.OutPort1) annotation (points=[-20, 118
          ; -20, 104; -12, 104], style(color=3));
    connect(Prod_5_2.OutPort1, DR_norm.InPort1) annotation (points=[20, 90; 20
          , 104; 7.5, 104], style(color=3));
    connect(DRMM.OutPort1, Prod_5_2.InPort2) annotation (points=[34.5, 172; 28
          , 172; 28, 104; 40, 104; 40, 80; 29.5, 80], style(color=3));
    connect(Population.OutPort3, Prod_5_2.InPort1) annotation (points=[-56, 
          128; -56, 80; 8.5, 80], style(color=3));
    connect(Prod_5_1.OutPort1, BR_norm.InPort1) annotation (points=[-160, 90; 
          -160, 104; -145.5, 104], style(color=3));
    connect(BRMM.OutPort1, Prod_5_1.InPort1) annotation (points=[-121.5, 186; 
          -184, 186; -184, 80; -172, 80], style(color=3));
    connect(Population.OutPort2, Prod_5_1.InPort2) annotation (points=[-84, 
          126; -84, 80; -150, 80], style(color=3));
    connect(BRPM.OutPort1, Prod_5_1.InPort5) annotation (points=[-136, 51.5; -
          136, 60; -162, 60; -162, 68.5], style(color=3));
    connect(Prod_5_1.InPort4, BRCM.OutPort1) annotation (points=[-154, 70; -96
          , 70; -96, 52], style(color=3));
    connect(DRPM.OutPort1, Prod_5_2.InPort5) annotation (points=[14, 51.5; 14
          , 60; 20, 60; 20, 68], style(color=3));
    connect(Prod_5_1.InPort3, BRFM.OutPort1) annotation (points=[-168, 72; -
          176, 72; -176, 51.5], style(color=3));
    connect(Prod_5_2.InPort3, DRCM.OutPort1) annotation (points=[12, 72; -24, 
          72; -24, 52], style(color=3));
    connect(Prod_5_2.InPort4, DRFM.OutPort1) annotation (points=[26, 72; 54, 
          72; 54, 50], style(color=3));
    connect(Population.OutPort1, Crowd_Rat.InPort1) annotation (points=[-70, 
          126; -70, 68; -60, 68; -60, 45.5], style(color=3));
    connect(Crowd_Rat.OutPort1, BRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -96, 8; -96, 23], style(color=3));
    connect(Crowd_Rat.OutPort1, DRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -24, 8; -24, 23], style(color=3));
    connect(Prod_2_1.InPort1, Population.OutPort4) annotation (points=[130, 80
          ; 100, 80; 100, 194; -51, 194; -51, 152; -52, 152], style(color=3));
    connect(Source3.MassInPort1, CI_Generation.MassOutPort1) annotation (
        points=[62, -40; 68, -40], style(color=77));
    connect(CI_Generation.MassOutPort2, Capital_Investment.MassInPort1) 
      annotation (points=[92, -40; 106, -40], style(color=77));
    connect(Capital_Investment.MassInPort2, CI_Discard.MassOutPort1) 
      annotation (points=[152, -40; 168, -40], style(color=77));
    connect(CI_Discard.MassOutPort2, Sink3.MassInPort1) annotation (points=[
          190, -40; 198, -40], style(color=77));
    connect(CI_Generation.InPort1, CIG_norm.OutPort1) annotation (points=[80, 
          -62; 80, -77; 75.5, -77], style(color=3));
    connect(CIG_norm.InPort1, Prod_2_2.OutPort1) annotation (points=[54, -76; 
          42, -76; 42, -88], style(color=3));
    connect(Prod_2_2.InPort1, Population.OutPort4) annotation (points=[30, -98
          ; 20, -98; 20, -14; 310, -14; 310, 194; -51, 194; -51, 152; -52, 152]
        , style(color=3));
    connect(CI_Discard.InPort1, CID_norm.OutPort1) annotation (points=[180, -
          60; 180, -78; 175.5, -78], style(color=3));
    connect(Capital_Investment.OutPort3, CID_norm.InPort1) annotation (points=
          [144, -52; 144, -76; 156, -76], style(color=3));
    connect(CIM.OutPort1, Prod_2_2.InPort2) annotation (points=[96, -100; 51.5
          , -100], style(color=3));
    connect(NRMM.OutPort1, Prod_2_1.InPort2) annotation (points=[184.5, 80; 
          152, 80], style(color=3));
    connect(Natural_Resources.OutPort2, NRFR.InPort1) annotation (points=[116
          , 126; 116, 51; 126.5, 51], style(color=3));
    connect(NREM.OutPort1, Prod_3_1.InPort1) annotation (points=[192, 52; 214
          , 52; 214, 39.8], style(color=3));
    connect(NRFR.OutPort1, NREM.InPort1) annotation (points=[147.5, 50; 163, 
          50], style(color=3));
    connect(Capital_Investment.OutPort4, CIR.inPort1) annotation (points=[148
          , -30; 148, 14; 128, 14], style(color=3));
    connect(CIR.outPort, Prod_3_1.InPort3) annotation (points=[90, 6; 80, 6; 
          80, 30; 204, 30], style(color=3));
    connect(Prod_3_1.OutPort1, ECIR.InPort1) annotation (points=[225.5, 30; 
          232.5, 30], style(color=3));
    connect(CIR.inPort2, Prod_2_2.InPort1) annotation (points=[126, -4; 140, -
          4; 140, -14; 20, -14; 20, -98; 30, -98], style(color=3));
    connect(Source4.MassInPort1, P_Generation.MassOutPort1) annotation (points
        =[-139, -140; -130, -140], style(color=77));
    connect(P_Generation.MassOutPort2, Pollution.MassInPort1) annotation (
        points=[-110, -140; -92, -140], style(color=77));
    connect(Pollution.MassInPort2, P_Absorption.MassOutPort1) annotation (
        points=[-48, -140; -30, -140], style(color=77));
    connect(P_Absorption.MassOutPort2, Sink4.MassInPort1) annotation (points=[
          -10, -140; -1, -140], style(color=77));
    connect(NotCIAF.OutPort1, Prod_3_1.InPort2) annotation (points=[204, 12; 
          216, 12; 216, 20.2], style(color=3));
    connect(DRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[16, 22; 16, 
          12; 0, 12; 0, -95; -18.5, -95], style(color=3));
    connect(BRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-136, 22; -
          136, -20; 0, -20; 0, -95; -18.5, -95], style(color=3));
    connect(Crowd_Rat.OutPort1, FCM.InPort1) annotation (points=[-60, 24; -60
          , -48; -72, -48], style(color=3));
    connect(FCM.OutPort1, Prod_3_2.InPort1) annotation (points=[-102, -46; -
          117, -46; -117, -56], style(color=3));
    connect(Food_Ratio.InPort1, Prod_3_2.OutPort1) annotation (points=[-138.5
          , -66; -127.5, -66], style(color=3));
    connect(Food_Ratio.OutPort1, BRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 22], style(color=3));
    connect(Food_Ratio.OutPort1, DRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 0; 54, 0; 54, 22], style(color=3));
    connect(Pollution.OutPort4, Pol_Ratio.InPort1) annotation (points=[-54, -
          129; -54, -95; -39.5, -95], style(color=3));
    connect(Source5.MassInPort1, CIAFG.MassOutPort1) annotation (points=[60, -
          140; 68, -140], style(color=77));
    connect(CIAFG.MassOutPort2, CIAF.MassInPort1) annotation (points=[90, -140
          ; 106, -140], style(color=77));
    connect(CIAF.MassInPort2, CIAFD.MassOutPort1) annotation (points=[152, -
          140; 168, -140], style(color=77));
    connect(CIAFD.MassOutPort2, Sink5.MassInPort1) annotation (points=[190, -
          140; 198, -140], style(color=77));
    connect(CIAF.OutPort4, NotCIAF.InPort1) annotation (points=[146, -128; 146
          , -106; 246, -106; 246, -8; 168, -8; 168, 10; 176, 10], style(color=3
        ));
    connect(CIAF_D.OutPort1, CIAFD.InPort1) annotation (points=[173.5, -180; 
          180, -180; 180, -161], style(color=3));
    connect(CIAF.OutPort3, CIAF_D.InPort1) annotation (points=[144, -154; 144
          , -180; 152.5, -180], style(color=3));
    connect(P_Abs.outPort, P_Absorption.InPort1) annotation (points=[-26.5, -
          180; -20, -180; -20, -161], style(color=3));
    connect(Pollution.OutPort1, P_Abs.inPort1) annotation (points=[-70, -154; 
          -70, -170; -64, -170], style(color=3));
    connect(Population.OutPort5, Prod_2_4.InPort1) annotation (points=[-86, 
          152; -96, 152; -96, 166; -198, 166; -198, -219; -171.5, -219], style(
          color=3));
    connect(POLCM.OutPort1, Prod_2_4.InPort2) annotation (points=[-133.5, -220
          ; -150.5, -220], style(color=3));
    connect(CIR.outPort, POLCM.InPort1) annotation (points=[90, 6; 80, 6; 80, 
          -6; 10, -6; 10, -116; -102.5, -116; -102.5, -218; -105, -218], style(
          color=3));
    connect(POLAT.OutPort1, P_Abs.inPort2) annotation (points=[-53.5, -220; -
          74, -220; -74, -188; -64, -188], style(color=3));
    connect(CIAFG.InPort1, CIAF_G.OutPort1) annotation (points=[80, -162; 80, 
          -180; 84.5, -180], style(color=3));
    connect(Prod_2_3.OutPort1, CIAF_G.InPort1) annotation (points=[120, -220.5
          ; 120, -180; 105.5, -180], style(color=3));
    connect(CFIFR.OutPort1, Prod_2_3.InPort1) annotation (points=[86, -231; 
          108.5, -231], style(color=3));
    connect(FPM.OutPort1, Prod_3_2.InPort3) annotation (points=[-50, -66; -
          106.5, -66], style(color=3));
    connect(CIAF.OutPort1, Prod_2_5.InPort1) annotation (points=[130, -154; 
          130, -200; 218.5, -200], style(color=3));
    connect(Prod_2_5.InPort2, Prod_3_1.InPort3) annotation (points=[239.5, -
          198; 256, -198; 256, -12; 160, -12; 160, 30; 204.5, 30], style(color=
            3));
    connect(Prod_2_5.OutPort1, CIRA.InPort1) annotation (points=[228, -208; 
          228, -226; 244, -226], style(color=3));
    connect(FPCI.OutPort1, Prod_3_2.InPort2) annotation (points=[-136, -106; -
          118, -106; -118, -75.8], style(color=3));
    connect(CIRA.OutPort1, FPCI.InPort1) annotation (points=[266, -224; 280, -
          224; 280, -250; -210, -250; -210, -106; -165, -106], style(color=3));
    connect(Prod_2_3.InPort2, CIQR.OutPort1) annotation (points=[129.5, -230; 
          150.5, -230], style(color=3));
    connect(QLMF.outPort, CIQR.InPort1) annotation (points=[300, -223; 290, -
          223; 290, -256; 200, -256; 200, -230; 180, -230], style(color=3));
    connect(FPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-20, -66; 0, 
          -66; 0, -96; -18, -96], style(color=3));
    connect(POLAT.InPort1, FPM.InPort1) annotation (points=[-24, -218; 0, -218
          ; 0, -66; -21, -66], style(color=3));
    connect(QLF.OutPort1, QLMF.inPort2) annotation (points=[364.5, -232; 336, 
          -232], style(color=3));
    connect(Food_Ratio.OutPort1, QLF.InPort1) annotation (points=[-160, -64; -
          220, -64; -220, -260; 400, -260; 400, -232; 393, -232], style(color=3
        ));
    connect(CFIFR.InPort1, QLF.InPort1) annotation (points=[56, -230; 40, -230
          ; 40, -260; 400, -260; 400, -232; 393, -232], style(color=3));
    connect(QLM.OutPort1, QLMF.inPort1) annotation (points=[335.5, -168; 344, 
          -168; 344, -214; 334, -214], style(color=3));
    connect(MSL.OutPort1, QLM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, -170; 306, -170], style(color=3));
    connect(MSL.OutPort1, NRMM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, 80; 214, 80], style(color=3));
    connect(ECIR.OutPort1, MSL.InPort1) annotation (points=[254, 32; 266.5, 32
          ], style(color=3));
    connect(CIM.InPort1, QLM.InPort1) annotation (points=[126, -100; 300, -100
          ; 300, -170; 308, -170], style(color=3));
    connect(DRMM.InPort1, NRMM.InPort1) annotation (points=[64, 170; 300, 170
          ; 300, 80; 213, 80], style(color=3));
    connect(BRMM.InPort1, NRMM.InPort1) annotation (points=[-92, 186; -58, 186
          ; -58, 202; 300, 202; 300, 80; 212, 80], style(color=3));
    connect(Prod_4_1.InPort4, QLM.OutPort1) annotation (points=[366, -168; 
          335.5, -168], style(color=3));
    connect(Prod_4_1.InPort2, QLF.OutPort1) annotation (points=[376, -174; 376
          , -200; 356, -200; 356, -232; 364.5, -232], style(color=3));
    connect(QLP.OutPort1, Prod_4_1.InPort1) annotation (points=[374, -130; 374
          , -152], style(color=3));
    connect(Prod_4_1.InPort3, QLC.OutPort1) annotation (points=[366, -158; 334
          , -158; 334, -129.5], style(color=3));
    connect(QLC.InPort1, Crowd_Rat.OutPort1) annotation (points=[334, -100; 
          334, 208; -208, 208; -208, 8; -60, 8; -60, 24], style(color=3));
    connect(QLP.InPort1, BRPM.InPort1) annotation (points=[376, -100; 376, 214
          ; -216, 214; -216, -20; -136, -20; -136, 23], style(color=3));
    connect(Prod_4_1.OutPort1, Quality_of_Life.InPort1) annotation (points=[
          386, -164; 400, -164], style(color=3));
    connect(Depletion.InPort1, NR_norm.OutPort1) annotation (points=[180, 118
          ; 180, 104; 174, 104], style(color=3));
    connect(Prod_2_1.OutPort1, NR_norm.InPort1) annotation (points=[140, 90; 
          140, 108; 146, 108], style(color=3));
    connect(P_Generation.InPort1, Pol_norm.OutPort1) annotation (points=[-120
          , -162; -120, -178; -125.5, -178], style(color=3));
    connect(Prod_2_4.OutPort1, Pol_norm.InPort1) annotation (points=[-160, -
          208; -160, -174; -154, -174], style(color=3));
    years = time + 1900;
    
    // Parameter equations
    NRUN = if years > 1970 then 0.25 else 1.0;
    NR_norm.InPort2.signal[1] = NRUN;
    POLN = if years > 1970 then 0.25 else 1.0;
    Pol_norm.InPort2.signal[1] = POLN;
  end World_3;
  model World_4 
    // Initial conditions for state variables
    parameter Real Population_0=1.65e9 "World population in 1900";
    parameter Real Pollution_0=2.0e8 "Pollution in 1900";
    parameter Real Nat_Resources_0=9.0e11 
      "Unrecoverable natural resources in 1900";
    parameter Real Cap_Invest_0=4.0e8 "Capital investment in 1900";
    parameter Real CIAF_0=0.2 
      "Proportion of capital investment in agriculture in 1900";
    
    // Parameters
    parameter Real BRN=0.04 "Normal birth rate";
    parameter Real CIAFN=0.3 "CIAF normalization";
    parameter Real CIAFT=15.0 "CIAF time constant";
    parameter Real CIDN=0.025 "Normal capital discard";
    parameter Real CIGN=0.05 "Normal capital generation";
    parameter Real ECIRN=1.0 "Capital normalization";
    parameter Real FC=1.0 "Food coefficient";
    parameter Real FN=1.0 "Food normalization";
    parameter Real Land_Area=1.35e8 "Area of arable land";
    parameter Real NRI=9.0e11 "Initial natural resources";
    parameter Real POLS=3.5999e9 "Standard pollution";
    parameter Real Pop_dens_norm=26.5 "Normal population density";
    parameter Real QLS=1.0 "Standard quality of life";
    
    // Output variables
    Real years "Time in calendar years";
    
    // Manipulated parameters
    Real NRUN "Normal resource utilization";
    Real POLN "Normal pollution";
    Real DRN "Normal death rate";
    
    // Measurement data
    Real time_vals[12]={1900,1910,1920,1930,1940,1950,1960,1970,1980,1990,2000
        ,2500};
    Real pop_vals[12]={1.6e9,1.75e9,1.86e9,2.07e9,2.3e9,2.556e9,3.0395e9,
        3.70656e9,4.3781e9,5.1903e9,5.9962e9,6e9};
    Real Pop_meas;
    annotation (
      Coordsys(
        extent=[-220, -260; 440, 220], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Ellipse(extent=[-62, 10; -58, 6], style(fillColor=73)), 
        Ellipse(extent=[98, 196; 102, 192], style(fillColor=73)), 
        Ellipse(extent=[138, -12; 142, -16], style(fillColor=73)), 
        Ellipse(extent=[-2, -18; 2, -22], style(fillColor=73)), 
        Ellipse(extent=[-178, 2; -174, -2], style(fillColor=73)), 
        Ellipse(extent=[78, 8; 82, 4], style(fillColor=73)), 
        Ellipse(extent=[-2, -92; 2, -96], style(fillColor=73)), 
        Ellipse(extent=[-178, -62; -174, -66], style(fillColor=73)), 
        Ellipse(extent=[-2, -64; 2, -68], style(fillColor=73)), 
        Ellipse(extent=[158, 32; 162, 28], style(fillColor=73)), 
        Ellipse(extent=[38, -258; 42, -262], style(fillColor=73)), 
        Ellipse(extent=[298, -98; 302, -102], style(fillColor=73)), 
        Ellipse(extent=[298, 32; 302, 28], style(fillColor=73)), 
        Ellipse(extent=[298, 82; 302, 78], style(fillColor=73)), 
        Ellipse(extent=[298, 172; 302, 168], style(fillColor=73)), 
        Ellipse(extent=[342, -166; 346, -170], style(fillColor=73)), 
        Ellipse(extent=[354, -230; 358, -234], style(fillColor=74)), 
        Ellipse(extent=[-98, 10; -94, 6], style(fillColor=73)), 
        Ellipse(extent=[-138, -18; -134, -22], style(fillColor=73))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65), 
      Icon(
        Rectangle(extent=[-220, 222; 442, -262], style(fillColor=49)), 
        Text(extent=[-152, 142; 368, 72], string="3rd Modification"), 
        Text(
          extent=[-148, 46; 372, -24], 
          string="DRN", 
          style(color=77)), 
        Text(
          extent=[-150, -34; 370, -104], 
          string="reduced to 0.2", 
          style(color=73)), 
        Text(
          extent=[-150, -120; 370, -190], 
          string="in 1970", 
          style(color=73))));
    SystemDynamics.Source Source1 annotation (extent=[-160, 130; -140, 150]);
    SystemDynamics.Rate_1 Birth_Rate annotation (extent=[-140, 120; -100, 160]
      );
    SystemDynamics.Level Population(x0=Population_0) annotation (extent=[-90, 
          120; -50, 160]);
    SystemDynamics.Rate_1 Death_Rate annotation (extent=[-40, 120; 0, 160]);
    SystemDynamics.Sink Sink1 annotation (extent=[0, 130; 20, 150]);
    SystemDynamics.Source Source2 annotation (extent=[40, 130; 60, 150]);
    SystemDynamics.Rate_1 Generation annotation (extent=[60, 120; 100, 160]);
    SystemDynamics.Level Natural_Resources(x0=Nat_Resources_0) annotation (
        extent=[110, 120; 150, 160]);
    SystemDynamics.Rate_1 Depletion annotation (extent=[160, 120; 200, 160]);
    SystemDynamics.Sink Sink2 annotation (extent=[200, 130; 220, 150]);
    SystemDynamics.Const Gen_Const(k=0) annotation (extent=[64, 86; 96, 116]);
    SystemDynamics.Funct BRMM(x_vals={0,1,2,3,4,5,20}, y_vals={1.2,1.0,0.85,
          0.75,0.7,0.7,0.7}) annotation (extent=[-90, 170; -120, 200]);
    SystemDynamics.Funct DRMM(x_vals={0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,20}, 
        y_vals={3.0,1.8,1.0,0.8,0.7,0.6,0.53,0.5,0.5,0.5,0.5,0.5}) annotation (
        extent=[66, 156; 36, 186]);
    SystemDynamics.Gain BR_norm(k=BRN) annotation (extent=[-150, 88; -120, 118
          ]);
    SystemDynamics.Prod_5 Prod_5_1 annotation (extent=[-176, 64; -146, 94]);
    SystemDynamics.Prod_5 Prod_5_2 annotation (extent=[4, 64; 34, 94]);
    SystemDynamics.Funct BRFM(x_vals={0,1,2,3,4,20}, y_vals={0.0,1.0,1.6,1.9,
          2.0,2.0}) annotation (extent=[-190, 20; -160, 50], rotation=90);
    SystemDynamics.Funct BRPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.02,
          0.9,0.7,0.4,0.25,0.15,0.1,0.1}) annotation (extent=[-150, 20; -120, 
          50], rotation=90);
    SystemDynamics.Funct BRCM(x_vals=0:5, y_vals={1.05,1.0,0.9,0.7,0.6,0.55}) 
      annotation (extent=[-110, 20; -80, 50], rotation=90);
    SystemDynamics.Funct DRCM(x_vals=0:5, y_vals={0.9,1.0,1.2,1.5,1.9,3.0}) 
      annotation (extent=[-40, 20; -10, 50], rotation=90);
    SystemDynamics.Funct DRPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={0.92,
          1.3,2.0,3.2,4.8,6.8,9.2,9.2}) annotation (extent=[0, 20; 30, 50], 
        rotation=90);
    SystemDynamics.Funct DRFM(x_vals={0,0.25,0.5,0.75,1,1.25,1.5,1.75,2,20}, 
        y_vals={30.0,3.0,2.0,1.4,1.0,0.7,0.6,0.5,0.5,0.5}) annotation (extent=[
          40, 20; 70, 50], rotation=90);
    SystemDynamics.Gain Crowd_Rat(k=1.0/(Land_Area*Pop_dens_norm)) annotation 
      (extent=[-76, 20; -44, 50], rotation=-90);
    SystemDynamics.Prod_2 Prod_2_1 annotation (extent=[126, 64; 156, 94]);
    SystemDynamics.Funct NRMM(x_vals=0:10, y_vals={0.0,1.0,1.8,2.4,2.9,3.3,3.6
          ,3.8,3.9,3.95,4.0}) annotation (extent=[186, 94; 216, 64], rotation=
          180);
    SystemDynamics.Source Source3 annotation (extent=[40, -50; 60, -30]);
    SystemDynamics.Rate_1 CI_Generation annotation (extent=[60, -60; 100, -20]
      );
    SystemDynamics.Level Capital_Investment(x0=Cap_Invest_0) annotation (
        extent=[110, -60; 150, -20]);
    SystemDynamics.Rate_1 CI_Discard annotation (extent=[160, -60; 200, -20]);
    SystemDynamics.Sink Sink3 annotation (extent=[200, -50; 220, -30]);
    SystemDynamics.Gain CIG_norm(k=CIGN) annotation (extent=[50, -92; 80, -62]
      );
    SystemDynamics.Prod_2 Prod_2_2 annotation (extent=[26, -114; 56, -84]);
    SystemDynamics.Gain CID_norm(k=CIDN) annotation (extent=[150, -92; 180, -
          62]);
    SystemDynamics.Funct CIM(x_vals={0,1,2,3,4,5,20}, y_vals={0.1,1.0,1.8,2.4,
          2.8,3.0,3.0}) annotation (extent=[128, -114; 98, -84]);
    SystemDynamics.Gain MSL(k=1.0/ECIRN) annotation (extent=[262, 16; 292, 46]
      );
    SystemDynamics.Gain NRFR(k=1.0/NRI) annotation (extent=[122, 36; 152, 66])
      ;
    SystemDynamics.Gain ECIR(k=1.0/(1.0 - CIAFN)) annotation (extent=[228, 16
          ; 258, 46]);
    SystemDynamics.Prod_3 Prod_3_1 annotation (extent=[200, 16; 230, 44], 
        rotation=-90);
    SystemDynamics.Funct NREM(x_vals=0:0.25:1, y_vals={0.0,0.15,0.5,0.85,1.0})
       annotation (extent=[190, 66; 160, 36], rotation=180);
    Modelica.Blocks.Math.Division CIR annotation (extent=[122, -10; 92, 20]);
    SystemDynamics.Source Source4 annotation (extent=[-160, -150; -140, -130])
      ;
    SystemDynamics.Rate_1 P_Generation annotation (extent=[-140, -160; -100, -
          120]);
    SystemDynamics.Level Pollution(x0=Pollution_0) annotation (extent=[-90, -
          160; -50, -120]);
    SystemDynamics.Rate_1 P_Absorption annotation (extent=[-40, -160; 0, -120]
      );
    SystemDynamics.Sink Sink4 annotation (extent=[0, -150; 20, -130]);
    SystemDynamics.Linear NotCIAF(m=-1.0, b=1.0) annotation (extent=[174, -4; 
          204, 26]);
    SystemDynamics.Gain Pol_Ratio(k=1.0/POLS) annotation (extent=[-44, -110; -
          14, -80]);
    SystemDynamics.Funct FCM(x_vals=0:5, y_vals={2.4,1.0,0.6,0.4,0.3,0.2}) 
      annotation (extent=[-100, -32; -70, -62], rotation=180);
    SystemDynamics.Prod_3 Prod_3_2 annotation (extent=[-102, -80; -132, -52], 
        rotation=-90);
    SystemDynamics.Gain Food_Ratio(k=FC/FN) annotation (extent=[-134, -80; -
          164, -50]);
    SystemDynamics.Source Source5 annotation (extent=[40, -150; 60, -130]);
    SystemDynamics.Rate_1 CIAFG annotation (extent=[60, -160; 100, -120]);
    SystemDynamics.Level CIAF(x0=CIAF_0) annotation (extent=[110, -160; 150, -
          120]);
    SystemDynamics.Rate_1 CIAFD annotation (extent=[160, -160; 200, -120]);
    SystemDynamics.Sink Sink5 annotation (extent=[200, -150; 220, -130]);
    SystemDynamics.Gain CIAF_D(k=1.0/CIAFT) annotation (extent=[148, -194; 178
          , -164]);
    SystemDynamics.Gain CIAF_G(k=1.0/CIAFT) annotation (extent=[110, -194; 80
          , -164]);
    SystemDynamics.Prod_2 Prod_2_3 annotation (extent=[104, -246; 134, -216]);
    Modelica.Blocks.Math.Division P_Abs annotation (extent=[-58, -194; -28, -
          164]);
    SystemDynamics.Prod_2 Prod_2_4 annotation (extent=[-176, -234; -146, -204]
      );
    SystemDynamics.Funct POLCM(x_vals={0,1,2,3,4,5,100}, y_vals={0.05,1.0,3.0,
          5.4,7.4,8.0,8.0}) annotation (extent=[-132, -204; -102, -234], 
        rotation=180);
    SystemDynamics.Funct POLAT(x_vals={0,10,20,30,40,50,60,120}, y_vals={0.6,
          2.5,5.0,8.0,11.5,15.5,20.0,20.0}) annotation (extent=[-52, -204; -22
          , -234], rotation=180);
    SystemDynamics.Funct CFIFR(x_vals={0,0.5,1,1.5,2,20}, y_vals={1.0,0.6,0.3,
          0.15,0.1,0.1}) annotation (extent=[84, -216; 54, -246], rotation=180)
      ;
    SystemDynamics.Funct FPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.02,
          0.9,0.65,0.35,0.2,0.1,0.05,0.05}) annotation (extent=[-48, -52; -18, 
          -82], rotation=180);
    SystemDynamics.Prod_2 Prod_2_5 annotation (extent=[214, -184; 244, -214]);
    SystemDynamics.Gain CIRA(k=1.0/CIAFN) annotation (extent=[240, -240; 270, 
          -210]);
    SystemDynamics.Funct FPCI(x_vals={0,1,2,3,4,5,6,100}, y_vals={0.5,1.0,1.4,
          1.7,1.9,2.05,2.2,2.2}) annotation (extent=[-138, -92; -168, -122], 
        rotation=180);
    SystemDynamics.Funct CIQR(x_vals={0,0.5,1,1.5,2,10}, y_vals={0.7,0.8,1.0,
          1.5,2.0,2.0}) annotation (extent=[152, -214; 182, -244], rotation=180
      );
    Modelica.Blocks.Math.Division QLMF annotation (extent=[330, -238; 300, -
          208]);
    SystemDynamics.Funct QLM(x_vals={0,1,2,3,4,5,20}, y_vals={0.2,1.0,1.7,2.3,
          2.7,2.9,2.9}) annotation (extent=[334, -154; 304, -184], rotation=180
      );
    SystemDynamics.Funct QLF(x_vals={0,1,2,3,4,20}, y_vals={0.0,1.0,1.8,2.4,
          2.7,2.7}) annotation (extent=[366, -218; 396, -248], rotation=180);
    SystemDynamics.Prod_4 Prod_4_1 annotation (extent=[360, -178; 390, -148], 
        rotation=-90);
    SystemDynamics.Funct QLC(x_vals=0:0.5:5, y_vals={2.0,1.3,1.0,0.75,0.55,
          0.45,0.38,0.3,0.25,0.22,0.2}) annotation (extent=[350, -98; 320, -128
          ], rotation=90);
    SystemDynamics.Funct QLP(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.04,
          0.85,0.6,0.3,0.15,0.05,0.02,0.02}) annotation (extent=[390, -98; 360
          , -128], rotation=90);
    SystemDynamics.Gain Quality_of_Life(k=QLS) annotation (extent=[392, -184; 
          432, -144]);
    Parameter_Change NR_norm annotation (extent=[146, 88; 176, 118]);
    Parameter_Change Pol_norm annotation (extent=[-154, -194; -124, -164]);
    Parameter_Change DR_norm annotation (extent=[14, 86; -16, 116]);
  equation 
    connect(Source1.MassInPort1, Birth_Rate.MassOutPort1) annotation (points
        =[-139, 140; -132, 140], style(color=77));
    connect(Birth_Rate.MassOutPort2, Population.MassInPort1) annotation (
        points=[-108, 140; -94, 140], style(color=77));
    connect(Population.MassInPort2, Death_Rate.MassOutPort1) annotation (
        points=[-46, 140; -32, 140], style(color=77));
    connect(Death_Rate.MassOutPort2, Sink1.MassInPort1) annotation (points=[-
          10, 140; -1, 140], style(color=77));
    connect(Source2.MassInPort1, Generation.MassOutPort1) annotation (points=[
          62, 140; 70, 140], style(color=77));
    connect(Generation.MassOutPort2, Natural_Resources.MassInPort1) 
      annotation (points=[92, 140; 108, 140], style(color=77));
    connect(Natural_Resources.MassInPort2, Depletion.MassOutPort1) annotation 
      (points=[152, 140; 168, 140], style(color=77));
    connect(Depletion.MassOutPort2, Sink2.MassInPort1) annotation (points=[190
          , 140; 198, 140], style(color=77));
    connect(Gen_Const.OutPort1, Generation.InPort1) annotation (points=[80, 
          111.5; 80, 120], style(color=3));
    connect(Birth_Rate.InPort1, BR_norm.OutPort1) annotation (points=[-120, 
          118; -120, 103; -124.5, 103], style(color=3));
    connect(DRMM.OutPort1, Prod_5_2.InPort2) annotation (points=[34.5, 172; 28
          , 172; 28, 104; 40, 104; 40, 80; 29.5, 80], style(color=3));
    connect(Population.OutPort3, Prod_5_2.InPort1) annotation (points=[-56, 
          128; -56, 80; 8.5, 80], style(color=3));
    connect(Prod_5_1.OutPort1, BR_norm.InPort1) annotation (points=[-160, 90; 
          -160, 104; -145.5, 104], style(color=3));
    connect(BRMM.OutPort1, Prod_5_1.InPort1) annotation (points=[-121.5, 186; 
          -184, 186; -184, 80; -172, 80], style(color=3));
    connect(Population.OutPort2, Prod_5_1.InPort2) annotation (points=[-84, 
          126; -84, 80; -150, 80], style(color=3));
    connect(BRPM.OutPort1, Prod_5_1.InPort5) annotation (points=[-136, 51.5; -
          136, 60; -162, 60; -162, 68.5], style(color=3));
    connect(Prod_5_1.InPort4, BRCM.OutPort1) annotation (points=[-154, 70; -96
          , 70; -96, 52], style(color=3));
    connect(DRPM.OutPort1, Prod_5_2.InPort5) annotation (points=[14, 51.5; 14
          , 60; 20, 60; 20, 68], style(color=3));
    connect(Prod_5_1.InPort3, BRFM.OutPort1) annotation (points=[-168, 72; -
          176, 72; -176, 51.5], style(color=3));
    connect(Prod_5_2.InPort3, DRCM.OutPort1) annotation (points=[12, 72; -24, 
          72; -24, 52], style(color=3));
    connect(Prod_5_2.InPort4, DRFM.OutPort1) annotation (points=[26, 72; 54, 
          72; 54, 50], style(color=3));
    connect(Population.OutPort1, Crowd_Rat.InPort1) annotation (points=[-70, 
          126; -70, 68; -60, 68; -60, 45.5], style(color=3));
    connect(Crowd_Rat.OutPort1, BRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -96, 8; -96, 23], style(color=3));
    connect(Crowd_Rat.OutPort1, DRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -24, 8; -24, 23], style(color=3));
    connect(Prod_2_1.InPort1, Population.OutPort4) annotation (points=[130, 80
          ; 100, 80; 100, 194; -51, 194; -51, 152; -52, 152], style(color=3));
    connect(Source3.MassInPort1, CI_Generation.MassOutPort1) annotation (
        points=[62, -40; 68, -40], style(color=77));
    connect(CI_Generation.MassOutPort2, Capital_Investment.MassInPort1) 
      annotation (points=[92, -40; 106, -40], style(color=77));
    connect(Capital_Investment.MassInPort2, CI_Discard.MassOutPort1) 
      annotation (points=[152, -40; 168, -40], style(color=77));
    connect(CI_Discard.MassOutPort2, Sink3.MassInPort1) annotation (points=[
          190, -40; 198, -40], style(color=77));
    connect(CI_Generation.InPort1, CIG_norm.OutPort1) annotation (points=[80, 
          -62; 80, -77; 75.5, -77], style(color=3));
    connect(CIG_norm.InPort1, Prod_2_2.OutPort1) annotation (points=[54, -76; 
          42, -76; 42, -88], style(color=3));
    connect(Prod_2_2.InPort1, Population.OutPort4) annotation (points=[30, -98
          ; 20, -98; 20, -14; 310, -14; 310, 194; -51, 194; -51, 152; -52, 152]
        , style(color=3));
    connect(CI_Discard.InPort1, CID_norm.OutPort1) annotation (points=[180, -
          60; 180, -78; 175.5, -78], style(color=3));
    connect(Capital_Investment.OutPort3, CID_norm.InPort1) annotation (points=
          [144, -52; 144, -76; 156, -76], style(color=3));
    connect(CIM.OutPort1, Prod_2_2.InPort2) annotation (points=[96, -100; 51.5
          , -100], style(color=3));
    connect(NRMM.OutPort1, Prod_2_1.InPort2) annotation (points=[184.5, 80; 
          152, 80], style(color=3));
    connect(Natural_Resources.OutPort2, NRFR.InPort1) annotation (points=[116
          , 126; 116, 51; 126.5, 51], style(color=3));
    connect(NREM.OutPort1, Prod_3_1.InPort1) annotation (points=[192, 52; 214
          , 52; 214, 39.8], style(color=3));
    connect(NRFR.OutPort1, NREM.InPort1) annotation (points=[147.5, 50; 163, 
          50], style(color=3));
    connect(Capital_Investment.OutPort4, CIR.inPort1) annotation (points=[148
          , -30; 148, 14; 128, 14], style(color=3));
    connect(CIR.outPort, Prod_3_1.InPort3) annotation (points=[90, 6; 80, 6; 
          80, 30; 204, 30], style(color=3));
    connect(Prod_3_1.OutPort1, ECIR.InPort1) annotation (points=[225.5, 30; 
          232.5, 30], style(color=3));
    connect(CIR.inPort2, Prod_2_2.InPort1) annotation (points=[126, -4; 140, -
          4; 140, -14; 20, -14; 20, -98; 30, -98], style(color=3));
    connect(Source4.MassInPort1, P_Generation.MassOutPort1) annotation (points
        =[-139, -140; -130, -140], style(color=77));
    connect(P_Generation.MassOutPort2, Pollution.MassInPort1) annotation (
        points=[-110, -140; -92, -140], style(color=77));
    connect(Pollution.MassInPort2, P_Absorption.MassOutPort1) annotation (
        points=[-48, -140; -30, -140], style(color=77));
    connect(P_Absorption.MassOutPort2, Sink4.MassInPort1) annotation (points=[
          -10, -140; -1, -140], style(color=77));
    connect(NotCIAF.OutPort1, Prod_3_1.InPort2) annotation (points=[204, 12; 
          216, 12; 216, 20.2], style(color=3));
    connect(DRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[16, 22; 16, 
          12; 0, 12; 0, -95; -18.5, -95], style(color=3));
    connect(BRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-136, 22; -
          136, -20; 0, -20; 0, -95; -18.5, -95], style(color=3));
    connect(Crowd_Rat.OutPort1, FCM.InPort1) annotation (points=[-60, 24; -60
          , -48; -72, -48], style(color=3));
    connect(FCM.OutPort1, Prod_3_2.InPort1) annotation (points=[-102, -46; -
          117, -46; -117, -56], style(color=3));
    connect(Food_Ratio.InPort1, Prod_3_2.OutPort1) annotation (points=[-138.5
          , -66; -127.5, -66], style(color=3));
    connect(Food_Ratio.OutPort1, BRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 22], style(color=3));
    connect(Food_Ratio.OutPort1, DRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 0; 54, 0; 54, 22], style(color=3));
    connect(Pollution.OutPort4, Pol_Ratio.InPort1) annotation (points=[-54, -
          129; -54, -95; -39.5, -95], style(color=3));
    connect(Source5.MassInPort1, CIAFG.MassOutPort1) annotation (points=[60, -
          140; 68, -140], style(color=77));
    connect(CIAFG.MassOutPort2, CIAF.MassInPort1) annotation (points=[90, -140
          ; 106, -140], style(color=77));
    connect(CIAF.MassInPort2, CIAFD.MassOutPort1) annotation (points=[152, -
          140; 168, -140], style(color=77));
    connect(CIAFD.MassOutPort2, Sink5.MassInPort1) annotation (points=[190, -
          140; 198, -140], style(color=77));
    connect(CIAF.OutPort4, NotCIAF.InPort1) annotation (points=[146, -128; 146
          , -106; 246, -106; 246, -8; 168, -8; 168, 10; 176, 10], style(color=3
        ));
    connect(CIAF_D.OutPort1, CIAFD.InPort1) annotation (points=[173.5, -180; 
          180, -180; 180, -161], style(color=3));
    connect(CIAF.OutPort3, CIAF_D.InPort1) annotation (points=[144, -154; 144
          , -180; 152.5, -180], style(color=3));
    connect(P_Abs.outPort, P_Absorption.InPort1) annotation (points=[-26.5, -
          180; -20, -180; -20, -161], style(color=3));
    connect(Pollution.OutPort1, P_Abs.inPort1) annotation (points=[-70, -154; 
          -70, -170; -64, -170], style(color=3));
    connect(Population.OutPort5, Prod_2_4.InPort1) annotation (points=[-86, 
          152; -96, 152; -96, 166; -198, 166; -198, -219; -171.5, -219], style(
          color=3));
    connect(POLCM.OutPort1, Prod_2_4.InPort2) annotation (points=[-133.5, -220
          ; -150.5, -220], style(color=3));
    connect(CIR.outPort, POLCM.InPort1) annotation (points=[90, 6; 80, 6; 80, 
          -6; 10, -6; 10, -116; -102.5, -116; -102.5, -218; -105, -218], style(
          color=3));
    connect(POLAT.OutPort1, P_Abs.inPort2) annotation (points=[-53.5, -220; -
          74, -220; -74, -188; -64, -188], style(color=3));
    connect(CIAFG.InPort1, CIAF_G.OutPort1) annotation (points=[80, -162; 80, 
          -180; 84.5, -180], style(color=3));
    connect(Prod_2_3.OutPort1, CIAF_G.InPort1) annotation (points=[120, -220.5
          ; 120, -180; 105.5, -180], style(color=3));
    connect(CFIFR.OutPort1, Prod_2_3.InPort1) annotation (points=[86, -231; 
          108.5, -231], style(color=3));
    connect(FPM.OutPort1, Prod_3_2.InPort3) annotation (points=[-50, -66; -
          106.5, -66], style(color=3));
    connect(CIAF.OutPort1, Prod_2_5.InPort1) annotation (points=[130, -154; 
          130, -200; 218.5, -200], style(color=3));
    connect(Prod_2_5.InPort2, Prod_3_1.InPort3) annotation (points=[239.5, -
          198; 256, -198; 256, -12; 160, -12; 160, 30; 204.5, 30], style(color=
            3));
    connect(Prod_2_5.OutPort1, CIRA.InPort1) annotation (points=[228, -208; 
          228, -226; 244, -226], style(color=3));
    connect(FPCI.OutPort1, Prod_3_2.InPort2) annotation (points=[-136, -106; -
          118, -106; -118, -75.8], style(color=3));
    connect(CIRA.OutPort1, FPCI.InPort1) annotation (points=[266, -224; 280, -
          224; 280, -250; -210, -250; -210, -106; -165, -106], style(color=3));
    connect(Prod_2_3.InPort2, CIQR.OutPort1) annotation (points=[129.5, -230; 
          150.5, -230], style(color=3));
    connect(QLMF.outPort, CIQR.InPort1) annotation (points=[300, -223; 290, -
          223; 290, -256; 200, -256; 200, -230; 180, -230], style(color=3));
    connect(FPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-20, -66; 0, 
          -66; 0, -96; -18, -96], style(color=3));
    connect(POLAT.InPort1, FPM.InPort1) annotation (points=[-24, -218; 0, -218
          ; 0, -66; -21, -66], style(color=3));
    connect(QLF.OutPort1, QLMF.inPort2) annotation (points=[364.5, -232; 336, 
          -232], style(color=3));
    connect(Food_Ratio.OutPort1, QLF.InPort1) annotation (points=[-160, -64; -
          220, -64; -220, -260; 400, -260; 400, -232; 393, -232], style(color=3
        ));
    connect(CFIFR.InPort1, QLF.InPort1) annotation (points=[56, -230; 40, -230
          ; 40, -260; 400, -260; 400, -232; 393, -232], style(color=3));
    connect(QLM.OutPort1, QLMF.inPort1) annotation (points=[335.5, -168; 344, 
          -168; 344, -214; 334, -214], style(color=3));
    connect(MSL.OutPort1, QLM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, -170; 306, -170], style(color=3));
    connect(MSL.OutPort1, NRMM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, 80; 214, 80], style(color=3));
    connect(ECIR.OutPort1, MSL.InPort1) annotation (points=[254, 32; 266.5, 32
          ], style(color=3));
    connect(CIM.InPort1, QLM.InPort1) annotation (points=[126, -100; 300, -100
          ; 300, -170; 308, -170], style(color=3));
    connect(DRMM.InPort1, NRMM.InPort1) annotation (points=[64, 170; 300, 170
          ; 300, 80; 213, 80], style(color=3));
    connect(BRMM.InPort1, NRMM.InPort1) annotation (points=[-92, 186; -58, 186
          ; -58, 202; 300, 202; 300, 80; 212, 80], style(color=3));
    connect(Prod_4_1.InPort4, QLM.OutPort1) annotation (points=[366, -168; 
          335.5, -168], style(color=3));
    connect(Prod_4_1.InPort2, QLF.OutPort1) annotation (points=[376, -174; 376
          , -200; 356, -200; 356, -232; 364.5, -232], style(color=3));
    connect(QLP.OutPort1, Prod_4_1.InPort1) annotation (points=[374, -130; 374
          , -152], style(color=3));
    connect(Prod_4_1.InPort3, QLC.OutPort1) annotation (points=[366, -158; 334
          , -158; 334, -129.5], style(color=3));
    connect(QLC.InPort1, Crowd_Rat.OutPort1) annotation (points=[334, -100; 
          334, 208; -208, 208; -208, 8; -60, 8; -60, 24], style(color=3));
    connect(QLP.InPort1, BRPM.InPort1) annotation (points=[376, -100; 376, 214
          ; -216, 214; -216, -20; -136, -20; -136, 23], style(color=3));
    connect(Prod_4_1.OutPort1, Quality_of_Life.InPort1) annotation (points=[
          386, -164; 400, -164], style(color=3));
    connect(Depletion.InPort1, NR_norm.OutPort1) annotation (points=[180, 118
          ; 180, 104; 174, 104], style(color=3));
    connect(Prod_2_1.OutPort1, NR_norm.InPort1) annotation (points=[140, 90; 
          140, 108; 146, 108], style(color=3));
    connect(P_Generation.InPort1, Pol_norm.OutPort1) annotation (points=[-120
          , -162; -120, -178; -125.5, -178], style(color=3));
    connect(Prod_2_4.OutPort1, Pol_norm.InPort1) annotation (points=[-160, -
          208; -160, -174; -154, -174], style(color=3));
    connect(Death_Rate.InPort1, DR_norm.OutPort1) annotation (points=[-20, 118
          ; -20, 100; -14.5, 100], style(color=3));
    connect(Prod_5_2.OutPort1, DR_norm.InPort1) annotation (points=[20, 90; 20
          , 106; 14, 106], style(color=3));
    years = time + 1900;
    
    // Parameter equations
    NRUN = if years > 1970 then 0.25 else 1.0;
    NR_norm.InPort2.signal[1] = NRUN;
    POLN = if years > 1970 then 0.25 else 1.0;
    Pol_norm.InPort2.signal[1] = POLN;
    DRN = if years > 1970 then 0.02 else 0.028;
    DR_norm.InPort2.signal[1] = DRN;
    
    // Measurement data
    Pop_meas = SystemDynamics.Interfaces.Piecewise(x=years, x_grid=time_vals, 
      y_grid=pop_vals);
  end World_4;
  block Reverse_Level 
    parameter Real x0 "Initial condition";
    parameter Real xm=0.0 "Minimum level";
    output Real level(start=x0) "Reversible state variable";
    output Real rate_in "Inflow";
    output Real rate_out "Outflow";
    input Real dir "Direction of time flow";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Rectangle(extent=[-80, 20; 80, -60], style(fillColor=45)), 
        Rectangle(extent=[-80, 60; 80, -60], style(color=77, thickness=2)), 
        Line(points=[-100, 0; -80, 0], style(color=77, thickness=2)), 
        Line(points=[80, 0; 100, 0], style(color=77, thickness=2))), 
      Icon(
        Rectangle(extent=[-80, 20; 80, -60], style(fillColor=45)), 
        Rectangle(extent=[-80, 60; 80, -60], style(color=77, thickness=2)), 
        Line(points=[-100, 0; -80, 0], style(color=77, thickness=2)), 
        Line(points=[80, 0; 100, 0], style(color=77, thickness=2)), 
        Text(extent=[-100, 100; 100, 70], string="%name"), 
        Text(extent=[-80, 20; 80, -6], string="x0 = %x0")));
    SystemDynamics.Interfaces.MassInPort MassInPort1 annotation (extent=[-120
          , -10; -100, 10]);
    SystemDynamics.Interfaces.MassInPort MassInPort2 annotation (extent=[100, 
          -10; 120, 10]);
    SystemDynamics.Interfaces.OutPort OutPort1 annotation (extent=[-4, -70; 6
          , -60], rotation=-90);
    SystemDynamics.Interfaces.OutPort OutPort2 annotation (extent=[-80, -70; -
          70, -60], rotation=-90);
    SystemDynamics.Interfaces.OutPort OutPort3 annotation (extent=[70, -70; 80
          , -60], rotation=-90);
    SystemDynamics.Interfaces.OutPort OutPort4 annotation (extent=[90, 60; 80
          , 50], rotation=-180);
    SystemDynamics.Interfaces.OutPort OutPort5 annotation (extent=[-80, 60; -
          90, 50]);
  equation 
    rate_in = MassInPort1.signal[1];
    rate_out = MassInPort2.signal[1];
    OutPort1.signal[1] = level;
    OutPort2.signal[1] = level;
    OutPort3.signal[1] = level;
    OutPort4.signal[1] = level;
    OutPort5.signal[1] = level;
    der(level) = if level > xm then dir*(rate_in - rate_out) else 0;
  end Reverse_Level;
  model World_5 
    // Initial conditions for state variables
    parameter Real Population_0=1.65e9 "World population in 1900";
    parameter Real Pollution_0=2.0e8 "Pollution in 1900";
    parameter Real Nat_Resources_0=9.0e11 
      "Unrecoverable natural resources in 1900";
    parameter Real Cap_Invest_0=4.0e8 "Capital investment in 1900";
    parameter Real CIAF_0=0.2 
      "Proportion of capital investment in agriculture in 1900";
    
    // Parameters
    parameter Real BRN=0.04 "Normal birth rate";
    parameter Real CIAFN=0.3 "CIAF normalization";
    parameter Real CIAFT=15.0 "CIAF time constant";
    parameter Real CIDN=0.025 "Normal capital discard";
    parameter Real CIGN=0.05 "Normal capital generation";
    parameter Real ECIRN=1.0 "Capital normalization";
    parameter Real FC=1.0 "Food coefficient";
    parameter Real FN=1.0 "Food normalization";
    parameter Real Land_Area=1.35e8 "Area of arable land";
    parameter Real NRI=9.0e11 "Initial natural resources";
    parameter Real POLS=3.5999e9 "Standard pollution";
    parameter Real Pop_dens_norm=26.5 "Normal population density";
    parameter Real QLS=1.0 "Standard quality of life";
    
    // Output variables
    Real years(start=1900) "Time in calendar years";
    
    // Manipulated parameters
    Real NRUN "Normal resource utilization";
    Real POLN "Normal pollution";
    Real DRN "Normal death rate";
    
    // Measurement data
    Real time_vals[12]={1900,1910,1920,1930,1940,1950,1960,1970,1980,1990,2000
        ,2500};
    Real pop_vals[12]={1.6e9,1.75e9,1.86e9,2.07e9,2.3e9,2.556e9,3.0395e9,
        3.70656e9,4.3781e9,5.1903e9,5.9962e9,6e9};
    Real Pop_meas;
    
    // Reversible time flow
    Real dir;
    parameter Real time_reverse=200 "Time of time reversal";
    annotation (
      Coordsys(
        extent=[-220, -260; 440, 220], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Ellipse(extent=[-62, 10; -58, 6], style(fillColor=73)), 
        Ellipse(extent=[98, 196; 102, 192], style(fillColor=73)), 
        Ellipse(extent=[138, -12; 142, -16], style(fillColor=73)), 
        Ellipse(extent=[-2, -18; 2, -22], style(fillColor=73)), 
        Ellipse(extent=[-178, 2; -174, -2], style(fillColor=73)), 
        Ellipse(extent=[78, 8; 82, 4], style(fillColor=73)), 
        Ellipse(extent=[-2, -92; 2, -96], style(fillColor=73)), 
        Ellipse(extent=[-178, -62; -174, -66], style(fillColor=73)), 
        Ellipse(extent=[-2, -64; 2, -68], style(fillColor=73)), 
        Ellipse(extent=[158, 32; 162, 28], style(fillColor=73)), 
        Ellipse(extent=[38, -258; 42, -262], style(fillColor=73)), 
        Ellipse(extent=[298, -98; 302, -102], style(fillColor=73)), 
        Ellipse(extent=[298, 32; 302, 28], style(fillColor=73)), 
        Ellipse(extent=[298, 82; 302, 78], style(fillColor=73)), 
        Ellipse(extent=[298, 172; 302, 168], style(fillColor=73)), 
        Ellipse(extent=[342, -166; 346, -170], style(fillColor=73)), 
        Ellipse(extent=[354, -230; 358, -234], style(fillColor=74)), 
        Ellipse(extent=[-98, 10; -94, 6], style(fillColor=73)), 
        Ellipse(extent=[-138, -18; -134, -22], style(fillColor=73))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65), 
      Icon(
        Rectangle(extent=[-220, 222; 442, -262], style(fillColor=49)), 
        Text(extent=[-152, 142; 368, 72], string="4th Modification"), 
        Text(
          extent=[-148, 46; 372, -24], 
          string="Simulation", 
          style(color=73)), 
        Text(
          extent=[-150, -34; 370, -104], 
          string="made", 
          style(color=73)), 
        Text(
          extent=[-150, -120; 370, -190], 
          string="reversible in time", 
          style(color=73))));
    SystemDynamics.Source Source1 annotation (extent=[-160, 130; -140, 150]);
    SystemDynamics.Rate_1 Birth_Rate annotation (extent=[-140, 120; -100, 160]
      );
    SystemDynamics.Rate_1 Death_Rate annotation (extent=[-40, 120; 0, 160]);
    SystemDynamics.Sink Sink1 annotation (extent=[0, 130; 20, 150]);
    SystemDynamics.Source Source2 annotation (extent=[40, 130; 60, 150]);
    SystemDynamics.Rate_1 Generation annotation (extent=[60, 120; 100, 160]);
    SystemDynamics.Rate_1 Depletion annotation (extent=[160, 120; 200, 160]);
    SystemDynamics.Sink Sink2 annotation (extent=[200, 130; 220, 150]);
    SystemDynamics.Const Gen_Const(k=0) annotation (extent=[64, 86; 96, 116]);
    SystemDynamics.Funct BRMM(x_vals={0,1,2,3,4,5,20}, y_vals={1.2,1.0,0.85,
          0.75,0.7,0.7,0.7}) annotation (extent=[-90, 170; -120, 200]);
    SystemDynamics.Funct DRMM(x_vals={0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,20}, 
        y_vals={3.0,1.8,1.0,0.8,0.7,0.6,0.53,0.5,0.5,0.5,0.5,0.5}) annotation (
        extent=[66, 156; 36, 186]);
    SystemDynamics.Gain BR_norm(k=BRN) annotation (extent=[-150, 88; -120, 118
          ]);
    SystemDynamics.Prod_5 Prod_5_1 annotation (extent=[-176, 64; -146, 94]);
    SystemDynamics.Prod_5 Prod_5_2 annotation (extent=[4, 64; 34, 94]);
    SystemDynamics.Funct BRFM(x_vals={0,1,2,3,4,20}, y_vals={0.0,1.0,1.6,1.9,
          2.0,2.0}) annotation (extent=[-190, 20; -160, 50], rotation=90);
    SystemDynamics.Funct BRPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.02,
          0.9,0.7,0.4,0.25,0.15,0.1,0.1}) annotation (extent=[-150, 20; -120, 
          50], rotation=90);
    SystemDynamics.Funct BRCM(x_vals=0:5, y_vals={1.05,1.0,0.9,0.7,0.6,0.55}) 
      annotation (extent=[-110, 20; -80, 50], rotation=90);
    SystemDynamics.Funct DRCM(x_vals=0:5, y_vals={0.9,1.0,1.2,1.5,1.9,3.0}) 
      annotation (extent=[-40, 20; -10, 50], rotation=90);
    SystemDynamics.Funct DRPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={0.92,
          1.3,2.0,3.2,4.8,6.8,9.2,9.2}) annotation (extent=[0, 20; 30, 50], 
        rotation=90);
    SystemDynamics.Funct DRFM(x_vals={0,0.25,0.5,0.75,1,1.25,1.5,1.75,2,20}, 
        y_vals={30.0,3.0,2.0,1.4,1.0,0.7,0.6,0.5,0.5,0.5}) annotation (extent=[
          40, 20; 70, 50], rotation=90);
    SystemDynamics.Gain Crowd_Rat(k=1.0/(Land_Area*Pop_dens_norm)) annotation 
      (extent=[-76, 20; -44, 50], rotation=-90);
    SystemDynamics.Prod_2 Prod_2_1 annotation (extent=[126, 64; 156, 94]);
    SystemDynamics.Funct NRMM(x_vals=0:10, y_vals={0.0,1.0,1.8,2.4,2.9,3.3,3.6
          ,3.8,3.9,3.95,4.0}) annotation (extent=[186, 94; 216, 64], rotation=
          180);
    SystemDynamics.Source Source3 annotation (extent=[40, -50; 60, -30]);
    SystemDynamics.Rate_1 CI_Generation annotation (extent=[60, -60; 100, -20]
      );
    SystemDynamics.Rate_1 CI_Discard annotation (extent=[160, -60; 200, -20]);
    SystemDynamics.Sink Sink3 annotation (extent=[200, -50; 220, -30]);
    SystemDynamics.Gain CIG_norm(k=CIGN) annotation (extent=[50, -92; 80, -62]
      );
    SystemDynamics.Prod_2 Prod_2_2 annotation (extent=[26, -114; 56, -84]);
    SystemDynamics.Gain CID_norm(k=CIDN) annotation (extent=[150, -92; 180, -
          62]);
    SystemDynamics.Funct CIM(x_vals={0,1,2,3,4,5,20}, y_vals={0.1,1.0,1.8,2.4,
          2.8,3.0,3.0}) annotation (extent=[128, -114; 98, -84]);
    SystemDynamics.Gain MSL(k=1.0/ECIRN) annotation (extent=[262, 16; 292, 46]
      );
    SystemDynamics.Gain NRFR(k=1.0/NRI) annotation (extent=[122, 36; 152, 66])
      ;
    SystemDynamics.Gain ECIR(k=1.0/(1.0 - CIAFN)) annotation (extent=[228, 16
          ; 258, 46]);
    SystemDynamics.Prod_3 Prod_3_1 annotation (extent=[200, 16; 230, 44], 
        rotation=-90);
    SystemDynamics.Funct NREM(x_vals=0:0.25:1, y_vals={0.0,0.15,0.5,0.85,1.0})
       annotation (extent=[190, 66; 160, 36], rotation=180);
    Modelica.Blocks.Math.Division CIR annotation (extent=[122, -10; 92, 20]);
    SystemDynamics.Source Source4 annotation (extent=[-160, -150; -140, -130])
      ;
    SystemDynamics.Rate_1 P_Generation annotation (extent=[-140, -160; -100, -
          120]);
    SystemDynamics.Rate_1 P_Absorption annotation (extent=[-40, -160; 0, -120]
      );
    SystemDynamics.Sink Sink4 annotation (extent=[0, -150; 20, -130]);
    SystemDynamics.Linear NotCIAF(m=-1.0, b=1.0) annotation (extent=[174, -4; 
          204, 26]);
    SystemDynamics.Gain Pol_Ratio(k=1.0/POLS) annotation (extent=[-44, -110; -
          14, -80]);
    SystemDynamics.Funct FCM(x_vals=0:5, y_vals={2.4,1.0,0.6,0.4,0.3,0.2}) 
      annotation (extent=[-100, -32; -70, -62], rotation=180);
    SystemDynamics.Prod_3 Prod_3_2 annotation (extent=[-102, -80; -132, -52], 
        rotation=-90);
    SystemDynamics.Gain Food_Ratio(k=FC/FN) annotation (extent=[-134, -80; -
          164, -50]);
    SystemDynamics.Source Source5 annotation (extent=[40, -150; 60, -130]);
    SystemDynamics.Rate_1 CIAFG annotation (extent=[60, -160; 100, -120]);
    SystemDynamics.Rate_1 CIAFD annotation (extent=[160, -160; 200, -120]);
    SystemDynamics.Sink Sink5 annotation (extent=[200, -150; 220, -130]);
    SystemDynamics.Gain CIAF_D(k=1.0/CIAFT) annotation (extent=[148, -194; 178
          , -164]);
    SystemDynamics.Gain CIAF_G(k=1.0/CIAFT) annotation (extent=[110, -194; 80
          , -164]);
    SystemDynamics.Prod_2 Prod_2_3 annotation (extent=[104, -246; 134, -216]);
    Modelica.Blocks.Math.Division P_Abs annotation (extent=[-58, -194; -28, -
          164]);
    SystemDynamics.Prod_2 Prod_2_4 annotation (extent=[-176, -234; -146, -204]
      );
    SystemDynamics.Funct POLCM(x_vals={0,1,2,3,4,5,100}, y_vals={0.05,1.0,3.0,
          5.4,7.4,8.0,8.0}) annotation (extent=[-132, -204; -102, -234], 
        rotation=180);
    SystemDynamics.Funct POLAT(x_vals={0,10,20,30,40,50,60,120}, y_vals={0.6,
          2.5,5.0,8.0,11.5,15.5,20.0,20.0}) annotation (extent=[-52, -204; -22
          , -234], rotation=180);
    SystemDynamics.Funct CFIFR(x_vals={0,0.5,1,1.5,2,20}, y_vals={1.0,0.6,0.3,
          0.15,0.1,0.1}) annotation (extent=[84, -216; 54, -246], rotation=180)
      ;
    SystemDynamics.Funct FPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.02,
          0.9,0.65,0.35,0.2,0.1,0.05,0.05}) annotation (extent=[-48, -52; -18, 
          -82], rotation=180);
    SystemDynamics.Prod_2 Prod_2_5 annotation (extent=[214, -184; 244, -214]);
    SystemDynamics.Gain CIRA(k=1.0/CIAFN) annotation (extent=[240, -240; 270, 
          -210]);
    SystemDynamics.Funct FPCI(x_vals={0,1,2,3,4,5,6,100}, y_vals={0.5,1.0,1.4,
          1.7,1.9,2.05,2.2,2.2}) annotation (extent=[-138, -92; -168, -122], 
        rotation=180);
    SystemDynamics.Funct CIQR(x_vals={0,0.5,1,1.5,2,10}, y_vals={0.7,0.8,1.0,
          1.5,2.0,2.0}) annotation (extent=[152, -214; 182, -244], rotation=180
      );
    Modelica.Blocks.Math.Division QLMF annotation (extent=[330, -238; 300, -
          208]);
    SystemDynamics.Funct QLM(x_vals={0,1,2,3,4,5,20}, y_vals={0.2,1.0,1.7,2.3,
          2.7,2.9,2.9}) annotation (extent=[334, -154; 304, -184], rotation=180
      );
    SystemDynamics.Funct QLF(x_vals={0,1,2,3,4,20}, y_vals={0.0,1.0,1.8,2.4,
          2.7,2.7}) annotation (extent=[366, -218; 396, -248], rotation=180);
    SystemDynamics.Prod_4 Prod_4_1 annotation (extent=[360, -178; 390, -148], 
        rotation=-90);
    SystemDynamics.Funct QLC(x_vals=0:0.5:5, y_vals={2.0,1.3,1.0,0.75,0.55,
          0.45,0.38,0.3,0.25,0.22,0.2}) annotation (extent=[350, -98; 320, -128
          ], rotation=90);
    SystemDynamics.Funct QLP(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.04,
          0.85,0.6,0.3,0.15,0.05,0.02,0.02}) annotation (extent=[390, -98; 360
          , -128], rotation=90);
    SystemDynamics.Gain Quality_of_Life(k=QLS) annotation (extent=[392, -184; 
          432, -144]);
    Parameter_Change NR_norm annotation (extent=[146, 88; 176, 118]);
    Parameter_Change Pol_norm annotation (extent=[-154, -194; -124, -164]);
    Parameter_Change DR_norm annotation (extent=[14, 86; -16, 116]);
    Reverse_Level Natural_Resources(x0=Nat_Resources_0) annotation (extent=[
          110, 120; 150, 160]);
    Reverse_Level Population(x0=Population_0) annotation (extent=[-90, 120; -
          50, 160]);
    Reverse_Level Pollution(x0=Pollution_0) annotation (extent=[-90, -160; -50
          , -120]);
    Reverse_Level Capital_Investment(x0=Cap_Invest_0) annotation (extent=[110
          , -60; 150, -20]);
    Reverse_Level CIAF(x0=CIAF_0) annotation (extent=[110, -160; 150, -120]);
  equation 
    connect(Source1.MassInPort1, Birth_Rate.MassOutPort1) annotation (points
        =[-139, 140; -132, 140], style(color=77));
    connect(Death_Rate.MassOutPort2, Sink1.MassInPort1) annotation (points=[-
          10, 140; -1, 140], style(color=77));
    connect(Source2.MassInPort1, Generation.MassOutPort1) annotation (points=[
          62, 140; 70, 140], style(color=77));
    connect(Depletion.MassOutPort2, Sink2.MassInPort1) annotation (points=[190
          , 140; 198, 140], style(color=77));
    connect(Gen_Const.OutPort1, Generation.InPort1) annotation (points=[80, 
          111.5; 80, 120], style(color=3));
    connect(Birth_Rate.InPort1, BR_norm.OutPort1) annotation (points=[-120, 
          118; -120, 103; -124.5, 103], style(color=3));
    connect(DRMM.OutPort1, Prod_5_2.InPort2) annotation (points=[34.5, 172; 28
          , 172; 28, 104; 40, 104; 40, 80; 29.5, 80], style(color=3));
    connect(Prod_5_1.OutPort1, BR_norm.InPort1) annotation (points=[-160, 90; 
          -160, 104; -145.5, 104], style(color=3));
    connect(BRMM.OutPort1, Prod_5_1.InPort1) annotation (points=[-121.5, 186; 
          -184, 186; -184, 80; -172, 80], style(color=3));
    connect(BRPM.OutPort1, Prod_5_1.InPort5) annotation (points=[-136, 51.5; -
          136, 60; -162, 60; -162, 68.5], style(color=3));
    connect(Prod_5_1.InPort4, BRCM.OutPort1) annotation (points=[-154, 70; -96
          , 70; -96, 52], style(color=3));
    connect(DRPM.OutPort1, Prod_5_2.InPort5) annotation (points=[14, 51.5; 14
          , 60; 20, 60; 20, 68], style(color=3));
    connect(Prod_5_1.InPort3, BRFM.OutPort1) annotation (points=[-168, 72; -
          176, 72; -176, 51.5], style(color=3));
    connect(Prod_5_2.InPort3, DRCM.OutPort1) annotation (points=[12, 72; -24, 
          72; -24, 52], style(color=3));
    connect(Prod_5_2.InPort4, DRFM.OutPort1) annotation (points=[26, 72; 54, 
          72; 54, 50], style(color=3));
    connect(Crowd_Rat.OutPort1, BRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -96, 8; -96, 23], style(color=3));
    connect(Crowd_Rat.OutPort1, DRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -24, 8; -24, 23], style(color=3));
    connect(Source3.MassInPort1, CI_Generation.MassOutPort1) annotation (
        points=[62, -40; 68, -40], style(color=77));
    connect(CI_Discard.MassOutPort2, Sink3.MassInPort1) annotation (points=[
          190, -40; 198, -40], style(color=77));
    connect(CI_Generation.InPort1, CIG_norm.OutPort1) annotation (points=[80, 
          -62; 80, -77; 75.5, -77], style(color=3));
    connect(CIG_norm.InPort1, Prod_2_2.OutPort1) annotation (points=[54, -76; 
          42, -76; 42, -88], style(color=3));
    connect(CI_Discard.InPort1, CID_norm.OutPort1) annotation (points=[180, -
          60; 180, -78; 175.5, -78], style(color=3));
    connect(CIM.OutPort1, Prod_2_2.InPort2) annotation (points=[96, -100; 51.5
          , -100], style(color=3));
    connect(NRMM.OutPort1, Prod_2_1.InPort2) annotation (points=[184.5, 80; 
          152, 80], style(color=3));
    connect(NREM.OutPort1, Prod_3_1.InPort1) annotation (points=[192, 52; 214
          , 52; 214, 39.8], style(color=3));
    connect(NRFR.OutPort1, NREM.InPort1) annotation (points=[147.5, 50; 163, 
          50], style(color=3));
    connect(CIR.outPort, Prod_3_1.InPort3) annotation (points=[90, 6; 80, 6; 
          80, 30; 204, 30], style(color=3));
    connect(Prod_3_1.OutPort1, ECIR.InPort1) annotation (points=[225.5, 30; 
          232.5, 30], style(color=3));
    connect(CIR.inPort2, Prod_2_2.InPort1) annotation (points=[126, -4; 140, -
          4; 140, -14; 20, -14; 20, -98; 30, -98], style(color=3));
    connect(Source4.MassInPort1, P_Generation.MassOutPort1) annotation (points
        =[-139, -140; -130, -140], style(color=77));
    connect(P_Absorption.MassOutPort2, Sink4.MassInPort1) annotation (points=[
          -10, -140; -1, -140], style(color=77));
    connect(NotCIAF.OutPort1, Prod_3_1.InPort2) annotation (points=[204, 12; 
          216, 12; 216, 20.2], style(color=3));
    connect(DRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[16, 22; 16, 
          12; 0, 12; 0, -95; -18.5, -95], style(color=3));
    connect(BRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-136, 22; -
          136, -20; 0, -20; 0, -95; -18.5, -95], style(color=3));
    connect(Crowd_Rat.OutPort1, FCM.InPort1) annotation (points=[-60, 24; -60
          , -48; -72, -48], style(color=3));
    connect(FCM.OutPort1, Prod_3_2.InPort1) annotation (points=[-102, -46; -
          117, -46; -117, -56], style(color=3));
    connect(Food_Ratio.InPort1, Prod_3_2.OutPort1) annotation (points=[-138.5
          , -66; -127.5, -66], style(color=3));
    connect(Food_Ratio.OutPort1, BRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 22], style(color=3));
    connect(Food_Ratio.OutPort1, DRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 0; 54, 0; 54, 22], style(color=3));
    connect(Source5.MassInPort1, CIAFG.MassOutPort1) annotation (points=[60, -
          140; 68, -140], style(color=77));
    connect(CIAFD.MassOutPort2, Sink5.MassInPort1) annotation (points=[190, -
          140; 198, -140], style(color=77));
    connect(CIAF_D.OutPort1, CIAFD.InPort1) annotation (points=[173.5, -180; 
          180, -180; 180, -161], style(color=3));
    connect(P_Abs.outPort, P_Absorption.InPort1) annotation (points=[-26.5, -
          180; -20, -180; -20, -161], style(color=3));
    connect(POLCM.OutPort1, Prod_2_4.InPort2) annotation (points=[-133.5, -220
          ; -150.5, -220], style(color=3));
    connect(CIR.outPort, POLCM.InPort1) annotation (points=[90, 6; 80, 6; 80, 
          -6; 10, -6; 10, -116; -102.5, -116; -102.5, -218; -105, -218], style(
          color=3));
    connect(POLAT.OutPort1, P_Abs.inPort2) annotation (points=[-53.5, -220; -
          74, -220; -74, -188; -64, -188], style(color=3));
    connect(CIAFG.InPort1, CIAF_G.OutPort1) annotation (points=[80, -162; 80, 
          -180; 84.5, -180], style(color=3));
    connect(Prod_2_3.OutPort1, CIAF_G.InPort1) annotation (points=[120, -220.5
          ; 120, -180; 105.5, -180], style(color=3));
    connect(CFIFR.OutPort1, Prod_2_3.InPort1) annotation (points=[86, -231; 
          108.5, -231], style(color=3));
    connect(FPM.OutPort1, Prod_3_2.InPort3) annotation (points=[-50, -66; -
          106.5, -66], style(color=3));
    connect(Prod_2_5.InPort2, Prod_3_1.InPort3) annotation (points=[239.5, -
          198; 256, -198; 256, -12; 160, -12; 160, 30; 204.5, 30], style(color=
            3));
    connect(Prod_2_5.OutPort1, CIRA.InPort1) annotation (points=[228, -208; 
          228, -226; 244, -226], style(color=3));
    connect(FPCI.OutPort1, Prod_3_2.InPort2) annotation (points=[-136, -106; -
          118, -106; -118, -75.8], style(color=3));
    connect(CIRA.OutPort1, FPCI.InPort1) annotation (points=[266, -224; 280, -
          224; 280, -250; -210, -250; -210, -106; -165, -106], style(color=3));
    connect(Prod_2_3.InPort2, CIQR.OutPort1) annotation (points=[129.5, -230; 
          150.5, -230], style(color=3));
    connect(QLMF.outPort, CIQR.InPort1) annotation (points=[300, -223; 290, -
          223; 290, -256; 200, -256; 200, -230; 180, -230], style(color=3));
    connect(FPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-20, -66; 0, 
          -66; 0, -96; -18, -96], style(color=3));
    connect(POLAT.InPort1, FPM.InPort1) annotation (points=[-24, -218; 0, -218
          ; 0, -66; -21, -66], style(color=3));
    connect(QLF.OutPort1, QLMF.inPort2) annotation (points=[364.5, -232; 336, 
          -232], style(color=3));
    connect(Food_Ratio.OutPort1, QLF.InPort1) annotation (points=[-160, -64; -
          220, -64; -220, -260; 400, -260; 400, -232; 393, -232], style(color=3
        ));
    connect(CFIFR.InPort1, QLF.InPort1) annotation (points=[56, -230; 40, -230
          ; 40, -260; 400, -260; 400, -232; 393, -232], style(color=3));
    connect(QLM.OutPort1, QLMF.inPort1) annotation (points=[335.5, -168; 344, 
          -168; 344, -214; 334, -214], style(color=3));
    connect(MSL.OutPort1, QLM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, -170; 306, -170], style(color=3));
    connect(MSL.OutPort1, NRMM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, 80; 214, 80], style(color=3));
    connect(ECIR.OutPort1, MSL.InPort1) annotation (points=[254, 32; 266.5, 32
          ], style(color=3));
    connect(CIM.InPort1, QLM.InPort1) annotation (points=[126, -100; 300, -100
          ; 300, -170; 308, -170], style(color=3));
    connect(DRMM.InPort1, NRMM.InPort1) annotation (points=[64, 170; 300, 170
          ; 300, 80; 213, 80], style(color=3));
    connect(BRMM.InPort1, NRMM.InPort1) annotation (points=[-92, 186; -58, 186
          ; -58, 202; 300, 202; 300, 80; 212, 80], style(color=3));
    connect(Prod_4_1.InPort4, QLM.OutPort1) annotation (points=[366, -168; 
          335.5, -168], style(color=3));
    connect(Prod_4_1.InPort2, QLF.OutPort1) annotation (points=[376, -174; 376
          , -200; 356, -200; 356, -232; 364.5, -232], style(color=3));
    connect(QLP.OutPort1, Prod_4_1.InPort1) annotation (points=[374, -130; 374
          , -152], style(color=3));
    connect(Prod_4_1.InPort3, QLC.OutPort1) annotation (points=[366, -158; 334
          , -158; 334, -129.5], style(color=3));
    connect(QLC.InPort1, Crowd_Rat.OutPort1) annotation (points=[334, -100; 
          334, 208; -208, 208; -208, 8; -60, 8; -60, 24], style(color=3));
    connect(QLP.InPort1, BRPM.InPort1) annotation (points=[376, -100; 376, 214
          ; -216, 214; -216, -20; -136, -20; -136, 23], style(color=3));
    connect(Prod_4_1.OutPort1, Quality_of_Life.InPort1) annotation (points=[
          386, -164; 400, -164], style(color=3));
    connect(Depletion.InPort1, NR_norm.OutPort1) annotation (points=[180, 118
          ; 180, 104; 174, 104], style(color=3));
    connect(Prod_2_1.OutPort1, NR_norm.InPort1) annotation (points=[140, 90; 
          140, 108; 146, 108], style(color=3));
    connect(P_Generation.InPort1, Pol_norm.OutPort1) annotation (points=[-120
          , -162; -120, -178; -125.5, -178], style(color=3));
    connect(Prod_2_4.OutPort1, Pol_norm.InPort1) annotation (points=[-160, -
          208; -160, -174; -154, -174], style(color=3));
    connect(Death_Rate.InPort1, DR_norm.OutPort1) annotation (points=[-20, 118
          ; -20, 100; -14.5, 100], style(color=3));
    connect(Prod_5_2.OutPort1, DR_norm.InPort1) annotation (points=[20, 90; 20
          , 106; 14, 106], style(color=3));
    connect(Generation.MassOutPort2, Natural_Resources.MassInPort1) 
      annotation (points=[92, 140; 108, 140], style(color=77));
    connect(Natural_Resources.MassInPort2, Depletion.MassOutPort1) annotation 
      (points=[152, 140; 168, 140], style(color=77));
    connect(Natural_Resources.OutPort2, NRFR.InPort1) annotation (points=[116
          , 126; 116, 51; 126.5, 51], style(color=3));
    connect(Birth_Rate.MassOutPort2, Population.MassInPort1) annotation (
        points=[-108, 140; -94, 140], style(color=77));
    connect(Population.MassInPort2, Death_Rate.MassOutPort1) annotation (
        points=[-46, 140; -32, 140], style(color=77));
    connect(Population.OutPort3, Prod_5_2.InPort1) annotation (points=[-56, 
          128; -56, 80; 8.5, 80], style(color=3));
    connect(Population.OutPort2, Prod_5_1.InPort2) annotation (points=[-84, 
          126; -84, 80; -150, 80], style(color=3));
    connect(Population.OutPort1, Crowd_Rat.InPort1) annotation (points=[-70, 
          126; -70, 68; -60, 68; -60, 45.5], style(color=3));
    connect(Prod_2_1.InPort1, Population.OutPort4) annotation (points=[130, 80
          ; 100, 80; 100, 194; -51, 194; -51, 152; -52, 152], style(color=3));
    connect(Prod_2_2.InPort1, Population.OutPort4) annotation (points=[30, -98
          ; 20, -98; 20, -14; 310, -14; 310, 194; -51, 194; -51, 152; -52, 152]
        , style(color=3));
    connect(Population.OutPort5, Prod_2_4.InPort1) annotation (points=[-86, 
          152; -96, 152; -96, 166; -198, 166; -198, -219; -171.5, -219], style(
          color=3));
    connect(P_Generation.MassOutPort2, Pollution.MassInPort1) annotation (
        points=[-110, -140; -92, -140], style(color=77));
    connect(Pollution.MassInPort2, P_Absorption.MassOutPort1) annotation (
        points=[-48, -140; -30, -140], style(color=77));
    connect(Pollution.OutPort4, Pol_Ratio.InPort1) annotation (points=[-54, -
          129; -54, -95; -39.5, -95], style(color=3));
    connect(Pollution.OutPort1, P_Abs.inPort1) annotation (points=[-70, -154; 
          -70, -170; -64, -170], style(color=3));
    connect(CI_Generation.MassOutPort2, Capital_Investment.MassInPort1) 
      annotation (points=[92, -40; 106, -40], style(color=77));
    connect(Capital_Investment.MassInPort2, CI_Discard.MassOutPort1) 
      annotation (points=[152, -40; 168, -40], style(color=77));
    connect(Capital_Investment.OutPort3, CID_norm.InPort1) annotation (points=
          [144, -52; 144, -76; 156, -76], style(color=3));
    connect(Capital_Investment.OutPort4, CIR.inPort1) annotation (points=[148
          , -30; 148, 14; 128, 14], style(color=3));
    connect(CIAFG.MassOutPort2, CIAF.MassInPort1) annotation (points=[90, -140
          ; 106, -140], style(color=77));
    connect(CIAF.MassInPort2, CIAFD.MassOutPort1) annotation (points=[152, -
          140; 168, -140], style(color=77));
    connect(CIAF.OutPort4, NotCIAF.InPort1) annotation (points=[146, -128; 246
          , -128; 246, -8; 168, -8; 168, 10; 176, 10], style(color=3));
    connect(CIAF.OutPort3, CIAF_D.InPort1) annotation (points=[144, -154; 144
          , -180; 152.5, -180], style(color=3));
    connect(CIAF.OutPort1, Prod_2_5.InPort1) annotation (points=[130, -154; 
          130, -200; 218.5, -200], style(color=3));
    
    // Parameter equations
    NRUN = if years > 1970 then 0.25 else 1.0;
    NR_norm.InPort2.signal[1] = NRUN;
    POLN = if years > 1970 then 0.25 else 1.0;
    Pol_norm.InPort2.signal[1] = POLN;
    DRN = if years > 1970 then 0.02 else 0.028;
    DR_norm.InPort2.signal[1] = DRN;
    
    // Measurement data
    Pop_meas = SystemDynamics.Interfaces.Piecewise(x=years, x_grid=time_vals, 
      y_grid=pop_vals);
    
    // Reversible time flow
    dir = if time > time_reverse then -1 else 1;
    Population.dir = dir;
    Pollution.dir = dir;
    Natural_Resources.dir = dir;
    Capital_Investment.dir = dir;
    CIAF.dir = dir;
    der(years) = dir;
  end World_5;
  model World_6 
    // Initial conditions for state variables
    parameter Real Population_0=1.65e9 "World population in 1900";
    parameter Real Pollution_0=2.0e8 "Pollution in 1900";
    parameter Real Nat_Resources_0=9.0e11 
      "Unrecoverable natural resources in 1900";
    parameter Real Cap_Invest_0=4.0e8 "Capital investment in 1900";
    parameter Real CIAF_0=0.2 
      "Proportion of capital investment in agriculture in 1900";
    
    // Parameters
    parameter Real BRN=0.04 "Normal birth rate";
    parameter Real CIAFN=0.3 "CIAF normalization";
    parameter Real CIAFT=15.0 "CIAF time constant";
    parameter Real CIDN=0.025 "Normal capital discard";
    parameter Real CIGN=0.05 "Normal capital generation";
    parameter Real DRN=0.028 "Normal death rate";
    parameter Real ECIRN=1.0 "Capital normalization";
    parameter Real FC=1.0 "Food coefficient";
    parameter Real FN=1.0 "Food normalization";
    parameter Real Land_Area=1.35e8 "Area of arable land";
    parameter Real NRI=9.0e11 "Initial natural resources";
    parameter Real POLN=1.0 "Normal pollution";
    parameter Real POLS=3.5999e9 "Standard pollution";
    parameter Real Pop_dens_norm=26.5 "Normal population density";
    parameter Real QLS=1.0 "Standard quality of life";
    
    // Output variables
    Real years "Time in calendar years";
    
    // Manipulated parameters
    Real NRUN "Normal resource utilization";
    parameter Real NRUN2=1.0 "Resource utilization after 1970";
    
    // Optimization
    Real rel_dPop "Relative derivative of population";
    Real min_dPop "Minimal derivative of population";
    Real min_QL(start=1) "Minimum quality of life";
    Real Perf_Index "Preformance index";
    annotation (
      Coordsys(
        extent=[-220, -260; 440, 220], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Ellipse(extent=[-62, 10; -58, 6], style(fillColor=73)), 
        Ellipse(extent=[98, 196; 102, 192], style(fillColor=73)), 
        Ellipse(extent=[138, -12; 142, -16], style(fillColor=73)), 
        Ellipse(extent=[-2, -18; 2, -22], style(fillColor=73)), 
        Ellipse(extent=[-178, 2; -174, -2], style(fillColor=73)), 
        Ellipse(extent=[78, 8; 82, 4], style(fillColor=73)), 
        Ellipse(extent=[-2, -92; 2, -96], style(fillColor=73)), 
        Ellipse(extent=[-178, -62; -174, -66], style(fillColor=73)), 
        Ellipse(extent=[-2, -64; 2, -68], style(fillColor=73)), 
        Ellipse(extent=[158, 32; 162, 28], style(fillColor=73)), 
        Ellipse(extent=[38, -258; 42, -262], style(fillColor=73)), 
        Ellipse(extent=[298, -98; 302, -102], style(fillColor=73)), 
        Ellipse(extent=[298, 32; 302, 28], style(fillColor=73)), 
        Ellipse(extent=[298, 82; 302, 78], style(fillColor=73)), 
        Ellipse(extent=[298, 172; 302, 168], style(fillColor=73)), 
        Ellipse(extent=[342, -166; 346, -170], style(fillColor=73)), 
        Ellipse(extent=[354, -230; 358, -234], style(fillColor=74)), 
        Ellipse(extent=[-98, 10; -94, 6], style(fillColor=73)), 
        Ellipse(extent=[-138, -18; -134, -22], style(fillColor=73))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65), 
      Icon(
        Rectangle(extent=[-220, 222; 442, -262], style(fillColor=49)), 
        Text(extent=[-152, 142; 368, 72], string="5th Modification"), 
        Text(
          extent=[-148, 46; 372, -24], 
          string="Optimization", 
          style(color=73)), 
        Text(
          extent=[-150, -34; 370, -104], 
          string="of use of", 
          style(color=73)), 
        Text(
          extent=[-150, -120; 370, -190], 
          string="natural resources", 
          style(color=73))));
    SystemDynamics.Source Source1 annotation (extent=[-160, 130; -140, 150]);
    SystemDynamics.Rate_1 Birth_Rate annotation (extent=[-140, 120; -100, 160]
      );
    SystemDynamics.Level Population(x0=Population_0) annotation (extent=[-90, 
          120; -50, 160]);
    SystemDynamics.Rate_1 Death_Rate annotation (extent=[-40, 120; 0, 160]);
    SystemDynamics.Sink Sink1 annotation (extent=[0, 130; 20, 150]);
    SystemDynamics.Source Source2 annotation (extent=[40, 130; 60, 150]);
    SystemDynamics.Rate_1 Generation annotation (extent=[60, 120; 100, 160]);
    SystemDynamics.Level Natural_Resources(x0=Nat_Resources_0) annotation (
        extent=[110, 120; 150, 160]);
    SystemDynamics.Rate_1 Depletion annotation (extent=[160, 120; 200, 160]);
    SystemDynamics.Sink Sink2 annotation (extent=[200, 130; 220, 150]);
    SystemDynamics.Const Gen_Const(k=0) annotation (extent=[64, 86; 96, 116]);
    SystemDynamics.Funct BRMM(x_vals={0,1,2,3,4,5,20}, y_vals={1.2,1.0,0.85,
          0.75,0.7,0.7,0.7}) annotation (extent=[-90, 170; -120, 200]);
    SystemDynamics.Funct DRMM(x_vals={0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,20}, 
        y_vals={3.0,1.8,1.0,0.8,0.7,0.6,0.53,0.5,0.5,0.5,0.5,0.5}) annotation (
        extent=[66, 156; 36, 186]);
    SystemDynamics.Gain BR_norm(k=BRN) annotation (extent=[-150, 88; -120, 118
          ]);
    SystemDynamics.Prod_5 Prod_5_1 annotation (extent=[-176, 64; -146, 94]);
    SystemDynamics.Gain DR_norm(k=DRN) annotation (extent=[12, 88; -18, 118]);
    SystemDynamics.Prod_5 Prod_5_2 annotation (extent=[4, 64; 34, 94]);
    SystemDynamics.Funct BRFM(x_vals={0,1,2,3,4,20}, y_vals={0.0,1.0,1.6,1.9,
          2.0,2.0}) annotation (extent=[-190, 20; -160, 50], rotation=90);
    SystemDynamics.Funct BRPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.02,
          0.9,0.7,0.4,0.25,0.15,0.1,0.1}) annotation (extent=[-150, 20; -120, 
          50], rotation=90);
    SystemDynamics.Funct BRCM(x_vals=0:5, y_vals={1.05,1.0,0.9,0.7,0.6,0.55}) 
      annotation (extent=[-110, 20; -80, 50], rotation=90);
    SystemDynamics.Funct DRCM(x_vals=0:5, y_vals={0.9,1.0,1.2,1.5,1.9,3.0}) 
      annotation (extent=[-40, 20; -10, 50], rotation=90);
    SystemDynamics.Funct DRPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={0.92,
          1.3,2.0,3.2,4.8,6.8,9.2,9.2}) annotation (extent=[0, 20; 30, 50], 
        rotation=90);
    SystemDynamics.Funct DRFM(x_vals={0,0.25,0.5,0.75,1,1.25,1.5,1.75,2,20}, 
        y_vals={30.0,3.0,2.0,1.4,1.0,0.7,0.6,0.5,0.5,0.5}) annotation (extent=[
          40, 20; 70, 50], rotation=90);
    SystemDynamics.Gain Crowd_Rat(k=1.0/(Land_Area*Pop_dens_norm)) annotation 
      (extent=[-76, 20; -44, 50], rotation=-90);
    SystemDynamics.Prod_2 Prod_2_1 annotation (extent=[126, 64; 156, 94]);
    SystemDynamics.Funct NRMM(x_vals=0:10, y_vals={0.0,1.0,1.8,2.4,2.9,3.3,3.6
          ,3.8,3.9,3.95,4.0}) annotation (extent=[186, 94; 216, 64], rotation=
          180);
    SystemDynamics.Source Source3 annotation (extent=[40, -50; 60, -30]);
    SystemDynamics.Rate_1 CI_Generation annotation (extent=[60, -60; 100, -20]
      );
    SystemDynamics.Level Capital_Investment(x0=Cap_Invest_0) annotation (
        extent=[110, -60; 150, -20]);
    SystemDynamics.Rate_1 CI_Discard annotation (extent=[160, -60; 200, -20]);
    SystemDynamics.Sink Sink3 annotation (extent=[200, -50; 220, -30]);
    SystemDynamics.Gain CIG_norm(k=CIGN) annotation (extent=[50, -92; 80, -62]
      );
    SystemDynamics.Prod_2 Prod_2_2 annotation (extent=[26, -114; 56, -84]);
    SystemDynamics.Gain CID_norm(k=CIDN) annotation (extent=[150, -92; 180, 
          -62]);
    SystemDynamics.Funct CIM(x_vals={0,1,2,3,4,5,20}, y_vals={0.1,1.0,1.8,2.4,
          2.8,3.0,3.0}) annotation (extent=[128, -114; 98, -84]);
    SystemDynamics.Gain MSL(k=1.0/ECIRN) annotation (extent=[262, 16; 292, 46]
      );
    SystemDynamics.Gain NRFR(k=1.0/NRI) annotation (extent=[122, 36; 152, 66])
      ;
    SystemDynamics.Gain ECIR(k=1.0/(1.0 - CIAFN)) annotation (extent=[228, 16
          ; 258, 46]);
    SystemDynamics.Prod_3 Prod_3_1 annotation (extent=[200, 16; 230, 44], 
        rotation=-90);
    SystemDynamics.Funct NREM(x_vals=0:0.25:1, y_vals={0.0,0.15,0.5,0.85,1.0})
       annotation (extent=[190, 66; 160, 36], rotation=180);
    Modelica.Blocks.Math.Division CIR annotation (extent=[122, -10; 92, 20]);
    SystemDynamics.Source Source4 annotation (extent=[-160, -150; -140, -130])
      ;
    SystemDynamics.Rate_1 P_Generation annotation (extent=[-140, -160; -100, 
          -120]);
    SystemDynamics.Level Pollution(x0=Pollution_0) annotation (extent=[-90, 
          -160; -50, -120]);
    SystemDynamics.Rate_1 P_Absorption annotation (extent=[-40, -160; 0, -120]
      );
    SystemDynamics.Sink Sink4 annotation (extent=[0, -150; 20, -130]);
    SystemDynamics.Linear NotCIAF(m=-1.0, b=1.0) annotation (extent=[174, -4; 
          204, 26]);
    SystemDynamics.Gain Pol_Ratio(k=1.0/POLS) annotation (extent=[-44, -110; 
          -14, -80]);
    SystemDynamics.Funct FCM(x_vals=0:5, y_vals={2.4,1.0,0.6,0.4,0.3,0.2}) 
      annotation (extent=[-100, -32; -70, -62], rotation=180);
    SystemDynamics.Prod_3 Prod_3_2 annotation (extent=[-102, -80; -132, -52], 
        rotation=-90);
    SystemDynamics.Gain Food_Ratio(k=FC/FN) annotation (extent=[-134, -80; 
          -164, -50]);
    SystemDynamics.Source Source5 annotation (extent=[40, -150; 60, -130]);
    SystemDynamics.Rate_1 CIAFG annotation (extent=[60, -160; 100, -120]);
    SystemDynamics.Level CIAF(x0=CIAF_0) annotation (extent=[110, -160; 150, 
          -120]);
    SystemDynamics.Rate_1 CIAFD annotation (extent=[160, -160; 200, -120]);
    SystemDynamics.Sink Sink5 annotation (extent=[200, -150; 220, -130]);
    SystemDynamics.Gain CIAF_D(k=1.0/CIAFT) annotation (extent=[148, -194; 178
          , -164]);
    SystemDynamics.Gain CIAF_G(k=1.0/CIAFT) annotation (extent=[110, -194; 80
          , -164]);
    SystemDynamics.Prod_2 Prod_2_3 annotation (extent=[104, -246; 134, -216]);
    Modelica.Blocks.Math.Division P_Abs annotation (extent=[-58, -194; -28, 
          -164]);
    SystemDynamics.Gain Pol_norm(k=POLN) annotation (extent=[-150, -194; -120
          , -164]);
    SystemDynamics.Prod_2 Prod_2_4 annotation (extent=[-176, -234; -146, -204]
      );
    SystemDynamics.Funct POLCM(x_vals={0,1,2,3,4,5,100}, y_vals={0.05,1.0,3.0,
          5.4,7.4,8.0,8.0}) annotation (extent=[-132, -204; -102, -234], 
        rotation=180);
    SystemDynamics.Funct POLAT(x_vals={0,10,20,30,40,50,60,120}, y_vals={0.6,
          2.5,5.0,8.0,11.5,15.5,20.0,20.0}) annotation (extent=[-52, -204; -22
          , -234], rotation=180);
    SystemDynamics.Funct CFIFR(x_vals={0,0.5,1,1.5,2,20}, y_vals={1.0,0.6,0.3,
          0.15,0.1,0.1}) annotation (extent=[84, -216; 54, -246], rotation=180)
      ;
    SystemDynamics.Funct FPM(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.02,
          0.9,0.65,0.35,0.2,0.1,0.05,0.05}) annotation (extent=[-48, -52; -18, 
          -82], rotation=180);
    SystemDynamics.Prod_2 Prod_2_5 annotation (extent=[214, -184; 244, -214]);
    SystemDynamics.Gain CIRA(k=1.0/CIAFN) annotation (extent=[240, -240; 270, 
          -210]);
    SystemDynamics.Funct FPCI(x_vals={0,1,2,3,4,5,6,100}, y_vals={0.5,1.0,1.4,
          1.7,1.9,2.05,2.2,2.2}) annotation (extent=[-138, -92; -168, -122], 
        rotation=180);
    SystemDynamics.Funct CIQR(x_vals={0,0.5,1,1.5,2,10}, y_vals={0.7,0.8,1.0,
          1.5,2.0,2.0}) annotation (extent=[152, -214; 182, -244], rotation=180
      );
    Modelica.Blocks.Math.Division QLMF annotation (extent=[330, -238; 300, 
          -208]);
    SystemDynamics.Funct QLM(x_vals={0,1,2,3,4,5,20}, y_vals={0.2,1.0,1.7,2.3,
          2.7,2.9,2.9}) annotation (extent=[334, -154; 304, -184], rotation=180
      );
    SystemDynamics.Funct QLF(x_vals={0,1,2,3,4,20}, y_vals={0.0,1.0,1.8,2.4,
          2.7,2.7}) annotation (extent=[366, -218; 396, -248], rotation=180);
    SystemDynamics.Prod_4 Prod_4_1 annotation (extent=[360, -178; 390, -148], 
        rotation=-90);
    SystemDynamics.Funct QLC(x_vals=0:0.5:5, y_vals={2.0,1.3,1.0,0.75,0.55,
          0.45,0.38,0.3,0.25,0.22,0.2}) annotation (extent=[350, -98; 320, -128
          ], rotation=90);
    SystemDynamics.Funct QLP(x_vals={0,10,20,30,40,50,60,120}, y_vals={1.04,
          0.85,0.6,0.3,0.15,0.05,0.02,0.02}) annotation (extent=[390, -98; 360
          , -128], rotation=90);
    SystemDynamics.Gain Quality_of_Life(k=QLS) annotation (extent=[392, -184; 
          432, -144]);
    Parameter_Change NR_norm annotation (extent=[146, 88; 176, 118]);
  equation 
    connect(Source1.MassInPort1, Birth_Rate.MassOutPort1) annotation (points
        =[-139, 140; -132, 140], style(color=77));
    connect(Birth_Rate.MassOutPort2, Population.MassInPort1) annotation (
        points=[-108, 140; -94, 140], style(color=77));
    connect(Population.MassInPort2, Death_Rate.MassOutPort1) annotation (
        points=[-46, 140; -32, 140], style(color=77));
    connect(Death_Rate.MassOutPort2, Sink1.MassInPort1) annotation (points=[
          -10, 140; -1, 140], style(color=77));
    connect(Source2.MassInPort1, Generation.MassOutPort1) annotation (points=[
          62, 140; 70, 140], style(color=77));
    connect(Generation.MassOutPort2, Natural_Resources.MassInPort1) 
      annotation (points=[92, 140; 108, 140], style(color=77));
    connect(Natural_Resources.MassInPort2, Depletion.MassOutPort1) annotation 
      (points=[152, 140; 168, 140], style(color=77));
    connect(Depletion.MassOutPort2, Sink2.MassInPort1) annotation (points=[190
          , 140; 198, 140], style(color=77));
    connect(Gen_Const.OutPort1, Generation.InPort1) annotation (points=[80, 
          111.5; 80, 120], style(color=3));
    connect(Birth_Rate.InPort1, BR_norm.OutPort1) annotation (points=[-120, 
          118; -120, 103; -124.5, 103], style(color=3));
    connect(Death_Rate.InPort1, DR_norm.OutPort1) annotation (points=[-20, 118
          ; -20, 104; -12, 104], style(color=3));
    connect(Prod_5_2.OutPort1, DR_norm.InPort1) annotation (points=[20, 90; 20
          , 104; 7.5, 104], style(color=3));
    connect(DRMM.OutPort1, Prod_5_2.InPort2) annotation (points=[34.5, 172; 28
          , 172; 28, 104; 40, 104; 40, 80; 29.5, 80], style(color=3));
    connect(Population.OutPort3, Prod_5_2.InPort1) annotation (points=[-56, 
          128; -56, 80; 8.5, 80], style(color=3));
    connect(Prod_5_1.OutPort1, BR_norm.InPort1) annotation (points=[-160, 90; 
          -160, 104; -145.5, 104], style(color=3));
    connect(BRMM.OutPort1, Prod_5_1.InPort1) annotation (points=[-121.5, 186; 
          -184, 186; -184, 80; -172, 80], style(color=3));
    connect(Population.OutPort2, Prod_5_1.InPort2) annotation (points=[-84, 
          126; -84, 80; -150, 80], style(color=3));
    connect(BRPM.OutPort1, Prod_5_1.InPort5) annotation (points=[-136, 51.5; 
          -136, 60; -162, 60; -162, 68.5], style(color=3));
    connect(Prod_5_1.InPort4, BRCM.OutPort1) annotation (points=[-154, 70; -96
          , 70; -96, 52], style(color=3));
    connect(DRPM.OutPort1, Prod_5_2.InPort5) annotation (points=[14, 51.5; 14
          , 60; 20, 60; 20, 68], style(color=3));
    connect(Prod_5_1.InPort3, BRFM.OutPort1) annotation (points=[-168, 72; 
          -176, 72; -176, 51.5], style(color=3));
    connect(Prod_5_2.InPort3, DRCM.OutPort1) annotation (points=[12, 72; -24, 
          72; -24, 52], style(color=3));
    connect(Prod_5_2.InPort4, DRFM.OutPort1) annotation (points=[26, 72; 54, 
          72; 54, 50], style(color=3));
    connect(Population.OutPort1, Crowd_Rat.InPort1) annotation (points=[-70, 
          126; -70, 68; -60, 68; -60, 45.5], style(color=3));
    connect(Crowd_Rat.OutPort1, BRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -96, 8; -96, 23], style(color=3));
    connect(Crowd_Rat.OutPort1, DRCM.InPort1) annotation (points=[-60, 24; -60
          , 8; -24, 8; -24, 23], style(color=3));
    connect(Prod_2_1.InPort1, Population.OutPort4) annotation (points=[130, 80
          ; 100, 80; 100, 194; -51, 194; -51, 152; -52, 152], style(color=3));
    connect(Source3.MassInPort1, CI_Generation.MassOutPort1) annotation (
        points=[62, -40; 68, -40], style(color=77));
    connect(CI_Generation.MassOutPort2, Capital_Investment.MassInPort1) 
      annotation (points=[92, -40; 106, -40], style(color=77));
    connect(Capital_Investment.MassInPort2, CI_Discard.MassOutPort1) 
      annotation (points=[152, -40; 168, -40], style(color=77));
    connect(CI_Discard.MassOutPort2, Sink3.MassInPort1) annotation (points=[
          190, -40; 198, -40], style(color=77));
    connect(CI_Generation.InPort1, CIG_norm.OutPort1) annotation (points=[80, 
          -62; 80, -77; 75.5, -77], style(color=3));
    connect(CIG_norm.InPort1, Prod_2_2.OutPort1) annotation (points=[54, -76; 
          42, -76; 42, -88], style(color=3));
    connect(Prod_2_2.InPort1, Population.OutPort4) annotation (points=[30, -98
          ; 20, -98; 20, -14; 310, -14; 310, 194; -51, 194; -51, 152; -52, 152]
        , style(color=3));
    connect(CI_Discard.InPort1, CID_norm.OutPort1) annotation (points=[180, 
          -60; 180, -78; 175.5, -78], style(color=3));
    connect(Capital_Investment.OutPort3, CID_norm.InPort1) annotation (points=
          [144, -52; 144, -76; 156, -76], style(color=3));
    connect(CIM.OutPort1, Prod_2_2.InPort2) annotation (points=[96, -100; 51.5
          , -100], style(color=3));
    connect(NRMM.OutPort1, Prod_2_1.InPort2) annotation (points=[184.5, 80; 
          152, 80], style(color=3));
    connect(Natural_Resources.OutPort2, NRFR.InPort1) annotation (points=[116
          , 126; 116, 51; 126.5, 51], style(color=3));
    connect(NREM.OutPort1, Prod_3_1.InPort1) annotation (points=[192, 52; 214
          , 52; 214, 39.8], style(color=3));
    connect(NRFR.OutPort1, NREM.InPort1) annotation (points=[147.5, 50; 163, 
          50], style(color=3));
    connect(Capital_Investment.OutPort4, CIR.inPort1) annotation (points=[148
          , -30; 148, 14; 128, 14], style(color=3));
    connect(CIR.outPort, Prod_3_1.InPort3) annotation (points=[90, 6; 80, 6; 
          80, 30; 204, 30], style(color=3));
    connect(Prod_3_1.OutPort1, ECIR.InPort1) annotation (points=[225.5, 30; 
          232.5, 30], style(color=3));
    connect(CIR.inPort2, Prod_2_2.InPort1) annotation (points=[126, -4; 140, 
          -4; 140, -14; 20, -14; 20, -98; 30, -98], style(color=3));
    connect(Source4.MassInPort1, P_Generation.MassOutPort1) annotation (points
        =[-139, -140; -130, -140], style(color=77));
    connect(P_Generation.MassOutPort2, Pollution.MassInPort1) annotation (
        points=[-110, -140; -92, -140], style(color=77));
    connect(Pollution.MassInPort2, P_Absorption.MassOutPort1) annotation (
        points=[-48, -140; -30, -140], style(color=77));
    connect(P_Absorption.MassOutPort2, Sink4.MassInPort1) annotation (points=[
          -10, -140; -1, -140], style(color=77));
    connect(NotCIAF.OutPort1, Prod_3_1.InPort2) annotation (points=[204, 12; 
          216, 12; 216, 20.2], style(color=3));
    connect(DRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[16, 22; 16, 
          12; 0, 12; 0, -95; -18.5, -95], style(color=3));
    connect(BRPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-136, 22; 
          -136, -20; 0, -20; 0, -95; -18.5, -95], style(color=3));
    connect(Crowd_Rat.OutPort1, FCM.InPort1) annotation (points=[-60, 24; -60
          , -48; -72, -48], style(color=3));
    connect(FCM.OutPort1, Prod_3_2.InPort1) annotation (points=[-102, -46; 
          -117, -46; -117, -56], style(color=3));
    connect(Food_Ratio.InPort1, Prod_3_2.OutPort1) annotation (points=[-138.5
          , -66; -127.5, -66], style(color=3));
    connect(Food_Ratio.OutPort1, BRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 22], style(color=3));
    connect(Food_Ratio.OutPort1, DRFM.InPort1) annotation (points=[-158, -64; 
          -176, -64; -176, 0; 54, 0; 54, 22], style(color=3));
    connect(Pollution.OutPort4, Pol_Ratio.InPort1) annotation (points=[-54, 
          -129; -54, -95; -39.5, -95], style(color=3));
    connect(Source5.MassInPort1, CIAFG.MassOutPort1) annotation (points=[60, 
          -140; 68, -140], style(color=77));
    connect(CIAFG.MassOutPort2, CIAF.MassInPort1) annotation (points=[90, -140
          ; 106, -140], style(color=77));
    connect(CIAF.MassInPort2, CIAFD.MassOutPort1) annotation (points=[152, 
          -140; 168, -140], style(color=77));
    connect(CIAFD.MassOutPort2, Sink5.MassInPort1) annotation (points=[190, 
          -140; 198, -140], style(color=77));
    connect(CIAF.OutPort4, NotCIAF.InPort1) annotation (points=[146, -128; 146
          , -106; 246, -106; 246, -8; 168, -8; 168, 10; 176, 10], style(color=3
        ));
    connect(CIAF_D.OutPort1, CIAFD.InPort1) annotation (points=[173.5, -180; 
          180, -180; 180, -161], style(color=3));
    connect(CIAF.OutPort3, CIAF_D.InPort1) annotation (points=[144, -154; 144
          , -180; 152.5, -180], style(color=3));
    connect(P_Abs.outPort, P_Absorption.InPort1) annotation (points=[-26.5, 
          -180; -20, -180; -20, -161], style(color=3));
    connect(Pollution.OutPort1, P_Abs.inPort1) annotation (points=[-70, -154; 
          -70, -170; -64, -170], style(color=3));
    connect(P_Generation.InPort1, Pol_norm.OutPort1) annotation (points=[-120
          , -161; -120, -180; -124.5, -180], style(color=3));
    connect(Prod_2_4.OutPort1, Pol_norm.InPort1) annotation (points=[-162, 
          -208.5; -162, -180; -146, -180], style(color=3));
    connect(Population.OutPort5, Prod_2_4.InPort1) annotation (points=[-86, 
          152; -96, 152; -96, 166; -198, 166; -198, -219; -171.5, -219], style(
          color=3));
    connect(POLCM.OutPort1, Prod_2_4.InPort2) annotation (points=[-133.5, -220
          ; -150.5, -220], style(color=3));
    connect(CIR.outPort, POLCM.InPort1) annotation (points=[90, 6; 80, 6; 80, 
          -6; 10, -6; 10, -116; -102.5, -116; -102.5, -218; -105, -218], style(
          color=3));
    connect(POLAT.OutPort1, P_Abs.inPort2) annotation (points=[-53.5, -220; 
          -74, -220; -74, -188; -64, -188], style(color=3));
    connect(CIAFG.InPort1, CIAF_G.OutPort1) annotation (points=[80, -162; 80, 
          -180; 84.5, -180], style(color=3));
    connect(Prod_2_3.OutPort1, CIAF_G.InPort1) annotation (points=[120, -220.5
          ; 120, -180; 105.5, -180], style(color=3));
    connect(CFIFR.OutPort1, Prod_2_3.InPort1) annotation (points=[86, -231; 
          108.5, -231], style(color=3));
    connect(FPM.OutPort1, Prod_3_2.InPort3) annotation (points=[-50, -66; 
          -106.5, -66], style(color=3));
    connect(CIAF.OutPort1, Prod_2_5.InPort1) annotation (points=[130, -154; 
          130, -200; 218.5, -200], style(color=3));
    connect(Prod_2_5.InPort2, Prod_3_1.InPort3) annotation (points=[239.5, 
          -198; 256, -198; 256, -12; 160, -12; 160, 30; 204.5, 30], style(color
          =3));
    connect(Prod_2_5.OutPort1, CIRA.InPort1) annotation (points=[228, -208; 
          228, -226; 244, -226], style(color=3));
    connect(FPCI.OutPort1, Prod_3_2.InPort2) annotation (points=[-136, -106; 
          -118, -106; -118, -75.8], style(color=3));
    connect(CIRA.OutPort1, FPCI.InPort1) annotation (points=[266, -224; 280, 
          -224; 280, -250; -210, -250; -210, -106; -165, -106], style(color=3))
      ;
    connect(Prod_2_3.InPort2, CIQR.OutPort1) annotation (points=[129.5, -230; 
          150.5, -230], style(color=3));
    connect(QLMF.outPort, CIQR.InPort1) annotation (points=[300, -223; 290, 
          -223; 290, -256; 200, -256; 200, -230; 180, -230], style(color=3));
    connect(FPM.InPort1, Pol_Ratio.OutPort1) annotation (points=[-20, -66; 0, 
          -66; 0, -96; -18, -96], style(color=3));
    connect(POLAT.InPort1, FPM.InPort1) annotation (points=[-24, -218; 0, -218
          ; 0, -66; -21, -66], style(color=3));
    connect(QLF.OutPort1, QLMF.inPort2) annotation (points=[364.5, -232; 336, 
          -232], style(color=3));
    connect(Food_Ratio.OutPort1, QLF.InPort1) annotation (points=[-160, -64; 
          -220, -64; -220, -260; 400, -260; 400, -232; 393, -232], style(color=
            3));
    connect(CFIFR.InPort1, QLF.InPort1) annotation (points=[56, -230; 40, -230
          ; 40, -260; 400, -260; 400, -232; 393, -232], style(color=3));
    connect(QLM.OutPort1, QLMF.inPort1) annotation (points=[335.5, -168; 344, 
          -168; 344, -214; 334, -214], style(color=3));
    connect(MSL.OutPort1, QLM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, -170; 306, -170], style(color=3));
    connect(MSL.OutPort1, NRMM.InPort1) annotation (points=[288, 31; 300, 31; 
          300, 80; 214, 80], style(color=3));
    connect(ECIR.OutPort1, MSL.InPort1) annotation (points=[254, 32; 266.5, 32
          ], style(color=3));
    connect(CIM.InPort1, QLM.InPort1) annotation (points=[126, -100; 300, -100
          ; 300, -170; 308, -170], style(color=3));
    connect(DRMM.InPort1, NRMM.InPort1) annotation (points=[64, 170; 300, 170
          ; 300, 80; 213, 80], style(color=3));
    connect(BRMM.InPort1, NRMM.InPort1) annotation (points=[-92, 186; -58, 186
          ; -58, 202; 300, 202; 300, 80; 212, 80], style(color=3));
    connect(Prod_4_1.InPort4, QLM.OutPort1) annotation (points=[366, -168; 
          335.5, -168], style(color=3));
    connect(Prod_4_1.InPort2, QLF.OutPort1) annotation (points=[376, -174; 376
          , -200; 356, -200; 356, -232; 364.5, -232], style(color=3));
    connect(QLP.OutPort1, Prod_4_1.InPort1) annotation (points=[374, -130; 374
          , -152], style(color=3));
    connect(Prod_4_1.InPort3, QLC.OutPort1) annotation (points=[366, -158; 334
          , -158; 334, -129.5], style(color=3));
    connect(QLC.InPort1, Crowd_Rat.OutPort1) annotation (points=[334, -100; 
          334, 208; -208, 208; -208, 8; -60, 8; -60, 24], style(color=3));
    connect(QLP.InPort1, BRPM.InPort1) annotation (points=[376, -100; 376, 214
          ; -216, 214; -216, -20; -136, -20; -136, 23], style(color=3));
    connect(Prod_4_1.OutPort1, Quality_of_Life.InPort1) annotation (points=[
          386, -164; 400, -164], style(color=3));
    connect(Depletion.InPort1, NR_norm.OutPort1) annotation (points=[180, 118
          ; 180, 104; 174, 104], style(color=3));
    connect(Prod_2_1.OutPort1, NR_norm.InPort1) annotation (points=[140, 90; 
          140, 108; 146, 108], style(color=3));
    years = time + 1900;
    
    // Parameter equations
    NRUN = if years > 1970 then NRUN2 else 1.0;
    NR_norm.InPort2.signal[1] = NRUN;
    
    // Optimization
    rel_dPop = (Birth_Rate.rate - Death_Rate.rate)/Population.level;
    when sample(100, 1) then
      min_dPop = min([pre(min_dPop), rel_dPop]);
      min_QL = min([pre(min_QL), Quality_of_Life.y]);
    end when;
    Perf_Index = min_QL + 5*min_dPop;
  end World_6;
end WorldModel;
