package SolarHouse 
  annotation (Coordsys(
      extent=[0, 0; 443, 453], 
      grid=[2, 2], 
      component=[20, 20]), Window(
      x=0.01, 
      y=0.31, 
      width=0.44, 
      height=0.65, 
      library=1, 
      autolayout=1));
  function Piecewise 
    input Real x "Independent variable";
    input Real x_grid[:] "Independent variable data points";
    input Real y_grid[:] "Dependent variable data points";
    output Real y "Interpolated result";
  protected 
    Integer n;
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Rectangle(extent=[-44, 60; -8, 40], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 40; -8, 20], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 20; -8, -2], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 0; -8, -22], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, -20; -8, -42], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, -40; -8, -60], style(color=0, fillColor=49)), 
        Rectangle(extent=[-8, 60; 28, 40], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 40; 28, 20], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 20; 28, -2], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 0; 28, -20], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, -20; 28, -40], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, -40; 28, -60], style(color=0, fillColor=7)), 
        Text(extent=[-150, 150; 150, 110], string="%name")), 
      Diagram(
        Rectangle(extent=[-44, 60; -8, 40], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 40; -8, 20], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 20; -8, -2], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 0; -8, -22], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, -20; -8, -42], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, -40; -8, -60], style(color=0, fillColor=49)), 
        Rectangle(extent=[-8, 60; 28, 40], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 40; 28, 20], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 20; 28, -2], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 0; 28, -20], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, -20; 28, -40], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, -40; 28, -60], style(color=0, fillColor=7)), 
        Text(extent=[-96, 94; 98, 70], string="Linear interpolation")));
  algorithm 
    n := size(x_grid, 1);
    assert(size(x_grid, 1) == size(y_grid, 1), "Size mismatch");
    assert(x >= x_grid[1] and x <= x_grid[n], "Out of range");
    for i in 1:n - 1 loop
      if x >= x_grid[i] and x <= x_grid[i + 1] then
        y := y_grid[i] + (y_grid[i + 1] - y_grid[i])*((x - x_grid[i])/(x_grid[
          i + 1] - x_grid[i]));
      end if;
    end for;
  end Piecewise;
  block TableLookup 
    extends Modelica.Blocks.Interfaces.SISO;
    parameter Real x_vals[:]={0} "Independent variable data points";
    parameter Real y_vals[:]={0} "Dependent variable data points";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Rectangle(extent=[-44, 60; -8, 40], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 40; -8, 20], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 20; -8, -2], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 0; -8, -22], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, -20; -8, -42], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, -40; -8, -60], style(color=0, fillColor=49)), 
        Rectangle(extent=[-8, 60; 28, 40], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 40; 28, 20], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 20; 28, -2], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 0; 28, -20], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, -20; 28, -40], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, -40; 28, -60], style(color=0, fillColor=7)), 
        Line(points=[-100, 0; -44, 0], style(color=9)), 
        Line(points=[28, 0; 100, 0], style(color=9)), 
        Text(
          extent=[-88, 20; -62, 2], 
          string="u", 
          style(color=9)), 
        Text(
          extent=[52, 20; 78, 2], 
          string="y", 
          style(color=9))), 
      Icon(
        Rectangle(extent=[-44, 60; -8, 40], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 40; -8, 20], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 20; -8, -2], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, 0; -8, -22], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, -20; -8, -42], style(color=0, fillColor=49)), 
        Rectangle(extent=[-44, -40; -8, -60], style(color=0, fillColor=49)), 
        Rectangle(extent=[-8, 60; 28, 40], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 40; 28, 20], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 20; 28, -2], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, 0; 28, -20], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, -20; 28, -40], style(color=0, fillColor=7)), 
        Rectangle(extent=[-8, -40; 28, -60], style(color=0, fillColor=7))));
  equation 
    y = Piecewise(x=u, x_grid=x_vals, y_grid=y_vals);
  end TableLookup;
  block DayHour 
    extends Modelica.Blocks.Interfaces.SISO;
    output Real day "day of the year";
    output Real hour "hour of the day";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Rectangle(extent=[-100, 100; 100, -100]), 
        Text(extent=[-80, 60; 80, 20], string="Convert the"), 
        Text(extent=[-80, 20; 80, -20], string="day of the year"), 
        Text(extent=[-80, -20; 80, -60], string="to the hour of the day")), 
      Icon(
        Text(extent=[-80, 72; 16, 32], string="Day of the year"), 
        Rectangle(extent=[-84, 72; 20, 32]), 
        Text(extent=[-22, -42; 86, -76], string="Hour of the day"), 
        Rectangle(extent=[-22, -42; 86, -78]), 
        Line(points=[-12, 12; 26, -26]), 
        Line(points=[26, -14; 26, -26; 14, -26])), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
  equation 
    day = u;
    hour = 24*(day - floor(day));
    y = hour;
  end DayHour;
  block Temperature 
    extends Modelica.Blocks.Interfaces.SISO;
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Line(points=[-140, 28; -94, 28], style(color=9)), 
        Line(points=[-104, 34; -94, 28; -104, 22], style(color=9)), 
        Text(
          extent=[-142, 40; -106, 32], 
          string="Hour in the year", 
          style(color=9)), 
        Text(
          extent=[-52, 56; -16, 48], 
          string="Day in the year", 
          style(color=9)), 
        Text(
          extent=[-6, 56; 30, 48], 
          string="Hour of the day", 
          style(color=9)), 
        Text(
          extent=[54, 36; 90, 28], 
          string="Daily variation", 
          style(color=9)), 
        Text(
          extent=[54, 28; 90, 20], 
          string="of temperature", 
          style(color=9)), 
        Text(
          extent=[6, -32; 58, -40], 
          string="Average temperature", 
          style(color=9)), 
        Text(
          extent=[6, -40; 38, -48], 
          string="over the year", 
          style(color=9))), 
      Icon(
        Rectangle(extent=[-30, 76; 26, -78]), 
        Rectangle(extent=[-6, 60; 2, -54]), 
        Ellipse(extent=[-8, -52; 4, -64], style(fillColor=73)), 
        Rectangle(extent=[-6, -10; 2, -58], style(fillColor=73)), 
        Line(points=[-8, -44; -18, -44]), 
        Line(points=[-8, -42; -12, -42]), 
        Line(points=[-8, -40; -12, -40]), 
        Line(points=[-8, -38; -12, -38]), 
        Line(points=[-8, -36; -12, -36]), 
        Line(points=[-8, 36; -18, 36]), 
        Line(points=[-8, 38; -12, 38]), 
        Line(points=[-8, 40; -12, 40]), 
        Line(points=[-8, 42; -12, 42]), 
        Line(points=[-8, 44; -12, 44]), 
        Line(points=[-8, -34; -18, -34]), 
        Line(points=[-8, -32; -12, -32]), 
        Line(points=[-8, -30; -12, -30]), 
        Line(points=[-8, -28; -12, -28]), 
        Line(points=[-8, -26; -12, -26]), 
        Line(points=[-8, -24; -18, -24]), 
        Line(points=[-8, -22; -12, -22]), 
        Line(points=[-8, -20; -12, -20]), 
        Line(points=[-8, -18; -12, -18]), 
        Line(points=[-8, -16; -12, -16]), 
        Line(points=[-8, -14; -18, -14]), 
        Line(points=[-8, -12; -12, -12]), 
        Line(points=[-8, -10; -12, -10]), 
        Line(points=[-8, -8; -12, -8]), 
        Line(points=[-8, -6; -12, -6]), 
        Line(points=[-8, -4; -18, -4]), 
        Line(points=[-8, -2; -12, -2]), 
        Line(points=[-8, 0; -12, 0]), 
        Line(points=[-8, 2; -12, 2]), 
        Line(points=[-8, 4; -12, 4]), 
        Line(points=[-8, 6; -18, 6]), 
        Line(points=[-8, 8; -12, 8]), 
        Line(points=[-8, 10; -12, 10]), 
        Line(points=[-8, 12; -12, 12]), 
        Line(points=[-8, 14; -12, 14]), 
        Line(points=[-8, 16; -18, 16]), 
        Line(points=[-8, 18; -12, 18]), 
        Line(points=[-8, 20; -12, 20]), 
        Line(points=[-8, 22; -12, 22]), 
        Line(points=[-8, 24; -12, 24]), 
        Line(points=[-8, 26; -18, 26]), 
        Line(points=[-8, 28; -12, 28]), 
        Line(points=[-8, 30; -12, 30]), 
        Line(points=[-8, 32; -12, 32]), 
        Line(points=[-8, 34; -12, 34])), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    Modelica.Blocks.Math.Gain K1(k={1/24}) annotation (extent=[-60, 20; -40, 
          40]);
    DayHour DayHour1 annotation (extent=[-20, 20; 0, 40]);
    Modelica.Blocks.Math.Product P1 annotation (extent=[60, -10; 80, 10]);
    TableLookup AvgTemperature(x_vals={0,480,1224,1896,2640,3360,4104,4824,
          5568,6312,7032,7776,8496,8760}, y_vals={284,283.71,284.26,288.71,
          292.59,296.48,302.59,302.59,302.04,299.81,294.26,287.59,284.26,284.0}
      ) annotation (extent=[-20, -40; 0, -20]);
    TableLookup TempVariation(x_vals=-1:33, y_vals={0.991,0.988,0.986,
          0.983,0.98,0.978,0.976,0.976,0.979,0.985,0.993,1.003,1.011,1.018,
          1.023,1.027,1.028,1.027,1.023,1.017,1.01,1.004,0.999,0.995,0.991,
          0.988,0.986,0.983,0.98,0.978,0.976,0.976,0.979,0.985,0.993}) 
      annotation (extent=[20, 20; 40, 40]);
  equation 
    connect(inPort, K1.inPort) annotation (points=[-100, 0; -80, 0; -80, 30
          ; -64, 30], style(color=3));
    connect(P1.outPort, outPort) annotation (points=[82, 0; 100, 0], style(
          color=3));
    connect(K1.outPort, DayHour1.inPort) annotation (points=[-38, 30; -24, 30]
        , style(color=3));
    connect(inPort, AvgTemperature.inPort) annotation (points=[-100, 0; -80, 0
          ; -80, -30; -24, -30], style(color=3));
    connect(AvgTemperature.outPort, P1.inPort2) annotation (points=[2, -30; 50
          , -30; 50, -6; 56, -6], style(color=3));
    connect(DayHour1.outPort, TempVariation.inPort) annotation (points=[2, 30
          ; 16, 30], style(color=3));
    connect(TempVariation.outPort, P1.inPort1) annotation (points=[42, 30; 50
          , 30; 50, 6; 56, 6], style(color=3));
  end Temperature;
  block Temperature2 
    extends Modelica.Blocks.Interfaces.SISO;
    constant Real pi=Modelica.Constants.pi;
    output Real corr "Temperature correction factor";
    output Real AvgTemp "Average temperature";
    output Real AvgTemp2 "Average temperature corrected";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Text(
          extent=[-52, 56; -16, 48], 
          string="Day in the year", 
          style(color=9)), 
        Text(
          extent=[-6, 56; 30, 48], 
          string="Hour of the day", 
          style(color=9)), 
        Text(
          extent=[54, 36; 90, 28], 
          string="Daily variation", 
          style(color=9)), 
        Text(
          extent=[54, 28; 90, 20], 
          string="of temperature", 
          style(color=9)), 
        Text(
          extent=[-36, -32; 16, -40], 
          string="Average temperature", 
          style(color=9)), 
        Text(
          extent=[-36, -40; -4, -48], 
          string="over the year", 
          style(color=9)), 
        Polygon(points=[20, -20; 20, -40; 40, -30; 20, -20], style(color=53))
          , 
        Text(
          extent=[20, -10; 40, -14], 
          string="Temperature", 
          style(color=53)), 
        Text(
          extent=[20, -14; 40, -18], 
          string="Correction", 
          style(color=57)), 
        Line(points=[-38, -30; 20, -30], style(color=57, fillColor=61)), 
        Line(points=[40, -30; 50, -30; 50, -6; 56, -6], style(color=53))), 
      Icon(
        Rectangle(extent=[-30, 76; 26, -78]), 
        Rectangle(extent=[-6, 60; 2, -54]), 
        Ellipse(extent=[-8, -52; 4, -64], style(fillColor=73)), 
        Rectangle(extent=[-6, -10; 2, -58], style(fillColor=73)), 
        Line(points=[-8, -44; -18, -44]), 
        Line(points=[-8, -42; -12, -42]), 
        Line(points=[-8, -40; -12, -40]), 
        Line(points=[-8, -38; -12, -38]), 
        Line(points=[-8, -36; -12, -36]), 
        Line(points=[-8, 36; -18, 36]), 
        Line(points=[-8, 38; -12, 38]), 
        Line(points=[-8, 40; -12, 40]), 
        Line(points=[-8, 42; -12, 42]), 
        Line(points=[-8, 44; -12, 44]), 
        Line(points=[-8, -34; -18, -34]), 
        Line(points=[-8, -32; -12, -32]), 
        Line(points=[-8, -30; -12, -30]), 
        Line(points=[-8, -28; -12, -28]), 
        Line(points=[-8, -26; -12, -26]), 
        Line(points=[-8, -24; -18, -24]), 
        Line(points=[-8, -22; -12, -22]), 
        Line(points=[-8, -20; -12, -20]), 
        Line(points=[-8, -18; -12, -18]), 
        Line(points=[-8, -16; -12, -16]), 
        Line(points=[-8, -14; -18, -14]), 
        Line(points=[-8, -12; -12, -12]), 
        Line(points=[-8, -10; -12, -10]), 
        Line(points=[-8, -8; -12, -8]), 
        Line(points=[-8, -6; -12, -6]), 
        Line(points=[-8, -4; -18, -4]), 
        Line(points=[-8, -2; -12, -2]), 
        Line(points=[-8, 0; -12, 0]), 
        Line(points=[-8, 2; -12, 2]), 
        Line(points=[-8, 4; -12, 4]), 
        Line(points=[-8, 6; -18, 6]), 
        Line(points=[-8, 8; -12, 8]), 
        Line(points=[-8, 10; -12, 10]), 
        Line(points=[-8, 12; -12, 12]), 
        Line(points=[-8, 14; -12, 14]), 
        Line(points=[-8, 16; -18, 16]), 
        Line(points=[-8, 18; -12, 18]), 
        Line(points=[-8, 20; -12, 20]), 
        Line(points=[-8, 22; -12, 22]), 
        Line(points=[-8, 24; -12, 24]), 
        Line(points=[-8, 26; -18, 26]), 
        Line(points=[-8, 28; -12, 28]), 
        Line(points=[-8, 30; -12, 30]), 
        Line(points=[-8, 32; -12, 32]), 
        Line(points=[-8, 34; -12, 34]), 
        Text(extent=[-84, -116; 92, -154], string="corrected")), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    Modelica.Blocks.Math.Gain K1(k={1/24}) annotation (extent=[-60, 20; -40, 
          40]);
    DayHour DayHour1 annotation (extent=[-20, 20; 0, 40]);
    Modelica.Blocks.Math.Product P1 annotation (extent=[60, -10; 80, 10]);
    TableLookup AvgTemperature(x_vals={0,480,1224,1896,2640,3360,4104,4824,
          5568,6312,7032,7776,8496,8760}, y_vals={284,283.71,284.26,288.71,
          292.59,296.48,302.59,302.59,302.04,299.81,294.26,287.59,284.26,284.0}
      ) annotation (extent=[-60, -40; -40, -20]);
    TableLookup TempVariation(x_vals=-1:33, y_vals={0.991,0.988,0.986,
          0.983,0.98,0.978,0.976,0.976,0.979,0.985,0.993,1.003,1.011,1.018,
          1.023,1.027,1.028,1.027,1.023,1.017,1.01,1.004,0.999,0.995,0.991,
          0.988,0.986,0.983,0.98,0.978,0.976,0.976,0.979,0.985,0.993}) 
      annotation (extent=[20, 20; 40, 40]);
  equation 
    connect(K1.outPort, DayHour1.inPort) annotation (points=[-38, 30; -24, 
          30], style(color=3));
    connect(inPort, K1.inPort) annotation (points=[-100, 0; -80, 0; -80, 30; -
          64, 30], style(color=3));
    connect(P1.outPort, outPort) annotation (points=[82, 0; 100, 0], style(
          color=3));
    connect(inPort, AvgTemperature.inPort) annotation (points=[-100, 0; -80, 0
          ; -80, -30; -64, -30], style(color=3));
    connect(DayHour1.outPort, TempVariation.inPort) annotation (points=[2, 30
          ; 16, 30], style(color=3));
    connect(TempVariation.outPort, P1.inPort1) annotation (points=[42, 30; 50
          , 30; 50, 6; 56, 6], style(color=3));
    AvgTemp = AvgTemperature.y;
    corr = 1 + 0.004*sin(pi*time/60) + 0.0025*cos(pi*(time - 60)/120) + 0.0002
      *sin(pi*(time - 30)/180);
    AvgTemp2 = corr*AvgTemp;
    P1.u2[1] = AvgTemp2;
  end Temperature2;
  block TFilter 
    extends Modelica.Blocks.Interfaces.SISO;
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Rectangle(extent=[-100, 100; 100, -100]), 
        Text(extent=[-80, 60; 80, 20], string="Temperature"), 
        Text(extent=[-80, 20; 80, -20], string="Filter")), 
      Icon(Text(extent=[-80, 20; 80, -20], string="Filter"), Text(extent=[-80
              , 60; 80, 20], string="Temperature")));
  equation 
    y = 1 - 2/(1 + Modelica.Math.exp(-u));
  end TFilter;
  block AirConditioning 
    extends Modelica.Blocks.Interfaces.SISO;
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Rectangle(extent=[-76, 38; 24, -58]), 
        Line(points=[-76, 38; -20, 64]), 
        Line(points=[24, 38; 80, 64]), 
        Line(points=[24, -58; 80, -28]), 
        Line(points=[-20, 64; 80, 64; 80, -28]), 
        Rectangle(extent=[-70, 32; -28, -52]), 
        Rectangle(extent=[-24, 32; 18, -52]), 
        Rectangle(extent=[-68, 30; -30, 26]), 
        Rectangle(extent=[-22, -46; 16, -50]), 
        Rectangle(extent=[-68, 24; -30, 20]), 
        Rectangle(extent=[-68, 18; -30, 14]), 
        Rectangle(extent=[-68, 12; -30, 8]), 
        Rectangle(extent=[-68, 4; -30, 0]), 
        Rectangle(extent=[-68, -2; -30, -6]), 
        Rectangle(extent=[-68, -8; -30, -12]), 
        Rectangle(extent=[-68, -14; -30, -18]), 
        Rectangle(extent=[-68, -22; -30, -26]), 
        Rectangle(extent=[-68, -28; -30, -32]), 
        Rectangle(extent=[-68, -34; -30, -38]), 
        Rectangle(extent=[-68, -40; -30, -44]), 
        Rectangle(extent=[-68, -46; -30, -50]), 
        Rectangle(extent=[-22, 30; 16, 26]), 
        Rectangle(extent=[-22, 24; 16, 20]), 
        Rectangle(extent=[-22, 18; 16, 14]), 
        Rectangle(extent=[-22, 12; 16, 8]), 
        Rectangle(extent=[-22, 4; 16, 0]), 
        Rectangle(extent=[-22, -2; 16, -6]), 
        Rectangle(extent=[-22, -8; 16, -12]), 
        Rectangle(extent=[-22, -14; 16, -18]), 
        Rectangle(extent=[-22, -22; 16, -26]), 
        Rectangle(extent=[-22, -28; 16, -32]), 
        Rectangle(extent=[-22, -34; 16, -38]), 
        Rectangle(extent=[-22, -40; 16, -44])), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    Modelica.Blocks.Sources.Constant C1(k={300}) annotation (extent=[-80, 20; 
          -60, 40]);
    Modelica.Blocks.Sources.Constant C2(k={295}) annotation (extent=[-80, -30
          ; -60, -10]);
    TFilter TF1 annotation (extent=[-20, 40; 0, 60]);
    TFilter TF2 annotation (extent=[-20, -10; 0, 10]);
    Modelica.Blocks.Nonlinear.Limiter L1(uMax={0}, uMin={-10}) annotation (
        extent=[10, 40; 30, 60]);
    Modelica.Blocks.Nonlinear.Limiter L2(uMax={10}, uMin={0}) annotation (
        extent=[10, -10; 30, 10]);
    Modelica.Blocks.Math.Add S1(k2=-1) annotation (extent=[-50, 40; -30, 60]);
    Modelica.Blocks.Math.Add S2(k2=-1) annotation (extent=[-50, -10; -30, 10])
      ;
    Modelica.Blocks.Math.Add A1 annotation (extent=[40, 14; 60, 34]);
    Modelica.Blocks.Math.Division Division1 annotation (extent=[40, -80; 60, -
          60]);
    Modelica.Blocks.Math.Gain G1(k={10000}) annotation (extent=[0, -60; 20, -
          40]);
  equation 
    connect(TF1.outPort, L1.inPort) annotation (points=[2, 50; 6, 50], style
        (color=3));
    connect(TF2.outPort, L2.inPort) annotation (points=[2, 0; 6, 0], style(
          color=3));
    connect(S1.outPort, TF1.inPort) annotation (points=[-28, 50; -24, 50], 
        style(color=3));
    connect(C1.outPort, S1.inPort2) annotation (points=[-58, 30; -56, 30; -56
          , 44; -54, 44], style(color=3));
    connect(inPort, S1.inPort1) annotation (points=[-100, 0; -90, 0; -90, 56; 
          -54, 56], style(color=3));
    connect(C2.outPort, S2.inPort2) annotation (points=[-58, -20; -56, -20; -
          56, -6; -54, -6], style(color=3));
    connect(inPort, S2.inPort1) annotation (points=[-100, 0; -56, 0; -56, 6; -
          54, 6], style(color=3));
    connect(S2.outPort, TF2.inPort) annotation (points=[-28, 0; -24, 0], style
        (color=3));
    connect(L1.outPort, A1.inPort1) annotation (points=[32, 50; 34, 50; 34, 30
          ; 36, 30], style(color=3));
    connect(L2.outPort, A1.inPort2) annotation (points=[32, 0; 34, 0; 34, 18; 
          36, 18], style(color=3));
    connect(A1.outPort, G1.inPort) annotation (points=[62, 24; 70, 24; 70, -24
          ; -10, -24; -10, -50; -4, -50], style(color=3));
    connect(G1.outPort, Division1.inPort1) annotation (points=[22, -50; 28, -
          50; 28, -64; 36, -64], style(color=3));
    connect(inPort, Division1.inPort2) annotation (points=[-100, 0; -90, 0; -
          90, -76; 36, -76], style(color=3));
    connect(Division1.outPort, outPort) annotation (points=[62, -70; 88, -70; 
          88, 0; 100, 0], style(color=3));
  end AirConditioning;
  block SolarPosition 
    extends Modelica.Blocks.Interfaces.SISO;
    constant Real pi=Modelica.Constants.pi;
    parameter Real lon=111 "Longitude in degrees";
    parameter Real lat=32 "Latitude in degrees";
    parameter Real lsm=105;
    parameter Real sfa "Orientation of surface in dgerees";
    parameter Real tilt "Tilt of surface in degrees";
    parameter Real latr=pi*lat/180 "Latitude in radians";
    parameter Real sfar=pi*sfa/180 "Orientation in radians";
    parameter Real tiltr=pi*tilt/180 "Tilt in radians";
    output Real h "Height";
    output Real dec "Declination";
    output Real inc1;
    output Real inc2;
    output Real inc3;
    output Real inc "Inclination";
    output Real alt "Altitude";
    output Real Time;
    output Real ETime;
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Ellipse(extent=[-24, 24; 26, -26]), 
        Polygon(points=[-66, 32; -18, 16; -36, 66; -6, 24; -6, 22; 2, 76; 8, 
              22; 44, 64; 20, 14; 70, 36; 26, 4; 78, -2; 26, -8; 70, -36; 18, -
              18; 38, -68; 8, -26; 2, -78; -6, -24; -38, -66; -18, -20; -64, -
              38; -22, -8; -74, -2; -24, 2; -66, 32], style(color=41, fillColor
              =49)), 
        Text(extent=[92, 36; 132, 12], string="inc"), 
        Text(extent=[90, -26; 130, -50], string="alt")), 
      Diagram(
        Line(points=[-144, 28; -98, 28], style(color=9)), 
        Line(points=[-108, 34; -98, 28; -108, 22], style(color=9)), 
        Line(points=[118, 26; 128, 20; 118, 14], style(color=9)), 
        Line(points=[82, 20; 128, 20], style(color=9)), 
        Line(points=[118, -34; 128, -40; 118, -46], style(color=9)), 
        Line(points=[82, -40; 128, -40], style(color=9)), 
        Text(
          extent=[-144, 40; -104, 28], 
          string="Time", 
          style(color=9)), 
        Text(
          extent=[78, 32; 118, 20], 
          string="Inclination", 
          style(color=9)), 
        Text(
          extent=[78, -28; 118, -40], 
          string="Altitude", 
          style(color=9)), 
        Rectangle(extent=[-4, 16; 80, -76], style(color=57)), 
        Text(
          extent=[6, 6; 70, -12], 
          string="Spherical", 
          style(color=57)), 
        Text(
          extent=[2, -12; 72, -28], 
          string="Trigonometry", 
          style(color=53)), 
        Line(points=[-38, 0; -4, 0], style(color=57)), 
        Line(points=[-82, 0; -82, -38; -4, -38], style(color=57)), 
        Line(points=[80, 0; 100, 0], style(color=57)), 
        Line(points=[80, -60; 100, -60], style(color=57)), 
        Text(
          extent=[-40, 10; -6, 0], 
          string="ETime", 
          style(color=53))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    Modelica.Blocks.Interfaces.OutPort OutPort1 annotation (extent=[100, -70; 
          120, -50]);
    TableLookup ET1(x_vals={0,480,1224,1896,2640,3360,4104,4824,5568,6312,7032
          ,7776,8496,8760}, y_vals={-6.4,-11.2,-13.9,-7.5,1.1,3.3,-1.4,-6.2,-
          2.4,7.5,15.4,13.8,1.6,-6.4}) annotation (extent=[-58, -10; -38, 10]);
  equation 
    connect(inPort, ET1.inPort) annotation (points=[-100, 0; -62, 0], style(
          color=3));
    Time = u;
    ETime = ET1.y;
    h = (pi/12)*(Time - 12 + ETime/60 + (lsm - lon)/15);
    dec = (pi/180)*23.45*sin(2*pi*(284 + Time/24)/365);
    inc1 = sin(dec)*sin(latr)*cos(tiltr) - sin(dec)*cos(latr)*sin(tiltr)*cos(
      sfar);
    inc2 = cos(dec)*cos(latr)*cos(tiltr)*cos(h) + cos(dec)*sin(latr)*sin(tiltr
      )*cos(sfar)*cos(h);
    inc3 = cos(dec)*sin(tiltr)*sin(sfar)*sin(h);
    inc = inc1 + inc2 + inc3;
    alt = cos(latr)*cos(dec)*cos(h) + sin(latr)*sin(dec);
    y = inc;
    OutPort1.signal[1] = alt;
  end SolarPosition;
  block SolarRadiation 
    extends Modelica.Blocks.Interfaces.SISO;
    constant Real pi=Modelica.Constants.pi;
    parameter Real sc=1353;
    parameter Real tilt "Tilt of surface in degrees";
    parameter Real tiltr=pi*tilt/180 "Tilt in radians";
    parameter Real ro=0.33;
    parameter Real cr=1 "Direct light gain factor";
    parameter Real cf=1 "Diffuse light gain factor";
    parameter Real absor "Absorption coefficient";
    parameter Real area "Area of exposed surface";
    parameter Real OH;
    parameter Real W;
    output Real altL "Limited altitude value";
    output Real sou;
    output Real idn;
    output Real dir "Direct light";
    output Real dif "Diffuse light";
    output Real inc "Inclination";
    output Real alt "Altitude";
    output Real Time;
    output Real BTime;
    output Real CTime;
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Text(
          extent=[-144, -46; -108, -68], 
          string="alt", 
          style(color=49)), 
        Text(
          extent=[-144, 10; -108, -12], 
          string="inc", 
          style(color=49)), 
        Line(points=[90, 20; 126, 20], style(color=9)), 
        Line(points=[116, 26; 126, 20; 116, 14], style(color=9)), 
        Text(
          extent=[88, 32; 114, 20], 
          string="Entropy", 
          style(color=9)), 
        Text(
          extent=[28, -12; 46, -16], 
          string="Entropy", 
          style(color=9)), 
        Text(
          extent=[28, -16; 46, -20], 
          string="Output", 
          style(color=9)), 
        Text(
          extent=[-2, 10; 16, 6], 
          string="Heat", 
          style(color=9)), 
        Text(
          extent=[-2, 6; 16, 2], 
          string="Output", 
          style(color=9)), 
        Text(
          extent=[-72, -96; -42, -100], 
          string="Direct Light", 
          style(color=9)), 
        Text(
          extent=[-40, -96; -10, -100], 
          string="Diffuse Light", 
          style(color=9))), 
      Icon(
        Ellipse(extent=[-24, 24; 26, -26]), 
        Polygon(points=[-66, 32; -18, 16; -36, 66; -6, 24; -6, 22; 2, 76; 8, 
              22; 44, 64; 20, 14; 70, 36; 26, 4; 78, -2; 26, -8; 70, -36; 18, -
              18; 38, -68; 8, -26; 2, -78; -6, -24; -38, -66; -18, -20; -64, -
              38; -22, -8; -74, -2; -24, 2; -66, 32], style(color=41, fillColor
              =49)), 
        Line(points=[16, -52; 22, -92], style(color=41, thickness=2)), 
        Line(points=[44, -42; 78, -78], style(color=41, thickness=2)), 
        Line(points=[62, -16; 92, -24], style(color=41, thickness=2)), 
        Line(points=[60, -22; 90, -34], style(color=41, thickness=2)), 
        Line(points=[48, -36; 88, -62], style(color=41, thickness=2)), 
        Line(points=[38, -52; 64, -90], style(color=41, thickness=2)), 
        Line(points=[22, -54; 36, -90], style(color=41, thickness=2)), 
        Text(
          extent=[-126, -30; -100, -46], 
          string="alt", 
          style(color=9)), 
        Text(
          extent=[-126, 30; -100, 14], 
          string="inc", 
          style(color=9)), 
        Text(
          extent=[10, 96; 36, 80], 
          string="Time", 
          style(color=9)), 
        Text(
          extent=[-42, -80; -12, -98], 
          string="Temp", 
          style(color=9)), 
        Text(
          extent=[88, 32; 114, 20], 
          string="Entropy", 
          style(color=9)), 
        Line(points=[116, 26; 126, 20; 116, 14], style(color=9)), 
        Line(points=[126, 20; 86, 20], style(color=9))), 
      Window(
        x=0.01, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[-140, -80; -
          100, -40]);
    Modelica.Blocks.Interfaces.InPort TimePort annotation (extent=[-10, 80; 10
          , 100], rotation=-90);
    Modelica.Blocks.Interfaces.InPort TempPort annotation (extent=[-10, -100; 
          10, -80], rotation=90);
    Modelica.Blocks.Nonlinear.Limiter L2(uMax={1E10}, uMin={0}) annotation (
        extent=[70, -10; 90, 10]);
    Modelica.Blocks.Nonlinear.Limiter altLim(uMax={10}, uMin={0.05}) 
      annotation (extent=[-80, 60; -60, 80]);
    Modelica.Blocks.Nonlinear.Limiter Ldir(uMax={1E10}, uMin={0}) annotation (
        extent=[-66, -90; -46, -70], rotation=90);
    Modelica.Blocks.Nonlinear.Limiter Ldif(uMax={1E10}, uMin={0}) annotation (
        extent=[-36, -90; -16, -70], rotation=90);
    Modelica.Blocks.Math.Product P1 annotation (extent=[40, -10; 60, 10]);
    Modelica.Blocks.Math.Division D1 annotation (extent=[10, -20; 30, 0]);
    Modelica.Blocks.Math.Add A1 annotation (extent=[-50, -30; -30, -10], 
        rotation=90);
    Modelica.Blocks.Math.Gain Absorption(k={absor}) annotation (extent=[-20, -
          10; 0, 10]);
    Modelica.Blocks.Math.Gain Gdif(k={cf}) annotation (extent=[-36, -60; -16, 
          -40], rotation=90);
    Modelica.Blocks.Math.Gain Gdir(k={cr}) annotation (extent=[-66, -60; -46, 
          -40], rotation=90);
    Modelica.Blocks.Math.Sign altSign annotation (extent=[-80, 20; -60, 40]);
    TableLookup BT1(x_vals={0,480,1224,1896,2640,3360,4104,4824,5568,6312,7032
          ,7776,8496,8760}, y_vals={0.14,0.142,0.144,0.156,0.18,0.196,0.205,
          0.207,0.201,0.177,0.16,0.149,0.142,0.14}) annotation (extent=[40, 60
          ; 60, 80]);
    TableLookup CT1(x_vals={0,480,1224,1896,2640,3360,4104,4824,5568,6312,7032
          ,7776,8496,8760}, y_vals={0.057,0.058,0.06,0.071,0.097,0.121,0.134,
          0.136,0.122,0.092,0.073,0.063,0.057,0.057}) annotation (extent=[40, 
          30; 60, 50]);
  equation 
    connect(L2.outPort, outPort) annotation (points=[92, 0; 100, 0], style(
          color=3));
    connect(P1.outPort, L2.inPort) annotation (points=[62, 0; 66, 0], style(
          color=3));
    connect(D1.outPort, P1.inPort2) annotation (points=[32, -10; 34, -10; 34, 
          -6; 36, -6], style(color=3));
    connect(TempPort, D1.inPort2) annotation (points=[0, -80; 0, -16; 6, -16]
        , style(color=3));
    connect(Absorption.outPort, D1.inPort1) annotation (points=[0, 0; 4, 0; 4
          , -4; 6, -4], style(color=3));
    connect(A1.outPort, Absorption.inPort) annotation (points=[-40, -8; -40, 0
          ; -24, 0], style(color=3));
    connect(Gdif.outPort, A1.inPort2) annotation (points=[-26, -38; -34, -38; 
          -34, -34], style(color=3));
    connect(Gdir.outPort, A1.inPort1) annotation (points=[-56, -38; -46, -38; 
          -46, -34], style(color=3));
    connect(Ldif.outPort, Gdif.inPort) annotation (points=[-26, -68; -26, -64]
        , style(color=3));
    connect(Ldir.outPort, Gdir.inPort) annotation (points=[-56, -68; -56, -64]
        , style(color=3));
    connect(altSign.outPort, P1.inPort1) annotation (points=[-58, 30; 30, 30; 
          30, 6; 36, 6], style(color=3));
    connect(InPort1, altSign.inPort) annotation (points=[-100, -60; -90, -60; 
          -90, 30; -84, 30], style(color=3));
    connect(InPort1, altLim.inPort) annotation (points=[-100, -60; -90, -60; -
          90, 70; -84, 70], style(color=3));
    connect(TimePort, BT1.inPort) annotation (points=[0, 82; 0, 70; 36, 70], 
        style(color=3));
    connect(TimePort, CT1.inPort) annotation (points=[0, 80; 0, 40; 36, 40], 
        style(color=3));
    Time = TimePort.signal[1];
    BTime = BT1.y;
    CTime = CT1.y;
    altL = altLim.y[1];
    inc = u;
    alt = InPort1.signal[1];
    sou = sc*(1 + 0.033*cos(2*pi*(Time/24)/365.));
    idn = sou/exp(BTime/altL);
    dir = idn*inc*(area - OH*W*(alt/sqrt(1 - alt*alt)));
    dif = idn*area*(CTime*(1 + cos(tiltr))/2 + (CTime + alt)*ro*(1 - cos(tiltr
      ))/2);
    Ldir.u[1] = dir;
    Ldif.u[1] = dif;
  end SolarRadiation;
  block SolarGlass 
    extends Modelica.Blocks.Interfaces.SISO;
    constant Real pi=Modelica.Constants.pi;
    parameter Real sc;
    parameter Real tilt "Tilt of surface in degrees";
    parameter Real tiltr=pi*tilt/180 "Tilt in radians";
    parameter Real ro=0.33;
    parameter Real cr=1 "Direct light gain factor";
    parameter Real cf=1 "Diffuse light gain factor";
    parameter Real hi=8.3;
    parameter Real ho=22.7;
    parameter Real area "Area of exposed surface";
    parameter Real OH;
    parameter Real W;
    output Real altL "Limited altitude value";
    output Real idn;
    output Real dir "Direct light";
    output Real dif "Diffuse light";
    output Real inc "Inclination";
    output Real alt "Altitude";
    output Real Time;
    output Real ATime;
    output Real BTime;
    output Real CTime;
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[1, 1], 
        component=[10, 10]), 
      Icon(
        Rectangle(extent=[-100, -100; 100, 100], style(color=3, fillColor=7))
          , 
        Ellipse(extent=[-24, 24; 26, -26]), 
        Polygon(points=[-66, 32; -18, 16; -36, 66; -6, 24; -6, 22; 2, 76; 8, 
              22; 44, 64; 20, 14; 70, 36; 26, 4; 78, -2; 26, -8; 70, -36; 18, -
              18; 38, -68; 8, -26; 2, -78; -6, -24; -38, -66; -18, -20; -64, -
              38; -22, -8; -74, -2; -24, 2; -66, 32], style(color=41, fillColor
              =49)), 
        Line(points=[16, -52; 22, -92], style(color=41, thickness=2)), 
        Line(points=[44, -42; 78, -78], style(color=41, thickness=2)), 
        Line(points=[62, -16; 92, -24], style(color=41, thickness=2)), 
        Line(points=[60, -22; 90, -34], style(color=41, thickness=2)), 
        Line(points=[48, -36; 88, -62], style(color=41, thickness=2)), 
        Line(points=[38, -52; 64, -90], style(color=41, thickness=2)), 
        Line(points=[22, -54; 36, -90], style(color=41, thickness=2)), 
        Text(
          extent=[-126, -30; -100, -46], 
          string="alt", 
          style(color=9)), 
        Text(
          extent=[-126, 30; -100, 14], 
          string="inc", 
          style(color=9)), 
        Text(
          extent=[10, 96; 36, 80], 
          string="Time", 
          style(color=9)), 
        Text(
          extent=[88, 32; 114, 20], 
          string="Entropy", 
          style(color=9)), 
        Line(points=[116, 26; 126, 20; 116, 14], style(color=9)), 
        Line(points=[126, 20; 86, 20], style(color=9)), 
        Text(
          extent=[-42, -80; -12, -98], 
          string="Temp", 
          style(color=9)), 
        Line(points=[38, 2; 38, 8; 96, -2; 96, -100], style(color=57, 
              thickness=2)), 
        Line(points=[38, -6; 38, -16], style(color=53, thickness=2)), 
        Line(points=[38, -26; 38, -88; 96, -100], style(color=53, thickness=2)
          )), 
      Diagram(
        Text(
          extent=[-125, 26; -100, 11], 
          string="inc", 
          style(color=9)), 
        Text(
          extent=[-123, -38; -98, -53], 
          string="alt", 
          style(color=9)), 
        Text(
          extent=[-88, -1; -80, -4], 
          string="direct", 
          style(color=9)), 
        Text(
          extent=[-88, -4; -80, -7], 
          string="light", 
          style(color=9)), 
        Text(
          extent=[44, -6; 55, -9], 
          string="diffuse", 
          style(color=9)), 
        Text(
          extent=[45, -10; 53, -13], 
          string="light", 
          style(color=9))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[-140, -80; -
          100, -40], layer="icon");
    Modelica.Blocks.Interfaces.InPort TempPort annotation (extent=[-10, -100; 
          10, -80], rotation=90);
    Modelica.Blocks.Interfaces.InPort TimePort annotation (extent=[-10, 80; 10
          , 100], rotation=-90);
    DirectGlassTransmission DirectGlassTransmission1 annotation (extent=[-80, 
          20; -60, 40]);
    DirectGlassAbsorption DirectGlassAbsorption1 annotation (extent=[-80, -60
          ; -60, -40]);
    Modelica.Blocks.Nonlinear.Limiter Ldir(uMax={1E10}, uMin={0}) annotation (
        extent=[-75, -10; -65, 0]);
    Modelica.Blocks.Math.Gain Gdir(k={cr}) annotation (extent=[-60, -10; -50, 
          0]);
    Modelica.Blocks.Math.Product P1 annotation (extent=[-40, 25; -30, 35]);
    Modelica.Blocks.Math.Product P2 annotation (extent=[-40, -55; -30, -45]);
    Modelica.Blocks.Math.Add A1 annotation (extent=[-20, 22; -10, 32]);
    Modelica.Blocks.Math.Add A2 annotation (extent=[-20, -52; -10, -42]);
    Modelica.Blocks.Math.Product P3 annotation (extent=[-10, 0; -20, 10]);
    Modelica.Blocks.Math.Product P4 annotation (extent=[-10, -30; -20, -20]);
    DiffuseGlassTransmission DiffuseGlassTransmission1 annotation (extent=[20
          , -2; 0, 18]);
    DiffuseGlassAbsorption DiffuseGlassAbsorption1 annotation (extent=[20, -38
          ; 0, -18]);
    Modelica.Blocks.Math.Gain Gdif(k={2*cf}) annotation (extent=[40, -15; 30, 
          -5]);
    Modelica.Blocks.Math.Gain G1(k={hi/(hi + ho)}) annotation (extent=[26, -54
          ; 40, -40]);
    Modelica.Blocks.Math.Add A3 annotation (extent=[51, -49; 61, -39]);
    Modelica.Blocks.Math.Division Division1 annotation (extent=[5, -75; 15, -
          65]);
    Modelica.Blocks.Math.Gain G2(k={sc}) annotation (extent=[-15, -75; -5, -65
          ]);
    Modelica.Blocks.Nonlinear.Limiter L1(uMax={1E10}, uMin={0}) annotation (
        extent=[25, -75; 35, -65]);
    Modelica.Blocks.Math.Product P5 annotation (extent=[45, -78; 55, -68]);
    Modelica.Blocks.Math.Sign altSig annotation (extent=[-70, -95; -60, -85]);
    Modelica.Blocks.Nonlinear.Limiter L2(uMax={1E10}, uMin={0}) annotation (
        extent=[65, -78; 75, -68]);
    Modelica.Blocks.Nonlinear.Limiter altLim(uMax={10}, uMin={0.05}) 
      annotation (extent=[-70, -75; -60, -65]);
    TableLookup BT1(x_vals={0,480,1224,1896,2640,3360,4104,4824,5568,6312,7032
          ,7776,8496,8760}, y_vals={0.14,0.142,0.144,0.156,0.18,0.196,0.205,
          0.207,0.201,0.177,0.16,0.149,0.142,0.14}) annotation (extent=[30, 55
          ; 45, 70]);
    TableLookup CT1(x_vals={0,480,1224,1896,2640,3360,4104,4824,5568,6312,7032
          ,7776,8496,8760}, y_vals={0.057,0.058,0.06,0.071,0.097,0.121,0.134,
          0.136,0.122,0.092,0.073,0.063,0.057,0.057}) annotation (extent=[30, 
          33; 45, 48]);
    TableLookup AT1(x_vals={0,480,1224,1896,2640,3360,4104,4824,5568,6312,7032
          ,7776,8496,8760}, y_vals={1232,1230,1214,1185,1135,1103,1088,1085,
          1107,1151,1192,1220,1233,1232}) annotation (extent=[30, 80; 45, 95]);
  equation 
    connect(inPort, DirectGlassTransmission1.inPort) annotation (points=[-
          100, 0; -90, 0; -90, 30; -84, 30], style(color=3));
    connect(inPort, DirectGlassAbsorption1.inPort) annotation (points=[-100, 0
          ; -90, 0; -90, -50; -84, -50], style(color=3));
    connect(Ldir.outPort, Gdir.inPort) annotation (points=[-64.5, -5; -62, -5]
        , style(color=3));
    connect(DirectGlassTransmission1.outPort, P1.inPort1) annotation (points=[
          -58, 30; -50, 30; -50, 33; -42, 33], style(color=3));
    connect(Gdir.outPort, P1.inPort2) annotation (points=[-49, -5; -45, -5; -
          45, 27; -42, 27], style(color=3));
    connect(DirectGlassAbsorption1.outPort, P2.inPort2) annotation (points=[-
          58, -50; -50, -50; -50, -53; -42, -53], style(color=3));
    connect(Gdir.outPort, P2.inPort1) annotation (points=[-50, -5; -45, -5; -
          45, -47; -42, -47], style(color=3));
    connect(P1.outPort, A1.inPort1) annotation (points=[-29, 30; -22, 30], 
        style(color=3));
    connect(P2.outPort, A2.inPort2) annotation (points=[-30, -50; -22, -50], 
        style(color=3));
    connect(P3.outPort, A1.inPort2) annotation (points=[-20, 5; -25, 5; -25, 
          24; -22, 24], style(color=3));
    connect(P4.outPort, A2.inPort1) annotation (points=[-20, -25; -25, -25; -
          25, -44; -22, -44], style(color=3));
    connect(DiffuseGlassTransmission1.outPort, P3.inPort1) annotation (points=
          [-1, 8; -8, 8], style(color=3));
    connect(DiffuseGlassAbsorption1.outPort, P4.inPort2) annotation (points=[-
          2, -28; -8, -28], style(color=3));
    connect(Gdif.outPort, P3.inPort2) annotation (points=[30, -10; -6, -10; -6
          , 2; -8, 2], style(color=3));
    connect(Gdif.outPort, P4.inPort1) annotation (points=[30, -10; -6, -10; -6
          , -22; -8, -22], style(color=3));
    connect(A2.outPort, G1.inPort) annotation (points=[-9, -47; 24.6, -47], 
        style(color=3));
    connect(G1.outPort, A3.inPort2) annotation (points=[41, -47; 49, -47], 
        style(color=3));
    connect(A1.outPort, A3.inPort1) annotation (points=[-9, 27; 56, 27; 56, -
          26; 44, -26; 44, -41; 49, -41], style(color=3));
    connect(TempPort, Division1.inPort2) annotation (points=[0, -80; 0, -73; 3
          , -73], style(color=3));
    connect(G2.outPort, Division1.inPort1) annotation (points=[-4, -70; 0, -70
          ; 0, -67; 3, -67], style(color=3));
    connect(A3.outPort, G2.inPort) annotation (points=[62, -44; 65, -44; 65, -
          60; -20, -60; -20, -70; -17, -70], style(color=3));
    connect(Division1.outPort, L1.inPort) annotation (points=[16, -70; 23, -70
          ], style(color=3));
    connect(L1.outPort, P5.inPort1) annotation (points=[36, -70; 43, -70], 
        style(color=3));
    connect(P5.outPort, L2.inPort) annotation (points=[56, -73; 64, -73], 
        style(color=3));
    connect(L2.outPort, outPort) annotation (points=[76, -73; 90, -73; 90, 0; 
          100, 0], style(color=3));
    connect(InPort1, altSig.inPort) annotation (points=[-100, -60; -90, -60; -
          90, -90; -72, -90], style(color=3));
    connect(altSig.outPort, P5.inPort2) annotation (points=[-59, -90; 40, -90
          ; 40, -76; 43, -76], style(color=3));
    connect(InPort1, altLim.inPort) annotation (points=[-100, -60; -90, -60; -
          90, -70; -72, -70], style(color=3));
    connect(TimePort, BT1.inPort) annotation (points=[0, 80; 0, 63; 27, 63], 
        style(color=3));
    connect(TimePort, CT1.inPort) annotation (points=[0, 80; 0, 63; 15, 63; 15
          , 40; 27, 40], style(color=3));
    connect(TimePort, AT1.inPort) annotation (points=[0, 80; 0, 63; 15, 63; 15
          , 88; 27, 88], style(color=3));
    Time = TimePort.signal[1];
    ATime = AT1.y;
    BTime = BT1.y;
    CTime = CT1.y;
    altL = altLim.y[1];
    inc = u;
    alt = InPort1.signal[1];
    idn = ATime/exp(BTime/altL);
    dir = idn*inc*(area - OH*W*(alt/sqrt(1 - alt*alt)));
    dif = idn*area*(CTime*(1 + cos(tiltr))/2 + (CTime + alt)*ro*(1 - cos(tiltr
      ))/2);
    Ldir.u[1] = dir;
    Gdif.u[1] = dif;
  end SolarGlass;
  block DirectGlassAbsorption 
    extends Modelica.Blocks.Interfaces.SISO;
    output Real inc "Inclination";
    output Real absor "Absorption";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Text(
          extent=[-140, 8; -114, -10], 
          string="inc", 
          style(color=49)), 
        Text(extent=[-60, 78; 60, 38], string="Absorption of"), 
        Text(extent=[-80, 38; 82, -2], string="direct solar radiation"), 
        Text(extent=[-60, -2; 60, -42], string="for DSA glass")), 
      Icon(
        Text(extent=[-60, 0; 60, -40], string="for DSA glass"), 
        Text(extent=[-80, 40; 82, 0], string="direct solar radiation"), 
        Text(extent=[-60, 80; 60, 40], string="Absorption of"), 
        Text(
          extent=[-140, 8; -114, -10], 
          string="inc", 
          style(color=49))));
  equation 
    inc = u;
    absor = 0.01154 + 0.77674*inc - 3.94657*inc*inc + 8.57881*inc^3 - 8.38135*
      inc^4 + 3.01188*inc^5;
    y = absor;
  end DirectGlassAbsorption;
  block DiffuseGlassAbsorption 
    extends Modelica.Blocks.Interfaces.SO;
    output Real absor "Absorption";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Text(extent=[-60, 80; 60, 40], string="Absorption of"), 
        Text(extent=[-80, 40; 82, 0], string="diffuse solar radiation"), 
        Text(extent=[-60, 0; 60, -40], string="for DSA glass")), 
      Icon(
        Text(extent=[-60, 80; 60, 40], string="Absorption of"), 
        Text(extent=[-80, 40; 82, 0], string="diffuse solar radiation"), 
        Text(extent=[-60, 0; 60, -40], string="for DSA glass")));
  equation 
    absor = 0.01154/2 + 0.77674/3 - 3.94657/4 + 8.57881/5 - 8.38135/6 + 
      3.01188/7;
    y = absor;
  end DiffuseGlassAbsorption;
  block DirectGlassTransmission 
    extends Modelica.Blocks.Interfaces.SISO;
    output Real inc "Inclination";
    output Real trans "Transmission";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Text(extent=[-60, 78; 60, 38], string="Transmission of"), 
        Text(extent=[-80, 38; 82, -2], string="direct solar radiation"), 
        Text(extent=[-60, -2; 60, -42], string="for DSA glass"), 
        Text(
          extent=[-140, 8; -114, -10], 
          string="inc", 
          style(color=49))), 
      Icon(
        Text(extent=[-60, 78; 60, 38], string="Transmission of"), 
        Text(extent=[-80, 38; 82, -2], string="direct solar radiation"), 
        Text(extent=[-60, -2; 60, -42], string="for DSA glass"), 
        Text(
          extent=[-140, 8; -114, -10], 
          string="inc", 
          style(color=49))));
  equation 
    inc = u;
    trans = -0.00885 + 2.71235*inc - 0.62062*inc*inc - 7.07329*inc^3 + 9.75995
      *inc^4 - 3.89922*inc^5;
    y = trans;
  end DirectGlassTransmission;
  block DiffuseGlassTransmission 
    extends Modelica.Blocks.Interfaces.SO;
    output Real trans "Transmission";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Text(extent=[-60, 78; 60, 38], string="Transmission of"), 
        Text(extent=[-80, 38; 82, -2], string="diffuse solar radiation"), 
        Text(extent=[-60, -2; 60, -42], string="for DSA glass")), 
      Icon(
        Text(extent=[-60, 78; 60, 38], string="Transmission of"), 
        Text(extent=[-80, 38; 82, -2], string="diffuse solar radiation"), 
        Text(extent=[-60, -2; 60, -42], string="for DSA glass")));
  equation 
    trans = -0.00885/2 + 2.71235/3 - 0.62062/4 - 7.07329/5 + 9.75995/6 - 
      3.89922/7;
    y = trans;
  end DiffuseGlassTransmission;
  model C1D 
    extends BondLib.Interfaces.TwoPort;
    parameter Real theta "Thermal Resistance";
    parameter Real gamma "Thermal Capacity";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Text(
          extent=[-78, 52; 78, -54], 
          string="C1D", 
          style(color=0, pattern=0)), 
        Text(extent=[-70, 90; 70, 50], string="%name"), 
        Line(points=[-64, -44; 64, -44], style(color=9)), 
        Line(points=[40, -34; 64, -44; 40, -54], style(color=9))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    BondLib.Bonds.fBond B1 annotation (extent=[-60, -10; -40, 10]);
    BondLib.Bonds.fBond B3 annotation (extent=[-20, 30; 0, 50]);
    BondLib.Bonds.eBond B2 annotation (extent=[0, -10; 20, 10]);
    BondLib.Bonds.eBond B4 annotation (extent=[20, 30; 40, 50]);
    BondLib.Bonds.eBond B5 annotation (extent=[40, -30; 60, -10], rotation=-90
      );
    BondLib.Junctions.J1p3 J1p3_1 annotation (extent=[-40, 10; -20, -10]);
    BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-80, -10; -60, 10]);
    BondLib.Junctions.J0p4 J0p4_1 annotation (extent=[40, -10; 60, 10]);
    BondLib.Thermal.RSth RSth1(theta=theta) annotation (extent=[0, 30; 20, 50]
      );
    BondLib.Thermal.Cth Cth1(gamma=gamma, e(start=288)) annotation (extent=[40
          , -50; 60, -30], rotation=-90);
  equation 
    connect(BondCon1, J0p2_1.BondCon1) annotation (points=[-94, 0; -80, 0], 
        style(color=8));
    connect(J0p2_1.BondCon2, B1.eBondCon1) annotation (points=[-60, 0; -60, 0]
        , style(color=8));
    connect(B1.fBondCon1, J1p3_1.BondCon1) annotation (points=[-40, 0; -40, 0]
        , style(color=8));
    connect(J1p3_1.BondCon3, B3.eBondCon1) annotation (points=[-30, 10; -30, 
          40; -20, 40], style(color=8));
    connect(B3.fBondCon1, RSth1.BondCon1) annotation (points=[0, 40; 0, 40], 
        style(color=8));
    connect(RSth1.BondCon2, B4.fBondCon1) annotation (points=[20, 40; 20, 40]
        , style(color=8));
    connect(B4.eBondCon1, J0p4_1.BondCon4) annotation (points=[40, 40; 50, 40
          ; 50, 10], style(color=8));
    connect(J1p3_1.BondCon2, B2.fBondCon1) annotation (points=[-20, 0; 0, 0], 
        style(color=8));
    connect(B2.eBondCon1, J0p4_1.BondCon1) annotation (points=[20, 0; 40, 0], 
        style(color=8));
    connect(J0p4_1.BondCon3, B5.fBondCon1) annotation (points=[50, -10; 50, -
          10], style(color=8));
    connect(B5.eBondCon1, Cth1.BondCon1) annotation (points=[50, -30; 50, -30]
        , style(color=8));
    connect(J0p4_1.BondCon2, BondCon2) annotation (points=[60, 0; 94, 0], 
        style(color=8));
  end C1D;
  model C1V 
    extends BondLib.Interfaces.TwoPort;
    parameter Real theta "Thermal Resistance";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Text(
          extent=[-78, 52; 78, -54], 
          string="C1V", 
          style(color=0, pattern=0)), 
        Text(extent=[-70, 90; 70, 50], string="%name"), 
        Line(points=[-64, -44; 64, -44], style(color=9)), 
        Line(points=[40, -34; 64, -44; 40, -54], style(color=9))), 
      Window(
        x=0.03, 
        y=0.02, 
        width=0.44, 
        height=0.65));
    BondLib.Bonds.fBond B1 annotation (extent=[-60, -10; -40, 10]);
    BondLib.Bonds.fBond B3 annotation (extent=[-20, 30; 0, 50]);
    BondLib.Bonds.eBond B2 annotation (extent=[0, -10; 20, 10]);
    BondLib.Bonds.eBond B4 annotation (extent=[20, 30; 40, 50]);
    BondLib.Junctions.J1p3 J1p3_1 annotation (extent=[-40, 10; -20, -10]);
    BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-80, -10; -60, 10]);
    BondLib.Thermal.RSth RSth1(theta=theta) annotation (extent=[0, 30; 20, 50]
      );
    BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[40, 10; 60, -10]);
  equation 
    connect(J0p2_1.BondCon2, B1.eBondCon1) annotation (points=[-60, 0; -60, 
          0], style(color=8));
    connect(B1.fBondCon1, J1p3_1.BondCon1) annotation (points=[-40, 0; -40, 0]
        , style(color=8));
    connect(J1p3_1.BondCon3, B3.eBondCon1) annotation (points=[-30, 10; -30, 
          40; -20, 40], style(color=8));
    connect(B3.fBondCon1, RSth1.BondCon1) annotation (points=[0, 40; 0, 40], 
        style(color=8));
    connect(RSth1.BondCon2, B4.fBondCon1) annotation (points=[20, 40; 20, 40]
        , style(color=8));
    connect(J1p3_1.BondCon2, B2.fBondCon1) annotation (points=[-20, 0; 0, 0], 
        style(color=8));
    connect(BondCon1, J0p2_1.BondCon1) annotation (points=[-94, 0; -80, 0], 
        style(color=8));
    connect(B4.eBondCon1, J0p3_1.BondCon3) annotation (points=[40, 40; 50, 40
          ; 50, 10], style(color=8));
    connect(B2.eBondCon1, J0p3_1.BondCon1) annotation (points=[20, 0; 40, 0], 
        style(color=8));
    connect(J0p3_1.BondCon2, BondCon2) annotation (points=[60, 0; 94, 0], 
        style(color=8));
  end C1V;
  model ExWall 
    extends BondLib.Interfaces.ActiveOnePort;
    parameter Real lv1=22.7 
      "Specific thermal conductance of convection outside";
    parameter Real dxv1=1 "Width of convective layer outside";
    parameter Real area "Area of external wall";
    parameter Real thv1=dxv1/(lv1*area) 
      "Thermal resistance of convection outside";
    parameter Real ld=1.7 "Specific thermal conductance of dry wall";
    parameter Real dxd=0.14 "Thickness of conduction layer";
    parameter Real theta=dxd/(ld*area) "Thermal resistance of dry wall layer";
    parameter Real lv2=8.3 "Specific thermal conductance of convection inside"
      ;
    parameter Real dxv2=1 "Width of convective layer inside";
    parameter Real thv2=dxv2/(lv2*area) 
      "Thermal resistance of convection inside";
    parameter Real c=840 "Specific heat capacity of dry wall";
    parameter Real dens=1762 "Density of external wall";
    parameter Real thk=0.41 "Thickness of external wall";
    parameter Real m=area*thk*dens/4 "Mass of external wall segment";
    parameter Real gamma=c*m/3600 "Heat capacity of external wall segment";
    parameter Real sfa "Orientation of wall";
    parameter Real tilt=90 "Tilt of wall";
    parameter Real OH;
    parameter Real W;
    parameter Real absor=0.5 "Absorption coefficient of wall";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Window(
        x=0.02, 
        y=0.01, 
        width=0.44, 
        height=0.65), 
      Icon(Text(
          extent=[-74, 68; 86, -66], 
          string="ExWall", 
          style(color=0)), Text(extent=[-70, 90; 70, 50], string="%name")), 
      Diagram);
    Temperature Temperature1 annotation (extent=[-50, 30; -30, 50]);
    SolarPosition SolarPosition1(sfa=sfa, tilt=tilt) annotation (extent=[-50, 
          60; -30, 80]);
    SolarRadiation SolarRadiation1(
      tilt=tilt, 
      absor=absor, 
      area=area, 
      OH=OH, 
      W=W) annotation (extent=[-20, 60; 0, 80]);
    C1D C1D1(theta=theta, gamma=gamma) annotation (extent=[40, -10; 60, 10]);
    C1D C1D3(theta=theta, gamma=gamma) annotation (extent=[10, -90; -10, -70])
      ;
    C1D C1D2(theta=theta, gamma=gamma) annotation (extent=[70, -70; 90, -50], 
        rotation=-90);
    C1V C1V1(theta=thv1) annotation (extent=[-40, -10; -20, 10]);
    C1V C1V2(theta=thv2) annotation (extent=[-70, -90; -90, -70]);
    Modelica.Blocks.Sources.Clock Clock1 annotation (extent=[-80, 60; -60, 80]
      );
    BondLib.Sources.mSE mSE1 annotation (extent=[0, 30; -20, 50]);
    BondLib.Sources.mSF mSF1 annotation (extent=[30, 60; 10, 80]);
    BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-80, -10; -60, 10]);
    BondLib.Bonds.fBond B2 annotation (extent=[-60, -10; -40, 10]);
    BondLib.Bonds.fBond B1 annotation (extent=[0, 30; 20, 50]);
    BondLib.Bonds.eBond B3 annotation (extent=[-20, -10; 0, 10]);
    BondLib.Bonds.eBond B4 annotation (extent=[30, 60; 50, 80]);
    BondLib.Bonds.eBond B5 annotation (extent=[0, -30; 20, -10], rotation=-90)
      ;
    BondLib.Thermal.Cth Cth1(gamma=gamma, e(start=288)) annotation (extent=[0
          , -50; 20, -30], rotation=-90);
    BondLib.Bonds.fBond B6 annotation (extent=[20, -10; 40, 10]);
    BondLib.Bonds.fBond B7 annotation (extent=[60, -10; 80, 10]);
    BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[70, -30; 90, -10], 
        rotation=90);
    BondLib.Bonds.fBond B8 annotation (extent=[70, -50; 90, -30], rotation=270
      );
    BondLib.Bonds.fBond B9 annotation (extent=[50, -90; 70, -70], rotation=180
      );
    BondLib.Junctions.J0p2 J0p2_3 annotation (extent=[30, -90; 50, -70]);
    BondLib.Bonds.fBond B10 annotation (extent=[10, -90; 30, -70], rotation=
          180);
    BondLib.Junctions.J0p2 J0p2_4 annotation (extent=[-50, -90; -30, -70]);
    BondLib.Bonds.fBond B12 annotation (extent=[-70, -90; -50, -70], rotation=
          180);
    BondLib.Bonds.eBond B13 annotation (extent=[-110, -70; -90, -50], rotation
        =90);
    BondLib.Junctions.J0p2 J0p2_5 annotation (extent=[-110, -50; -90, -30], 
        rotation=90);
    BondLib.Junctions.J0p5 J0p5_1 annotation (extent=[0, 10; 20, -10]);
    BondLib.Sensors.E E1 annotation (extent=[20, 44; 0, 64]);
    BondLib.Bonds.fBond B14 annotation (extent=[20, 44; 40, 64], rotation=180)
      ;
    BondLib.Bonds.fBond B11 annotation (extent=[-30, -90; -10, -70], rotation=
          180);
  equation 
    connect(Clock1.outPort, SolarPosition1.inPort) annotation (points=[-58, 
          70; -52, 70], style(color=3));
    connect(SolarPosition1.outPort, SolarRadiation1.inPort) annotation (points
        =[-28, 70; -24, 70], style(color=3));
    connect(SolarPosition1.OutPort1, SolarRadiation1.InPort1) annotation (
        points=[-28, 64; -24, 64], style(color=3));
    connect(SolarRadiation1.outPort, mSF1.InPort1) annotation (points=[2, 70; 
          10, 70], style(color=3));
    connect(Temperature1.outPort, mSE1.InPort1) annotation (points=[-28, 40; -
          20, 40], style(color=3));
    connect(J0p2_1.BondCon2, B2.eBondCon1) annotation (points=[-60, 0; -60, 0]
        , style(color=8));
    connect(mSE1.BondCon1, B1.eBondCon1) annotation (points=[0, 40; 0, 40], 
        style(color=8));
    connect(B1.fBondCon1, J0p2_1.BondCon1) annotation (points=[20, 40; 26, 40
          ; 26, 22; -86, 22; -86, 0; -80, 0], style(color=8));
    connect(B2.fBondCon1, C1V1.BondCon1) annotation (points=[-40, 0; -40, 0], 
        style(color=8));
    connect(C1V1.BondCon2, B3.fBondCon1) annotation (points=[-20, 0; -20, 0], 
        style(color=8));
    connect(mSF1.BondCon1, B4.fBondCon1) annotation (points=[30, 70; 30, 70], 
        style(color=8));
    connect(B5.eBondCon1, Cth1.BondCon1) annotation (points=[10, -30; 10, -30]
        , style(color=8));
    connect(B6.fBondCon1, C1D1.BondCon1) annotation (points=[40, 0; 40, 0], 
        style(color=8));
    connect(C1D1.BondCon2, B7.eBondCon1) annotation (points=[60, 0; 60, 0], 
        style(color=8));
    connect(B7.fBondCon1, J0p2_2.BondCon2) annotation (points=[80, 0; 80, -10]
        , style(color=8));
    connect(B9.fBondCon1, J0p2_3.BondCon2) annotation (points=[50, -80; 50, -
          80], style(color=8));
    connect(J0p2_3.BondCon1, B10.eBondCon1) annotation (points=[30, -80; 30, -
          80], style(color=8));
    connect(B10.fBondCon1, C1D3.BondCon1) annotation (points=[10, -80; 10, -80
          ], style(color=8));
    connect(J0p2_4.BondCon1, B12.eBondCon1) annotation (points=[-50, -80; -50
          , -80], style(color=8));
    connect(B12.fBondCon1, C1V2.BondCon1) annotation (points=[-70, -80; -70, -
          80], style(color=8));
    connect(Clock1.outPort, Temperature1.inPort) annotation (points=[-58, 70; 
          -56, 70; -56, 40; -54, 40], style(color=3));
    connect(Clock1.outPort, SolarRadiation1.TimePort) annotation (points=[-58
          , 70; -56, 70; -56, 90; -10, 90; -10, 80], style(color=3));
    connect(C1V2.BondCon2, B13.fBondCon1) annotation (points=[-90, -80; -100, 
          -80; -100, -70], style(color=8));
    connect(B13.eBondCon1, J0p2_5.BondCon1) annotation (points=[-100, -50; -
          100, -50], style(color=8));
    connect(J0p2_5.BondCon2, BondCon1) annotation (points=[-100, -30; -100, -6
          ], style(color=8));
    connect(B3.eBondCon1, J0p5_1.BondCon1) annotation (points=[0, 0; 0, 0], 
        style(color=8));
    connect(J0p5_1.BondCon2, B6.eBondCon1) annotation (points=[20, 0; 20, 0], 
        style(color=8));
    connect(J0p5_1.BondCon5, B5.fBondCon1) annotation (points=[10, -10; 10, -
          10], style(color=8));
    connect(B14.fBondCon1, E1.BondCon1) annotation (points=[20, 54; 20, 54], 
        style(color=8));
    connect(J0p5_1.BondCon3, B14.eBondCon1) annotation (points=[6, 10; 6, 18; 
          46, 18; 46, 54; 40, 54], style(color=8));
    connect(B4.eBondCon1, J0p5_1.BondCon4) annotation (points=[50, 70; 60, 70
          ; 60, 14; 14, 14; 14, 10], style(color=8));
    connect(E1.OutPort1, SolarRadiation1.TempPort) annotation (points=[-2, 54
          ; -10, 54; -10, 60], style(color=3));
    connect(J0p2_2.BondCon1, B8.eBondCon1) annotation (points=[80, -30; 80, -
          30], style(color=8));
    connect(B8.fBondCon1, C1D2.BondCon1) annotation (points=[80, -50; 80, -50]
        , style(color=8));
    connect(C1D2.BondCon2, B9.eBondCon1) annotation (points=[80, -70; 80, -80
          ; 70, -80], style(color=8));
    connect(C1D3.BondCon2, B11.eBondCon1) annotation (points=[-10, -80; -10, -
          80], style(color=8));
    connect(B11.fBondCon1, J0p2_4.BondCon2) annotation (points=[-30, -80; -30
          , -80], style(color=8));
  end ExWall;
  model IntWall 
    extends BondLib.Interfaces.TwoPort;
    
    parameter Real lv=8.3 "Specific thermal conductance of convection";
    parameter Real dxv=1 "Width of convective layer";
    parameter Real area "Area of internal wall";
    parameter Real thv=dxv/(lv*area) "Thermal resistance of convection";
    parameter Real ld=1.7 "Specific thermal conductance of dry wall";
    parameter Real dxd=0.1 "Thickness of conduction layer";
    parameter Real theta=dxd/(ld*area) "Thermal resistance of dry wall layer";
    parameter Real c=840 "Specific heat capacity of dry wall";
    parameter Real dens=1762 "Density of internal wall";
    parameter Real thk=0.3 "Thickness of internal wall";
    parameter Real m=area*thk*dens/4 "Mass of internal wall segment";
    parameter Real gamma=c*m/3600 "Heat capacity of internal wall segment";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Text(
          extent=[-74, 68; 86, -66], 
          string="IntWall", 
          style(color=0)), 
        Text(extent=[-70, 90; 70, 50], string="%name"), 
        Line(points=[-64, -44; 64, -44], style(color=9)), 
        Line(points=[40, -34; 64, -44; 40, -54], style(color=9))), 
      Diagram(
        Line(points=[0, 0; 0, 0], style(color=8)), 
        Line(points=[10, -10; 10, -10], style(color=8)), 
        Line(points=[20, 0; 20, 0], style(color=8))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    C1D C1D1(theta=theta, gamma=gamma) annotation (extent=[40, -10; 60, 10]);
    C1D C1D3(theta=theta, gamma=gamma) annotation (extent=[-10, -70; -30, -50]
      );
    C1D C1D2(theta=theta, gamma=gamma) annotation (extent=[70, -70; 50, -50]);
    C1V C1V1(theta=thv) annotation (extent=[-40, -10; -20, 10]);
    C1V C1V2(theta=thv) annotation (extent=[-50, -90; -30, -70]);
    BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-80, -10; -60, 10]);
    BondLib.Bonds.fBond B1 annotation (extent=[-60, -10; -40, 10]);
    BondLib.Bonds.eBond B2 annotation (extent=[-20, -10; 0, 10]);
    BondLib.Bonds.eBond B3 annotation (extent=[0, -30; 20, -10], rotation=-90)
      ;
    BondLib.Thermal.Cth Cth1(gamma=gamma, e(start=288)) annotation (extent=[0
          , -50; 20, -30], rotation=-90);
    BondLib.Bonds.fBond B4 annotation (extent=[20, -10; 40, 10]);
    BondLib.Bonds.fBond B5 annotation (extent=[60, -10; 80, 10]);
    BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[70, -30; 90, -10], 
        rotation=90);
    BondLib.Bonds.fBond B6 annotation (extent=[70, -50; 90, -30], rotation=270
      );
    BondLib.Bonds.fBond B7 annotation (extent=[30, -70; 50, -50], rotation=180
      );
    BondLib.Junctions.J0p2 J0p2_3 annotation (extent=[10, -70; 30, -50]);
    BondLib.Bonds.fBond B8 annotation (extent=[-10, -70; 10, -50], rotation=
          180);
    BondLib.Junctions.J0p2 J0p2_4 annotation (extent=[-70, -70; -50, -50]);
    BondLib.Bonds.fBond B9 annotation (extent=[-50, -70; -30, -50], rotation=
          180);
    BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[0, -10; 20, 10]);
    BondLib.Bonds.eBond B10 annotation (extent=[-70, -90; -50, -70]);
    BondLib.Junctions.J0p2 J0p2_5 annotation (extent=[-10, -90; 10, -70]);
    BondLib.Bonds.eBond B11 annotation (extent=[-30, -90; -10, -70]);
  equation 
    connect(J0p2_1.BondCon2, B1.eBondCon1) annotation (points=[-60, 0; -60, 
          0], style(color=8));
    connect(B1.fBondCon1, C1V1.BondCon1) annotation (points=[-40, 0; -40, 0], 
        style(color=8));
    connect(C1V1.BondCon2, B2.fBondCon1) annotation (points=[-20, 0; -20, 0], 
        style(color=8));
    connect(B3.eBondCon1, Cth1.BondCon1) annotation (points=[10, -30; 10, -30]
        , style(color=8));
    connect(B4.fBondCon1, C1D1.BondCon1) annotation (points=[40, 0; 40, 0], 
        style(color=8));
    connect(C1D1.BondCon2, B5.eBondCon1) annotation (points=[60, 0; 60, 0], 
        style(color=8));
    connect(B5.fBondCon1, J0p2_2.BondCon2) annotation (points=[80, 0; 80, -10]
        , style(color=8));
    connect(B2.eBondCon1, J0p3_1.BondCon1) annotation (points=[0, 0; 0, 0], 
        style(color=8));
    connect(J0p3_1.BondCon3, B3.fBondCon1) annotation (points=[10, -10; 10, -
          10], style(color=8));
    connect(J0p3_1.BondCon2, B4.eBondCon1) annotation (points=[20, 0; 20, 0], 
        style(color=8));
    connect(BondCon1, J0p2_1.BondCon1) annotation (points=[-94, 0; -80, 0], 
        style(color=8));
    connect(J0p2_2.BondCon1, B6.eBondCon1) annotation (points=[80, -30; 80, -
          30], style(color=8));
    connect(B6.fBondCon1, C1D2.BondCon1) annotation (points=[80, -50; 80, -60
          ; 70, -60], style(color=8));
    connect(C1D2.BondCon2, B7.eBondCon1) annotation (points=[50, -60; 50, -60]
        , style(color=8));
    connect(B7.fBondCon1, J0p2_3.BondCon2) annotation (points=[30, -60; 30, -
          60], style(color=8));
    connect(J0p2_3.BondCon1, B8.eBondCon1) annotation (points=[10, -60; 10, -
          60], style(color=8));
    connect(B8.fBondCon1, C1D3.BondCon1) annotation (points=[-10, -60; -10, -
          60], style(color=8));
    connect(C1D3.BondCon2, B9.eBondCon1) annotation (points=[-30, -60; -30, -
          60], style(color=8));
    connect(B9.fBondCon1, J0p2_4.BondCon2) annotation (points=[-50, -60; -50, 
          -60], style(color=8));
    connect(J0p2_4.BondCon1, B10.fBondCon1) annotation (points=[-70, -60; -80
          , -60; -80, -80; -70, -80], style(color=8));
    connect(B10.eBondCon1, C1V2.BondCon1) annotation (points=[-50, -80; -50, -
          80], style(color=8));
    connect(C1V2.BondCon2, B11.fBondCon1) annotation (points=[-30, -80; -30, -
          80], style(color=8));
    connect(B11.eBondCon1, J0p2_5.BondCon1) annotation (points=[-10, -80; -10
          , -80], style(color=8));
    connect(J0p2_5.BondCon2, BondCon2) annotation (points=[10, -80; 100, -80; 
          100, -6], style(color=8));
  end IntWall;
  model Roof 
    extends BondLib.Interfaces.ActiveOnePort;
    parameter Real lv1=22.7 
      "Specific thermal conductance of convection outside";
    parameter Real dxv1=1 "Width of convective layer outside";
    parameter Real area "Area of roof";
    parameter Real thv1=dxv1/(lv1*area) 
      "Thermal resistance of convection outside";
    parameter Real ld1=45.3 
      "Specific thermal conductance of outmost roof layer";
    parameter Real dxd1=0.038 "Thickness of outmost roof layer";
    parameter Real theta1=dxd1/(ld1*area) 
      "Thermal resistance of outmost layer";
    parameter Real ld2=0.0385 
      "Specific thermal conductance of center roof layer";
    parameter Real dxd2=0.2 "Thickness of center roof layer";
    parameter Real theta2=dxd2/(ld2*area) "Thermal resistance of center layer"
      ;
    parameter Real ld3=0.1143 
      "Specific thermal conductance of inner roof layer";
    parameter Real dxd3=0.016 "Thickness of inner roof layer";
    parameter Real theta3=dxd3/(ld3*area) "Thermal resistance of inner layer";
    parameter Real lv2=7.4 "Specific thermal conductance of convection inside"
      ;
    parameter Real dxv2=1 "Width of convective layer inside";
    parameter Real thv2=dxv2/(lv2*area) 
      "Thermal resistance of convection inside";
    parameter Real c1=500 "Specific heat capacity of outmost roof layer";
    parameter Real dens1=7850 "Density of outmost roof layer";
    parameter Real m1=area*ld1*dens1 "Mass of outmost roof layer";
    parameter Real gamma1=c1*m1/3600 "Heat capacity of outmost roof layer";
    parameter Real c2=657 "Specific heat capacity of outmost roof layer";
    parameter Real dens2=19.2 "Density of outmost roof layer";
    parameter Real m2=0.5*area*ld2*dens2 "Mass of outmost roof layer";
    parameter Real gamma2=c2*m2/3600 "Heat capacity of outmost roof layer";
    parameter Real c3=1214 "Specific heat capacity of outmost roof layer";
    parameter Real dens3=544 "Density of outmost roof layer";
    parameter Real m3=area*ld3*dens3 "Mass of outmost roof layer";
    parameter Real gamma3=c3*m3/3600 "Heat capacity of outmost roof layer";
    parameter Real sfa=180 "Orientation of wall";
    parameter Real tilt=7 "Tilt of wall";
    parameter Real OH;
    parameter Real W;
    parameter Real absor=0.2 "Absorption coefficient of wall";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(Text(
          extent=[-74, 68; 86, -66], 
          string="Roof", 
          style(color=0)), Text(extent=[-70, 90; 70, 50], string="%name")), 
      Diagram, 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    Temperature Temperature1 annotation (extent=[-50, 30; -30, 50]);
    SolarPosition SolarPosition1(sfa=sfa, tilt=tilt) annotation (extent=[-50, 
          60; -30, 80]);
    SolarRadiation SolarRadiation1(
      tilt=tilt, 
      absor=absor, 
      area=area, 
      OH=OH, 
      W=W) annotation (extent=[-20, 60; 0, 80]);
    C1D C1D1(theta=theta1, gamma=gamma2) annotation (extent=[40, -10; 60, 10])
      ;
    C1V C1V1(theta=thv1) annotation (extent=[-40, -10; -20, 10]);
    Modelica.Blocks.Sources.Clock Clock1 annotation (extent=[-80, 60; -60, 80]
      );
    BondLib.Sources.mSE mSE1 annotation (extent=[0, 30; -20, 50]);
    BondLib.Sources.mSF mSF1 annotation (extent=[30, 60; 10, 80]);
    BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-80, -10; -60, 10]);
    BondLib.Bonds.fBond B2 annotation (extent=[-60, -10; -40, 10]);
    BondLib.Bonds.fBond B1 annotation (extent=[0, 30; 20, 50]);
    BondLib.Bonds.eBond B3 annotation (extent=[-20, -10; 0, 10]);
    BondLib.Bonds.eBond B4 annotation (extent=[30, 60; 50, 80]);
    BondLib.Bonds.eBond B5 annotation (extent=[0, -30; 20, -10], rotation=-90)
      ;
    BondLib.Thermal.Cth Cth1(gamma=gamma1, e(start=288)) annotation (extent=[0
          , -50; 20, -30], rotation=-90);
    BondLib.Bonds.fBond B6 annotation (extent=[20, -10; 40, 10]);
    BondLib.Bonds.fBond B7 annotation (extent=[60, -10; 80, 10]);
    BondLib.Junctions.J0p5 J0p5_1 annotation (extent=[0, 10; 20, -10]);
    BondLib.Sensors.E E1 annotation (extent=[20, 44; 0, 64]);
    BondLib.Bonds.fBond B14 annotation (extent=[20, 44; 40, 64], rotation=180)
      ;
    C1D C1D3(theta=theta3, gamma=gamma3) annotation (extent=[10, -90; -10, -70
          ]);
    C1D C1D2(theta=theta2, gamma=gamma2) annotation (extent=[70, -70; 90, -50]
        , rotation=-90);
    C1V C1V2(theta=thv2) annotation (extent=[-70, -90; -90, -70]);
    BondLib.Bonds.fBond B9 annotation (extent=[50, -90; 70, -70], rotation=180
      );
    BondLib.Junctions.J0p2 J0p2_3 annotation (extent=[30, -90; 50, -70]);
    BondLib.Bonds.fBond B10 annotation (extent=[10, -90; 30, -70], rotation=
          180);
    BondLib.Junctions.J0p2 J0p2_4 annotation (extent=[-50, -90; -30, -70]);
    BondLib.Bonds.fBond B12 annotation (extent=[-70, -90; -50, -70], rotation=
          180);
    BondLib.Bonds.eBond B13 annotation (extent=[-110, -70; -90, -50], rotation
        =90);
    BondLib.Bonds.fBond B11 annotation (extent=[-30, -90; -10, -70], rotation=
          180);
    BondLib.Junctions.J0p2 J0p2_5 annotation (extent=[-110, -50; -90, -30], 
        rotation=90);
    BondLib.Bonds.fBond B8 annotation (extent=[70, -50; 90, -30], rotation=270
      );
    BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[70, -30; 90, -10], 
        rotation=90);
  equation 
    connect(Clock1.outPort, SolarPosition1.inPort) annotation (points=[-58, 
          70; -52, 70], style(color=3));
    connect(SolarPosition1.outPort, SolarRadiation1.inPort) annotation (points
        =[-28, 70; -24, 70], style(color=3));
    connect(SolarPosition1.OutPort1, SolarRadiation1.InPort1) annotation (
        points=[-28, 64; -24, 64], style(color=3));
    connect(SolarRadiation1.outPort, mSF1.InPort1) annotation (points=[2, 70; 
          10, 70], style(color=3));
    connect(Temperature1.outPort, mSE1.InPort1) annotation (points=[-28, 40; -
          20, 40], style(color=3));
    connect(J0p2_1.BondCon2, B2.eBondCon1) annotation (points=[-60, 0; -60, 0]
        , style(color=8));
    connect(mSE1.BondCon1, B1.eBondCon1) annotation (points=[0, 40; 0, 40], 
        style(color=8));
    connect(B1.fBondCon1, J0p2_1.BondCon1) annotation (points=[20, 40; 26, 40
          ; 26, 22; -86, 22; -86, 0; -80, 0], style(color=8));
    connect(B2.fBondCon1, C1V1.BondCon1) annotation (points=[-40, 0; -40, 0], 
        style(color=8));
    connect(C1V1.BondCon2, B3.fBondCon1) annotation (points=[-20, 0; -20, 0], 
        style(color=8));
    connect(mSF1.BondCon1, B4.fBondCon1) annotation (points=[30, 70; 30, 70], 
        style(color=8));
    connect(B5.eBondCon1, Cth1.BondCon1) annotation (points=[10, -30; 10, -30]
        , style(color=8));
    connect(B6.fBondCon1, C1D1.BondCon1) annotation (points=[40, 0; 40, 0], 
        style(color=8));
    connect(C1D1.BondCon2, B7.eBondCon1) annotation (points=[60, 0; 60, 0], 
        style(color=8));
    connect(Clock1.outPort, Temperature1.inPort) annotation (points=[-58, 70; 
          -56, 70; -56, 40; -54, 40], style(color=3));
    connect(Clock1.outPort, SolarRadiation1.TimePort) annotation (points=[-58
          , 70; -56, 70; -56, 90; -10, 90; -10, 80], style(color=3));
    connect(B3.eBondCon1, J0p5_1.BondCon1) annotation (points=[0, 0; 0, 0], 
        style(color=8));
    connect(J0p5_1.BondCon5, B5.fBondCon1) annotation (points=[10, -10; 10, -
          10], style(color=8));
    connect(J0p5_1.BondCon2, B6.eBondCon1) annotation (points=[20, 0; 20, 0], 
        style(color=8));
    connect(B14.fBondCon1, E1.BondCon1) annotation (points=[20, 54; 20, 54], 
        style(color=8));
    connect(E1.OutPort1, SolarRadiation1.TempPort) annotation (points=[-2, 54
          ; -10, 54; -10, 60], style(color=3));
    connect(J0p5_1.BondCon3, B14.eBondCon1) annotation (points=[6, 10; 6, 18; 
          46, 18; 46, 54; 40, 54], style(color=8));
    connect(B4.eBondCon1, J0p5_1.BondCon4) annotation (points=[50, 70; 60, 70
          ; 60, 14; 14, 14; 14, 10], style(color=8));
    connect(B9.fBondCon1, J0p2_3.BondCon2) annotation (points=[50, -80; 50, -
          80], style(color=8));
    connect(J0p2_3.BondCon1, B10.eBondCon1) annotation (points=[30, -80; 30, -
          80], style(color=8));
    connect(B10.fBondCon1, C1D3.BondCon1) annotation (points=[10, -80; 10, -80
          ], style(color=8));
    connect(J0p2_4.BondCon1, B12.eBondCon1) annotation (points=[-50, -80; -50
          , -80], style(color=8));
    connect(B12.fBondCon1, C1V2.BondCon1) annotation (points=[-70, -80; -70, -
          80], style(color=8));
    connect(C1V2.BondCon2, B13.fBondCon1) annotation (points=[-90, -80; -100, 
          -80; -100, -70], style(color=8));
    connect(C1D2.BondCon2, B9.eBondCon1) annotation (points=[80, -70; 80, -80
          ; 70, -80], style(color=8));
    connect(C1D3.BondCon2, B11.eBondCon1) annotation (points=[-10, -80; -10, -
          80], style(color=8));
    connect(B11.fBondCon1, J0p2_4.BondCon2) annotation (points=[-30, -80; -30
          , -80], style(color=8));
    connect(B13.eBondCon1, J0p2_5.BondCon1) annotation (points=[-100, -50; -
          100, -50], style(color=8));
    connect(J0p2_5.BondCon2, BondCon1) annotation (points=[-100, -30; -100, -6
          ], style(color=8));
    connect(B8.fBondCon1, C1D2.BondCon1) annotation (points=[80, -50; 80, -50]
        , style(color=8));
    connect(J0p2_2.BondCon1, B8.eBondCon1) annotation (points=[80, -30; 80, -
          30], style(color=8));
    connect(B7.fBondCon1, J0p2_2.BondCon2) annotation (points=[80, 0; 80, -10]
        , style(color=8));
  end Roof;
  model Window 
    extends BondLib.Interfaces.ActiveOnePort;
    parameter Real lv=3.4 "Specific thermal conductance of convection outside"
      ;
    parameter Real dxv=1 "Width of convective layer outside";
    parameter Real area "Area of window";
    parameter Real thv=dxv/(lv*area) 
      "Thermal resistance of convection outside";
    parameter Real tilt=90 "Tilt of window";
    parameter Real OH;
    parameter Real W;
    parameter Real sc=0.85;
    parameter Real sfa "Orientation of window";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(Text(
          extent=[-74, 68; 86, -66], 
          string="Window", 
          style(color=0)), Text(extent=[-70, 90; 70, 50], string="%name")), 
      Diagram, 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    Temperature Temperature1 annotation (extent=[-50, 30; -30, 50]);
    SolarPosition SolarPosition1(sfa=sfa, tilt=tilt) annotation (extent=[-50, 
          60; -30, 80]);
    C1V C1V1(theta=thv) annotation (extent=[-40, -10; -20, 10]);
    Modelica.Blocks.Sources.Clock Clock1 annotation (extent=[-80, 60; -60, 80]
      );
    BondLib.Sources.mSE mSE1 annotation (extent=[0, 30; -20, 50]);
    BondLib.Sources.mSF mSF1 annotation (extent=[30, 60; 10, 80]);
    BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-80, -10; -60, 10]);
    BondLib.Bonds.fBond B2 annotation (extent=[-60, -10; -40, 10]);
    BondLib.Bonds.fBond B1 annotation (extent=[0, 30; 20, 50]);
    BondLib.Bonds.eBond B3 annotation (extent=[-20, -10; 0, 10]);
    BondLib.Bonds.eBond B4 annotation (extent=[30, 60; 50, 80]);
    SolarGlass SolarGlass1(
      sc=sc, 
      tilt=tilt, 
      area=area, 
      OH=OH, 
      W=W) annotation (extent=[-20, 60; 0, 80]);
    BondLib.Sensors.E E1 annotation (extent=[20, 44; 0, 64]);
    BondLib.Bonds.fBond B5 annotation (extent=[20, 44; 40, 64], rotation=180);
    BondLib.Junctions.J0p4 J0p4_1 annotation (extent=[0, -10; 20, 10]);
  equation 
    connect(Clock1.outPort, SolarPosition1.inPort) annotation (points=[-58, 
          70; -52, 70], style(color=3));
    connect(Temperature1.outPort, mSE1.InPort1) annotation (points=[-28, 40; -
          20, 40], style(color=3));
    connect(J0p2_1.BondCon2, B2.eBondCon1) annotation (points=[-60, 0; -60, 0]
        , style(color=8));
    connect(mSE1.BondCon1, B1.eBondCon1) annotation (points=[0, 40; 0, 40], 
        style(color=8));
    connect(B1.fBondCon1, J0p2_1.BondCon1) annotation (points=[20, 40; 26, 40
          ; 26, 20; -86, 20; -86, 0; -80, 0], style(color=8));
    connect(B2.fBondCon1, C1V1.BondCon1) annotation (points=[-40, 0; -40, 0], 
        style(color=8));
    connect(C1V1.BondCon2, B3.fBondCon1) annotation (points=[-20, 0; -20, 0], 
        style(color=8));
    connect(mSF1.BondCon1, B4.fBondCon1) annotation (points=[30, 70; 30, 70], 
        style(color=8));
    connect(SolarPosition1.outPort, SolarGlass1.inPort) annotation (points=[-
          30, 70; -24, 70], style(color=3));
    connect(SolarPosition1.OutPort1, SolarGlass1.InPort1) annotation (points=[
          -28, 64; -24, 64], style(color=3));
    connect(SolarGlass1.outPort, mSF1.InPort1) annotation (points=[2, 70; 10, 
          70], style(color=3));
    connect(Clock1.outPort, Temperature1.inPort) annotation (points=[-58, 70; 
          -56, 70; -56, 40; -54, 40], style(color=3));
    connect(Clock1.outPort, SolarGlass1.TimePort) annotation (points=[-58, 70
          ; -56, 70; -56, 90; -10, 90; -10, 80], style(color=3));
    connect(B5.fBondCon1, E1.BondCon1) annotation (points=[20, 54; 20, 54], 
        style(color=8));
    connect(E1.OutPort1, SolarGlass1.TempPort) annotation (points=[-2, 54; -10
          , 54; -10, 60], style(color=3));
    connect(B3.eBondCon1, J0p4_1.BondCon1) annotation (points=[0, 0; 0, 0], 
        style(color=8));
    connect(J0p4_1.BondCon4, B5.eBondCon1) annotation (points=[10, 10; 10, 16
          ; 46, 16; 46, 54; 40, 54], style(color=8));
    connect(B4.eBondCon1, J0p4_1.BondCon2) annotation (points=[50, 70; 60, 70
          ; 60, 0; 20, 0], style(color=8));
    connect(J0p4_1.BondCon3, BondCon1) annotation (points=[10, -10; 10, -20; -
          100, -20; -100, -6], style(color=8));
  end Window;
  model Door 
    extends BondLib.Interfaces.TwoPort;
    parameter Real lv=0.189 
      "Specific thermal conductance of convection outside";
    parameter Real dxv=1 "Width of convective layer outside";
    parameter Real area "Area of door";
    parameter Real thv=dxv/(lv*area) 
      "Thermal resistance of convection outside";
    parameter Real sfa "Orientation of door";
    parameter Real tilt=90 "Tilt of door";
    parameter Real OH;
    parameter Real W;
    parameter Real absor=0.2 "Absorption coefficient of door";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Line(points=[10, -10; 10, -10], style(color=8)), 
        Line(points=[60, 0; 60, 0], style(color=8)), 
        Line(points=[40, 0; 40, 0], style(color=8)), 
        Line(points=[20, 0; 20, 0], style(color=8)), 
        Line(points=[0, 0; 0, 0], style(color=8)), 
        Line(points=[0, 40; 0, 40], style(color=8)), 
        Line(points=[-20, 0; -20, 30; -10, 30; -10, 60], style(color=53))), 
      Icon(
        Text(
          extent=[-78, 66; 82, -68], 
          string="Door", 
          style(color=0)), 
        Text(extent=[-70, 90; 70, 50], string="%name"), 
        Line(points=[-64, -44; 64, -44], style(color=9)), 
        Line(points=[40, -34; 64, -44; 40, -54], style(color=9))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    SolarPosition SolarPosition1(sfa=sfa, tilt=tilt) annotation (extent=[-50, 
          60; -30, 80]);
    C1V C1V1(theta=thv) annotation (extent=[0, -10; 20, 10]);
    Modelica.Blocks.Sources.Clock Clock1 annotation (extent=[-80, 60; -60, 80]
      );
    BondLib.Sources.mSF mSF1 annotation (extent=[30, 60; 10, 80]);
    BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-40, -10; -20, 10]);
    BondLib.Bonds.fBond B1 annotation (extent=[-20, -10; 0, 10]);
    BondLib.Bonds.eBond B2 annotation (extent=[20, -10; 40, 10]);
    BondLib.Bonds.eBond B3 annotation (extent=[30, 60; 50, 80]);
    BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[40, 10; 60, -10]);
    SolarRadiation SolarRadiation1(
      tilt=tilt, 
      absor=absor, 
      area=area, 
      OH=OH, 
      W=W) annotation (extent=[-20, 60; 0, 80]);
  equation 
    connect(Clock1.outPort, SolarPosition1.inPort) annotation (points=[-58, 
          70; -52, 70], style(color=3));
    connect(J0p2_1.BondCon2, B1.eBondCon1) annotation (points=[-20, 0; -20, 0]
        , style(color=8));
    connect(B1.fBondCon1, C1V1.BondCon1) annotation (points=[0, 0; 0, 0], 
        style(color=8));
    connect(C1V1.BondCon2, B2.fBondCon1) annotation (points=[20, 0; 20, 0], 
        style(color=8));
    connect(mSF1.BondCon1, B3.fBondCon1) annotation (points=[30, 70; 30, 70], 
        style(color=8));
    connect(B2.eBondCon1, J0p3_1.BondCon1) annotation (points=[40, 0; 40, 0], 
        style(color=8));
    connect(SolarPosition1.outPort, SolarRadiation1.inPort) annotation (points
        =[-28, 70; -24, 70], style(color=3));
    connect(SolarPosition1.OutPort1, SolarRadiation1.InPort1) annotation (
        points=[-28, 64; -24, 64], style(color=3));
    connect(SolarRadiation1.outPort, mSF1.InPort1) annotation (points=[2, 70; 
          10, 70], style(color=3));
    connect(BondCon1, J0p2_1.BondCon1) annotation (points=[-94, 0; -40, 0], 
        style(color=8));
    connect(J0p3_1.BondCon2, BondCon2) annotation (points=[60, 0; 94, 0], 
        style(color=8));
    connect(Clock1.outPort, SolarRadiation1.TimePort) annotation (points=[-58
          , 70; -56, 70; -56, 92; -10, 92; -10, 80], style(color=3));
    connect(B3.eBondCon1, J0p3_1.BondCon3) annotation (points=[50, 70; 50, 10]
        , style(color=8));
    SolarRadiation1.TempPort.signal[1] = J0p2_1.e1;
  end Door;
  model Slab 
    extends BondLib.Interfaces.TwoPort;
    parameter Real ld=5.11 "Specific thermal conductance of slab";
    parameter Real dxd=1 "Thickness of slab for conduction";
    parameter Real thk=0.2 "Thickness of slab";
    parameter Real perim "Area of floor for conduction";
    parameter Real aread=perim*thk "Effective area of floor for conduction";
    parameter Real theta=dxd/(ld*aread) "Thermal resistance of slab";
    parameter Real lv=7.4 "Specific thermal conductance of convection";
    parameter Real dxv=1 "Width of convective layer";
    parameter Real area "Area of floor for convection";
    parameter Real thv=dxv/(lv*area) "Thermal resistance of convection";
    parameter Real c=835 "Specific heat capacity of slab";
    parameter Real dens=2083 "Density of slab";
    parameter Real m=area*thk*dens "Mass of slab";
    parameter Real gamma=c*m/3600 "Heat capacity of slab";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Line(points=[0, 0; 0, 0], style(color=8)), 
        Line(points=[10, -10; 10, -10], style(color=8)), 
        Line(points=[20, 0; 20, 0], style(color=8)), 
        Line(points=[10, -10; 10, -10], style(color=8)), 
        Line(points=[0, 0; 0, 0], style(color=8)), 
        Line(points=[20, 0; 20, 0], style(color=8))), 
      Icon(
        Text(
          extent=[-74, 68; 86, -66], 
          string="Slab", 
          style(color=0)), 
        Text(extent=[-70, 90; 70, 50], string="%name"), 
        Line(points=[-64, -44; 64, -44], style(color=9)), 
        Line(points=[40, -34; 64, -44; 40, -54], style(color=9))));
    C1D C1D1(theta=theta, gamma=gamma) annotation (extent=[-50, -10; -30, 10])
      ;
    C1V C1V1(theta=thv) annotation (extent=[30, -10; 50, 10]);
    BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-90, -10; -70, 10]);
    BondLib.Bonds.fBond B1 annotation (extent=[-70, -10; -50, 10]);
    BondLib.Bonds.eBond B4 annotation (extent=[50, -10; 70, 10]);
    BondLib.Bonds.fBond B3 annotation (extent=[10, -10; 30, 10]);
    BondLib.Bonds.fBond B2 annotation (extent=[-30, -10; -10, 10]);
    BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[-10, -10; 10, 10]);
    BondLib.Junctions.J0p2 J0p2_3 annotation (extent=[70, -10; 90, 10]);
  equation 
    connect(J0p2_1.BondCon2, B1.eBondCon1) annotation (points=[-70, 0; -70, 
          0], style(color=8));
    connect(J0p2_2.BondCon2, B3.eBondCon1) annotation (points=[10, 0; 10, 0], 
        style(color=8));
    connect(B1.fBondCon1, C1D1.BondCon1) annotation (points=[-50, 0; -50, 0], 
        style(color=8));
    connect(B3.fBondCon1, C1V1.BondCon1) annotation (points=[30, 0; 30, 0], 
        style(color=8));
    connect(BondCon1, J0p2_1.BondCon1) annotation (points=[-94, 0; -90, 0], 
        style(color=8));
    connect(J0p2_3.BondCon2, BondCon2) annotation (points=[90, 0; 94, 0], 
        style(color=8));
    connect(C1D1.BondCon2, B2.eBondCon1) annotation (points=[-30, 0; -30, 0], 
        style(color=8));
    connect(B2.fBondCon1, J0p2_2.BondCon1) annotation (points=[-10, 0; -10, 0]
        , style(color=8));
    connect(C1V1.BondCon2, B4.fBondCon1) annotation (points=[50, 0; 50, 0], 
        style(color=8));
    connect(B4.eBondCon1, J0p2_3.BondCon1) annotation (points=[70, 0; 70, 0], 
        style(color=8));
  end Slab;
  model LivingRoom 
    parameter Real c=1000 "Specific heat capacity of living room";
    parameter Real m=265 "Mass of living room";
    parameter Real gamma=c*m/3600 "Heat capacity of living room";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[1, 1], 
        component=[10, 10]), 
      Diagram(
        Line(points=[-90, 90; -90, -40; 90, -40; 90, 20; -30, 20; -30, 90; -90
              , 90], style(color=41, thickness=2)), 
        Line(points=[-29, 90; 90, 90; 90, 20], style(color=57, thickness=2)), 
        Line(points=[30, 90; 30, 20], style(color=53, thickness=2)), 
        Line(points=[10, -40; 10, -80; 90, -80; 90, -41], style(color=53, 
              thickness=2)), 
        Rectangle(extent=[60, -38; 80, -42], style(color=41)), 
        Rectangle(extent=[-30, -38; -10, -42], style(color=41)), 
        Rectangle(extent=[-70, -38; -50, -42], style(color=41)), 
        Rectangle(extent=[-84, 92; -64, 88], style(color=41)), 
        Polygon(points=[-56, 90; -41, 75; -41, 90; -56, 90], style(color=41))
          , 
        Polygon(points=[20, 20; 20, 40; 5, 20; 20, 20], style(color=41)), 
        Polygon(points=[40, 20; 40, 40; 56, 20; 40, 20], style(color=41)), 
        Polygon(points=[19, -40; 19, -20; 34, -40; 19, -40], style(color=41))
          , 
        Rectangle(extent=[-10, 92; 10, 88], style(color=53)), 
        Rectangle(extent=[50, 92; 70, 88], style(color=53)), 
        Rectangle(extent=[10, -78; 30, -82], style(color=53)), 
        Rectangle(extent=[30, -78; 50, -82], style(color=53)), 
        Rectangle(extent=[50, -78; 70, -82], style(color=53)), 
        Rectangle(extent=[70, -78; 90, -82], style(color=53)), 
        Polygon(points=[10, -50; -10, -50; 10, -65; 10, -50], style(color=53))
          , 
        Text(
          extent=[-19, 69; 17, 54], 
          string="Bedroom 2", 
          style(color=53)), 
        Text(
          extent=[41, 70; 76, 54], 
          string="Bedroom 1", 
          style(color=53)), 
        Text(
          extent=[33, -58; 64, -70], 
          string="Sunspace", 
          style(color=53)), 
        Line(points=[50, -44; 50, -44], style(color=8)), 
        Line(points=[41, -64; 41, -64], style(color=8)), 
        Line(points=[70, -61; 70, -60], style(color=8))), 
      Icon(
        Line(points=[-90, 90; -90, -40; 90, -40; 90, 20; -30, 20; -30, 90; -90
              , 90], style(color=9, thickness=2)), 
        Line(points=[-29, 90; 90, 90; 90, 20], style(color=57, thickness=2)), 
        Line(points=[30, 90; 30, 20], style(color=53, thickness=2)), 
        Line(points=[10, -40; 10, -80; 90, -80; 90, -41], style(color=53, 
              thickness=2)), 
        Rectangle(extent=[60, -38; 80, -42], style(color=9)), 
        Rectangle(extent=[-30, -38; -10, -42], style(color=9)), 
        Rectangle(extent=[-70, -38; -50, -42], style(color=9)), 
        Rectangle(extent=[-84, 92; -64, 88], style(color=9)), 
        Polygon(points=[-55, 90; -40, 75; -40, 90; -55, 90], style(color=9)), 
        Polygon(points=[20, 20; 20, 40; 5, 20; 20, 20], style(color=9)), 
        Polygon(points=[40, 20; 40, 40; 56, 20; 40, 20], style(color=9)), 
        Polygon(points=[20, -40; 20, -20; 35, -40; 20, -40], style(color=9)), 
        Rectangle(extent=[-10, 92; 10, 88], style(color=53)), 
        Rectangle(extent=[50, 92; 70, 88], style(color=53)), 
        Rectangle(extent=[10, -78; 30, -82], style(color=53)), 
        Rectangle(extent=[30, -78; 50, -82], style(color=53)), 
        Rectangle(extent=[50, -78; 70, -82], style(color=53)), 
        Rectangle(extent=[70, -78; 90, -82], style(color=53)), 
        Polygon(points=[10, -50; -10, -50; 10, -65; 10, -50], style(color=53))
          , 
        Text(
          extent=[-19, 69; 17, 54], 
          string="Bedroom 2", 
          style(color=53)), 
        Text(
          extent=[41, 70; 76, 54], 
          string="Bedroom 1", 
          style(color=53)), 
        Text(
          extent=[35, -54; 66, -66], 
          string="Sunspace", 
          style(color=53)), 
        Text(
          extent=[-66, 3; 59, -17], 
          string="Living Room", 
          style(color=9))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    Window NWin(
      area=1.5, 
      OH=0, 
      W=0, 
      sfa=180) annotation (extent=[-85, 68; -66, 88], rotation=90);
    ExWall NWall(
      area=6.6, 
      sfa=180, 
      OH=0.3, 
      W=12) annotation (extent=[-70, 68; -50, 88], rotation=90);
    Door NDoor(
      lv=3.57, 
      area=2.17, 
      sfa=180, 
      OH=0, 
      W=0, 
      absor=0.6) annotation (extent=[-54, 68; -34, 88], rotation=270);
    ExWall WWall(
      area=20.7, 
      sfa=90, 
      OH=0.3, 
      W=8) annotation (extent=[-80, 20; -100, 40]);
    BondLib.Bonds.eBond NB1 annotation (extent=[-85, 48; -65, 68], rotation=-
          90);
    BondLib.Bonds.eBond NB2 annotation (extent=[-70, 48; -50, 68], rotation=-
          90);
    BondLib.Bonds.eBond NB3 annotation (extent=[-55, 48; -35, 68], rotation=-
          90);
    BondLib.Bonds.eBond WB1 annotation (extent=[-79, 22; -64, 37]);
    BondLib.Junctions.J0p5 J0p5_1 annotation (extent=[-63, 40; -43, 20]);
    Window SWin1(
      area=2.23, 
      OH=0, 
      W=0, 
      sfa=0) annotation (extent=[-70, -62; -50, -42], rotation=-90);
    Window SWin2(
      area=2.23, 
      OH=0, 
      W=0, 
      sfa=0) annotation (extent=[-29, -62; -9, -42], rotation=-90);
    ExWall SWall(
      area=15.7, 
      sfa=0, 
      OH=0.3, 
      W=12) annotation (extent=[-50, -62; -30, -42], rotation=-90);
    BondLib.Bonds.eBond SB1 annotation (extent=[-70, -42; -50, -22], rotation=
          90);
    BondLib.Bonds.eBond SB2 annotation (extent=[-50, -42; -30, -22], rotation=
          90);
    BondLib.Bonds.eBond SB3 annotation (extent=[-30, -42; -10, -22], rotation=
          90);
    BondLib.Bonds.eBond CB1 annotation (extent=[-62, 5; -47, 20], rotation=-90
      );
    BondLib.Junctions.J0p6 J0p6_1 annotation (extent=[-13, -10; 7, 10]);
    BondLib.Bonds.eBond CB2 annotation (extent=[-29, -7; -14, 8]);
    BondLib.Bonds.eBond CB3 annotation (extent=[7, -7; 22, 8], rotation=180);
    BondLib.Junctions.J0p5 J0p5_3 annotation (extent=[22, 10; 42, -10]);
    BondLib.Bonds.eBond CB4 annotation (extent=[42, -7; 57, 8], rotation=180);
    BondLib.Junctions.J0p6 J0p6_2 annotation (extent=[57, -10; 77, 10]);
    Door SsWin(
      lv=3.4, 
      area=1.1, 
      sfa=0, 
      OH=3.1, 
      W=5.4, 
      absor=0) annotation (extent=[60, -60; 80, -40], rotation=-90);
    Door SsDoor(
      lv=3.57, 
      area=2.17, 
      sfa=0, 
      OH=0, 
      W=0, 
      absor=0) annotation (extent=[15, -60; 35, -40], rotation=-90);
    IntWall SsWall(
      area=12.9, 
      dxd=0.14, 
      thk=0.41) annotation (extent=[42, -35; 57, -19], rotation=-90);
    BondLib.Bonds.fBond SB4 annotation (extent=[52, -15; 62, -5], rotation=180
      );
    BondLib.Bonds.eBond SB5 annotation (extent=[40, -55; 60, -35], rotation=-
          90);
    BondLib.Bonds.fBond NB4 annotation (extent=[2, 20; 17, 34], rotation=90);
    BondLib.Bonds.fBond NB5 annotation (extent=[17, 20; 32, 34], rotation=90);
    Temperature2 Temperature2_1 annotation (extent=[-84, 95; -74, 105]);
    Modelica.Blocks.Sources.Clock Clock1 annotation (extent=[-100, 95; -90, 
          105]);
    BondLib.Sources.mSE mSE1 annotation (extent=[-54, 94; -68, 107]);
    BondLib.Bonds.fBond NB6 annotation (extent=[-54, 95; -44, 105]);
    BondLib.Junctions.J0p4 J0p4_1 annotation (extent=[44, -65; 55, -55]);
    BondLib.Bonds.fBond SB6 annotation (extent=[17, -40; 32, -25], rotation=-
          90);
    BondLib.Bonds.fBond SB7 annotation (extent=[63, -40; 78, -25], rotation=-
          90);
    BondLib.Bonds.eBond SB8 annotation (extent=[31, -66; 44, -53]);
    BondLib.Bonds.eBond SB9 annotation (extent=[55, -67; 68, -54], rotation=
          180);
    BondLib.Interfaces.BondCon BondCon1 annotation (extent=[45, -77; 55, -67])
      ;
    IntWall WallL2(area=15.2) annotation (extent=[15, 34; 35, 54], rotation=90
      );
    Door DoorL2(
      lv=3.57, 
      area=2.17, 
      sfa=180, 
      OH=0, 
      W=0, 
      absor=0) annotation (extent=[-1, 34; 19, 54], rotation=90);
    BondLib.Bonds.eBond NB7 annotation (extent=[3, 55; 16, 68], rotation=90);
    BondLib.Bonds.eBond NB8 annotation (extent=[19, 55; 32, 68], rotation=90);
    BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[9, 75; 25, 62]);
    BondLib.Interfaces.BondCon BondCon2 annotation (extent=[12, 78; 22, 88]);
    BondLib.Bonds.fBond NB9 annotation (extent=[42, 20; 57, 34], rotation=90);
    BondLib.Bonds.fBond NB10 annotation (extent=[63, 20; 78, 34], rotation=90)
      ;
    Door DoorL1(
      lv=3.57, 
      area=2.17, 
      sfa=180, 
      OH=0, 
      W=0, 
      absor=0) annotation (extent=[40, 34; 60, 54], rotation=90);
    IntWall WallL1(area=6.6) annotation (extent=[60, 34; 80, 54], rotation=90)
      ;
    BondLib.Bonds.eBond NB11 annotation (extent=[43, 55; 56, 68], rotation=90)
      ;
    BondLib.Bonds.eBond NB12 annotation (extent=[64, 55; 77, 68], rotation=90)
      ;
    BondLib.Junctions.J0p3 J0p3_2 annotation (extent=[52, 75; 68, 62]);
    BondLib.Interfaces.BondCon BondCon3 annotation (extent=[55, 78; 65, 88]);
    BondLib.Bonds.eBond EB1 annotation (extent=[77, -6; 92, 9], rotation=180);
    ExWall EWall(
      area=11.3, 
      sfa=270, 
      OH=0.3, 
      W=8) annotation (extent=[92, -10; 112, 10]);
    BondLib.Bonds.eBond AB1 annotation (extent=[-30, 16; -16, 30], rotation=-
          90);
    BondLib.Bonds.eBond RB1 annotation (extent=[-14, 16; 0, 30], rotation=-90)
      ;
    Roof Roof1(
      area=72.5, 
      OH=0, 
      W=0) annotation (extent=[-17, 30; 3, 50], rotation=90);
    BondLib.Sources.mSF mSF1 annotation (extent=[-30, 30; -15, 45], rotation=
          90);
    AirConditioning AirConditioning1 annotation (extent=[-28, 50; -18, 60], 
        rotation=-90);
    BondLib.Bonds.eBond CCB1 annotation (extent=[-14, -29; 0, -15], rotation=-
          90);
    BondLib.Thermal.Cth Cth1(gamma=gamma, e(start=288)) annotation (extent=[-
          14, -45; 1, -30], rotation=-90);
    Slab Slab1(perim=23.7, area=72.5) annotation (extent=[-4, -80; 15, -60], 
        rotation=90);
    BondLib.Sources.mSE mSE2 annotation (extent=[-20, -86; -34, -73]);
    BondLib.Bonds.fBond SLB1 annotation (extent=[-19, -87; -6, -75]);
    BondLib.Bonds.eBond SLB2 annotation (extent=[-4, -60; 16, -40], rotation=
          90);
    BondLib.Junctions.J0p6 J0p6_3 annotation (extent=[-50, -10; -30, 10]);
    BondLib.Sensors.E E1 annotation (extent=[-28, 67; -18, 77], rotation=-90);
    BondLib.Bonds.fBond AB2 annotation (extent=[-31, 77; -16, 92], rotation=-
          90);
  equation 
    connect(NWin.BondCon1, NB1.fBondCon1) annotation (points=[-75, 68; -75, 
          68], style(color=8));
    connect(NWall.BondCon1, NB2.fBondCon1) annotation (points=[-60, 68; -60, 
          68], style(color=8));
    connect(WWall.BondCon1, WB1.fBondCon1) annotation (points=[-79, 30; -79, 
          30], style(color=8));
    connect(WB1.eBondCon1, J0p5_1.BondCon1) annotation (points=[-64, 30; -64, 
          30], style(color=8));
    connect(NB1.eBondCon1, J0p5_1.BondCon3) annotation (points=[-75, 47; -75, 
          40; -58, 40], style(color=8));
    connect(NB2.eBondCon1, J0p5_1.BondCon4) annotation (points=[-60, 48; -50, 
          48; -50, 40], style(color=8));
    connect(NB3.eBondCon1, J0p5_1.BondCon2) annotation (points=[-45, 48; -45, 
          40; -40, 40; -40, 30; -44, 30], style(color=8));
    connect(SWin1.BondCon1, SB1.fBondCon1) annotation (points=[-60, -42; -60, 
          -42], style(color=8));
    connect(SWall.BondCon1, SB2.fBondCon1) annotation (points=[-40, -42; -40, 
          -42], style(color=8));
    connect(SWin2.BondCon1, SB3.fBondCon1) annotation (points=[-20, -42; -20, 
          -42], style(color=8));
    connect(J0p5_1.BondCon5, CB1.fBondCon1) annotation (points=[-54, 19; -54, 
          20], style(color=8));
    connect(CB2.eBondCon1, J0p6_1.BondCon1) annotation (points=[-14, 0; -13, 0
          ], style(color=8));
    connect(CB3.eBondCon1, J0p6_1.BondCon2) annotation (points=[7, 0; 7, 0], 
        style(color=8));
    connect(J0p5_3.BondCon1, CB3.fBondCon1) annotation (points=[22, 1; 22, 1]
        , style(color=8));
    connect(CB4.eBondCon1, J0p5_3.BondCon2) annotation (points=[42, 1; 41, 1]
        , style(color=8));
    connect(J0p6_2.BondCon1, CB4.fBondCon1) annotation (points=[57, 0; 57, 0]
        , style(color=8));
    connect(J0p5_3.BondCon3, NB4.eBondCon1) annotation (points=[28, 10; 28, 15
          ; 9.5, 15; 9.5, 20], style(color=8));
    connect(Clock1.outPort, Temperature2_1.inPort) annotation (points=[-89.5, 
          100; -85, 100], style(color=3));
    connect(Temperature2_1.outPort, mSE1.InPort1) annotation (points=[-73, 100
          ; -67.3, 100], style(color=3));
    connect(mSE1.BondCon1, NB6.eBondCon1) annotation (points=[-54, 100; -54, 
          100], style(color=8));
    connect(NDoor.BondCon2, NB3.fBondCon1) annotation (points=[-45, 68; -45, 
          68], style(color=8));
    connect(NB6.fBondCon1, NDoor.BondCon1) annotation (points=[-44, 100; -40, 
          100; -40, 95; -45, 95; -45, 89], style(color=8));
    connect(J0p5_3.BondCon4, NB5.eBondCon1) annotation (points=[36, 10; 36, 18
          ; 24, 18; 24, 20], style(color=8));
    connect(SsWall.BondCon2, SB5.fBondCon1) annotation (points=[50, -35; 50, -
          35], style(color=8));
    connect(SB6.fBondCon1, SsDoor.BondCon1) annotation (points=[25, -40; 25, -
          40], style(color=8));
    connect(SB7.fBondCon1, SsWin.BondCon1) annotation (points=[70, -40; 70, -
          40], style(color=8));
    connect(J0p6_2.BondCon4, SB7.eBondCon1) annotation (points=[71, -11; 71, -
          25], style(color=8));
    connect(SB5.eBondCon1, J0p4_1.BondCon4) annotation (points=[50, -55; 50, -
          55], style(color=8));
    connect(SB8.eBondCon1, J0p4_1.BondCon1) annotation (points=[44, -60; 44, -
          60], style(color=8));
    connect(SsDoor.BondCon2, SB8.fBondCon1) annotation (points=[25, -60; 31, -
          60], style(color=8));
    connect(SB9.eBondCon1, J0p4_1.BondCon2) annotation (points=[55, -60; 55, -
          60], style(color=8));
    connect(SsWin.BondCon2, SB9.fBondCon1) annotation (points=[70, -60; 68, -
          60], style(color=8));
    connect(J0p4_1.BondCon3, BondCon1) annotation (points=[50, -65; 50, -73], 
        style(color=8));
    connect(NB5.fBondCon1, WallL2.BondCon1) annotation (points=[25, 34; 25, 34
          ], style(color=8));
    connect(NB4.fBondCon1, DoorL2.BondCon1) annotation (points=[9, 34; 9, 34]
        , style(color=8));
    connect(DoorL2.BondCon2, NB7.fBondCon1) annotation (points=[9, 55; 9, 55]
        , style(color=8));
    connect(WallL2.BondCon2, NB8.fBondCon1) annotation (points=[25, 55; 25, 55
          ], style(color=8));
    connect(NB7.eBondCon1, J0p3_1.BondCon1) annotation (points=[9, 68; 9, 68.5
          ], style(color=8));
    connect(NB8.eBondCon1, J0p3_1.BondCon2) annotation (points=[25, 68; 25, 
          68.5], style(color=8));
    connect(J0p3_1.BondCon3, BondCon2) annotation (points=[17, 75; 17, 82], 
        style(color=8));
    connect(J0p6_2.BondCon5, NB9.eBondCon1) annotation (points=[63, 10; 63, 16
          ; 49, 16; 49, 20], style(color=8));
    connect(J0p6_2.BondCon6, NB10.eBondCon1) annotation (points=[71, 10; 71, 
          20], style(color=8));
    connect(NB9.fBondCon1, DoorL1.BondCon1) annotation (points=[50, 34; 50, 34
          ], style(color=8));
    connect(NB10.fBondCon1, WallL1.BondCon1) annotation (points=[70, 34; 70, 
          34], style(color=8));
    connect(DoorL1.BondCon2, NB11.fBondCon1) annotation (points=[50, 54; 50, 
          55], style(color=8));
    connect(WallL1.BondCon2, NB12.fBondCon1) annotation (points=[70, 55; 70, 
          55], style(color=8));
    connect(NB11.eBondCon1, J0p3_2.BondCon1) annotation (points=[50, 68; 52, 
          68], style(color=8));
    connect(NB12.eBondCon1, J0p3_2.BondCon2) annotation (points=[71, 68; 68, 
          68], style(color=8));
    connect(J0p3_2.BondCon3, BondCon3) annotation (points=[60, 75; 60, 80], 
        style(color=8));
    connect(EB1.eBondCon1, J0p6_2.BondCon2) annotation (points=[77, 1; 77, 1]
        , style(color=8));
    connect(EWall.BondCon1, EB1.fBondCon1) annotation (points=[92, 0; 92, 1.5]
        , style(color=8));
    connect(RB1.eBondCon1, J0p6_1.BondCon6) annotation (points=[-7, 16; -7, 13
          ; 1, 13; 1, 10], style(color=8));
    connect(AB1.eBondCon1, J0p6_1.BondCon5) annotation (points=[-23, 16; -23, 
          10; -7, 10], style(color=8));
    connect(Roof1.BondCon1, RB1.fBondCon1) annotation (points=[-7, 30; -7, 30]
        , style(color=8));
    connect(mSF1.BondCon1, AB1.fBondCon1) annotation (points=[-23, 30; -23, 30
          ], style(color=8));
    connect(AirConditioning1.outPort, mSF1.InPort1) annotation (points=[-23, 
          49; -23, 45], style(color=3));
    connect(J0p6_1.BondCon3, CCB1.fBondCon1) annotation (points=[-7, -11; -7, 
          -15], style(color=8));
    connect(CCB1.eBondCon1, Cth1.BondCon1) annotation (points=[-7, -29; -7, -
          29.5; -6.5, -29.5; -6.5, -30], style(color=8));
    connect(mSE2.BondCon1, SLB1.eBondCon1) annotation (points=[-20, -80; -19.5
          , -80; -19.5, -81; -19, -81], style(color=8));
    connect(SLB1.fBondCon1, Slab1.BondCon1) annotation (points=[-6, -81; 6, -
          81; 6, -80], style(color=8));
    connect(Temperature2_1.outPort, mSE2.InPort1) annotation (points=[-74, 100
          ; -70, 100; -70, 110; -105, 110; -105, -80; -34, -80], style(color=3)
      );
    connect(Slab1.BondCon2, SLB2.fBondCon1) annotation (points=[6, -59; 6, -60
          ], style(color=8));
    connect(SLB2.eBondCon1, J0p6_1.BondCon4) annotation (points=[6, -40; 6, -
          25; 0, -25; 0, -10], style(color=8));
    connect(CB1.eBondCon1, J0p6_3.BondCon5) annotation (points=[-54, 5; -54, 2
          ; -49, 2; -49, 10; -44, 10], style(color=8));
    connect(J0p6_3.BondCon2, CB2.fBondCon1) annotation (points=[-30, 0; -29, 0
          ], style(color=8));
    connect(SB1.eBondCon1, J0p6_3.BondCon1) annotation (points=[-60, -22; -60
          , 0; -50, 0], style(color=8));
    connect(SB2.eBondCon1, J0p6_3.BondCon3) annotation (points=[-40, -22; -40
          , -16; -44, -16; -44, -10], style(color=8));
    connect(SB3.eBondCon1, J0p6_3.BondCon4) annotation (points=[-20, -21; -20
          , -10; -36, -10], style(color=8));
    connect(E1.OutPort1, AirConditioning1.inPort) annotation (points=[-23, 66
          ; -23, 62], style(color=3));
    connect(AB2.fBondCon1, E1.BondCon1) annotation (points=[-23, 77; -23, 77]
        , style(color=8));
    connect(J0p6_3.BondCon6, AB2.eBondCon1) annotation (points=[-36, 10; -36, 
          95; -24, 95; -24, 92], style(color=8));
    connect(SB4.eBondCon1, J0p6_2.BondCon3) annotation (points=[62, -10; 63, -
          10], style(color=8));
    connect(SB4.fBondCon1, SsWall.BondCon1) annotation (points=[52, -10; 49, -
          10; 49, -19], style(color=8));
    connect(J0p5_3.BondCon5, SB6.eBondCon1) annotation (points=[32, -10; 32, -
          16; 25, -16; 25, -25], style(color=8));
  end LivingRoom;
  model BedRoom1 
    parameter Real c=1000 "Specific heat capacity of living room";
    parameter Real m=54 "Mass of living room";
    parameter Real gamma=c*m/3600 "Heat capacity of living room";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[1, 1], 
        component=[10, 10]), 
      Icon(
        Line(points=[-90, 90; -90, -40; 90, -40; 90, 20; -30, 20; -30, 90; -90
              , 90], style(color=53, thickness=2)), 
        Line(points=[-29, 90; 90, 90; 90, 20], style(color=57, thickness=2)), 
        Line(points=[30, 90; 30, 20], style(color=53, thickness=2)), 
        Line(points=[10, -40; 10, -80; 90, -80; 90, -41], style(color=53, 
              thickness=2)), 
        Rectangle(extent=[60, -38; 80, -42], style(color=53)), 
        Rectangle(extent=[-30, -38; -10, -42], style(color=53)), 
        Rectangle(extent=[-70, -38; -50, -42], style(color=53)), 
        Rectangle(extent=[-84, 92; -64, 88], style(color=53)), 
        Polygon(points=[-55, 90; -40, 75; -40, 90; -55, 90], style(color=53))
          , 
        Polygon(points=[20, 20; 20, 40; 5, 20; 20, 20], style(color=53)), 
        Polygon(points=[40, 20; 40, 40; 56, 20; 40, 20], style(color=9)), 
        Polygon(points=[20, -40; 20, -20; 35, -40; 20, -40], style(color=53))
          , 
        Rectangle(extent=[-10, 92; 10, 88], style(color=53)), 
        Rectangle(extent=[50, 92; 70, 88], style(color=9)), 
        Rectangle(extent=[10, -78; 30, -82], style(color=53)), 
        Rectangle(extent=[30, -78; 50, -82], style(color=53)), 
        Rectangle(extent=[50, -78; 70, -82], style(color=53)), 
        Rectangle(extent=[70, -78; 90, -82], style(color=53)), 
        Polygon(points=[10, -50; -10, -50; 10, -65; 10, -50], style(color=53))
          , 
        Text(
          extent=[-19, 69; 17, 54], 
          string="Bedroom 2", 
          style(color=53)), 
        Text(
          extent=[34, 74; 86, 40], 
          string="Bedroom 1", 
          style(color=9)), 
        Text(
          extent=[35, -54; 66, -66], 
          string="Sunspace", 
          style(color=53)), 
        Text(
          extent=[-36, 0; 44, -10], 
          string="Living Room", 
          style(color=53)), 
        Rectangle(extent=[30, 90; 90, 20], style(color=9, thickness=2))), 
      Diagram(
        Line(points=[-90, 90; -90, -40; 90, -40; 90, 20; -30, 20; -30, 90; -90
              , 90], style(color=53, thickness=2)), 
        Line(points=[-29, 90; 90, 90; 90, 20], style(color=57, thickness=2)), 
        Line(points=[30, 90; 30, 20], style(color=53, thickness=2)), 
        Line(points=[10, -40; 10, -80; 90, -80; 90, -41], style(color=53, 
              thickness=2)), 
        Rectangle(extent=[60, -38; 80, -42], style(color=53)), 
        Rectangle(extent=[-30, -38; -10, -42], style(color=53)), 
        Rectangle(extent=[-70, -38; -50, -42], style(color=53)), 
        Rectangle(extent=[-84, 92; -64, 88], style(color=53)), 
        Polygon(points=[-55, 90; -40, 75; -40, 90; -55, 90], style(color=53))
          , 
        Polygon(points=[20, 20; 20, 40; 5, 20; 20, 20], style(color=53)), 
        Polygon(points=[40, 20; 40, 40; 56, 20; 40, 20], style(color=41)), 
        Polygon(points=[20, -40; 20, -20; 35, -40; 20, -40], style(color=53))
          , 
        Rectangle(extent=[-10, 92; 10, 88], style(color=53)), 
        Rectangle(extent=[50, 92; 70, 88], style(color=41)), 
        Rectangle(extent=[10, -78; 30, -82], style(color=53)), 
        Rectangle(extent=[30, -78; 50, -82], style(color=53)), 
        Rectangle(extent=[50, -78; 70, -82], style(color=53)), 
        Rectangle(extent=[70, -78; 90, -82], style(color=53)), 
        Polygon(points=[10, -50; -10, -50; 10, -65; 10, -50], style(color=53))
          , 
        Text(
          extent=[-19, 69; 17, 54], 
          string="Bedroom 2", 
          style(color=53)), 
        Text(
          extent=[34, 72; 86, 38], 
          string="Bedroom 1", 
          style(color=41)), 
        Text(
          extent=[35, -54; 66, -66], 
          string="Sunspace", 
          style(color=53)), 
        Text(
          extent=[-36, -7; 44, -17], 
          string="Living Room", 
          style(color=53)), 
        Rectangle(extent=[30, 90; 90, 20], style(color=41, thickness=2)), 
        Line(points=[56, 29; 56, 29], style(color=8))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    BondLib.Junctions.J0p5 J0p5_1 annotation (extent=[50, 30; 70, 50]);
    BondLib.Junctions.J0p5 J0p5_2 annotation (extent=[50, 84; 70, 64]);
    BondLib.Bonds.eBond CB1 annotation (extent=[53, 50; 67, 64], rotation=-90)
      ;
    BondLib.Bonds.eBond EB1 annotation (extent=[71, 33; 86, 48], rotation=180)
      ;
    ExWall EWall(
      area=9.4, 
      sfa=270, 
      OH=0.3, 
      W=8) annotation (extent=[86, 30; 106, 50]);
    BondLib.Bonds.eBond NB1 annotation (extent=[49, 84; 63, 99], rotation=-90)
      ;
    BondLib.Bonds.eBond NB2 annotation (extent=[70, 84; 84, 99], rotation=-90)
      ;
    Window NWin(
      area=1.5, 
      OH=0, 
      W=0, 
      sfa=180) annotation (extent=[47, 89; 28, 109]);
    ExWall NWall(
      area=9, 
      sfa=180, 
      OH=0.3, 
      W=12) annotation (extent=[87, 89; 107, 109]);
    BondLib.Bonds.eBond RB1 annotation (extent=[70, 67; 84, 81], rotation=-180
      );
    Roof Roof1(
      area=15, 
      OH=0, 
      W=0) annotation (extent=[84, 64; 104, 84]);
    BondLib.Bonds.fBond WB1 annotation (extent=[35, 67; 50, 81], rotation=180)
      ;
    IntWall Wall12(area=6.6) annotation (extent=[35, 64; 15, 84]);
    BondLib.Bonds.eBond WB2 annotation (extent=[2, 68; 15, 81], rotation=180);
    BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-13, 67; 2, 82]);
    BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-30, 70; -20, 80])
      ;
    BondLib.Bonds.eBond CCB1 annotation (extent=[73, 6; 87, 20], rotation=-90)
      ;
    BondLib.Thermal.Cth Cth1(gamma=gamma, e(start=288)) annotation (extent=[72
          , -9; 87, 6], rotation=-90);
    BondLib.Bonds.eBond CB2 annotation (extent=[48, 5; 63, 20], rotation=90);
    BondLib.Interfaces.BondCon BondCon2 annotation (extent=[51, -20; 61, -10])
      ;
    BondLib.Bonds.eBond SLB2 annotation (extent=[10, 15; 30, 35], rotation=90)
      ;
    Slab Slab1(perim=7.9, area=15) annotation (extent=[10, -5; 30, 15], 
        rotation=90);
    BondLib.Bonds.fBond SLB1 annotation (extent=[-2, -11; 11, 1]);
    BondLib.Sources.mSE mSE1 annotation (extent=[-3, -15; -22, 5]);
    Temperature2 Temperature2_1 annotation (extent=[-50, -16; -30, 4]);
    Modelica.Blocks.Sources.Clock Clock1 annotation (extent=[-80, -16; -60, 4]
      );
  equation 
    connect(CB1.eBondCon1, J0p5_1.BondCon5) annotation (points=[60, 50; 60, 
          50], style(color=8));
    connect(J0p5_2.BondCon5, CB1.fBondCon1) annotation (points=[60, 64; 60, 64
          ], style(color=8));
    connect(EB1.eBondCon1, J0p5_1.BondCon2) annotation (points=[71, 40; 71, 40
          ], style(color=8));
    connect(EWall.BondCon1, EB1.fBondCon1) annotation (points=[87, 40; 86, 40]
        , style(color=8));
    connect(NB1.eBondCon1, J0p5_2.BondCon3) annotation (points=[56, 84; 56, 84
          ], style(color=8));
    connect(NB2.eBondCon1, J0p5_2.BondCon4) annotation (points=[77, 84; 64, 84
          ], style(color=8));
    connect(NWall.BondCon1, NB2.fBondCon1) annotation (points=[87, 99; 77, 99]
        , style(color=8));
    connect(RB1.eBondCon1, J0p5_2.BondCon2) annotation (points=[70, 74; 71, 74
          ], style(color=8));
    connect(Roof1.BondCon1, RB1.fBondCon1) annotation (points=[84, 74; 84, 74]
        , style(color=8));
    connect(NWin.BondCon1, NB1.fBondCon1) annotation (points=[47, 99; 56, 99]
        , style(color=8));
    connect(J0p5_2.BondCon1, WB1.eBondCon1) annotation (points=[50, 74; 50, 74
          ], style(color=8));
    connect(WB1.fBondCon1, Wall12.BondCon1) annotation (points=[35, 74; 35, 74
          ], style(color=8));
    connect(Wall12.BondCon2, WB2.fBondCon1) annotation (points=[14, 74; 15, 74
          ], style(color=8));
    connect(WB2.eBondCon1, J0p2_1.BondCon2) annotation (points=[2, 74; 2, 74.5
          ], style(color=8));
    connect(J0p2_1.BondCon1, BondCon1) annotation (points=[-13, 75; -25, 75], 
        style(color=8));
    connect(J0p5_1.BondCon4, CCB1.fBondCon1) annotation (points=[64, 30; 80, 
          30; 80, 20], style(color=8));
    connect(CCB1.eBondCon1, Cth1.BondCon1) annotation (points=[80, 6; 80, 6], 
        style(color=8));
    connect(CB2.eBondCon1, J0p5_1.BondCon3) annotation (points=[56, 20; 56, 30
          ], style(color=8));
    connect(CB2.fBondCon1, BondCon2) annotation (points=[56, 5; 56, -12], 
        style(color=8));
    connect(SLB2.eBondCon1, J0p5_1.BondCon1) annotation (points=[20, 35; 20, 
          40; 50, 40], style(color=8));
    connect(Slab1.BondCon2, SLB2.fBondCon1) annotation (points=[20, 15; 20, 15
          ], style(color=8));
    connect(SLB1.fBondCon1, Slab1.BondCon1) annotation (points=[11, -5; 20, -5
          ], style(color=8));
    connect(mSE1.BondCon1, SLB1.eBondCon1) annotation (points=[-3, -5; -2, -5]
        , style(color=8));
    connect(Temperature2_1.outPort, mSE1.InPort1) annotation (points=[-28, -6
          ; -22, -6], style(color=3));
    connect(Clock1.outPort, Temperature2_1.inPort) annotation (points=[-59, -6
          ; -54, -6], style(color=3));
  end BedRoom1;
  model BedRoom2 
    parameter Real c=1000 "Specific heat capacity of living room";
    parameter Real m=54 "Mass of living room";
    parameter Real gamma=c*m/3600 "Heat capacity of living room";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[1, 1], 
        component=[10, 10]), 
      Icon(
        Line(points=[-90, 90; -90, -40; 90, -40; 90, 20; -30, 20; -30, 90; -90
              , 90], style(color=53, thickness=2)), 
        Line(points=[-29, 90; 90, 90; 90, 20], style(color=57, thickness=2)), 
        Line(points=[30, 90; 30, 20], style(color=53, thickness=2)), 
        Line(points=[10, -40; 10, -80; 90, -80; 90, -41], style(color=53, 
              thickness=2)), 
        Rectangle(extent=[60, -38; 80, -42], style(color=53)), 
        Rectangle(extent=[-30, -38; -10, -42], style(color=53)), 
        Rectangle(extent=[-70, -38; -50, -42], style(color=53)), 
        Rectangle(extent=[-84, 92; -64, 88], style(color=53)), 
        Polygon(points=[-55, 90; -40, 75; -40, 90; -55, 90], style(color=53))
          , 
        Polygon(points=[20, 20; 20, 40; 5, 20; 20, 20], style(color=9)), 
        Polygon(points=[40, 20; 40, 40; 56, 20; 40, 20], style(color=53)), 
        Polygon(points=[20, -40; 20, -20; 35, -40; 20, -40], style(color=53))
          , 
        Rectangle(extent=[-10, 92; 10, 88], style(color=9)), 
        Rectangle(extent=[50, 92; 70, 88], style(color=53)), 
        Rectangle(extent=[10, -78; 30, -82], style(color=53)), 
        Rectangle(extent=[30, -78; 50, -82], style(color=53)), 
        Rectangle(extent=[50, -78; 70, -82], style(color=53)), 
        Rectangle(extent=[70, -78; 90, -82], style(color=53)), 
        Polygon(points=[10, -50; -10, -50; 10, -65; 10, -50], style(color=53))
          , 
        Text(
          extent=[41, 69; 77, 54], 
          string="Bedroom 1", 
          style(color=53)), 
        Text(
          extent=[-26, 76; 26, 42], 
          string="Bedroom 2", 
          style(color=9)), 
        Text(
          extent=[35, -54; 66, -66], 
          string="Sunspace", 
          style(color=53)), 
        Text(
          extent=[-36, 0; 44, -10], 
          string="Living Room", 
          style(color=53)), 
        Rectangle(extent=[-30, 90; 30, 20], style(color=9, thickness=2))), 
      Diagram(
        Line(points=[-90, 90; -90, -40; 90, -40; 90, 20; -30, 20; -30, 90; -90
              , 90], style(color=53, thickness=2)), 
        Line(points=[-29, 90; 90, 90; 90, 20], style(color=57, thickness=2)), 
        Line(points=[30, 90; 30, 20], style(color=53, thickness=2)), 
        Line(points=[10, -40; 10, -80; 90, -80; 90, -41], style(color=53, 
              thickness=2)), 
        Rectangle(extent=[60, -38; 80, -42], style(color=53)), 
        Rectangle(extent=[-30, -38; -10, -42], style(color=53)), 
        Rectangle(extent=[-70, -38; -50, -42], style(color=53)), 
        Rectangle(extent=[-84, 92; -64, 88], style(color=53)), 
        Polygon(points=[-55, 90; -40, 75; -40, 90; -55, 90], style(color=53))
          , 
        Polygon(points=[20, 20; 20, 40; 5, 20; 20, 20], style(color=41)), 
        Polygon(points=[40, 20; 40, 40; 56, 20; 40, 20], style(color=53)), 
        Polygon(points=[20, -40; 20, -20; 35, -40; 20, -40], style(color=53))
          , 
        Rectangle(extent=[-10, 92; 10, 88], style(color=41)), 
        Rectangle(extent=[50, 92; 70, 88], style(color=53)), 
        Rectangle(extent=[10, -78; 30, -82], style(color=53)), 
        Rectangle(extent=[30, -78; 50, -82], style(color=53)), 
        Rectangle(extent=[50, -78; 70, -82], style(color=53)), 
        Rectangle(extent=[70, -78; 90, -82], style(color=53)), 
        Polygon(points=[10, -50; -10, -50; 10, -65; 10, -50], style(color=53))
          , 
        Text(
          extent=[41, 69; 77, 54], 
          string="Bedroom 1", 
          style(color=53)), 
        Text(
          extent=[-26, 76; 26, 42], 
          string="Bedroom 2", 
          style(color=41)), 
        Text(
          extent=[35, -54; 66, -66], 
          string="Sunspace", 
          style(color=53)), 
        Text(
          extent=[-27, -1; 53, -11], 
          string="Living Room", 
          style(color=53)), 
        Rectangle(extent=[-30, 90; 30, 20], style(color=41, thickness=2)), 
        Polygon(points=[20, 20; 20, 40; 5, 20; 20, 20], style(color=53))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    BondLib.Junctions.J0p5 J0p5_2 annotation (extent=[-10, 84; 10, 64]);
    BondLib.Bonds.eBond CB1 annotation (extent=[-7, 50; 7, 64], rotation=-90);
    BondLib.Junctions.J0p4 J0p4_1 annotation (extent=[-10, 30; 10, 50]);
    BondLib.Bonds.eBond NB1 annotation (extent=[-11, 84; 3, 99], rotation=-90)
      ;
    BondLib.Bonds.eBond NB2 annotation (extent=[13, 84; 27, 99], rotation=-90)
      ;
    ExWall NWall(
      area=9, 
      sfa=180, 
      OH=0.3, 
      W=12) annotation (extent=[30, 89; 50, 109]);
    Window NWin(
      area=1.5, 
      OH=0, 
      W=0, 
      sfa=180) annotation (extent=[-14, 89; -33, 109]);
    BondLib.Bonds.eBond RB1 annotation (extent=[-24, 67; -10, 81]);
    Roof Roof1(
      area=15, 
      OH=0, 
      W=0) annotation (extent=[-24, 64; -44, 84]);
    BondLib.Bonds.eBond CCB1 annotation (extent=[13, 16; 27, 30], rotation=-90
      );
    BondLib.Thermal.Cth Cth1(gamma=gamma, e(start=288)) annotation (extent=[13
          , 1; 28, 16], rotation=-90);
    BondLib.Bonds.eBond CB2 annotation (extent=[-8, 3; 7, 18], rotation=90);
    BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-5, -20; 5, -10]);
    BondLib.Bonds.eBond SLB2 annotation (extent=[-30, 10; -10, 30], rotation=
          90);
    Slab Slab1(perim=4.2, area=15) annotation (extent=[-30, -10; -10, 10], 
        rotation=90);
    BondLib.Bonds.fBond SLB1 annotation (extent=[-34, -16; -21, -4]);
    BondLib.Sources.mSE mSE1 annotation (extent=[-34, -20; -53, 0]);
    Temperature2 Temperature2_1 annotation (extent=[-80, 0; -60, 20], rotation
        =-90);
    Modelica.Blocks.Sources.Clock Clock1 annotation (extent=[-80, 30; -60, 50]
        , rotation=-90);
    BondLib.Bonds.eBond CB3 annotation (extent=[22, 66; 37, 81], rotation=180)
      ;
    BondLib.Interfaces.BondCon BondCon2 annotation (extent=[50, 68; 60, 78]);
  equation 
    connect(J0p5_2.BondCon5, CB1.fBondCon1) annotation (points=[0, 63; 0, 64
          ], style(color=8));
    connect(CB1.eBondCon1, J0p4_1.BondCon4) annotation (points=[0, 50; 0, 50]
        , style(color=8));
    connect(NB1.eBondCon1, J0p5_2.BondCon3) annotation (points=[-4, 84; -4, 84
          ], style(color=8));
    connect(NB2.eBondCon1, J0p5_2.BondCon4) annotation (points=[20, 84; 4, 84]
        , style(color=8));
    connect(NWall.BondCon1, NB2.fBondCon1) annotation (points=[30, 99; 20, 99]
        , style(color=8));
    connect(NWin.BondCon1, NB1.fBondCon1) annotation (points=[-14, 99; -4, 99]
        , style(color=8));
    connect(RB1.eBondCon1, J0p5_2.BondCon1) annotation (points=[-10, 74; -10, 
          74], style(color=8));
    connect(Roof1.BondCon1, RB1.fBondCon1) annotation (points=[-24, 74; -24, 
          74], style(color=8));
    connect(J0p4_1.BondCon2, CCB1.fBondCon1) annotation (points=[10, 40; 20, 
          40; 20, 30], style(color=8));
    connect(CCB1.eBondCon1, Cth1.BondCon1) annotation (points=[20, 16; 20, 16]
        , style(color=8));
    connect(CB2.eBondCon1, J0p4_1.BondCon3) annotation (points=[0, 18; 0, 30]
        , style(color=8));
    connect(CB2.fBondCon1, BondCon1) annotation (points=[0, 3; 0, -11], style(
          color=8));
    connect(SLB2.eBondCon1, J0p4_1.BondCon1) annotation (points=[-20, 30; -20
          , 40; -10, 40], style(color=8));
    connect(Slab1.BondCon2, SLB2.fBondCon1) annotation (points=[-20, 10; -20, 
          10], style(color=8));
    connect(SLB1.fBondCon1, Slab1.BondCon1) annotation (points=[-21, -10; -20
          , -10], style(color=8));
    connect(mSE1.BondCon1, SLB1.eBondCon1) annotation (points=[-34, -10; -34, 
          -10], style(color=8));
    connect(Temperature2_1.outPort, mSE1.InPort1) annotation (points=[-70, -2
          ; -70, -10; -53, -10], style(color=3));
    connect(Clock1.outPort, Temperature2_1.inPort) annotation (points=[-70, 28
          ; -70, 24], style(color=3));
    connect(CB3.eBondCon1, J0p5_2.BondCon2) annotation (points=[22, 74; 10, 74
          ], style(color=8));
    connect(CB3.fBondCon1, BondCon2) annotation (points=[37, 74; 52, 74], 
        style(color=8));
  end BedRoom2;
  model SunSpace 
    parameter Real c=1000 "Specific heat capacity of living room";
    parameter Real m=48.6 "Mass of living room";
    parameter Real gamma=c*m/3600 "Heat capacity of living room";
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[1, 1], 
        component=[10, 10]), 
      Icon(
        Line(points=[-90, 90; -90, -40; 90, -40; 90, 20; -30, 20; -30, 90; -90
              , 90], style(color=53, thickness=2)), 
        Line(points=[-29, 90; 90, 90; 90, 20], style(color=57, thickness=2)), 
        Line(points=[30, 90; 30, 20], style(color=53, thickness=2)), 
        Line(points=[10, -40; 10, -80; 90, -80; 90, -41], style(color=53, 
              thickness=2)), 
        Rectangle(extent=[60, -38; 80, -42], style(color=9)), 
        Rectangle(extent=[-30, -38; -10, -42], style(color=53)), 
        Rectangle(extent=[-70, -38; -50, -42], style(color=53)), 
        Rectangle(extent=[-84, 92; -64, 88], style(color=53)), 
        Polygon(points=[-55, 90; -40, 75; -40, 90; -55, 90], style(color=53))
          , 
        Polygon(points=[20, 20; 20, 40; 5, 20; 20, 20], style(color=53)), 
        Polygon(points=[40, 20; 40, 40; 56, 20; 40, 20], style(color=53)), 
        Polygon(points=[20, -40; 20, -20; 35, -40; 20, -40], style(color=9)), 
        Rectangle(extent=[-10, 92; 10, 88], style(color=53)), 
        Rectangle(extent=[50, 92; 70, 88], style(color=53)), 
        Rectangle(extent=[10, -78; 30, -82], style(color=9)), 
        Rectangle(extent=[30, -78; 50, -82], style(color=9)), 
        Rectangle(extent=[50, -78; 70, -82], style(color=9)), 
        Rectangle(extent=[70, -78; 90, -82], style(color=9)), 
        Polygon(points=[10, -50; -10, -50; 10, -65; 10, -50], style(color=9))
          , 
        Text(
          extent=[-19, 69; 17, 54], 
          string="Bedroom 2", 
          style(color=53)), 
        Text(
          extent=[24, -50; 76, -70], 
          string="Sunspace", 
          style(color=9)), 
        Text(
          extent=[-36, 0; 44, -10], 
          string="Living Room", 
          style(color=53)), 
        Text(
          extent=[41, 69; 77, 54], 
          string="Bedroom 1", 
          style(color=53)), 
        Rectangle(extent=[10, -40; 90, -80], style(color=9, thickness=2))), 
      Diagram(
        Rectangle(extent=[30, 2; 70, -2], style(color=41)), 
        Polygon(points=[-60, 0; -60, 40; -30, 0; -60, 0], style(color=41)), 
        Rectangle(extent=[-70, -78; -30, -82], style(color=41)), 
        Polygon(points=[-70, -20; -110, -20; -70, -48; -70, -20], style(color=
                41)), 
        Text(
          extent=[-6, -22; 26, -40], 
          string="Sunspace", 
          style(color=41)), 
        Rectangle(extent=[-70, 0; 90, -80], style(color=41, thickness=2)), 
        Rectangle(extent=[-30, -78; 10, -82], style(color=41)), 
        Rectangle(extent=[10, -78; 50, -82], style(color=41)), 
        Rectangle(extent=[50, -78; 90, -82], style(color=41)), 
        Line(points=[90, 0; 90, 100], style(color=53, thickness=2)), 
        Line(points=[-70, 0; -100, 0], style(color=53, thickness=2)), 
        Text(
          extent=[-86, 74; 14, 60], 
          string="Living Room", 
          style(color=53))));
    BondLib.Junctions.J0p6 J0p6_1 annotation (extent=[20, -55; 50, -25]);
    BondLib.Junctions.J0p6 J0p6_2 annotation (extent=[-30, -55; 0, -25]);
    BondLib.Bonds.eBond CB1 annotation (extent=[0, -50; 20, -30]);
    BondLib.Bonds.eBond EB1 annotation (extent=[50, -50; 70, -30], rotation=
          180);
    Window EWin(
      lv=4.9, 
      area=6.2, 
      OH=0, 
      W=0, 
      sfa=270) annotation (extent=[70, -50; 89, -30]);
    BondLib.Bonds.eBond EB2 annotation (extent=[41, -30; 61, -10], rotation=
          180);
    Door EWall(
      lv=5.11, 
      area=3, 
      sfa=270, 
      OH=0, 
      W=0) annotation (extent=[81, -30; 61, -10]);
    BondLib.Bonds.fBond EB3 annotation (extent=[81, -30; 101, -10], rotation=
          180);
    BondLib.Sources.mSE mSE1 annotation (extent=[111, -10; 91, 10], rotation=
          90);
    BondLib.Bonds.eBond CB2 annotation (extent=[19, -17; 39, 3], rotation=270)
      ;
    BondLib.Interfaces.BondCon BondCon1 annotation (extent=[24, 8; 34, 18]);
    BondLib.Bonds.eBond WB1 annotation (extent=[-50, -50; -30, -30]);
    Window WWin(
      lv=4.9, 
      area=6.2, 
      OH=0, 
      W=0, 
      sfa=90) annotation (extent=[-50, -50; -69, -30]);
    BondLib.Bonds.eBond WB2 annotation (extent=[-41, -30; -21, -10]);
    Door WWall(
      lv=5.11, 
      area=3, 
      sfa=90, 
      OH=0, 
      W=0) annotation (extent=[-61, -30; -41, -10]);
    BondLib.Bonds.fBond WB3 annotation (extent=[-81, -30; -61, -10]);
    BondLib.Sources.mSE mSE2 annotation (extent=[-91, -10; -71, 10], rotation=
          90);
    BondLib.Bonds.eBond RB1 annotation (extent=[-19, -15; 1, 5], rotation=270)
      ;
    Door SsRoof(
      lv=0.23, 
      area=17.2, 
      sfa=0, 
      tilt=15, 
      OH=0, 
      W=0, 
      absor=0.3) annotation (extent=[-19, 5; 1, 25], rotation=-90);
    BondLib.Bonds.fBond RB2 annotation (extent=[-19, 25; 1, 45], rotation=-90)
      ;
    BondLib.Sources.mSE mSE3 annotation (extent=[-20, 35; -40, 55]);
    BondLib.Bonds.eBond CCB1 annotation (extent=[19, -75; 39, -55], rotation=-
          90);
    BondLib.Thermal.Cth Cth1(gamma=gamma, e(start=288)) annotation (extent=[19
          , -95; 39, -75], rotation=-90);
    BondLib.Bonds.eBond SB1 annotation (extent=[-19, -75; 1, -55], rotation=90
      );
    Window SWin(
      lv=4.9, 
      area=10.3, 
      OH=0, 
      W=0, 
      sfa=0) annotation (extent=[-19, -95; 1, -75], rotation=-90);
    BondLib.Bonds.eBond SB2 annotation (extent=[-40, -75; -20, -55], rotation=
          90);
    Door SWall(
      lv=5.11, 
      area=1, 
      sfa=0, 
      OH=0, 
      W=0) annotation (extent=[-40, -95; -20, -75], rotation=90);
    BondLib.Bonds.fBond SB3 annotation (extent=[-60, -105; -40, -85]);
    BondLib.Sources.mSE mSE4 annotation (extent=[-60, -105; -80, -85]);
    BondLib.Bonds.eBond SLB2 annotation (extent=[40, -75; 60, -55], rotation=
          90);
    Slab Slab1(
      thk=0.1, 
      perim=11.7, 
      area=17.2, 
      c=880, 
      dens=2300) annotation (extent=[40, -95; 60, -75], rotation=90);
    BondLib.Bonds.fBond SLB1 annotation (extent=[60, -105; 80, -85], rotation=
          180);
    BondLib.Sources.mSE mSE5 annotation (extent=[80, -105; 100, -85]);
    Temperature2 Temperature2_1 annotation (extent=[-70, 80; -50, 100]);
    Modelica.Blocks.Sources.Clock Clock1 annotation (extent=[-100, 80; -80, 
          100]);
  equation 
    connect(J0p6_2.BondCon2, CB1.fBondCon1) annotation (points=[0, -40; 0, -
          40], style(color=8));
    connect(CB1.eBondCon1, J0p6_1.BondCon1) annotation (points=[20, -40; 20, -
          40], style(color=8));
    connect(EB1.eBondCon1, J0p6_1.BondCon2) annotation (points=[50, -40; 50, -
          40], style(color=8));
    connect(EWin.BondCon1, EB1.fBondCon1) annotation (points=[70, -40; 70, -40
          ], style(color=8));
    connect(EB2.eBondCon1, J0p6_1.BondCon6) annotation (points=[41, -20; 41, -
          25], style(color=8));
    connect(EWall.BondCon2, EB2.fBondCon1) annotation (points=[61, -20; 61, -
          20], style(color=8));
    connect(EB3.fBondCon1, EWall.BondCon1) annotation (points=[81, -20; 81, -
          20], style(color=8));
    connect(mSE1.BondCon1, EB3.eBondCon1) annotation (points=[101, -10; 101, -
          20], style(color=8));
    connect(CB2.eBondCon1, J0p6_1.BondCon5) annotation (points=[29, -17; 29, -
          25], style(color=8));
    connect(WB1.eBondCon1, J0p6_2.BondCon1) annotation (points=[-30, -40; -30
          , -40], style(color=8));
    connect(WWin.BondCon1, WB1.fBondCon1) annotation (points=[-50, -40; -50, -
          40], style(color=8));
    connect(J0p6_2.BondCon5, WB2.eBondCon1) annotation (points=[-21, -25; -21
          , -21], style(color=8));
    connect(WWall.BondCon2, WB2.fBondCon1) annotation (points=[-41, -20; -41, 
          -20], style(color=8));
    connect(WB3.fBondCon1, WWall.BondCon1) annotation (points=[-61, -20; -61, 
          -20], style(color=8));
    connect(RB1.eBondCon1, J0p6_2.BondCon6) annotation (points=[-9, -15; -9, -
          24], style(color=8));
    connect(SsRoof.BondCon2, RB1.fBondCon1) annotation (points=[-9, 5; -9, 5]
        , style(color=8));
    connect(RB2.fBondCon1, SsRoof.BondCon1) annotation (points=[-9, 25; -9, 25
          ], style(color=8));
    connect(mSE3.BondCon1, RB2.eBondCon1) annotation (points=[-20, 45; -10, 45
          ], style(color=8));
    connect(mSE2.BondCon1, WB3.eBondCon1) annotation (points=[-81, -10; -81, -
          20], style(color=8));
    connect(SB1.eBondCon1, J0p6_2.BondCon4) annotation (points=[-9, -55; -9, -
          55], style(color=8));
    connect(J0p6_1.BondCon3, CCB1.fBondCon1) annotation (points=[29, -55; 29, 
          -55], style(color=8));
    connect(CCB1.eBondCon1, Cth1.BondCon1) annotation (points=[29, -75; 29, -
          75], style(color=8));
    connect(SWin.BondCon1, SB1.fBondCon1) annotation (points=[-9, -75; -9, -75
          ], style(color=8));
    connect(SB2.eBondCon1, J0p6_2.BondCon3) annotation (points=[-30, -55; -20
          , -55], style(color=8));
    connect(SWall.BondCon2, SB2.fBondCon1) annotation (points=[-30, -75; -30, 
          -76], style(color=8));
    connect(SB3.fBondCon1, SWall.BondCon1) annotation (points=[-40, -95; -30, 
          -95], style(color=8));
    connect(mSE4.BondCon1, SB3.eBondCon1) annotation (points=[-60, -95; -60, -
          95], style(color=8));
    connect(SLB2.eBondCon1, J0p6_1.BondCon4) annotation (points=[50, -55; 40, 
          -55], style(color=8));
    connect(Slab1.BondCon2, SLB2.fBondCon1) annotation (points=[50, -75; 50, -
          75], style(color=8));
    connect(SLB1.fBondCon1, Slab1.BondCon1) annotation (points=[60, -95; 50, -
          95], style(color=8));
    connect(mSE5.BondCon1, SLB1.eBondCon1) annotation (points=[80, -95; 80, -
          95], style(color=8));
    connect(Clock1.outPort, Temperature2_1.inPort) annotation (points=[-78, 90
          ; -74, 90], style(color=3));
    connect(Temperature2_1.outPort, mSE1.InPort1) annotation (points=[-48, 90
          ; 101, 90; 101, 9], style(color=3));
    connect(Temperature2_1.outPort, mSE5.InPort1) annotation (points=[-49, 90
          ; 113, 90; 113, -95; 100, -95], style(color=3));
    connect(Temperature2_1.outPort, mSE3.InPort1) annotation (points=[-49, 90
          ; -30, 90; -30, 107; -105, 107; -105, 45; -39, 45], style(color=3));
    connect(Temperature2_1.outPort, mSE2.InPort1) annotation (points=[-49, 90
          ; -30, 90; -30, 107; -105, 107; -105, 26; -81, 26; -81, 10], style(
          color=3));
    connect(Temperature2_1.outPort, mSE4.InPort1) annotation (points=[-50, 90
          ; -30, 90; -30, 107; -105, 107; -105, -95; -80, -95], style(color=3))
      ;
    connect(CB2.fBondCon1, BondCon1) annotation (points=[30, 3; 30, 10], style
        (color=8));
  end SunSpace;
  model House 
    annotation (
      Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Rectangle(extent=[-82, 88; 86, 82], style(color=9)), 
        Rectangle(extent=[-74, 82; 78, 74], style(color=9)), 
        Rectangle(extent=[-74, 74; 78, 38], style(color=9)), 
        Rectangle(extent=[-50, 68; -30, 50], style(color=9)), 
        Line(points=[-40, 68; -40, 50], style(color=9)), 
        Rectangle(extent=[-52, 68; -28, 66], style(color=0, fillColor=9)), 
        Rectangle(extent=[-8, 68; 12, 50], style(color=9)), 
        Line(points=[2, 68; 2, 50], style(color=9)), 
        Rectangle(extent=[-10, 68; 14, 66], style(color=0, fillColor=9)), 
        Rectangle(extent=[52, 68; 72, 50], style(color=9)), 
        Line(points=[62, 68; 62, 50], style(color=9)), 
        Rectangle(extent=[46, 68; 30, 38], style(color=9)), 
        Rectangle(extent=[28, 68; 74, 66], style(color=9, fillColor=9)), 
        Rectangle(extent=[28, 40; 48, 38], style(color=9, fillColor=9)), 
        Ellipse(extent=[42, 54; 44, 52], style(color=9)), 
        Line(points=[-86, 38; 90, 38], style(color=9)), 
        Text(
          extent=[-90, 38; -2, 30], 
          string="North Elevation", 
          style(color=9)), 
        Rectangle(extent=[-38, -20; 78, 10], style(color=9)), 
        Line(points=[78, 10; 78, 18; -38, 12], style(color=9)), 
        Line(points=[-38, -20; -38, 12], style(color=9)), 
        Line(points=[78, 18; -46, 12; -46, 18; 78, 26; 78, 18], style(color=9)
          ), 
        Line(points=[-38, 10; -76, 2; -76, -2; -38, 6], style(color=9)), 
        Rectangle(extent=[-74, -2; -38, -20], style(color=9)), 
        Rectangle(extent=[-72, -20; -40, -2], style(color=9)), 
        Rectangle(extent=[-60, -20; -50, -2], style(color=9)), 
        Rectangle(extent=[-62, -20; -52, -2], style(color=9)), 
        Line(points=[-40, -2; -40, 6], style(color=9)), 
        Line(points=[-42, -2; -42, 6], style(color=9)), 
        Line(points=[-44, -2; -44, 4], style(color=9)), 
        Line(points=[-46, -2; -46, 4], style(color=9)), 
        Line(points=[-48, -2; -48, 4], style(color=9)), 
        Line(points=[-50, -2; -50, 4], style(color=9)), 
        Line(points=[-52, -2; -52, 4], style(color=9)), 
        Line(points=[-54, -2; -54, 2], style(color=9)), 
        Line(points=[-56, -2; -56, 2], style(color=9)), 
        Line(points=[-58, -2; -58, 2], style(color=9)), 
        Line(points=[-60, -4; -60, 2], style(color=9)), 
        Line(points=[-62, -4; -62, 0], style(color=9)), 
        Line(points=[-64, -2; -64, 0], style(color=9)), 
        Line(points=[-66, -2; -66, 0], style(color=9)), 
        Line(points=[-68, -2; -68, 0], style(color=9)), 
        Text(
          extent=[4, -20; 92, -28], 
          string="East Elevation", 
          style(color=9)), 
        Line(points=[-86, -20; 88, -20], style(color=9)), 
        Rectangle(extent=[-80, -40; 84, -46], style(color=9)), 
        Rectangle(extent=[-80, -46; 84, -54], style(color=9)), 
        Rectangle(extent=[-72, -54; 76, -90], style(color=9)), 
        Rectangle(extent=[-72, -90; 76, -56], style(color=9)), 
        Rectangle(extent=[-62, -64; -34, -82], style(color=9)), 
        Line(points=[-48, -64; -48, -82], style(color=9)), 
        Rectangle(extent=[-64, -62; -32, -64], style(color=9, fillColor=9)), 
        Rectangle(extent=[-24, -64; 4, -82], style(color=9)), 
        Line(points=[-10, -64; -10, -82], style(color=9)), 
        Rectangle(extent=[-26, -62; 6, -64], style(color=9, fillColor=9)), 
        Rectangle(extent=[10, -56; 82, -64], style(color=9, fillColor=7)), 
        Rectangle(extent=[10, -64; 82, -66], style(color=9, fillColor=7)), 
        Rectangle(extent=[10, -66; 82, -68], style(color=9, fillColor=7)), 
        Rectangle(extent=[14, -68; 76, -90], style(color=9)), 
        Rectangle(extent=[16, -68; 74, -90], style(color=9)), 
        Rectangle(extent=[20, -68; 34, -90], style(color=9)), 
        Ellipse(extent=[30, -76; 32, -80], style(color=9)), 
        Rectangle(extent=[36, -68; 50, -90], style(color=9)), 
        Rectangle(extent=[52, -68; 72, -90], style(color=9)), 
        Rectangle(extent=[52, -78; 64, -68], style(color=9)), 
        Line(points=[56, -78; 56, -68], style(color=9)), 
        Line(points=[-86, -90; 88, -90], style(color=9)), 
        Text(
          extent=[-88, -90; 0, -98], 
          string="South Elevation", 
          style(color=9)), 
        Rectangle(extent=[12, -90; 78, -92], style(color=9, fillColor=9))), 
      Window(
        x=0.55, 
        y=0.19, 
        width=0.44, 
        height=0.65));
    BedRoom1 BedRoom1_1 annotation (extent=[-100, -100; 100, 100]);
    BedRoom2 BedRoom2_1 annotation (extent=[-100, -100; 100, 100]);
    SunSpace SunSpace1 annotation (extent=[-100, -100; 100, 100]);
    LivingRoom LivingRoom1 annotation (extent=[-100, -100; 100, 100]);
  equation 
    connect(BedRoom2_1.BondCon2, BedRoom1_1.BondCon1) annotation (points=[52
          , 74; -22, 74], style(color=41, thickness=4));
    connect(LivingRoom1.BondCon3, BedRoom1_1.BondCon2) annotation (points=[60
          , 80; 60, -16], style(color=77, thickness=4));
    connect(BedRoom2_1.BondCon1, LivingRoom1.BondCon2) annotation (points=[0, 
          -12; 0, -17; 14, 84; 14, 84], style(color=81, thickness=4));
    connect(LivingRoom1.BondCon1, SunSpace1.BondCon1) annotation (points=[48, 
          -72; 48, 10; 30, 10], style(color=49, thickness=4));
  end House;
end SolarHouse;
