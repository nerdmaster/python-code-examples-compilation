package Cardiovascular 
  annotation (Coordsys(
      extent=[0, 0; 402, 261], 
      grid=[2, 2], 
      component=[20, 20]));
  package Generic 
    annotation (
      Coordsys(
        extent=[0, 0; 443, 453], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Rectangle(extent=[-100, -100; 80, 50], style(fillColor=30, fillPattern
              =1)), 
        Polygon(points=[-100, 50; -80, 70; 100, 70; 80, 50; -100, 50], style(
              fillColor=30, fillPattern=1)), 
        Polygon(points=[100, 70; 100, -80; 80, -100; 80, 50; 100, 70], style(
              fillColor=30, fillPattern=1)), 
        Text(
          extent=[-85, 35; 65, -85], 
          string="Library", 
          style(color=3)), 
        Text(
          extent=[-120, 122; 120, 73], 
          string="%name", 
          style(color=1))), 
      Window(
        x=0.01, 
        y=0, 
        width=0.44, 
        height=0.65, 
        library=1, 
        autolayout=1));
    model modC 
      extends BondLib.Interfaces.ModPassiveOnePort;
      parameter Real q0=0 "Initial charge";
      Real C "Bondgraphic capacitance";
      Real q(start=q0) "Bondgraphic charge";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(Text(
            extent=[-60, 80; 60, -80], 
            string="mC", 
            style(color=0))), 
        Icon(Text(extent=[-70, 90; 70, 50], string="%name"), Text(
            extent=[-60, 80; 60, -80], 
            string="mC", 
            style(color=0))), 
        Window(
          x=0.45, 
          y=0.01, 
          width=0.44, 
          height=0.65));
    equation 
      C = s;
      der(q) = f;
      e = q/C;
    end modC;
    model nlR 
      extends BondLib.Interfaces.PassiveOnePort;
      parameter Real r=1 "Linear resistance";
      parameter Real r2=0 "Quadratic resistance";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(Text(
            extent=[-80, 80; 80, -80], 
            string="nl.R", 
            style(color=0)), Text(extent=[-70, 90; 70, 50], string="%name")), 
        Diagram(Text(
            extent=[-80, 80; 80, -80], 
            string="nl.R", 
            style(color=0))));
    equation 
      e = r*f + r2*f^2;
    end nlR;
    block Linear 
      parameter Real m "Gradient";
      parameter Real b "Offset";
      input Real u "Input signal";
      output Real y "Output signal";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(
          Rectangle(extent=[-80, 80; 80, -80], style(thickness=2)), 
          Line(points=[-40, -40; -40, 60; -44, 48; -36, 48; -40, 58]), 
          Line(points=[-40, -40; 60, -40; 48, -34; 48, -44; 60, -40]), 
          Text(extent=[-64, 62; -46, 38], string="y"), 
          Text(extent=[40, -50; 62, -70], string="u"), 
          Line(points=[-40, -10; 48, 30])), 
        Icon(
          Text(extent=[-98, -80; 100, -118], string="%name"), 
          Rectangle(extent=[-80, 80; 80, -80], style(thickness=2)), 
          Line(points=[-40, -40; -40, 60; -44, 48; -36, 48; -40, 58]), 
          Line(points=[-40, -40; 60, -40; 48, -34; 48, -44; 60, -40]), 
          Text(extent=[-64, 62; -46, 38], string="y"), 
          Text(extent=[40, -50; 62, -70], string="u"), 
          Line(points=[-40, -10; 48, 30])));
      SystemDynamics.Interfaces.InPort InPort1 annotation (extent=[-100, -10; 
            -80, 10]);
      SystemDynamics.Interfaces.OutPort OutPort1 annotation (extent=[80, -10; 
            100, 10]);
    equation 
      u = InPort1.signal[1];
      y = m*u + b;
      OutPort1.signal[1] = y;
    end Linear;
    model Heart_Chamber 
      extends BondLib.Interfaces.TwoPort;
      parameter Real m=1 "Multiplier of modulating signal";
      parameter Real b=0 "Bias of modulating signal";
      parameter Real V0=0 "Initial volume";
      parameter Real Vr=0 "Residual volume";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Window(
          x=0.45, 
          y=0.01, 
          width=0.44, 
          height=0.65), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80]), 
          Line(points=[-92, 0; -80, 0]), 
          Line(points=[80, 0; 94, 0]), 
          Text(extent=[-60, 40; 60, 0], string="Heart"), 
          Text(extent=[-70, 0; 70, -40], string="Chamber")), 
        Diagram(
          Text(extent=[-50, -6; -24, -16], string="Elasticity"), 
          Ellipse(extent=[-46, -76; -42, -80], style(arrow=4, fillColor=73)), 
          Text(extent=[6, -50; 32, -60], string="Pressure"), 
          Text(extent=[8, -44; 30, -54], string="Thoracic")));
      BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-50, -10; -30, 10]);
      BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[30, -10; 50, 10]);
      BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[-10, 10; 10, -10]);
      BondLib.Junctions.J1p4 J1p4_1 annotation (extent=[-10, 30; 10, 50]);
      BondLib.Bonds.eBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.fBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.eBond B3 annotation (extent=[-10, 10; 10, 30], rotation=90
        );
      BondLib.Bonds.eBond B4 annotation (extent=[-10, 50; 10, 70], rotation=90
        );
      BondLib.Bonds.fBond B5 annotation (extent=[-30, 30; -10, 50]);
      BondLib.Bonds.fBond B6 annotation (extent=[10, 30; 30, 50], rotation=180
        );
      BondLib.Sources.mSE mSE2 annotation (extent=[30, 30; 50, 50]);
      BondLib.Sources.mSE mSE1 annotation (extent=[-30, 30; -50, 50]);
      modC C1(
        q0=V0, 
        m=m, 
        b=b) annotation (extent=[-10, 70; 10, 90], rotation=90);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[-100, -70
            ; -80, -50]);
      Modelica.Blocks.Interfaces.InPort InPort2 annotation (
        extent=[10, -80; -10, -100], 
        rotation=-90, 
        layer="icon");
      Linear Lin1(m=m, b=b) annotation (extent=[-78, -92; -48, -62]);
      Modelica.Blocks.Math.Division Div1 annotation (extent=[-36, -82; -18, -
            62]);
      Modelica.Blocks.Sources.Constant One annotation (extent=[-74, -52; -54, 
            -32]);
      Modelica.Blocks.Math.Gain Gain1(k={Vr}) annotation (extent=[-70, 10; -50
            , 30], rotation=90);
      Modelica.Blocks.Math.Gain Gain2(k={-1}) annotation (extent=[40, -70; 60
            , -50]);
    equation 
      connect(B1.eBondCon1, J0p3_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J0p2_1.BondCon2, B1.fBondCon1) annotation (points=[-30, 0; -30, 
            0], style(color=8));
      connect(J0p2_1.BondCon1, BondCon1) annotation (points=[-50, 0; -94, 0], 
          style(color=8));
      connect(J0p3_1.BondCon2, B2.eBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(B2.fBondCon1, J0p2_2.BondCon1) annotation (points=[30, 0; 30, 0]
          , style(color=8));
      connect(J0p2_2.BondCon2, BondCon2) annotation (points=[50, 0; 94, 0], 
          style(color=8));
      connect(J0p3_1.BondCon3, B3.fBondCon1) annotation (points=[0, 10; 0, 10]
          , style(color=8));
      connect(B3.eBondCon1, J1p4_1.BondCon3) annotation (points=[0, 30; 0, 30]
          , style(color=8));
      connect(J1p4_1.BondCon4, B4.fBondCon1) annotation (points=[0, 50; 0, 50]
          , style(color=8));
      connect(B5.fBondCon1, J1p4_1.BondCon1) annotation (points=[-10, 40; -10
            , 40], style(color=8));
      connect(B6.fBondCon1, J1p4_1.BondCon2) annotation (points=[10, 40; 10, 
            40], style(color=8));
      connect(mSE2.BondCon1, B6.eBondCon1) annotation (points=[30, 40; 30, 40]
          , style(color=8));
      connect(mSE1.BondCon1, B5.eBondCon1) annotation (points=[-30, 40; -30, 
            40], style(color=8));
      connect(B4.eBondCon1, C1.BondCon1) annotation (points=[0, 70; 0, 70], 
          style(color=8));
      connect(Lin1.InPort1, InPort1) annotation (points=[-76.5, -77; -81, -77
            ; -81, -60; -90, -60], style(color=3));
      connect(One.outPort, Div1.inPort1) annotation (points=[-53, -42; -46, -
            42; -46, -66; -37.8, -66], style(color=3));
      connect(Div1.outPort, C1.InPort1) annotation (points=[-17.1, -72; -14, -
            72; -14, -20; -80, -20; -80, 96; 0, 96; 0, 90], style(color=3));
      connect(Gain1.outPort, mSE1.InPort1) annotation (points=[-60, 30; -60, 
            40; -50, 40], style(color=3));
      connect(Lin1.OutPort1, Div1.inPort2) annotation (points=[-48, -78; -37.8
            , -78], style(color=3));
      connect(Gain1.inPort, Lin1.OutPort1) annotation (points=[-60, 6; -60, -
            14; -10, -14; -10, -86; -44, -86; -44, -78; -48, -78], style(color=
              3));
      connect(InPort2, Gain2.inPort) annotation (points=[0, -80; 0, -60; 36, -
            60], style(color=3));
      connect(Gain2.outPort, mSE2.InPort1) annotation (points=[60, -60; 80, -
            60; 80, 40; 50, 40], style(color=3));
    end Heart_Chamber;
    function Piecewise 
      input Real x "Independent variable";
      input Real x_grid[:] "Independent variable data points";
      input Real y_grid[:] "Dependent variable data points";
      output Real y "Interpolated result";
    protected 
      Integer n;
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(
          Rectangle(extent=[-44, 60; -8, 40], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 40; -8, 20], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 20; -8, -2], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 0; -8, -22], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, -20; -8, -42], style(color=0, fillColor=49))
            , 
          Rectangle(extent=[-44, -40; -8, -60], style(color=0, fillColor=49))
            , 
          Rectangle(extent=[-8, 60; 28, 40], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 40; 28, 20], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 20; 28, -2], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 0; 28, -20], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, -20; 28, -40], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, -40; 28, -60], style(color=0, fillColor=7))), 
        Diagram(
          Rectangle(extent=[-44, 60; -8, 40], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 40; -8, 20], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 20; -8, -2], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 0; -8, -22], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, -20; -8, -42], style(color=0, fillColor=49))
            , 
          Rectangle(extent=[-44, -40; -8, -60], style(color=0, fillColor=49))
            , 
          Rectangle(extent=[-8, 60; 28, 40], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 40; 28, 20], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 20; 28, -2], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 0; 28, -20], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, -20; 28, -40], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, -40; 28, -60], style(color=0, fillColor=7)), 
          Text(extent=[-96, 94; 98, 70], string="Linear interpolation")));
    algorithm 
      n := size(x_grid, 1);
      assert(size(x_grid, 1) == size(y_grid, 1), "Size mismatch");
      assert(x >= x_grid[1] and x <= x_grid[n], "Out of range");
      for i in 1:n - 1 loop
        if x >= x_grid[i] and x <= x_grid[i + 1] then
          y := y_grid[i] + (y_grid[i + 1] - y_grid[i])*((x - x_grid[i])/(
            x_grid[i + 1] - x_grid[i]));
        end if;
      end for;
    end Piecewise;
    block Table_Lookup 
      parameter Real x_vals[:]={0} "Independent variable data points";
      parameter Real y_vals[:]={0} "Dependent variable data points";
      input Real u "Input variable";
      output Real y "Output variable";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(
          Line(points=[-70, 0; -44, 0], style(color=9)), 
          Line(points=[28, 0; 100, 0], style(color=9)), 
          Text(
            extent=[-72, 20; -46, 2], 
            string="u", 
            style(color=9)), 
          Text(
            extent=[52, 20; 78, 2], 
            string="y", 
            style(color=9)), 
          Rectangle(extent=[-44, 60; -8, 40], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 40; -8, 20], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 20; -8, -2], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 0; -8, -22], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, -20; -8, -42], style(color=0, fillColor=49))
            , 
          Rectangle(extent=[-44, -40; -8, -60], style(color=0, fillColor=49))
            , 
          Rectangle(extent=[-8, 60; 28, 40], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 40; 28, 20], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 20; 28, -2], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 0; 28, -20], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, -20; 28, -40], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, -40; 28, -60], style(color=0, fillColor=7))), 
        Icon(
          Text(extent=[-100, 122; 100, 80], string="%name"), 
          Line(points=[-70, 80; -70, -80; 40, -80; 100, 0; 40, 80; -70, 80], 
              style(thickness=2)), 
          Rectangle(extent=[-44, 60; -8, 40], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 40; -8, 20], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 20; -8, -2], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, 0; -8, -22], style(color=0, fillColor=49)), 
          Rectangle(extent=[-44, -20; -8, -42], style(color=0, fillColor=49))
            , 
          Rectangle(extent=[-44, -40; -8, -60], style(color=0, fillColor=49))
            , 
          Rectangle(extent=[-8, 60; 28, 40], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 40; 28, 20], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 20; 28, -2], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, 0; 28, -20], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, -20; 28, -40], style(color=0, fillColor=7)), 
          Rectangle(extent=[-8, -40; 28, -60], style(color=0, fillColor=7))));
      SystemDynamics.Interfaces.InPort InPort1 annotation (extent=[-90, -10; -
            70, 10]);
      SystemDynamics.Interfaces.OutPort OutPort1 annotation (extent=[100, -10
            ; 120, 10]);
    equation 
      u = InPort1.signal[1];
      y = Interfaces.Piecewise(x=u, x_grid=x_vals, y_grid=y_vals);
      OutPort1.signal[1] = y;
    end Table_Lookup;
  end Generic;
  package HeartLib 
    annotation (
      Coordsys(
        extent=[0, 0; 402, 261], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Rectangle(extent=[-100, -100; 80, 50], style(fillColor=30, fillPattern
              =1)), 
        Polygon(points=[-100, 50; -80, 70; 100, 70; 80, 50; -100, 50], style(
              fillColor=30, fillPattern=1)), 
        Polygon(points=[100, 70; 100, -80; 80, -100; 80, 50; 100, 70], style(
              fillColor=30, fillPattern=1)), 
        Text(
          extent=[-85, 35; 65, -85], 
          string="Library", 
          style(color=3)), 
        Text(
          extent=[-120, 122; 120, 73], 
          string="%name", 
          style(color=1))), 
      Window(
        x=0.02, 
        y=0.11, 
        width=0.4, 
        height=0.4, 
        library=1, 
        autolayout=1));
    model Left_Atrium 
      parameter Real EAIS=0.28 "Max. elasticity";
      parameter Real EAID=0.12 "Min. elasticity";
      parameter Real VAI0=69.23 "Initial volume";
      parameter Real VUAI=30.0 "Residual volume";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80], style(fillColor=43)), 
          Text(extent=[-48, 40; 50, 0], string="Left"), 
          Text(extent=[-58, 0; 60, -40], string="Atrium"), 
          Line(points=[0, 92; 0, 80]), 
          Line(points=[0, -80; 0, -94])), 
        Diagram(
          Text(extent=[-30, -36; -10, -44], string="EAIK2"), 
          Text(extent=[4, -22; 16, -32], string="X"), 
          Text(extent=[-82, -46; -72, -56], string="B2"), 
          Text(extent=[-40, -64; 38, -72], string="Thoracic Pressure")), 
        Window(
          x=0.02, 
          y=0.02, 
          width=0.44, 
          height=0.65));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-10, 90; 10, 110
            ], layer="icon");
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[-10, -110; 10, -
            90], layer="icon");
      Generic.Heart_Chamber Left_Atrium_intern(
        b=EAID, 
        V0=VAI0, 
        Vr=VUAI) annotation (extent=[20, 22; 80, 78]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (
        extent=[-70, -100; -50, -80], 
        rotation=90, 
        layer="icon");
      Modelica.Blocks.Interfaces.InPort InPort2 annotation (extent=[-100, -70
            ; -80, -50], layer="icon");
      Modelica.Blocks.Interfaces.InPort InPort3 annotation (extent=[100, -70; 
            80, -50], layer="icon");
      Generic.Linear Lin1(m=EAIS, b=-EAID) annotation (extent=[-64, -58; -34, 
            -28]);
      Modelica.Blocks.Math.Product P1 annotation (extent=[-10, 0; 10, 20], 
          rotation=90);
    equation 
      connect(InPort2, Lin1.InPort1) annotation (points=[-80, -60; -70, -
            60; -70, -43; -62.5, -43], style(color=3));
      connect(P1.outPort, Left_Atrium_intern.InPort1) annotation (points=[0, 
            22; 0, 34; 20, 34], style(color=3));
      connect(Lin1.OutPort1, P1.inPort1) annotation (points=[-34, -44; -6, -44
            ; -6, -4], style(color=3));
      connect(InPort3, P1.inPort2) annotation (points=[80, -60; 6, -60; 6, -4]
          , style(color=3));
      connect(InPort1, Left_Atrium_intern.InPort2) annotation (points=[-60, -
            80; -60, -72; 50, -72; 50, 24.9], style(color=3));
      connect(Left_Atrium_intern.BondCon2, BondCon2) annotation (points=[82, 
            50; 90, 50; 90, 0; 70, 0; 70, -86; 0, -86; 0, -94], style(color=8))
        ;
      connect(Left_Atrium_intern.BondCon1, BondCon1) annotation (points=[18, 
            50; 0, 50; 0, 94], style(color=8));
    end Left_Atrium;
    model Right_Atrium 
      parameter Real EADS=0.15 "Max. elasticity";
      parameter Real EADD=0.05 "Min. elasticity";
      parameter Real VAD0=45.51 "Initial volume";
      parameter Real VUAD=30.0 "Residual volume";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80], style(fillColor=71)), 
          Text(extent=[-48, 40; 50, 0], string="Right"), 
          Text(extent=[-58, 0; 60, -40], string="Atrium"), 
          Line(points=[-80, 40; -94, 40]), 
          Line(points=[0, -80; 0, -94]), 
          Line(points=[60, -80; 60, -94]), 
          Line(points=[-80, -30; -94, -30]), 
          Line(points=[-30, -94; -30, -80])), 
        Diagram(
          Text(extent=[4, -22; 16, -32], string="X"), 
          Text(extent=[-82, -46; -72, -56], string="B2"), 
          Text(extent=[-32, -34; -8, -44], string="EADK2"), 
          Text(extent=[-40, -64; 38, -72], string="Thoracic Pressure"), 
          Line(points=[-10, -10; -10, -10], style(color=8))), 
        Window(
          x=0.52, 
          y=0.04, 
          width=0.44, 
          height=0.65));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, 30; -90, 
            50], layer="icon");
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[-110, -40; -90, 
            -20], layer="icon");
      BondLib.Interfaces.BondCon BondCon3 annotation (extent=[50, -110; 70, -
            90], layer="icon");
      BondLib.Interfaces.BondCon BondCon4 annotation (extent=[-10, -110; 10, -
            90], layer="icon");
      Generic.Heart_Chamber Right_Atrium_intern(
        b=EADD, 
        V0=VAD0, 
        Vr=VUAD) annotation (extent=[20, 22; 80, 78]);
      BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-90, -40; -70, -20]);
      BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[-90, 30; -70, 50]);
      BondLib.Junctions.J0p2 J0p2_3 annotation (extent=[-50, -20; -30, 0], 
          rotation=90);
      BondLib.Bonds.eBond B1 annotation (extent=[-60, 60; -40, 80]);
      BondLib.Bonds.eBond B2 annotation (extent=[-70, 0; -50, 20], rotation=90
        );
      BondLib.Bonds.eBond B3 annotation (extent=[-50, 0; -30, 20], rotation=90
        );
      BondLib.Bonds.eBond B4 annotation (extent=[-34, 0; -14, 20], rotation=90
        );
      BondLib.Interfaces.BondCon BondCon5 annotation (extent=[-40, -110; -20, 
            -90], layer="icon");
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (
        extent=[-70, -100; -50, -80], 
        rotation=90, 
        layer="icon");
      Modelica.Blocks.Interfaces.InPort InPort2 annotation (extent=[-100, -70
            ; -80, -50], layer="icon");
      Modelica.Blocks.Interfaces.InPort InPort3 annotation (extent=[100, -70; 
            80, -50], layer="icon");
      Modelica.Blocks.Math.Product P1 annotation (extent=[-10, 0; 10, 20], 
          rotation=90);
      Generic.Linear Lin1(m=EADS, b=-EADD) annotation (extent=[-64, -58; -34, 
            -28]);
      BondLib.Bonds.eBond B5 annotation (extent=[-10, 40; 10, 60]);
      BondLib.Junctions.J0p5 J0p5_1 annotation (extent=[-40, 40; -20, 60]);
      BondLib.Junctions.J0p2 J0p2_4 annotation (extent=[-34, -20; -14, 0], 
          rotation=90);
    equation 
      connect(P1.outPort, Right_Atrium_intern.InPort1) annotation (points=
            [0, 22; 0, 34; 20, 34], style(color=3));
      connect(InPort3, P1.inPort2) annotation (points=[80, -60; 6, -60; 6, -4]
          , style(color=3));
      connect(Lin1.OutPort1, P1.inPort1) annotation (points=[-34, -44; -6, -44
            ; -6, -4], style(color=3));
      connect(InPort2, Lin1.InPort1) annotation (points=[-80, -60; -70, -60; -
            70, -42; -64, -42], style(color=3));
      connect(InPort1, Right_Atrium_intern.InPort2) annotation (points=[-60, -
            80; -60, -72; 50, -72; 50, 22], style(color=3));
      connect(B5.eBondCon1, Right_Atrium_intern.BondCon1) annotation (points=[
            10, 50; 18, 50], style(color=8));
      connect(J0p5_1.BondCon2, B5.fBondCon1) annotation (points=[-20, 50; -10
            , 50], style(color=8));
      connect(J0p2_2.BondCon1, BondCon1) annotation (points=[-90, 40; -94, 40]
          , style(color=8));
      connect(BondCon3, Right_Atrium_intern.BondCon2) annotation (points=[60, 
            -94; 60, -20; 88, -20; 88, 50; 82, 50], style(color=8));
      connect(B1.eBondCon1, J0p5_1.BondCon5) annotation (points=[-40, 70; -30
            , 70; -30, 60], style(color=8));
      connect(J0p2_2.BondCon2, B1.fBondCon1) annotation (points=[-70, 40; -66
            , 40; -66, 70; -60, 70], style(color=8));
      connect(B2.eBondCon1, J0p5_1.BondCon1) annotation (points=[-60, 20; -60
            , 50; -40, 50], style(color=8));
      connect(J0p2_1.BondCon1, BondCon2) annotation (points=[-90, -30; -94, -
            30], style(color=8));
      connect(J0p2_1.BondCon2, B2.fBondCon1) annotation (points=[-70, -30; -66
            , -30; -66, -8; -60, -8; -60, 0], style(color=8));
      connect(B3.eBondCon1, J0p5_1.BondCon3) annotation (points=[-40, 20; -40
            , 30; -34, 30; -34, 40], style(color=8));
      connect(B4.eBondCon1, J0p5_1.BondCon4) annotation (points=[-24, 20; -24
            , 30; -26, 30; -26, 40], style(color=8));
      connect(J0p2_3.BondCon2, B3.fBondCon1) annotation (points=[-40, 0; -40, 
            0], style(color=8));
      connect(J0p2_4.BondCon2, B4.fBondCon1) annotation (points=[-24, 0; -24, 
            0], style(color=8));
      connect(BondCon5, J0p2_3.BondCon1) annotation (points=[-30, -94; -30, -
            26; -40, -26; -40, -20], style(color=8));
      connect(BondCon4, J0p2_4.BondCon1) annotation (points=[0, -94; 0, -26; -
            24, -26; -24, -20], style(color=8));
    end Right_Atrium;
    model Left_Ventriculum 
      parameter Real EVIS=0.955 "Max. elasticity";
      parameter Real EVID=0.0225 "Min. elasticity";
      parameter Real VVI0=196.22 "Initial volume";
      parameter Real VUVI=0.0 "Residual volume";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80], style(fillColor=43)), 
          Text(extent=[-48, 40; 50, 0], string="Left"), 
          Text(extent=[-78, 0; 80, -40], string="Ventriculum"), 
          Line(points=[0, 92; 0, 80]), 
          Line(points=[80, 0; 94, 0])), 
        Diagram(
          Text(extent=[-82, -46; -72, -56], string="B2"), 
          Text(extent=[-30, -36; -10, -44], string="EVIK2"), 
          Text(extent=[4, -22; 16, -32], string="Y"), 
          Text(extent=[-16, -64; 62, -72], string="Thoracic Pressure")), 
        Window(
          x=0.02, 
          y=0.03, 
          width=0.44, 
          height=0.65));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-10, 90; 10, 110
            ], layer="icon");
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ], layer="icon");
      Generic.Heart_Chamber Left_Ventriculum_intern(
        b=EVID, 
        V0=VVI0, 
        Vr=VUVI) annotation (extent=[20, 22; 80, 78]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[-10, -100
            ; 10, -80], rotation=90);
      Modelica.Blocks.Interfaces.InPort InPort2 annotation (extent=[-100, -70
            ; -80, -50]);
      Modelica.Blocks.Interfaces.InPort InPort3 annotation (extent=[100, -70; 
            80, -50]);
      Generic.Linear Lin1(m=EVIS, b=-EVID) annotation (extent=[-64, -58; -34, 
            -28]);
      Modelica.Blocks.Math.Product P1 annotation (extent=[-10, 0; 10, 20], 
          rotation=90);
    equation 
      connect(Left_Ventriculum_intern.BondCon1, BondCon1) annotation (
          points=[18, 50; 0, 50; 0, 94], style(color=8));
      connect(Left_Ventriculum_intern.BondCon2, BondCon2) annotation (points=[
            82, 50; 86, 50; 86, 0; 94, 0], style(color=8));
      connect(InPort2, Lin1.InPort1) annotation (points=[-80, -60; -70, -60; -
            70, -44; -64, -44], style(color=3));
      connect(P1.outPort, Left_Ventriculum_intern.InPort1) annotation (points=
            [0, 22; 0, 34; 20, 34], style(color=3));
      connect(P1.inPort1, Lin1.OutPort1) annotation (points=[-6, -4; -6, -44; 
            -34, -44], style(color=3));
      connect(InPort3, P1.inPort2) annotation (points=[80, -60; 6, -60; 6, -4]
          , style(color=3));
      connect(InPort1, Left_Ventriculum_intern.InPort2) annotation (points=[0
            , -80; 0, -72; 50, -72; 50, 22], style(color=3));
    end Left_Ventriculum;
    model Right_Ventriculum 
      parameter Real EVDS=0.3 "Max. elasticity";
      parameter Real EVDD=0.0015 "Min. elasticity";
      parameter Real VVD0=270.91 "Initial volume";
      parameter Real VUVD=0.0 "Residual volume";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80], style(fillColor=71)), 
          Text(extent=[-48, 40; 50, 0], string="Right"), 
          Text(extent=[-78, 0; 80, -40], string="Ventriculum"), 
          Line(points=[0, 92; 0, 80]), 
          Line(points=[-60, 80; -60, 94])), 
        Window(
          x=0.51, 
          y=0.02, 
          width=0.44, 
          height=0.65), 
        Diagram(
          Text(extent=[-82, -46; -72, -56], string="B2"), 
          Text(extent=[-32, -34; -8, -44], string="EVDK2"), 
          Text(extent=[4, -22; 16, -32], string="Y"), 
          Text(extent=[-16, -64; 62, -72], string="Thoracic Pressure")));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-10, 90; 10, 110
            ], layer="icon");
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[-70, 90; -50, 
            110], layer="icon");
      Generic.Heart_Chamber Left_Ventriculum_intern(
        b=EVDD, 
        V0=VVD0, 
        Vr=VUVD) annotation (extent=[20, 22; 80, 78]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[-10, -100
            ; 10, -80], rotation=90);
      Modelica.Blocks.Interfaces.InPort InPort2 annotation (extent=[-100, -70
            ; -80, -50]);
      Modelica.Blocks.Interfaces.InPort InPort3 annotation (extent=[100, -70; 
            80, -50]);
      Modelica.Blocks.Math.Product P1 annotation (extent=[-10, 0; 10, 20], 
          rotation=90);
      Generic.Linear Lin1(m=EVDS, b=-EVDD) annotation (extent=[-64, -58; -34, 
            -28]);
    equation 
      connect(InPort2, Lin1.InPort1) annotation (points=[-80, -60; -70, -
            60; -70, -42; -64, -42], style(color=3));
      connect(Lin1.OutPort1, P1.inPort1) annotation (points=[-34, -44; -6, -44
            ; -6, -4], style(color=3));
      connect(InPort3, P1.inPort2) annotation (points=[80, -60; 6, -60; 6, -4]
          , style(color=3));
      connect(InPort1, Left_Ventriculum_intern.InPort2) annotation (points=[0
            , -80; 0, -72; 50, -72; 50, 22], style(color=3));
      connect(P1.outPort, Left_Ventriculum_intern.InPort1) annotation (points=
            [0, 22; 0, 34; 20, 34], style(color=3));
      connect(Left_Ventriculum_intern.BondCon1, BondCon2) annotation (points=[
            18, 50; -60, 50; -60, 94], style(color=8));
      connect(Left_Ventriculum_intern.BondCon2, BondCon1) annotation (points=[
            82, 50; 90, 50; 90, 80; 0, 80; 0, 94], style(color=8));
    end Right_Ventriculum;
    model Mitral_Valve 
      parameter Real r12=0.003 "Resistance";
      parameter Real tauF12=0.01 "Time constant";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(
          Polygon(points=[94, 0; 60, 40; 60, 14; -94, 14; -94, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=43)), 
          Text(extent=[-80, 44; 42, 18], string="Mitral"), 
          Text(extent=[-76, -18; 46, -44], string="Valve")), 
        Window(
          x=0.48, 
          y=0.06, 
          width=0.44, 
          height=0.65));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10], layer="icon");
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ], layer="icon");
      BondLib.Junctions.J1p5 J1p5_1 annotation (extent=[-10, 10; 10, -10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.eBond B3 annotation (extent=[-20, 20; 0, 40], rotation=90)
        ;
      BondLib.Bonds.Bond B4 annotation (extent=[0, 20; 20, 40], rotation=90);
      BondLib.Bonds.Bond B5 annotation (extent=[-10, -30; 10, -10], rotation=
            270);
      BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[-10, -30; 10, -50]);
      BondLib.Bonds.fBond B6 annotation (extent=[-30, -50; -10, -30], rotation
          =180);
      BondLib.Bonds.Bond B7 annotation (extent=[10, -50; 30, -30]);
      BondLib.Switches.Sw Sw1 annotation (extent=[30, -50; 50, -30]);
      BondLib.Switches.Sw Sw2 annotation (extent=[0, 40; 20, 60], rotation=90)
        ;
      BondLib.Passive.R R1(R=r12) annotation (extent=[-20, 40; 0, 60], 
          rotation=90);
      BondLib.Passive.I I1(I=r12*tauF12) annotation (extent=[-30, -50; -50, -
            30]);
    equation 
      connect(B1.fBondCon1, J1p5_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J1p5_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(J1p5_1.BondCon3, B3.fBondCon1) annotation (points=[-4, 10; -4, 
            15; -10, 15; -10, 20], style(color=8));
      connect(J1p5_1.BondCon4, B4.BondCon1) annotation (points=[4, 10; 4, 15; 
            10, 15; 10, 20], style(color=8));
      connect(J1p5_1.BondCon5, B5.BondCon1) annotation (points=[0, -10; 0, -
            10.2], style(color=8));
      connect(B5.BondCon2, J0p3_1.BondCon3) annotation (points=[0, -30; 0, -30
            ], style(color=8));
      connect(J0p3_1.BondCon1, B6.eBondCon1) annotation (points=[-10, -40; -10
            , -40], style(color=8));
      connect(J0p3_1.BondCon2, B7.BondCon1) annotation (points=[10, -40; 10.2
            , -40], style(color=8));
      connect(B7.BondCon2, Sw1.BondCon1) annotation (points=[30, -40; 30, -40]
          , style(color=8));
      connect(B4.BondCon2, Sw2.BondCon1) annotation (points=[10, 40; 10, 40], 
          style(color=8));
      connect(B3.eBondCon1, R1.BondCon1) annotation (points=[-10, 40; -10, 40]
          , style(color=8));
      connect(B6.fBondCon1, I1.BondCon1) annotation (points=[-30, -40; -30, -
            40], style(color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -94, 0], 
          style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
      Sw1.open = BondCon1.e > BondCon2.e;
      Sw2.open = not Sw1.open;
    end Mitral_Valve;
    model Tricuspid_Valve 
      parameter Real r8=0.003 "Resistance";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(
          Polygon(points=[94, 0; 60, 40; 60, 14; -94, 14; -94, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=71)), 
          Text(extent=[-80, 46; 42, 20], string="Triscuspid"), 
          Text(extent=[-76, -20; 46, -46], string="Valve")), 
        Window(
          x=0.45, 
          y=0.01, 
          width=0.44, 
          height=0.65));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10], layer="icon");
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ], layer="icon");
      BondLib.Junctions.J1p4 J1p4_1 annotation (extent=[-10, -10; 10, 10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.Bond B3 annotation (extent=[-10, 10; 10, 30], rotation=90)
        ;
      BondLib.Bonds.Bond B4 annotation (extent=[-10, -30; 10, -10], rotation=
            270);
      BondLib.Passive.R R1(R=r8) annotation (extent=[-10, 30; 10, 50], 
          rotation=90);
      BondLib.Switches.Sw Sw1 annotation (extent=[-10, -50; 10, -30], rotation
          =-90);
    equation 
      connect(B1.fBondCon1, J1p4_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J1p4_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(J1p4_1.BondCon4, B3.BondCon1) annotation (points=[0, 10; 0, 10.2
            ], style(color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -94, 0], 
          style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
      connect(B3.BondCon2, R1.BondCon1) annotation (points=[0, 30; 0, 30], 
          style(color=8));
      connect(J1p4_1.BondCon3, B4.BondCon1) annotation (points=[0, -10; 
            1.77636e-015, -10], style(color=8));
      connect(B4.BondCon2, Sw1.BondCon1) annotation (points=[-1.77636e-015, -
            30; 0, -30], style(color=8));
      Sw1.open = BondCon1.e < BondCon2.e;
    end Tricuspid_Valve;
    model Aortic_Valve 
      parameter Real r0=0.000796;
      parameter Real r1=0.003 "Linear resistance";
      parameter Real KF1=0.001;
      parameter Real AVI=1.539;
      parameter Real DF1K1=KF1*r0/(2*AVI^2) "Quadratic resistance";
      parameter Real L1=0.00022 "Inductance";
      annotation (Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), Icon(
          Polygon(points=[94, 0; 60, 40; 60, 14; -94, 14; -94, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=43)), 
          Text(extent=[-78, 46; 44, 20], string="Aortic"), 
          Text(extent=[-76, -20; 46, -46], string="Valve")));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10], layer="icon");
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ], layer="icon");
      BondLib.Junctions.J1p5 J1p5_1 annotation (extent=[-10, 10; 10, -10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.eBond B3 annotation (extent=[-20, 20; 0, 40], rotation=90)
        ;
      BondLib.Bonds.Bond B4 annotation (extent=[0, 20; 20, 40], rotation=90);
      BondLib.Bonds.Bond B5 annotation (extent=[-10, -30; 10, -10], rotation=
            270);
      BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[-10, -30; 10, -50]);
      BondLib.Bonds.fBond B6 annotation (extent=[-30, -50; -10, -30], rotation
          =180);
      BondLib.Bonds.Bond B7 annotation (extent=[10, -50; 30, -30]);
      BondLib.Switches.Sw Sw1 annotation (extent=[30, -50; 50, -30]);
      BondLib.Switches.Sw Sw2 annotation (extent=[0, 40; 20, 60], rotation=90)
        ;
      BondLib.Passive.I I1(I=L1) annotation (extent=[-30, -50; -50, -30]);
      Generic.nlR nlR1(r=r1, r2=DF1K1) annotation (extent=[-20, 40; 0, 60], 
          rotation=90);
    equation 
      connect(B1.fBondCon1, J1p5_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J1p5_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(J1p5_1.BondCon3, B3.fBondCon1) annotation (points=[-4, 10; -4, 
            15; -10, 15; -10, 20], style(color=8));
      connect(J1p5_1.BondCon4, B4.BondCon1) annotation (points=[4, 10; 4, 15; 
            10, 15; 10, 20], style(color=8));
      connect(J1p5_1.BondCon5, B5.BondCon1) annotation (points=[0, -10; 0, -
            10.2], style(color=8));
      connect(B5.BondCon2, J0p3_1.BondCon3) annotation (points=[0, -30; 0, -30
            ], style(color=8));
      connect(J0p3_1.BondCon1, B6.eBondCon1) annotation (points=[-10, -40; -10
            , -40], style(color=8));
      connect(J0p3_1.BondCon2, B7.BondCon1) annotation (points=[10, -40; 10.2
            , -40], style(color=8));
      connect(B7.BondCon2, Sw1.BondCon1) annotation (points=[30, -40; 30, -40]
          , style(color=8));
      connect(B4.BondCon2, Sw2.BondCon1) annotation (points=[10, 40; 10, 40], 
          style(color=8));
      connect(B6.fBondCon1, I1.BondCon1) annotation (points=[-30, -40; -30, -
            40], style(color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -94, 0], 
          style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
      connect(B3.eBondCon1, nlR1.BondCon1) annotation (points=[-10, 40; -10, 
            40], style(color=8));
      Sw1.open = BondCon1.e > BondCon2.e or I1.f > 0;
      Sw2.open = not Sw1.open;
    end Aortic_Valve;
    model Pulmonar_Valve 
      parameter Real r0=0.000796;
      parameter Real r9=0.003 "Linear resistance";
      parameter Real AVD=1.539;
      parameter Real DF9K1=r0/(2*AVD^2) "Quadratic resistance";
      parameter Real L9=0.00018 "Inductance";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(
          Polygon(points=[94, 0; 60, 40; 60, 14; -94, 14; -94, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=71)), 
          Text(extent=[-78, 46; 44, 20], string="Pulmonary"), 
          Text(extent=[-76, -20; 46, -46], string="Valve")), 
        Diagram(Line(points=[-10, 40; -10, 40], style(color=8))), 
        Window(
          x=0.45, 
          y=0.08, 
          width=0.44, 
          height=0.65));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10], layer="icon");
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ], layer="icon");
      BondLib.Junctions.J1p5 J1p5_1 annotation (extent=[-10, 10; 10, -10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.eBond B3 annotation (extent=[-20, 20; 0, 40], rotation=90)
        ;
      BondLib.Bonds.Bond B4 annotation (extent=[0, 20; 20, 40], rotation=90);
      BondLib.Bonds.Bond B5 annotation (extent=[-10, -30; 10, -10], rotation=
            270);
      BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[-10, -30; 10, -50]);
      BondLib.Bonds.fBond B6 annotation (extent=[-30, -50; -10, -30], rotation
          =180);
      BondLib.Bonds.Bond B7 annotation (extent=[10, -50; 30, -30]);
      BondLib.Switches.Sw Sw1 annotation (extent=[30, -50; 50, -30]);
      BondLib.Switches.Sw Sw2 annotation (extent=[0, 40; 20, 60], rotation=90)
        ;
      BondLib.Passive.I I1(I=L9) annotation (extent=[-30, -50; -50, -30]);
      Generic.nlR nlR1(r=r9, r2=DF9K1) annotation (extent=[-20, 40; 0, 60], 
          rotation=90);
    equation 
      connect(B1.fBondCon1, J1p5_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J1p5_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(J1p5_1.BondCon3, B3.fBondCon1) annotation (points=[-4, 10; -4, 
            15; -10, 15; -10, 20], style(color=8));
      connect(J1p5_1.BondCon4, B4.BondCon1) annotation (points=[4, 10; 4, 15; 
            10, 15; 10, 20], style(color=8));
      connect(J1p5_1.BondCon5, B5.BondCon1) annotation (points=[0, -10; 0, -
            10.2], style(color=8));
      connect(B5.BondCon2, J0p3_1.BondCon3) annotation (points=[0, -30; 0, -30
            ], style(color=8));
      connect(J0p3_1.BondCon1, B6.eBondCon1) annotation (points=[-10, -40; -10
            , -40], style(color=8));
      connect(J0p3_1.BondCon2, B7.BondCon1) annotation (points=[10, -40; 10.2
            , -40], style(color=8));
      connect(B7.BondCon2, Sw1.BondCon1) annotation (points=[30, -40; 30, -40]
          , style(color=8));
      connect(B4.BondCon2, Sw2.BondCon1) annotation (points=[10, 40; 10, 40], 
          style(color=8));
      connect(B6.fBondCon1, I1.BondCon1) annotation (points=[-30, -40; -30, -
            40], style(color=8));
      connect(B3.eBondCon1, nlR1.BondCon1) annotation (points=[-10, 40; -10, 
            40], style(color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -94, 0], 
          style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
      Sw1.open = BondCon1.e > BondCon2.e or I1.f > 0;
      Sw2.open = not Sw1.open;
    end Pulmonar_Valve;
    model Heart_Muscle 
      extends BondLib.Interfaces.ModTwoPort;
      parameter Real r13=54.0 "Coronary resistance";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(
          Polygon(points=[94, 0; 60, 40; 60, 14; -12, 14; -12, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=71)), 
          Rectangle(extent=[-12, 14; -96, -14], style(fillColor=43)), 
          Text(extent=[-86, 44; 52, 18], string="Heart"), 
          Text(extent=[-86, -18; 52, -44], string="Muscle")), 
        Diagram(Text(extent=[-14, -64; -2, -74], string="Q6"), Text(extent=[42
                , -40; 58, -50], string="FAC2")));
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Junctions.J1p3 J1p3_1 annotation (extent=[-10, 10; 10, -10]);
      BondLib.Bonds.Bond B3 annotation (extent=[-10, 10; 10, 30], rotation=90)
        ;
      BondLib.Bonds.Bond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Passive.mR mR1 annotation (extent=[-10, 30; 10, 50], rotation=90
        );
      Modelica.Blocks.Math.Gain Gain1(k={r13}) annotation (extent=[20, -60; 40
            , -40]);
    equation 
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], 
          style(color=8));
      connect(J1p3_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(J1p3_1.BondCon3, B3.BondCon1) annotation (points=[0, 10; 0, 10.2
            ], style(color=8));
      connect(B1.BondCon2, J1p3_1.BondCon1) annotation (points=[-10, 0; -10, 0
            ], style(color=8));
      connect(B1.BondCon1, BondCon1) annotation (points=[-30, 0; -94, 0], 
          style(color=8));
      connect(B3.BondCon2, mR1.BondCon1) annotation (points=[0, 30; 0, 30], 
          style(color=8));
      connect(InPort1, Gain1.inPort) annotation (points=[0, -80; 0, -50; 16, -
            50], style(color=3));
      connect(Gain1.outPort, mR1.InPort1) annotation (points=[42, -50; 60, -50
            ; 60, 60; 0, 60; 0, 50], style(color=3));
    end Heart_Muscle;
    block Sinus_Rhythm 
      constant Real pi=4.0*Math.atan(1.0);
      parameter Real LA10=0.1;
      parameter Real LA11=0.09;
      parameter Real LA12=0.04 
        "turn-off time of atrium minus turn-on time of ventriculum";
      parameter Real LA13=0.16;
      parameter Real LA14=0.2;
      input Real tlast "Time when  current period began";
      input Real TH "Heart rate period";
      output Real TAS "Atrial sinus on period";
      output Real piTAS "Atrial inverse sinus on period";
      output Real TVS "Ventricular sinus on period";
      output Real piTVS "Ventricular inverse sinus on period";
      output Real TAV "Atrial sinus on period minus underlap";
      output Real tcurr "Current relative time within period";
      output Real X "Sinusoidal aortic elasticity";
      output Real Y "Sinusoidal ventricular elasticity";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(
          Polygon(points=[-80, 80; -80, -80; 20, -80; 80, 0; 20, 80; -80, 80]
              , style(fillColor=51)), 
          Line(points=[-100, 0; -80, 0]), 
          Line(points=[50, 40; 100, 40]), 
          Line(points=[50, -40; 100, -40]), 
          Text(extent=[-64, 40; 36, -2], string="Sinus"), 
          Text(extent=[-68, 0; 50, -42], string="Rhythm"), 
          Text(extent=[60, 54; 78, 40], string="X"), 
          Text(extent=[60, -40; 78, -54], string="Y"), 
          Text(extent=[-100, 14; -82, 0], string="TH"), 
          Text(extent=[-34, -82; -8, -96], string="tlast")), 
        Diagram(
          Polygon(points=[-80, 80; -80, -80; 20, -80; 80, 0; 20, 80; -80, 80]
              , style(fillColor=51)), 
          Line(points=[-100, 0; -80, 0]), 
          Line(points=[50, 40; 100, 40]), 
          Line(points=[50, -40; 100, -40]), 
          Text(extent=[-64, 40; 36, -2], string="Sinus"), 
          Text(extent=[-68, 0; 50, -42], string="Rhythm"), 
          Text(extent=[-100, 14; -82, 0], string="TH"), 
          Text(extent=[60, 54; 78, 40], string="X"), 
          Text(extent=[60, -40; 78, -54], string="Y"), 
          Text(extent=[-34, -82; -8, -96], string="tlast")));
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[-120, -10
            ; -100, 10]);
      Modelica.Blocks.Interfaces.OutPort OutPort1 annotation (extent=[100, 30
            ; 120, 50]);
      Modelica.Blocks.Interfaces.OutPort OutPort2 annotation (extent=[100, -50
            ; 120, -30]);
      Modelica.Blocks.Interfaces.InPort InPort2 annotation (extent=[-10, -100
            ; 10, -80], rotation=90);
    equation 
      TH = InPort1.signal[1];
      tlast = InPort2.signal[1];
      TAS = LA11*TH + LA10;
      piTAS = pi/TAS;
      TVS = LA14*TH + LA13;
      piTVS = pi/TVS;
      TAV = TAS - LA12;
      tcurr = time - tlast;
      X = if tcurr < TAS then Math.sin(piTAS*tcurr) else 0;
      Y = if tcurr > TAV and tcurr < TAV + TVS then Math.sin(piTVS*(tcurr - 
        TAV)) else 0;
      OutPort1.signal[1] = X;
      OutPort2.signal[1] = Y;
    end Sinus_Rhythm;
  end HeartLib;
  package Arteries 
    annotation (
      Coordsys(
        extent=[0, 0; 402, 261], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Rectangle(extent=[-100, -100; 80, 50], style(fillColor=30, fillPattern
              =1)), 
        Polygon(points=[-100, 50; -80, 70; 100, 70; 80, 50; -100, 50], style(
              fillColor=30, fillPattern=1)), 
        Polygon(points=[100, 70; 100, -80; 80, -100; 80, 50; 100, 70], style(
              fillColor=30, fillPattern=1)), 
        Text(
          extent=[-85, 35; 65, -85], 
          string="Library", 
          style(color=3)), 
        Text(
          extent=[-120, 122; 120, 73], 
          string="%name", 
          style(color=1))), 
      Window(
        x=0, 
        y=0.6, 
        width=0.4, 
        height=0.4, 
        library=1, 
        autolayout=1));
    model Pulmonary_Trunk 
      parameter Real CACP=4.3 "Capacitance";
      parameter Real VUACP=50.0 "Residual Volume";
      parameter Real VACP0=139.91 "Initial Vlume";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(
          Line(points=[-30, 40; -30, 40], style(color=8)), 
          Text(extent=[34, -52; 56, -62], string="Thoracic"), 
          Text(extent=[32, -60; 58, -70], string="Pressure"), 
          Line(points=[0, 70; 0, 70], style(color=8))), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80], style(fillColor=71)), 
          Text(extent=[-60, 40; 62, 0], string="Pulmonary"), 
          Text(extent=[-60, 8; 62, -24], string="Trunk"), 
          Line(points=[80, 0; 94, 0]), 
          Line(points=[0, -80; 0, -94])));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-10, -110; 10, -
            90]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ]);
      BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[-10, 10; 10, -10]);
      BondLib.Bonds.eBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.fBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[30, -10; 50, 10]);
      BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-50, -10; -30, 10]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[50, -100; 
            70, -80], rotation=90);
      BondLib.Bonds.eBond B3 annotation (extent=[-10, 10; 10, 30], rotation=90
        );
      BondLib.Junctions.J1p4 J1p4_1 annotation (extent=[-10, 30; 10, 50]);
      BondLib.Bonds.fBond B6 annotation (extent=[10, 30; 30, 50], rotation=180
        );
      BondLib.Bonds.fBond B5 annotation (extent=[-30, 30; -10, 50]);
      BondLib.Sources.mSE mSE1 annotation (extent=[30, 30; 50, 50]);
      BondLib.Bonds.eBond B4 annotation (extent=[-10, 50; 10, 70], rotation=90
        );
      Modelica.Blocks.Math.Gain Gain2(k={-1}) annotation (extent=[50, -40; 70
            , -20], rotation=90);
      BondLib.Sources.SE SE1(e0=VUACP/CACP) annotation (extent=[-30, 30; -50, 
            50]);
      Generic.modC C1(
        q0=VACP0, 
        m=m, 
        b=b) annotation (extent=[-10, 70; 10, 90], rotation=90);
      Modelica.Blocks.Sources.Constant Con1(k={CACP}) annotation (extent=[-60
            , 70; -40, 90]);
    equation 
      connect(B1.eBondCon1, J0p3_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J0p3_1.BondCon2, B2.eBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(B2.fBondCon1, J0p2_2.BondCon1) annotation (points=[30, 0; 30, 0]
          , style(color=8));
      connect(J0p2_2.BondCon2, BondCon2) annotation (points=[50, 0; 94, 0], 
          style(color=8));
      connect(J0p2_1.BondCon2, B1.fBondCon1) annotation (points=[-30, 0; -30, 
            0], style(color=8));
      connect(J0p3_1.BondCon3, B3.fBondCon1) annotation (points=[0, 10; 0, 10]
          , style(color=8));
      connect(B3.eBondCon1, J1p4_1.BondCon3) annotation (points=[0, 30; 0, 30]
          , style(color=8));
      connect(B6.fBondCon1, J1p4_1.BondCon2) annotation (points=[10, 40; 10, 
            40], style(color=8));
      connect(B5.fBondCon1, J1p4_1.BondCon1) annotation (points=[-10, 40; -10
            , 40], style(color=8));
      connect(mSE1.BondCon1, B6.eBondCon1) annotation (points=[30, 40; 30, 40]
          , style(color=8));
      connect(J1p4_1.BondCon4, B4.fBondCon1) annotation (points=[0, 50; 0, 50]
          , style(color=8));
      connect(BondCon1, J0p2_1.BondCon1) annotation (points=[0, -94; 0, -80; -
            60, -80; -60, 0; -50, 0], style(color=8));
      connect(InPort1, Gain2.inPort) annotation (points=[60, -80; 60, -44], 
          style(color=3));
      connect(Gain2.outPort, mSE1.InPort1) annotation (points=[60, -18; 60, 40
            ; 50, 40], style(color=3));
      connect(SE1.BondCon1, B5.eBondCon1) annotation (points=[-30, 40; -30, 40
            ], style(color=8));
      connect(B4.eBondCon1, C1.BondCon1) annotation (points=[0, 70; 0, 70], 
          style(color=8));
      connect(Con1.outPort, C1.InPort1) annotation (points=[-38, 80; -20, 80; 
            -20, 94; 0, 94; 0, 90], style(color=3));
    end Pulmonary_Trunk;
    model Ascending_Aorta 
      parameter Real CA1=0.28 "Capacitance";
      parameter Real VUA1=53.0 "Residual Volume";
      parameter Real VA10=78.96 "Initial Vlume";
      parameter Real K=0.04;
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(Text(extent=[34, -52; 56, -62], string="Thoracic"), Text(
              extent=[32, -60; 58, -70], string="Pressure")), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80], style(fillColor=43)), 
          Line(points=[80, 0; 94, 0]), 
          Line(points=[0, -80; 0, -94]), 
          Line(points=[-80, 0; -94, 0]), 
          Text(extent=[-60, 40; 62, 0], string="Ascending"), 
          Text(extent=[-60, 8; 62, -24], string="Aorta")));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ]);
      BondLib.Interfaces.BondCon BondCon3 annotation (extent=[-10, -110; 10, -
            90]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[50, -100; 
            70, -80], rotation=90);
      Modelica.Blocks.Math.Gain Gain2(k={-1}) annotation (extent=[50, -40; 70
            , -20], rotation=90);
      BondLib.Bonds.eBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-50, -10; -30, 10]);
      BondLib.Bonds.Bond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Junctions.J0p4 J0p4_1 annotation (extent=[-10, -10; 10, 10]);
      BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[30, -10; 50, 10]);
      BondLib.Bonds.Bond B3 annotation (extent=[-10, -30; 10, -10], rotation=-
            90);
      BondLib.Junctions.J0p2 J0p2_3 annotation (extent=[-10, -50; 10, -30], 
          rotation=-90);
      BondLib.Bonds.Bond B4 annotation (extent=[-10, 10; 10, 30], rotation=90)
        ;
      BondLib.Junctions.J1p5 J1p5_1 annotation (extent=[-10, 50; 10, 30]);
      BondLib.Bonds.fBond B6 annotation (extent=[10, 30; 30, 50], rotation=180
        );
      BondLib.Sources.mSE mSE1 annotation (extent=[30, 30; 50, 50]);
      BondLib.Bonds.fBond B5 annotation (extent=[-30, 30; -10, 50]);
      BondLib.Sources.SE SE1(e0=VUA1/CA1) annotation (extent=[-30, 30; -50, 50
            ]);
      BondLib.Bonds.eBond B7 annotation (extent=[-30, 60; -10, 80], rotation=
            180);
      Generic.modC C1(
        q0=VA10, 
        m=m, 
        b=b) annotation (extent=[-30, 60; -50, 80]);
      Modelica.Blocks.Sources.Constant Con1(k={CA1}) annotation (extent=[-80, 
            60; -60, 80]);
      BondLib.Bonds.Bond B8 annotation (extent=[10, 60; 30, 80]);
      BondLib.Passive.R R1(R=K/CA1) annotation (extent=[30, 60; 50, 80]);
    equation 
      connect(Gain2.inPort, InPort1) annotation (points=[60, -44; 60, -80]
          , style(color=3));
      connect(J0p2_1.BondCon2, B1.fBondCon1) annotation (points=[-30, 0; -30, 
            0], style(color=8));
      connect(J0p2_1.BondCon1, BondCon1) annotation (points=[-50, 0; -94, 0], 
          style(color=8));
      connect(B1.eBondCon1, J0p4_1.BondCon1) annotation (points=[-10, 0; -10, 
            0], style(color=8));
      connect(J0p4_1.BondCon2, B2.BondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(B2.BondCon2, J0p2_2.BondCon1) annotation (points=[30, 0; 30, 0]
          , style(color=8));
      connect(J0p2_2.BondCon2, BondCon2) annotation (points=[50, 0; 94, 0], 
          style(color=8));
      connect(J0p4_1.BondCon3, B3.BondCon1) annotation (points=[0, -10; 0, -
            10.2], style(color=8));
      connect(B3.BondCon2, J0p2_3.BondCon1) annotation (points=[0, -30; 0, -30
            ], style(color=8));
      connect(J0p2_3.BondCon2, BondCon3) annotation (points=[0, -50; 0, -94], 
          style(color=8));
      connect(J0p4_1.BondCon4, B4.BondCon1) annotation (points=[0, 10; 0, 10.2
            ], style(color=8));
      connect(B4.BondCon2, J1p5_1.BondCon5) annotation (points=[0, 30; 0, 30]
          , style(color=8));
      connect(B6.fBondCon1, J1p5_1.BondCon2) annotation (points=[10, 40; 10, 
            40], style(color=8));
      connect(mSE1.BondCon1, B6.eBondCon1) annotation (points=[30, 40; 30, 40]
          , style(color=8));
      connect(Gain2.outPort, mSE1.InPort1) annotation (points=[60, -18; 60, 40
            ; 50, 40], style(color=3));
      connect(B5.fBondCon1, J1p5_1.BondCon1) annotation (points=[-10, 40; -10
            , 40], style(color=8));
      connect(SE1.BondCon1, B5.eBondCon1) annotation (points=[-30, 40; -30, 40
            ], style(color=8));
      connect(J1p5_1.BondCon3, B7.fBondCon1) annotation (points=[-4, 50; -4, 
            70; -10, 70], style(color=8));
      connect(B7.eBondCon1, C1.BondCon1) annotation (points=[-30, 70; -30, 70]
          , style(color=8));
      connect(Con1.outPort, C1.InPort1) annotation (points=[-58, 70; -50, 70]
          , style(color=3));
      connect(J1p5_1.BondCon4, B8.BondCon1) annotation (points=[4, 50; 4, 70; 
            10, 70], style(color=8));
      connect(B8.BondCon2, R1.BondCon1) annotation (points=[30, 70; 30, 70], 
          style(color=8));
    end Ascending_Aorta;
    model Aortic_Arch 
      parameter Real CA2=0.29 "Capacitance";
      parameter Real VUA2=61.0 "Residual Volume";
      parameter Real VA20=87.89 "Initial Vlume";
      parameter Real K=0.04;
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(
          Text(extent=[34, -52; 56, -62], string="Thoracic"), 
          Text(extent=[32, -60; 58, -70], string="Pressure"), 
          Line(points=[-10, 0; -10, 0], style(color=8)), 
          Line(points=[10, 0; 10.2, 0], style(color=8))), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80], style(fillColor=43)), 
          Line(points=[-80, 0; -94, 0]), 
          Line(points=[0, -80; 0, -94]), 
          Line(points=[0, 80; 0, 94]), 
          Text(extent=[-60, 40; 62, 0], string="Aortic"), 
          Text(extent=[-60, 8; 62, -24], string="Arch")));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[-10, 90; 10, 110
            ]);
      BondLib.Interfaces.BondCon BondCon3 annotation (extent=[-10, -110; 10, -
            90]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[50, -100; 
            70, -80], rotation=90);
      Modelica.Blocks.Math.Gain Gain2(k={-1}) annotation (extent=[50, -40; 70
            , -20], rotation=90);
      BondLib.Bonds.eBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-50, -10; -30, 10]);
      BondLib.Bonds.Bond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Junctions.J0p4 J0p4_1 annotation (extent=[-10, -10; 10, 10]);
      BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[30, -10; 50, 10]);
      BondLib.Bonds.Bond B3 annotation (extent=[-10, -30; 10, -10], rotation=-
            90);
      BondLib.Junctions.J0p2 J0p2_3 annotation (extent=[-10, -50; 10, -30], 
          rotation=-90);
      BondLib.Bonds.Bond B4 annotation (extent=[-10, 10; 10, 30], rotation=90)
        ;
      BondLib.Junctions.J1p5 J1p5_1 annotation (extent=[-10, 50; 10, 30]);
      BondLib.Bonds.fBond B6 annotation (extent=[10, 30; 30, 50], rotation=180
        );
      BondLib.Sources.mSE mSE1 annotation (extent=[30, 30; 50, 50]);
      BondLib.Bonds.fBond B5 annotation (extent=[-30, 30; -10, 50]);
      BondLib.Sources.SE SE1(e0=VUA2/CA2) annotation (extent=[-30, 30; -50, 50
            ]);
      BondLib.Bonds.eBond B7 annotation (extent=[-30, 60; -10, 80], rotation=
            180);
      Generic.modC C1(
        q0=VA20, 
        m=m, 
        b=b) annotation (extent=[-30, 60; -50, 80]);
      Modelica.Blocks.Sources.Constant Con1(k={CA2}) annotation (extent=[-80, 
            60; -60, 80]);
      BondLib.Bonds.Bond B8 annotation (extent=[10, 60; 30, 80]);
      BondLib.Passive.R R1(R=K/CA2) annotation (extent=[30, 60; 50, 80]);
    equation 
      connect(Gain2.inPort, InPort1) annotation (points=[60, -44; 60, -80]
          , style(color=3));
      connect(J0p2_1.BondCon2, B1.fBondCon1) annotation (points=[-30, 0; -30, 
            0], style(color=8));
      connect(J0p2_1.BondCon1, BondCon1) annotation (points=[-50, 0; -94, 0], 
          style(color=8));
      connect(B1.eBondCon1, J0p4_1.BondCon1) annotation (points=[-10, 0; -10, 
            0], style(color=8));
      connect(J0p4_1.BondCon2, B2.BondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(B2.BondCon2, J0p2_2.BondCon1) annotation (points=[30, 0; 30, 0]
          , style(color=8));
      connect(J0p4_1.BondCon3, B3.BondCon1) annotation (points=[0, -10; 0, -
            10.2], style(color=8));
      connect(B3.BondCon2, J0p2_3.BondCon1) annotation (points=[0, -30; 0, -30
            ], style(color=8));
      connect(J0p2_3.BondCon2, BondCon3) annotation (points=[0, -50; 0, -94], 
          style(color=8));
      connect(J0p4_1.BondCon4, B4.BondCon1) annotation (points=[0, 10; 0, 10.2
            ], style(color=8));
      connect(B4.BondCon2, J1p5_1.BondCon5) annotation (points=[0, 30; 0, 30]
          , style(color=8));
      connect(B6.fBondCon1, J1p5_1.BondCon2) annotation (points=[10, 40; 10, 
            40], style(color=8));
      connect(mSE1.BondCon1, B6.eBondCon1) annotation (points=[30, 40; 30, 40]
          , style(color=8));
      connect(Gain2.outPort, mSE1.InPort1) annotation (points=[60, -18; 60, 40
            ; 50, 40], style(color=3));
      connect(B5.fBondCon1, J1p5_1.BondCon1) annotation (points=[-10, 40; -10
            , 40], style(color=8));
      connect(SE1.BondCon1, B5.eBondCon1) annotation (points=[-30, 40; -30, 40
            ], style(color=8));
      connect(J1p5_1.BondCon3, B7.fBondCon1) annotation (points=[-4, 50; -4, 
            70; -10, 70], style(color=8));
      connect(B7.eBondCon1, C1.BondCon1) annotation (points=[-30, 70; -30, 70]
          , style(color=8));
      connect(Con1.outPort, C1.InPort1) annotation (points=[-58, 70; -50, 70]
          , style(color=3));
      connect(J1p5_1.BondCon4, B8.BondCon1) annotation (points=[4, 50; 4, 70; 
            10, 70], style(color=8));
      connect(B8.BondCon2, R1.BondCon1) annotation (points=[30, 70; 30, 70], 
          style(color=8));
      connect(J0p2_2.BondCon2, BondCon2) annotation (points=[50, 0; 80, 0; 80
            , 84; 0, 84; 0, 94], style(color=8));
    end Aortic_Arch;
    model Thoracic_Aorta 
      parameter Real CAT=0.29 "Capacitance";
      parameter Real VUAT=59.0 "Residual Volume";
      parameter Real VAT0=85.89 "Initial Vlume";
      parameter Real K=0.04;
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(
          Text(extent=[34, -52; 56, -62], string="Thoracic"), 
          Text(extent=[32, -60; 58, -70], string="Pressure"), 
          Line(points=[-10, 0; -10, 0], style(color=8)), 
          Line(points=[10, 0; 10.2, 0], style(color=8))), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80], style(fillColor=43)), 
          Line(points=[-80, 0; -94, 0]), 
          Line(points=[0, 80; 0, 94]), 
          Line(points=[0, -80; 0, -94]), 
          Line(points=[-60, -80; -60, -94]), 
          Text(extent=[-60, 40; 62, 0], string="Thoracic"), 
          Text(extent=[-60, 8; 62, -24], string="Aorta")));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[-10, 90; 10, 110
            ]);
      BondLib.Interfaces.BondCon BondCon3 annotation (extent=[-10, -110; 10, -
            90]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[50, -100; 
            70, -80], rotation=90);
      Modelica.Blocks.Math.Gain Gain2(k={-1}) annotation (extent=[50, -40; 70
            , -20], rotation=90);
      BondLib.Bonds.eBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-50, -10; -30, 10]);
      BondLib.Bonds.Bond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[30, -10; 50, 10]);
      BondLib.Bonds.Bond B3 annotation (extent=[-30, -40; -10, -20], rotation=
            -180);
      BondLib.Junctions.J0p2 J0p2_3 annotation (extent=[-50, -40; -30, -20], 
          rotation=-180);
      BondLib.Bonds.Bond B4 annotation (extent=[-10, 10; 10, 30], rotation=90)
        ;
      BondLib.Junctions.J1p5 J1p5_1 annotation (extent=[-10, 50; 10, 30]);
      BondLib.Bonds.fBond B6 annotation (extent=[10, 30; 30, 50], rotation=180
        );
      BondLib.Sources.mSE mSE1 annotation (extent=[30, 30; 50, 50]);
      BondLib.Bonds.fBond B5 annotation (extent=[-30, 30; -10, 50]);
      BondLib.Sources.SE SE1(e0=VUAT/CAT) annotation (extent=[-30, 30; -50, 50
            ]);
      BondLib.Bonds.eBond B7 annotation (extent=[-30, 60; -10, 80], rotation=
            180);
      Generic.modC C1(
        q0=VAT0, 
        m=m, 
        b=b) annotation (extent=[-30, 60; -50, 80]);
      Modelica.Blocks.Sources.Constant Con1(k={CAT}) annotation (extent=[-80, 
            60; -60, 80]);
      BondLib.Bonds.Bond B8 annotation (extent=[10, 60; 30, 80]);
      BondLib.Passive.R R1(R=K/CAT) annotation (extent=[30, 60; 50, 80]);
      BondLib.Junctions.J0p5 J0p5_1 annotation (extent=[-10, -10; 10, 10]);
      BondLib.Bonds.Bond B9 annotation (extent=[0, -40; 20, -20], rotation=-90
        );
      BondLib.Junctions.J0p2 J0p2_4 annotation (extent=[0, -60; 20, -40], 
          rotation=-90);
      BondLib.Interfaces.BondCon BondCon4 annotation (extent=[-70, -110; -50, 
            -90]);
    equation 
      connect(Gain2.inPort, InPort1) annotation (points=[60, -44; 60, -80]
          , style(color=3));
      connect(J0p2_1.BondCon2, B1.fBondCon1) annotation (points=[-30, 0; -30, 
            0], style(color=8));
      connect(B2.BondCon2, J0p2_2.BondCon1) annotation (points=[30, 0; 30, 0]
          , style(color=8));
      connect(B4.BondCon2, J1p5_1.BondCon5) annotation (points=[0, 30; 0, 30]
          , style(color=8));
      connect(B6.fBondCon1, J1p5_1.BondCon2) annotation (points=[10, 40; 10, 
            40], style(color=8));
      connect(mSE1.BondCon1, B6.eBondCon1) annotation (points=[30, 40; 30, 40]
          , style(color=8));
      connect(Gain2.outPort, mSE1.InPort1) annotation (points=[60, -18; 60, 40
            ; 50, 40], style(color=3));
      connect(B5.fBondCon1, J1p5_1.BondCon1) annotation (points=[-10, 40; -10
            , 40], style(color=8));
      connect(SE1.BondCon1, B5.eBondCon1) annotation (points=[-30, 40; -30, 40
            ], style(color=8));
      connect(J1p5_1.BondCon3, B7.fBondCon1) annotation (points=[-4, 50; -4, 
            70; -10, 70], style(color=8));
      connect(B7.eBondCon1, C1.BondCon1) annotation (points=[-30, 70; -30, 70]
          , style(color=8));
      connect(Con1.outPort, C1.InPort1) annotation (points=[-58, 70; -50, 70]
          , style(color=3));
      connect(J1p5_1.BondCon4, B8.BondCon1) annotation (points=[4, 50; 4, 70; 
            10, 70], style(color=8));
      connect(B8.BondCon2, R1.BondCon1) annotation (points=[30, 70; 30, 70], 
          style(color=8));
      connect(J0p2_1.BondCon1, BondCon2) annotation (points=[-50, 0; -70, 0; -
            70, 40; -88, 40; -88, 88; 0, 88; 0, 94], style(color=8));
      connect(J0p5_1.BondCon5, B4.BondCon1) annotation (points=[0, 10; 0, 10]
          , style(color=8));
      connect(J0p5_1.BondCon2, B2.BondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(B1.eBondCon1, J0p5_1.BondCon1) annotation (points=[-10, 0; -10, 
            0], style(color=8));
      connect(J0p5_1.BondCon3, B3.BondCon1) annotation (points=[-4, -10; -4, -
            30; -10, -30], style(color=8));
      connect(B3.BondCon2, J0p2_3.BondCon1) annotation (points=[-30, -30; -30
            , -30], style(color=8));
      connect(J0p2_3.BondCon2, BondCon1) annotation (points=[-50, -30; -80, -
            30; -80, 0; -94, 0], style(color=8));
      connect(J0p5_1.BondCon4, B9.BondCon1) annotation (points=[4, -10; 4, -15
            ; 10, -15; 10, -20], style(color=8));
      connect(B9.BondCon2, J0p2_4.BondCon1) annotation (points=[10, -40; 10, -
            40], style(color=8));
      connect(J0p2_4.BondCon2, BondCon4) annotation (points=[10, -60; 10, -70
            ; -60, -70; -60, -94], style(color=8));
      connect(J0p2_2.BondCon2, BondCon3) annotation (points=[50, 0; 80, 0; 80
            , -76; 0, -76; 0, -94], style(color=8));
    end Thoracic_Aorta;
    model Ascending_Aorta_Flow 
      parameter Real r2=3.1e-5 "Resistance";
      parameter Real L2=0.00043 "Inductance";
      parameter Real F02=19.5 "Initial flow";
      annotation (Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), Icon(
          Polygon(points=[94, 0; 60, 40; 60, 14; -94, 14; -94, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=43)), 
          Text(extent=[-80, 44; 42, 18], string="Ascending"), 
          Text(extent=[-76, -18; 46, -44], string="Aorta Flow")));
      BondLib.Junctions.J1p4 J1p4_1 annotation (extent=[-10, -10; 10, 10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.Bond B4 annotation (extent=[-10, -30; 10, -10], rotation=
            270);
      BondLib.Passive.R R1(R=r2) annotation (extent=[-10, 30; 10, 50], 
          rotation=90);
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ]);
      BondLib.Bonds.eBond B3 annotation (extent=[-10, 10; 10, 30], rotation=90
        );
      BondLib.Passive.I I1(I=L2, f(start=F02)) annotation (extent=[-10, -50; 
            10, -30], rotation=-90);
    equation 
      connect(B1.fBondCon1, J1p4_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J1p4_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -94, 0], 
          style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
      connect(J1p4_1.BondCon3, B4.BondCon1) annotation (points=[0, -10; 
            1.77636e-015, -10], style(color=8));
      connect(J1p4_1.BondCon4, B3.fBondCon1) annotation (points=[0, 10; -
            5.55112e-016, 10], style(color=8));
      connect(B3.eBondCon1, R1.BondCon1) annotation (points=[5.55112e-016, 30
            ; 0, 30], style(color=8));
      connect(B4.BondCon2, I1.BondCon1) annotation (points=[0, -30; 0, -30], 
          style(color=8));
    end Ascending_Aorta_Flow;
    model Descending_Aortic_Arch_Flow 
      parameter Real r3=0.0009 "Resistance";
      parameter Real L3=0.0038 "Inductance";
      parameter Real F03=37.91 "Initial flow";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(
          Line(points=[-1.77636e-015, -30; 0, -30], style(color=8)), 
          Line(points=[0, 10; 0, 10.2], style(color=8)), 
          Line(points=[0, 30; 0, 30], style(color=8))), 
        Icon(
          Polygon(points=[94, 0; 60, 40; 60, 14; -94, 14; -94, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=43)), 
          Text(extent=[-80, 44; 42, 18], string="Descending"), 
          Text(extent=[-76, -18; 46, -44], string="Aortic Arch Flow")));
      BondLib.Junctions.J1p4 J1p4_1 annotation (extent=[-10, -10; 10, 10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.Bond B4 annotation (extent=[-10, -30; 10, -10], rotation=
            270);
      BondLib.Passive.R R1(R=r3) annotation (extent=[-10, 30; 10, 50], 
          rotation=90);
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ]);
      BondLib.Bonds.eBond B3 annotation (extent=[-10, 10; 10, 30], rotation=90
        );
      BondLib.Passive.I I1(I=L3, f(start=F03)) annotation (extent=[-10, -50; 
            10, -30], rotation=-90);
    equation 
      connect(B1.fBondCon1, J1p4_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J1p4_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -94, 0], 
          style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
      connect(J1p4_1.BondCon3, B4.BondCon1) annotation (points=[0, -10; 
            1.77636e-015, -10], style(color=8));
      connect(J1p4_1.BondCon4, B3.fBondCon1) annotation (points=[0, 10; -
            5.55112e-016, 10], style(color=8));
      connect(B3.eBondCon1, R1.BondCon1) annotation (points=[5.55112e-016, 30
            ; 0, 30], style(color=8));
      connect(B4.BondCon2, I1.BondCon1) annotation (points=[0, -30; 0, -30], 
          style(color=8));
    end Descending_Aortic_Arch_Flow;
    model Ascending_Aortic_Arch_Flow 
      parameter Real r21=0.047 "Resistance";
      parameter Real L21=0.014 "Inductance";
      parameter Real F021=3.55 "Initial flow";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(
          Line(points=[-1.77636e-015, -30; 0, -30], style(color=8)), 
          Line(points=[0, 10; 0, 10.2], style(color=8)), 
          Line(points=[0, 30; 0, 30], style(color=8))), 
        Icon(
          Polygon(points=[94, 0; 60, 40; 60, 14; -94, 14; -94, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=43)), 
          Text(extent=[-80, 44; 42, 18], string="Ascending"), 
          Text(extent=[-76, -18; 46, -44], string="Aortic Arch Flow")));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10], layer="icon");
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ], layer="icon");
      BondLib.Junctions.J1p4 J1p4_1 annotation (extent=[-10, -10; 10, 10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.Bond B4 annotation (extent=[-10, -30; 10, -10], rotation=
            270);
      BondLib.Passive.R R1(R=r21) annotation (extent=[-10, 30; 10, 50], 
          rotation=90);
      BondLib.Bonds.eBond B3 annotation (extent=[-10, 10; 10, 30], rotation=90
        );
      BondLib.Passive.I I1(I=L21, f(start=F021)) annotation (extent=[-10, -50
            ; 10, -30], rotation=-90);
    equation 
      connect(B1.fBondCon1, J1p4_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J1p4_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(J1p4_1.BondCon3, B4.BondCon1) annotation (points=[0, -10; 
            1.77636e-015, -10], style(color=8));
      connect(J1p4_1.BondCon4, B3.fBondCon1) annotation (points=[0, 10; -
            5.55112e-016, 10], style(color=8));
      connect(B3.eBondCon1, R1.BondCon1) annotation (points=[5.55112e-016, 30
            ; 0, 30], style(color=8));
      connect(B4.BondCon2, I1.BondCon1) annotation (points=[0, -30; 0, -30], 
          style(color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -94, 0], 
          style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
    end Ascending_Aortic_Arch_Flow;
  end Arteries;
  package Veins 
    annotation (
      Coordsys(
        extent=[0, 0; 402, 261], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Rectangle(extent=[-100, -100; 80, 50], style(fillColor=30, fillPattern
              =1)), 
        Polygon(points=[-100, 50; -80, 70; 100, 70; 80, 50; -100, 50], style(
              fillColor=30, fillPattern=1)), 
        Polygon(points=[100, 70; 100, -80; 80, -100; 80, 50; 100, 70], style(
              fillColor=30, fillPattern=1)), 
        Text(
          extent=[-85, 35; 65, -85], 
          string="Library", 
          style(color=3)), 
        Text(
          extent=[-120, 122; 120, 73], 
          string="%name", 
          style(color=1))), 
      Window(
        x=0, 
        y=0.6, 
        width=0.4, 
        height=0.4, 
        library=1, 
        autolayout=1));
    model Pulmonary_Veins 
      parameter Real CVCP=8.4 "Capacitance constant";
      parameter Real ALFA=20.0;
      parameter Real VUVCP=460.0 "Residual volume";
      parameter Real VVCP0=96.24 "Initial volume";
      Real COE7;
      Real D2 "Venous tone control value";
      Real RE7;
      Real VVCP "Volume";
      Real C "Capacitance";
      Real P0 "Residual pressure";
      
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(
          Line(points=[-30, 40; -30, 40], style(color=8)), 
          Text(extent=[36, -52; 58, -62], string="Thoracic"), 
          Text(extent=[34, -60; 60, -70], string="Pressure"), 
          Line(points=[0, 70; 0, 70], style(color=8)), 
          Text(extent=[-80, 70; -68, 60], string="D2"), 
          Text(extent=[-70, -68; -50, -80], string="VVCP")), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80], style(fillColor=43)), 
          Text(extent=[-60, 40; 62, 0], string="Pulmonary"), 
          Text(extent=[-60, 8; 62, -24], string="Veins"), 
          Line(points=[-80, 0; -94, 0]), 
          Line(points=[0, -80; 0, -94])));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[-10, -110; 10, -
            90]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[50, -100; 
            70, -80], rotation=90);
      BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[10, 10; -10, -10], 
          rotation=90);
      BondLib.Bonds.eBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.fBond B2 annotation (extent=[-10, -30; 10, -10], rotation=
            -90);
      BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[-10, -50; 10, -30], 
          rotation=-90);
      BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-50, -10; -30, 10]);
      BondLib.Bonds.eBond B3 annotation (extent=[-10, 10; 10, 30], rotation=90
        );
      BondLib.Junctions.J1p4 J1p4_1 annotation (extent=[-10, 30; 10, 50]);
      BondLib.Bonds.fBond B6 annotation (extent=[10, 30; 30, 50], rotation=180
        );
      BondLib.Bonds.fBond B5 annotation (extent=[-30, 30; -10, 50]);
      BondLib.Sources.mSE mSE2 annotation (extent=[30, 30; 50, 50]);
      BondLib.Bonds.eBond B4 annotation (extent=[-10, 50; 10, 70], rotation=90
        );
      Modelica.Blocks.Math.Gain Gain2(k={-1}) annotation (extent=[50, -40; 70
            , -20], rotation=90);
      Generic.modC C1(
        q0=VVCP0, 
        m=m, 
        b=b) annotation (extent=[-10, 70; 10, 90], rotation=90);
      BondLib.Sources.mSE mSE1 annotation (extent=[-30, 30; -50, 50]);
      Modelica.Blocks.Interfaces.InPort InPort2 annotation (extent=[-100, 50; 
            -80, 70]);
      Modelica.Blocks.Interfaces.OutPort OutPort1 annotation (extent=[-70, -
            100; -50, -80], rotation=-90);
    equation 
      connect(J0p2_1.BondCon2, B1.fBondCon1) annotation (points=[-30, 0; -
            30, 0], style(color=8));
      connect(B3.eBondCon1, J1p4_1.BondCon3) annotation (points=[0, 30; 0, 30]
          , style(color=8));
      connect(B6.fBondCon1, J1p4_1.BondCon2) annotation (points=[10, 40; 10, 
            40], style(color=8));
      connect(B5.fBondCon1, J1p4_1.BondCon1) annotation (points=[-10, 40; -10
            , 40], style(color=8));
      connect(mSE2.BondCon1, B6.eBondCon1) annotation (points=[30, 40; 30, 40]
          , style(color=8));
      connect(J1p4_1.BondCon4, B4.fBondCon1) annotation (points=[0, 50; 0, 50]
          , style(color=8));
      connect(Gain2.outPort, mSE2.InPort1) annotation (points=[60, -18; 60, 40
            ; 50, 40], style(color=3));
      connect(B4.eBondCon1, C1.BondCon1) annotation (points=[0, 70; 0, 70], 
          style(color=8));
      connect(InPort1, Gain2.inPort) annotation (points=[60, -80; 60, -44], 
          style(color=3));
      connect(mSE1.BondCon1, B5.eBondCon1) annotation (points=[-30, 40; -30, 
            40], style(color=8));
      connect(J0p2_1.BondCon1, BondCon1) annotation (points=[-50, 0; -94, 0], 
          style(color=8));
      connect(B1.eBondCon1, J0p3_1.BondCon3) annotation (points=[-10, 0; -10, 
            6.10623e-016], style(color=8));
      connect(J0p3_1.BondCon1, B3.fBondCon1) annotation (points=[0, 10; 0, 10]
          , style(color=8));
      connect(J0p3_1.BondCon2, B2.eBondCon1) annotation (points=[0, -10; 0, -
            10], style(color=8));
      connect(B2.fBondCon1, J0p2_2.BondCon1) annotation (points=[0, -30; 0, -
            30], style(color=8));
      connect(J0p2_2.BondCon2, BondCon2) annotation (points=[0, -50; 0, -94], 
          style(color=8));
      D2 = InPort2.signal[1];
      VVCP = C1.q;
      COE7 = if VVCP > VUVCP then 1.0 else ALFA;
      RE7 = D2/CVCP;
      C = COE7/RE7;
      P0 = VUVCP/C;
      C1.s = C;
      mSE1.s = P0;
      OutPort1.signal[1] = VVCP;
    end Pulmonary_Veins;
    model Pulmonary_Venal_Flow 
      parameter Real VUVCP=460.0 "Residual pulmonary vein volume";
      parameter Real r11=0.007 "Resistance constant";
      Real VVCP "Pulmonary vein volume";
      Real k "Resistance coefficient";
      Real PVCP "Pulmonary vein pressure";
      Real PAI "Left atrial pressure";
      Real R "Resistance";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Icon(
          Polygon(points=[94, 0; 60, 40; 60, 14; -94, 14; -94, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=43)), 
          Text(extent=[-80, 44; 42, 18], string="Pulmonary"), 
          Text(extent=[-74, -18; 48, -44], string="Venal Flow")), 
        Diagram(Text(extent=[-80, -44; -56, -58], string="VVCP")));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10], layer="icon");
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ], layer="icon");
      BondLib.Junctions.J1p3 J1p3_1 annotation (extent=[-10, 10; 10, -10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.fBond B3 annotation (extent=[-10, 10; 10, 30], rotation=90
        );
      BondLib.Passive.mR mR1 annotation (extent=[-10, 30; 10, 50], rotation=90
        );
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[-100, -70
            ; -80, -50]);
    equation 
      connect(B1.fBondCon1, J1p3_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J1p3_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -92, 0], 
          style(color=8));
      connect(J1p3_1.BondCon3, B3.eBondCon1) annotation (points=[0, 10; 0, 10]
          , style(color=8));
      connect(B3.fBondCon1, mR1.BondCon1) annotation (points=[0, 30; 0, 30], 
          style(color=8));
      PVCP = BondCon1.e;
      PAI = BondCon2.e;
      VVCP = InPort1.signal[1];
      k = if PAI > PVCP then -3.0 else 1.0;
      R = (VUVCP/VVCP)^2*(r11/k);
      mR1.s = R;
    end Pulmonary_Venal_Flow;
    model Superior_Vena_Cava 
      parameter Real CVCS=8.3 "Capacitance constant";
      parameter Real ALFA=20.0;
      parameter Real VUVCS=488.0 "Residual volume";
      parameter Real VVCS0=520.62 "Initial volume";
      Real COE2;
      Real D2 "Venous tone control value";
      Real RE2;
      Real VVCS "Volume";
      Real C "Capacitance";
      Real P0 "Residual pressure";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(
          Line(points=[-30, 40; -30, 40], style(color=8)), 
          Text(extent=[34, -52; 56, -62], string="Thoracic"), 
          Text(extent=[32, -58; 58, -68], string="Pressure"), 
          Line(points=[0, 70; 0, 70], style(color=8)), 
          Line(points=[-30, 40; -30, 40], style(color=8)), 
          Text(extent=[-80, 70; -68, 60], string="D2"), 
          Text(extent=[-70, -68; -50, -80], string="VVCS")), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80], style(fillColor=71)), 
          Line(points=[0, -80; 0, -94]), 
          Line(points=[0, 80; 0, 94]), 
          Text(extent=[-60, 40; 62, 0], string="Superior"), 
          Text(extent=[-60, 8; 62, -24], string="Vena Cava")));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-10, 90; 10, 110
            ]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[-10, -110; 10, -
            90]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[50, -100; 
            70, -80], rotation=90);
      BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[10, -10; -10, -30], 
          rotation=90);
      BondLib.Bonds.eBond B1 annotation (extent=[-30, -30; -10, -10]);
      BondLib.Bonds.fBond B2 annotation (extent=[-10, -50; 10, -30], rotation=
            -90);
      BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[-10, -70; 10, -50], 
          rotation=-90);
      BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-50, -30; -30, -10]);
      BondLib.Bonds.eBond B3 annotation (extent=[-10, -10; 10, 10], rotation=
            90);
      BondLib.Junctions.J1p4 J1p4_1 annotation (extent=[-10, 10; 10, 30]);
      BondLib.Bonds.fBond B6 annotation (extent=[10, 10; 30, 30], rotation=180
        );
      BondLib.Bonds.fBond B5 annotation (extent=[-30, 10; -10, 30]);
      BondLib.Sources.mSE mSE2 annotation (extent=[30, 10; 50, 30]);
      BondLib.Bonds.eBond B4 annotation (extent=[-10, 30; 10, 50], rotation=90
        );
      Modelica.Blocks.Math.Gain Gain2(k={-1}) annotation (extent=[50, -40; 70
            , -20], rotation=90);
      Generic.modC C1(
        q0=VVCS0, 
        m=m, 
        b=b) annotation (extent=[-10, 50; 10, 70], rotation=90);
      BondLib.Sources.mSE mSE1 annotation (extent=[-30, 10; -50, 30]);
      Modelica.Blocks.Interfaces.InPort InPort2 annotation (extent=[-100, 50; 
            -80, 70]);
      Modelica.Blocks.Interfaces.OutPort OutPort1 annotation (extent=[-70, -
            100; -50, -80], rotation=-90);
    equation 
      connect(J0p2_1.BondCon2, B1.fBondCon1) annotation (points=[-30, -20
            ; -30, -20], style(color=8));
      connect(B3.eBondCon1, J1p4_1.BondCon3) annotation (points=[0, 10; 0, 10]
          , style(color=8));
      connect(B6.fBondCon1, J1p4_1.BondCon2) annotation (points=[10, 20; 10, 
            20], style(color=8));
      connect(B5.fBondCon1, J1p4_1.BondCon1) annotation (points=[-10, 20; -10
            , 20], style(color=8));
      connect(mSE2.BondCon1, B6.eBondCon1) annotation (points=[30, 20; 30, 20]
          , style(color=8));
      connect(J1p4_1.BondCon4, B4.fBondCon1) annotation (points=[0, 30; 0, 30]
          , style(color=8));
      connect(Gain2.outPort, mSE2.InPort1) annotation (points=[60, -19; 60, 20
            ; 50, 20], style(color=3));
      connect(B4.eBondCon1, C1.BondCon1) annotation (points=[0, 50; 0, 50], 
          style(color=8));
      connect(InPort1, Gain2.inPort) annotation (points=[60, -80; 60, -42], 
          style(color=3));
      connect(mSE1.BondCon1, B5.eBondCon1) annotation (points=[-30, 20; -30, 
            20], style(color=8));
      connect(J0p2_1.BondCon1, BondCon1) annotation (points=[-50, -20; -60, -
            20; -60, 84; 0, 84; 0, 94], style(color=8));
      connect(B1.eBondCon1, J0p3_1.BondCon3) annotation (points=[-10, -20; -10
            , -20], style(color=8));
      connect(J0p3_1.BondCon1, B3.fBondCon1) annotation (points=[0, -10; 0, -
            10], style(color=8));
      connect(J0p3_1.BondCon2, B2.eBondCon1) annotation (points=[0, -30; 0, -
            30], style(color=8));
      connect(B2.fBondCon1, J0p2_2.BondCon1) annotation (points=[0, -50; 0, -
            50], style(color=8));
      connect(J0p2_2.BondCon2, BondCon2) annotation (points=[0, -70; 0, -94], 
          style(color=8));
      D2 = InPort2.signal[1];
      VVCS = C1.q;
      COE2 = if VVCS > VUVCS then 1.0 else ALFA;
      RE2 = D2/CVCS;
      C = COE2/RE2;
      P0 = VUVCS/C;
      C1.s = C;
      mSE1.s = P0;
      OutPort1.signal[1] = VVCS;
    end Superior_Vena_Cava;
    model Inferior_Vena_Cava 
      parameter Real CVCI=8.3 "Capacitance constant";
      parameter Real ALFA=20.0;
      parameter Real VUVCI=488.0 "Residual volume";
      parameter Real VVCI0=504.4 "Initial volume";
      Real COE6;
      Real VVCI "Volume";
      Real C "Capacitance";
      Real P0 "Residual pressure";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(
          Line(points=[-30, 40; -30, 40], style(color=8)), 
          Text(extent=[34, -52; 56, -62], string="Thoracic"), 
          Text(extent=[32, -58; 58, -68], string="Pressure"), 
          Line(points=[0, 70; 0, 70], style(color=8)), 
          Line(points=[-30, 40; -30, 40], style(color=8)), 
          Text(extent=[50, 80; 70, 68], string="VVCI")), 
        Icon(
          Rectangle(extent=[-80, 80; 80, -80], style(fillColor=71)), 
          Line(points=[0, 80; 0, 94]), 
          Line(points=[0, -80; 0, -94]), 
          Line(points=[-60, -80; -60, -94]), 
          Text(extent=[-60, 40; 62, 0], string="Inferior"), 
          Text(extent=[-60, 8; 62, -24], string="Vena Cava")));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-10, 90; 10, 110
            ]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[-10, -110; 10, -
            90]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[50, -100; 
            70, -80], rotation=90);
      BondLib.Bonds.eBond B1 annotation (extent=[-30, -30; -10, -10]);
      BondLib.Bonds.fBond B2 annotation (extent=[10, -30; 30, -10]);
      BondLib.Junctions.J0p2 J0p2_2 annotation (extent=[-10, -70; 10, -50], 
          rotation=-90);
      BondLib.Junctions.J0p2 J0p2_1 annotation (extent=[-50, -30; -30, -10]);
      BondLib.Bonds.eBond B3 annotation (extent=[-10, -10; 10, 10], rotation=
            90);
      BondLib.Junctions.J1p4 J1p4_1 annotation (extent=[-10, 10; 10, 30]);
      BondLib.Bonds.fBond B6 annotation (extent=[10, 10; 30, 30], rotation=180
        );
      BondLib.Bonds.fBond B5 annotation (extent=[-30, 10; -10, 30]);
      BondLib.Sources.mSE mSE2 annotation (extent=[30, 10; 50, 30]);
      BondLib.Bonds.eBond B4 annotation (extent=[-10, 30; 10, 50], rotation=90
        );
      Modelica.Blocks.Math.Gain Gain2(k={-1}) annotation (extent=[50, -50; 70
            , -30], rotation=90);
      Generic.modC C1(
        q0=VVCI0, 
        m=m, 
        b=b) annotation (extent=[-10, 50; 10, 70], rotation=90);
      BondLib.Sources.mSE mSE1 annotation (extent=[-30, 10; -50, 30]);
      Modelica.Blocks.Interfaces.OutPort OutPort1 annotation (extent=[50, 100
            ; 70, 80], rotation=-90);
      BondLib.Junctions.J0p4 J0p4_1 annotation (extent=[-10, -30; 10, -10]);
      BondLib.Junctions.J0p2 J0p2_3 annotation (extent=[30, -30; 50, -10]);
      BondLib.Bonds.eBond B7 annotation (extent=[-10, -50; 10, -30], rotation=
            90);
      BondLib.Interfaces.BondCon BondCon3 annotation (extent=[-70, -110; -50, 
            -90]);
    equation 
      connect(J0p2_1.BondCon2, B1.fBondCon1) annotation (points=[-30, -20
            ; -30, -20], style(color=8));
      connect(B3.eBondCon1, J1p4_1.BondCon3) annotation (points=[0, 10; 0, 10]
          , style(color=8));
      connect(B6.fBondCon1, J1p4_1.BondCon2) annotation (points=[10, 20; 10, 
            20], style(color=8));
      connect(B5.fBondCon1, J1p4_1.BondCon1) annotation (points=[-10, 20; -10
            , 20], style(color=8));
      connect(mSE2.BondCon1, B6.eBondCon1) annotation (points=[30, 20; 30, 20]
          , style(color=8));
      connect(J1p4_1.BondCon4, B4.fBondCon1) annotation (points=[0, 30; 0, 30]
          , style(color=8));
      connect(Gain2.outPort, mSE2.InPort1) annotation (points=[60, -29; 60, 20
            ; 50, 20], style(color=3));
      connect(B4.eBondCon1, C1.BondCon1) annotation (points=[0, 50; 0, 50], 
          style(color=8));
      connect(InPort1, Gain2.inPort) annotation (points=[60, -80; 60, -52], 
          style(color=3));
      connect(mSE1.BondCon1, B5.eBondCon1) annotation (points=[-30, 20; -30, 
            20], style(color=8));
      connect(J0p2_2.BondCon2, BondCon2) annotation (points=[0, -70; 0, -94], 
          style(color=8));
      connect(B1.eBondCon1, J0p4_1.BondCon1) annotation (points=[-10, -20; -10
            , -20], style(color=8));
      connect(J0p4_1.BondCon4, B3.fBondCon1) annotation (points=[0, -10; 0, -
            10], style(color=8));
      connect(J0p4_1.BondCon2, B2.eBondCon1) annotation (points=[10, -20; 10, 
            -20], style(color=8));
      connect(B2.fBondCon1, J0p2_3.BondCon1) annotation (points=[30, -20; 30, 
            -20], style(color=8));
      connect(J0p2_3.BondCon2, BondCon1) annotation (points=[50, -20; 70, -20
            ; 70, 50; 20, 50; 20, 84; 0, 84; 0, 94], style(color=8));
      connect(J0p2_2.BondCon1, B7.fBondCon1) annotation (points=[0, -50; -
            5.55112e-016, -50], style(color=8));
      connect(B7.eBondCon1, J0p4_1.BondCon3) annotation (points=[5.55112e-016
            , -30; 0, -30], style(color=8));
      connect(J0p2_1.BondCon1, BondCon3) annotation (points=[-50, -20; -60, -
            20; -60, -94], style(color=8));
      VVCI = C1.q;
      COE2 = if VVCI > VUVCI then 1.0 else ALFA;
      C = COE6*CVCI;
      P0 = VUVCI/C;
      C1.s = C;
      mSE1.s = P0;
      OutPort1.signal[1] = VVCI;
    end Inferior_Vena_Cava;
    model Sup_Cava_Venal_Flow 
      parameter Real VUVCS=488.0 "Residual superior cava volume";
      parameter Real r24=0.06 "Resistance constant";
      Real VVCS "Superior cava volume";
      Real k "Resistance coefficient";
      Real PVCS "Superior cava pressure";
      Real PAD "Right atrial pressure";
      Real R "Resistance";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(Text(extent=[-80, -44; -56, -58], string="VVCS")), 
        Icon(
          Polygon(points=[94, 0; 60, 40; 60, 14; -94, 14; -94, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=71)), 
          Text(extent=[-80, 44; 42, 18], string="Superior Cava"), 
          Text(extent=[-74, -18; 48, -44], string="Venal Flow")));
      BondLib.Junctions.J1p3 J1p3_1 annotation (extent=[-10, 10; 10, -10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.fBond B3 annotation (extent=[-10, 10; 10, 30], rotation=90
        );
      BondLib.Passive.mR mR1 annotation (extent=[-10, 30; 10, 50], rotation=90
        );
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[-100, -70
            ; -80, -50]);
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ]);
    equation 
      connect(B1.fBondCon1, J1p3_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J1p3_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -92, 0], 
          style(color=8));
      connect(J1p3_1.BondCon3, B3.eBondCon1) annotation (points=[0, 10; 0, 10]
          , style(color=8));
      connect(B3.fBondCon1, mR1.BondCon1) annotation (points=[0, 30; 0, 30], 
          style(color=8));
      PVCS = BondCon1.e;
      PAD = BondCon2.e;
      VVCS = InPort1.signal[1];
      k = if PAD > PVCS then 0.1 else 1.0;
      R = (VUVCS/VVCS)^2*(r24/k);
      mR1.s = R;
    end Sup_Cava_Venal_Flow;
    model Inf_Cava_Venal_Flow 
      parameter Real VUVCI=488.0 "Residual superior cava volume";
      parameter Real r7=0.015 "Resistance constant";
      Real VVCI "Superior cava volume";
      Real k "Resistance coefficient";
      Real PVCI "Superior cava pressure";
      Real PAD "Right atrial pressure";
      Real R "Resistance";
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(Text(extent=[-80, -44; -56, -58], string="VVCI")), 
        Icon(
          Polygon(points=[94, 0; 60, 40; 60, 14; -94, 14; -94, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=71)), 
          Text(extent=[-80, 44; 42, 18], string="Inferior Cava"), 
          Text(extent=[-74, -18; 48, -44], string="Venal Flow")));
      BondLib.Junctions.J1p3 J1p3_1 annotation (extent=[-10, 10; 10, -10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.fBond B3 annotation (extent=[-10, 10; 10, 30], rotation=90
        );
      BondLib.Passive.mR mR1 annotation (extent=[-10, 30; 10, 50], rotation=90
        );
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[-100, -70
            ; -80, -50]);
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ]);
    equation 
      connect(B1.fBondCon1, J1p3_1.BondCon1) annotation (points=[-10, 0; -
            10, 0], style(color=8));
      connect(J1p3_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10, 0]
          , style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -92, 0], 
          style(color=8));
      connect(J1p3_1.BondCon3, B3.eBondCon1) annotation (points=[0, 10; 0, 10]
          , style(color=8));
      connect(B3.fBondCon1, mR1.BondCon1) annotation (points=[0, 30; 0, 30], 
          style(color=8));
      PVCI = BondCon1.e;
      PAD = BondCon2.e;
      VVCI = InPort1.signal[1];
      k = if PAD > PVCI then 0.1 else 1.0;
      R = (VUVCI/VVCI)^2*(r7/k);
      mR1.s = R;
    end Inf_Cava_Venal_Flow;
  end Veins;
  package Organs 
    annotation (
      Coordsys(
        extent=[0, 0; 402, 261], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Rectangle(extent=[-100, -100; 80, 50], style(fillColor=30, fillPattern
              =1)), 
        Polygon(points=[-100, 50; -80, 70; 100, 70; 80, 50; -100, 50], style(
              fillColor=30, fillPattern=1)), 
        Polygon(points=[100, 70; 100, -80; 80, -100; 80, 50; 100, 70], style(
              fillColor=30, fillPattern=1)), 
        Text(
          extent=[-85, 35; 65, -85], 
          string="Library", 
          style(color=3)), 
        Text(
          extent=[-120, 122; 120, 73], 
          string="%name", 
          style(color=1))), 
      Window(
        x=0, 
        y=0.6, 
        width=0.4, 
        height=0.4, 
        library=1, 
        autolayout=1));
    model Heart 
      annotation (
        Coordsys(
          extent=[-120, -160; 140, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(
          Ellipse(extent=[-66, -96; -62, -100], style(fillColor=73)), 
          Ellipse(extent=[22, -66; 26, -70], style(fillColor=73)), 
          Ellipse(extent=[-24, -88; -20, -92], style(fillColor=73)), 
          Ellipse(extent=[16, -88; 20, -92], style(fillColor=73)), 
          Ellipse(extent=[-42, -88; -38, -92], style(fillColor=73)), 
          Text(extent=[-94, -108; -78, -120], string="B2"), 
          Text(extent=[-38, -120; -22, -132], string="pTh"), 
          Text(extent=[4, -130; 20, -142], string="Q6"), 
          Ellipse(extent=[98, -66; 102, -70], style(fillColor=74)), 
          Ellipse(extent=[10, -66; 14, -70], style(fillColor=73)), 
          Text(extent=[118, -102; 134, -114], string="TH"), 
          Text(extent=[74, -136; 98, -148], string="tlast")), 
        Icon(
          Polygon(points=[10, 80; -100, 80; -100, -40; -50, -140; 10, -140; 10
                , 80], style(fillColor=71)), 
          Text(extent=[-92, 36; 6, 14], string="Atrium"), 
          Text(extent=[-94, 54; 4, 32], string="Right"), 
          Polygon(points=[10, 80; 120, 80; 120, -40; 70, -140; 10, -140; 10, 
                80], style(fillColor=43)), 
          Line(points=[-100, 0; 120, 0]), 
          Text(extent=[16, 38; 114, 16], string="Atrium"), 
          Text(extent=[16, 54; 114, 34], string="Left"), 
          Text(extent=[-88, -38; 10, -68], string="Ventriculum"), 
          Text(extent=[-90, -26; 8, -46], string="Right"), 
          Text(extent=[12, -38; 110, -68], string="Ventriculum"), 
          Text(extent=[12, -26; 110, -46], string="Left"), 
          Polygon(points=[-60, 94; -52, 88; -56, 88; -56, 80; -64, 80; -64, 88
                ; -68, 88; -60, 94], style(fillColor=71)), 
          Polygon(points=[-80, -90; -72, -94; -76, -94; -76, -154; -84, -154; 
                -84, -94; -88, -94; -80, -90], style(fillColor=71)), 
          Polygon(points=[80, 80; 88, 84; 84, 84; 84, 94; 76, 94; 76, 84; 72, 
                84; 80, 80], style(fillColor=43)), 
          Polygon(points=[-100, 60; -106, 52; -106, 56; -114, 56; -114, 64; 
                -106, 64; -106, 68; -100, 62; -100, 60], style(fillColor=71)), 
          Polygon(points=[-100, 20; -106, 12; -106, 16; -114, 16; -114, 24; 
                -106, 24; -106, 28; -100, 22; -100, 20], style(fillColor=71)), 
          Polygon(points=[134, -32; 128, -40; 128, -36; 120, -36; 120, -28; 
                128, -28; 128, -24; 134, -30; 134, -32], style(fillColor=43)), 
          Polygon(points=[96, -90; 102, -82; 102, -86; 134, -86; 134, -96; 102
                , -96; 102, -100; 96, -92; 96, -90], style(fillColor=43))), 
        Window(
          x=0.27, 
          y=0.01, 
          width=0.62, 
          height=0.94));
      HeartLib.Right_Atrium Right_Atrium1 annotation (extent=[-88, 20; -28, 80
            ]);
      HeartLib.Right_Ventriculum Right_Ventriculum1 annotation (extent=[-52, 
            -80; 8, -20]);
      HeartLib.Tricuspid_Valve Tricuspid_Valve1 annotation (extent=[-60, -20; 
            -20, 20], rotation=-90);
      HeartLib.Left_Atrium Left_Atrium1 annotation (extent=[30, 20; 90, 80]);
      HeartLib.Left_Ventriculum Left_Ventriculum1 annotation (extent=[30, -80
            ; 90, -20]);
      HeartLib.Mitral_Valve Mitral_Valve1 annotation (extent=[40, -20; 80, 20]
          , rotation=-90);
      HeartLib.Aortic_Valve Aortic_Valve1 annotation (extent=[90, -70; 130, 
            -30]);
      HeartLib.Pulmonar_Valve Pulmonar_Valve1 annotation (extent=[-20, 40; 20
            , 80], rotation=90);
      HeartLib.Heart_Muscle Heart_Muscle1 annotation (extent=[40, -130; 0, -90
            ]);
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-130, 50; -110, 
            70]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[-130, 10; -110, 
            30]);
      BondLib.Interfaces.BondCon BondCon3 annotation (extent=[-90, -170; -70, 
            -150]);
      BondLib.Interfaces.BondCon BondCon4 annotation (extent=[130, -100; 150, 
            -80]);
      BondLib.Interfaces.BondCon BondCon5 annotation (extent=[130, -40; 150, 
            -20]);
      BondLib.Interfaces.BondCon BondCon6 annotation (extent=[-70, 90; -50, 
            110]);
      BondLib.Interfaces.BondCon BondCon7 annotation (extent=[70, 90; 90, 110]
        );
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[-120, -130
            ; -100, -110]);
      Modelica.Blocks.Interfaces.InPort InPort2 annotation (extent=[-50, -160
            ; -30, -140], rotation=90);
      Modelica.Blocks.Interfaces.InPort InPort3 annotation (extent=[10, -160; 
            30, -140], rotation=90);
      Modelica.Blocks.Interfaces.InPort InPort4 annotation (extent=[140, -130
            ; 120, -110]);
      Modelica.Blocks.Interfaces.InPort InPort5 annotation (extent=[110, -140
            ; 90, -160], rotation=-90);
      HeartLib.Sinus_Rhythm Sinus_Rhythm1 annotation (extent=[120, -140; 80, 
            -100]);
    equation 
      connect(Right_Atrium1.BondCon3, Tricuspid_Valve1.BondCon1) 
        annotation (points=[-42, 22; -42, 21; -40, 21; -40, 20], style(color=8)
        );
      connect(Tricuspid_Valve1.BondCon2, Right_Ventriculum1.BondCon2) 
        annotation (points=[-40, -20; -42, -20], style(color=8));
      connect(Left_Atrium1.BondCon2, Mitral_Valve1.BondCon1) annotation (
          points=[62, 20; 60, 20], style(color=8));
      connect(Mitral_Valve1.BondCon2, Left_Ventriculum1.BondCon1) annotation (
          points=[60, -20; 60, -20], style(color=8));
      connect(Left_Ventriculum1.BondCon2, Aortic_Valve1.BondCon1) annotation (
          points=[92, -50; 90, -50], style(color=8));
      connect(Right_Ventriculum1.BondCon1, Pulmonar_Valve1.BondCon1) 
        annotation (points=[-22, -18; -22, 0; 0, 0; 0, 38], style(color=8));
      connect(Pulmonar_Valve1.BondCon2, BondCon6) annotation (points=[0, 80; 0
            , 88; -60, 88; -60, 94], style(color=8));
      connect(Left_Atrium1.BondCon1, BondCon7) annotation (points=[60, 82; 60
            , 88; 80, 88; 80, 94], style(color=8));
      connect(Right_Atrium1.BondCon1, BondCon1) annotation (points=[-88, 62; 
            -106, 62; -106, 60; -114, 60], style(color=8));
      connect(Right_Atrium1.BondCon2, BondCon2) annotation (points=[-88, 42; 
            -106, 42; -106, 20; -114, 20], style(color=8));
      connect(Aortic_Valve1.BondCon2, BondCon5) annotation (points=[130, -50; 
            134, -50; 134, -30], style(color=8));
      connect(Right_Atrium1.BondCon5, BondCon3) annotation (points=[-68, 18; 
            -68, -68; -80, -68; -80, -154], style(color=8));
      connect(Heart_Muscle1.BondCon2, Right_Atrium1.BondCon4) annotation (
          points=[0, -110; -58, -110; -58, 20], style(color=8));
      connect(Right_Ventriculum1.InPort2, InPort1) annotation (points=[-52, 
            -68; -64, -68; -64, -120; -110, -120], style(color=3));
      connect(Left_Ventriculum1.InPort2, InPort1) annotation (points=[30, -68
            ; 24, -68; 24, -98; -64, -98; -64, -120; -100, -120], style(color=3
          ));
      connect(Left_Atrium1.InPort2, Left_Ventriculum1.InPort2) annotation (
          points=[30, 32; 24, 32; 24, -68; 30, -68], style(color=3));
      connect(Right_Atrium1.InPort2, Right_Ventriculum1.InPort2) annotation (
          points=[-88, 32; -96, 32; -96, -98; -64, -98; -64, -68; -52, -68], 
          style(color=3));
      connect(Heart_Muscle1.BondCon1, BondCon4) annotation (points=[42, -110; 
            54, -110; 54, -94; 100, -94; 100, -90; 134, -90], style(color=8));
      connect(Left_Ventriculum1.InPort1, Right_Ventriculum1.InPort1) 
        annotation (points=[60, -80; 60, -90; -22, -90; -22, -78], style(color=
              3));
      connect(InPort2, Right_Ventriculum1.InPort1) annotation (points=[-40, 
            -140; -40, -90; -22, -90; -22, -80], style(color=3));
      connect(Right_Atrium1.InPort1, InPort2) annotation (points=[-76, 20; -76
            , -90; -40, -90; -40, -142], style(color=3));
      connect(Left_Atrium1.InPort1, Left_Ventriculum1.InPort1) annotation (
          points=[42, 20; 42, 0; 18, 0; 18, -90; 60, -90; 60, -80], style(color
            =3));
      connect(Heart_Muscle1.InPort1, InPort3) annotation (points=[20, -130; 20
            , -156], style(color=3));
      connect(Sinus_Rhythm1.InPort1, InPort4) annotation (points=[122, -120; 
            120, -120], style(color=3));
      connect(InPort5, Sinus_Rhythm1.InPort2) annotation (points=[100, -140; 
            100, -140], style(color=3));
      connect(Sinus_Rhythm1.OutPort1, Left_Ventriculum1.InPort3) annotation (
          points=[76, -112; 70, -112; 70, -84; 100, -84; 100, -68; 90, -68], 
          style(color=3));
      connect(Left_Atrium1.InPort3, Left_Ventriculum1.InPort3) annotation (
          points=[90, 32; 100, 32; 100, -68; 90, -68], style(color=3));
      connect(Sinus_Rhythm1.OutPort2, Right_Ventriculum1.InPort3) annotation (
          points=[76, -128; 48, -128; 48, -80; 12, -80; 12, -68; 8, -68], style
          (color=3));
      connect(Right_Atrium1.InPort3, Right_Ventriculum1.InPort3) annotation (
          points=[-28, 32; 12, 32; 12, -68; 8, -68], style(color=3));
    end Heart;
    model Lungs 
      parameter Real r10=0.11 "Pulmonary resistance";
      Real FAC7;
      Real FS;
      Real PVCP "Pressure of pulmonary veins";
      
      annotation (
        Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), 
        Diagram(Text(extent=[-14, -64; -2, -74], string="Q4"), Text(extent=[42
                , -40; 58, -50], string="FAC7")), 
        Icon(
          Rectangle(extent=[-12, 14; -96, -14], style(fillColor=71)), 
          Polygon(points=[94, 0; 60, 40; 60, 14; -12, 14; -12, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=43)), 
          Text(extent=[-86, 44; 52, 18], string="Lungs")));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[10, -100; 
            -10, -80], rotation=90);
      BondLib.Junctions.J1p3 J1p3_1 annotation (extent=[-10, 10; 10, -10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.fBond B3 annotation (extent=[-10, 10; 10, 30], rotation=90
        );
      BondLib.Passive.mR mR1 annotation (extent=[30, 30; 50, 50]);
      BondLib.Junctions.J0p3 J0p3_1 annotation (extent=[-10, 30; 10, 50]);
      BondLib.Bonds.fBond B4 annotation (extent=[10, 30; 30, 50]);
      Modelica.Blocks.Math.Gain Gain1(k={r10}) annotation (extent=[20, -60; 40
            , -40]);
      BondLib.Bonds.eBond B5 annotation (extent=[-30, 30; -10, 50]);
      BondLib.Sources.mSF mSF1 annotation (extent=[-30, 30; -50, 50]);
    equation 
      connect(J1p3_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10
            , 0], style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
      connect(B1.fBondCon1, J1p3_1.BondCon1) annotation (points=[-10, 0; -10, 
            0], style(color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -94, 0], 
          style(color=8));
      connect(J1p3_1.BondCon3, B3.eBondCon1) annotation (points=[0, 10; 0, 10]
          , style(color=8));
      connect(B3.fBondCon1, J0p3_1.BondCon3) annotation (points=[0, 30; 0, 30]
          , style(color=8));
      connect(J0p3_1.BondCon2, B4.eBondCon1) annotation (points=[10, 40; 10, 
            40], style(color=8));
      connect(B4.fBondCon1, mR1.BondCon1) annotation (points=[30, 40; 30, 40]
          , style(color=8));
      connect(Gain1.inPort, InPort1) annotation (points=[16, -50; 0, -50; 0, -
            80], style(color=3));
      connect(Gain1.outPort, mR1.InPort1) annotation (points=[42, -50; 60, -50
            ; 60, 40; 50, 40], style(color=3));
      connect(B5.eBondCon1, J0p3_1.BondCon1) annotation (points=[-10, 40; -10
            , 40], style(color=8));
      connect(mSF1.BondCon1, B5.fBondCon1) annotation (points=[-30, 40; -30, 
            40], style(color=8));
      PVCP = BondCon2.e;
      FAC7 = mR1.s;
      FS = if PVCP < 7 then (7 - PVCP)/FAC7 else 0;
      mSF1.s = FS;
    end Lungs;
    model Bronchi 
      annotation (Coordsys(
          extent=[-100, -100; 100, 100], 
          grid=[2, 2], 
          component=[20, 20]), Icon(
          Rectangle(extent=[-12, 14; -96, -14], style(fillColor=43)), 
          Polygon(points=[94, 0; 60, 40; 60, 14; -12, 14; -12, -14; 60, -14; 
                60, -40; 94, -2; 94, 0], style(fillColor=71)), 
          Text(extent=[-86, 44; 52, 18], string="Bronchi")));
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-110, -10; -90, 
            10], layer="icon");
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[90, -10; 110, 10
            ], layer="icon");
      BondLib.Junctions.J1p3 J1p3_1 annotation (extent=[-10, 10; 10, -10]);
      BondLib.Bonds.eBond B2 annotation (extent=[10, -10; 30, 10]);
      BondLib.Bonds.fBond B1 annotation (extent=[-30, -10; -10, 10]);
      BondLib.Bonds.eBond B3 annotation (extent=[-10, 10; 10, 30], rotation=-
            90);
      BondLib.Sources.SF SF1(f0=0) annotation (extent=[-10, 30; 10, 50], 
          rotation=90);
    equation 
      connect(J1p3_1.BondCon2, B2.fBondCon1) annotation (points=[10, 0; 10
            , 0], style(color=8));
      connect(B1.fBondCon1, J1p3_1.BondCon1) annotation (points=[-10, 0; -10, 
            0], style(color=8));
      connect(B3.eBondCon1, J1p3_1.BondCon3) annotation (points=[0, 10; 0, 10]
          , style(color=8));
      connect(B1.eBondCon1, BondCon1) annotation (points=[-30, 0; -94, 0], 
          style(color=8));
      connect(B2.eBondCon1, BondCon2) annotation (points=[30, 0; 94, 0], style
          (color=8));
      connect(SF1.BondCon1, B3.fBondCon1) annotation (points=[0, 30; 0, 30], 
          style(color=8));
    end Bronchi;
  end Organs;
  package Body_Parts 
    annotation (
      Coordsys(
        extent=[0, 0; 402, 261], 
        grid=[2, 2], 
        component=[20, 20]), 
      Icon(
        Rectangle(extent=[-100, -100; 80, 50], style(fillColor=30, fillPattern
              =1)), 
        Polygon(points=[-100, 50; -80, 70; 100, 70; 80, 50; -100, 50], style(
              fillColor=30, fillPattern=1)), 
        Polygon(points=[100, 70; 100, -80; 80, -100; 80, 50; 100, 70], style(
              fillColor=30, fillPattern=1)), 
        Text(
          extent=[-85, 35; 65, -85], 
          string="Library", 
          style(color=3)), 
        Text(
          extent=[-120, 122; 120, 73], 
          string="%name", 
          style(color=1))), 
      Window(
        x=0, 
        y=0.6, 
        width=0.4, 
        height=0.4, 
        library=1, 
        autolayout=1));
    model Thorax 
      annotation (Coordsys(
          extent=[-100, -140; 180, 140], 
          grid=[2, 2], 
          component=[20, 20]), Diagram(
          Ellipse(extent=[144, -110; 148, -114], style(fillColor=73)), 
          Ellipse(extent=[70, -110; 74, -114], style(fillColor=73)), 
          Ellipse(extent=[64, -78; 68, -82], style(fillColor=73)), 
          Ellipse(extent=[-24, -78; -20, -82], style(fillColor=73)), 
          Ellipse(extent=[158, -18; 162, -22], style(fillColor=73)), 
          Ellipse(extent=[80, 76; 84, 72], style(fillColor=73)), 
          Ellipse(extent=[0, 76; 4, 72], style(fillColor=73)), 
          Ellipse(extent=[-92, 114; -88, 110], style(fillColor=73)), 
          Text(extent=[-96, 98; -82, 86], string="D2"), 
          Text(extent=[80, -98; 98, -110], string="pTh"), 
          Text(extent=[-96, 58; -82, 46], string="B2"), 
          Text(extent=[-98, -22; -84, -34], string="Q6"), 
          Text(extent=[-98, -62; -84, -74], string="TH"), 
          Text(extent=[-98, 18; -84, 6], string="Q4"), 
          Text(extent=[-98, -102; -76, -114], string="tlast")));
      Organs.Heart Heart1 annotation (extent=[-40, -20; 20, 40]);
      Veins.Sup_Cava_Venal_Flow Sup_Cava_Venal_Flow1 annotation (extent=[-80, 
            40; -40, 80], rotation=-90);
      Veins.Inf_Cava_Venal_Flow Inf_Cava_Venal_Flow1 annotation (extent=[-80, 
            -22; -40, 18], rotation=90);
      Veins.Superior_Vena_Cava Superior_Vena_Cava1 annotation (extent=[-80, 80
            ; -40, 120]);
      Veins.Inferior_Vena_Cava Inferior_Vena_Cava1 annotation (extent=[-80, -
            70; -40, -30]);
      Organs.Lungs Lungs1 annotation (extent=[10, 80; 50, 120]);
      Organs.Bronchi Bronchi1 annotation (extent=[114, -90; 74, -50]);
      Veins.Pulmonary_Venal_Flow Pulmonary_Venal_Flow1 annotation (extent=[50
            , 32; 90, 72], rotation=-90);
      Veins.Pulmonary_Veins Pulmonary_Veins1 annotation (extent=[50, 80; 90, 
            120]);
      Arteries.Ascending_Aorta Ascending_Aorta1 annotation (extent=[34, -10; 
            74, 30]);
      Arteries.Ascending_Aorta_Flow Ascending_Aorta_Flow1 annotation (extent=[
            74, -10; 114, 30]);
      Arteries.Aortic_Arch Aortic_Arch1 annotation (extent=[114, -10; 154, 30]
        );
      Arteries.Pulmonary_Trunk Pulmonary_Trunk1 annotation (extent=[-30, 80; 
            10, 120]);
      Arteries.Ascending_Aortic_Arch_Flow Ascending_Aortic_Arch_Flow1 
        annotation (extent=[114, 30; 154, 70], rotation=90);
      Arteries.Descending_Aortic_Arch_Flow Descending_Aortic_Arch_Flow1 
        annotation (extent=[114, -50; 154, -10], rotation=-90);
      Arteries.Thoracic_Aorta Thoracic_Aorta1 annotation (extent=[114, -90; 
            154, -50]);
      BondLib.Interfaces.BondCon BondCon1 annotation (extent=[-70, 130; -50, 
            150]);
      BondLib.Interfaces.BondCon BondCon2 annotation (extent=[130, 130; 150, 
            150]);
      BondLib.Interfaces.BondCon BondCon3 annotation (extent=[130, -150; 150, 
            -130]);
      BondLib.Interfaces.BondCon BondCon4 annotation (extent=[90, -150; 110, -
            130]);
      BondLib.Interfaces.BondCon BondCon5 annotation (extent=[-30, -150; -10, 
            -130]);
      BondLib.Interfaces.BondCon BondCon6 annotation (extent=[-70, -150; -50, 
            -130]);
      Generic.Table_Lookup Table_Lookup1 annotation (extent=[24, -132; 64, -92
            ]);
      Modelica.Blocks.Sources.Clock Clock1 annotation (extent=[-16, -128; 18, 
            -96]);
      Modelica.Blocks.Interfaces.InPort InPort1 annotation (extent=[-120, 90; 
            -100, 110]);
      Modelica.Blocks.Interfaces.InPort InPort2 annotation (extent=[-120, 50; 
            -100, 70]);
      Modelica.Blocks.Interfaces.InPort InPort4 annotation (extent=[-120, -30
            ; -100, -10]);
      Modelica.Blocks.Interfaces.InPort InPort5 annotation (extent=[-120, -70
            ; -100, -50]);
      Modelica.Blocks.Interfaces.InPort InPort6 annotation (extent=[-120, -110
            ; -100, -90]);
      Modelica.Blocks.Interfaces.InPort InPort3 annotation (extent=[-120, 10; 
            -100, 30]);
    equation 
      connect(Sup_Cava_Venal_Flow1.BondCon2, Heart1.BondCon1) annotation (
          points=[-60, 40; -60, 30; -40, 30], style(color=8));
      connect(Inf_Cava_Venal_Flow1.BondCon2, Heart1.BondCon2) annotation (
          points=[-60, 18; -60, 22; -40, 22], style(color=8));
      connect(Superior_Vena_Cava1.BondCon2, Sup_Cava_Venal_Flow1.BondCon1) 
        annotation (points=[-60, 80; -60, 80], style(color=8));
      connect(Superior_Vena_Cava1.OutPort1, Sup_Cava_Venal_Flow1.InPort1) 
        annotation (points=[-72, 82; -72, 80], style(color=3));
      connect(Inferior_Vena_Cava1.BondCon1, Inf_Cava_Venal_Flow1.BondCon1) 
        annotation (points=[-60, -30; -60, -22], style(color=8));
      connect(Inferior_Vena_Cava1.OutPort1, Inf_Cava_Venal_Flow1.InPort1) 
        annotation (points=[-48, -32; -48, -20], style(color=3));
      connect(Heart1.BondCon5, Ascending_Aorta1.BondCon1) annotation (points=[
            20, 10; 32, 10], style(color=8));
      connect(Ascending_Aorta1.BondCon3, Heart1.BondCon4) annotation (points=[
            54, -10; 54, -16; 30, -16; 30, -4; 22, -4], style(color=8));
      connect(Ascending_Aorta1.BondCon2, Ascending_Aorta_Flow1.BondCon1) 
        annotation (points=[74, 10; 74, 10], style(color=8));
      connect(Ascending_Aorta_Flow1.BondCon2, Aortic_Arch1.BondCon1) 
        annotation (points=[114, 10; 114, 10], style(color=8));
      connect(Heart1.BondCon6, Pulmonary_Trunk1.BondCon1) annotation (points=[
            -26, 40; -26, 50; -10, 50; -10, 80], style(color=8));
      connect(Pulmonary_Trunk1.BondCon2, Lungs1.BondCon1) annotation (points=[
            10, 100; 10, 100], style(color=8));
      connect(Lungs1.BondCon2, Pulmonary_Veins1.BondCon1) annotation (points=[
            50, 100; 50, 100], style(color=8));
      connect(Pulmonary_Veins1.BondCon2, Pulmonary_Venal_Flow1.BondCon1) 
        annotation (points=[70, 80; 70, 72], style(color=8));
      connect(Pulmonary_Veins1.OutPort1, Pulmonary_Venal_Flow1.InPort1) 
        annotation (points=[58, 82; 58, 70], style(color=3));
      connect(Pulmonary_Venal_Flow1.BondCon2, Heart1.BondCon7) annotation (
          points=[70, 34; 70, 30; 30, 30; 30, 48; 6, 48; 6, 42], style(color=8)
        );
      connect(Aortic_Arch1.BondCon2, Ascending_Aortic_Arch_Flow1.BondCon1) 
        annotation (points=[134, 30; 134, 30], style(color=8));
      connect(Aortic_Arch1.BondCon3, Descending_Aortic_Arch_Flow1.BondCon1) 
        annotation (points=[134, -12; 134, -10], style(color=8));
      connect(Descending_Aortic_Arch_Flow1.BondCon2, Thoracic_Aorta1.BondCon2)
         annotation (points=[134, -50; 134, -50], style(color=8));
      connect(Thoracic_Aorta1.BondCon1, Bronchi1.BondCon1) annotation (points=
            [114, -70; 114, -70], style(color=8));
      connect(Bronchi1.BondCon2, Heart1.BondCon3) annotation (points=[74, -70
            ; -30, -70; -30, -20], style(color=8));
      connect(Superior_Vena_Cava1.BondCon1, BondCon1) annotation (points=[-60
            , 120; -60, 134], style(color=8));
      connect(Ascending_Aortic_Arch_Flow1.BondCon2, BondCon2) annotation (
          points=[134, 70; 134, 100; 140, 100; 140, 134], style(color=8));
      connect(Inferior_Vena_Cava1.BondCon3, BondCon6) annotation (points=[-72
            , -72; -72, -108; -60, -108; -60, -132], style(color=8));
      connect(Inferior_Vena_Cava1.BondCon2, BondCon5) annotation (points=[-60
            , -70; -60, -90; -20, -90; -20, -132], style(color=8));
      connect(Thoracic_Aorta1.BondCon3, BondCon3) annotation (points=[134, -90
            ; 134, -100; 140, -100; 140, -134], style(color=8));
      connect(Thoracic_Aorta1.BondCon4, BondCon4) annotation (points=[122, -90
            ; 122, -100; 100, -100; 100, -134], style(color=8));
      connect(Clock1.outPort, Table_Lookup1.InPort1) annotation (points=[19.5
            , -112; 28, -112], style(color=3));
      connect(Table_Lookup1.OutPort1, Thoracic_Aorta1.InPort1) annotation (
          points=[68, -112; 146, -112; 146, -88], style(color=3));
      connect(Table_Lookup1.OutPort1, Aortic_Arch1.InPort1) annotation (points
          =[66, -112; 160, -112; 160, -20; 146, -20; 146, -10], style(color=3))
        ;
      connect(Table_Lookup1.OutPort1, Ascending_Aorta1.InPort1) annotation (
          points=[68, -112; 72, -112; 72, -80; 66, -80; 66, -10], style(color=3
          ));
      connect(Table_Lookup1.OutPort1, Inferior_Vena_Cava1.InPort1) annotation 
        (points=[66, -112; 72, -112; 72, -80; -48, -80; -48, -70], style(color=
              3));
      connect(Inferior_Vena_Cava1.InPort1, Heart1.InPort2) annotation (points=
            [-48, -70; -48, -80; -22, -80; -22, -20], style(color=3));
      connect(Aortic_Arch1.InPort1, Pulmonary_Veins1.InPort1) annotation (
          points=[146, -8; 146, -20; 160, -20; 160, 74; 82, 74; 82, 82], style(
            color=3));
      connect(Pulmonary_Trunk1.InPort1, Superior_Vena_Cava1.InPort1) 
        annotation (points=[2, 80; 2, 74; -48, 74; -48, 80], style(color=3));
      connect(Superior_Vena_Cava1.InPort2, InPort1) annotation (points=[-80, 
            112; -90, 112; -90, 100; -100, 100], style(color=3));
      connect(Superior_Vena_Cava1.InPort2, Pulmonary_Veins1.InPort2) 
        annotation (points=[-80, 112; -90, 112; -90, 124; 44, 124; 44, 112; 50
            , 112], style(color=3));
      connect(Heart1.InPort1, InPort2) annotation (points=[-40, -10; -46, -10
            ; -46, 26; -74, 26; -74, 60; -110, 60], style(color=3));
      connect(Heart1.InPort4, InPort5) annotation (points=[20, -10; 26, -10; 
            26, -74; -100, -74; -100, -60], style(color=3));
      connect(Heart1.InPort5, InPort6) annotation (points=[10, -20; 10, -86; -
            84, -86; -84, -100; -100, -100], style(color=3));
      connect(Pulmonary_Veins1.InPort1, Pulmonary_Trunk1.InPort1) annotation (
          points=[82, 80; 82, 74; 2, 74; 2, 80], style(color=3));
      connect(InPort4, Heart1.InPort3) annotation (points=[-100, -20; -80, -20
            ; -80, -26; -8, -26; -8, -20], style(color=3));
      connect(InPort3, Lungs1.InPort1) annotation (points=[-100, 20; -80, 20; 
            -80, 36; -46, 36; -46, 60; 30, 60; 30, 82], style(color=3));
    end Thorax;
  end Body_Parts;
end Cardiovascular;
