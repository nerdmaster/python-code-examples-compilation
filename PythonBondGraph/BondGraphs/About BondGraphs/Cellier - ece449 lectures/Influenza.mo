package Influenza 
  annotation (Coordsys(
      extent=[0, 0; 442, 394], 
      grid=[2, 2], 
      component=[20, 20]), Window(
      x=0.01, 
      y=0.02, 
      width=0.4, 
      height=0.4, 
      library=1, 
      autolayout=1));
  block Infection_Rate 
    extends SystemDynamics.Interfaces.Nonlin_2;
    input Real P_ni "Noninfected population";
    input Real R_con "Contraction rate";
    output Real R_inf "Infection rate";
    annotation (Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
  equation 
    P_ni = u1;
    R_con = u2;
    R_inf = noEvent(integer(min([R_con, P_ni])));
    y = R_inf;
  end Infection_Rate;
  block Rate_equation 
    extends SystemDynamics.Interfaces.Nonlin_2;
    annotation (Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
  equation 
    y = noEvent(integer(u1/u2));
  end Rate_equation;
  model Flu 
    annotation (
      Coordsys(
        extent=[-220, -220; 200, 160], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(Ellipse(extent=[52, 4; 60, -2], style(fillColor=73))), 
      Window(
        x=0.03, 
        y=0, 
        width=0.65, 
        height=0.95));
    SystemDynamics.Level Non_infected(x0=10000) annotation (extent=[-160, -100
          ; -120, -60]);
    SystemDynamics.Rate_1 Infection annotation (extent=[-92, -100; -50, -60]);
    SystemDynamics.Level Infected(x0=0) annotation (extent=[-20, -100; 20, -60
          ]);
    SystemDynamics.Rate_1 Incubation annotation (extent=[50, -100; 92, -60]);
    SystemDynamics.Level Sick(x0=0) annotation (extent=[120, -100; 160, -60]);
    Infection_Rate Infection_Rate1 annotation (extent=[-116, -152; -78, -112])
      ;
    SystemDynamics.Prod_4 Prod_4_1 annotation (extent=[-160, -160; -122, -120]
        , rotation=-90);
    SystemDynamics.Const Contacts(k=15) annotation (extent=[-198, -200; -160, 
          -160]);
    SystemDynamics.Const Contraction(k=0.25) annotation (extent=[-160, -200; -
          122, -160]);
    Rate_equation Incubation_rate annotation (extent=[40, -160; 80, -120]);
    SystemDynamics.Const Time_to_Breakdown(k=4) annotation (extent=[8, -200; 
          48, -160]);
    Modelica.Blocks.Math.Add Contagious annotation (extent=[40, -40; 70, -10]
        , rotation=90);
    Modelica.Blocks.Math.Division Perc_Infected annotation (extent=[-70, -40; 
          -100, -10]);
    Modelica.Blocks.Math.Add3 Total_Pop annotation (extent=[0, 20; -30, 50]);
    SystemDynamics.Level Immune(x0=0) annotation (extent=[0, 100; -40, 140]);
    SystemDynamics.Rate_1 Cure annotation (extent=[100, 100; 58, 140]);
    Rate_equation Cure_rate annotation (extent=[130, 40; 90, 80]);
    SystemDynamics.Const Sick_Duration(k=2) annotation (extent=[120, 0; 160, 
          40]);
    SystemDynamics.Rate_1 Activation annotation (extent=[-98, 100; -140, 140])
      ;
    Rate_equation Activation_rate annotation (extent=[-60, 60; -100, 100]);
    SystemDynamics.Const Immune_Period(k=26) annotation (extent=[-140, 12; -
          100, 52]);
  equation 
    connect(Non_infected.MassInPort2, Infection.MassOutPort1) annotation (
        points=[-118, -80; -81.5, -80], style(color=77));
    connect(Infection.MassOutPort2, Infected.MassInPort1) annotation (points=[
          -60.5, -80; -22, -80], style(color=77));
    connect(Infected.MassInPort2, Incubation.MassOutPort1) annotation (points=
          [22, -80; 60.5, -80], style(color=77));
    connect(Incubation.MassOutPort2, Sick.MassInPort1) annotation (points=[
          81.5, -80; 118, -80], style(color=77));
    connect(Infection_Rate1.OutPort1, Infection.InPort1) annotation (points=[-
          79.9, -134; -72, -134; -72, -101], style(color=3));
    connect(Non_infected.OutPort3, Infection_Rate1.InPort1) annotation (points
        =[-126, -94; -126, -126; -114.1, -126], style(color=3));
    connect(Non_infected.OutPort1, Prod_4_1.InPort1) annotation (points=[-140
          , -93; -140, -124], style(color=3));
    connect(Contacts.OutPort1, Prod_4_1.InPort4) annotation (points=[-180, -
          164; -180, -146; -152, -146], style(color=3));
    connect(Contraction.OutPort1, Prod_4_1.InPort2) annotation (points=[-140, 
          -166; -140, -156], style(color=3));
    connect(Infected.OutPort3, Incubation_rate.InPort1) annotation (points=[16
          , -94; 16, -132; 40, -132], style(color=3));
    connect(Time_to_Breakdown.OutPort1, Incubation_rate.InPort2) annotation (
        points=[28, -164; 28, -148; 42, -148], style(color=3));
    connect(Incubation_rate.OutPort1, Incubation.InPort1) annotation (points=[
          78, -140; 90, -140; 90, -112; 72, -112; 72, -102], style(color=3));
    connect(Sick.OutPort5, Contagious.inPort2) annotation (points=[124, -68; 
          100, -68; 100, -56; 64, -56; 64, -43], style(color=3));
    connect(Infected.OutPort4, Contagious.inPort1) annotation (points=[18, -68
          ; 46, -68; 46, -46], style(color=3));
    connect(Perc_Infected.outPort, Prod_4_1.InPort3) annotation (points=[-102
          , -24; -200, -24; -200, -132; -152, -132], style(color=3));
    connect(Contagious.outPort, Perc_Infected.inPort1) annotation (points=[56
          , -8; 56, 0; -20, 0; -20, -16; -64, -16], style(color=3));
    connect(Total_Pop.outPort, Perc_Infected.inPort2) annotation (points=[-
          31.5, 35; -52, 35; -52, -34; -64, -34], style(color=3));
    connect(Non_infected.OutPort4, Total_Pop.inPort3) annotation (points=[-122
          , -68; -100, -68; -100, -52; 20, -52; 20, 24; 6, 24], style(color=3))
      ;
    connect(Contagious.outPort, Total_Pop.inPort2) annotation (points=[56, -8
          ; 56, 35; 6, 35], style(color=3));
    connect(Immune.OutPort2, Total_Pop.inPort1) annotation (points=[-4, 108; 
          20, 108; 20, 47; 4, 47], style(color=3));
    connect(Cure.MassOutPort2, Immune.MassInPort1) annotation (points=[68.5, 
          120; 4, 120], style(color=77));
    connect(Sick.MassInPort2, Cure.MassOutPort1) annotation (points=[162, -80
          ; 180, -80; 180, 120; 89.5, 120], style(color=77));
    connect(Cure_rate.OutPort1, Cure.InPort1) annotation (points=[92, 60; 80, 
          60; 80, 98], style(color=3));
    connect(Sick.OutPort4, Cure_rate.InPort1) annotation (points=[156, -68; 
          166, -68; 166, 68; 130, 68], style(color=3));
    connect(Sick_Duration.OutPort1, Cure_rate.InPort2) annotation (points=[140
          , 36; 140, 52; 130, 52], style(color=3));
    connect(Immune.MassInPort2, Activation.MassOutPort1) annotation (points=[-
          42, 120; -108.5, 120], style(color=77));
    connect(Activation.MassOutPort2, Non_infected.MassInPort1) annotation (
        points=[-130, 120; -180, 120; -180, -80; -162, -80], style(color=77));
    connect(Activation_rate.OutPort1, Activation.InPort1) annotation (points=[
          -100, 80; -118, 80; -118, 99], style(color=3));
    connect(Immune.OutPort3, Activation_rate.InPort1) annotation (points=[-36
          , 106; -36, 88; -61.6, 88], style(color=3));
    connect(Immune_Period.OutPort1, Activation_rate.InPort2) annotation (
        points=[-120, 48; -120, 54; -48, 54; -48, 70; -62, 70], style(color=3))
      ;
    connect(Prod_4_1.OutPort1, Infection_Rate1.InPort2) annotation (points=[-
          126, -140; -116, -140], style(color=3));
  algorithm 
    when time > 8.0 then
      reinit(Non_infected.Integrator1.y[1], 9999);
      reinit(Infected.Integrator1.y[1], 1);
    end when;
    
  end Flu;
end Influenza;
