model WaterSerpentine 
  annotation (
    Coordsys(
      extent=[-80, -160; 200, 100], 
      grid=[2, 2], 
      component=[20, 20]), 
    Window(
      x=0.37, 
      y=0.02, 
      width=0.53, 
      height=0.85), 
    Icon(Line(points=[-50, -100; -30, -100; -30, 38; -10, 38; -10, -98; 10, 
            -98; 10, 38; 30, 38; 30, -98; 50, -98; 50, 38; 70, 38; 70, -98; 90
            , -98; 90, 38; 110, 38; 110, -98; 130, -98; 130, 38; 150, 38; 150, 
            -100], style(thickness=2)), Line(points=[150, -100; 170, -100], 
          style(thickness=2))), 
    Diagram(
      Line(points=[30, -20; 30, -19.4737], style(color=8)), 
      Line(points=[30, -0.0526316; 30, 0], style(color=8)), 
      Line(points=[0, 30; 0, 30], style(color=8)), 
      Line(points=[-20, -50; -20, -50], style(color=8)), 
      Line(points=[-50, 0; -50, 0], style(color=8)), 
      Line(points=[-20, 30; -20, 30], style(color=8)), 
      Line(points=[0, -50; 0, -50], style(color=8)), 
      Line(points=[-50, -20; -50, -20], style(color=8))));
  ThermoBondLib.Substances.Water Water1 annotation (extent=[20, -100; 40, -80]
      , rotation=-90);
  ThermoBondLib.Bonds.eThermoBond B1 annotation (extent=[20, -80; 40, -60], 
      rotation=-90);
  ThermoBondLib.Junctions.J0p3 J0p3_1 annotation (extent=[20, -60; 40, -40], 
      rotation=-90);
  ThermoBondLib.Bonds.fThermoBond B2 annotation (extent=[20, -40; 40, -20], 
      rotation=90);
  ThermoBondLib.Bonds.eThermoBond B3 annotation (extent=[20, 0; 40, 20], 
      rotation=90);
  ThermoBondLib.Junctions.J0p3 J0p3_2 annotation (extent=[20, 20; 40, 40], 
      rotation=-90);
  ThermoBondLib.Bonds.eThermoBond B4 annotation (extent=[20, 40; 40, 60], 
      rotation=90);
  ThermoBondLib.Substances.Water Water2 annotation (extent=[20, 60; 40, 80], 
      rotation=90);
  ThermoBondLib.Passive.FVF FVF1 annotation (extent=[40, -28; 20, 8], rotation
      =90);
  ThermoBondLib.Bonds.fThermoBond B5 annotation (extent=[0, 20; 20, 40], 
      rotation=180);
  ThermoBondLib.Passive.VF VF1 annotation (extent=[0, 18; -20, 40]);
  ThermoBondLib.Bonds.eThermoBond B6 annotation (extent=[-40, 20; -20, 40], 
      rotation=180);
  ThermoBondLib.Junctions.J0p3 J0p3_3 annotation (extent=[-40, 20; -60, 40], 
      rotation=-90);
  ThermoBondLib.Bonds.eThermoBond B7 annotation (extent=[-60, 40; -40, 60], 
      rotation=90);
  ThermoBondLib.Substances.Water Water3 annotation (extent=[-60, 60; -40, 80]
      , rotation=90);
  ThermoBondLib.Bonds.fThermoBond B8 annotation (extent=[-60, 0; -40, 20], 
      rotation=270);
  ThermoBondLib.Passive.VF VF2 annotation (extent=[-38, -28; -62, 6], rotation
      =270);
  ThermoBondLib.Bonds.eThermoBond B9 annotation (extent=[-60, -40; -40, -20], 
      rotation=270);
  ThermoBondLib.Junctions.J0p3 J0p3_4 annotation (extent=[-40, -60; -60, -40]
      , rotation=-90);
  ThermoBondLib.Bonds.eThermoBond B10 annotation (extent=[-60, -80; -40, -60]
      , rotation=-90);
  ThermoBondLib.Substances.Water Water4 annotation (extent=[-60, -100; -40, 
        -80], rotation=-90);
  ThermoBondLib.Bonds.fThermoBond B11 annotation (extent=[-40, -60; -20, -40])
    ;
  ThermoBondLib.Passive.VF VF3 annotation (extent=[-20, -62; 0, -40]);
  ThermoBondLib.Bonds.eThermoBond B12 annotation (extent=[0, -60; 20, -40]);
  BondLib.Passive.TF TF1 annotation (extent=[100, -20; 80, 0]);
  BondLib.Bonds.eBond B13 annotation (extent=[60, -20; 80, 0], rotation=180);
  BondLib.Bonds.eBond B14 annotation (extent=[100, -20; 120, 0], rotation=180)
    ;
  BondLib.Junctions.J1p4 J1p4_1 annotation (extent=[120, -20; 140, 0]);
  BondLib.Bonds.fBond B15 annotation (extent=[120, 0; 140, 20], rotation=90);
  BondLib.Passive.I I1 annotation (extent=[120, 20; 140, 40], rotation=90);
  BondLib.Bonds.eBond B16 annotation (extent=[140, -20; 160, 0]);
  BondLib.Passive.R R1 annotation (extent=[160, -20; 180, 0]);
  BondLib.Bonds.fBond B17 annotation (extent=[120, -40; 140, -20], rotation=90
    );
  BondLib.Passive.GY GY1 annotation (extent=[120, -60; 140, -40], rotation=90)
    ;
  BondLib.Bonds.eBond B18 annotation (extent=[120, -80; 140, -60], rotation=90
    );
  BondLib.Junctions.J1p4 J1p4_2 annotation (extent=[120, -100; 140, -80]);
  BondLib.Bonds.fBond B19 annotation (extent=[120, -120; 140, -100], rotation=
        90);
  BondLib.Sources.SE SE1 annotation (extent=[120, -140; 140, -120], rotation=
        -90);
  BondLib.Bonds.eBond B20 annotation (extent=[140, -100; 160, -80]);
  BondLib.Passive.R R2 annotation (extent=[158, -100; 178, -80]);
  BondLib.Bonds.fBond B21 annotation (extent=[100, -100; 120, -80], rotation=
        180);
  BondLib.Passive.I I2 annotation (extent=[80, -80; 100, -100], rotation=180);
equation 
  connect(B1.eThBondCon1, Water1.ThBondCon1) annotation (points=[30, -80; 30, 
        -80], style(color=8));
  connect(J0p3_1.ThBondCon2, B1.fThBondCon1) annotation (points=[30, -60; 30, 
        -60], style(color=8));
  connect(J0p3_1.ThBondCon1, B2.eThBondCon1) annotation (points=[30, -40; 30, 
        -40], style(color=8));
  connect(B3.eThBondCon1, J0p3_2.ThBondCon2) annotation (points=[30, 20; 30, 
        20], style(color=8));
  connect(J0p3_2.ThBondCon1, B4.fThBondCon1) annotation (points=[30, 40; 30, 
        40], style(color=8));
  connect(B4.eThBondCon1, Water2.ThBondCon1) annotation (points=[30, 60; 30, 
        60], style(color=8));
  connect(J0p3_2.ThBondCon3, B5.eThBondCon1) annotation (points=[20, 30; 20, 
        30], style(color=8));
  connect(B6.eThBondCon1, J0p3_3.ThBondCon3) annotation (points=[-40, 30; -40
        , 30], style(color=8));
  connect(J0p3_3.ThBondCon1, B7.fThBondCon1) annotation (points=[-50, 40; -50
        , 40], style(color=8));
  connect(B7.eThBondCon1, Water3.ThBondCon1) annotation (points=[-50, 60; -50
        , 60], style(color=8));
  connect(J0p3_3.ThBondCon2, B8.eThBondCon1) annotation (points=[-50, 20; -50
        , 20], style(color=8));
  connect(B9.eThBondCon1, J0p3_4.ThBondCon1) annotation (points=[-50, -40; -50
        , -40], style(color=8));
  connect(J0p3_4.ThBondCon2, B10.fThBondCon1) annotation (points=[-50, -60; 
        -50, -60], style(color=8));
  connect(B10.eThBondCon1, Water4.ThBondCon1) annotation (points=[-50, -80; 
        -50, -80], style(color=8));
  connect(J0p3_4.ThBondCon3, B11.eThBondCon1) annotation (points=[-40, -50; 
        -40, -50], style(color=8));
  connect(B12.eThBondCon1, J0p3_1.ThBondCon3) annotation (points=[20, -50; 20
        , -50], style(color=8));
  connect(FVF1.BondCon1, B13.eBondCon1) annotation (points=[49, -10; 60, -10]
      , style(color=8));
  connect(B13.fBondCon1, TF1.BondCon2) annotation (points=[80, -10; 80, -10], 
      style(color=8));
  connect(B14.eBondCon1, TF1.BondCon1) annotation (points=[100, -10; 100, -10]
      , style(color=8));
  connect(J1p4_1.BondCon1, B14.fBondCon1) annotation (points=[120, -10; 120, 
        -10], style(color=8));
  connect(J1p4_1.BondCon4, B15.eBondCon1) annotation (points=[130, 0; 130, 0]
      , style(color=8));
  connect(B15.fBondCon1, I1.BondCon1) annotation (points=[130, 20; 130, 20], 
      style(color=8));
  connect(J1p4_1.BondCon2, B16.fBondCon1) annotation (points=[140, -10; 140, 
        -10], style(color=8));
  connect(B16.eBondCon1, R1.BondCon1) annotation (points=[160, -10; 160, -10]
      , style(color=8));
  connect(B17.fBondCon1, J1p4_1.BondCon3) annotation (points=[130, -20; 130, 
        -20], style(color=8));
  connect(GY1.BondCon2, B17.eBondCon1) annotation (points=[130, -40; 130, -40]
      , style(color=8));
  connect(B18.eBondCon1, GY1.BondCon1) annotation (points=[130, -60; 130, -60]
      , style(color=8));
  connect(J1p4_2.BondCon4, B18.fBondCon1) annotation (points=[130, -80; 130, 
        -80], style(color=8));
  connect(B19.fBondCon1, J1p4_2.BondCon3) annotation (points=[130, -100; 130, 
        -100], style(color=8));
  connect(SE1.BondCon1, B19.eBondCon1) annotation (points=[130, -120; 130, 
        -120], style(color=8));
  connect(J1p4_2.BondCon2, B20.fBondCon1) annotation (points=[140, -90; 140, 
        -90], style(color=8));
  connect(B20.eBondCon1, R2.BondCon1) annotation (points=[160, -90; 158, -90]
      , style(color=8));
  connect(J1p4_2.BondCon1, B21.eBondCon1) annotation (points=[120, -90; 120, 
        -90], style(color=8));
  connect(I2.BondCon1, B21.fBondCon1) annotation (points=[100, -90; 100, -90]
      , style(color=8));
  connect(B2.fThBondCon1, FVF1.ThBondCon1) annotation (points=[30, -20; 30, 
        -20], style(color=8));
  connect(B3.fThBondCon1, FVF1.ThBondCon2) annotation (points=[30, 0; 30, 
        -0.526316], style(color=8));
  connect(B5.fThBondCon1, VF1.ThBondCon1) annotation (points=[0, 30; 0, 
        29.6111], style(color=8));
  connect(B6.fThBondCon1, VF1.ThBondCon2) annotation (points=[-20, 30; -20, 
        29.6111], style(color=8));
  connect(B8.fThBondCon1, VF2.ThBondCon1) annotation (points=[-50, 0; -50, 0]
      , style(color=8));
  connect(B9.fThBondCon1, VF2.ThBondCon2) annotation (points=[-50, -20; -50, 
        -20], style(color=8));
  connect(B11.fThBondCon1, VF3.ThBondCon1) annotation (points=[-20, -50; -20, 
        -50.3889], style(color=8));
  connect(B12.fThBondCon1, VF3.ThBondCon2) annotation (points=[0, -50; 0, 
        -50.3889], style(color=8));
end WaterSerpentine;
