package LarchBudMoth 
  annotation (Coordsys(
      extent=[0, 0; 402, 261], 
      grid=[2, 2], 
      component=[20, 20]));
  block Starvation 
    extends SystemDynamics.Interfaces.Nonlin_2;
    output Real foliage;
    output Real food_demand;
    output Real starvation;
    annotation (Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]));
  equation 
    foliage = u1;
    food_demand = u2;
    starvation = exp(-foliage/food_demand);
    y = starvation;
  end Starvation;
  block Defoliation 
    extends SystemDynamics.Interfaces.Nonlin_3;
    output Real foliage;
    output Real food_demand;
    output Real starvation;
    output Real defoliation;
    annotation (Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]));
  equation 
    foliage = u1;
    food_demand = u2;
    starvation = u3;
    defoliation = (1.0 - starvation)*(food_demand/foliage);
    y = defoliation;
  end Defoliation;
  block Grecr 
    extends SystemDynamics.Interfaces.Nonlin_2;
    output Real defoliation "First input";
    output Real rawfiber "Second input";
    output Real zraw "Auxiliary variable";
    output Real grecr1 "Auxiliary variable";
    output Real grecr2 "Auxiliary variable";
    output Real grecr3 "Auxiliary variable";
    output Real grecr4 "Auxiliary variable";
    output Real grecr "Output variable";
    
    annotation (Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), Window(
        x=0.22, 
        y=0.11, 
        width=0.6, 
        height=0.6));
  equation 
    defoliation = u1;
    rawfiber = u2;
    zraw = 0.425 + sqrt((18.0 - rawfiber)/max([(rawfiber - 11.99)^2, 0.01]));
    grecr1 = if zraw > rawfiber - 11.99 then 11.99/rawfiber else 1.0 - zraw/
      rawfiber;
    grecr2 = if rawfiber > 11.99 then grecr1 else 1.0;
    grecr3 = 1.0 + (defoliation - 0.4)*(18.0 - rawfiber)/(0.4*rawfiber);
    grecr4 = 18.0/rawfiber;
    grecr = if defoliation < 0.4 then grecr2 else if defoliation < 0.8 then 
      grecr3 else grecr4;
    y = grecr;
  end Grecr;
  block Logarithm 
    extends SystemDynamics.Interfaces.Nonlin_1;
    annotation (Coordsys(
        extent=[-100, -100; 100, 100], 
        grid=[2, 2], 
        component=[20, 20]), Window(
        x=0.4, 
        y=0.4, 
        width=0.6, 
        height=0.6));
  equation 
    y = Modelica.Math.log10(u);
  end Logarithm;
  model LBM 
    parameter Real wintermortality=0.5728 
      "Percentage of eggs dying during the winter";
    parameter Real biomass=91.3 "Dry needle biomass per tree in kg";
    parameter Integer nbr_trees=511147 "Number of trees";
    parameter Integer egg0=4765975 "Initial number of eggs";
    parameter Real rawfiber0=15.0 "Initial rawfiber";
    Real LogDensitySim "Simulated logarithmic larvae density";
    Real LogDensityMeas "Measured logarithmic larvae density";
    Real years "Simulation time in years";
    annotation (
      Coordsys(
        extent=[-200, -240; 280, 200], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Ellipse(extent=[48, 58; 54, 54], style(fillColor=73)), 
        Ellipse(extent=[28, -142; 32, -146], style(fillColor=73)), 
        Ellipse(extent=[34, -146; 38, -150], style(fillColor=73)), 
        Ellipse(extent=[82, -138; 86, -142], style(fillColor=73)), 
        Ellipse(extent=[-62, -92; -58, -98], style(fillColor=73)), 
        Ellipse(extent=[-62, -200; -56, -204], style(fillColor=73)), 
        Text(
          extent=[2, 92; 50, 82], 
          string="small Larvae", 
          style(color=10)), 
        Text(
          extent=[70, 68; 118, 58], 
          string="large Larvae", 
          style(color=10)), 
        Text(
          extent=[116, 124; 164, 114], 
          string="Insects", 
          style(color=10)), 
        Text(
          extent=[14, 122; 62, 112], 
          string="Females", 
          style(color=10)), 
        Ellipse(extent=[134, 58; 140, 52], style(fillColor=73))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    SystemDynamics.Source Source1 annotation (extent=[-180, 140; -160, 160]);
    SystemDynamics.Rate_1 NewEggs annotation (extent=[-140, 130; -100, 170]);
    SystemDynamics.DiscreteLevel Eggs(x0=egg0) annotation (extent=[-80, 130; 
          -40, 170]);
    SystemDynamics.Rate_1 OldEggs annotation (extent=[-20, 130; 20, 170]);
    SystemDynamics.Sink Sink1 annotation (extent=[40, 140; 60, 160]);
    SystemDynamics.Gain Hatching(k=1.0 - wintermortality) annotation (extent=[
          -40, 60; 0, 100]);
    SystemDynamics.DiscreteLevel Rawfiber(x0=rawfiber0) annotation (extent=[
          -40, -80; 0, -40]);
    SystemDynamics.Rate_1 NewFiber annotation (extent=[-100, -80; -60, -40]);
    SystemDynamics.Source Source2 annotation (extent=[-130, -70; -110, -50]);
    SystemDynamics.Rate_1 OldFiber annotation (extent=[22, -80; 62, -40]);
    SystemDynamics.Sink Sink2 annotation (extent=[70, -70; 90, -50]);
    SystemDynamics.Linear Incoincidence(m=0.05112, b=-0.17932) annotation (
        extent=[-40, 0; -10, 30]);
    SystemDynamics.Linear Coincidence(m=-1.0, b=1.0) annotation (extent=[0, 0
          ; 30, 30]);
    SystemDynamics.Prod_2 Prod_2_1 annotation (extent=[8, 40; 40, 70], 
        rotation=-90);
    SystemDynamics.Linear Weakening(m=0.124017, b=-1.435284) annotation (
        extent=[10, -36; 40, -6]);
    SystemDynamics.Linear Physiology(m=-1.0, b=1.0) annotation (extent=[52, 
          -36; 82, -6]);
    SystemDynamics.Linear TreeFoliage(m=-2.25933, b=67.38939) annotation (
        extent=[-40, -138; -10, -108]);
    SystemDynamics.Prod_2 Prod_2_2 annotation (extent=[-6, -160; 26, -130], 
        rotation=-90);
    SystemDynamics.Const NbrTrees(k=nbr_trees) annotation (extent=[-10, -200; 
          30, -160]);
    Starvation Starvation1 annotation (extent=[40, -160; 80, -120]);
    SystemDynamics.Gain FoodDemand(k=0.005472) annotation (extent=[68, 0; 100
          , 30]);
    SystemDynamics.Linear FoodSupply(m=-1.0, b=1.0) annotation (extent=[90, 
          -154; 120, -124]);
    SystemDynamics.Prod_3 Prod_3_1 annotation (extent=[120, -36; 150, -6], 
        rotation=270);
    SystemDynamics.Gain SexRatio(k=0.44) annotation (extent=[124, 90; 84, 130]
      );
    SystemDynamics.Linear Fecundity(m=-18.475457, b=356.72636) annotation (
        extent=[-110, -138; -80, -108]);
    SystemDynamics.Prod_2 Prod_2_3 annotation (extent=[-172, 60; -140, 90], 
        rotation=-90);
    Defoliation Defoliation1 annotation (extent=[60, -220; 100, -180]);
    Grecr Grecr1 annotation (extent=[120, -220; 160, -180]);
    SystemDynamics.Prod_2 Prod_2_4 annotation (extent=[-124, -216; -156, -186]
      );
    SystemDynamics.Gain LarvaeDensity(k=1.0/(biomass*nbr_trees)) annotation (
        extent=[162, 36; 202, 76]);
    Logarithm Logarithm1 annotation (extent=[208, 40; 238, 70]);
    SystemDynamics.Funct Measurement(x_vals=1949:1978, y_vals={0.018,0.082,
          0.444,4.174,68.797,331.76,126.541,21.28,2.246,0.085,0.08,0.371,1.638,
          22.878,248.817,184.272,3.116,0.019,0.002,0.059,0.197,1.068,10.569,
          173.932,249.612,176.023,4.749,0.014,0.008,0.056}) annotation (extent=
          [160, -80; 200, -40]);
    Logarithm Logarithm2 annotation (extent=[216, -74; 246, -44]);
  equation 
    connect(Source1.MassInPort1, NewEggs.MassOutPort1) annotation (points=[
          -158, 150; -130, 150], style(color=77));
    connect(NewEggs.MassOutPort2, Eggs.MassInPort1) annotation (points=[-108, 
          150; -84, 150], style(color=77));
    connect(Eggs.MassInPort2, OldEggs.MassOutPort1) annotation (points=[-36, 
          150; -12, 150], style(color=77));
    connect(Eggs.OutPort3, OldEggs.InPort1) annotation (points=[-44, 136; -44
          , 124; 0, 124; 0, 128], style(color=3));
    connect(OldEggs.MassOutPort2, Sink1.MassInPort1) annotation (points=[12, 
          150; 38, 150], style(color=77));
    connect(Eggs.OutPort1, Hatching.InPort1) annotation (points=[-60, 136; -60
          , 80; -36, 80], style(color=3));
    connect(NewFiber.MassOutPort2, Rawfiber.MassInPort1) annotation (points=[
          -68, -60; -44, -60], style(color=77));
    connect(Source2.MassInPort1, NewFiber.MassOutPort1) annotation (points=[
          -108, -60; -90, -60], style(color=77));
    connect(Rawfiber.MassInPort2, OldFiber.MassOutPort1) annotation (points=[4
          , -60; 30, -60], style(color=77));
    connect(Rawfiber.OutPort3, OldFiber.InPort1) annotation (points=[-4, -74; 
          -4, -86; 42, -86; 42, -82], style(color=3));
    connect(OldFiber.MassOutPort2, Sink2.MassInPort1) annotation (points=[54, 
          -60; 68, -60], style(color=77));
    connect(Rawfiber.OutPort5, Incoincidence.InPort1) annotation (points=[-36
          , -50; -50, -50; -50, 15; -38, 15], style(color=3));
    connect(Incoincidence.OutPort1, Coincidence.InPort1) annotation (points=[
          -10, 16; 0, 16], style(color=3));
    connect(Hatching.OutPort1, Prod_2_1.InPort1) annotation (points=[-4, 80; 
          24, 80; 24, 65.5], style(color=3));
    connect(Coincidence.OutPort1, Prod_2_1.InPort2) annotation (points=[30, 16
          ; 40, 16; 40, 34; 24, 34; 24, 44.5], style(color=3));
    connect(Rawfiber.OutPort4, Weakening.InPort1) annotation (points=[-2, -48
          ; 4, -48; 4, -22; 12, -22], style(color=3));
    connect(Weakening.OutPort1, Physiology.InPort1) annotation (points=[38.5, 
          -20; 52, -20], style(color=3));
    connect(TreeFoliage.OutPort1, Prod_2_2.InPort1) annotation (points=[-10, 
          -122; 10, -122; 10, -134.5], style(color=3));
    connect(NbrTrees.OutPort1, Prod_2_2.InPort2) annotation (points=[10, -164
          ; 10, -155.5], style(color=3));
    connect(Prod_2_2.OutPort1, Starvation1.InPort1) annotation (points=[22, 
          -144; 30, -144; 30, -132; 40, -132], style(color=3));
    connect(Prod_2_1.OutPort1, FoodDemand.InPort1) annotation (points=[35.2, 
          56; 50, 56; 50, 16; 74, 16], style(color=3));
    connect(FoodDemand.OutPort1, Starvation1.InPort2) annotation (points=[95.2
          , 16; 110, 16; 110, -100; 36, -100; 36, -148; 42, -148], style(color=
            3));
    connect(Starvation1.OutPort1, FoodSupply.InPort1) annotation (points=[80, 
          -140; 90, -140], style(color=3));
    connect(Physiology.OutPort1, Prod_3_1.InPort3) annotation (points=[82, -22
          ; 124.5, -22], style(color=3));
    connect(Prod_2_1.OutPort1, Prod_3_1.InPort1) annotation (points=[36, 56; 
          136, 56; 136, -10], style(color=3));
    connect(FoodSupply.OutPort1, Prod_3_1.InPort2) annotation (points=[120, 
          -140; 136, -140; 136, -31.5], style(color=3));
    connect(Prod_3_1.OutPort1, SexRatio.InPort1) annotation (points=[145.5, 
          -20; 154, -20; 154, 110; 118, 110], style(color=3));
    connect(Rawfiber.OutPort1, TreeFoliage.InPort1) annotation (points=[-20, 
          -74; -20, -100; -52, -100; -52, -124; -40, -124], style(color=3));
    connect(SexRatio.OutPort1, Prod_2_3.InPort1) annotation (points=[90, 110; 
          -156, 110; -156, 85.5], style(color=3));
    connect(Prod_2_3.OutPort1, NewEggs.InPort1) annotation (points=[-144, 76; 
          -120, 76; -120, 128], style(color=3));
    connect(Rawfiber.OutPort2, Fecundity.InPort1) annotation (points=[-36, -74
          ; -35.5, -74; -35.5, -94; -122, -94; -122, -124; -108.5, -124], style
        (color=3));
    connect(Fecundity.OutPort1, Prod_2_3.InPort2) annotation (points=[-80, 
          -124; -70, -124; -70, -160; -156, -160; -156, 64], style(color=3));
    connect(Prod_2_2.OutPort1, Defoliation1.InPort1) annotation (points=[22, 
          -144; 30, -144; 30, -188; 60, -188], style(color=3));
    connect(Defoliation1.InPort2, Starvation1.InPort2) annotation (points=[60
          , -200; 36, -200; 36, -148; 42, -148], style(color=3));
    connect(Starvation1.OutPort1, Defoliation1.InPort3) annotation (points=[78
          , -140; 84, -140; 84, -168; 50, -168; 50, -212; 60, -212], style(
          color=3));
    connect(Defoliation1.OutPort1, Grecr1.InPort1) annotation (points=[100, 
          -200; 110, -200; 110, -192; 121.6, -192], style(color=3));
    connect(Rawfiber.OutPort2, Grecr1.InPort2) annotation (points=[-36, -72; 
          -36, -94; -60, -94; -60, -226; 112, -226; 112, -206; 120, -206], 
        style(color=3));
    connect(Grecr1.InPort2, Prod_2_4.InPort1) annotation (points=[120, -206; 
          112, -206; 112, -226; -60, -226; -60, -202; -128, -202], style(color=
            3));
    connect(Grecr1.OutPort1, Prod_2_4.InPort2) annotation (points=[160, -200; 
          180, -200; 180, -232; -166, -232; -166, -202; -151.2, -202], style(
          color=3));
    connect(Prod_2_4.OutPort1, NewFiber.InPort1) annotation (points=[-140, 
          -190.5; -140, -88; -80, -88; -80, -82], style(color=3));
    connect(Prod_2_1.OutPort1, LarvaeDensity.InPort1) annotation (points=[35.2
          , 56; 168, 56], style(color=3));
    connect(LarvaeDensity.OutPort1, Logarithm1.InPort1) annotation (points=[
          198, 56; 208, 56], style(color=3));
    connect(Measurement.OutPort1, Logarithm2.InPort1) annotation (points=[204
          , -60; 218, -60], style(color=3));
    LogDensitySim = Logarithm1.OutPort1.signal[1];
    years = time + 1949;
    Measurement.InPort1.signal[1] = years;
    LogDensityMeas = Logarithm2.OutPort1.signal[1];
  end LBM;
end LarchBudMoth;
