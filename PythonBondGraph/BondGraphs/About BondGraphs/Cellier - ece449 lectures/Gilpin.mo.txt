package Gilpin 
  annotation (Coordsys(
      extent=[0, 0; 402, 261], 
      grid=[2, 2], 
      component=[20, 20]));
  model Gilpin_Model 
    annotation (
      Coordsys(
        extent=[-200, -200; 260, 200], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Ellipse(extent=[138, 22; 142, 18], style(fillColor=73)), 
        Ellipse(extent=[-82, 42; -78, 38], style(fillColor=73)), 
        Ellipse(extent=[-68, 62; -64, 58], style(fillColor=73)), 
        Ellipse(extent=[154, 62; 158, 58], style(fillColor=73))), 
      Window(
        x=0.05, 
        y=0, 
        width=0.44, 
        height=0.65));
    SystemDynamics.Source Source1 annotation (extent=[-180, 100; -160, 120]);
    SystemDynamics.Level Prey_1(x0=100) annotation (extent=[-100, 90; -60, 130
          ]);
    SystemDynamics.Sink Sink1 annotation (extent=[0, 100; 20, 120]);
    SystemDynamics.Source Source2 annotation (extent=[40, 100; 60, 120]);
    SystemDynamics.Level Prey_2(x0=100) annotation (extent=[120, 90; 160, 130]
      );
    SystemDynamics.Sink Sink2 annotation (extent=[220, 100; 240, 120]);
    SystemDynamics.Rate_1 Birth_1 annotation (extent=[-150, 90; -110, 130]);
    SystemDynamics.Rate_3 Death_1 annotation (extent=[-50, 90; -10, 130]);
    SystemDynamics.Rate_1 Birth_2 annotation (extent=[70, 90; 110, 130]);
    SystemDynamics.Rate_3 Death_2 annotation (extent=[168, 90; 208, 130]);
    SystemDynamics.Prod_3 Prod_3_1 annotation (extent=[-44, 46; -14, 76]);
    SystemDynamics.Const k1(k=0.001) annotation (extent=[-20, 14; 20, 52]);
    SystemDynamics.Prod_3 Prod_3_2 annotation (extent=[174, 46; 204, 76]);
    SystemDynamics.Const k2(k=0.001) annotation (extent=[200, 14; 240, 52]);
    SystemDynamics.Prod_3 Prod_3_3 annotation (extent=[-94, 154; -64, 184], 
        rotation=270);
    SystemDynamics.Const k3(k=0.001) annotation (extent=[-140, 150; -100, 188]
        , rotation=-90);
    SystemDynamics.Prod_3 Prod_3_4 annotation (extent=[204, 186; 174, 156], 
        rotation=-90);
    SystemDynamics.Const k4(k=0.0015) annotation (extent=[252, 150; 212, 188]
        , rotation=-90);
    SystemDynamics.Level Predator(x0=100) annotation (extent=[20, -80; 60, -40
          ]);
    SystemDynamics.Rate_1 Death_P annotation (extent=[80, -80; 120, -40]);
    SystemDynamics.Rate_2 Birth_P annotation (extent=[-40, -80; -2, -40]);
    SystemDynamics.Source Source3 annotation (extent=[-80, -70; -60, -50]);
    SystemDynamics.Sink Sink3 annotation (extent=[140, -70; 160, -50]);
    SystemDynamics.Prod_3 Prod_3_5 annotation (extent=[-94, -20; -64, 8], 
        rotation=-90);
    SystemDynamics.Const k5(k=0.01) annotation (extent=[-140, -24; -100, 14], 
        rotation=-90);
    SystemDynamics.Prod_3 Prod_3_6 annotation (extent=[124, -20; 154, 8], 
        rotation=-90);
    SystemDynamics.Const k6(k=0.001) annotation (extent=[80, -24; 120, 14], 
        rotation=-90);
    SystemDynamics.Prod_3 Prod_3_7 annotation (extent=[-70, -140; -40, -112], 
        rotation=-90);
    SystemDynamics.Const k7(k=0.005) annotation (extent=[-120, -144; -80, -106
          ], rotation=-90);
    SystemDynamics.Prod_3 Prod_3_8 annotation (extent=[100, -120; 70, -150], 
        rotation=-90);
    SystemDynamics.Const k8(k=0.0005) annotation (extent=[152, -160; 112, -122
          ], rotation=-90);
  equation 
    connect(Source1.MassInPort1, Birth_1.MassOutPort1) annotation (points=[
          -158, 110; -140, 110], style(color=77));
    connect(Birth_1.MassOutPort2, Prey_1.MassInPort1) annotation (points=[-120
          , 110; -104, 110], style(color=77));
    connect(Prey_1.MassInPort2, Death_1.MassOutPort1) annotation (points=[-56
          , 110; -42, 110], style(color=77));
    connect(Death_1.MassOutPort2, Sink1.MassInPort1) annotation (points=[-20, 
          110; -2, 110], style(color=77));
    connect(Source2.MassInPort1, Birth_2.MassOutPort1) annotation (points=[62
          , 110; 80, 110], style(color=77));
    connect(Birth_2.MassOutPort2, Prey_2.MassInPort1) annotation (points=[102
          , 110; 118, 110], style(color=77));
    connect(Prey_2.MassInPort2, Death_2.MassOutPort1) annotation (points=[164
          , 110; 176, 110], style(color=77));
    connect(Death_2.MassOutPort2, Sink2.MassInPort1) annotation (points=[200, 
          110; 219, 110], style(color=77));
    connect(Birth_1.InPort1, Prey_1.OutPort2) annotation (points=[-130, 88; 
          -130, 80; -94, 80; -94, 96], style(color=3));
    connect(Birth_2.InPort1, Prey_2.OutPort2) annotation (points=[90, 88; 90, 
          80; 126, 80; 126, 96], style(color=3));
    connect(Prod_3_1.OutPort1, Death_1.InPort3) annotation (points=[-30, 72; 
          -30, 88], style(color=3));
    connect(k1.OutPort1, Prod_3_1.InPort2) annotation (points=[0, 48; 0, 60; 
          -18, 60], style(color=3));
    connect(Prey_1.OutPort3, Prod_3_1.InPort1) annotation (points=[-66, 96; 
          -66, 60; -40, 60], style(color=3));
    connect(Prey_1.OutPort3, Prod_3_1.InPort3) annotation (points=[-66, 96; 
          -66, 40; -30, 40; -30, 50.5], style(color=3));
    connect(Prod_3_2.OutPort1, Death_2.InPort3) annotation (points=[188, 72; 
          188, 88], style(color=3));
    connect(k2.OutPort1, Prod_3_2.InPort2) annotation (points=[220, 48; 220, 
          60; 200, 60], style(color=3));
    connect(Prey_2.OutPort3, Prod_3_2.InPort1) annotation (points=[156, 96; 
          156, 60; 178.5, 60], style(color=3));
    connect(Prey_2.OutPort3, Prod_3_2.InPort3) annotation (points=[156, 96; 
          156, 40; 188, 40; 188, 50.5], style(color=3));
    connect(k3.OutPort1, Prod_3_3.InPort3) annotation (points=[-104, 170; 
          -89.5, 170], style(color=3));
    connect(Prey_1.OutPort5, Prod_3_3.InPort2) annotation (points=[-98, 122; 
          -106, 122; -106, 148; -80, 148; -80, 158], style(color=3));
    connect(Prod_3_3.OutPort1, Death_1.InPort1) annotation (points=[-68.5, 170
          ; -52, 170; -52, 80; -38, 80; -38, 88], style(color=3));
    connect(Prey_2.OutPort5, Prod_3_3.InPort1) annotation (points=[124, 120; 
          116, 120; 116, 190; -78, 190; -78, 179.5], style(color=3));
    connect(k4.OutPort1, Prod_3_4.InPort3) annotation (points=[216, 170; 199.5
          , 170], style(color=3));
    connect(Prod_3_4.OutPort1, Death_2.InPort1) annotation (points=[178, 172; 
          166, 172; 166, 80; 180, 80; 180, 90], style(color=3));
    connect(Prod_3_4.InPort2, Prey_2.OutPort4) annotation (points=[188, 181.5
          ; 188, 190; 158, 190; 158, 120; 157, 120], style(color=3));
    connect(Prod_3_4.InPort1, Prey_1.OutPort4) annotation (points=[188, 160; 
          188, 150; -58, 150; -58, 120; -62, 120], style(color=3));
    connect(Predator.MassInPort2, Death_P.MassOutPort1) annotation (points=[62
          , -60; 90, -60], style(color=77));
    connect(Birth_P.MassOutPort2, Predator.MassInPort1) annotation (points=[
          -10, -60; 16, -60], style(color=77));
    connect(Death_P.MassOutPort2, Sink3.MassInPort1) annotation (points=[110, 
          -60; 139, -60], style(color=77));
    connect(Birth_P.MassOutPort1, Source3.MassInPort1) annotation (points=[-32
          , -60; -60, -60], style(color=77));
    connect(k5.OutPort1, Prod_3_5.InPort3) annotation (points=[-104, -6; -89.5
          , -6], style(color=3));
    connect(Prey_1.OutPort1, Prod_3_5.InPort1) annotation (points=[-80, 96; 
          -80, 4], style(color=3));
    connect(Predator.OutPort5, Prod_3_5.InPort2) annotation (points=[22, -49; 
          12, -49; 12, -38; -78, -38; -78, -15.8], style(color=3));
    connect(Prod_3_5.OutPort1, Death_1.InPort2) annotation (points=[-68.5, -6
          ; 20, -6; 20, 80; -22, 80; -22, 88], style(color=3));
    connect(Prey_2.OutPort1, Prod_3_6.InPort1) annotation (points=[140, 96; 
          140, 4], style(color=3));
    connect(Prod_3_6.OutPort1, Death_2.InPort2) annotation (points=[149.5, -6
          ; 242, -6; 242, 80; 196, 80; 196, 88], style(color=3));
    connect(Prod_3_6.InPort3, k6.OutPort1) annotation (points=[128.5, -6; 116
          , -6], style(color=3));
    connect(Predator.OutPort4, Prod_3_6.InPort2) annotation (points=[58, -49; 
          68, -49; 68, -38; 140, -38; 140, -15.8], style(color=3));
    connect(Predator.OutPort3, Death_P.InPort1) annotation (points=[54, -74; 
          54, -98; 100, -98; 100, -82], style(color=3));
    connect(Prod_3_7.InPort3, k7.OutPort1) annotation (points=[-65.5, -126; 
          -84, -126], style(color=3));
    connect(Predator.OutPort2, Prod_3_7.InPort2) annotation (points=[24, -74; 
          24, -150; -54, -150; -54, -135.8], style(color=3));
    connect(Prod_3_7.OutPort1, Birth_P.InPort1) annotation (points=[-44.5, 
          -126; -28, -126; -28, -82], style(color=3));
    connect(Prey_1.OutPort1, Prod_3_7.InPort1) annotation (points=[-80, 98; 
          -80, 40; -160, 40; -160, -94; -54, -94; -54, -116.2], style(color=3))
      ;
    connect(Prod_3_8.OutPort1, Birth_P.InPort2) annotation (points=[74, -136; 
          -13.4, -136; -13.4, -82], style(color=3));
    connect(k8.OutPort1, Prod_3_8.InPort3) annotation (points=[118, -141; 107
          , -141; 107, -136; 96, -136], style(color=3));
    connect(Predator.OutPort1, Prod_3_8.InPort1) annotation (points=[40, -74; 
          40, -160; 84, -160; 84, -145.5], style(color=3));
    connect(Prey_2.OutPort1, Prod_3_8.InPort2) annotation (points=[140, 98; 
          140, 20; 180, 20; 180, -110; 86, -110; 86, -124], style(color=3));
  end Gilpin_Model;
end Gilpin;
