R1 = 100; R2 = 20;
L = 0.0015; C = 1E-6;
R1C = 1/(R1*C);  R2C = 1/(R2*C);
a11 = -(R1C + R2C);
A = [ a11 , 0 ; 0 , 0 ];
b = [ R1C ; 1/L ];
c = [ 1 , 0 ];
d = 0;
S = ss(A,b,c,d);
t = [ 0 : 1E-6 : 2E-3 ];
u = 10*sin(5000*t);
x0 = [ 1 ; 2 ];
y = lsim(S,u,t,x0);
plot(t,y)
grid on
return
