package PressureCooker 
  annotation (Coordsys(
      extent=[0, 0; 402, 261], 
      grid=[2, 2], 
      component=[20, 20]));
  model PressureCooker 
    parameter Real Tcover=293.15 "Temperature of cover";
    parameter Real Tbottom=393.15 "Temperature of heating element";
    parameter Real HE_bottom_water_lambda=0.35 "Thermal conductance";
    parameter Real HE_air_vapor_lambda=0.35 "Thermal conductance";
    parameter Real HE_water_air_lambda=0.2 "Thermal conductance";
    parameter Real HE_water_vapor_lambda=2.0 "Thermal conductance";
    parameter Real HE_vapor_boundair_lambda=0.025 "Thermal conductance";
    parameter Real HE_air_boundvapor_lambda=0.025 "Thermal conductance";
    parameter Real HE_vapor_boundvapor_lambda=0.08 "Thermal conductance";
    parameter Real HE_air_boundair_lambda=0.08 "Thermal conductance";
    parameter Real HE_boundair_boundvapor_lambda=0.35 "Thermal conductance";
    parameter Real HE_boundair_cover_lambda=0.25 "Thermal conductance";
    parameter Real HE_boundvapor_cover_lambda=0.25 "Thermal conductance";
    parameter Real HE_bottom_boundvapor_lambda1=0.01 "Thermal conductance";
    parameter Real HE_bottom_boundvapor_lambda2=0.03 "Thermal conductance";
    parameter Real HE_bottom_boundair_lambda1=0.01 "Thermal conductance";
    parameter Real HE_bottom_boundair_lambda2=0.03 "Thermal conductance";
    parameter Real SwitchTime=10000 
      "Time of switching from heating to cooling";
    parameter Real kRF_vapor=0.000105;
    parameter Real kRF_air=0.000105;
    Real HE_bottom_boundvapor_lambda;
    Real HE_bottom_boundair_lambda;
    Real CF_Air_V "Air volume";
    Real CF_BoundaryAir_V "Air volume in boundary layer";
    Real CF_Air_M "Air mass";
    Real CF_BoundaryAir_M "Air mass in boundary layer";
    Real CF_Vapor_V "Water vapor volume";
    Real CF_BoundaryVapor_V "Water vapor volume in boundary layer";
    Real CF_Vapor_M "Water vapor mass";
    Real CF_BoundaryVapor_M "Water vapor mass in boundary layer";
    Real CF_Water_V "Water volume";
    Boolean CF_Water_Exist "True is there is water";
    Real Mass_frac "Mass fraction";
    Real Mass_frac_boundary "Mass fraction in boundary layer";
    Real Volume_frac "Volume fraction";
    Real Volume_frac_boundary "Volume fraction in boundary layer";
    Real Vtot "Total volume";
    annotation (
      Coordsys(
        extent=[-200, -150; 220, 170], 
        grid=[2, 2], 
        component=[20, 20]), 
      Diagram(
        Text(
          extent=[-18, -112; 22, -128], 
          string="Water", 
          style(color=57)), 
        Text(
          extent=[-128, -10; -88, -26], 
          string="Air", 
          style(color=57)), 
        Text(
          extent=[94, -10; 134, -26], 
          string="Vapor", 
          style(color=57)), 
        Rectangle(extent=[-62, -74; 88, -140], style(color=57)), 
        Rectangle(extent=[-168, 30; -20, -26], style(color=57)), 
        Rectangle(extent=[-10, 56; 170, -26], style(color=57)), 
        Text(
          extent=[90, 94; 130, 78], 
          string="Vapor", 
          style(color=57)), 
        Text(
          extent=[90, 82; 130, 66], 
          string="Boundary", 
          style(color=57)), 
        Rectangle(extent=[170, 64; 54, 158], style(color=57)), 
        Rectangle(extent=[-170, 66; -50, 160], style(color=57)), 
        Text(
          extent=[-132, 94; -92, 78], 
          string="Air", 
          style(color=57)), 
        Text(
          extent=[-130, 82; -90, 66], 
          string="Boundary", 
          style(color=57)), 
        Line(points=[-100, 100; -100, 100], style(color=8)), 
        Line(points=[-72, 80; -72, 80], style(color=8)), 
        Line(points=[20, 140; 20, 140], style(color=8)), 
        Line(points=[132, 122; 132, 120], style(color=8)), 
        Line(points=[132, 140; 132, 140], style(color=8)), 
        Line(points=[-90, -110; -90, -110], style(color=8)), 
        Line(points=[174, 74; 174, 74], style(color=8)), 
        Line(points=[174, 54; 174, 54], style(color=8)), 
        Line(points=[-174, 74; -174, 74], style(color=8)), 
        Line(points=[-174, 54; -174, 54], style(color=8))), 
      Window(
        x=0.45, 
        y=0.01, 
        width=0.44, 
        height=0.65));
    ThermoBondLib.Bonds.fThermoBond B1 annotation (extent=[-110, -140; -90, 
          -120]);
    ThermoBondLib.Bonds.eThermoBond B2 annotation (extent=[-70, -140; -50, 
          -120]);
    ThermoBondLib.Passive.HE HE1 annotation (extent=[-90, -140; -70, -120]);
    ThermoBondLib.Junctions.J0p5 J0p5_1 annotation (extent=[-50, -80; -10, 
          -120]);
    ThermoBondLib.Bonds.fThermoBond B3 annotation (extent=[-10, -110; 10, -90]
        , rotation=180);
    ThermoBondLib.Junctions.J0p5 J0p5_2 annotation (extent=[10, -80; 50, -120]
      );
    ThermoBondLib.Bonds.eThermoBond B4 annotation (extent=[50, -140; 70, -120]
      );
    ThermoBondLib.Substances.Water Water1(
      epsM=7E-4, 
      S_int(start=14.77309), 
      V_int(start=0.049857665), 
      M_int(start=50)) annotation (extent=[70, -140; 90, -120]);
    ThermoBondLib.Junctions.J0p6 J0p6_1 annotation (extent=[-160, -20; -120, 
          20]);
    ThermoBondLib.Bonds.fThermoBond B5 annotation (extent=[-120, -10; -100, 10
          ], rotation=180);
    ThermoBondLib.Junctions.J0p6 J0p6_2 annotation (extent=[-100, -20; -60, 20
          ]);
    ThermoBondLib.Bonds.eThermoBond B6 annotation (extent=[-60, -10; -40, 10])
      ;
    ThermoBondLib.Substances.Air Air1(
      S_int(start=7.374476568), 
      V_int(start=0.9), 
      M_int(start=1.082873165)) annotation (extent=[-40, -10; -20, 10]);
    ThermoBondLib.Junctions.J0p5 J0p5_3 annotation (extent=[40, -20; 0, 20]);
    ThermoBondLib.Bonds.fThermoBond B7 annotation (extent=[40, -10; 60, 10], 
        rotation=180);
    ThermoBondLib.Junctions.J0p5 J0p5_4 annotation (extent=[100, -20; 60, 20])
      ;
    ThermoBondLib.Bonds.fThermoBond B8 annotation (extent=[100, -10; 120, 10])
      ;
    ThermoBondLib.Junctions.J0p5 J0p5_5 annotation (extent=[120, 20; 160, -20]
      );
    ThermoBondLib.Bonds.eThermoBond B9 annotation (extent=[70, 20; 90, 40], 
        rotation=90);
    ThermoBondLib.Substances.WaterVapor WaterVapor1(
      epsM=0.0001, 
      S(start=0.09213013), 
      V(start=0.0189028), 
      M(start=0.0127859)) annotation (extent=[70, 40; 90, 60], rotation=90);
    ThermoBondLib.Junctions.J0p6 J0p6_3 annotation (extent=[120, 80; 160, 120]
      );
    ThermoBondLib.Bonds.fThermoBond B10 annotation (extent=[100, 90; 120, 110]
        , rotation=180);
    ThermoBondLib.Junctions.J0p6 J0p6_4 annotation (extent=[60, 80; 100, 120])
      ;
    ThermoBondLib.Substances.WaterVapor WaterVapor2(
      epsM=0.00001, 
      S(start=0.001842602), 
      V(start=0.000378056), 
      M(start=0.000255718)) annotation (extent=[102, 140; 82, 160]);
    ThermoBondLib.Junctions.J0p6 J0p6_5 annotation (extent=[-160, 80; -120, 
          120]);
    ThermoBondLib.Bonds.fThermoBond B12 annotation (extent=[-120, 90; -100, 
          110]);
    ThermoBondLib.Bonds.eThermoBond B13 annotation (extent=[-142, 120; -122, 
          140], rotation=90);
    ThermoBondLib.Substances.Air Air2(
      S_int(start=0.14748953), 
      V_int(start=0.018), 
      M_int(start=0.021657463)) annotation (extent=[-142, 140; -122, 160], 
        rotation=90);
    ThermoBondLib.Bonds.fThermoBond B14 annotation (extent=[-60, -30; -40, -10
          ]);
    ThermoBondLib.Passive.PVE PVE1(G=0.01) annotation (extent=[-40, -30; -20, 
          -10]);
    ThermoBondLib.Bonds.eThermoBond B15 annotation (extent=[-20, -30; 0, -10])
      ;
    ThermoBondLib.Bonds.fThermoBond B16 annotation (extent=[-60, -50; -40, -30
          ]);
    ThermoBondLib.Passive.HE HE2 annotation (extent=[-40, -50; -20, -30]);
    ThermoBondLib.Bonds.eThermoBond B17 annotation (extent=[-20, -50; 0, -30])
      ;
    ThermoBondLib.Bonds.eThermoBond B18 annotation (extent=[-158, -40; -138, 
          -20], rotation=90);
    ThermoBondLib.Passive.PVE PVE2(G=0.01) annotation (extent=[-158, -60; -138
          , -40], rotation=90);
    ThermoBondLib.Bonds.fThermoBond B19 annotation (extent=[-158, -80; -138, 
          -60], rotation=90);
    ThermoBondLib.Bonds.eThermoBond B20 annotation (extent=[-138, -40; -118, 
          -20], rotation=90);
    ThermoBondLib.Passive.HE HE3 annotation (extent=[-138, -60; -118, -40], 
        rotation=90);
    ThermoBondLib.Bonds.fThermoBond B21 annotation (extent=[-138, -80; -118, 
          -60], rotation=90);
    ThermoBondLib.Bonds.fThermoBond B22 annotation (extent=[0, -60; 20, -40]);
    ThermoBondLib.Substances.BoilCondWater BoilCondWater1(Rb=0.1, Rc=0.1) 
      annotation (extent=[20, -60; 40, -40]);
    ThermoBondLib.Bonds.eThermoBond B23 annotation (extent=[40, -60; 60, -40])
      ;
    ThermoBondLib.Bonds.fThermoBond B24 annotation (extent=[20, -80; 40, -60])
      ;
    ThermoBondLib.Passive.HE HE4 annotation (extent=[40, -80; 60, -60]);
    ThermoBondLib.Bonds.eThermoBond B25 annotation (extent=[60, -80; 80, -60])
      ;
    ThermoBondLib.Bonds.fThermoBond B26 annotation (extent=[60, -100; 80, -80]
      );
    ThermoBondLib.Passive.PVE PVE3(G=0.01) annotation (extent=[80, -100; 100, 
          -80]);
    ThermoBondLib.Bonds.eThermoBond B27 annotation (extent=[100, -100; 120, 
          -80]);
    ThermoBondLib.Bonds.fThermoBond B28 annotation (extent=[180, 10; 200, 30]
        , rotation=-90);
    ThermoBondLib.Substances.BoilCondWater BoilCondWater2(Rb=0.00196, Rc=0.04)
       annotation (extent=[200, 10; 180, -10], rotation=-90);
    ThermoBondLib.Bonds.eThermoBond B29 annotation (extent=[180, -30; 200, -10
          ], rotation=-90);
    ThermoBondLib.Bonds.fThermoBond B30 annotation (extent=[-32, 70; -12, 90]
        , rotation=-180);
    ThermoBondLib.Passive.HE HE5 annotation (extent=[-52, 90; -32, 70]);
    ThermoBondLib.Bonds.eThermoBond B31 annotation (extent=[-72, 70; -52, 90]
        , rotation=-180);
    ThermoBondLib.Bonds.fThermoBond B32 annotation (extent=[12, 70; 32, 90]);
    ThermoBondLib.Passive.HE HE6 annotation (extent=[32, 90; 52, 70]);
    ThermoBondLib.Bonds.eThermoBond B33 annotation (extent=[52, 70; 72, 90]);
    ThermoBondLib.Bonds.fThermoBond B34 annotation (extent=[-30, 50; -10, 70]
        , rotation=-180);
    ThermoBondLib.Passive.PVE PVE4(G=0.00001) annotation (extent=[-50, 50; -30
          , 70]);
    ThermoBondLib.Bonds.eThermoBond B35 annotation (extent=[-70, 50; -50, 70]
        , rotation=-180);
    ThermoBondLib.Bonds.fThermoBond B36 annotation (extent=[12, 50; 32, 70]);
    ThermoBondLib.Passive.PVE PVE5(G=0.00001) annotation (extent=[32, 50; 52, 
          70]);
    ThermoBondLib.Bonds.eThermoBond B37 annotation (extent=[52, 50; 72, 70]);
    ThermoBondLib.Bonds.fThermoBond B38 annotation (extent=[120, 20; 140, 40]
        , rotation=90);
    ThermoBondLib.Passive.HE HE7 annotation (extent=[140, 40; 120, 60], 
        rotation=90);
    ThermoBondLib.Bonds.eThermoBond B39 annotation (extent=[120, 60; 140, 80]
        , rotation=90);
    ThermoBondLib.Bonds.fThermoBond B40 annotation (extent=[140, 20; 160, 40]
        , rotation=90);
    ThermoBondLib.Passive.RFq RFq1 annotation (extent=[140, 40; 160, 60], 
        rotation=90);
    ThermoBondLib.Bonds.eThermoBond B41 annotation (extent=[140, 60; 160, 80]
        , rotation=90);
    ThermoBondLib.Bonds.fThermoBond B42 annotation (extent=[164, 34; 184, 54]
        , rotation=90);
    ThermoBondLib.Bonds.eThermoBond B43 annotation (extent=[164, 74; 184, 94]
        , rotation=90);
    ThermoBondLib.Bonds.fThermoBond B44 annotation (extent=[-140, 20; -120, 40
          ], rotation=90);
    ThermoBondLib.Passive.HE HE8 annotation (extent=[-140, 40; -120, 60], 
        rotation=90);
    ThermoBondLib.Bonds.eThermoBond B45 annotation (extent=[-140, 60; -120, 80
          ], rotation=90);
    ThermoBondLib.Bonds.fThermoBond B46 annotation (extent=[-160, 20; -140, 40
          ], rotation=90);
    ThermoBondLib.Passive.RFq RFq2 annotation (extent=[-160, 40; -140, 60], 
        rotation=90);
    ThermoBondLib.Bonds.eThermoBond B47 annotation (extent=[-160, 60; -140, 80
          ], rotation=90);
    ThermoBondLib.Bonds.eThermoBond B48 annotation (extent=[-184, 74; -164, 94
          ], rotation=90);
    ThermoBondLib.Bonds.fThermoBond B49 annotation (extent=[-184, 34; -164, 54
          ], rotation=90);
    ThermoBondLib.Passive.PVE PVE6(G=0.01) annotation (extent=[-10, 90; 10, 
          110]);
    ThermoBondLib.Bonds.fThermoBond B50 annotation (extent=[-30, 90; -10, 110]
      );
    ThermoBondLib.Bonds.eThermoBond B51 annotation (extent=[10, 90; 30, 110]);
    ThermoBondLib.Bonds.fThermoBond B52 annotation (extent=[-30, 110; -10, 130
          ]);
    ThermoBondLib.Passive.HE HE9 annotation (extent=[-10, 130; 10, 110]);
    ThermoBondLib.Bonds.eThermoBond B53 annotation (extent=[10, 110; 30, 130])
      ;
    ThermoBondLib.Bonds.fThermoBond B54 annotation (extent=[-60, 130; -40, 150
          ]);
    ThermoBondLib.Passive.HE HE10 annotation (extent=[-40, 150; -20, 130]);
    ThermoBondLib.Bonds.eThermoBond B55 annotation (extent=[-20, 130; 0, 150])
      ;
    ThermoBondLib.Sources.SE SE2(T0=Tcover) annotation (extent=[40, 150; 60, 
          170]);
    ThermoBondLib.Bonds.eThermoBond B56 annotation (extent=[20, 130; 40, 150]
        , rotation=180);
    ThermoBondLib.Bonds.fThermoBond B57 annotation (extent=[60, 130; 80, 150]
        , rotation=180);
    ThermoBondLib.Passive.HE HE11 annotation (extent=[40, 150; 60, 130]);
    ThermoBondLib.Junctions.J0p6 J0p6_6 annotation (extent=[-100, 80; -60, 120
          ]);
    ThermoBondLib.Junctions.J0p3 J0p3_1 annotation (extent=[0, 150; 20, 130]);
    ThermoBondLib.Bonds.eThermoBond B58 annotation (extent=[20, 150; 40, 170])
      ;
    ThermoBondLib.Junctions.J0p3 J0p3_2 annotation (extent=[122, 140; 142, 160
          ]);
    ThermoBondLib.Bonds.eThermoBond B59 annotation (extent=[102, 140; 122, 160
          ], rotation=180);
    ThermoBondLib.Junctions.J0p4 J0p4_1 annotation (extent=[-130, -140; -110, 
          -120]);
    ThermoBondLib.Bonds.fThermoBond B60 annotation (extent=[-110, -120; -90, 
          -100], rotation=180);
    ThermoBondLib.Passive.HE HE12 annotation (extent=[216, -74; 196, -94], 
        rotation=-90);
    ThermoBondLib.Bonds.eThermoBond B61 annotation (extent=[196, -74; 216, -54
          ], rotation=90);
    ThermoBondLib.Bonds.fThermoBond B62 annotation (extent=[196, -114; 216, 
          -94], rotation=90);
    ThermoBondLib.Bonds.fThermoBond B11 annotation (extent=[122, 120; 142, 140
          ], rotation=-90);
    ThermoBondLib.Bonds.fThermoBond B63 annotation (extent=[-198, -116; -178, 
          -96], rotation=90);
    ThermoBondLib.Passive.HE HE13 annotation (extent=[-198, -76; -178, -96], 
        rotation=-90);
    ThermoBondLib.Bonds.eThermoBond B64 annotation (extent=[-198, -76; -178, 
          -56], rotation=90);
    ThermoBondLib.Sources.mSE mSE1 annotation (extent=[-90, -120; -70, -100]);
    ThermoBondLib.Passive.RFV RFV1(G=1E-4) annotation (extent=[164, 54; 184
          , 74], rotation=90);
    ThermoBondLib.Passive.RFV RFV2(G=1E-4) annotation (extent=[-184, 54; 
          -164, 74], rotation=90);
  equation 
    connect(B1.fThBondCon1, HE1.ThBondCon1) annotation (points=[-90, -130; 
          -90, -130], style(color=8));
    connect(HE1.ThBondCon2, B2.fThBondCon1) annotation (points=[-70, -130; -70
          , -130], style(color=8));
    connect(J0p5_2.ThBondCon1, B3.eThBondCon1) annotation (points=[10, -100; 
          10, -100], style(color=8));
    connect(B3.fThBondCon1, J0p5_1.ThBondCon2) annotation (points=[-10, -100; 
          -10, -102], style(color=8));
    connect(B2.eThBondCon1, J0p5_1.ThBondCon4) annotation (points=[-50, -130; 
          -30, -130; -30, -120], style(color=8));
    connect(J0p5_2.ThBondCon4, B4.fThBondCon1) annotation (points=[30, -120; 
          30, -130; 50, -130], style(color=8));
    connect(B4.eThBondCon1, Water1.ThBondCon1) annotation (points=[70, -130; 
          70, -130], style(color=8));
    connect(B5.fThBondCon1, J0p6_1.ThBondCon2) annotation (points=[-120, 
          1.22125e-015; -120, 0], style(color=8));
    connect(J0p6_2.ThBondCon1, B5.eThBondCon1) annotation (points=[-100, 0; 
          -100, -1.22125e-015], style(color=8));
    connect(J0p6_2.ThBondCon2, B6.fThBondCon1) annotation (points=[-58, 0; -60
          , 0], style(color=8));
    connect(B6.eThBondCon1, Air1.ThBondCon1) annotation (points=[-40, 0; -40, 
          0], style(color=8));
    connect(B7.fThBondCon1, J0p5_3.ThBondCon1) annotation (points=[40, 0; 40, 
          0], style(color=8));
    connect(J0p5_4.ThBondCon2, B7.eThBondCon1) annotation (points=[60, 0; 60, 
          0], style(color=8));
    connect(J0p5_4.ThBondCon1, B8.eThBondCon1) annotation (points=[100, 2; 100
          , 0], style(color=8));
    connect(B8.fThBondCon1, J0p5_5.ThBondCon1) annotation (points=[120, 0; 120
          , 0], style(color=8));
    connect(J0p5_4.ThBondCon4, B9.fThBondCon1) annotation (points=[80, 20; 80
          , 20], style(color=8));
    connect(B9.eThBondCon1, WaterVapor1.ThBondCon1) annotation (points=[80, 40
          ; 80, 40], style(color=8));
    connect(J0p6_3.ThBondCon1, B10.eThBondCon1) annotation (points=[120, 100; 
          120, 100], style(color=8));
    connect(B10.fThBondCon1, J0p6_4.ThBondCon2) annotation (points=[100, 100; 
          100, 100], style(color=8));
    connect(J0p6_5.ThBondCon2, B12.eThBondCon1) annotation (points=[-120, 100
          ; -120, 100], style(color=8));
    connect(B14.fThBondCon1, PVE1.ThBondCon1) annotation (points=[-40, -20; 
          -40, -20], style(color=8));
    connect(PVE1.ThBondCon2, B15.fThBondCon1) annotation (points=[-20, -20; 
          -20, -20], style(color=8));
    connect(J0p6_2.ThBondCon5, B14.eThBondCon1) annotation (points=[-72, -20; 
          -60, -20], style(color=8));
    connect(B15.eThBondCon1, J0p5_3.ThBondCon5) annotation (points=[0, -20; 12
          , -20], style(color=8));
    connect(B16.fThBondCon1, HE2.ThBondCon1) annotation (points=[-40, -40; -40
          , -40], style(color=8));
    connect(HE2.ThBondCon2, B17.fThBondCon1) annotation (points=[-20, -40; -20
          , -40], style(color=8));
    connect(J0p6_2.ThBondCon3, B16.eThBondCon1) annotation (points=[-88, -20; 
          -88, -40; -60, -40], style(color=8));
    connect(B17.eThBondCon1, J0p5_3.ThBondCon3) annotation (points=[0, -40; 28
          , -40; 28, -20], style(color=8));
    connect(B18.eThBondCon1, J0p6_1.ThBondCon3) annotation (points=[-148, -20
          ; -148, -20], style(color=8));
    connect(PVE2.ThBondCon2, B18.fThBondCon1) annotation (points=[-148, -40; 
          -148, -40], style(color=8));
    connect(B19.fThBondCon1, PVE2.ThBondCon1) annotation (points=[-148, -60; 
          -148, -60], style(color=8));
    connect(J0p5_1.ThBondCon1, B19.eThBondCon1) annotation (points=[-50, -100
          ; -148, -100; -148, -80], style(color=8));
    connect(HE3.ThBondCon2, B20.fThBondCon1) annotation (points=[-128, -40; 
          -128, -40], style(color=8));
    connect(B21.fThBondCon1, HE3.ThBondCon1) annotation (points=[-128, -60; 
          -128, -60], style(color=8));
    connect(B20.eThBondCon1, J0p6_1.ThBondCon5) annotation (points=[-128, -20
          ; -132, -20], style(color=8));
    connect(J0p5_1.ThBondCon3, B21.eThBondCon1) annotation (points=[-38, -80; 
          -128, -80], style(color=8));
    connect(B22.fThBondCon1, BoilCondWater1.ThBondCon1) annotation (points=[20
          , -50; 20, -50], style(color=8));
    connect(BoilCondWater1.ThBondCon2, B23.fThBondCon1) annotation (points=[40
          , -50; 40, -50], style(color=8));
    connect(B23.eThBondCon1, J0p5_4.ThBondCon5) annotation (points=[60, -50; 
          72, -50; 72, -20], style(color=8));
    connect(J0p5_1.ThBondCon5, B22.eThBondCon1) annotation (points=[-22, -80; 
          -22, -50; 0, -50], style(color=8));
    connect(B24.fThBondCon1, HE4.ThBondCon1) annotation (points=[40, -70; 40, 
          -70], style(color=8));
    connect(HE4.ThBondCon2, B25.fThBondCon1) annotation (points=[60, -70; 60, 
          -70], style(color=8));
    connect(B24.eThBondCon1, J0p5_2.ThBondCon3) annotation (points=[20, -70; 
          20, -80], style(color=8));
    connect(B25.eThBondCon1, J0p5_4.ThBondCon3) annotation (points=[80, -70; 
          88, -70; 88, -20], style(color=8));
    connect(PVE3.ThBondCon2, B27.fThBondCon1) annotation (points=[100, -90; 
          100, -90], style(color=8));
    connect(B26.fThBondCon1, PVE3.ThBondCon1) annotation (points=[80, -90; 80
          , -90], style(color=8));
    connect(B26.eThBondCon1, J0p5_2.ThBondCon5) annotation (points=[60, -90; 
          38, -90; 38, -80], style(color=8));
    connect(B27.eThBondCon1, J0p5_5.ThBondCon4) annotation (points=[120, -90; 
          140, -90; 140, -22], style(color=8));
    connect(J0p6_3.ThBondCon6, B28.eThBondCon1) annotation (points=[148, 122; 
          148, 130; 190, 130; 190, 30], style(color=8));
    connect(B29.eThBondCon1, J0p5_2.ThBondCon2) annotation (points=[190, -30; 
          190, -100; 50, -100], style(color=8));
    connect(HE5.ThBondCon1, B31.fThBondCon1) annotation (points=[-52, 80; -52
          , 80], style(color=8));
    connect(B30.fThBondCon1, HE5.ThBondCon2) annotation (points=[-32, 80; -32
          , 80], style(color=8));
    connect(B34.fThBondCon1, PVE4.ThBondCon2) annotation (points=[-30, 60; -30
          , 60], style(color=8));
    connect(PVE4.ThBondCon1, B35.fThBondCon1) annotation (points=[-50, 60; -50
          , 60], style(color=8));
    connect(B33.eThBondCon1, J0p6_4.ThBondCon3) annotation (points=[72, 80; 72
          , 80], style(color=8));
    connect(HE6.ThBondCon2, B33.fThBondCon1) annotation (points=[52, 80; 52, 
          80], style(color=8));
    connect(B32.fThBondCon1, HE6.ThBondCon1) annotation (points=[32, 80; 32, 
          80], style(color=8));
    connect(B37.eThBondCon1, J0p6_4.ThBondCon5) annotation (points=[72, 60; 88
          , 60; 88, 78], style(color=8));
    connect(PVE5.ThBondCon2, B37.fThBondCon1) annotation (points=[52, 60; 52, 
          60], style(color=8));
    connect(B36.fThBondCon1, PVE5.ThBondCon1) annotation (points=[32, 60; 32, 
          60], style(color=8));
    connect(J0p5_5.ThBondCon3, B38.eThBondCon1) annotation (points=[132, 20; 
          130, 20], style(color=8));
    connect(B38.fThBondCon1, HE7.ThBondCon1) annotation (points=[130, 40; 130
          , 40], style(color=8));
    connect(HE7.ThBondCon2, B39.fThBondCon1) annotation (points=[130, 60; 130
          , 60], style(color=8));
    connect(B39.eThBondCon1, J0p6_3.ThBondCon3) annotation (points=[130, 80; 
          132, 80], style(color=8));
    connect(J0p5_5.ThBondCon5, B40.eThBondCon1) annotation (points=[148, 20; 
          150, 20], style(color=8));
    connect(B40.fThBondCon1, RFq1.ThBondCon1) annotation (points=[150, 40; 150
          , 40], style(color=8));
    connect(RFq1.ThBondCon2, B41.fThBondCon1) annotation (points=[150, 60; 150
          , 60], style(color=8));
    connect(B41.eThBondCon1, J0p6_3.ThBondCon5) annotation (points=[150, 80; 
          150, 80], style(color=8));
    connect(B43.eThBondCon1, J0p6_3.ThBondCon2) annotation (points=[174, 94; 
          174, 100; 160, 100], style(color=8));
    connect(J0p5_5.ThBondCon2, B42.eThBondCon1) annotation (points=[160, 0; 
          174, 0; 174, 34], style(color=8));
    connect(J0p6_1.ThBondCon6, B44.eThBondCon1) annotation (points=[-132, 20; 
          -130, 20], style(color=8));
    connect(B44.fThBondCon1, HE8.ThBondCon1) annotation (points=[-130, 40; 
          -130, 40], style(color=8));
    connect(HE8.ThBondCon2, B45.fThBondCon1) annotation (points=[-130, 60; 
          -130, 60], style(color=8));
    connect(B45.eThBondCon1, J0p6_5.ThBondCon5) annotation (points=[-130, 80; 
          -132, 80], style(color=8));
    connect(J0p6_1.ThBondCon4, B46.eThBondCon1) annotation (points=[-148, 20; 
          -150, 20], style(color=8));
    connect(B46.fThBondCon1, RFq2.ThBondCon1) annotation (points=[-150, 40; 
          -150, 40], style(color=8));
    connect(RFq2.ThBondCon2, B47.fThBondCon1) annotation (points=[-150, 60; 
          -150, 60], style(color=8));
    connect(B47.eThBondCon1, J0p6_5.ThBondCon3) annotation (points=[-150, 80; 
          -148, 80], style(color=8));
    connect(B48.eThBondCon1, J0p6_5.ThBondCon1) annotation (points=[-174, 94; 
          -174, 100; -160, 100], style(color=8));
    connect(J0p6_1.ThBondCon1, B49.eThBondCon1) annotation (points=[-160, 0; 
          -174, 0; -174, 34], style(color=8));
    connect(B50.fThBondCon1, PVE6.ThBondCon1) annotation (points=[-10, 100; 
          -10, 100], style(color=8));
    connect(PVE6.ThBondCon2, B51.fThBondCon1) annotation (points=[10, 100; 10
          , 100], style(color=8));
    connect(B51.eThBondCon1, J0p6_4.ThBondCon1) annotation (points=[30, 100; 
          60, 100], style(color=8));
    connect(B52.fThBondCon1, HE9.ThBondCon1) annotation (points=[-10, 120; -10
          , 120], style(color=8));
    connect(HE9.ThBondCon2, B53.fThBondCon1) annotation (points=[10, 120; 10, 
          120], style(color=8));
    connect(B53.eThBondCon1, J0p6_4.ThBondCon4) annotation (points=[30, 120; 
          72, 120], style(color=8));
    connect(B54.fThBondCon1, HE10.ThBondCon1) annotation (points=[-40, 140; 
          -40, 140], style(color=8));
    connect(HE10.ThBondCon2, B55.fThBondCon1) annotation (points=[-20, 140; 
          -20, 140], style(color=8));
    connect(J0p6_4.ThBondCon6, B57.eThBondCon1) annotation (points=[88, 120; 
          88, 140; 80, 140], style(color=8));
    connect(B57.fThBondCon1, HE11.ThBondCon2) annotation (points=[60, 140; 60
          , 140], style(color=8));
    connect(HE11.ThBondCon1, B56.fThBondCon1) annotation (points=[40, 140; 40
          , 140], style(color=8));
    connect(B12.fThBondCon1, J0p6_6.ThBondCon1) annotation (points=[-100, 100
          ; -100, 100], style(color=8));
    connect(B31.eThBondCon1, J0p6_6.ThBondCon5) annotation (points=[-72, 80; 
          -72, 80], style(color=8));
    connect(B35.eThBondCon1, J0p6_6.ThBondCon3) annotation (points=[-70, 60; 
          -88, 60; -88, 80], style(color=8));
    connect(J0p6_6.ThBondCon2, B50.eThBondCon1) annotation (points=[-60, 100; 
          -30, 100], style(color=8));
    connect(J0p6_6.ThBondCon6, B52.eThBondCon1) annotation (points=[-72, 120; 
          -30, 120], style(color=8));
    connect(B56.eThBondCon1, J0p3_1.ThBondCon2) annotation (points=[20, 140; 
          20, 140], style(color=8));
    connect(B55.eThBondCon1, J0p3_1.ThBondCon1) annotation (points=[0, 140; 0
          , 140], style(color=8));
    connect(J0p6_6.ThBondCon4, B54.eThBondCon1) annotation (points=[-88, 120; 
          -88, 140; -60, 140], style(color=8));
    connect(B58.eThBondCon1, SE2.ThBondCon1) annotation (points=[40, 160; 40, 
          160], style(color=8));
    connect(J0p3_1.ThBondCon3, B58.fThBondCon1) annotation (points=[10, 150; 
          10, 160; 20, 160], style(color=8));
    connect(J0p6_5.ThBondCon6, B13.fThBondCon1) annotation (points=[-132, 120
          ; -132, 120], style(color=8));
    connect(B13.eThBondCon1, Air2.ThBondCon1) annotation (points=[-132, 140; 
          -132, 140], style(color=8));
    connect(J0p3_2.ThBondCon1, B59.fThBondCon1) annotation (points=[122, 150; 
          122, 150], style(color=8));
    connect(B59.eThBondCon1, WaterVapor2.ThBondCon1) annotation (points=[102, 
          150; 102, 150], style(color=8));
    connect(J0p4_1.ThBondCon2, B1.eThBondCon1) annotation (points=[-110, -130
          ; -110, -130], style(color=8));
    connect(B60.fThBondCon1, J0p4_1.ThBondCon4) annotation (points=[-110, -110
          ; -120, -110; -120, -120], style(color=8));
    connect(HE12.ThBondCon2, B61.fThBondCon1) annotation (points=[206, -74; 
          206, -74], style(color=8));
    connect(B62.fThBondCon1, HE12.ThBondCon1) annotation (points=[206, -94; 
          206, -94], style(color=8));
    connect(J0p4_1.ThBondCon3, B62.eThBondCon1) annotation (points=[-120, -140
          ; -120, -148; 206, -148; 206, -114], style(color=8));
    connect(B61.eThBondCon1, J0p3_2.ThBondCon2) annotation (points=[206, -54; 
          206, 150; 142, 150], style(color=8));
    connect(J0p3_2.ThBondCon3, B11.eThBondCon1) annotation (points=[132, 140; 
          132, 140], style(color=8));
    connect(B11.fThBondCon1, J0p6_3.ThBondCon4) annotation (points=[132, 120; 
          132, 120], style(color=8));
    connect(B63.fThBondCon1, HE13.ThBondCon1) annotation (points=[-188, -96; 
          -188, -96], style(color=8));
    connect(HE13.ThBondCon2, B64.fThBondCon1) annotation (points=[-188, -76; 
          -188, -76], style(color=8));
    connect(J0p4_1.ThBondCon1, B63.eThBondCon1) annotation (points=[-130, -130
          ; -188, -130; -188, -116], style(color=8));
    connect(B64.eThBondCon1, J0p6_5.ThBondCon4) annotation (points=[-188, -56
          ; -188, 120; -148, 120], style(color=8));
    connect(J0p6_2.ThBondCon4, B34.eThBondCon1) annotation (points=[-88, 20; 
          -88, 46; -6, 46; -6, 60; -10, 60], style(color=8));
    connect(J0p6_2.ThBondCon6, B32.eThBondCon1) annotation (points=[-72, 20; 
          -72, 42; 4, 42; 4, 80; 12, 80], style(color=8));
    connect(J0p5_3.ThBondCon2, B30.eThBondCon1) annotation (points=[0, 0; 0, 
          80; -12, 80], style(color=8));
    connect(J0p5_3.ThBondCon4, B36.eThBondCon1) annotation (points=[20, 20; 20
          , 42; 8, 42; 8, 60; 12, 60], style(color=8));
    connect(mSE1.ThBondCon1, B60.eThBondCon1) annotation (points=[-90, -110; 
          -90, -110], style(color=8));
    connect(B42.fThBondCon1, RFV1.ThBondCon1) annotation (points=[174, 54; 
          174, 54], style(color=8));
    connect(RFV1.ThBondCon2, B43.fThBondCon1) annotation (points=[174, 74; 
          174, 74], style(color=8));
    connect(B49.fThBondCon1, RFV2.ThBondCon1) annotation (points=[-174, 54; 
          -174, 54], style(color=8));
    connect(RFV2.ThBondCon2, B48.fThBondCon1) annotation (points=[-174, 74; 
          -174, 74], style(color=8));
    connect(B28.fThBondCon1, BoilCondWater2.ThBondCon2) annotation (points=[
          190, 10; 190, 10], style(color=8));
    connect(BoilCondWater2.ThBondCon1, B29.fThBondCon1) annotation (points=[
          190, -10; 190, -10], style(color=8));
    CF_Air_V = Air1.V;
    CF_BoundaryAir_V = Air2.V;
    CF_Air_M = Air1.M;
    CF_BoundaryAir_M = Air2.M;
    CF_Vapor_V = WaterVapor1.V;
    CF_BoundaryVapor_V = WaterVapor2.V;
    CF_Vapor_M = WaterVapor1.M;
    CF_BoundaryVapor_M = WaterVapor2.M;
    CF_Water_V = Water1.V;
    CF_Water_Exist = Water1.Exist;
    mSE1.s = 293.15 + (Tbottom - 293.15)*(1 - exp(-time));
    HE1.s = if time < SwitchTime then HE_bottom_water_lambda else 0;
    HE2.s = HE_air_vapor_lambda;
    HE3.s = HE_water_air_lambda*CF_Air_V/(CF_Vapor_V + CF_Air_V);
    HE4.s = HE_water_vapor_lambda*CF_Vapor_V/(CF_Vapor_V + CF_Air_V);
    HE5.s = HE_vapor_boundair_lambda;
    HE6.s = HE_air_boundvapor_lambda;
    HE7.s = HE_vapor_boundvapor_lambda;
    HE8.s = HE_air_boundair_lambda;
    HE9.s = HE_boundair_boundvapor_lambda;
    HE10.s = if time < SwitchTime then 0 else HE_boundair_cover_lambda;
    HE11.s = if time < SwitchTime then 0 else HE_boundvapor_cover_lambda;
    HE_bottom_boundvapor_lambda = if CF_Water_Exist then 
      HE_bottom_boundvapor_lambda1 else HE_bottom_boundvapor_lambda2;
    HE12.s = if time < SwitchTime then HE_bottom_boundvapor_lambda else 0;
    HE_bottom_boundair_lambda = if CF_Water_Exist then 
      HE_bottom_boundair_lambda1 else HE_bottom_boundair_lambda2;
    HE13.s = if time < SwitchTime then HE_bottom_boundair_lambda else 0;
    Mass_frac = CF_Vapor_M/(CF_Vapor_M + CF_Air_M);
    Mass_frac_boundary = CF_BoundaryVapor_M/(CF_BoundaryVapor_M + 
      CF_BoundaryAir_M);
    RFq1.s = kRF_vapor*(Mass_frac - Mass_frac_boundary);
    RFq2.s = kRF_air*(Mass_frac_boundary - Mass_frac);
    BoilCondWater1.Vgas = CF_Vapor_V + CF_Air_V;
    BoilCondWater2.Vgas = CF_BoundaryVapor_V + CF_BoundaryAir_V;
    Volume_frac = CF_Vapor_V/(CF_Vapor_V + CF_Air_V);
    Volume_frac_boundary = CF_BoundaryVapor_V/(CF_BoundaryVapor_V + 
      CF_BoundaryAir_V);
    Vtot = CF_Water_V + CF_Vapor_V + CF_BoundaryVapor_V + CF_Air_V + 
      CF_BoundaryAir_V;
  end PressureCooker;
end PressureCooker;
