# -*- coding: utf-8 -*-
"""
Created on Wed Oct 15 08:39:08 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""
try:
    import networkx as nx
except ImportError:
    raise ImportError("bondGraph requires networkx")

try:
    import matplotlib.pyplot as plt
except ImportError:
    raise ImportError("bondGraph requires matplotlib")

try:
    import numpy as np
except ImportError:
    raise ImportError("bondGraph requires numpy")

try:
    import sympy as sym
except ImportError:
    raise ImportError("bondGraph requires numpy")

from BondGraphs import graphClass as grph

#------------------------------------------------------------

fnameData = 'RLC_Data.dat'
fnameBGS  = 'RLC_BGS.bgs'

## Setting parameters

C = 0.5 #<- Capacitance 0.5 F
L = 1.5 #<- Inductance 1.5 H
R = 100 #<- Resistance 100 Ohm
Va = 10 #<- Voltage source amplitude 10 V
f = 10  #<- Voltage source frequency 10 Hz

omega = 2*np.pi/f

t = np.linspace(0,10,200)
#-------------------------------------
V = Va * np.sin(omega*t) #<- Voltage source function
plt.plot(t,V)

#-------------------------------------
# Bond Connectivity Map
#-------------------------------------
bonds = [(2,1),
         (3,2),
         (4,2),
         (5,2)]

class Element(object):
    def __init__(self):
        self.etype = ''
        self.exprtype = ''
        self.mvars = sym('t')
        self.expr = None
        self.x0 = 0
        self.phx = 1
        self.nn = True

elements = [Element()]

#-------------------------------------
# Elements Characteristics
#-------------------------------------
elements[1].etype = 'MSE'
elements[1].exprtype = 'NMC'
elements[1].mvars = sym('t')
elements[1].expr = V

elements[2].etype = '1'

elements[3].etype = 'C'
elements[3].exprtype = 'NE'
elements[3].x0 = 0
elements[3].expr = V
elements[3].phx = 1
elements[3].nn = True

elements[4].etype = 'I'
elements[4].exprtype = 'CC'
elements[4].x0 = 0
elements[4].expr = 1/L
elements[4].phx = 2
elements[4].nn = True

elements[5].etype = 'R'
elements[5].exprtype = 'CC'
elements[5].expr = R

## Storing runopts and BGsystem

# save(fnameBGSD,'runopts','bgs')

#------------------------------------------------------------
#------------------------------------------------------------


graph = { "a" : ["c"],
          "b" : ["c", "e"],
          "c" : ["a", "b", "d", "e"],
          "d" : ["c"],
          "e" : ["c", "b"],
          "f" : []
        }

g = { "a" : ["d"],
      "b" : ["c"],
      "c" : ["b", "c", "d", "e"],
      "d" : ["a", "c"],
      "e" : ["c"],
      "f" : []
    }
    
h = { "a" : ["d","f"],
       "b" : ["c","b"],
       "c" : ["b", "c", "d", "e"],
       "d" : ["a", "c"],
       "e" : ["c"],
       "f" : ["a"]
    }

complete_graph = { 
    "a" : ["b","c"],
    "b" : ["a","c"],
    "c" : ["a","b"]
}

isolated_graph = { 
    "a" : [],
    "b" : [],
    "c" : []
}

#------------------------------------------------------------

graph0 = grph.Graph(graph)
print("graph density of complete_graph:")
print(graph0.density())
print 'Edges of graph'
print(graph0.edges())
#->|[('a', 'c'), ('c', 'a'), ('c', 'b'), ('c', 'd'), ('c', 'e'), ('b', 'c'), ('b', 'e'), ('e', 'c'), ('e', 'b'), ('d', 'c')]

print 'Isolated nodes on graph'
print(graph0.find_isolated_vertices())
#->|

   
graph1 = grph.Graph(complete_graph)
print("graph density of complete_graph:")
print(graph1.density())

graph2 = grph.Graph(isolated_graph)
print("graph density of isolated_graph:")
print(graph2.density())

graph3 = grph.Graph(g)    
print("graph density of g:")
print(graph3.density())

print("Vertices of graph:")
print(graph3.vertices())

print("Edges of graph:")
print(graph3.edges())

print("Add vertex:")
graph3.add_vertex("z")

print("Vertices of graph:")
print(graph3.vertices())
 
print("Add an edge:")
graph3.add_edge({"a","z"})

print("Vertices of graph:")
print(graph3.vertices())

print("Edges of graph:")
print(graph3.edges())

print('Adding an edge {"x","y"} with new vertices:')
graph3.add_edge({"x","y"})
print("Vertices of graph:")
print(graph3.vertices())
print("Edges of graph:")
print(graph3.edges())

print ' ... done!'

#------------------------------------------------------------

#g = Bondgraph(' single mass/spring/damper chain ') 
#c = g.addNode(Capacitor(1.96, ’spring’))
#r = g.addNode(Resistor(0.02, ’damper’))
#i = g.addNode(Inertia(0.002, ’mass’))
#j = g.addNode(CommonFlowJunction()) 
#s = g.addNode(EffortSource( ’E’))
#
#g.bondNodes(j , c) 
#g.bondNodes(j , r) 
#g.bondNodes(j , i) 
#g.bondNodes(s, j)
#g.assignCausality() 
#g.numberElements()
#
#print StateSpace(g.equations ())

print ' ... done!'

