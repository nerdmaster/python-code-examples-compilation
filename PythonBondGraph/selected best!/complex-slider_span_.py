# -*- coding: utf-8 -*-
"""
Created on Fri Oct 10 00:45:08 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""

import random
import numpy as np
from numpy.random import rand
import matplotlib.animation as animation
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure, show
from matplotlib.lines import Line2D
from matplotlib.collections import RegularPolyCollection
from matplotlib.widgets import Slider,       \
                               Button,       \
                               RadioButtons, \
                               SpanSelector
plt.ion()
#---------------------------------------------------------
#>>>--------------------------------------------------<<<#
class Scope:
    def __init__(self, ax, maxt=2, dt=0.02):
        self.ax = ax
        self.dt = dt
        self.maxt = maxt
        self.tdata = [0]
        self.ydata = [0]
        self.line = Line2D(self.tdata, self.ydata)
        self.ax.add_line(self.line)
        self.ax.set_ylim(-.1, 1.1)
        self.ax.set_xlim(0, self.maxt)
#>>>--------------------------------------------------<<<#
    def update(self, y):
        lastt = self.tdata[-1]
        if lastt > self.tdata[0] + self.maxt: # reset the arrays
            self.tdata = [self.tdata[-1]]
            self.ydata = [self.ydata[-1]]
            self.ax.set_xlim(self.tdata[0], self.tdata[0] + self.maxt)
            self.ax.figure.canvas.draw()

        t = self.tdata[-1] + self.dt
        self.tdata.append(t)
        self.ydata.append(y)
        self.line.set_data(self.tdata, self.ydata)
        return self.line,
#>>>--------------------------------------------------<<<#
def emitter(p=0.03):
    'return a random value with probability p, else 0'
    while True:
        v = np.random.rand(1)
        if v > p:
            yield 0.
        else:
            yield np.random.rand(1)
#>>>--------------------------------------------------<<<#
def onselect(xmin, xmax):
    indmin, indmax = np.searchsorted(x, (xmin, xmax))
    indmax = min(len(x)-1, indmax)

    thisx = x[indmin:indmax]
    thisy = y[indmin:indmax]
    line2.set_data(thisx, thisy)
    ax2.set_xlim(thisx[0], thisx[-1])
    ax2.set_ylim(thisy.min(), thisy.max())
    fig.canvas.draw()
#>>>--------------------------------------------------<<<#
def onpress(event):
    """
    press 'a' to add a random point from the collection, 'd' to delete one
    """
    if event.key=='a':
        x,y = rand(2)
        color = cm.jet(rand())
        offsets.append((x,y))
        facecolors.append(color)
        collection.set_offsets(offsets)
        collection.set_facecolors(facecolors)
        fig.canvas.draw()
    elif event.key=='d':
        N = len(offsets)
        if N>0:
            ind = random.randint(0,N-1)
            offsets.pop(ind)
            facecolors.pop(ind)
            collection.set_offsets(offsets)
            collection.set_facecolors(facecolors)
            fig.canvas.draw()
    elif event.key=='q':
        print ' ... closing ...'
        plt.close()
        print ' ... quiting ...'
        exit
    else:
        print 'invalid key [%s]' % event.key
#>>>--------------------------------------------------<<<#
def handle_close(evt):
    print('Closed Figure!')
#>>>--------------------------------------------------<<<#
def update(val):
    amp = samp.val
    freq = sfreq.val
    l.set_ydata(amp*np.sin(2*np.pi*freq*t))
    plt.draw()
#>>>--------------------------------------------------<<<#
def reset(event):
    sfreq.reset()
    samp.reset()
#>>>--------------------------------------------------<<<#
def colorfunc(label):
    l.set_color(label)
    plt.draw()
#>>>--------------------------------------------------<<<#

#>>>--------------------------------------------------<<<#


#---------------------------------------------------------

#----------------------------------------------------------

fig1 = plt.figure(1,figsize=(8,6))
fig1.canvas.mpl_connect('close_event', handle_close)

ax1 = fig1.add_subplot(211, axisbg='#FFFFCC')

x = np.arange(0.0, 5.0, 0.01)
y = np.sin(2*np.pi*x) + 0.5*np.random.randn(len(x))

ax1.plot(x, y, '-')
ax1.set_ylim(-2,2)
ax1.set_title('Press left mouse button and drag to test')

ax2 = fig1.add_subplot(212, axisbg='#FFFFCC')
line2, = ax2.plot(x, y, '-')

# set useblit True on gtkagg for enhanced performance
span = SpanSelector(ax1, onselect, 'horizontal', useblit=True,
                    rectprops=dict(alpha=0.5, facecolor='red') )

#----------------------------------------------------------

#----------------------------------------------------------

x = np.arange(0.0, 5.0, 0.01)
y = np.sin(2*np.pi*x) + 0.5*np.random.randn(len(x))

t = np.arange(0.0, 1.0, 0.001)
a0 = 5
f0 = 3
s = a0*np.sin(2*np.pi*f0*t)
l, = plt.plot(t,s, lw=2, color='red')

fig = plt.figure(3,figsize=(8,6))
fig.canvas.mpl_connect('close_event', handle_close)

ax = plt.subplot(111)
plt.subplots_adjust(left=0.25, bottom=0.25)
plt.axis([0, 1, -10, 10])
axcolor = 'lightgoldenrodyellow'
axfreq = plt.axes([0.25, 0.1, 0.65, 0.03], axisbg=axcolor)
axamp  = plt.axes([0.25, 0.15, 0.65, 0.03], axisbg=axcolor)

sfreq = Slider(axfreq, 'Freq', 0.1, 30.0, valinit=f0)
samp = Slider(axamp, 'Amp', 0.1, 10.0, valinit=a0)

sfreq.on_changed(update)
samp.on_changed(update)

resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
button.on_clicked(reset)

rax = plt.axes([0.025, 0.5, 0.15, 0.15], axisbg=axcolor)
radio = RadioButtons(rax, ('red', 'blue', 'green'), active=0)
radio.on_clicked(colorfunc)

#----------------------------------------------------------

#----------------------------------------------------------

fig3 = plt.figure(3)
fig3.canvas.mpl_connect('close_event', handle_close)

ax3 = fig.add_subplot(111)
scope = Scope(ax3)

# pass a generator in "emitter" to produce data for the update func
ani = animation.FuncAnimation(fig3, scope.update, emitter, interval=10,
    blit=True)

#----------------------------------------------------------

#----------------------------------------------------------

fig4 = figure()
ax4 = fig4.add_subplot(111, xlim=(0,1), ylim=(0,1), autoscale_on=False)
ax4.set_title("Press 'a' to add a point, 'd' to delete one")
# a single point
offsets = [(0.5,0.5)]
facecolors = [cm.jet(0.5)]

collection = RegularPolyCollection(
    #fig.dpi,
    5, # a pentagon
    rotation=0,
    sizes=(50,),
    facecolors = facecolors,
    edgecolors = 'black',
    linewidths = (1,),
    offsets = offsets,
    transOffset = ax.transData,
    )

ax4.add_collection(collection)

fig4.canvas.mpl_connect('key_press_event', onpress)

#----------------------------------------------------------

#----------------------------------------------------------

show()

#plt.show()

