# -*- coding: utf-8 -*-
"""
Created on Thu Oct  9 16:15:53 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                        |
+------------------------------------------------------+
"""

import numpy as np
import matplotlib.pyplot as plt

def do_sine(x):
    z = x
    import pdb; pdb.set_trace()
    return np.sin(z)
    
x = np.arange(0, 2*np.pi, 0.01)
y = do_sine(x)
plt.plot(x,y)
plt.show()

    