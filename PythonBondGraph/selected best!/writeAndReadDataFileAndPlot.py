# writeAndReadDataFileAndPlot.py
import numpy as np
import pylab as pl

################################################################################

dataFileName = 'testdata.txt'

# Let's make 2 arrays (x, y) which we will write to a file
# x is an array containing numbers 0 to 10, with intervals of 1
x = np.arange(0.0, 10., 1.)

# y is an array containing the values in x, squared
y = x*x

print 'x = ', x
print 'y = ', y

# Now open a file to write the data to
print '\n:: Opening data file ...'
# 'w' means open for 'writing'
file = open(dataFileName, 'w')

print '\n:: Writing to data file ...'
# loop over each line you want to write to file
for i in range(len(x)):
    # make a string for each line you want to write
    # '\t' means 'tab'
    # '\n' means 'newline'
    # 'str()' means you are converting the quantity in brackets to a string type
    txt = str(x[i]) + '\t' + str(y[i]) + ' \n'
    # write the txt to the file
    file.write(txt)

print '\n:: Opening data file ...'
# Close your file
file.close()

print 'Data written into: ', dataFileName

################################################################################

# Use numpy to load the data contained in the file
# 'testdata.txt' (just written above)
# into a 2-D array called data
print '\n:: Opening data file and reading ...'
data = np.loadtxt(dataFileName)

# plot the first column as x, and second column as y
pl.plot(data[:,0], data[:,1], 'ro')

# give plot a title
theTitle = 'Plot of y vs. x (data read from file: %s)' % dataFileName
pl.title(theTitle)

# make axis labels
pl.xlabel('x')
pl.ylabel('y')
pl.xlim(0.0, 10.)
print '\n:: Plotting ...'

pl.show()

print '\n:: ... done!'
