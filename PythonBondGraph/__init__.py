# -*- coding: utf-8 -*-
"""
Created on Fri Oct 17 12:09:50 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""

"""
This is an object-oriented BondGraph library.

A procedural interface is provided,
at your terminal, followed by::

    import PyBondGraph as pbg

For the most part, direct use of the 
object-oriented library is encouraged 
when programming.

Modules include:

    :mod:`PyBondGraph.graph`
        defines the 
        :class:`~PyBondGraph.Graph` class.  
        Most PyBondGraph commands are wrappers for 
        graph methods.

    :mod:`PyBondGraph.bond`
        defines the 
        :class:`~PyBondGraph.bond.Bond` class.

    :mod:`PyBondGraph.dsc`
        defines the 
        :class:`~PyBondGraph.bond.Bond` base class 
        for all classes  related to
        dynamic systems and control things.

    :mod:`PyBondGraph.text`
        defines the :class:`~PyBondGraph.text.Text`,
        :class:`~PyBondGraph.text.TextWithDash`, and
        :class:`~PyBondGraph.text.Annotate` classes

    :mod:`PyBondGraph.image`
        defines the :class:`~PyBondGraph.image.AxesImage` and
        :class:`~PyBondGraph.image.FigureImage` classes

    :mod:`PyBondGraph.collections`
        classes for efficient drawing of groups of lines or polygons

    :mod:`PyBondGraph.colors`
        classes for interpreting color specifications and for making
        colormaps

    :mod:`PyBondGraph.backends`
        a subpackage with modules for various gui libraries and output
        formats

The base PyBondGraph namespace includes:

    :data:`~PyBondGraph.rcParams`
        a global dictionary of default configuration settings.  
        It is initialized by code which may be overridded by a 
        PyBondGraphrc file.

    :func:`~PyBondGraph.rc`
        a function for setting groups of rcParams values

    :func:`~PyBondGraph.use`
        a function for setting the PyBondGraph backend.  
        If used, this function must be called immediately after
        importing PyBondGraph for the first time.

PyBondGraph was initially written by 
Benito R. Fernandez and his students in the class
ME-369P - Application Programming for Engineers (2014) 
and is now developed and maintained by a host of others.

Occasionally the internal documentation (python docstrings) 
will refer to MATLAB&reg;, a registered trademark of The MathWorks, Inc.
and Dymola&ref;, a registered trademark of  TBA.

"""
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import sys
import distutils.version
from pyBondGraph_requirements import satisfy_requirements as test_reqs

__version__  = '0.0.1'
__version__numpy__ = '1.9.0'        # minimum required numpy version
__version__sympy__ = '0.7.5'        # minimum required sympy version
__version__scipy__ = '0.14.0'       # minimum required scipy
__version__matplotlib__ = u'1.4.0'  # minimum required matplotlib
__version__PyBondGraph__ = '0.0.1'  # minimum required PyBondGraph

#----------------------------
test_reqs()
#----------------------------

try:
    import PyBondGraph # >= u'1.4.0'
    if PyBondGraph.__version__ < u'1.4.0':
        ImportError("pyBondGraph requires PyBondGraph >= u'1.4.0'")
except ImportError:
    raise ImportError("pyBondGraph requires PyBondGraph")

try:
    import numpy
except ImportError:
    raise ImportError("pyBondGraph requires numpy")

try:
    import sympy
except ImportError:
    raise ImportError("pyBondGraph requires sympy")

try:
    import scipy
except ImportError:
    raise ImportError("pyBondGraph requires scipy")

try:
    import matplotlib
except ImportError:
    raise ImportError("pyBondGraph requires matplotlib")

__version__numpy__ = numpy.__version__
__version__sympy__ = sympy.__version__
__version__scipy__ = scipy.__version__
__version__matplotlib__ = matplotlib.__version__
__version__PyBondGraph__ = PyBondGraph.__version__
