# -*- coding: utf-8 -*-
"""
Created on Fri Oct 24 08:54:00 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""


"""
Base class for undirected graphs.

A Graph stores nodes and edges with optional data, or attributes.

Graphs hold undirected edges.  Self loops are allowed but multiple
(parallel) edges are not.

Nodes can be arbitrary (hashable) Python objects with optional
key/value attributes.

Edges are represented as links between nodes with optional
key/value attributes.

Parameters
----------
data : input graph
    Data to initialize graph.  If data=None (default) an empty
    graph is created.  The data can be an edge list, or any
    NetworkX graph object.  If the corresponding optional Python
    packages are installed the data can also be a NumPy matrix
    or 2d ndarray, a SciPy sparse matrix, or a PyGraphviz graph.
attr : keyword arguments, optional (default= no attributes)
    Attributes to add to graph as key=value pairs.

See Also
--------
DiGraph
MultiGraph
MultiDiGraph

Examples
--------
Create an empty graph structure (a "null graph") with no nodes and
no edges.
"""
G = nx.Graph()
"""
G can be grown in several ways.

**Nodes:**

Add one node at a time:
"""
G.add_node(1)
"""
Add the nodes from any container (a list, dict, set or
even the lines from a file or the nodes from another graph).
"""
G.add_nodes_from([2,3])
G.add_nodes_from(range(100,110))
H=nx.Graph()
H.add_path([0,1,2,3,4,5,6,7,8,9])
G.add_nodes_from(H)
"""
In addition to strings and integers any hashable Python object
(except None) can represent a node, e.g. a customized node object,
or even another Graph.
"""
G.add_node(H)
"""
**Edges:**

G can also be grown by adding edges.

Add one edge,
"""
G.add_edge(1, 2)
"""
a list of edges,
"""
G.add_edges_from([(1,2),(1,3)])
"""
or a collection of edges,
"""
G.add_edges_from(H.edges())
"""
If some edges connect nodes not yet in the graph, the nodes
are added automatically.  There are no errors when adding
nodes or edges that already exist.

**Attributes:**

Each graph, node, and edge can hold key/value attribute pairs
in an associated attribute dictionary (the keys must be hashable).
By default these are empty, but can be added or changed using
add_edge, add_node or direct manipulation of the attribute
dictionaries named graph, node and edge respectively.
"""
G = nx.Graph(day="Friday")
G.graph
"""
{'day': 'Friday'}

Add node attributes using add_node(), add_nodes_from() or G.node

"""
G.add_node(1, time='5pm')
G.add_nodes_from([3], time='2pm')
G.node[1]
{'time': '5pm'}
G.node[1]['room'] = 714
del G.node[1]['room'] # remove attribute
"""
G.nodes(data=True)
"""
[(1, {'time': '5pm'}), (3, {'time': '2pm'})]

'''Warning: adding a node to G.node does not add it to the graph.

Add edge attributes using add_edge(), add_edges_from(), subscript
notation, or G.edge.
'''

G.add_edge(1, 2, weight=4.7 )
G.add_edges_from([(3,4),(4,5)], color='red')
G.add_edges_from([(1,2,{'color':'blue'}), (2,3,{'weight':8})])
G[1][2]['weight'] = 4.7
G.edge[1][2]['weight'] = 4
"""

**Shortcuts:**

Many common graph features allow python syntax to speed reporting.

"""
1 in G     # check if node in graph
"""
True
"""
[n for n in G if n<3]   # iterate through nodes
"""
[1, 2]
"""
len(G)  # number of nodes in graph
"""
5

The fastest way to traverse all edges of a graph is via
adjacency_iter(), but the edges() method is often more convenient.

"""
for n,nbrsdict in G.adjacency_iter():
     for nbr,eattr in nbrsdict.items():
        if 'weight' in eattr:
            (n,nbr,eattr['weight'])

"""
(1, 2, 4)
(2, 1, 4)
(2, 3, 8)
(3, 2, 8)
>>> [ (u,v,edata['weight']) for u,v,edata in G.edges(data=True) if 'weight' in edata ]
[(1, 2, 4), (2, 3, 8)]

**Reporting:**

Simple graph information is obtained using methods.
Iterator versions of many reporting methods exist for efficiency.
Methods exist for reporting nodes(), edges(), neighbors() and degree()
as well as the number of nodes and edges.

For details on these and other miscellaneous methods, see below.
"""