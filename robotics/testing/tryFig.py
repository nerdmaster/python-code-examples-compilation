# -*- coding: utf-8 -*-
"""
Created on Tue Oct  7 20:53:10 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                        |
+------------------------------------------------------+
"""

#!/usr/bin/python
import pylab as plb
import matplotlib.pyplot as plt


plt.ion()     # turns on interactive mode

fig1=plt.figure(1)
ax = fig1.add_subplot(1,1,1)
h = ax.plot([10,2,3],[4,25,6],'bo-')
text(3, 3, 'Here', bbox=dict(facecolor='red', alpha=0.5))
ax.figure.canvas.draw()

#fig1.show()  # this does not show a figure if uncommented
#fig1.show()
for i in range(50):
    h[0].set_data([i,i+1,i-1],[i,-i,2-i])
    ax.figure.canvas.draw()
    #raw_input(('[' + str(i) + '] Press Enter ...'))
    plt.pause(0.5)
    

print "did it draw the first figure?"
raw_input('Press Enter to continue...')

fig2=plt.figure(2)
bx = fig2.add_subplot(1,1,1)
bx.plot([1,2,3],[4,5,6],'ro-')
text(2, 3, 'Again', fontsize=12)


fig1.draw()  # this does not show a figure if uncommented
raw_input('Press Enter to continue...')

plt.ion()     # turns on interactive mode
plt.show()    # now this should be non-blocking

print "did it draw the second figure?"
raw_input('Press Enter to continue...')


for i in range(100):
    h[0].set_data([i,i+1,i-1],[i,-i,2*i])
    ax.figure.canvas.draw()

print "doing something else now"