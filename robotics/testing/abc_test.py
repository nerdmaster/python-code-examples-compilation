# -*- coding: utf-8 -*-
"""
Created on Tue Oct  7 15:19:40 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                        |
+------------------------------------------------------+
"""

from abc import ABCMeta, abstractmethod

class Animal:
    __metaclass__ = ABCMeta

    @abstractmethod
    def say_something(self):
          return "I'm an animal!"

class Cat(Animal):
    def say_something(self):
        return "Miauuu!"

# a = Animal()
#
#Traceback (most recent call last):
#  File "/Users/benito/.../testing/abc_test.py", 
#    line 25, in <module>
#        a = Animal()
#TypeError: Can't instantiate abstract class Animal 
#           with abstract methods say_something

class Cat(Animal):
    def say_something(self):
        s = super(Cat, self).say_something()
        return "%s - %s" % (s, "Miauuu")
        
c = Cat()
c.say_something()
#-> "I'm an animal! - Miauuu"