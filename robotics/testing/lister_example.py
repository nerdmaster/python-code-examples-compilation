# -*- coding: utf-8 -*-
"""
Created on Tue Oct  7 15:01:34 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                        |
+------------------------------------------------------+
"""
# from:
# https://thenewcircle.com/static/bookshelf/python_fundamentals_tutorial/oop.html

class Lister(object):
    def __init__(self, *args):
        self.items = tuple(args)
    def __iter__(self):
        return (i for i in self.items)


l = Lister('a', 'b', 'c')

for letter in l:
    print letter,

print '\n'

# a b c

class Lister2(object):
    def __init__(self, *args):
        self.items = tuple(args)
    def __iter__(self):
        for i in self.items:            
            yield i
            
l2 = Lister2('a', 'b', 'c')

for letter in l2:
    print letter,

print '\n'

# a b c
