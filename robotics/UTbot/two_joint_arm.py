# -*- coding: utf-8 -*-
"""
Created on Wed Oct  8 01:17:32 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Engineering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author:  benito                             |
+------------------------------------------------------+
"""

def twojointarm(state,t,aparams):
        """
        passive two-joint arm in a vertical plane
        X is fwd(+) and back(-)
        Y is up(+) and down(-)
        gravity acts down
        shoulder angle a1 relative to Y vert, +ve counter-clockwise
        elbow angle a2 relative to upper arm, +ve counter-clockwise
        """
        a1,a2,a1d,a2d = state
        l1,l2 = aparams['l1'], aparams['l2']
        m1,m2 = aparams['m1'], aparams['m2']
        i1,i2 = aparams['i1'], aparams['i2']
        r1,r2 = aparams['r1'], aparams['r2']
        g = 9.81
        M11 = i1 + i2 + (m1*r1*r1) + (m2*((l1*l1) + (r2*r2) + (2*l1*r2*cos(a2))))
        M12 = i2 + (m2*((r2*r2) + (l1*r2*cos(a2))))
        M21 = M12
        M22 = i2 + (m2*r2*r2)
        M = matrix([[M11,M12],[M21,M22]])
        C1 = -(m2*l1*a2d*a2d*r2*sin(a2)) - (2*m2*l1*a1d*a2d*r2*sin(a2))
        C2 = m2*l1*a1d*a1d*r2*sin(a2)
        C = matrix([[C1],[C2]])
        G1 = (g*sin(a1)*((m2*l1)+(m1*r1))) + (g*m2*r2*sin(a1+a2))
        G2 = g*m2*r2*sin(a1+a2)
        G = matrix([[G1],[G2]])
        ACC = inv(M) * (-C-G)
        a1dd,a2dd = ACC[0,0],ACC[1,0]
        return [a1d, a2d, a1dd, a2dd]
        
