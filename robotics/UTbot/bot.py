# -*- coding: utf-8 -*-
"""
Created on Tue Oct  7 19:14:12 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                        |
+------------------------------------------------------+
"""

# import sympy
from sympy import *
#import numpy as np
from numpy import *
from matplotlib.pyplot import *
from scipy.integrate import odeint
import matplotlib.pyplot as plt

plt.ion()     # turns on interactive mode

def linea():
    print 55*'-'

# make up an elbow trajectory (pretend this was recorded in an experiment)
t = linspace(0,1,200)
theta = sin(2*pi*t/4)

figure(1, figsize=(20, 15))
subplot(2,3,(1,2))
plot(t,theta*180/pi)
xlabel('TIME (sec)')
ylabel('ELBOW ANGLE (deg)')

# compute hand position Hx,Hy
l = 0.45
Hx = l * cos(theta)
Hy = l * sin(theta)

subplot(2,3,(4,5))
plot(t,Hx,'b-')
plot(t,Hy,'r-')
xlabel('TIME (sec)')
ylabel('HAND POSITION (m)')
legend(('Hx','Hy'),loc='lower right')

subplot(2,3,(3,6))
plot((0,Hx[0]),(0,Hy[0]),'g-')
plot((0,Hx[-1]),(0,Hy[-1]),'r-')
plot(Hx[0:-1:10],Hy[0:-1:10],'k.')
xlabel('X (m)')
ylabel('Y (m)')
axis('equal')

plt.show()

print ' ... next!'
#%%---------------------------------------------

#_-^-__-^-__-^-__-^-__-^-__-^-__-^-__-^-__-^-_#
#_-^-__-^-__-^-__-^-__-^-__-^-__-^-__-^-__-^-_#


# Function to transform joint angles (a1,a2) to hand position (Hx,Hy)
def joints_to_hand(a1,a2,l1,l2):
  Ex = l1 * cos(a1)
  Ey = l1 * sin(a1)
  Hx = Ex + (l2 * cos(a1+a2))
  Hy = Ey + (l2 * sin(a1+a2))
  return Ex,Ey,Hx,Hy

# limb geometry
l1 = 0.34 # metres
l2 = 0.46 # metres

# decide on a range of joint angles
n1steps = 10
n2steps = 10
a1range = linspace(0*pi/180, 120*pi/180, n1steps) # shoulder
a2range = linspace(0*pi/180, 120*pi/180, n2steps)   # elbow

# sample all combinations and plot joint and hand coordinates
f2=figure(2,figsize=(8,12))
for i in range(n1steps):
  for j in range(n2steps):
    subplot(2,1,1)
    plot(a1range[i]*180/pi,a2range[j]*180/pi,'r+')
    ex,ey,hx,hy = joints_to_hand(a1range[i], a2range[j], l1, l2)
    subplot(2,1,2)
    plot(hx, hy, 'r+')
    
subplot(2,1,1)
xlabel('Shoulder Angle (deg)')
ylabel('Elbow Angle (deg)')
title('Joint Space')
subplot(2,1,2)
xlabel('Hand Position X (m)')
ylabel('Hand Position Y (m)')
title('Hand Space')

a1 = a1range[n1steps/2]
a2 = a2range[n2steps/2]
ex,ey,hx,hy = joints_to_hand(a1,a2,l1,l2)

subplot(2,1,1)
plot(a1*180/pi,a2*180/pi,'bo',markersize=5)
axis('equal')
xl = get(get(f2,'axes')[0],'xlim')
yl = get(get(f2,'axes')[0],'ylim')
plot((xl[0],xl[1]),(a2*180/pi,a2*180/pi),'b-')
plot((a1*180/pi,a1*180/pi),(yl[0],yl[1]),'b-')

subplot(2,1,2)
plot((0,ex,hx),(0,ey,hy),'b-')
plot(hx,hy,'bo',markersize=5)
axis('equal')
xl = get(get(f2,'axes')[1],'xlim')
yl = get(get(f2,'axes')[1],'ylim')
plot((xl[0],xl[1]),(hy,hy),'b-')
plot((hx,hx),(yl[0],yl[1]),'b-')

plt.show()

print ' ... next!'
#%%---------------------------------------------

#_-^-__-^-__-^-__-^-__-^-__-^-__-^-__-^-__-^-_#
#_-^-__-^-__-^-__-^-__-^-__-^-__-^-__-^-__-^-_#
#_-^-__-^-__-^-__-^-__-^-__-^-__-^-__-^-__-^-_#






linea()
print ' Define these variables as symbolic'
print "        a1,a2,l1,l2 = symbols('a1 a2 l1 l2')"
# define these variables as symbolic 
a1,a2,l1,l2 = symbols('a1 a2 l1 l2')

linea()
print " Forward kinematics for Hx and Hy"
# forward kinematics for Hx and Hy
hx = l1*cos(a1) + l2*cos(a1+a2)
hy = l1*sin(a1) + l2*sin(a1+a2)
print ' hx   = ', hx
print ' hy   = ', hy

linea()
print " Use sympy diff() to get partial "
print " derivatives for Jacobian matrix"
print "     J11 = diff(hx,a1)"
print "     J12 = diff(hx,a2)"
print "     J21 = diff(hy,a1)"
print "     J22 = diff(hy,a2)"
# use sympy diff() to get partial 
# derivatives for Jacobian matrix
J_11 = diff(hx,a1)
J_12 = diff(hx,a2)
J_21 = diff(hy,a1)
J_22 = diff(hy,a2)

linea()
print ' J_11 = diff(hx,a1) =', J_11
print ' J_12 = diff(hx,a2) =', J_12
print ' J_11 = diff(hy,a1) =', J_11
print ' J_11 = diff(hy,a2) =', J_22
linea()

print ' ... next!'
#%%---------------------------------------------

def jacobiand(A,Ad,aparams):
    """Given joint angles A=(a1,a2) and velocities Ad=(a1d,a2d) 
    returns the time derivative of the Jacobian matrix d/dt (J)"""
    l1 = aparams['l1']
    l2 = aparams['l2']
    Jd11 = -l1*cos(A[0])*Ad[0] - l2*(Ad[0] + Ad[1])*cos(A[0] + A[1])
    Jd12 = -l2*(Ad[0] + Ad[1])*cos(A[0] + A[1])
    Jd21 = -l1*sin(A[0])*Ad[0] - l2*(Ad[0] + Ad[1])*sin(A[0] + A[1])
    Jd22 = -l2*(Ad[0] + Ad[1])*sin(A[0] + A[1])
    Jd = matrix([[Jd11, Jd12],[Jd21, Jd22]])
    return Jd


print ' ... next!'
#%%---------------------------------------------


print ' ... done!'


