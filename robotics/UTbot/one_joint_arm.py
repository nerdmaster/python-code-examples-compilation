# -*- coding: utf-8 -*-
"""
Created on Wed Oct  8 00:50:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""
from  numpy import *
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from matplotlib import *

plt.ion()     # turns on interactive mode

def onejointarm(state,t):
    theta = state[0]      # joint angle (rad)
    theta_dot = state[1]  # joint velocity (rad/s)
    m = 1.65              # kg
    r = 0.50              # link length (m)
    b = 0.20              # coefficient of friction (N m s)
    g = 9.81              # gravitational constant (m/s/s)
    i = 0.025             # moment of inertia (kg m m)
    theta_ddot = -(m*g*r*sin(theta)+b*theta_dot) / (i + (m*r*r))
    return [theta_dot, theta_ddot]

t = linspace(0.0,10.0,1001)   # 10 seconds sampled at 1000 Hz
state0 = [90.0*pi/180.0, 0.0] # 90 deg initial angle, 0 deg/sec initial velocity
state = odeint(onejointarm, state0, t)

plt.figure(1)
plt.plot(t,state*180/pi)
plt.legend(('${\theta}$','${\dot{\theta}}$'))
plt.xlabel('$t$ (sec)')
plt.ylabel('$\theta$ [deg] & $\dot{\theta}$ [deg/sec]')
plt.show()

print ' ... done!'
