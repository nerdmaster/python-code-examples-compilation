# -*- coding: utf-8 -*-
"""
Created on Tue Oct  7 20:19:50 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                        |
+------------------------------------------------------+
"""
from sympy import *
from numpy import *
from matplotlib.pyplot import *

# Function to transform joint angles (a1,a2) to hand position (Hx,Hy) 
def joints_to_hand(a1,a2,l1,l2):
    Ex = l1 * cos(a1)
    Ey = l1 * sin(a1)
    Hx = Ex + (l2 * cos(a1+a2)) 
    Hy = Ey + (l2 * sin(a1+a2)) 
    return Ex,Ey,Hx,Hy
    
# limb geometry 
l1 = 0.34 # metres 
l2 = 0.46 # metres

# decide on a range of joint angles
n1steps = 10
n2steps = 10
a1range = linspace(0*pi/180, 120*pi/180, n1steps) # shoulder 
a2range = linspace(0*pi/180, 120*pi/180, n2steps) # elbow

f1=figure(1,figsize=(8,12))
# sample all combinations and plot joint and hand coordinates f=figure(figsize=(8,12))
for i in range(n1steps):
    for j in range(n2steps):
        subplot(2,1,1) 
        plot(a1range[i]*180/pi,a2range[j]*180/pi,'r+')
        ex,ey,hx,hy = joints_to_hand(a1range[i], a2range[j], l1, l2) 
        subplot(2,1,2)
        plot(hx, hy, 'r+')

subplot(2,1,1) 
xlabel('Shoulder Angle (deg)') 
ylabel('Elbow Angle (deg)') 
title('Joint Space') 

subplot(2,1,2)
xlabel('Hand Position X (m)')
ylabel('Hand Position Y (m)')
title('Hand Space')

f1.block=False
show(f1)#,block=False)

a1 = a1range[n1steps/2]
a2 = a2range[n2steps/2]

f2=figure(2,figsize=(8,12))
ex,ey,hx,hy = joints_to_hand(a1,a2,l1,l2) 
subplot(2,1,1) 
plot(a1*180/pi,a2*180/pi,'bo',markersize=5)
axis('equal')
xl = get(get(f2,'axes')[0],'xlim')
yl = get(get(f2,'axes')[0],'ylim') 
plot((xl[0],xl[1]),(a2*180/pi,a2*180/pi),'b-') 
plot((a1*180/pi,a1*180/pi),(yl[0],yl[1]),'b-') 

subplot(2,1,2)
plot((0,ex,hx),(0,ey,hy),'b-') 
plot(hx,hy,'bo',markersize=5)
axis('equal')
xl = get(get(f2,'axes')[1],'xlim')
yl = get(get(f2,'axes')[1],'ylim') 
plot((xl[0],xl[1]),(hy,hy),'b-') 
plot((hx,hx),(yl[0],yl[1]),'b-')

show(f2)#,block=False)