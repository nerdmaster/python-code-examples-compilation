# -*- coding: utf-8 -*-
"""
Created on Tue Oct  7 20:09:08 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                        |
+------------------------------------------------------+
"""

# import sympy
from sympy import *
import numpy  as np

def linea():
    print 55*'-'

linea()
print ' Define these variables as symbolic'
print "        a1,a2,l1,l2 = symbols('a1 a2 l1 l2')"
# define these variables as symbolic 
a1,a2,l1,l2 = symbols('a1 a2 l1 l2')

linea()
print " Forward kinematics for Hx and Hy"
# forward kinematics for Hx and Hy
hx = l1*cos(a1) + l2*cos(a1+a2)
hy = l1*sin(a1) + l2*sin(a1+a2)
print ' hx   = ', hx
print ' hy   = ', hy

linea()
print " Use sympy diff() to get partial "
print " derivatives for Jacobian matrix"
print "     J11 = diff(hx,a1)"
print "     J12 = diff(hx,a2)"
print "     J21 = diff(hy,a1)"
print "     J22 = diff(hy,a2)"
# use sympy diff() to get partial 
# derivatives for Jacobian matrix
J_11 = diff(hx,a1)
J_12 = diff(hx,a2)
J_21 = diff(hy,a1)
J_22 = diff(hy,a2)

linea()
print ' J_11 = diff(hx,a1) =', J_11
print ' J_12 = diff(hx,a2) =', J_12
print ' J_11 = diff(hy,a1) =', J_11
print ' J_11 = diff(hy,a2) =', J_22
linea()


def jacobian(A,aparams):
   """
   Given joint angles A=(a1,a2)
   returns the Jacobian matrix J(q) = dH/dA
   """
   l1 = aparams['l1']
   l2 = aparams['l2']
   dHxdA1 = -l1*sin(A[0]) - l2*sin(A[0]+A[1])
   dHxdA2 = -l2*sin(A[0]+A[1])
   dHydA1 = l1*cos(A[0]) + l2*cos(A[0]+A[1])
   dHydA2 = l2*cos(A[0]+A[1])
   J = np.matrix([[dHxdA1,dHxdA2],[dHydA1,dHydA2]])
   return J

aparams = {'l1' : 0.3384, 'l2' : 0.4554}
A = np.array([45.0,90.0])*np.pi/180       # joint angles
Ad = np.matrix([[-5.0],[3.0]])*np.pi/180  # joint velocities
J = jacobian(A,aparams)
Hd = J*Ad
print Hd


def jacobiand(A,Ad,aparams):
    """
    Given joint angles A=(a1,a2) and velocities Ad=(a1d,a2d)
    returns the time derivative of the Jacobian matrix d/dt (J)
    """
    l1 = aparams['l1']
    l2 = aparams['l2']
    Jd11 = -l1*cos(A[0])*Ad[0] - l2*(Ad[0] + Ad[1])*cos(A[0] + A[1])
    Jd12 = -l2*(Ad[0] + Ad[1])*cos(A[0] + A[1])
    Jd21 = -l1*sin(A[0])*Ad[0] - l2*(Ad[0] + Ad[1])*sin(A[0] + A[1])
    Jd22 = -l2*(Ad[0] + Ad[1])*sin(A[0] + A[1])
    Jd = matrix([[Jd11, Jd12],[Jd21, Jd22]])
    return Jd


