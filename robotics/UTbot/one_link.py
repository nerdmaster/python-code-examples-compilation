# -*- coding: utf-8 -*-
"""
Created on Tue Oct  7 19:57:01 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                        |
+------------------------------------------------------+
"""

from numpy import *
from matplotlib.pyplot import *
import matplotlib.pyplot as plt

plt.ion()     # turns on interactive mode

def linea():
    print 55*'-'

# make up an elbow trajectory (pretend this was recorded in an experiment)
t = linspace(0,1,200)
theta = sin(2*pi*t/4)

figure(1, figsize=(20, 15))
subplot(2,3,(1,2))
plot(t,theta*180/pi)
xlabel('Time [sec]')
ylabel('Elbow Angle, $\theta(t)$ [deg]')

# compute hand position Hx,Hy
l = 0.45
Hx = l * cos(theta)
Hy = l * sin(theta)

subplot(2,3,(4,5))
plot(t,Hx,'b-')
plot(t,Hy,'r-')
xlabel('TIME (sec)')
ylabel('Hand position, $x(t), y(t)$ [m]')
legend(('Hx','Hy'),loc='lower right')

subplot(2,3,(3,6))
plot((0,Hx[0]),(0,Hy[0]),'g-')
plot((0,Hx[-1]),(0,Hy[-1]),'r-')
plot(Hx[0:-1:10],Hy[0:-1:10],'k.')
xlabel('$x(t)$ [m]')
ylabel('$y(t)$ [m]')
axis('equal')

plt.show()

print ' ... done!'
